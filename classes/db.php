<?php
switch($DBTYPE) {

	case 'MySQL':
		require($the_include."/mysql.php");
		break;
	case 'mysql4':
		require($the_include."/mysql4.php");
		break;

	case 'postgres':
		require($the_include."/postgres7.php");
		break;

	case 'mssql':
		require($the_include."/mssql.php");
		break;

	case 'oracle':
		require($the_include."/oracle.php");
		break;

	case 'msaccess':
		require($the_include."/msaccess.php");
		break;

	case 'mssql-odbc':
		require($the_include."/mssql-odbc.php");
		break;
	
	case 'db2':
		require($the_include."/db2.php");
		break;
}


$db = new sql_db($DBHOST, $UNAME, $DBPASSWORD, $DBNAME, true);

if(!$db->db_connect_id) { 
    die("<br><br><center><img src=\"".IMAGES."mysql.gif\" width=\"100\"><br><br><b>There seems to be a problem with the MySQL server, sorry for the inconvenience.<br><br>We should be back shortly.</center></b>");
}

?>