<?php
require_once("EasyValidation.php");
$rules = array(
    "username" => array("required"=>true, "minLength"=>5, "maxLength"=>30, "pcre"=>"/^[a-zA-Z]*$/"),
    "password" => array("required"=>true, "minLength"=>5, "maxLength"=>100),
    "email" => array("email"=>true)
);
$errMsgs = array(
    "#password" => array("minLength"=>"For security plase make sure your pass is minimum :value chars long."),
    "pcre" => "Not matches pattern.",
    "required" => ":field is required.",
    "alnum"=> ":field should only contain alphanumeric characters.",
    "minLength" => ":field must be a minimum of :value characters.",
    "maxLength" => ":field must be a maximum of :value characters.",
    "email" => "Not a valid email address."
);
try{
if( !empty($_POST["submit"]) ){
    
    $validator = new EasyValidation($errMsgs);
    $validator->check($_POST, $rules);
   
    if( $e = $validator->getErrors() ){
        echo "<pre>", print_r($e), "</pre>";
    }
    else{
        echo "<b>Valid</b>";
    }
}
}
catch(Exception $e){
    echo $e->getMessage();
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style type="text/css">
            ul{
                list-style: none;
            }
            li{
                margin: 5px 0;
                font-size: 1.2em;
            }
            label{
                display: inline-block;
                width: 200px;
            }
        </style>
    </head>
    <body>
        <form action="" method="POST">
            <ul>
                <li>
                    <label for="username">Username</label>
                    <input type="text" name="username" id="username" />
                </li>
                <li>
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" />
                </li>
                <li>
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" />
                </li>
                <li>
                   <input type="submit" name="submit" value="Register" />
                </li>
            </ul>
        </form>
    </body>
</html>
