<?php
ini_set('display_errors', '1');
if (!isset($_SESSION)) {
    session_start();
}
ob_start();
date_default_timezone_set('Pacific/Auckland');
$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') ? 'https' : 'http';
$num_of_facility = 3;
$the_include = 'includes/db';
if (strpos($_SERVER['REQUEST_URI'], 'admin')) {
    $the_include = 'classes';
} else {
    $the_include = 'classes';
}
switch ($_SERVER['HTTP_HOST']) {
    CASE 'localhost':
        $DBTYPE = "MySQL";
        $DBHOST = 'localhost'; //192.168.1.12';
        $DBNAME = 'new_agedadvisor_live';
        $UNAME = 'agedadvisor';
        $DBPASSWORD = 'f3Cdc7~3';
        $prefix = 'ad_';
        break;
    default:
        $DBTYPE = "MySQL";
        $DBHOST = 'localhost'; //192.168.1.12';
        $DBNAME = 'agedadvisor';
        $UNAME = 'root';
        $DBPASSWORD = 'root';
        $prefix = 'ad_';
}

require_once("includes/common/common.php");

require_once("includes/constant.php");

require_once("includes/common/message.php");

require_once("db.php");

require_once("includes/facebook.php");

require_once ('twitter/twitteroauth.php');

require_once("classes/mainClass.class.php");

require_once("includes/common/functions.php");

require_once("classes/ajaxPagingManager.class.php");

require_once("classes/validation.class.php");

require_once("classes/EasyValidation.php");

require_once("admin/server_validations.php");

require_once("includes/methods.php");

?>

