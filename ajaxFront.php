<?php

include("application_top.php");
global $db;
switch ($_POST['action']) {
    //geenerate Coupon Code for gift card in admin coupon section
    case 'plan':
    echo durationData($type);
    break;
    // state list on front/admin
    case 'status' :
        //use this for status change
    echo status($table, $id, $status);
    break;
    case "enquiry" :
    echo enquiry($id);
    break;
    case "extra-enquiry-not-upgraded-not-interested" :
    echo extenqunupgnin($data);
    break;
    case "new-enquiry-to-upgrade" :
    echo newenquiryupgrade($prodid,$supid,$enqid,$supname,$supphone);
    break;    
    case "new-enquiry-to-paylater" :
    echo newenquirypaylater($prodid,$supid,$enqid,$supname,$supposition);
    break;
    case "review-friendly" :
    echo reviewfriendly($prodid,$rfval);
    break;
    case 'abuse' :
        //use this for status change
    echo abuse($table, $id, $status, $reason);
    break;
    // Delete record in admin panel
    case "delete" :
    echo delete($table, $id);
    break;
    case "paid":
    echo payableAmount($id);
    break;
    case "cities":
    echo getCity($name);
    break;
    case "getSupplierProList":
    echo getSupplierProList($name,"AND (prd.certification_service_type='Retirement Village' OR prd.certification_service_type='Aged Care') ");
    break;
    case "getHomeCareProList":
    echo getSupplierProList($name,"AND prd.certification_service_type='Home Care' ");
    break;
    case "price":
    echo getPrice($days, $to, $from, $facilityTypeId);
    break;
    case "getSuburb":
    echo getSuburb($cityId);
    break;
    case "getRestCare":
    echo getRestCare($suburbId);
    break;
    case "getProduct":
    echo getProduct($supplierId);
    break;
    case "deleteFile":
    echo deleteFile($table, $id);
    break;
    case "readStatus":
    echo readStatus($table, $id, $read_status);
    break;
    case "facilityname":
    echo facilityName($name);
    break;
    case "citiesname":
    echo citiesName($name);
    break;
    case "logResponse":
    echo log_response($response);
    break;
}

function facilityName($name) {
    global $db;
    $sql = "select id, title from ad_products WHERE deleted=0 AND  title LIKE '%" . $name . "%'";
    $result = mysqli_query($db->db_connect_id, $sql) or die("Error " . mysqli_error("error"));

    $event_list = array();
    while($row = mysqli_fetch_assoc($result))
    {
        array_push($event_list,array("value"=>$row['id'],"label"=>$row['title']));
    }
       
    return json_encode($event_list);
    //echo  json_encode($event_list);
}

function getRestCare($suburbId) {
    global $db;
    $cityList = array();
    $sql_query = "select id, title from " . _prefix('rest_cares') . " WHERE suburb_id = " . $suburbId . " AND status =1  ORDER BY title ASC";
    $res = mysqli_query($db->db_connect_id, $sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $cityList[$records['id']] = $records['title'];
    }

    $select = '';
    $option = '<option value="">---------------Select-------------------</option>';
    foreach ($cityList as $key => $value) {
        if ($selected != null) {
            $select = $selected == $key ? 'selected' : '';
        }
        $option .= '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
    }
    return $option;
}

function getSuburb($cityId) {
    global $db;
    $cityList = array();
    $sql_query = "select id, title from " . _prefix('suburbs') . " WHERE city_id = " . $cityId . " AND status =1  ORDER BY title ASC";
    $res = mysqli_query($db->db_connect_id, $sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $cityList[$records['id']] = $records['title'];
    }

    $select = '';
    $option = '<option value="">---------------Select-------------------</option>';
    foreach ($cityList as $key => $value) {
        if ($selected != null) {
            $select = $selected == $key ? 'selected' : '';
        }
        $option .= '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
    }
    return $option;
}

//function getCity($name) {
//    global $db;
//    $sql_query = "SELECT city.id as id, city.title, sb.id AS suburbId, sb.title AS suburb , prd.id AS restCareId, prd.title AS restCare FROM ad_products AS prd "
//            . " Left Join " . _prefix("cities") . " AS city ON city.id = prd.city_id AND city.status =1 AND city.deleted =0 "
//            . " Left Join " . _prefix("suburbs") . " AS sb ON sb.id=prd.suburb_id AND sb.status =1 AND sb.deleted = 0"
//            . " WHERE city.deleted =0 AND city.status =1 AND (city.title LIKE '%" . $name . "%' OR prd.title LIKE '%" . $name . "%' ) LIMIT 15";
//    $res = mysqli_query($db->db_connect_id, $sql_query);
//    while ($records = mysqli_fetch_assoc($res)) {
//        $recordList[] = $records;
//    }
////        $records = mysqli_fetch_assoc($res);
//    return json_encode($recordList);
//}
function getCity($name) {
    global $db;
//    $sql_query = "SELECT city.id AS id, city.title, null AS restCare, null AS proCity, null AS proSuburb, null as providers FROM " . _prefix("cities") . " AS city"
//    . " WHERE city.title like '%" . $name . "%' AND  city.deleted =0 AND city.status =1"
//    . " UNION ALL "
//    . " SELECT NULL AS id, prd.address_suburb AS title, null AS restCare, null AS proCity, null AS proSuburb, null as providers FROM " . _prefix("products") . " AS prd"
//    . " WHERE prd.address_suburb like '%" . $name . "%' AND  prd.deleted =0 AND prd.status =1 group by prd.address_suburb"
//    . " UNION ALL"
//    . " SELECT city.id AS id, city.title,prd.title AS restCare, prd.address_city AS proCity, prd.address_suburb AS proSuburb, usr.company as providers FROM ad_products AS prd"
//    . " LEFT JOIN " . _prefix("cities") . " AS city ON city.id = prd.city_id AND city.status = 1 AND city.deleted = 0 "  
//    . " LEFT JOIN " . _prefix("users") . " AS usr ON usr.id = prd.supplier_id AND usr.status = 1 AND usr.deleted = 0 AND usr.user_type = 1 AND usr.company <> '' "
//    . " WHERE prd.deleted=0 AND   city.deleted =0 AND city.status = 1 AND (CONCAT(city.title, ', ' , prd.address_suburb, ', ', prd.title) LIKE '%" . $name . "%'"
//    . " OR CONCAT(city.title, ', ' , prd.address_suburb) LIKE '%" . $name . "%' OR CONCAT(prd.address_city, ', ' , prd.address_suburb, ', ' , prd.title) LIKE '%" . $name . "%'"
//    . " OR CONCAT(prd.address_city, ', ' , prd.address_suburb) LIKE '%" . $name . "%' OR city.title LIKE '%" . $name . "%' OR prd.address_city LIKE '%" . $name . "%'"
//    . " OR prd.address_suburb LIKE '%" . $name . "%'OR prd.title LIKE '%" . $name . "%' OR usr.company LIKE '%" . $name . "%' ) LIMIT 15";
    
    $sql_query = " SELECT city.id AS id, city.title, null AS restCare, null AS proCity, null AS proSuburb, null as providers FROM " . _prefix("cities") . " AS city"
    . " WHERE city.title like '%" . $name . "%' AND  city.deleted =0 AND city.status = 1 "
    . " UNION "
    . " SELECT NULL AS id, prd.address_suburb AS title, null AS restCare, null AS proCity, null AS proSuburb, null as providers FROM " . _prefix("products") . " AS prd"
    . " WHERE prd.address_suburb like '%" . $name . "%' AND  prd.deleted =0 AND prd.status =1 group by prd.address_suburb "
    . " UNION "
    . " SELECT NULL AS id, usr.company AS title, null AS restCare, null AS proCity, null AS proSuburb, usr.company as providers FROM " . _prefix("users") . " AS usr"
    . " WHERE usr.company like '%" . $name . "%' AND  usr.deleted =0 AND usr.status = 1  AND usr.company <> ''"
    . " UNION "
    . " SELECT city.id AS id, city.title,prd.title AS restCare, prd.address_city AS proCity, prd.address_suburb AS proSuburb, usr.company as providers FROM ad_products AS prd"
    . " LEFT JOIN " . _prefix("cities") . " AS city ON city.id = prd.city_id AND city.status = 1 AND city.deleted = 0 "  
    . " LEFT JOIN " . _prefix("users") . " AS usr ON usr.id = prd.supplier_id AND usr.status = 1 AND usr.deleted = 0 AND usr.user_type = 1 AND usr.company <> '' "
    . " WHERE prd.deleted=0 AND city.deleted =0 AND city.status = 1 AND (CONCAT(city.title, ', ' , prd.address_suburb, ', ', prd.title) LIKE '%" . $name . "%'"
    . " OR CONCAT(city.title, ', ' , prd.address_suburb) LIKE '%" . $name . "%' OR CONCAT(prd.address_city, ', ' , prd.address_suburb, ', ' , prd.title) LIKE '%" . $name . "%'"
    . " OR CONCAT(prd.address_city, ', ' , prd.address_suburb) LIKE '%" . $name . "%' OR city.title LIKE '%" . $name . "%' OR prd.address_city LIKE '%" . $name . "%'"
    . " OR prd.address_suburb LIKE '%" . $name . "%'OR prd.title LIKE '%" . $name . "%' OR usr.company LIKE '%" . $name . "%' ) LIMIT 15";

    $res = mysqli_query($db->db_connect_id, $sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $recordList[] = $records;
    }
    return json_encode($recordList);
}



// get auto suggestion at reivew page
function getSupplierProList($name,$type="AND (prd.certification_service_type='Retirement Village' OR prd.certification_service_type='Aged Care')") {
    global $db;
    $sql_query = "SELECT city.id as id, city.title AS cty,user.id AS supplierId, sb.id AS suburbId, sb.title AS suburb , prd.id AS restCareId, prd.title AS restCare, prd.certification_service_type AS servType FROM ad_products AS prd "
    . " Left Join " . _prefix("users") . " AS user ON user.id = prd.supplier_id AND user.status =1 AND user.deleted =0 "
    . " Left Join " . _prefix("cities") . " AS city ON city.id = prd.city_id AND city.status =1 AND city.deleted =0 "
    . " Left Join " . _prefix("suburbs") . " AS sb ON sb.id=prd.suburb_id AND sb.status =1 AND sb.deleted = 0"
    . " WHERE  city.deleted =0 AND city.status =1 AND (city.title LIKE '%" . $name . "%' OR prd.title LIKE '%" . $name . "%' ) ".$type." LIMIT 15";
    $res = mysqli_query($db->db_connect_id, $sql_query);
    while ($records = mysqli_fetch_assoc($res)) {

        $recordList[] = $records;
    }
    if (empty($recordList)) {
        $recordList[] = array('restCare' => 'Your search did not find any match. Please try again.');
        return json_encode($recordList);
    } else {
        return json_encode($recordList);
    }
}

function getPrice($days, $to, $from, $facilityTypeId) {
    //$db;
    $condition = "(('Reservation.deleted' = 0) AND ('Reservation.status' = 1) AND  ('Reservation.service_category_id' = $facilityTypeId) AND (Reservation.from_date < '$to') AND (Reservation.to_date > '$from') AND (Reservation.from_date > '$to') AND (Reservation.to_date < '$from'))";
    $sql_query = "SELECT price  FROM " . _prefix("ads_masters") . " where  $condition";
    $ckeckResult = mysqli_query($db->db_connect_id, $sql_query);
    $result = mysqli_fetch_array($ckeckResult);
//        prd(count($result));

    if (count($result) <= 5) {

        $sql_query = "SELECT price  FROM " . _prefix("space_prices") . " where  no_of_day ='$days'";
        $res = mysqli_query($db->db_connect_id, $sql_query);
        while ($records = mysqli_fetch_assoc($res)) {
            $total = $records['price'];
        }
        if ($total == '') {
            $sql_query = "SELECT price  FROM " . _prefix("space_prices") . " where id=1";
            $res = mysqli_query($db->db_connect_id, $sql_query);
            while ($records = mysqli_fetch_assoc($res)) {
                $price_per_day = $records['price'];
            }
            $total = $price_per_day * $days;
            echo $total;
        } else {
            //$count = $db->sql_numrows($res);
            echo $total;
            die;
        }
    } else {
        echo 'N/A';
        die;
    }
}

function delete($table, $id) {
    global $db;
    $updateSql = "UPDATE " . _prefix("$table") . " SET deleted = '1' WHERE id= $id";
    $updateResult = $db->sql_query($updateSql);
    if ($table == 'point_earning_setting') {
        $insert_data = "UPDATE " . _prefix("$table") . " SET referral_point='',referral_value = '', social_point = '',social_value = '', "
        . "min_value = '', client_point = '', client_value = '' WHERE id = $id";
        $db->sql_query($insert_data);
    }
}

function status($table, $id, $status) {
    global $db;
    $changeStatus = $status == 1 ? 0 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET status = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        $CurrentStatus = $changeStatus == 0 ? '<img src="' . ADMIN_IMAGE . 'deactivate.png" title="Inactive" />' : '<img src="' . ADMIN_IMAGE . 'activate.png" title="active"/>';
        echo "<a href='javascript:void(0);' id='$table-" . $id . "-" . $changeStatus . "' class='status'>$CurrentStatus</a>";
    }
}

function enquiry($id) {
    global $db;
    $dateTime = date('Y-m-d H:i:s');    
    $UpdateSql = "UPDATE " . _prefix("enquiries") . " SET clicked_display = 1, clicked_display_time = '".$dateTime."'  WHERE md5(id)= '$id'";    
    $updateResult = $db->sql_query($UpdateSql); 
    if($updateResult){
        echo "1";
    }else{
        echo "0";
    }
}



function extenqunupgnin($data){
global $db;
$dateTime = date('Y-m-d H:i:s');  
$params = array();
parse_str($data, $params);

$enq_id  = $params['enquiry_id'];
$prid    = $params['prid'];
$uid     = $params['uid'];
$suid    = $params['suid'];
$reason  = $params['sec-modal'];
$insertVal = false;

if($reason=="not"){ $reason = "NOT 'CURRENTLY' TAKING ENQUIRIES"; $insertVal = 1; }
if($reason=="nin"){ $reason = "NOT TAKING ENQUIRIES"; $insertVal = 2; }

if($insertVal == 2){    
    
    $qryupdates="update ad_enquiries set time_of_response='$dateTime' where md5(id)='$enq_id'"; 
    $resqrychk=$db->sql_query($qryupdates);
    
    $UpdateSql = "UPDATE " . _prefix("enquiries") . " SET taking_enquiries = $insertVal, facility_responded = 'y'  WHERE md5(prod_id)= '$prid'";            
    $updateResult = $db->sql_query($UpdateSql);       
}

if($insertVal == 1){
    $UpdateSql = "UPDATE " . _prefix("enquiries") . " SET taking_enquiries = $insertVal, facility_responded = 'y',time_of_response='$dateTime'  WHERE md5(id)= '$enq_id'";            
    $updateResult = $db->sql_query($UpdateSql); 
}


$userQuery = "SELECT `first_name`,`last_name`,`email` FROM `ad_users` WHERE md5(id)='$uid'";
$userQueryRes = $db->sql_query($userQuery);
$userData = $db->sql_fetchrowset($userQueryRes);

foreach($userData as $user){        
    $first_name = $user['first_name'];
    $last_name = $user['last_name'];
    $useremail = $user['email'];            
    $username = $first_name.' '.$last_name; 
}

$addressQuery ="SELECT `title`,`address_suburb`,`address_city`,`zip`, `latitude`, `longitude` FROM `ad_products` WHERE md5(id)='$prid'";
$addressQueryRes = $db->sql_query($addressQuery);
$addressData = $db->sql_fetchrowset($addressQueryRes);

foreach($addressData as $addData){        
    $suburb = $addData['address_suburb'];
    $city = $addData['address_city'];
    $zip = $addData['zip'];        
    $lat = $addData['latitude'];
    $long = $addData['longitude']; 
    $title = $addData['title']; 
}
              
$link_to_facility_in_area = "<a style='background-color: #F15922;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='https://www.agedadvisor.nz/other-facilities/?sub=".$suburb."&pid=".$prid."&city=".$city."&zip=".$zip."&lat=".$lat."&long=".$long."&uid=".$uid."'>Click to view other facilities in the area</a>";  

    $to = $useremail;
    $headers = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $headers .= 'From:   ';            
    $headers .= "reviews@agedadvisor.co.nz";
    $headers .= "\n";
    $headers .= "Cc: reviews@agedadvisor.co.nz\r\n";
    $emailTemplate = emailTemplate( 'Enq-availability-reason' );   
    if ( $emailTemplate[ 'subject' ] != '' ) {       
        $message    = str_replace( array(
            '{name}',            
            '{facility_name}', 
            '{reason}',
            '{link_to_facility_in_area}' 
        ), array(
            $username,            
            $title,
            $reason,
            $link_to_facility_in_area
        ), stripslashes( $emailTemplate[ 'description' ] ) );                
        $result  = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $title, $message, $headers );
    }
    if ( $result ) {
        echo '1';
    } else {
        echo '<font color="#FF0000">Server Error!</font>';
    }
}






function newenquiryupgrade($prodid,$supid,$enqid,$supname,$supphone){
global $db;

$qryDayresp="select id,user_id,facility_name,time_of_enquiry,time_of_response,facility_responded from ad_enquiries where md5(id)='$enqid'";
$resDayresp=$db->sql_query($qryDayresp);
$rowDayresp=$db->sql_fetchrowset($resDayresp);

$dateTime = date('Y-m-d H:i:s');  
if($rowDayresp['enquiry_paid_status']!=2){
    $qryupdate="update ad_enquiries set time_of_response='$dateTime' where md5(id)='$enqid'";
    $resqry=$db->sql_query($qryupdate);
}

$enqidQuery = "SELECT `id` FROM `ad_enquiries` WHERE md5(id)='$enqid'";
$enqidQueryRes = $db->sql_query($enqidQuery);
$enqidData = $db->sql_fetchrowset($enqidQueryRes);

foreach($enqidData as $data){        
    $id = $data['id'];    
}

$titleQuery ="SELECT `title` FROM `ad_products` WHERE md5(id)='$prodid'";
$titleQueryRes = $db->sql_query($titleQuery);
$titleData = $db->sql_fetchrowset($titleQueryRes);

foreach($titleData as $data){           
    $title = $data['title']; 
}    
    $to = "nigel@agedadvisor.co.nz";
    $headers = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $headers .= 'From:   ';            
    $headers .= "reviews@agedadvisor.co.nz";
    $headers .= "\n";
    $headers .= "Cc: reviews@agedadvisor.co.nz\r\n";
    $emailTemplate = emailTemplate( 'enq-availability-to-upgrade' );   
    if ( $emailTemplate[ 'subject' ] != '' ) {       
        $message    = str_replace( array(
            '{sup_name}',            
            '{facility_name}', 
            '{sup_phone}',
            '{enq_id}' 
        ), array(
            $supname,            
            $title,
            $supphone,
            $id
        ), stripslashes( $emailTemplate[ 'description' ] ) );                
        $result  = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $title . ' at ' . date( "g:i a d.m.y" ), $message, $headers );
    }
    if ( $result ) {        
        $updateEnq = "UPDATE ad_enquiries SET `upgrade_name` = '$supname', `upgrade_phone` = '$supphone', `enquiry_paid_status` = '2' WHERE id= $id";
        $updateResult = $db->sql_query($updateEnq);
        if ($updateResult) {
            echo '1';
        }
    } else {
        echo '<font color="#FF0000">Server Error!</font>';
    }
}

function newenquirypaylater($prodid,$supid,$enqid,$supname,$supposition){
global $db;

$dateTime = date('Y-m-d H:i:s');    
$qryupdates="update ad_enquiries set time_of_response='$dateTime' where md5(id)='$enqid'"; 
$resqrychk=$db->sql_query($qryupdates);


$enqidQuery = "SELECT `id` FROM `ad_enquiries` WHERE md5(id)='$enqid'";
$enqidQueryRes = $db->sql_query($enqidQuery);
$enqidData = $db->sql_fetchrowset($enqidQueryRes);

$roomsQuery = "SELECT no_of_room, exfac.no_of_beds nobeds, certification_service_type from ad_extra_facility AS exfac"
            . " LEFT JOIN ad_products AS adpro ON adpro.id = exfac.product_id "
            . "where md5(product_id)='$prodid' AND exfac.status=1 AND adpro.status=1";     
    $roomsQueryRes = $db->sql_query( $roomsQuery );

    while ( $roomsQueryData = $db->sql_fetchrow($roomsQueryRes ) ) {
        $aprtmt = $roomsQueryData['no_of_room'];
        $beds = $roomsQueryData['nobeds'];
        $fac_type = $roomsQueryData['certification_service_type'];
    }

    if($fac_type == "Aged Care"){    
        if($beds >=100){
            $amt = 16.62;
            $paylateramt = number_format(10*$amt,2);
        }
        if($beds >= 40 && $beds <= 99){
            $amt = 10.92;
            $paylateramt = number_format(10*$amt,2);
        }
        if($beds < 40 ){
            $amt = 5.72;
            $paylateramt = number_format(10*$amt,2);
        }
    }

    if($fac_type == "Retirement Village" || $fac_type == "Home Care" ){
        if($aprtmt >=30 && $aprtmt <= 79 ){
            $amt = 28.17;
            $paylateramt = number_format(10*$amt,2);
        }
        if($aprtmt >= 80){
            $amt = 33.92;
            $paylateramt = number_format(10*$amt,2);
        }
        if($aprtmt < 30 ){
            $amt = 22.42;
            $paylateramt = number_format(10*$amt,2);
        }        
    }       

foreach($enqidData as $data){        
    $id = $data['id'];    
}

$titleQuery ="SELECT `title` FROM `ad_products` WHERE md5(id)='$prodid'";

$titleQueryRes = $db->sql_query($titleQuery);
$titleData = $db->sql_fetchrowset($titleQueryRes);

foreach($titleData as $data){           
    $title = $data['title']; 
}    
    $to = "nigel@agedadvisor.co.nz";    
    $headers = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $headers .= 'From:   ';            
    $headers .= "reviews@agedadvisor.co.nz";
    $headers .= "\n";
    $headers .= "Cc: reviews@agedadvisor.co.nz\r\n";
    $emailTemplate = emailTemplate( 'enq-availability-to-paylater' );   
    if ( $emailTemplate[ 'subject' ] != '' ) {       
        $message    = str_replace( array(
            '{sup_name}',            
            '{facility_name}', 
            '{sup_position}',
            '{amt}',
            '{enq_id}' 
        ), array(
            $supname,            
            $title,            
            $supposition,
            $paylateramt,
            $id
        ), stripslashes( $emailTemplate[ 'description' ] ) );                
        $result  = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $title . ' at ' . date( "g:i a d.m.y" ), $message, $headers );
    }
    if ( $result ) {
        
        $updateEnq = "UPDATE ad_enquiries SET `paylater_amt` = '$paylateramt', `paylater_name` = '$supname',`paylater_position` = '$supposition',`enquiry_paid_status` = '3' WHERE id = $id";
        $updateResult = $db->sql_query($updateEnq);
        if ($updateResult) {        
            echo '1';
        }
    } else {
        echo '<font color="#FF0000">Server Error!</font>';
    }
}


function abuse($table, $id, $status, $reason) {
    global $db;
    if (isset($_SESSION['csId'])) {
        $customer_id = $_SESSION['csId'];
    } else if (isset($_SESSION['userId'])) {
        $customer_id = $_SESSION['userId'];
    } else {
        $msg = "First you login then Report a comment";
        $_SESSION['msg'] = $msg;
        redirect_to(HOME_PATH . 'login');
        die();
    }
    $abuse_date = date('Y/m/d h:i:s', time());
    $changeStatus = 1; //$status == 1 ? 1 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET abused_request = '".$changeStatus."',abuse_reason='".mysqli_escape_string($db->db_connect_id, $reason)."', abuse_date='".$abuse_date."',abused_by='".$customer_id."' WHERE id= ".$id."";
    
// Email Nigel reviews@agedadvisor.co.nz    
    $to_email='reviews@agedadvisor.co.nz';
    $email_subject='Moderation Request';
    $message="Customer ID = $customer_id \n
    Date = $abuse_date \n
    Reason = $reason \n\n
    RAW SQL = ".mysqli_escape_string($db->db_connect_id, $UpdateSql);
    $bcc='';
    sendmail($to_email, $email_subject, $message, '', $bcc);
    
    
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        echo '<p style="margin-bottom:0;margin-top:22px;color:#f15922;font-style: italic;">This comment has been flagged for moderation</p>';
    }
}

function durationData($type = NULL) {
    $durationList = array();
    $sql_query = "select id, duration,title from " . _prefix('membership_prices') . " where status = 1 AND type=" . $type . " ORDER BY id ASC";
    $res = mysqli_query($db->db_connect_id, $sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $durationList[$records['id']] = $records['title'] . " (" . $records['duration'] . " months)";
    }
    $option = '<option value=""> Duration (In months)</option>';
    foreach ($durationList as $key => $value) {
//            if ($selected != null) {
//                $select = $selected == $key ? 'selected' : '';
//            }
        if ($_SESSION['planId'] >= $key) {
            $option .= '<option value="' . $key . '" ' . $select . ' disabled="disable">' . $value . '</option>';
        } else {
            $option .= '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
        }
    }

    echo $option;
//        echo optionDuration($durationList, $selected);
    die;
}

function payableAmount($id = null) {
    global $db;
    $sql_query = "select price from " . _prefix('membership_prices') . " where id=" . $id . " AND status = 1 limit 1";
    $res = $db->sql_query($sql_query);
    $value = $db->sql_fetchrow($res);
//prd($value[price]);
    echo $value[price];
    die;
}

function getProduct($supplierId = null) {
    echo productList($supplierId, $productId);
}

function deleteFile($table, $id) {
    global $db;
    $query = "SELECT file,type FROM " . _prefix("$table") . " where id='$id'";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    $updateSql = "UPDATE " . _prefix("$table") . " SET deleted = '1' WHERE id= $id";
    $updateResult = $db->sql_query($updateSql);
    if ($updateResult) {
        if ($data[0]['type']) {
            @unlink(ADMIN_PATH . 'files/gallery/videos/' . $data[0]['file']);
            @unlink(ADMIN_PATH . 'files/gallery/videos/thumb_' . $data[0]['file']);
            @unlink(ADMIN_PATH . 'files/gallery/videos/thumb_feedback_' . $data[0]['file']);
        } else {
            @unlink(ADMIN_PATH . 'files/gallery/images/' . $data[0]['file']);
            @unlink(ADMIN_PATH . 'files/gallery/images/thumb_' . $data[0]['file']);
            @unlink(ADMIN_PATH . 'files/gallery/images/thumb_feedback_' . $data[0]['file']);
        }
    }
}
function readStatus($table, $id, $read_status) {
    global $db;
   // prd($id);
    $changeStatus = $read_status == 1 ? 0 : 1;
    $UpdateSql = "UPDATE " . _prefix("$table") . " SET read_status = '$changeStatus' WHERE id= $id";
    $updateResult = $db->sql_query($UpdateSql);
    if ($updateResult) {
        echo '<i class = "fa fa-check-square fa-2x" title="Read"></i>';
    }
}

function reviewfriendly($prodid,$rfval){
    global $db;     
    $_SESSION['rf'] = $rfval;    
    $UpdateSql = "UPDATE " . _prefix("products") . " SET review_friendly = '$rfval' WHERE id= $prodid";
    $updateResult = $db->sql_query($UpdateSql);    
    if ($updateResult) {        
        echo 1;
    }
}

function citiesName($name) {
    global $db;
    $sql = "select id, title from ad_cities WHERE title LIKE '%" . $name . "%'";
    //$sql = "select city.id, city.title, pro.city_id from ad_cities AS city inner join ad_products AS pro on city.id=pro.city_id WHERE pro.deleted=0 AND city.title LIKE '%" . $name . "%'";
    $result = mysqli_query($db->db_connect_id, $sql) or die("Error " . mysqli_error("error"));

    $event_list = array();
    while($row = mysqli_fetch_assoc($result))
    {
        array_push($event_list,array("value"=>$row['id'],"label"=>$row['title']));

    }


    return json_encode($event_list);
}

function log_response($response) {  
    global $db;            
    $dateTime = date('Y-m-d H:i:s'); 
    $UpdateSql = "UPDATE " . _prefix("enquiries") . " SET facility_responded = 'y', time_of_response = '$dateTime' WHERE md5(id) = '$response' ";
    $updateResult = $db->sql_query($UpdateSql);    
    if ($updateResult) {        
        return true;
    }
}
/************************************put this function from dev file *************************/
/*function facilityName($name) {
    global $db;
    $sql = "select id, title from ad_products WHERE deleted=0 AND  title LIKE '%" . $name . "%'";
    $result = mysqli_query($db->db_connect_id, $sql) or die("Error " . mysqli_error("error"));

    $event_list = array();
    while($row = mysqli_fetch_assoc($result))
    {
        array_push($event_list,array("value"=>$row['id'],"label"=>$row['title']));

       }
     
       
    return json_encode($event_list);

    //echo  json_encode($event_list);

}
*/
