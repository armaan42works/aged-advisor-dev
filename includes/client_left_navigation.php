<?php
   /* 
     * Objective  : Navigation on the Client dashboard
     * Filename : client_left_navigation.php
     * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com> 
     * Created On : 25 August 2014
 */

    if(isset($pathInfo['call_parts']['3']) && $pathInfo['call_parts']['3'] != '' && $pathInfo['call_parts']['2']  == 'inbox'){
         $update = array('is_read_client' => 1);
        $db->update(_prefix('messages'),$update, "WHERE (md5(id) = '{$pathInfo['call_parts']['3']}' || md5(parent_message_id) = '{$pathInfo['call_parts']['3']}')");
    }
   
$unread = 0;
$sql_query = "SELECT * FROM " . _prefix('messages') . " WHERE ((`receiver_id` ={$_SESSION['userId']} || sender_id = '0') || (`sender_id` ={$_SESSION['userId']} || receiver_id = '0')) && `is_read_client` =0 && (parent_message_id IS NULL || parent_message_id  = '0')";
$res = $db->sql_query($sql_query);
$unread = $db->sql_numrows($res);
?>
<div class="dashboard_container">
<div class="Dashbord_left_panel">
<ul class="Dashbord_nav">
<li class="<?php echo $submodule == 'quote' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/quote">Request a Free Quote</a></li>
<li class="nav_1 <?php echo $submodule == 'project' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/project">My Project</a></li>
<li class="nav_10 <?php echo $submodule == 'projectProgress' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/projectProgress">Projects In Progress</a></li>
<li class="nav_2 <?php echo $submodule == 'projectProposed' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/projectProposed">Proposed Projects</a></li>
<li class="nav_3 <?php echo $submodule == 'projectCompleted' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/projectCompleted">Completed Projects</a></li>
<li class="nav_4 <?php echo $submodule == 'coupon' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/coupon">My Coupons/Gift Cards</a></li>
<li class="nav_5 <?php echo $submodule == 'message' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/message">Message Box (<?php echo $unread; ?>)</a></li>
<li class="nav_6 <?php echo $submodule == 'payment' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/payment">Payment Records</a></li>
<li class="nav_7 <?php echo $submodule == 'library' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/library">File Library</a></li>
<li class="nav_8 <?php echo $submodule == 'profile' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/profile">My Profile</a></li>
<li class="nav_9 <?php echo $submodule == 'changePassword' ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH ?>user/changePassword">Change Password</a></li>
</ul>

</div>
