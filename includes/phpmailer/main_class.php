<?php
class  main_class{
  public $last_message;
  public $total_record;
  public function 	Tournament_name($t_id){
  
	  $Tournament_name=mysqli_fetch_array(mysqli_query($db->db_connect_id,"select Title from admintournament where TournamentID='".$t_id."'"));
	  return $Tournament_name[0];
  }
    public  function redirect($url){
	    header("location:".$url);
	 }
	 public  function  set_message($message){    ///    set Last message 
	    $_SESSION['last_message']=$message;
	 }
	 public  function  clear_message(){  ///    Get and elear  message 
	 if($_SESSION['last_message']!=''){
      echo '<table    width="100%" cellpadding="0" cellspacing="0" border="0">
		 <td class="grnhead">
		 <span style="color:#FF0000; font-weight:bold">'.$_SESSION['last_message'].'</span>
		 </td>
		 </table>';
		 }
	    $_SESSION['last_message']="";
		
	 }
	 public function Last_visited(){     ////  fetch administrator  last Login 
	     return date("dS  F  Y l  H:m:s",$_SESSION['admin_last_login']);
	 }
	 public function team_name($team_id){  ////// team name 
	  $team_name=mysqli_fetch_array(mysqli_query($db->db_connect_id,"select tName from adminteam where tTeamId='".$team_id."' and dDelete='0'"));
	  return $team_name[0];
	 }
	 
	 public function position_name($position_id){ /// designation / position name
	  $Position=mysqli_fetch_array(mysqli_query($db->db_connect_id,"select Position from position where PositionId='".$position_id."'"));
	  return $Position[0];
	 }
	 
	 public function club_name($club_id){ /// designation / position name
	  $club=mysqli_fetch_array(mysqli_query($db->db_connect_id,"select cName from adminclubdetail where cClubId='".$club_id."'"));
	  return $club[0];
	 }
	 
	  
	 // Fetch records for player table
	 public function player_name($pPlayerId)
	 {
	 	 $player_name=mysqli_fetch_array(mysqli_query($db->db_connect_id,"select pName from adminplayer where pPlayerId='".$pPlayerId."'"));
	      return $player_name[0];
	 }//End
	  
	 public function update($table,$sql_qry,$where='')
		 {	      ////  mysql  insert Query 
		     $val_v='';
			 $key_v="";
			 if(count($sql_qry)>0){
			 foreach($sql_qry as $key=>$val){
 		      $key_v.=", ".$key."='".mysqli_real_escape_string($db-db_connect_id,htmlspecialchars_decode(htmlentities($val,ENT_QUOTES),ENT_QUOTES))."' ";
			 }
		      
			        $sql="update ".$table ." set  ".substr($key_v,1)." ". " ".$where; 
		    	      mysqli_query($db->db_connect_id,$sql) or die(mysqli_errno($db->db_connect_id));		
			 }
		}
		
	 public function insert($table,$sql_qry)
		 {	      ////  mysql  insert Query 
		     $val_v='';
			 $key_v="";
			 if(count($sql_qry)>0){
			 foreach($sql_qry as $key=>$val)
			{
 		      $key_v.=",".$key;
			  $val_v.=",'".mysqli_real_escape_string($db-db_connect_id,htmlspecialchars_decode(htmlentities($val,ENT_QUOTES),ENT_QUOTES))."'";
			 
			 }
		     
	          $sql="insert into ".$table ."  (".substr($key_v,1).")". "  values (".substr($val_v,1).")";   
			  mysqli_query($db->db_connect_id,$sql) or die(mysqli_errno($db->db_connect_id));		
			 }
	     }

            public function delete($table,$sql_qry,$where='')
		 {	      ////  mysql  insert Query 
		     $val_v='';
			 $key_v="";
			 if(count($sql_qry)>0){
			 foreach($sql_qry as $key=>$val){
 		      $key_v.=", ".$key."='".$val."' ";
			 }
		    $sql="delete from ".$table ."".$where; 
		    mysqli_query($db->db_connect_id,$sql) or die(mysqli_errno($db->db_connect_id));		
			 }
		}

	 public function fetch_all($field,$table,$where='',$limit='',$order='')
	  {	      ////  mysql  fetch  Query 
		   $num_res="select ".$field ." from ".$table ." " .$where ." ".$order;
		   $total=mysqli_query($db->db_connect_id,$num_res);
		   $total_num=mysqli_num_rows($total);
		   $this->total_record=$total_num;
		  $sql="select ".$field ." from ".$table ." " .$where ." ".$order." ".$limit;
			
		   $pro=mysqli_query($db->db_connect_id,$sql);
			
		   return $pro;
	   	 }
	 public function file_upload($file_name,$file_tmp_name,$dest_path){
	 
    ###########  Image UPLOAD ############################################
   $sizekb=filesize($file_tmp_name);
      //compare the size with the maxim size we defined and print error if bigger
      $errors=1;
      if($sizekb > MAX_SIZE*1024){  
       echo '<h1>You have exceeded the size limit!</h1>';
       $errors=0;
      }
     else{
       $new_name=$file_name;
	   $path =$dest_path.$new_name;
       copy($file_tmp_name,$path) ;   
	   $errors=1;
	  }
    return $errors;	   
	 }
########### END  Image UPLOAD ############################################
	 public function image_file_upload($file_name,$file_tmp_name,$dest_path){
	 
    ###########  Image UPLOAD ############################################
   $sizekb=filesize($file_tmp_name);
      //compare the size with the maxim size we defined and print error if bigger
      $errors=1;
      if($sizekb > MAX_SIZE*1024){  
       echo '<h1>You have exceeded the size limit!</h1>';
       $errors=0;
      }
     else{
       $new_name=$file_name;
	   $path =$dest_path.$new_name;
       copy($file_tmp_name,$path) ;   
	   $this->make_thumb($path,THUMB_PATH.$new_name,THUMB_WIDTH,THUMB_HEIGHT);
	   $this->make_thumb($path,UPLOAD_PATH."Thumb_image_small/".$new_name,THUMB_WIDTH_SMAILL,THUMB_HEIGHT_SMALL);
	   $errors=1;
	  }
    return $errors;	   
	 }
########### END  Image UPLOAD ############################################
function make_thumb($img_name,$filename,$new_w,$new_h)
{
//get image extension.
  $ext=$this->getExtension($img_name);
//creates the new image using the appropriate function from gd library
  if(!strcmp("jpg",$ext) || !strcmp("jpeg",$ext))
  $src_img=imagecreatefromjpeg($img_name);

   if(!strcmp("png",$ext))
   $src_img=imagecreatefrompng($img_name);
  
  if(!strcmp("gif",$ext))
  $src_img=imagecreatefromgif($img_name);
  
//gets the dimmensions of the image
   $old_x=imageSX($src_img);
   $old_y=imageSY($src_img);

// 1. calculate the ratio by dividing the old dimmensions with the new ones
  $ratio1=$old_x/$new_w;
  $ratio2=$old_y/$new_h;
  if($ratio1>$ratio2) {
  $thumb_w=$new_w;
  $thumb_h=$old_y/$ratio1;
  }
  else {
   $thumb_h=$new_h;
   $thumb_w=$old_x/$ratio2;
 }

// we create a new image with the new dimmensions
 $dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);

 // resize the big image to the new created one
 imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

// output the created image to the file. Now we will have the thumbnail into the file named by $filename
if(!strcmp("png",$ext))
imagepng($dst_img,$filename);
elseif(!strcmp("gif",$ext))
imagegif($dst_img,$filename);
else
imagejpeg($dst_img,$filename);

//destroys source and destination images.
imagedestroy($dst_img);
imagedestroy($src_img);
}

// This function reads the extension of the file.
// It is used to determine if the file is an image by checking the extension.
function getExtension($str) {
$i = strrpos($str,".");
if (!$i) { return ""; }
$l = strlen($str) - $i;
$ext = substr($str,$i+1,$l);
return $ext;
}
 /* Function using for Day , Months and year */
function chooseDate($timestamp = "",$cheked=''){

$pro=explode("-",$cheked);


    $timestamp = ($timestamp != "") ? $timestamp : time();
	/* Display for months */
    $months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    $out = "<select name='month'>\r\n";
	
    foreach($months as $k => $m){
	      if($cheked!='')
        $out .= "<option value='".($k+1)."' ".(($k+1)== $pro[1] ? "selected='selected'" : "").">".$m."</option>\r\n";		  
		  else
        $out .= "<option value='".($k+1)."' ".($m == date('M', $timestamp) ? "selected='selected'" : "").">".$m."</option>\r\n";
    }
      
    $out .= "</select><select name='days'>\r\n";
    for($i = 1; $i <= 31; $i++){
			if($cheked!='')
			$out .= "<option value='".$i."' ".($i== $pro[2] ? "selected='selected'" : "").">".$i."</option>\r\n";		  
		  else
        $out .= "<option value='".$i."' ".($i == date('j', $timestamp) ? "selected='selected'" : "").">".$i."</option>\r\n";
    } 
    $out .= "</select><select name='year'>\r\n <option value=''>Year</option>";
	
    for($i = date('Y'); $i >= 1970; $i--){
			if($cheked!='')
			$out .= "<option value='".$i."' ".($i== $pro[0] ? "selected='selected'" : "").">".$i."</option>\r\n";		  
	$out .= "<option value='".$i."' ".($i == date('Y', $timestamp) ? "selected='selected'":"").">".$i."</option>\r\n";
    }
    $out .= "</select><br/>";
    return $out;
} // EOF

/****** Function for current date and modifydate ******/


function choosemonthyear($timestamp = "",$cheked=''){

$pro=explode("-",$cheked);


    $timestamp = ($timestamp != "") ? $timestamp : time();
	/* Display for months */
    $months = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
    $out = "<select name='month' class='drop' >\r\n";
	
    foreach($months as $k => $m){
	      if($cheked!='')
        $out .= "<option value='".($m)."' ".(($k+1)== $pro[1] ? "selected='selected'" : "").">".$m."</option>\r\n";		  
		  else
        $out .= "<option value='".($m)."' ".($m == date('M', $timestamp) ? "selected='selected'" : "").">".$m."</option>\r\n";
    }
      
    $out .= "</select> &nbsp;&nbsp;&nbsp;";
   
    $out .= "<select name='year' class='drop'>\r\n <option value=''>Year</option>";
	
    for($i = date('Y'); $i >= 1970; $i--){
			if($cheked!='')
			$out .= "<option value='".$i."' ".($i== $pro[0] ? "selected='selected'" : "").">".$i."</option>\r\n";		  
	$out .= "<option value='".$i."' ".($i == date('Y', $timestamp) ? "selected='selected'":"").">".$i."</option>\r\n";
    }
    $out .= "</select><br/>";
    return $out;
} // EOF

function date_formate($date){
 return  @date("d M, Y",$date);
}  
/*****End funtion **********/
/************function usin*********************/
function date_formate1($date){
 return  @date("d M, Y",strtotime($date));
}

#################
################################# Records per page 
function record_per_page(){
   $href_array=$_REQUEST;
   $string_rfN='';
  foreach($href_array as $key=>$val){  
	 $string_rfN.="&".$key."=".$val;
  }


$per_page='<form name="extra" method="post" action="?perpage'.$string_rfN.'" ><select name="record_per_page" id="record_per_page" onchange="javascript:form.submit()";>';
 for($i=10;$i<=100;){
$per_page.='<option value="'.$i.'" '.($_SESSION['record_per_page']==$i?'selected="selected"':'').'>'.$i.'</option>';
	$i=$i+10;
 }
$per_page.='</select></form>';

return  $per_page;
  
}
#########################

  }
  if(!$main_obj)
  $main_obj=new main_class();

    ?>


