<?php record_mtime("footer.php");?>
<div class="clearfix"></div>
</div>
<?php if(ISSET($do_not_show_sponsors_at_bottom) && $do_not_show_sponsors_at_bottom){ ?>
<style type="text/css">
    .flex-div{overflow:hidden;position:relative;}
    .flex-slides{width:1400%;transition-duration:.6s;transform:translate3d(-689px,0px,0px);}
    .li-style{float:left;display:block;}
</style>  
<div class="flexslider">
    <div class="flex-viewport flex-div">
        <ul class="slides bottom-logo flex-slides">
            <?php $sponsorsLogo = getSponsorsLogo(); ?><?php echo $sponsorsLogo; ?>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<?php }else{ ?>
</div>
<div class="flexslider" id="bottom">
    <ul class="slides"><?php $printout = getLogoImage(); ?><?php echo $printout; ?></ul>
</div>
 <div class="footer-wrapper no-pad-tb v2">
<?php } ?>
<div class="footer-wrapper no-pad-tb v2">
        <div class="footer-top-area section-padding">
            <div class="container">
                <div class="row nav-folderized">
                    <div class="col-lg-3 col-md-12">
                        <div class="footer-logo">
                            <a href="index.html"> <img id="logo-footer" class="footer-logo footer-logos" src="<?php echo HOME_PATH; ?>/images/agedadvisor_retirement_villages_agedcare_logow.png" alt="footer-logo" width="167" height="55"></a>
                            <div class="company-desc">
                                <p>
                                   <?php
                            global $db;
                            $sql_query = "select count(id) AS number_reviews from " . _prefix('feedbacks') . " where deleted='0' ";
                            $number_reviews='xx';
                            
                            $res = mysqli_query($db->db_connect_id, $sql_query);                            
                            if($res){
                                $records = mysqli_fetch_assoc($res);
                                $number_reviews = $records['number_reviews'];                                
                            }

                            $sql_query = "select count(distinct(product_id)) AS number_facilities from " . _prefix('feedbacks') . " where deleted='0' ";
                            $number_facilities='xx';

                            $res = mysqli_query($db->db_connect_id, $sql_query);
                            if($res){
                                $records = mysqli_fetch_assoc($res);
                                $number_facilities = $records['number_facilities'];
                            }

                        ?></p>
						<p><?php echo  $number_reviews ?> reviews over <?php echo  $number_facilities ?> facilities.
                        </p>
						  <h2 class="title">Contact us</h2>
                            <ul class="list footer-list">
                                <li>
                                    <div class="contact-info">
                                        <div class="icon">
                                            <i class="ion-ios-location"></i>
                                        </div>
                                        <div class="text">13th North Ave, Florida, USA</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="contact-info">
                                        <div class="icon">
                                            <i class="ion-email"></i>
                                        </div>
                                        <div class="text"><a href="#">info@listagram.com</a></div>
                                    </div>
                                </li>
                                <li>
                                    <div class="contact-info">
                                        <div class="icon">
                                            <i class="ion-ios-telephone"></i>
                                        </div>
                                        <div class="text">+44 078 767 595</div>
                                    </div>
                                </li>
                            </ul>
                            <ul class="social-buttons style2">
							    
                           
                            <li>
                                <a href="https://www.facebook.com/agedadvisor" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                    <i class="ion-social-facebook"></i>
                                </a>
                            </li>
                             
                             
                            
                            <li>
                                <a href="https://twitter.com/aged_nz" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                                    <i class="ion-social-twitter"></i>
                                </a>
                            </li>
                            </a>
                            </li>
                        
						
                                
                            </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="footer-content nav">
                            <h2 class="title">NORTH ISLAND</h2>
                            <ul class="list">                               
								
	<li><a class="link-hov style2" <a href="<?php echo HOME_PATH . 'search/for/149/Retirement-Villages-Rest-Homes-Aged-Care-Whangarei'; ?>">Whangarei</a></li>
     <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/150/Retirement-Villages-Rest-Homes-Aged-Care-Auckland'; ?>">Auckland</a></li>
     <li><a class="link-hov style2" href="<?php echo HOME_PATH .'search/for/152/Retirement-Villages-Rest-Homes-Aged-Care-Hamilton'; ?>">Hamilton</a></li>
      <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/153/Retirement-Villages-Rest-Homes-Aged-Care-Tauranga'; ?>">Tauranga</a></li>
     <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/154/Retirement-Villages-Rest-Homes-Aged-Care-Rotorua'; ?>">Rotorua</a></li>
      <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/155/Retirement-Villages-Rest-Homes-Aged-Care-Taupo'; ?>">Taupo</a></li>
     <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/156/Retirement-Villages-Rest-Homes-Aged-Care-Gisborne'; ?>">Gisborne</a></li>
     <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/157/Retirement-Villages-Rest-Homes-Aged-Care-Napier'; ?>">Napier</a></li>
      <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/158/Retirement-Villages-Rest-Homes-Aged-Care-New-Plymouth'; ?>">New Plymouth</a></li>
      <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/159/Retirement-Villages-Rest-Homes-Aged-Care-Wanganui'; ?>">Wanganui</a> </li>
       <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/160/Retirement-Villages-Rest-Homes-Aged-Care-Feilding'; ?>">Feilding</a></li>
      <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/161/Retirement-Villages-Rest-Homes-Aged-Care-Palmerston-North'; ?>">Palmerston North</a></li>
      <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/162/Retirement-Villages-Rest-Homes-Aged-Care-Masterton'; ?>">Masterton</a></li>
       <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/163/Retirement-Villages-Rest-Homes-Aged-Care-Upper-Hutt'; ?>">Upper Hutt</a></li>
       <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/164/Retirement-Villages-Rest-Homes-Aged-Care-Lower-Hutt'; ?>">Lower Hutt</a></li>
        <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/165/Retirement-Villages-Rest-Homes-Aged-Care-Wellington'; ?>">Wellington</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="footer-content nav">
                            <h2 class="title">SOUTH ISLAND</h2>
                            <ul class="list">
                              <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/166/Retirement-Villages-Rest-Homes-Aged-Care-Nelson'; ?>">Nelson</a></li>
                        <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/167/Retirement-Villages-Rest-Homes-Aged-Care-Blenheim'; ?>">Blenheim</a></li>
                       <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/168/Retirement-Villages-Rest-Homes-Aged-Care-Christchurch'; ?>">Christchurch</a></li>
                       <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/169/Retirement-Villages-Rest-Homes-Aged-Care-Timaru'; ?>">Timaru</a></li>
                        <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/170/Retirement-Villages-Rest-Homes-Aged-Care-Oamaru'; ?>">Oamaru</a></li>
                        <li><a class="link-hov style2"  href="<?php echo HOME_PATH . 'search/for/171/Retirement-Villages-Rest-Homes-Aged-Care-Dunedin'; ?>">Dunedin</a></li>
                       <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/172/Retirement-Villages-Rest-Homes-Aged-Care-Invercargill'; ?>">Invercargill</a></li>
                            </ul>
							
							<h2 class="title">FACILITY TYPES</h2>
                            <ul class="list">
                              
                      <li><a class="link-hov style2"  href="<?php echo HOME_PATH . 'search/for/173/Retirement-Village-Apartment-1-bedroom'; ?>">Apartment 1 bedroom</a></li>
                       <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/174/Retirement-Village-Apartment-2-bedroom'; ?>">Apartment 2 bedroom</a></li>
                        <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/175/Retirement-Village-Apartment-3-bedroom'; ?>">Apartment 3 bedroom</a></li>
                        <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/176/Retirement-Village-Studio-Unit'; ?>">Studio Unit</a></li>
                        <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/177/Rest-home-care'; ?>">Rest home care</a></li>
                        <li><a class="link-hov style2"  href="<?php echo HOME_PATH . 'search/for/178/Medical-hospital-care'; ?>">Medical (hospital care)</a></li>
                        <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/179/Dementia-care'; ?>">Dementia care</a></li>
                        <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/180/Geriatric'; ?>">Geriatric</a></li>
                        <li><a class="link-hov style2" href="<?php echo HOME_PATH . 'search/for/181/Psychogeriatric'; ?>">Psychogeriatric</a><li/>
                            </ul>
						 
                        
							
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-12">
                        <div class="footer-content nav">
                            <h2 class="title">INFORMATION</h2>
                             
                            <ul class="list">
                              
                    
                       <li><a class="link-hov style2" href="<?php echo HOME_PATH; ?>">Home</a></li>
                      <li><a class="link-hov style2" href="<?php echo HOME_PATH; ?>about-us">About Us</a></li>
                        <li><a class="link-hov style2"  href="<?php echo HOME_PATH; ?>articles">Articles</a></li>
                       <li><a class="link-hov style2"  href="<?php echo HOME_PATH; ?>best-retirement-village-and-aged-care-awards">Best Facility Awards</a></li>
                       <li><a class="link-hov style2"  href="<?php echo HOME_PATH; ?>Compare-Retirement-Villages-Entry-Age-Costs-Fees-Conditions">Compare Villages</a></li>
                       <li><a class="link-hov style2"  href="<?php echo HOME_PATH; ?>supplier/ranking">Compare Providers</a><br />
                        <li><a class="link-hov style2"  href="<?php echo HOME_PATH; ?>reviews">Recent Reviews</a></li>
                        <li><a class="link-hov style2"  href="<?php echo HOME_PATH; ?>formal-complaints-hdc-dhb-moh">Voice a Complaint</a></li>
                        <li><a class="link-hov style2"  href="<?php echo HOME_PATH; ?>sponsors-advertising">Sponsors &amp; Advertising</a></li>
                       <li><a class="link-hov style2"  href="<?php echo HOME_PATH; ?>contact-us">Contact Us</a></li>
                        <li><a class="link-hov style2"  href="<?php echo HOME_PATH ?>terms-and-conditions">Terms of Service &amp;<br>Privacy Policy</a><</li>
                        <li><a class="link-hov style2"  href="<?php echo HOME_PATH; ?>faq">FAQs</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="payment-method">
                            <?php echo  date('Y') ?> &copy; All Rights Reserved.
                        </div>
                    </div>
                    <div class="col-md-6 text-right sm-left">
                        <ul class="additional-link">
                            <li> <a href="<?php echo HOME_PATH ?>terms-and-conditions">Terms of Service &amp; Privacy Policy</a></li> 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
</div>
<script type="text/javascript" src="<?php echo HOME_PATH;?>includes/js/plugin.js"></script> 
<script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/plugins/flexslider/jquery.flexslider.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/plugins/parallax-slider/js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/plugins/parallax-slider/js/jquery.cslider.js"></script>
<!--FOR ON pAGE SEO mergerd the codes of this file in app.js <script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/js/custom.js"></script>--> 

<!-- NEW CSSs--->
   
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD_8C7p0Ws2gUu7wo0b6pK9Qu7LuzX2iWY&amp;libraries=places&amp;callback=initAutocomplete"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/js/markerclusterer.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/js/maps.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>includes/js/infobox.min.js"></script>


<script type="text/javascript" src="<?php echo HOME_PATH;?>includes/js/main.js"></script>
<!-- END NEW CSSs--->

<script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/js/app.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/js/plugins/parallax-slider.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/js/plugins/fancy-box.js"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.15"></script> -->

<!--<script src="https://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerwithlabel/src/markerwithlabel.js"></script>-->
<!--[if IE 6]><script type="text/javascript" src="js/jq-png-min.js"></script><![endif]-->
<!--[if IE]><script type="text/javascript" src="js/ieh5fix.js"></script><![endif]-->
<!--<script type="text/javascript" src="<?php echo HOME_PATH; ?>js/jquery-ui.min.js"></script>-->
<script type="text/javascript" src="<?php echo HOME_PATH; ?>js/modernizr.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>js/jquery-ui-timepicker-addon.js"></script>

<script type="text/javascript" src="<?php echo HOME_PATH; ?>js/commonAjax.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>js/star-rating.min.js"></script>
 

<?php header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 3600)); ?>
<script src="<?php echo HOME_PATH; ?>js/responsiveslides.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        App.init();
        App.initSliders();
        ParallaxSlider.initParallaxSlider();
        FancyBox.initFancybox();
        $(".flexslider").flexslider({animation: "slide", animationLoop: true, controlNav: false, directionNav: false, itemWidth: 160, itemMargin: 10, minItems: 2, maxItems: 8})
    });
</script>
<!--[if lt IE 9]>
<script src="assets/plugins/respond.js"></script>
<script src="assets/plugins/html5shiv.js"></script>
<script src="assets/js/plugins/placeholder-IE-fixes.js"></script>
<![endif]-->

<style type="text/css">.footer-logos{height:55px;width:auto}</style>
<?php
    if( ($_SERVER['REMOTE_ADDR']=="103.211.15.112" ||  $_SERVER['REMOTE_ADDR']=="125.236.218.32") && ISSET($GLOBALS['process_time_start'])){// Its Malcolm
        record_mtime("footer.php");
        record_mtime('TOTAL PAGE TIME');

        echo "<hr><h4>Only viewable by IPaddress on /classes/mysql.php - Showing Process Times</h4>";
        //print_r($GLOBALS['process_time_start']);
        $i=0;
        foreach($GLOBALS['process_time_start'] as $name => $time) {
            $start=0;$end=0;$i=$i+1;
            if(ISSET($time['start'])){$start=$time['start'];}
            if(ISSET($time['end'])){$end=$time['end'];}
            $amount=$end - $start;
            if($amount>0.01){
                echo "&nbsp;<b>".number_format($end - $start,4) . "s</b>, $name<br>";
            }
        }// end for each
        echo "<h1>total queries $i</h1>";
    }
?>
</body>
</html>