<?php    
    $nav_url = $_SERVER['REQUEST_URI'];
?>


<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div class="sidebar" id="sidebar">
        <!-- begin sidebar scrollbar -->
        <div class="slimScrollDiv">
            <div data-height="100%" data-scrollbar="true">
                <!-- begin sidebar nav -->
                <ul class="nav">
                    <li class="has-sub sub_nav <?php echo (strpos($nav_url, 'productList') > 0 || (strpos($nav_url, 'productList'))) ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH; ?>supplier/channel?set=proOut"><i class="fa fa-list-alt"></i> <span>Return to Facility List</span></a></li>                    
                    <li class="has-sub sub_nav <?php echo (strpos($nav_url, 'accountInfo') > 0 || (strpos($nav_url, 'editAccountInfo'))) ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH; ?>supplier/accountInfo"><i class="fa fa-folder-open-o "></i> <span>Individual Facility Info</span></a></li>
                    <li class="has-sub sub_nav <?php echo strpos($nav_url, 'order') > 0 || strpos($nav_url, 'product-views') > 0 ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH . 'supplier/order-manager' ?>"><i class="fa fa-list-alt"></i> <span>View Payment History</span></a></li>
                    <!--<li class="has-sub sub_nav <?php echo (strpos($nav_url, 'add-manager') > 0 || (strpos($nav_url, 'editAddsBooking')) || (strpos($nav_url, 'editArtAddsBooking')) || (strpos($nav_url, 'addBookingPaymentProcess'))) ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH . 'supplier/add-manager' ?>"><i class="fa fa-file-text-o"></i> <span>Ad Booking Manager</span></a></li>-->
                    <li class="has-sub sub_nav <?php echo (strpos($nav_url, 'gallery-manager') > 0 || (strpos($nav_url, 'addGallery'))) ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH; ?>supplier/gallery-manager"><i class="fa fa-picture-o"></i> <span>Upload/Edit Images</span></a></li>
                    <li class="has-sub sub_nav <?php echo (strpos($nav_url, 'feedback-center') > 0 || (strpos($nav_url, 'feedback-view'))) ? 'active' : '';?>"><a href="<?php echo HOME_PATH; ?>supplier/feedback-center"><i class="fa fa-twitch"></i> <span>Read Reviews/Comments</span></a></li>
                    <li class="has-sub sub_nav <?php echo (strpos($nav_url, 'enquiry-center') > 0 || (strpos($nav_url, 'enquiry-view'))) ? 'active' : '';?>"><a href="<?php echo HOME_PATH; ?>supplier/enquiry-center"><i class="fa fa-pencil-square-o"></i> <span>View Enquiries</span></a></li>
                    <!--<li class="has-sub sub_nav <?php echo (strpos($nav_url, 'artwork-manager') > 0 || (strpos($nav_url, 'artwork'))) ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH . 'supplier/artwork-manager' ?>"><i class="fa fa-briefcase"></i> <span>Artwork Manager</span></a></li>-->
                    <!--<li class="has-sub sub_nav <?php echo (strpos($nav_url, 'product-manager') > 0 || (strpos($nav_url, 'addProduct') || (strpos($nav_url, 'extraInfo')) || (strpos($nav_url, 'xlsProductUpload')))) ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH . 'supplier/product-manager' ?>"><i class="fa fa-pencil-square-o"></i> <span>Service/Product Manager</span></a></li>-->
                    <li class="has-sub sub_nav <?php echo strpos($nav_url, 'issue_mail') > 0 ? 'active' : ''; ?>"><a href="<?php echo HOME_PATH . 'supplier/issue_mail' ?>"><i class="fa fa-envelope-o"></i> <span>Email Aged Advisor</span></a></li>
                </ul>
            </div>
        </div>
        <!-- end sidebar scrollbar -->
    </div>
</div>