<?php
require_once 'constant.php';
$uid = $_SESSION['id'];
$proId = $_SESSION['proId'];
$proName = $_SESSION['proName'];
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$pageIdRequested = isset($_REQUEST['pageId']) ? $_REQUEST['pageId'] : '';
$search_input = isset($_REQUEST['search_input']) ? $_REQUEST['search_input'] : '';
$search_type = isset($_REQUEST['search_type']) ? $_REQUEST['search_type'] : '';
global $db;
switch ($action) {
    case 'artwork':
        manage_artWork();
        break;
    case 'product':
        manage_product($uid, $search_input, $search_type);
        break;
    case 'productviews':
        manage_productviews($uid);
        break;
    case 'order':
        manage_order($uid);
        break;
    case 'partwork':
        manage_personalArtWork($uid);
        break;
    case 'gallery':
        manage_gallery($uid);
        break;
    case 'articles':
        articles_list();
        break;
    case 'globalsearch':
        global_search($searchvalue);
        break;
    case 'reviews':
        review_list();
        break;
    case 'viewReviews':
        viewReview($id); // Main Facility details Pages
        break;
    case 'viewFeedback':
        viewFeedback($id);
    case 'viewFeedbackdDetail':
        viewFeedbackdDetail($id);
        break;
    case 'feedback':
        feedback_center($uid, $proId);
        break;
    case 'enquiry':
        enquiry_center($uid, $proId);
        break;
    case 'forum':
        manage_forum();
    case 'blog':
        manage_blog();
        break;
    case 'adsBookingList':
        manage_adsBookingList($uid, $proId, $proName);
        break;
    default :
        test();
        break;
}

function test() {
    
}

function get_first_image_from_gallery($pid) {
    $image_file = '';
    global $db;

    $sql_query = "select file from " . _prefix('galleries') . " where pro_id = '$pid' AND type = 0 AND status = 1 AND deleted = 0 ORDER BY Favourite_image DESC LIMIT 0,1 ";
    $res = mysqli_query($db->db_connect_id, $sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $image_file = $records['file'];
    }
    return $image_file;
}

function manage_forum($search_input, $search_type) {
    global $db;
    $condition = " where bg.deleted = 0 AND bg.type= 1 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' && bg.title like "%' . $search_input . '%" ';
                break;
            case "author":
                $condition .= ' && user.first_name like "%' . $search_input . '%" ';
                $condition .= ' && user.last_name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && ( bg.title like "%' . $search_input . '%" OR user.name like "%' . $search_input . '%" )';
        }
    }

    $query = "SELECT bg.*, user.user_name AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . " $condition GROUP BY bg.id ORDER BY bg.id DESC ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT bg.*, user.user_name AS author , count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . "$condition  GROUP BY bg.id ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);

    if ($count > 0) {

        foreach ($data as $key => $blog) {
            $description = strip_tags($blog['title']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $output .= '<div class="group_one">
                    <div class="text-right"><a href="' . HOME_PATH . 'forum/view?id=' . md5($blog['id']) . '">Post Comment</a></div>
                <div class="line_bottom">
                    <div class="width_90">Subject :</div>
                    <div class="margin_left_125">' . $description . '</div>
                </div>

                <div class="line_bottom">
                    <div class="width_90">Posted By :</div>
                    <div class="margin_left_125">' . $blog['author'] . '</div>
                </div>
                <div class="line_bottom">
                    <div class="width_90">Last Post :</div>
                    <div class="margin_left_125">' . date('M d, Y', strtotime($blog['modified'])) . '</div>
                </div>
                <div class="line_bottom">
                    <div class="width_90">Comments :</div>
                    <div class="margin_left_125">' . $blog['comment'] . '</div>
                </div>
            </div>';
        }
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $pageRequested = isset($_POST['pageId']) && !empty($_POST['pageId']) ? $_POST['pageId'] : $pageIdRequested;
            $pageRequested = 1;
            $output .= get_sp_pagination_link($paginationCount, 'forum', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;

    die;
}

/* * *************************************manage blog code dt 6-2-16 ********************************** */

function manage_blog($search_input, $search_type) {
    global $db;
    $condition = " where bg.deleted = 0 AND bg.type= 0 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' && bg.title like "%' . $search_input . '%" ';
                break;
            case "author":
                $condition .= ' && user.first_name like "%' . $search_input . '%" ';
                $condition .= ' && user.last_name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && ( bg.title like "%' . $search_input . '%" OR user.name like "%' . $search_input . '%" )';
        }
    }

    $query = "SELECT bg.*, user.user_name AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . "$condition  GROUP BY bg.id ORDER BY bg.id DESC ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT bg.*, user.user_name AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . "$condition  GROUP BY bg.id ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        foreach ($data as $key => $blog) {
            $description = strip_tags($blog['title']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $content = $blog['content'];
            if ($blog['img_name'] != '') {
                $blog_img_name = $blog['img_name'];
            } else {
                $blog_img_name = 'default.png';
            }
            /* $output .= '<div class="group_one">
              <div class="text-right"><a href="' . HOME_PATH . 'blog/view?id=' . md5($blog['id']) . '">Post Comment</a></div>
              <div class="line_bottom">
              <div class="width_90">Subject :</div>
              <div class="margin_left_125">' . $description . '</div>
              </div>

              <div class="line_bottom">
              <div class="width_90">Posted By :</div>
              <div class="margin_left_125">' . $blog['author'] . '</div>
              </div>
              <div class="line_bottom">
              <div class="width_90">Last Post :</div>
              <div class="margin_left_125">' . date('M d, Y', strtotime($blog['modified'])) . '</div>
              </div>
              <div class="line_bottom">
              <div class="width_90">Comments :</div>
              <div class="margin_left_125">' . $blog['comment'] . '</div>
              </div>
              </div>'; */

            /*             * ************************************laout for blog section**************************************** */
            $output .= '<div class="association-section blog-content">
         
         <div class="associate-content-box">
         
                 <div class="col-xs-3"><div class="association-img"><img title="Community Lorem Ipsum" src="admin/files/pages/image/' . $blog_img_name . '" alt="logo Image"></div></div>  
                 <div class="col-xs-9">
                 <div class="association-text">
                   <div class="headline">
                    <h2>' . $description . '</h2>
                     </div>
                      ' . $content . '
                
                
            
                    <p>Posted By : &nbsp;' . $blog['author'] . '</p>
                
               
                    <p>Last Post : &nbsp;' . date('M d, Y', strtotime($blog['created'])) . '</p>
            
            
                    <p>Comments : &nbsp;' . $blog['comment'] . '</p>
                 <p><a href="' . HOME_PATH . 'blog/view/' . md5($blog['id']) . '">Read More</a></p>
          
                
                
                <div class="clearfix"></div>
                
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            </div>
            
            </div>';




            /*             * *********************************end of layout of blog sectiion********************************* */
        }
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'blog', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;

    die;
}

/* * ***********************************end of manage blog code ************************************ */


/*
  function manage_blog($search_input, $search_type) {
  global $db;
  $condition = " where bg.deleted = 0 AND bg.type= 0 ";
  if (isset($search_type) && $search_input != '') {
  switch ($search_type) {
  case "title":
  $condition .= ' && bg.title like "%' . $search_input . '%" ';
  break;
  case "author":
  $condition .= ' && user.first_name like "%' . $search_input . '%" ';
  $condition .= ' && user.last_name like "%' . $search_input . '%" ';
  break;
  default :
  $condition .= ' && ( bg.title like "%' . $search_input . '%" OR user.name like "%' . $search_input . '%" )';
  }
  }

  $query = "SELECT bg.*, user.user_name AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
  . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
  . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
  . "$condition  GROUP BY bg.id ORDER BY bg.id DESC ";
  $res = $db->sql_query($query);
  $count = $db->sql_numrows($res);
  $i = 1;
  if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
  $id = $_POST['pageId'];
  $i = $i + PAGE_PER_NO * $id;
  } else {
  $id = '0';
  $i = 1;
  }
  $pageLimit = PAGE_PER_NO * $id;
  $query = "SELECT bg.*, user.user_name AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
  . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
  . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
  . "$condition  GROUP BY bg.id ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

  $res = $db->sql_query($query);
  $data = $db->sql_fetchrowset($res);
  if ($count > 0) {
  foreach ($data as $key => $blog) {
  $description = strip_tags($blog['title']);
  $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
  $output .= '<div class="group_one">
  <div class="text-right"><a href="' . HOME_PATH . 'blog/view?id=' . md5($blog['id']) . '">Post Comment</a></div>
  <div class="line_bottom">
  <div class="width_90">Subject :</div>
  <div class="margin_left_125">' . $description . '</div>
  </div>

  <div class="line_bottom">
  <div class="width_90">Posted By :</div>
  <div class="margin_left_125">' . $blog['author'] . '</div>
  </div>
  <div class="line_bottom">
  <div class="width_90">Last Post :</div>
  <div class="margin_left_125">' . date('M d, Y', strtotime($blog['modified'])) . '</div>
  </div>
  <div class="line_bottom">
  <div class="width_90">Comments :</div>
  <div class="margin_left_125">' . $blog['comment'] . '</div>
  </div>
  </div>';
  }
  $pagingCount = $count;
  if ($pagingCount > 0) {
  $data = '';
  $paginationCount = getPagination($pagingCount);
  $colspan = 6;
  $output .= get_sp_pagination_link($paginationCount, 'blog', $data, $colspan, $_POST['pageId']);
  }
  } else {
  $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
  }
  echo $output;

  die;
  }

 */

function manage_artWork() {
    global $db;
    $condition = " where deleted = 0 ";
    $query = "SELECT id  FROM " . _prefix("artwork_users") . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT aUsers.* , CONCAT(IFNULL(users.first_name,''),'',IFNULL(users.last_name,'')) AS user_name  FROM " . _prefix("artwork_users") . " AS aUsers   "
            . "LEFT JOIN " . _prefix("users") . " AS users ON  aUsers.user_id= users.id "
            . "where aUsers.deleted=0 ORDER BY aUsers.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<table class="table table-striped table-hover  table-bordered">
                    <thead>
                    <tr><th width="5%" align="left">S.No</th>
                    <th width="20%" align="left">Artwork Description</th>
                    <th width="15%" align="left">Client Expected Date</th>
                    <th width="15%" align="left">Client Given Artwork</th>
                    <th width="15%" align="left">Admin Expected Date</th>
                    <th width="15%" align="left">Admin Given Artwork</th>
                    <th width="20%" align="left">Admin Description</th>
                    <th width="10%" align="left">Action</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $lastChanges = ($record['modified'] == '0000-00-00 00:00:00') ? date('M d, Y', strtotime($record['created'])) : date('M d, Y', strtotime($record['modified']));
            $output .= '<tr class="row_' . $record['id'] . '" >';
            $output .= '<td>' . $i . ' .</td>';
            $output .= '<td>' . $record['user_name'] . '</td>';
            $output .= '<td><a href="' . HOME_PATH_URL . 'artwork.php?partwork&id=' . md5($record['id']) . '"><i class="fa fa-envelope"></i>&nbsp;View</a></td>';
            $output .= '<td>' . $record['total_requested'] . '</td>';
            $output .= '<td>' . $record['total_completed'] . '</td>';
            $output .= '<td>' . (($record['total_requested'] - $record['total_requested']) >= 0 ? ($record['total_requested'] - $record['total_requested']) : 0) . '</td>';
            $output .= '<td>' . $lastChanges . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="artwork_users-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-artwork_users-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output .= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $output .= '</tbody></table>';
        $paging_query = "SELECT id, user_id, artw_user_id, client_given, admin_given, client_description, admin_description, expected_date, admin_expected_date, status, created  FROM " . _prefix("artwork_users") . "   " . "$condition ";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'client', $data, $colspan);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    die;
}

function manage_personalArtWork($uid) {
    global $db;
    $condition = " where aUsers.deleted = 0 AND aUsers.user_id='" . $uid . "' AND aUsers.pro_id='" . $_SESSION['proId'] . "' ";
    $query = "SELECT aUsers.* , CONCAT(IFNULL(users.first_name,''),'',IFNULL(users.last_name,'')) AS user_name  FROM " . _prefix("artworks") . " AS aUsers   "
            . "LEFT JOIN " . _prefix("users") . " AS users ON  aUsers.user_id= users.id "
            . "$condition ORDER BY aUsers.id DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT aUsers.* , CONCAT(IFNULL(users.first_name,''),'',IFNULL(users.last_name,'')) AS user_name  FROM " . _prefix("artworks") . " AS aUsers   "
            . "LEFT JOIN " . _prefix("users") . " AS users ON  aUsers.user_id= users.id "
            . "$condition ORDER BY aUsers.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord">
   <thead>
      <tr>
         <th class="col-md-1">S.No</th>
                    <th class="col-md-2">Title</th>
                    <th class="col-md-2">Expected Date</th>
                    <th class="col-md-2">Client Artwork</th>
                    <th class="col-md-1">Admin Return</th>
                    <th class="col-md-1">Artwork Status</th>
                    <th class="col-md-1">Download</th>
                    <th class="col-md-2">Action</th>
         </tr>
   </thead>
   <tbody>';

        foreach ($data as $key => $record) {
            $verified_status = ($record['approve_status'] == 1) ? 'Recieved' : 'Requested';
            $verified = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img1 = $verified_status . ' <img src="' . $verified . '" title="' . $verified_status . '" />';
            $client_description = (strlen($record['client_description']) > 40) ? substr($record['client_description'], 0, 40) . '...' : $record['client_description'];
            $output .= '<tr class="row_' . $record['id'] . '" >';
            $output .= '<td class= "text-center">' . $i . ' .</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td class= "text-center">' . date('M d, Y', strtotime($record['expected_date'])) . '</td>';
            $fileImage = ADMIN_PATH . 'files/artwork/thumb_' . $record['client_given'];
            $fileImageView = HOME_PATH . 'admin/files/artwork/' . $record['client_given'];
            if (file_exists($fileImage)) {
                $output .= !empty($record['client_given']) ? '<td class= "text-center"><a  class="fancybox" title="' . $record['client_given'] . '" href="' . $fileImageView . '"><img  title="View Image"  src="' . MAIN_PATH . '/files/artwork/thumb_' . $record['client_given'] . '"></a><br/><br/></td>' : '';
            } else {
                $output .= '<td class= "text-center">N/A</td>';
            }
            $fileImageadmin = ADMIN_PATH . 'files/artwork/thumb_' . $record['admin_returned'];
            $fileImageViewadmin = HOME_PATH . 'admin/files/artwork/' . $record['admin_returned'];
            if (file_exists($fileImageadmin)) {
                $output .= !empty($record['admin_returned']) ? '<td class= "text-center"><a  class="fancybox" title="' . $record['client_given'] . '" href="' . $fileImageViewadmin . '"><img  title="View Image"  src="' . MAIN_PATH . '/files/artwork/thumb_' . $record['admin_returned'] . '"></a><br/><br/></td>' : '';
            } else {
                $output .= '<td class= "text-center">Pending</td>';
            }
            $output .= '<td class= "text-center">' . $verified_status . '</td>';
            if (file_exists($fileImageadmin)) {
                $output .= !empty($record['admin_returned']) ? '<td class= "text-center"><a href="' . MAIN_PATH . 'download.php?type=artwork&file=' . $record['admin_returned'] . '"><button style="padding:6px 8px; font-size:13px; margin-bottom:0px; " class="btn btn-danger center-block btn_search btn_pay" >DOWNLOAD</button></a></td>' : 'N/A';
            } else {
                $output .= '<td class= "text-center">N/A</td>';
            }
// $output .= '<td class= "text-center"><a href="' . MAIN_PATH . 'download.php?type=artwork&file=' . $record['client_given'] . '"><button style="padding:6px 8px; font-size:13px; margin-bottom:0px; " class="btn btn-danger center-block btn_search btn_pay" >DOWNLOAD</button></a></td>';
            $output .= '<td><span style="margin-right:5px;"><a href="' . HOME_PATH . 'supplier/artwork?edit&id=' . md5($record['id']) . '"><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-artworks-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a></span>'
                    . '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT id, user_id, artw_user_id, client_given, admin_returned , client_description, admin_description, expected_date, admin_expected_date, status, created  FROM " . _prefix("artworks") . "   " . "Where deleted=0 AND md5(artw_user_id)='" . $rid . "'";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 8;
            $output .= get_sp_pagination_link($paginationCount, 'partwork', $data, $colspan, $_POST['pageId']);
        }
        $output .= '</tbody></table>';
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script>

        $(".fancybox").fancybox();
        $(".fancybox1").fancybox();
        $(document).ready(function () {

            var path = '<?php echo HOME_PATH; ?>ajaxFront.php';
            /*
             * Code for Delete
             */

            $('.delete').live('click', function () {
                var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
                    var details = $(this).attr('id').split('-');
                    var table = details[1];
                    var id = details[2];
                    $.ajax({
                        type: 'POST',
                        url: path,
                        data: {action: 'delete', id: id, table: table},
                        success: function () {
                            $('.row_' + id).hide();
                        }
                    });
                }
            });

        });
    </script>

    <?php
    die;
}

function feedback_center($uid, $proId) {
    global $db;
    $getAllRating = overAllRating($uid);
    $condition = " where fdbk.sp_id='" . $uid . "' AND pds.id='" . $_SESSION['proId'] . "' AND fdbk.deleted=0";
    // $condition = " where fdbk.sp_id='" . $uid . "' AND pds.id='" . $_SESSION['proId'] . "' AND fdbk.deleted=0 AND fdbk.status=1 ";
    $query = "SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "$condition ORDER BY fdbk.created DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "$condition ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    //prd($data);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord">
   <thead>
      <tr>
         <!----<th class="col-md-1 text-center">S.No</th>---->
                    <!----<th class="col-md-3">Service/Product</th>-->
                    <th class="col-md-5">Comment to management</th>
                    <th class="col-md-2">Mark as read</th>
                    <th class="col-md-5">Rating</th>
         </tr>
   </thead>
   <tbody>';
        $TotalRating = 0;
        foreach ($data as $key => $record) {
            $i = 1;
            $verified_status = ($record['approve_status'] == 1) ? 'Completed' : 'Pending';
            $verified = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img1 = $verified_status . ' <img src="' . $verified . '" title="' . $verified_status . '" />';
            $client_description = (strlen($record['client_description']) > 40) ? substr($record['client_description'], 0, 40) . '...' : $record['client_description'];
            $feedback_reply = (strlen($record['feedback_reply']) > 40) ? substr($record['feedback_reply'], 0, 40) . '...' : $record['feedback_reply'];
            $reviewStatus = $record['status'];
            $TotalRating = $TotalRating + (($QualityOfServiceWgt * $record['quality_of_care']) + ($cStaffWgt * $record['caring_staff']) + ($resManWgt * $record['responsive_management']) + ($tripWgt * $record['trips_outdoor_activities']) + ($indoorEntWgt * $record['indoor_entertainment']) + ($socialWgt * $record['social_atmosphere']) + ($foodWgt * $record['enjoyable_food']) + ($overAllRatingWgt * $record['overall_rating']));
            switch ($record['quality_of_care']) {
                case 1:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $service_div = 'Not Rated';
                    break;
            }
            switch ($record['caring_staff']) {
                case 1:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $value_div = 'Not Rated';
                    break;
            }
            switch ($record['responsive_management']) {
                case 1:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $activity_div = 'Not Rated';
                    break;
            }
            switch ($record['trips_outdoor_activities']) {
                case 1:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $management_div = 'Not Rated';
                    break;
            }
            switch ($record['indoor_entertainment']) {
                case 1:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $indoorEnt_div = 'Not Rated';
                    break;
            }
            switch ($record['social_atmosphere']) {
                case 1:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $social_div = 'Not Rated';
                    break;
            }
            switch ($record['enjoyable_food']) {
                case 1:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $food_div = 'Not Rated';
                    break;
            }
            switch ($record['overall_rating']) {
                case 1:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $overall_div = 'Not Rated';
                    break;
            }

            $output .= '<tr class="row_' . $record['id'] . '" >';
            $output .= '<td>';

            if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                $output .= '
            <p class="text-center abuse_div abuse-' . $record['id'] . '"><a style="color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a></p>
                            ';
            } else if ($record['abused'] == 1) {
                $output .= '<p style="color:#f15922;font-style: italic;" class="text-center">Moderated by Admin</p>';
            } else {
                $output .= '<p style="color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
            }

            $output .= $record['3'] . '<br/>' . $record['feedback'] . '<br><a href = "' . HOME_PATH . 'supplier/feedback-view?id=' . md5($record['id']) . '">View full comments</a><br>';

            if ($feedback_reply) {
                $output .= '<strong><i>Your reply:</i></strong> ' . $feedback_reply . '<br><i>';
                if (!$record['Admin_FINAL_REPLY']) {
                    $output .= '<a href = "' . HOME_PATH . 'supplier/feedback-view?id=' . md5($record['id']) . '#reply_here">Edit reply</a>';
                }
                $output .= '</i>';
            } else {
                $output .= '            <i><a href = "' . HOME_PATH . 'supplier/feedback-view?id=' . md5($record['id']) . '#reply_here">Reply to review</a></i>';
            }
            if ($reviewStatus == 0) {
                $output .= '<p style="font-size:13px;">Note: This review is not yet online as it is currently awaiting Aged Advisor admin approval.</p>';
            }
            $output .= '</td>';
            if ($record['read_status'] == 0) {
                $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;" ><i class ="read_status fa fa-file-text fa-2x" title="Un Read" id="feedbacks-' . $record['id'] . '-' . $record['read_status'] . '" style="color:red;cursor:pointer;"></i></span></td>';
            } else {
                $output .= '<td><i class = "fa fa-check-square fa-2x" title="Read"></i></td>';
            }
            $output .= '<td>Quality of Care : ' . $service_div . ' </br>'
                    . 'Caring/Helpful Staff : ' . $value_div . '</br>'
                    . 'Responsive Management : ' . $activity_div . '</br>'
                    . 'Trips/Outdoor Activities : ' . $management_div . '</br>'
                    . 'Indoor Entertainment : ' . $indoorEnt_div . '</br>'
                    . 'Social Atmosphere : ' . $social_div . '</br>'
                    . 'Enjoyable Food : ' . $food_div . '</br>'
                    . 'Overall Rating : ' . $overall_div . '</td>';
            $output .= '</td>';
            $output .= '</tr>';

            $i ++;
        }

        $paging_query = "SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
                . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
                . "$condition ORDER BY fdbk.created DESC";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 8;
            $output .= get_sp_pagination_link($paginationCount, 'feedback', $data, $colspan, $_POST['pageId']);
        }
        $output .= '</tbody></table>';
        $output .= '<div class = "col-sm-offset-2 col-sm-8 margin">
            <span>Overall Rating: ' . OverAllNEWRatingProduct($proId) . '</span>
            </div>';
    } else {
        $output = '<table class = "table table-bordered table_bord"><tbody><tr><td class = "text-center text-danger">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script>

        $(".fancybox").fancybox();
        $(".fancybox1").fancybox();
        $(document).ready(function () {

            var path = '<?php echo HOME_PATH; ?>ajaxFront.php';

            $('.read_status').live('click', function () {
                var details = $(this).attr('id').split('-');
                var table = details[0];
                var id = details[1];
                var read_status = details[2];
                var image = "<?php echo ADMIN_IMAGE . 'approval.gif' ?>";
                $('.status-' + id).html('<img src=' + image + ' >');

                //alert(table);
                //return false;
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'readStatus', id: id, table: table, read_status: read_status},
                    success: function (response) {
                        $('.status-' + id).html($.trim(response));
                    }
                });
            });
            /*
             * Code for Delete
             */

            $('.delete').live('click', function () {
                var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
                    var details = $(this).attr('id').split('-');
                    var table = details[1];
                    var id = details[2];
                    $.ajax({
                        type: 'POST',
                        url: path,
                        data: {action: 'delete', id: id, table: table},
                        success: function () {
                            $('.row_' + id).hide();
                        }
                    });
                }
            });

        });
    </script>

    <?php
    die;
}

function enquiry_center($uid, $proId) {
    global $db;

    $sup_id = $_SESSION['id'];
    $prod_id = md5($_SESSION['proId']);

    $roomsQuery = "SELECT no_of_room, no_of_beds, certification_service_type, title from ad_extra_facility AS exfac"
            . " LEFT JOIN ad_products AS adpro ON adpro.id = exfac.product_id "
            . "where md5(product_id) = '$prod_id' AND exfac.status=1 AND adpro.status=1";
    $roomsQueryRes = mysqli_query($db->db_connect_id, $roomsQuery);

    while ($roomsQueryData = mysqli_fetch_array($roomsQueryRes)) {
        $aprtmt = $roomsQueryData['no_of_rooms'];
        $beds = $roomsQueryData['no_of_beds'];
        $fac_type = $roomsQueryData['certification_service_type'];
        $fac_title = $roomsQueryData['title'];
    }
    $fmt = new NumberFormatter('en_US', NumberFormatter::CURRENCY);
    $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 2);

    function nigel_format($value) {
        if (floor($value) == $value) {
            return $value;
        } else {
            return number_format($value, 2);
        }
    }

    if ($fac_type == "Aged Care") {
        if ($beds >= 100) {
            $amt = 16.67;
            $gst = nigel_format(14.50);
            $paylatergst = $fmt->formatCurrency($gst * 10, USD);
            $amtforupgrade = $fmt->formatCurrency($gst * 2, USD);
            $paylateramt = $fmt->formatCurrency(10 * $amt, USD);
        }
        if ($beds >= 40 && $beds <= 99) {
            $amt = 10.92;
            $gst = nigel_format(9.50);
            $paylatergst = $fmt->formatCurrency($gst * 10, USD);
            $amtforupgrade = $fmt->formatCurrency($gst * 2, USD);
            $paylateramt = $fmt->formatCurrency(10 * $amt, USD);
        }
        if ($beds < 40) {
            $amt = 5.72;
            $gst = nigel_format(4.97);
            $paylatergst = $fmt->formatCurrency($gst * 10, USD);
            $amtforupgrade = $fmt->formatCurrency($gst * 2, USD);
            $paylateramt = $fmt->formatCurrency(10 * $amt, USD);
        }
    }

    if ($fac_type == "Retirement Village" || $fac_type == "Home Care") {
        if ($aprtmt >= 30 && $aprtmt <= 79) {
            $amt = 28.17;
            $gst = nigel_format(24.50);
            $paylatergst = $fmt->formatCurrency($gst * 10, USD);
            $amtforupgrade = $fmt->formatCurrency($gst * 2, USD);
            $paylateramt = $fmt->formatCurrency(10 * $amt, USD);
        }
        if ($aprtmt >= 80) {
            $amt = 33.92;
            $gst = nigel_format(29.50);
            $paylatergst = $fmt->formatCurrency($gst * 10, USD);
            $amtforupgrade = $fmt->formatCurrency($gst * 2, USD);
            $paylateramt = $fmt->formatCurrency(10 * $amt, USD);
        }
        if ($aprtmt < 30) {
            $amt = 22.42;
            $gst = nigel_format(19.50);
            $paylatergst = $fmt->formatCurrency($gst * 10, USD);
            $amtforupgrade = $fmt->formatCurrency($gst * 2, USD);
            $paylateramt = $fmt->formatCurrency(10 * $amt, USD);
        }
    }

    $upgradeQuery = "SELECT plan_id FROM " . _prefix("pro_plans") . " WHERE md5(pro_id) = '$prod_id' AND supplier_id = " . $sup_id . " AND current_plan = 1";
    $resUpgradeQuery = $db->sql_query($upgradeQuery);

    while ($dataUpgradeQuery = mysqli_fetch_array($resUpgradeQuery)) {
        $plan_id = $dataUpgradeQuery['plan_id'];
    }

    $supplierQuery = "SELECT first_name, last_name, phone,email FROM " . _prefix("users") . " WHERE id = " . $sup_id;
    $resSupplierQuery = $db->sql_query($supplierQuery);

    while ($dataSupplierQuery = mysqli_fetch_array($resSupplierQuery)) {
        $first_name = $dataSupplierQuery['first_name'];
        $last_name = $dataSupplierQuery['last_name'];
        $supphone = $dataSupplierQuery['phone'];
        $supemail = $dataSupplierQuery['email'];
        $supfullname = $first_name . ' ' . $last_name;
    }

    $condition = " where md5(enq.prod_id) = '" . $prod_id . "' AND user.deleted=0";
    $queryEnq = "SELECT enq.id,enq.time_of_response,  enq.clicked_display, enq.enq_specifics, enq.facility_responded,enq.time_of_enquiry, enq.user_id, "
            . "enq.enquiry_paid_status, user.first_name,user.last_name,user.email,user.phone,enq.prod_id,"
            . "enq.Supplier_id  FROM " . _prefix("enquiries") . " AS enq   "
            . "LEFT JOIN " . _prefix("users") . " AS user ON  enq.user_id= user.id "
            . "$condition ORDER BY enq.time_of_enquiry DESC";

    $resQuery = $db->sql_query($queryEnq);
    $count = $db->sql_numrows($resQuery);
//    $ip = $_SERVER['REMOTE_ADDR'];
//    if ($ip == '103.87.57.146'){ echo $count;}
    $resData = $db->sql_fetchrowset($resQuery);

    if ($count > 0) {

        $mainDiv = "<div class='col-sm-12'>";
        echo $mainDiv;
        foreach ($resData as $key => $record) {
            if (!empty($record['phone']) || (!empty($record['email']) )) {

                $name = $record['first_name'] . " " . $record['last_name'];
                $phone = $record['phone'];
                $user_id = md5($record['user_id']);
                $enqId = md5($record['id']);
                $message = $record['enq_specifics'];
                $dateTime = $record['time_of_enquiry'];
                $enqStatus = $record['enquiry_paid_status'];
                $enqResponse = $record['facility_responded'];
                $clickdisplay = $record['clicked_display'];
                $time_of_response = $record['time_of_response'];

                if ($enqResponse == 'y') {
                    $resp = "<i class='fa fa-check-square fa-2x' title='Answered' style='color : #36e036;'></i><p style='text-align:center;'>Answered</p>"
                            . "<p style='text-align:center;'>$time_of_response</p>";
                } else {

                    if (($plan_id > 1 && $clickdisplay != 1) || ($plan_id <= 1 && $enqStatus < 1)) {
                        $resp = "<select class='supplier_response' style='font-style:normal;text-align:center;color: #ccc;' disabled>"
                                . "<option value>Select</option><option value=''>Yes</option>"
                                . "</select>"
                                . "<p style='text-align:center;'><i class='fa fa-check-square fa-2x' title='Un-Answered' style='color: red;'></i></p>"
                                . "<p style='text-align:center;'>Unanswered</p>";
                    } else {
                        $resp = "<select class='supplier_response' style='font-style:normal;text-align:center;'>"
                                . "<option value>Select</option><option value='$enqId'>Yes</option>"
                                . "</select>"
                                . "<p style='text-align:center;'><i class='fa fa-check-square fa-2x' title='Un-Answered' style='color: red;'></i></p>"
                                . "<p style='text-align:center;'>Unanswered</p>";
                    }
                }

                if ($plan_id > 1 || $enqStatus == 1 || $enqStatus == 2 || $enqStatus == 3) {

                    if ($clickdisplay == 1) {

                        $output = "<div class='row' style='margin-top: 15px; margin-bottom: 15px; border: 1px solid #ccc;'>"
                                . "<div class='col-sm-4'>"
                                . "<p style='margin-top:25px;'>The persons name is : </p>"
                                . "<p>The persons email is : </p>"
                                . "<p>And their contact no. is : </p>"
                                . "<p>Date/Time of Enquiry : </p>"
                                . "<p>Message : </p>"
                                . "</div>"
                                . "<div class='col-sm-3'>"
                                . "<p style='margin-top:25px; text-align:center; font-weight: bold;'>Enquiry Status</p>"
                                . "<p style='font-style: italic'>&nbsp;</p>"
                                . "<p style='font-style: italic; text-align:center;'>" . $resp . "</p>"
                                . "<p style='font-style: italic'>&nbsp;</p>"
                                . "<p style='font-style: italic'>&nbsp;</p>"
                                . "</div>"
                                . "<div class='col-sm-4'>"
                                . "<p style='color:#f25822;font-weight: 900;margin-top:25px;'>" . $name . " </p>"
                                . "<p style=\"color:#f25822;font-weight: 900;\"><a href=\"mailto:" . $record['email'] . "\">" . $record['email'] . "</a> </p>"
                                . "<p style=\"color:#f25822;font-weight: 900;\">" . $phone . " </p>"
                                . "<p>" . $dateTime . " </p>"
                                . "<p>" . $message . " </p>"
                                . "</div>"
                                . "</div>";
                    } else {
                        $output = "<div class='row default' style='margin-top: 15px; margin-bottom: 15px; border: 1px solid #ccc;'>"
                                . "<div class='col-sm-4'>"
                                . "<p style='font-style: italic; margin-top:25px;'>The persons name is : </p>"
                                . "<p style='font-style: italic'>The persons email is : </p>"
                                . "<p style='font-style: italic'>And their contact no. is : </p>"
                                . "<p style='font-style: italic'>Date/Time of Enquiry : </p>"
                                . "<p style='font-style: italic'>Message : </p>"
                                . "</div>"
                                . "<div class='col-sm-3'>"
                                . "<p style='margin-top:25px; text-align:center; font-weight: bold;'>Enquiry Status</p>"
                                . "<p style='font-style: italic'>&nbsp;</p>"
                                . "<p style='font-style: italic; text-align:center;'>" . $resp . "</p>"
                                . "<p style='font-style: italic'>&nbsp;</p>"
                                . "<p style='font-style: italic'>&nbsp;</p>"
                                . "</div>"
                                . "<div class='col-sm-3'>"
                                . "<p style='font-style: italic; margin-top:25px;'>" . rtrim(mb_strimwidth($name, 0, 6, "...")) . " </p>"
                                . "<p style='font-style: italic'>" . rtrim(mb_strimwidth($record['email'], 0, 10, "...@...")) . " </p>"
                                . "<p style='font-style: italic'>" . rtrim(mb_strimwidth($phone, 0, 8, "...")) . " </p>"
                                . "<p style='font-style: italic'>" . $dateTime . " </p>"
                                . "<p style='font-style: italic'>" . rtrim(mb_strimwidth($message, 0, 6, "...")) . " </p>"
                                . "</div>"
                                . "<div class='col-sm-2'>"
                                . "<button style='margin-top: 45px; width:100%;' value='" . $enqId . "' class='displayButton btn btn-danger inner_btn comment_btn' type='submit'>Display</button>"
                                . "</div>"
                                . "</div>"
                                . "<div class='row showonclick' style='margin-top: 15px; margin-bottom: 15px; border: 1px solid #ccc;'>"
                                . "<div class='col-sm-4'>"
                                . "<p style='margin-top:25px;'>The persons name is : </p>"
                                . "<p>The persons email is : </p>"
                                . "<p>And their contact no. is : </p>"
                                . "<p>Date/Time of Enquiry : </p>"
                                . "<p>Message : </p>"
                                . "</div>"
                                . "<div class='col-sm-3'>"
                                . "<p style='margin-top:25px; text-align:center; font-weight: bold;'>Enquiry Status</p>"
                                . "<p style='font-style: italic'>&nbsp;</p>"
                                . "<p style='font-style: italic; text-align:center;'>" . $resp . "</p>"
                                . "<p style='font-style: italic'>&nbsp;</p>"
                                . "<p style='font-style: italic'>&nbsp;</p>"
                                . "</div>"
                                . "<div class='col-sm-4'>"
                                . "<p style='color:#f25822;font-weight: 900;margin-top:25px;'>&nbsp;" . $name . " </p>"
                                . "<p style=\"color:#f25822;font-weight: 900;\">&nbsp;<a href=\"mailto:" . $record['email'] . "\">" . $record['email'] . "</a> </p>"
                                . "<p style=\"color:#f25822;font-weight: 900;\">&nbsp;" . $phone . " </p>"
                                . "<p>&nbsp;" . $dateTime . " </p>"
                                . "<p>&nbsp;" . $message . " </p>"
                                . "</div>"
                                . "</div>";
                    }
                    echo $output;
                } else {

                    $output = "<div class='row' style='margin-top: 15px; margin-bottom: 15px; border: 1px solid #ccc;'>"
                            . "<div class='col-sm-4'>"
                            . "<p style='margin-top:25px; font-style: italic'>The persons name is : </p>"
                            . "<p style='font-style: italic'>The persons email is : </p>"
                            . "<p style='font-style: italic'>And their contact no. is : </p>"
                            . "<p style='font-style: italic'>Date/Time of Enquiry : </p>"
                            . "<p style='font-style: italic'>Message : </p>"
                            . "</div>"
                            . "<div class='col-sm-3'>"
                            . "<p style='margin-top:25px; text-align:center; font-weight: bold;'>Enquiry Status</p>"
                            . "<p style='font-style: italic'>&nbsp;</p>"
                            . "<p style='font-style: italic; text-align:center;'>" . $resp . "</p>"
                            . "<p style='font-style: italic'>&nbsp;</p>"
                            . "<p style='font-style: italic'>&nbsp;</p>"
                            . "</div>"
                            . "<div class='col-sm-3'>"
                            . "<p style='margin-top:25px; font-style: italic'>" . rtrim(mb_strimwidth($name, 0, 6, "...")) . " </p>"
                            . "<p style='font-style: italic'>" . rtrim(mb_strimwidth($record['email'], 0, 10, "...@...")) . " </p>"
                            . "<p style='font-style: italic'>" . rtrim(mb_strimwidth($phone, 0, 8, "...")) . " </p>"
                            . "<p style='font-style: italic'>" . $dateTime . " </p>"
                            . "<p style='font-style: italic'>" . rtrim(mb_strimwidth($message, 0, 6, "...")) . "</p>"
                            . "</div>"
                            . "<div class='col-sm-2'>"
                            . "<button style='margin-top: 45px; width:100%;' value='" . $enqId . "-" . $user_id . "' class='nonUpgradeddisplayButton btn btn-danger inner_btn comment_btn' type='submit'>Display</button>"
                            . "</div>"
                            . "</div>";
                    echo $output;
                }
            }
        }
        $options_modal = '<div class="dashboard_right_col">                
                            <div id="myModal" class="modal fade" role="dialog">
                                <div class="modal-dialog" style="width:890px;">
                                    <div class="modal-content">                            
                                        <div class="provider-compair-heading">
                                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>To access the enquirers details, simply choose one of these options.. <i class="fa fa-times-circle" aria-hidden="true"style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                                        </div>
                                        
                        <div class="provider-compair-form">                            
                                <div class="form-group" style="display:inline-block;">
                                    <div class="col-sm-9">
                                        <p><strong>1. ACCESS ENQUIRY NOW FOR FREE*.</strong><br>
                                        * Access the enquiry now and only pay if the lead turns into a
                                        sale / bed admission. Click [Access Enquiry Now] button to view terms and any success fee.</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-dangers" id="paylater_button" style="width: 182px; color: #fff;background-color: #042e6f;border-color: rgba(0,0,0,.5);margin-top: 17px;"> Access Enquiry Now </button>                                    
                                        <p style="text-align:center;color: #FF9800;">Free</p>                                      
                                    </div>
                                                                    </div>
                           
                                <div class="form-group" style="display:inline-block;margin-top:20px;">
                                    <div class="col-sm-9">
                                        <p><strong>2. UPGRADE NOW.</strong><br>
                                            This will give you immediate access to enquiries plus
                                            increase your conversion rates with extra photos, a website
                                            link, extra content and more. Either login to upgrade or send
                                            an \'Upgrade\' request and we\'ll contact you back.</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-dangers" id="upgrade_button" style="width: 182px; color: #fff;background-color: #f15922;border-color: rgba(0,0,0,.5);margin-top: 10px;"> Request Upgrade </button>
                                        <p style="text-align:center;color: #FF9800;">From ' . $amtforupgrade . ' + gst</p>
                                        <p style="text-align:center; margin-top: -15px;">OR</p>
                                        <a href="https://www.agedadvisor.nz/supplier/login" style="width: 182px; margin-top: -7px;background-color: #f15922;color: #fff;border-color: rgba(0,0,0,.5);" class="btn btn-dangers">Login to Upgrade</a>                                        
                                    </div>    
                                </div>
                            
                            
                                <div class="form-group" style="display:inline-block;">
                                    <div class="col-sm-9">
                                        <p><strong>2. PAY NOW FOR THE ENQUIRY ONLY.</strong><br>
                                        Pay only 50% of the Bronze upgrade cost and gain
                                        immediate access to this enquiry now, without upgrading.</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-dangers" id="paynow" style="width: 182px; color: #fff;background-color: #042e6f;border-color: rgba(0,0,0,.5);margin-top: 20px;"> Pay for Enquiry Now </button>
                                        <p style="text-align:center;color: #FF9800;">Only $' . $gst . ' + gst</p>
                                    </div>    
                                </div>
                            
                            
                               
                                   
                                    <p style="text-align:center;color: #FF9800; margin-bottom: 0px;"><a href="https://www.agedadvisor.nz/why_am_i_being_charged" target="_blank">Why am I being charged for some options?</a></p>
                              
                            <div class="divider">
                                <hr class="left"/>If you are not taking enquiries, then select from below<hr class="right" />
                            </div>                                        

                                                <div class="form-group" style="display:inline-block;">
                                                    <div class="col-sm-9">
                                                        <p><strong>NOT `CURRENTLY` TAKING ENQUIRIES.</strong><br>
                                                        If you`re full, we will advise the enquirer but continue to
                                                        keep you informed when another enquiry comes in.</p>
                                                    </div>
                                                    <div class="col-sm-3">

                                                        <button type="button" class="nottaking btn btn-dangers" value="not" style="width: 182px; color: #fff;background-color: #4f5e94;border-color: rgba(0,0,0,.5);margin-top: 15px;"> Not Taking Enquiries </button>
                                                    </div>
                                                </div>

                                                <div class="form-group" style="display:inline-block;">
                                                    <div class="col-sm-9">
                                                        <p><strong>NOT TAKING ENQUIRIES - PLEASE STOP EMAILS.</strong><br>
                                                        We will advise the enquirer and stop sending any future
                                                        enquiries through to you, unless you advise us otherwise.</p>
                                                    </div>
                                                    <div class="col-sm-3">                                        
                                                        <button type="button" class="notinterested btn btn-dangers" value="nin" style="width: 182px; color: #fff;background-color: #4f5e94;border-color: rgba(0,0,0,.5);margin-top: 15px;"> Stop ALL Enquiry Emails </button>
                                                    </div>
                                                </div>                                                                                                                                         

                                            <div style="height:0px;">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>                
                        </div>

                        <div class="dashboard_right_col">                
                            <div id="notModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content blue-light-gradient">
                                        <div class="provider-compair-heading">
                                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>ARE YOU SURE? <i class="fa fa-times-circle" aria-hidden="true"style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                                        </div>
                                        <div class="provider-compair-form">                            
                                            <form name="cancelform" method="post" id="cancelform">                                                             
                                                <div style="height:20px;">&nbsp;</div>                                                                                                                             
                                                <input type="hidden" name="supname" id="supname" value="">                                                
                                                <input type="hidden" name="uid" id="uid" value="">                                                
                                                <button type="submit" value="" class="btn btn-danger" id="sec-modal">Yes</button>
                                                <button type="button" class="btn btn-info" data-dismiss="modal" style="margin-left:15px;" id="cancel-button">Cancel</button>
                                            </form>
                                            <div style="height:0px;">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                            </div>                
                        </div>
                        <div class="dashboard_right_col">                
                            <div id="payNowModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content blue-light-gradient">             
                                        <div class="provider-compair-heading">
                                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>Pay Now: Secure Credit Card Payment <i class="fa fa-times-circle" aria-hidden="true"style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                                        </div>
                                        <div class="provider-compair-form">
                                            <!--<h4 id="info" style="color: #f9920d; margin-left: 15px;"></h4>-->
                                            <div class="alert alert-success" role="alert" id="infos" style="display:none; text-align: center;">                                
                                            </div>
                                            <div class="alert alert-danger" role="alert" id="infof" style="display:none; text-align: center;">

                                            </div>
                                            <!--AMIT ENC KEY
                                            <form name="payNowForm" id="payNowForm" data-eway-encrypt-key="i0ofnD2guLGAigbo6kLRzYJGQY6VpNkG8MjOnn86pBIlgW2jZeHOQamImT9t2L5gX8wwWb8wIyWBqb/UZ873k/Eq7LOcarcWzBCIT8C2mYuDUZKWV+U0k5oaGBJ5QRVTlWngbEplEznmSS+4priuyH0qU369moki4hax5jmWz0YPEBM7sdAK/WaofNG+Zcngiee94jB/3+GpGtbXVisCDlp2IL1FcmIOcrIc3X47cPutLGTRbZHIsiDLs0wGebimzXMeJ+rZnlk+mxJfF7YiVAGNjdF3Bi0eeUYlAYKkrO6OZSclDOF+wpEFmM84NcFeKy+aoiU8awnOMRnClzRfvQ==">-->
                                            <form name="payNowForm" id="payNowForm" data-eway-encrypt-key="u7kmQFBSy9ZWHMj+5q+uxkJhbMalzh8Ni5DiNn75MG4NmxFufiEqtAX/GNW2cmSUACk42g0MwBDD+mETaiEJlfNyhMCqYU/eWk+jMY/raxxQFlzfMtDhRUIPm7xkJDeucAvvqx15L5mg5efpNJBxnkTwxwIGMjDvTMCt4kmcCrtLaIFRvHyX9OSqczetvlzWA1NzTyhbyw1z/o45FwQy9smE+QiRvLF8PH5Zt+yNelPf55ksKCxEIzkJmmumtz7dZJgCj6qHs/6j8E5wW6V0qs653Y4lK9W9rYSNQdt5Ks6f+pRrBUlb7USKyC8CLtkuysrRrFIliz8tfONr9gGihw==">
                                                <div class="form-group" style="min-height: 50px;">
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Name on Card</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="text" value="" class="form-control" id="card_name" placeholder="Name">
                                                    </div>                        
                                                </div>                                        
                                                <div class="form-group" style="min-height: 50px;">
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Card Number</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="number" value="" class="form-control" id="card_number" placeholder="Number">
                                                    </div>
                                                </div>                                       
                                                <div class="form-group" style="min-height: 50px;">                                    
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">CVV</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="text" value="" class="form-control" id="cvv" placeholder="CVV">
                                                    </div>
                                                </div>                                 
                                                <div class="form-group" style="min-height: 50px;">                                    
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Valid Till</label>
                                                    <div class="col-sm-4" style="margin-bottom: 15px;">                                        
                                                        <select name="month" id="month" style="width:100%;min-height: 34px;" required>
                                                            <option value="">Month</option>
                                                            <option value="01">January</option>
                                                            <option value="02">February</option>
                                                            <option value="03">March</option>
                                                            <option value="04">April</option>
                                                            <option value="05">May</option>
                                                            <option value="06">June</option>
                                                            <option value="07">July</option>
                                                            <option value="08">August</option>
                                                            <option value="09">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select> 
                                                    </div>
                                                    <div class="col-sm-5" style="margin-bottom: 15px;">
                                                        <input type="text" class="form-control" id="year" placeholder="Year(yy or yyyy)" required maxlength="4" pattern="\d{4}">
                                                    </div>
                                                </div>
                                                <div class="form-group" style="min-height: 50px;">
                                                    <div class="col-sm-3">
                                                        <label class="col-form-label">Total Amount</label>
                                                        <p style="font-size: 13px;margin-top: -10px;">*Including gst</p>
                                                    </div>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="text" value="' . $amt . '" class="form-control" placeholder="Amount" disabled>
                                                    </div>
                                                </div> 
                                                <div class="form-group"> 
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;"></label>                                    
                                                    <button type="submit" id="submit_payment" class="btn btn-danger" style="width:120px;margin-left: 15px;">Make Payment </button>
                                                    <p style="display: none;width: 140px; margin-left: 15px;" id="sendingmsgpay" class="btn btn-danger">Processing..<span> 
                                                        <img src="../../assets/img/72.gif"> </span>
                                                    </p>
                                                    <p style="display: none;width: auto;" id="sentmsgpay" class="btn btn-danger">Transaction Successful! Click here to see the enquirer details.</p>
                                                    <input type="hidden" name="supid" id="supid" value="' . $sup_id . '"> 
                                                    <input type="hidden" name="enqid" id="enqid" value="' . $enqId . '">
                                                    <input type="hidden" name="prodid" id="prodid" value="' . $prod_id . '">
                                                </div>                                                            
                                                <div style="height:0px;">&nbsp;</div>
                                            </form>
                                        </div>
                                        <div>

                                                <img src="https://seeklogo.com/images/M/mastercard-logo-473B8726A9-seeklogo.com.png" class="images" alt="master">

                                                <img src="https://seeklogo.com/images/V/VISA-logo-62D5B26FE1-seeklogo.com.png" class="images">

                                                <img src="https://eway.io/images/eway-logos/eway-logo-1499-630.png" class="images">

                                        </div>
                                    </div>
                                </div>
                            </div>                
                        </div>

                        <div class="dashboard_right_col">                
                            <div id="upgradeModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content blue-light-gradient">        <!-- When logged in to their account upgrade option-->            
                                        <div class="provider-compair-heading">
                                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>Please enter the following details <i class="fa fa-times-circle" aria-hidden="true" style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                                        </div>
                                        <div class="provider-compair-form">
                                            <div class="alert alert-success" role="alert" id="upgradeinfos" style="display:none; text-align: center;">
                                                <strong>Thank You!</strong> Your details has been sent to AgedAdvisor!
                                            </div>
                                            <div class="alert alert-danger" role="alert" id="upgradeinfof" style="display:none; text-align: center;">
                                                <strong>Oh snap!</strong> Server error. Please try again!
                                            </div>
                                            <form name="upgradeform" id="upgradeform">                                 
                                                <div class="form-group" style="min-height: 50px;">
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Name</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="text" value="' . $supfullname . '" class="form-control" id="upgradename" placeholder="Name" required>
                                                    </div>                        
                                                </div>                                        
                                                <div class="form-group" style="min-height: 50px;">
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Facility Name</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="text" value="' . $fac_title . '" class="form-control" id="upgradefname" placeholder="Facility Name" disabled>
                                                    </div>
                                                </div>                                       
                                                <div class="form-group" style="min-height: 50px;">                                    
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Phone</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="number" value="' . $supphone . '" class="form-control" id="upgradephone" placeholder="Phone" required>
                                                    </div>
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;"></label>
                                                    <button type="submit" id="upgrade_submit" class="btn btn-danger" style="margin-left: 15px;">Request Upgrade </button>
                                                    <p style="display: none;width: 140px; margin-left: 15px;" id="sendingmsgupgrade" class="btn btn-danger">Sending Now<span> 
                                                        <img src="../../assets/img/72.gif"> </span>
                                                    </p>
                                                    <p style="display: none;width: 110px; margin-left: 15px;" id="sentmsgupgrade" class="btn btn-danger">Request Sent!</p>
                                                    <p style="margin-left: 15px; margin-top: 15px; font-size: 17px;color: #000;">We will automatically display the enquirers details once this is sent.</p>                                                                        
                                                </div>                                 
                                                <div style="height:0px;">&nbsp;</div>
                                            </form>
                                        </div>
                                        <div class="provider-compair-headings" style="text-align:center;">
                                            <p>or</p>
                                            <h4>Free Phone 0800 243323</h4>
                                            <h4><a href="https://www.agedadvisor.nz/Upgrade_options_jul2017.pdf" target="_blank">Download Prices and Options PDF</a></h4>
                                        </div>
                                    </div>
                                </div>
                            </div>                
                        </div>

                        <div class="dashboard_right_col">                <!-- When logged in to their account paylater option-->
                            <div id="paylaterModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content blue-light-gradient">             
                                        <div class="provider-compair-heading">
                                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>Please enter the following details <i class="fa fa-times-circle" aria-hidden="true" style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                                        </div>
                                        <div class="provider-compair-form">                                                        
                                            <div class="alert alert-success" role="alert" id="paylaterinfos" style="display:none; text-align: center;">
                                                <strong>Thank You!</strong> Aged Advisor will invoice your facility <strong>' . $paylateramt . ' ONLY IF</strong> this lead turns into a sale/admission.                                
                                            </div>
                                            <div class="alert alert-danger" role="alert" id="paylaterinfof" style="display:none; text-align: center;">
                                                <strong>Oh snap!</strong> Server error. Please try again!
                                            </div>
                                            <form name="paylaterform" id="paylaterform">
                                                <div class="form-group" style="min-height: 50px;">                                    
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Facility Name</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="text" value="' . $fac_title . '" class="form-control" placeholder="Facility Name" disabled="disabled">
                                                    </div>
                                                </div>
                                                <div class="form-group" style="min-height: 50px;">
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Name</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="text" value="" class="form-control" id="latername" placeholder="Your Name" required>
                                                    </div>                        
                                                </div>                                                                                                               
                                                <div class="form-group" style="min-height: 50px;">                                    
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Position</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="text" value="" class="form-control" id="laterposition" placeholder="Your Position" required>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="min-height: 50px;"> Get the enquiry now for FREE, and only pay if the enquiry converts into a sale or bed admission. A payment, only on invoice of ' . $paylateramt . ', is payable to Agedadvisor on confirmation of sale or bed admission.                                   
                                                    <!--<label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Total Amount</label>
                                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                                        <input type="text" value="' . $paylateramt . '" class="form-control" placeholder="Amount" disabled>
                                                    </div>-->
                                                </div>

                                                <div class="form-group" style="min-height: 50px;">                                    
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;"></label>
                                                    <div class="col-sm-1" style="margin-bottom: 15px;">
                                                        <input type="checkbox" value="" class="form-control" id="terms"  checked="checked" required>
                                                    </div>
                                                    <div class="col-sm-8" style="margin-bottom: 15px; padding: 0;">
                                                        <span><a href="https://www.agedadvisor.nz/terms-and-conditions" target="_blank">I agree to the Terms and Conditions.</a></span>
                                                    </div>
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;"></label>
                                                    <button type="submit" id="paylater_submit" class="btn btn-danger" style="width:112px;margin-left: 15px;">Submit </button>
                                                    <p style="display: none;width: 140px; margin-left: 15px;" id="sendingmsgpaylater" class="btn btn-danger">Sending Now <span> 
                                                        <img src="../../assets/img/72.gif"> </span>
                                                    </p>
                                                    <p style="display: none;width: 110px; margin-left: 15px;" id="sentmsgpaylater" class="btn btn-danger">Request Sent!</p>                                    
                                                </div>                                                            
                                                <div style="height:0px;">&nbsp;</div>                                
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>                
                        </div>';
        echo $options_modal;
        echo "</div>";
    } else {
        $output = '<table class = "table table-bordered table_bord"><tbody><tr><td class = "text-center text-danger">No enquiries to show!</td></tr></tbody></table>';
        echo $output;
    }
    ?>

    <script src="https://secure.ewaypayments.com/scripts/eCrypt.min.js"></script>
    <script>
        $(document).ready(function () {
            var path = '<?php echo HOME_PATH; ?>ajaxFront.php';
            var paymentPath = '<?php echo HOME_PATH; ?>enquiry-payment/index.php';

            $('.supplier_response').on('change', function () {
                var sup_response = $(this).val();
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'logResponse', response: sup_response},
                    success: function (result) {
                        if (result == true) {
                            location.reload(true);
                        }
                    }
                });
            });

            $('.showonclick').hide();
            $('.displayButton').on('click', function () {
                $(this).parent().parent().hide();
                $(this).parent().parent().next().show();
                var enqid = $(this).val();
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'enquiry', id: enqid},
                    success: function (response) {}
                });
            })

            $('.nonUpgradeddisplayButton').on('click', function () {
                var tempval = $(this).val();
                ids = tempval.split('-');
                enqid = ids[0];
                uid = ids[1];
                $('#enqid').val(enqid);
                $('#uid').val(uid);
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'enquiry', id: enqid},
                    success: function (response) {}
                });
                $('#myModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            $('#paynow').on('click', function () {
                $('#myModal').modal('hide');
                $('#payNowModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            $('#upgrade_button').on('click', function () {
                $('#myModal').modal('hide');
                $('#upgradeModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });
            $('#paylater_button').on('click', function () {
                $('#myModal').modal('hide');
                $('#paylaterModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
            });

            $("#submit_payment").click(function (event) {
                $('#notModal').modal('hide');
                $("#sendingmsgpay").show();
                $("#submit_payment").hide();
                $.ajax({
                    type: 'POST',
                    url: paymentPath,
                    data: {
                        card_name: $('#card_name').val(),
                        card_number: eCrypt.encryptValue($('#card_number').val()),
                        card_cvn: eCrypt.encryptValue($('#cvv').val()),
                        month: $('#month').val(),
                        year: $('#year').val(),
                        supid: $('#supid').val(),
                        prodid: $('#prodid').val(),
                        enqid: $('#enqid').val()
                    },
                    success: function (response) {
                        var result = jQuery.parseJSON(response);
                        if (result.status == 1) {
                            $('#infos').html(result.response_message);
                            $('#infos').show();
                            $('#infof').hide();
                            $('#payNowForm').get(0).reset();
                            $("#sentmsgpay").show();
                            $("#sendingmsgpay").hide();
                            $("#sentmsgpay").click(function () {
                                location.reload();
                            });
                        } else if (result.status == 2) {
                            $("#sendingmsgpay").hide();
                            $("#submit_payment").show();
                            var error_message = "";
                            error_message += "Error: " + result.response_message + "<br>";
                            $('#infof').html(error_message);
                            $('#infof').show();
                            $('#infos').hide();
                        }
                    }
                });
                event.preventDefault();
            });

            $('.nottaking, .notinterested').click(function (event) {
                var reason = $(this).val();
                $('#sec-modal').val(reason);
                $('#myModal').modal('hide');
                $('#notModal').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                event.preventDefault();
            });

            $("#cancelform").submit(function (event) {
                reason = $('#sec-modal').val();
                uid = $('#uid').val();
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'extra-enquiry-not-upgraded-not-interested', reason: reason, prid: '<?php echo  $prod_id ?>', suid: <?php echo  $sup_id ?>, uid: uid},
                    success: function (response) {
                    }
                });
                $('#notModal').modal('hide');
                event.preventDefault();
            });

            $("#upgradeform").submit(function (event) {
                var enqid = $('#enqid').val();
                $("#sendingmsgupgrade").show();
                $("#upgrade_submit").hide();
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'new-enquiry-to-upgrade', prodid: '<?php echo  $prod_id ?>', supid: <?php echo  $sup_id ?>, enqid: enqid, supname: '<?php echo  $supfullname ?>', supphone: '<?php echo  $supphone ?>'},
                    success: function (response) {
                        if (response == 1) {
                            $("#upgradeinfos").show();
                            $("#sentmsgupgrade").show();
                            $("#sendingmsgupgrade").hide();
                            setTimeout(function () {
                                location.reload();
                            }, 3000);
                        } else {
                            $("#upgradeinfof").show();
                            setTimeout(function () {
                                $("#sendingmsgupgrade").hide();
                                $("#upgradeinfof").hide();
                                $("#upgrade_submit").show();
                            }, 1500);
                        }
                    }
                });
                event.preventDefault();
            });

            $("#paylaterform").submit(function (event) {
                var enqid = $('#enqid').val();
                var supname = $('#latername').val();
                var supposition = $('#laterposition').val();
                $("#sendingmsgpaylater").show();
                $("#paylater_submit").hide();
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'new-enquiry-to-paylater', prodid: '<?php echo  $prod_id ?>', supid: <?php echo  $sup_id ?>, enqid: enqid, supname: supname, supposition: supposition},
                    success: function (response) {
                        if (response == 1) {
                            $("#paylaterinfos").show();
                            $("#sentmsgpaylater").show();
                            $("#sendingmsgpaylater").hide();
                            setTimeout(function () {
                                location.reload();
                            }, 8000);
                        } else {
                            $("#paylaterinfof").show();
                        }
                    }
                });
                event.preventDefault();
            });
        })
    </script>
    <?php        
    die;
    }

    function manage_gallery($uid) {
    global $db;
    $gallery_type = 0;
    $condition = " where deleted = 0 ";
    $query = "SELECT id, title, file FROM " . _prefix("galleries") . " where deleted = 0 AND pro_id=".$_SESSION['proId'];
    //AND user_id=" . $uid . // So will show previous owners images too.
    $query .= ' AND type=' . $gallery_type;
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
    $id = $_POST['pageId'];
    $i = $i + PAGE_PER_NO * $id;
    } else {
    $id = '0';
    $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;


    /*$query = "SELECT id, title,file, status,type, created FROM " . _prefix("galleries") . "   "
    . " where deleted = 0 AND pro_id=".$_SESSION['proId']." AND user_id=" . $uid . ' AND type=' . $gallery_type . "  ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;*/

    $query_check_radio_value = "SELECT * FROM ad_galleries 
    where deleted = '0' AND pro_id={$_SESSION['proId']} ";
    //AND user_id={$uid}// So will show previous owners images too.

    $query_check_radio_value.=" AND type= {$gallery_type} AND status_first_img='1'" ;
    $check_radio=mysqli_query($db->db_connect_id, $query_check_radio_value);

    while($check_radio_value=mysqli_fetch_array($check_radio))
    {

    $check_radio_file=  $check_radio_value['file'];
    $check_radio_status_first_img= $check_radio_value['status_first_img'];
    $img_id= $check_radio_value['id'];
    }


    $query = "SELECT id, title,file, status,type, created FROM " . _prefix("galleries") . "   "
    . " where deleted = 0 AND pro_id=".$_SESSION['proId'];
    //." AND user_id=" . $uid . 
    $query .=    ' AND type=' . $gallery_type . "  ORDER BY status_first_img
    DESC limit " . $pageLimit . ',' . PAGE_PER_NO;     

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    $output = '<div class="tab-pane fade in active" id="home">';
    if ($count > 0) {
    $output .= '<div class="table-responsive">
    <table class="table table-bordered table_bord gallery_bord">
    <thead><tr>
    <th class="th_text" align="center">Thumbnail</th>
    <th class="th_text">Images title</th>
    <th class="th_text">Select Ist Image</th>
    <th class="th_text">Options</th>
    </tr></thead>
    <tbody>';
    foreach ($data as $key => $record) {

    if($record['id']==$img_id){
    $checked = "checked='checked'";
    }
    else{
    $checked=' ';
    }

    $fileImage = ADMIN_PATH . 'files/gallery/images/thumb_' . $record['file'];
    $fileImageSrc = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
    $fileImageView = HOME_PATH . 'admin/files/gallery/images/' . $record['file'];

    $output.='<tr class="row_' . $record['id'] . '" >';
    $output.='<td style="width:20%;">';
    $output.='<div class="row">';
    $output.='<div class="col-md-12 col-sm-12 col-xs-12 text-center">';
    if (file_exists($fileImage)) {
    $output .=!empty($record['file']) ? '<img title="View Image"  src="' . $fileImageSrc . '" alt="">' : "";
    } else {
    $output .= 'N/A';
    }

    $output.='</div>';
    $output.='</div>';
    $output.='</td>';
    $output.='<td style="width:40%;">' . $record['title'] . ' </td>';
    $output.='<td style="width:40%;"><input type="radio" id="galley_image" name="gallery_image" class="galley_image" value='.$record['id'].' '.$checked.'> </td>';

    $output.='<td class="table_view text-center" style="width:22%;">
    <a id="edit" class= title="' . $record['file'] . '" href="editGallery?edit&gtitle='.base64_encode($record['title']).'&gallid='.base64_encode($record['id']).'">Edit</a> <a target="_blank" id="imagefirst_id"  class="fancybox" title="' . $record['file'] . '" href="' . $fileImageView . '">View</a> <a class="last delete" href="editGallery?delete&gtitle='.base64_encode($record['title']).'&gallid='.base64_encode($record['id']).'"  id="del-galleries-' . $record['id'] . '">Delete</a></td>';
    $output.='</tr>';

    $i ++;
    }
    $output.='</tbody></table>
    </div>';

    $paging_query = "SELECT id, title,file FROM " . _prefix("galleries") . "   " . " where deleted = 0 AND pro_id=".$_SESSION['proId'];
    //" AND user_id=" . $uid . 
    $paging_query .=' AND type=' . $gallery_type;
    $paging_res = $db->sql_query($paging_query);
    $pagingCount = $db->sql_numrows($paging_res);
    if ($pagingCount > 0) {
    $data = '';
    $paginationCount = getPagination($pagingCount);
    $colspan = 6;
    if (isset($search_type)) {
    $data .="&search_input=$search_input&search_type=$search_type";
    }
    $output .= get_pagination_link($paginationCount, 'gallery', $data, $colspan);
    }
    } else {
    $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    $output .= '</div>';

    $query = "SELECT id, title, file FROM " . _prefix("galleries") . " where deleted = 0 AND pro_id=".$_SESSION['proId'];
    //" AND user_id=" . $uid . 
    $query.=' AND type IN (1,2)';
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
    $id = $_POST['pageId'];
    $i = $i + PAGE_PER_NO * $id;
    } else {
    $id = '0';
    $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;

    $query = "SELECT id, title,file,type FROM " . _prefix("galleries") . "   "
    . " where deleted = 0  AND pro_id=" . $_SESSION['proId'] . " AND type IN (1,2) ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    $output .= '<div class="tab-pane fade" id="ios">';
    if ($_SESSION['videoEnable'] == 1) {
    if ($count > 0) {
    $output .= '<div class="table-responsive">
    <table class="table table-bordered table_bord gallery_bord" id="pageData">
    <thead><tr>
    <th class="th_text">Video Title</th>
    <th class="th_text">Video Type</th>
    <th class="th_text">Options</th>
    </tr></thead>
    <tbody>';
    $j=1;
    foreach ($data as $key => $record) {
    $playImage = HOME_PATH . 'images/play.png';
    $fileVideo = ADMIN_PATH . 'files/gallery/videos/' . $record['file'];
    $output.='<tr class="row_' . $record['id'] . '" >';
    $output.='<td style="width:30%;">' . $record['title'] . ' </td>';
    if ($record['type'] == 1) {
    if (file_exists($fileVideo)) {
    $output.='<td style="width:40%;text-align:center;">Uploaded Video </td>';

    $output .=!empty($record['file']) ? '<td class="table_view" style="width:22%;"><a  class="videoPlayer" href="#videoShow'.$j.'"><img  title="Play Video" src="' . $playImage . '"></a> <a class="last delete"  href="javascript:void(0);" id="del-galleries-' . $record['id'] . '">&nbsp;Delete</a><div id="videoShow'.$j.'" style="display:none" >'
    . '<object id="flowplayer'.$j.'" width="400" height="288" data="'.HOME_PATH.'js/flowplayer-3.2.7.swf" type="application/x-shockwave-flash">'
    . '<param name="allowfullscreen" value="true" />'
    . '<param name="flashvars" value="config={\'clip\':{\'url\':\''.HOME_PATH . 'admin/files/gallery/videos/' . $record['file'].' \',\'autoPlay\':false}}">'
    . '</object>'
    . '</div></td>' : '';
    } else {
    $output .= '<td>N/A</td><td><a class="last delete"  href="javascript:void(0);" id="del-galleries-' . $record['id'] . '">&nbsp;Delete</a></td>';
    }
    } else if ($record['type'] == 2) {
    $output.='<td style="width:40%;">Youtube Video </td>';

    $output .=!empty($record['file']) ? '<td class="table_view" style="width:20%;"><a  class="youtube" href="' . $record['file'] . '"><img  title="Play Video" src="' . $playImage . '"></a> <a class="last delete"  href="javascript:void(0);" id="del-galleries-' . $record['id'] . '">&nbsp;Delete</a></td>' : '';
    } else {
    $output .= '<td>N/A</td><td><a class="last delete"  href="javascript:void(0);" id="del-galleries-' . $record['id'] . '">&nbsp;Delete</a></td>';
    }
    $output.='</tr>';

    $i ++;
    $j++;
    }
    $output.='</tbody> </table></div>';

    $paging_query = "SELECT id, title,file FROM " . _prefix("galleries") . "   " . " where deleted = 0  AND pro_id=" . $_SESSION['proId'] . ' AND type IN (1,2)';
    $paging_res = $db->sql_query($paging_query);
    $pagingCount = $db->sql_numrows($paging_res);
    if ($pagingCount > 0) {
    $data = '';
    $paginationCount = getPagination($pagingCount);
    $colspan = 6;
    if (isset($search_type)) {
    $data .="&search_input=$search_input&search_type=$search_type";
    }
    $output .= get_sp_pagination_link($paginationCount, 'gallery', $data, $colspan, $_POST['pageId']);
    }
    } else {
    $output.= '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    } else {
    $output.= '<div class="tab-pane" id="ios"><div class="table-responsive"><table class="table table-bordered table_bord"><tbody><tr><td class="text-center">To Upload Video. Please Upgrade Plan</td></tr></tbody></table></div></div>';
    }

    $output .= '</div>';

    echo $output;
    ?>
    <input type="hidden" value="<?php echo $uid; ?>" id="user_id" >
    <input type="hidden" value="<?php echo $_SESSION['proId']; ?>" id="product_id" >
    <script type="text/javascript">
        $(".image").colorbox({
            width: '600px',
            height: '500px'
        });
        $(document).ready(function() {
        $(".videoPlayer").fancybox({
        'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'speedIn': 600,
                'speedOut': 200
        });
                $('.youtube').fancybox({
        openEffect: 'none',
                closeEffect: 'none',
                prevEffect: 'none',
                nextEffect: 'none',
                arrows: false,
                helpers: {
                media: {},
                        buttons: {}
                }
        });
                $(".fancybox").fancybox();
                var path = '<?php echo HOME_PATH; ?>ajaxFront.php';
                $('.status').live('click', function() {
        var details = $(this).attr('id').split('-');
                var table = details[0];
                var id = details[1];
                var status = details[2];
                $.ajax({
                type: 'POST',
                        url: path,
                        data: {action: 'status', id: id, table: table, status: status},
                        success: function(response) {
                        $('.status-' + id).html($.trim(response));
                        }
                });
        });
                /*
                 * Code for Delete
                 */

                $('.delete').live('click', function() {
        var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
        var details = $(this).attr('id').split('-');
                var table = details[1];
                var id = details[2];
                $.ajax({
                type: 'POST',
                        url: path,
                        data: {action: 'deleteFile', id: id, table: table},
                        success: function() {
                        $('.row_' + id).hide();
                        }
                });
        }
        });</script>
    <?php
    die;
}

function articles_list() {

    global $db;
    $query = "SELECT page.*, concat(user.first_name,' ',user.last_name) AS editor FROM " . _prefix('pages') . " AS page LEFT JOIN " . _prefix('users') . " AS user ON page.created_by=user.id  WHERE page.status=1 && page.deleted= 0 ORDER BY page.id DESC ";
    $resArt = $db->sql_query($query);
    $count_article = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getArticles = "SELECT page.*, concat(user.first_name,' ',user.last_name) AS editor FROM " . _prefix('pages') . " AS page LEFT JOIN " . _prefix('users') . " AS user ON page.created_by=user.id  WHERE page.status=1 && page.deleted= 0 ORDER BY page.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resArticles = $db->sql_query($getArticles);
    $count = $db->sql_numrows($resArticles);
    $dataArticles = $db->sql_fetchrowset($resArticles);
    if ($count > 0) {
        foreach ($dataArticles as $article) {
            $desClass = 'col-lg-10 col-md-10 ';
            $pullLeft = 'padding-left:0px; ';
            $short_description = (strlen($article['content']) > 250) ? substr($article['content'], 0, 250) . '...' : $article['content'];
            if ($article['banner_image'] =='') {
                $image = '';
                $desClass = 'col-lg-12 col-md-12 ';
                $pullLeft = '';
            } else {

                if (@getimagesize(DOCUMENT_PATH . 'admin/files/pages/banner/thumb_' . $article['banner_image'])) {
                    $href = MAIN_PATH . 'files/pages/banner/thumb_' . $article['banner_image'];
                    $image = ' 
                         <img class="img-responsive" src="' . $href . '" style="    height: 260px;"> 
                     ';
                }
            }
            $output .= ' <div class="col-lg-4">
                        <div class="card single-blog-item v1">
                           ' . $image . '
                         
							 <a class="blog-cat btn v6 red" href="' . HOME_PATH . 'articles/view/' . base64_encode($article['id']) . '/' . str_replace(' ', '-', $article['page_title']) . '">
						Read more</a>
                            <div class="card-body">
                                <h4 class="card-title">	<a href="' . HOME_PATH . 'articles/view/' . base64_encode($article['id']) . '/' . str_replace(' ', '-', $article['page_title']) . '" style="color:#f15922">' . $article['page_title'] . '</a></h4>
                                <div class="bottom-content">
                                    <p class="date">' . date('M d, Y', strtotime($article['created'])) . ' </p>
									
                                </div>
                            </div>
                        </div>
                    </div>';
        }
        $pagingCount = $count_article;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'articles', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord">
		<tbody>
		<tr>
		<td class="text-center">No record Found</td>
		</tr>
		</tbody>
		</table>';
    }
    echo $output;

    die;
}

function global_search($searchvalue) {
    global $db;
    if ($searchvalue != '') {
        $condition = " where prd.deleted = 0 && ( prd.title like  '%" . $searchvalue . "%' OR cit.title like '%" . $searchvalue . "%' OR srv.name like '%" . $searchvalue . "%' OR prd.keyword like '%" . $searchvalue . "%')";
        $query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " ORDER BY prd.id DESC ";
        $resArt = $db->sql_query($query);
        $count = $db->sql_numrows($resArt);
        $i = 1;
        if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
            $id = $_POST['pageId'];
            $i = $i + PAGE_PER_NO * $id;
        } else {
            $id = '0';
            $i = 1;
        }
        $pageLimit = PAGE_PER_NO * $id;
        $getArticles = "SELECT prd.id, prd.title,sp.first_name AS spFName, sp.last_name AS spLName, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd "
                . "Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type "
                . "LEFT JOIN " . _prefix("users") . " AS sp ON  prd.supplier_id= sp.id "
                . "Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " ORDER BY prd.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
        $resArticles = $db->sql_query($getArticles);
        $count = $db->sql_numrows($resArticles);
        $dataArticles = $db->sql_fetchrowset($resArticles);
        if ($count > 0) {
            foreach ($dataArticles as $article) {
                // $short_description = (strlen($article['short_description']) > 150) ? substr($article['short_description'], 0, 150) . '...' : $article['short_description'];
                if ($article['banner_image'] == '') {
                    $image = '';
                    $desClass = 'col-lg-12 col-md-12 ';
                    $pullLeft = '';
                } else {

                    if (file_exists(DOCUMENT_PATH . 'admin/files/gallery/images/' . $article['image'])) {
                        $href = HOME_PATH . 'admin/files/gallery/images/' . $article['image'];
                        $widthStyle = '';
                    }
                    $image = '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col_lg_bg" style="' . $widthStyle . '">
                        <a href="' . HOME_PATH . "search/productDetail?&id=" . md5($article['id']) . '"><img class="img-responsive" src="' . $href . '"></a>
                    </div>';
                }
                $output .= '<div class="col-md-12">
                <div class="media">
                    <h2 class="col_h2"><a href="' . HOME_PATH . "search/productDetail?&id=" . md5($article['id']) . '">' . $article['title'] . '</a> </h2>
                    <div class="posted" style="padding-bottom:0;">Facility Type: <span>' . $article['name'] . '</span></div>
                    <div class="posted" style="padding-bottom:0;">Location: <span>' . $article['city'] . '</span></div>
                    <div class="posted">Supplier Name: ' . $article['spFName'] . ' ' . $article['spLName'] . '</div>

                    ' . $image . '
                    <div class="' . $desClass . 'col-sm-12 col-xs-12 padding_left"><p class="p_text" style="' . $pullLeft . '"">' . $article['description'] . '</p></div>
                </div>
            </div>';
            }
            $pagingCount = $count;
            if ($pagingCount > 0) {
                $data = $searchvalue;
                $paginationCount = getPagination($pagingCount);
                $colspan = 6;
                $output .= get_pagination_link($paginationCount, 'globalsearch', $data, $colspan, $_POST['pageId']);
            }
        } else {
            $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    die;
}

function review_list() {

    global $db;
    $query = "SELECT fdbk.* , pds.id as prId, pds.title,pds.image as pImage, sp.user_name as spUsername,  sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername,  cs.first_name AS csFName, cs.last_name AS csLName,pds.quick_url  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
            . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . "WHERE  pds.title !='' AND fdbk.status=1 AND  fdbk.deleted=0  ORDER BY fdbk.id DESC";
    $resArt = $db->sql_query($query);
    $count1 = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getReviews = "SELECT fdbk.* ,IFNULL(fdbk.title,'N/A') as title,pds.id as prId, pds.title as pdtitle, pds.id AS pImage, sp.id as sp_id, sp.user_name as spUsername,  sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername,  cs.first_name AS csFName, cs.last_name AS csLName,pds.quick_url  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
            . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . "WHERE  pds.title !='' AND fdbk.status=1 AND  fdbk.deleted=0 ORDER BY fdbk.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $data = $db->sql_fetchrowset($resReviews);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSum = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';
    $qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
    $resultKnowMe = $db->sql_query($qryKnowMe);
    $knowMeData = $db->sql_fetchrowset($resultKnowMe);
    foreach ($knowMeData as $key => $record) {
        $knowMe[$record['id']] = $record['title'];
    }
    $qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
    $resultDuration = $db->sql_query($qryDuration);
    $durationVisitData = $db->sql_fetchrowset($resultDuration);
    foreach ($durationVisitData as $key => $record) {
        $durationVisit[$record['id']] = $record['title'];
    }
    $lastVisit = array('', 'Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
    $recommended = array('No', 'Yes', 'Not sure');

    if ($count > 0) 
	{
        foreach ($data as $key => $record) 
		{
            // print_r($record);
            if ($record['feedback'] != '') 
			{
                if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) 
				{
                    $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']) . '" alt="product image" class="img-fluid" /></a>';
                } else {
                    $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'images/no_image_available.png" alt="" class="img-fluid" /></a>';
                }
                $TotalRating = $record['overall_rating'];
                $Rating = $TotalRating;
                $overallRating = round($Rating);
                switch ($overallRating) 
				{
                    case 1:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 2:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 3:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 4:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 5:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                        break;
                    default:
                        $all_Rating = 'Not Rated';
                        break;
                }
                $rattingpercent1 = $Rating * 20;
                $str1 = '<div class="star-inactive"><div class="star-active" style="width:' . $rattingpercent1 . '%;"></div></div>';
                $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
                //  $supplierName = (empty($record['spFName'])) ? 'N/A' : $record['spFName'] . '&nbsp;' . $record['spLName'];
                $title = (empty($record['title'])) ? 'N/A' : $record['title'];
                $feedback = (strlen($record['feedback']) > 20) ? substr($record['feedback'], 0, 20) . '...' : $record['feedback'];
                $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
                $reviews .= '
				 <li>
                    <div class="customer-review_wrap">
                       <div class="reviewer-img">' . $image .'                                                    
							 ';
				if ($record['abused'] == 1) 
				{
                    $reviews .= '<span style="color:#f15922;font-style: italic;">Moderated by Admin</span>';
                }
                if ($record['feedback_reply']) 
				{
                    $reviews .= '<span style="color:rgb(138, 138, 138);
                            "  ><i> Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</i></span>';
                }
               $reviews .= '</div>
                                                <div class="customer-content-wrap">
                                                    <div class="customer-content">
                                                        <div class="customer-review">
                                                            <h6>' . $record['title'] . '</h6>
                                                            <p><a href="' . HOME_PATH . $record['quick_url'] . '">' . substr($record['pdtitle'], 0, 60) . '</a></p>
                                                        </div>
                                                        <div class="customer-rating">' . $str1 . '</div>
														
                                                    </div>
                                                    <p class="customer-text">' . ((empty($record['pros'])) ? 'N/A' : $pros) . '</p>
                                                    <div class="like-btn mar-top-40">
                                                         ' . $commenter . '
                                                        <a href="javascript:void(0)" class="rate-review float-right read_more">Read more</a>
                                                    </div>
                                               
                                            
                                          ';
                
                
                if ($record['abused_request'] == 0 || $record['abused'] == 1) 
				{
                    $reviews .= '<div style="display: block;" class="reads_more col-md-12 padding_none">
							<div class="css_apply row" style="margin:22px 0px;">';
 

                    $reviews .= '     <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="padding_none"><strong style="font-size:15px;">Pros</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="padding_none"><strong style="font-size:15px;">Cons</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_left">' . $record['feedback'] . '</div> 
                                    </div> ';

                    if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                        $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                        $reviews .= '<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_none">
            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-top:22px;color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a>
                            </div>';
                    } else if ($record['abused'] == 1) {
                        $reviews .= '';
                    } else {
                        $reviews .= '<p style="margin-top:22px;color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
                    }


                    if ($record['Admin_FINAL_REPLY']) {
                        $record['Admin_FINAL_REPLY'] = str_ireplace("\n", '<br>', $record['Admin_FINAL_REPLY']);
                        $reviews .= ' <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left alert alert-warning" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px;font-style: italic;">Moderators Comment: </strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><i>' . $record['Admin_FINAL_REPLY'] . '</i></div> 
                                    </div> ';
                    }



                    if ($record['feedback_reply']) {
                        $record['feedback_reply'] = str_ireplace("\n", '<br>', $record['feedback_reply']);
                        $reviews .= ' <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left alert alert-info" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px;font-style: italic;">Reply from  ' . substr($record['pdtitle'], 0, 60) . '</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><i>' . $record['feedback_reply'] . '</i></div> 
                                    </div> ';
                    }

                    $reviews .= '<div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
                                        <table class="table table-bordered table_bord">                   
                                            <thead>
                                                <tr>
                                                    <th width="" class="th_text">Categories</th>
                                                    <th width="" class="th_text">Rating</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <tr>
                                                    <td>Quality of Care</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['quality_of_care']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Caring/Helpful Staff</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['caring_staff']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Responsive Management</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['responsive_management']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>Trips/Outdoor Activities</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['trips_outdoor_activities']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Indoor Entertainment</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['indoor_entertainment']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>Social Atmosphere</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['social_atmosphere']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enjoyable Food</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['enjoyable_food']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>OVERALL RATING</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['overall_rating']) . '</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_left" style=" margin-bottom: 10px;"><strong style="font-size:15px">How do you know about this place?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_left">' . ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) . '</div> 
                                    </div> 

                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_left" style=" margin-bottom: 10px;"><strong style="font-size:15px">I have lived at / visited this facility for</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_left">' . ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_left" style=" margin-bottom: 10px;"><strong style="font-size:15px">I last lived at / visited this facility</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_left">' . (($record['last_visit'] == '') ? 'N/A' : $lastVisit[$record['last_visit']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Would you recommend this facility to a friend?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_left">' . (($record['recommended'] == '') ? 'N/A' : $recommended[$record['recommended']]) . '</div> 
                                    </div>                    
                                </div>';
                    $reviews .= '</div><div class="col-md-6"></div></div>';
                }
                if ($record['abused'] == 1) {
                    $reviews .= '<p style="color:#f15922;font-style: italic;">Moderated by Admin</p>';
                } else if ($record['abused_request'] == 1) {
                    $reviews .= '<p style="color:#f15922;font-style: italic;" >This comment has been flagged for moderation</p>';
                }
                $reviews .= '</div> </div></li>';
            }
        }
        $pagingCount = $count1;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $reviews .= get_sp_pagination_link($paginationCount, 'reviews', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $reviews = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $reviews;
    ?>
    <script>
                $(".fancybox").fancybox();
                $('.reads_more').hide();
                $('.read_more').click(function() {
        $(this).siblings('.feedback').toggle();
                //alert($(this).text());
                $(this).text() == 'Hide' ? $(this).text('Read more') : $(this).text('Hide');
                //$(".feedback").html("")
                // $(".css_apply").css({'background-color': '#e5eecc', 'border': 'solid 1px #c3c3c3', 'padding': '10px', 'width': '50%'});
                $(this).parent().next('div.reads_more').slideToggle("slow", function() {
        //$(this).text('Read More');
        });
        });</script>
    <?php
    die;
}

function review_listold() 
{
    // function getReviews() {
    global $db;
    $query = "SELECT * FROM " . _prefix('reviews') . " WHERE status=1 && deleted= 0 ORDER BY id DESC ";
    $resArt = $db->sql_query($query);
    $count = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getReviews = "SELECT * FROM " . _prefix('reviews') . " WHERE status=1 && deleted= 0 ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $dataReviews = $db->sql_fetchrowset($resReviews);
    $reviews = '';
//    if ($count > 0) {
//        foreach ($dataReviews as $review) {
//            $reviews .= '<div class="col-md-2 col-sm-2 col-xs-12 pdding_none" style="margin-bottom:2%;"><img src="images/img_project.jpg" alt=""></div>
//                    <div class="col-md-10 col-sm-10 col-xs-12 pdding_right padding_left_col">
//                        <h3 class="hedding_three">“' . $review['title'] . '�?</h3>
//                        <p><img src="images/star.jpg" alt=""></p>
//                        <p class="margin_bottom">' . $review['short_description'] . '</p>
//                    </div>
//                    <div class="espesar"></div>';
//        }
//    }
//    return $reviews;
//}
//    global $db;
//    $query = "SELECT page.*, concat(user.first_name,' ',user.last_name) AS editor FROM " . _prefix('pages') . " AS page LEFT JOIN " . _prefix('users') . " AS user ON page.created_by=user.id  WHERE page.status=1 && page.deleted= 0 ORDER BY page.id DESC ";
//    $resArt = $db->sql_query($query);
//    $count = $db->sql_numrows($resArt);
//    $i = 1;
//    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
//        $id = $_POST['pageId'];
//        $i = $i + PAGE_PER_NO * $id;
//    } else {
//        $id = '0';
//        $i = 1;
//    }
//    $pageLimit = PAGE_PER_NO * $id;
//    $getArticles = "SELECT page.*, concat(user.first_name,' ',user.last_name) AS editor FROM " . _prefix('pages') . " AS page LEFT JOIN " . _prefix('users') . " AS user ON page.created_by=user.id  WHERE page.status=1 && page.deleted= 0 ORDER BY page.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
//    $resArticles = $db->sql_query($getArticles);
//    $count = $db->sql_numrows($resArticles);
//    $dataArticles = $db->sql_fetchrowset($resArticles);
    if ($count > 0) {
        foreach ($dataReviews as $article) {
            $abuse = ($article['abused'] == 1) ? 'disabused' : 'abused';
            $short_description = (strlen($article['short_description']) > 150) ? substr($article['short_description'], 0, 150) . '...' : $article['short_description'];
            if ($article['banner_image'] == '') {
                $image = '';
            } else {
                if (file_exists(DOCUMENT_PATH . 'admin/files/pages/feature_image/' . $article['banner_image'])) {
                    $href = MAIN_PATH . '/files/pages/feature_image/' . $article['banner_image'];
                    $image = '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col_lg_bg">
                        <a class="pull-left img_post" href="#"><img class="img-responsive" src="' . $href . '"></a>
                    </div>';
                } else {
                    $image = '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 col_lg_bg">
                        <a class="pull-left img_post" href="#"><img class="img-responsive" src="images/img_project.jpg"></a>
                    </div>';
                }
            }
            $output .= '<div class="col-md-12">
                <div class="media">
                    <h2 class="col_h2">' . $article['page_title'] . ' </h2>
                    <div class="posted">Posted on : <span>' . $article['created'] . ' </span><a href="#">' . $article['editor'] . '</a></div>
                    ' . $image . '
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col_lg_bg">
                        <div class="media-body">
                            <p>' . $short_description . '</p>
                                <p><img src="images/star.jpg" alt=""></p>
                                </div>';
            if ($article['abused'] == 0) {
                $output .= '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
            <span class="abuse_div abuse-' . $article['id'] . '"><a href="javascript:void(0);" id="reviews-' . $article['id'] . '-' . $article['abused'] . '" class="inner_btn abuse">Set Abuse</a>
                     </div>';
            } else {
                $output .= '<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left "><span class="abuse_div inner_btn">Abused</span></div>';
            }
            $output .= '</div>
                </div>
            </div>';
        }

        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'articles', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script>
                $('.abuse').live('click', function() {
        var details = $(this).attr('id').split('-');
                var table = details[0];
                var id = details[1];
                var abuse = details[2];
                $.ajax({
                type: 'POST',
                        url: '<?php echo HOME_PATH; ?>ajaxFront.php',
                        data: {action: 'abuse', id: id, table: table, abuse: abuse},
                        success: function(response) {
                        $('.abuse-' + id).html($.trim(response));
                        }
                });
        });</script>
    <?php
    die;
}

function manage_product($uid, $search_input, $search_type) {
    global $db;
    $condition = "where prd.deleted = 0 AND prd.status=1 AND prd.id='" . $_SESSION['proId'] . "'";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' && prd.title like "%' . $search_input . '%" ';
                break;
            case "keyword":
                $condition .= ' && prd.keyword like "%' . $search_input . '%" ';
                break;
            case "author":
                $condition .= ' && cit.title like "%' . $search_input . '%" ';
                break;
            case "facility_type":
                $condition .= ' && srv.name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && ( prd.title like "%' . $search_input . '%" OR cit.title like "%' . $search_input . '%" OR srv.name like "%' . $search_input . '%" OR prd.keyword like "%' . $search_input . '%"  )';
        }
    }
    $query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC ";
    //  $query = "SELECT id  FROM " . _prefix("products") . " Where deleted=0 AND supplier_id=" . $uid;
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT prd.id,prd.address_city, prd.address, prd.title, prd.description, prd.keyword,prd.pro_extra_info, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
//        $query = "SELECT id, title, description, image, status FROM " . _prefix("products") . "   "
//                . "$condition ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
//    prd($data);
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord ">
                    <thead>
                    <tr>
                    <th class="col-md-1">S.No</th>
                    <th class="col-md-2">Title</th>
                    <th class="col-md-3">Address</th>
                   <th class="col-md-2">City</th>
                   <th class="col-md-2">Extra Info</th>
                    <th class="col-md-2">Action</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['address']);
            //$description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . ' .</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $description . '</td>';
            $output .= '<td>' . $record['address_city'] . '</td>';
            if (empty($record['pro_extra_info'])) {
                $output .= '<td><a href=' . HOME_PATH . 'supplier/extraInfo?edit&pro_id=' . base64_encode($record['id']) . '&id=' . base64_encode($record['pro_extra_info']) . '>Add Extra Info</a></td>';
            } else {
                $output .= '<td><a href=' . HOME_PATH . 'supplier/extraInfo?edit&pro_id=' . base64_encode($record['id']) . '&id=' . base64_encode($record['pro_extra_info']) . '>Edit Extra Info</a></td>';
            }

            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="products-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
//                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH . 'supplier/addProduct?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-products-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output .= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $output .= '</tbody></table>';
        $paging_query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC ";
        // $paging_query = "SELECT *  FROM " . _prefix("products") . " Where deleted=0 AND supplier_id=" . $uid;
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'product', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script type="text/javascript">
                $(".image").colorbox({
        width: '600px',
                height: '500px'
        });
                $(document).ready(function() {
        $(".videoPlayer").fancybox({
        'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'speedIn': 600,
                'speedOut': 200
        });
                $(".fancybox").fancybox();
                var path = '<?php echo HOME_PATH; ?>ajaxFront.php';
                $('.status').live('click', function() {
        var details = $(this).attr('id').split('-');
                var table = details[0];
                var id = details[1];
                var status = details[2];
                //return false;
                $.ajax({
                type: 'POST',
                        url: path,
                        data: {action: 'status', id: id, table: table, status: status},
                        success: function(response) {
                        $('.status-' + id).html($.trim(response));
                        }
                });
        });
                /*
                 * Code for Delete
                 */

                $('.delete').live('click', function() {
        var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
        var details = $(this).attr('id').split('-');
                var table = details[1];
                var id = details[2];
                $.ajax({
                type: 'POST',
                        url: path,
                        data: {action: 'delete', id: id, table: table},
                        success: function() {
                        $('.row_' + id).hide();
                        }
                });
        }
        });
        });</script>

    <?php
    die;
}

function manage_productviews($uid) {
    global $db;
    $condition = " where prd.deleted = 0 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' && prd.title like "%' . $search_input . '%" ';
                break;
            case "keyword":
                $condition .= ' && prd.keyword like "%' . $search_input . '%" ';
                break;
            case "author":
                $condition .= ' && cit.title like "%' . $search_input . '%" ';
                break;
            case "facility_type":
                $condition .= ' && srv.name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && ( prd.title like "%' . $search_input . '%" OR cit.title like "%' . $search_input . '%" OR srv.name like "%' . $search_input . '%" OR prd.keyword like "%' . $search_input . '%"  )';
        }
    }
    $query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC ";
    //  $query = "SELECT id  FROM " . _prefix("products") . " Where deleted=0 AND supplier_id=" . $uid;
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT prd.id, prd.title, prd.description, prd.keyword,prd.pro_extra_info, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
//        $query = "SELECT id, title, description, image, status FROM " . _prefix("products") . "   "
//                . "$condition ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord ">
                    <thead>
                    <tr>
                    <th class="col-md-1">S.No</th>
                    <th class="col-md-2">Title</th>
                    <th class="col-md-2">Description</th>
                    <th class="col-md-2">Number of Reviews</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $key => $record) {

            $description = strip_tags($record['description']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $totalviews = overAllRatingProductCount($record['id']);
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . ' .</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $description . '</td>';

            $output .= '<td><a href="' . HOME_PATH . 'search/viewReview?id=' . md5($record['id']) . '" target=_blank><i class="fa fa-users"></i>&nbsp;' . $totalviews . '</a></td>';

            $output .= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $output .= '</tbody></table>';
        $paging_query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC ";
        // $paging_query = "SELECT *  FROM " . _prefix("products") . " Where deleted=0 AND supplier_id=" . $uid;
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'productviews', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>


    <?php
    die;
}

function manage_order($uid) {
    global $db;

    $query = "SELECT " . _prefix("payments_test") . ".payid  FROM " . _prefix("payments_test") . "," . _prefix("direct_credit") . " Where " . _prefix("payments_test") . ".items =  " . _prefix("direct_credit") . ".our_order_number AND " . _prefix("direct_credit") . ".deleted=0 AND " . _prefix("direct_credit") . ".product_id='" . $_SESSION['proId'] . "' ORDER BY date DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    //echo $query;
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT " . _prefix("payments_test") . ".*  FROM " . _prefix("payments_test") . "," . _prefix("direct_credit") . " Where " . _prefix("payments_test") . ".items =  " . _prefix("direct_credit") . ".our_order_number AND " . _prefix("direct_credit") . ".deleted=0 AND " . _prefix("direct_credit") . ".product_id='" . $_SESSION['proId'] . "' ORDER BY date DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord ">
                    <thead>
                    <tr>
                    <th class="col-md-4">Invoice (click to open)</th>
                    <th class="col-md-1">Amount</th>
                    <th class="col-md-1">Method</th>
                    <th class="col-md-3">Action</th>
                    <th class="col-md-2">Status</th>
                    <th class="col-md-2">DATE PAID</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $key => $records) {
//            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
//            $status = ($record['status'] == 1) ? 'Active' : 'InActive';


            $output .= '<tr>';



            $invoicenumber = $records['items'];
            if (!is_numeric($invoicenumber)) {
                $invoicenumber = 0;
            }
            if ($invoicenumber < 100000) {
                $invoicenumber = 0;
            }

            /* print_r($itemarray); 
              echo "<br />"; */
            if ($records['token'] != '') {
                $paymentmethod = "PAYPAL";
            } else {
                $paymentmethod = "Direct Credit";
            }


            /* echo "<br />";
              print_r($jarray);
             */

            // $finalitem = implode(", ", $jarray);

            $output .= '<td class="col-md-4 text-left"><a href="/modules/supplier/process_display_invoice.php?on=' . $invoicenumber . '" target="_blank">' . $invoicenumber . '</a></td>
                        <td class="col-md-1">$<span id="total">' . $records['amount'] . '</span></td>
                        <td class="col-md-1">' . $paymentmethod . '</td>
                        <td class="col-md-3 text-center">';
            if ($records['status'] == 'Active') {
                $output .= '<a href="/modules/supplier/cancel-profile.php?token=' . $records['token'] . '&profileid=' . $records['profileid'] . '&amount=' . $records['amount'] . '&on=' . $invoicenumber . '" onclick="return confirm(\'are you sure want to Cancel this Subscription\')">Cancel</a>';
            } else {
                $output .= "Cancelled on " . $records['canceldate'];
            }
            $output .= '</td>
                        <td class="col-md-2 text-center">' . $records['status'] . '</td>
                        <td class="col-md-2 text-center">' . $records['date'] . '</td>
                    </tr>';

            $i ++;
        }
        $output .= '</tbody></table>';
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'order', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center text-danger">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script type="text/javascript">
                $(".image").colorbox({
        width: '600px',
                height: '500px;'
        });
                $(document).ready(function() {

        $(".fancybox").fancybox();
                var path = '<?php echo HOME_PATH; ?>ajaxFront.php';
        });</script>

    <?php
    die;
}

function manage_adsBookingList($uid, $pro_id, $proName) {
    global $db;
    $condition = "WHERE bg.deleted = 0 AND bg.payment_status=1 AND bg.supplier_id=$uid AND bg.pro_id=$pro_id ";
    $query = "SELECT bg.id FROM " . _prefix("ads_masters") . " AS bg "
            . "  LEFT JOIN " . _prefix("services") . " AS service ON service.id=bg.service_category_id "
            . " $condition ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT bg.id,bg.status, bg.title,bg.from_date,bg.to_date,bg.approve_status,bg.image,service.name AS cat_title  FROM " . _prefix("ads_masters") . " AS bg "
            . "  LEFT JOIN " . _prefix("services") . " AS service ON service.id=bg.service_category_id "
            . " $condition  ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);

// prd($data['0']['cat_title']);
    if ($count > 0) {
        $output = '<table class="table table-striped table-hover table-bordered">
                    <thead><tr>
                                 <th class="col-md-1 text-center">S.No</th>
                                 <th class="col-md-2 text-center">Title</th>
                                 <th class="col-md-1 text-center">From Date</th>
                                 <th class="col-md-1 text-center">To Date</th>
                                 <th class="col-md-2 text-center">Facility Name</th>
                                 <th class="col-md-1 text-center">Image</th>
                                 <th class="col-md-1 text-center">Approve Status</th>
                                 <th class="col-md-2 text-center">Action</th>
                   </tr></thead>
                    <tbody>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $approve_status = ($record['approve_status'] == 1) ? 'Approved' : 'Disapproved';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $approved = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 40) ? substr($description, 0, 40) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $img1 = $approve_status . ' <img src="' . $approved . '" title="' . $approve_status . '" />';
            $ImageList = MAIN_PATH . 'files/ads_image/thumb_' . $record['image'];
            $ImageView = MAIN_PATH . 'files/ads_image/' . $record['image'];

            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . ' .</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['from_date'])) . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['to_date'])) . '</td>';
            $output .= '<td>' . $proName . '</td>';
            $fileImage = ADMIN_PATH . 'files/ads_image/thumb_' . $record['image'];
            if (file_exists($fileImage)) {
                $output .= !empty($record['image']) ? '<td align="center"><a  class="fancybox" title="' . $record['image'] . '" href="' . $ImageView . '"><img  title="View Image"  src="' . $ImageList . '"></a></td>' : '';

//$output .=!empty($record['image'])? '<td><a  class="image" href="' . HOME_PATH_URL . 'popup.php?adsImage&id=' . md5($record['id']) . '"><img  title="View image" src="' . MAIN_PATH . '/files/ads_image/thumb_' . $record['image'] . '"></a><br/><a href="' . MAIN_PATH . '/download.php?type=ads_image&file=' . $record['image'] . '">Download<i class="fa fa-download"></i></a></td>' : '';
            } else {
                $output .= '<td><img  title="View image" src="' . HOME_PATH . 'images/notavail.gif"></a></td>';
            }
            $output .= !empty($approve_status) ? '<td>' . $approve_status . '</td>' : '<td>N/A</td>';
            $output .= '<td class="text-center"><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="ads_masters-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
//                        . '<span style="margin-right:5px;"><a href=' . HOME_PATH . 'supplier/editAddsBooking?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-ads_masters-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output .= '</td>';
            $output .= '</tr>';
            $i ++;
        }

        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 8;
            $output .= get_sp_pagination_link($paginationCount, 'adsBookingList', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo HOME_PATH; ?>css/pagination.css" media="screen" />
    <script>
                $(document).ready(function() {
        $('.load').click(function() {
        var value = $(this).attr('id').split('-');
                $('#loading-' + value[1]).show();
        });
                $(".fancybox").fancybox();
                var path = '<?php echo HOME_PATH; ?>ajaxFront.php';
                $('.status').live('click', function() {
        var details = $(this).attr('id').split('-');
                var table = details[0];
                var id = details[1];
                var status = details[2];
                //return false;
                $.ajax({
                type: 'POST',
                        url: path,
                        data: {action: 'status', id: id, table: table, status: status},
                        success: function(response) {
                        $('.status-' + id).html($.trim(response));
                        }
                });
        });
                /*
                 * Code for Delete
                 */

                $('.delete').live('click', function() {
        var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
        var details = $(this).attr('id').split('-');
                var table = details[1];
                var id = details[2];
                $.ajax({
                type: 'POST',
                        url: path,
                        data: {action: 'delete', id: id, table: table},
                        success: function() {
                        $('.row_' + id).hide();
                        }
                });
        }
        });
        });</script>
    <?php
    die;
}

function viewReview($pid) {
    global $db;
    $query = " SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " WHERE md5(fdbk.product_id) ='" . $pid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC";
    $resArt = $db->sql_query($query);
    $count1 = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getReviews = " SELECT fdbk.*, IFNULL(fdbk.title,'N/A') as title , pds.title as pdtitle , pds.id AS pImage, cs.user_name AS csUsername, pds.quick_url FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . " WHERE md5(fdbk.product_id) ='" . $pid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $data = $db->sql_fetchrowset($resReviews);
    // echo"<pre>";
    // print_r($data) ;
    // echo"<pre>";
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSum = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';
    $qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
    $resultKnowMe = $db->sql_query($qryKnowMe);
    $knowMeData = $db->sql_fetchrowset($resultKnowMe);
    foreach ($knowMeData as $key => $record) {
        $knowMe[$record['id']] = $record['title'];
    }
    $qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
    $resultDuration = $db->sql_query($qryDuration);
    $durationVisitData = $db->sql_fetchrowset($resultDuration);
    foreach ($durationVisitData as $key => $record) {
        $durationVisit[$record['id']] = $record['title'];
    }
    $lastVisit = array('', 'Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
    $recommended = array('No', 'Yes', 'Not sure');





    if ($count > 0) {
        foreach ($data as $key => $record) {
            if ($record['feedback'] != '') {
                if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) {
                    $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']) . '" alt="product image" /></a>';
                } else {
                    $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'images/no_image_available.png" alt="" /></a>';
                }
                $know_me_id = $record['know_me'];

                if ($know_me_id == 1) {
                    $know_me_title = "Resident";
                }
                if ($know_me_id == 2) {
                    $know_me_title = "Family member / Friend";
                }
                if ($know_me_id == 3) {
                    $know_me_title = "Staff Member";
                }
                if ($know_me_id == 4) {
                    $know_me_title = "Visitor";
                }
                if ($know_me_id == 0 || $know_me_id > 4) {
                    $know_me_title = "Unspecified";
                }
                if ($record['visit_duration'] == 1) {
                    $visit_duration = "< 10 days / visits";
                }
                if ($record['visit_duration'] == 2) {
                    $visit_duration = "10 - 100 days / visits";
                }
                if ($record['visit_duration'] == 3) {
                    $visit_duration = "> 100 days / visits";
                }
                if ($record['visit_duration'] == 0 || $record['visit_duration'] > 3) {
                    $visit_duration = "Unspecified";
                }
                $last_visit = $record['last_visit'];

                $feedback_created = $record['created'];
                $feedback_date = date("Y-m-d", strtotime($feedback_created));
                $feedback_fetch_date = date("d", strtotime($feedback_date));
                $feedback_fetch_month = date("m", strtotime($feedback_date));
                $feedback_monthName = date("M", mktime(0, 0, 0, $feedback_fetch_month, 10));
                $feedback_fetch_year = date("Y", strtotime($feedback_date));
                $TotalRating = $record['overall_rating'];
                $Rating = $TotalRating;
                $overallRating = round($Rating);
                switch ($overallRating) {

                    case 1:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 2:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 3:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 4:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 5:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                        break;
                    default:
                        $all_Rating = 'Not Rated';
                        break;
                }
                $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
                //  $supplierName = (empty($record['spFName'])) ? 'N/A' : $record['spFName'] . '&nbsp;' . $record['spLName'];
                $title = (empty($record['title'])) ? 'N/A' : $record['title'];
                $feedback = (strlen($record['feedback']) > 20) ? substr($record['feedback'], 0, 20) . '...' : $record['feedback'];
                $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
                $reviews .= '<div class="col-md-12"><div class="review-content"><div class="col-xs-2 m-hide"><p class="view-review-font review-by">Reviewed by:<br>' . $commenter . '<br>' . $feedback_monthName . ' ' . $feedback_fetch_year . '</p><ul class="view-review-font review-img-list"><li>' . $know_me_title . '</li><li>' . $visit_duration . '</li></ul></div>
                    <div class="col-md-10 col-sm-10 col-xs-12 pdding_right padding_left_col">
                        <h3 class="hedding_three">' . $record['title'] . '</h3>
                        <p style="margin:0" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">' . $all_Rating . '<span class="text-hide" content=' . $overallRating . ' data-rating=' . $overallRating . ' itemprop="ratingValue">' . $overallRating . ' stars</span></p>
                            <p><span style="color:rgb(138, 138, 138);
                            "  >Product/Service Name:&nbsp;<a href="' . HOME_PATH . $record['quick_url'] . '">' . substr($record['pdtitle'], 0, 60) . '</a></span></br>
                                <!--span style="color:rgb(138, 138, 138);
                            " >Supplier:&nbsp;' . $supplierName . '</span><br/-->
                            <span style="color:rgb(138, 138, 138);
                            "  >';

                if ($record['feedback_reply']) {
                    $reviews .= '<br><span style="color:rgb(138, 138, 138);
                            "  ><i> Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</i></span>';
                }
                $reviews .= '</p>';
                if ($record['abused_request'] == 0 || $record['abused'] == 1) {
                    $reviews .= '<p><span class="margin_bottom feedback"  itemprop="reviewBody">' . ((empty($record['pros'])) ? 'N/A' : $pros) . '</span><br><a href="javascript:void(0)" class="read_more">Read more</a></p>'
                            . '<div style="display: none;" class="reads_more col-md-12 padding_none"><div class="css_apply row" style="border-top:1px solid #ddd; border-bottom:1px solid #ddd; margin:22px 0px;">';
//                    if (isset($record['feedback'])) {
//
//                        $reviews.= $record['feedback'] . '<br/>';
//                    } else {
//                        $reviews.= 'No record found <br/>';
//                    }

                    $reviews .= '     <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Pros</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Cons</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . $record['feedback'] . '</div> 
                                    </div> ';

                    if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                        $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                        $reviews .= '<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-top:22px;color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a>
                            </div>';
                    } else if ($record['abused'] == 1) {
                        $reviews .= '<div style="margin-bottom:0;margin-top:22px;" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg text-center"><span style="color:#f15922;font-style: italic;">Moderated by Admin</span></div>';
                    } else {
                        $reviews .= '<p style="margin-top:22px;color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
                    }


                    if ($record['Admin_FINAL_REPLY']) {
                        $record['Admin_FINAL_REPLY'] = str_ireplace("\n", '<br>', $record['Admin_FINAL_REPLY']);
                        $reviews .= ' <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left alert alert-warning" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px;font-style: italic;">Moderators Comment: </strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><i>' . $record['Admin_FINAL_REPLY'] . '</i></div> 
                                    </div> ';
                    }




                    if ($record['feedback_reply']) {
                        $record['feedback_reply'] = str_ireplace("\n", '<br>', $record['feedback_reply']);
                        $reviews .= ' <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left alert alert-info" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px;font-style: italic;">Reply from  ' . substr($record['pdtitle'], 0, 60) . '</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><i>' . $record['feedback_reply'] . '</i></div> 
                                    </div> ';
                    }

                    $reviews .= '                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
                                        <table class="table table-bordered table_bord">                   
                                            <thead>
                                                <tr>
                                                    <th width="" class="th_text">Categories</th>
                                                    <th width="" class="th_text">Rating</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <tr>
                                                    <td>Quality of Care</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['quality_of_care']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Caring/Helpful Staff</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['caring_staff']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Responsive Management</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['responsive_management']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>Trips/Outdoor Activities</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['trips_outdoor_activities']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Indoor Entertainment</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['indoor_entertainment']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>Social Atmosphere</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['social_atmosphere']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enjoyable Food</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['enjoyable_food']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>OVERALL RATING</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['overall_rating']) . '</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">How do you know about this place?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) . '</div> 
                                    </div> 

                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I have lived at / visited this facility for</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I last lived at / visited this facility</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . (($record['last_visit'] == '') ? 'N/A' : $lastVisit[$record['last_visit']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">Would you recommend this facility to a friend?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . (($record['recommended'] == '') ? 'N/A' : $recommended[$record['recommended']]) . '</div> 
                                    </div>                    
                                </div>';
                    $reviews .= '</div></div><div class="col-md-6"></div></div>';
                }

                if ($record['abused'] == 1) {
                    $reviews .= '<p style="color:#f15922;font-style: italic;">Moderated by Admin</p>';
                } else if ($record['abused_request'] == 1) {
                    $reviews .= '<p style="color:#f15922;font-style: italic;" >This comment has been flagged for moderation</p>';
                }


                $reviews .= '</div>
                    </div>';

                $print_logo = '<div class="clearfix"></div><div class ="review_print">
                     <div class="form-print col-sm-8 col-sm-offset-2 col-xs-12 " id="print_review">
                        <div class="form-logo"><img id="logo-header" src="/images/find_agedadvisor_retirement_villages_agedcare_logo.png" alt="Agedadvisor find the right retirement or restcare facilities" width="auto" height="54">
                            <h2>Reviews of ' . $record['pdtitle'] . '</h2>
                        ' . date("d-m-Y") . '</div>';

                $print_review .= '<div class="col-md-12"><div class="review-content"><div class="col-xs-2 m-hide"><p class="view-review-font review-by">Reviewed by:<br>' . $commenter . '<br>' . $feedback_monthName . ' ' . $feedback_fetch_year . '</p></div><div class="col-md-10 col-sm-10 col-xs-12 pdding_right padding_left_col">
                        <h3 class="hedding_three">' . $record['title'] . '</h3>
                        <p style="margin:0" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">' . $all_Rating . '<span class="text-hide" content=' . $overallRating . ' data-rating=' . $overallRating . ' itemprop="ratingValue">' . $overallRating . ' stars</span></p>
                            <p><span style="color:rgb(138, 138, 138);
                            "  >Product/Service Name:&nbsp;<a>' . substr($record['pdtitle'], 0, 60) . '</a></span></br>
                                <!--span style="color:rgb(138, 138, 138);
                            " >Supplier:&nbsp;' . $supplierName . '</span><br/-->
                            <span style="color:rgb(138, 138, 138);
                            "  >';
                $print_review .= '<div class="row">
                           <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                           <div class="row">
                                        <strong style="font-size:15px; float:left; margin:0 35px 0 15px;">Pros</strong>
                                        <div class="col-md-10 col-sm-10 col-xs-10 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                        </div>
                                        </div>
                                    </div> 
                                    <div class="row">                                
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                  <div class="row">
                                  <strong style="font-size:15px; float:left; margin:0 30px 0 15px;">Cons</strong> 
                                        <div class="col-md-10 col-sm-10 col-xs-10 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> </div>
                                      
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> </div>
                                        <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . $record['feedback'] . '</div> </div>
                                    </div></div></div></div></div>';
            }
        }
        $print_review .= '<div class="col-md-12 col-sm-12 col-xs-12" style="text-align:center"><h5> <i class="fa fa-copyright" aria-hidden="true"></i> 2016. Aged Advisor New Zealand All rights reserved<h5></div></div></div>';
        $pagingCount = $count1;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $reviews .= get_spcheck_pagination_link($paginationCount, 'viewReviews', $data, $colspan, $_REQUEST['pageId']);
        }
    } else {
        $reviews = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $reviews;
    echo $print_logo;
    echo $print_review;
    ?>

    <script>
                $(".fancybox").fancybox();
                $('.reads_more').hide();
                $('.read_more').click(function() {
        $(this).siblings('.feedback').toggle();
                //alert($(this).text());
                $(this).text() == 'Hide' ? $(this).text('Read more') : $(this).text('Hide');
                //$(".feedback").html("")
                // $(".css_apply").css({'background-color': '#e5eecc', 'border': 'solid 1px #c3c3c3', 'padding': '10px', 'width': '50%'});
                $(this).parent().next('div.reads_more').slideToggle("slow", function() {
        //$(this).text('Read More');
        });
        });</script>
    <?php
    die;
}

function viewFeedback($pid) {
    global $db;
    $query = " SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " WHERE md5(fdbk.product_id) ='" . $pid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC";
    $resArt = $db->sql_query($query);
    $count1 = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getReviews = " SELECT fdbk.*, IFNULL(fdbk.title,'N/A') as title , pds.title as pdtitle , pds.image AS pImage, cs.user_name AS csUsername FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . " WHERE md5(fdbk.product_id) ='" . $pid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $data = $db->sql_fetchrowset($resReviews);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSum = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';
    $qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
    $resultKnowMe = $db->sql_query($qryKnowMe);
    $knowMeData = $db->sql_fetchrowset($resultKnowMe);
    foreach ($knowMeData as $key => $record) {
        $knowMe[$record['id']] = $record['title'];
    }
    $qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
    $resultDuration = $db->sql_query($qryDuration);
    $durationVisitData = $db->sql_fetchrowset($resultDuration);
    foreach ($durationVisitData as $key => $record) {
        $durationVisit[$record['id']] = $record['title'];
    }
    $lastVisit = array('', 'Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
    $recommended = array('No', 'Yes', 'Not sure');
    if ($count > 0) {
        foreach ($data as $key => $record) {
            if ($record['feedback'] != '') {

                $reviews .= '<div class="load col-md-12 col-sm-12 col-xs-12 thumbnail" id="' . md5($record['id']) . '"><div col-md-12 col-sm-12 col-xs-12 style="padding:20px;">
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Pros</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Cons</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . $record['feedback'] . '</div> 
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
                                        <table class="table table-bordered table_bord">                   
                                            <thead>
                                                <tr>
                                                    <th width="" class="text-left">Categories</th>
                                                    <th width="" class="text-left">Rating</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <tr>
                                                    <td>Quality of Care</td>
                                                    <td>' . getStars($record['quality_of_care']) . '</td>
                                                </tr>
                                                <tr>
                                                    <td>Caring/Helpful Staff</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['caring_staff']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Responsive Management</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['responsive_management']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Trips/Outdoor Activities</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['trips_outdoor_activities']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Indoor Entertainment</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['indoor_entertainment']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Social Atmosphere</td>
                                                    <td class"text-center"> 
                                                        <div >' . getStars($record['social_atmosphere']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enjoyable Food</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['enjoyable_food']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>OVERALL RATING</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['overall_rating']) . '</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">How do you know about this place?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) . '</div> 
                                    </div> 

                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I have lived at / visited this facility for</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I last lived at / visited this facility</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['last_visit'])) ? 'N/A' : $lastVisit[$record['last_visit']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">Would you recommend this facility to a friend?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['recommended'])) ? 'N/A' : $recommended[$record['recommended']]) . '</div> 
                                    </div>                    
                                </div></div>';
                //$reviews.= '</div><div class="col-md-6"></div></div></div>';
            }
//                if ($record['abused'] == 0 && $record['abused_request'] == 0) {
//                    if (isset($_SESSION['csId']) || isset($_SESSION['userId'])) {
//                        $path_url = HOME_PATH . 'modules/review/report_reason.php?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
//                        $reviews .='<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
//            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-bottom:0;
//                            margin-top:22px;
//                            color:#f15922;" class="fancybox" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '" class=" abuse">Report this comment</a>
//                            </div>';
//                    } else {
////                        $msg = "First you login then Report a comment";
////                        $_SESSION['msg'] = $msg;
//                        $path_url = HOME_PATH . 'login?return=true&msgR='.base64_encode('First you login then Report a comment');
//                        $reviews .='<div class = "col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
//                            <span class = "abuse_div abuse-' . $record['id'] . '"><a style = "margin-bottom:0;margin-top:22px;color:#f15922;" href = "' . $path_url . '" id = "feedbacks-' . $record['id'] . '-' . $record['abused'] . '" class = " abuse">Report this comment</a></span>
//                            </div>';
//                    }
//                } else if ($record['abused'] == 1) {
//                    $reviews .='<div style="margin-bottom:0;margin-top:22px;" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg  "><span style="color:#f15922;font-style: italic;">Moderated by Admin</span></div>';
//                } else {
//                    $reviews .='<p style="margin-bottom:0;margin-top:22px;color:#f15922;font-style: italic;">This comment has been flagged for moderation</p>';
//                }
            $reviews .= '</div>
                    </div>';
        }
    } else {
        $reviews = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $reviews;
    ?>
    <script>
                $(function() {
                $("div.load").lazyload({
                event: "sporty"
                });
                });
                $(window).bind("load", function() {
        var timeout = setTimeout(function() {
        $("div.load").trigger("sporty")
        }, 5000);
        });</script>
    <?php
    die;
}

function viewFeedbackdDetail($fid) {
    global $db;
    $query = " SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " WHERE md5(fdbk.id) ='" . $fid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC";
    $resArt = $db->sql_query($query);
    $count1 = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    /*  $getReviews = " SELECT fdbk.*, IFNULL(fdbk.title,'N/A') as title , pds.title as pdtitle , pds.image AS pImage, cs.user_name AS csUsername FROM " . _prefix("feedbacks") . " AS fdbk   "
      . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
      . " LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
      . " WHERE md5(fdbk.id) ='" . $fid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO; */

    $getReviews = " SELECT fdbk.*, IFNULL(fdbk.title,'N/A') as title , pds.title as pdtitle , pds.image AS pImage, cs.user_name AS csUsername FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . " WHERE md5(fdbk.id) ='" . $fid . "' AND fdbk.deleted = 0 ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $data = $db->sql_fetchrowset($resReviews);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSum = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';
    $qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
    $resultKnowMe = $db->sql_query($qryKnowMe);
    $knowMeData = $db->sql_fetchrowset($resultKnowMe);
    foreach ($knowMeData as $key => $record) {
        $knowMe[$record['id']] = $record['title'];
    }
    $qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
    $resultDuration = $db->sql_query($qryDuration);
    $durationVisitData = $db->sql_fetchrowset($resultDuration);
    foreach ($durationVisitData as $key => $record) {
        $durationVisit[$record['id']] = $record['title'];
    }
    $lastVisit = array('', 'Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
    $recommended = array('No', 'Yes', 'Not sure');
    if ($count > 0) {
        foreach ($data as $key => $record) {
            if ($record['feedback'] != '') {
                //Mark as read that is set read_status field of feedback table to 1
                if ($record['read_status'] == 0) {
                    $UpdateSql = "UPDATE " . _prefix("feedbacks") . " SET read_status =1 WHERE md5(id)='" . $fid . "' ";
                    $updateResult = $db->sql_query($UpdateSql);
                }

                $reviews .= '<div class="load col-md-12 col-sm-12 col-xs-12 thumbnail"><div col-md-12 col-sm-12 col-xs-12 style="padding:20px;">';

                if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                    $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                    $reviews .= '<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-top:0px;color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a>
                            </div>';
                } else if ($record['abused'] == 1) {
                    $reviews .= '<div style="margin-bottom:0;margin-top:0px;" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg text-center"><span style="color:#f15922;font-style: italic;">Moderated by Admin</span></div>';
                } else {
                    $reviews .= '<p style="margin-top:0px;color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
                }





                $reviews .= '     <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Pros</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Cons</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . $record['feedback'] . '</div> 
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
                                        <table class="table table-bordered table_bord">                   
                                            <thead>
                                                <tr>
                                                    <th width="" class="text-left">Categories</th>
                                                    <th width="" class="text-left">Rating</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <tr>
                                                    <td>Quality of Care</td>
                                                    <td>' . getStars($record['quality_of_care']) . '</td>
                                                </tr>
                                                <tr>
                                                    <td>Caring/Helpful Staff</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['caring_staff']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Responsive Management</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['responsive_management']) . '</div>
                                                    </td>
                                                 </tr>
                                                <tr>
                                                   <td>Trips/Outdoor Activities</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['trips_outdoor_activities']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Indoor Entertainment</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['indoor_entertainment']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Social Atmosphere</td>
                                                    <td class"text-center"> 
                                                        <div >' . getStars($record['social_atmosphere']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enjoyable Food</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['enjoyable_food']) . '</div>
                                                    </td>
                                                 </tr>
                                                <tr>
                                                   <td>OVERALL RATING</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['overall_rating']) . '</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">How do you know about this place?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) . '</div> 
                                    </div> 

                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I have lived at / visited this facility for</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I last lived at / visited this facility</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['last_visit'])) ? 'N/A' : $lastVisit[$record['last_visit']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">Would you recommend this facility to a friend?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['recommended'])) ? 'No' : $recommended[$record['recommended']]) . '</div> 
                                    </div>                    
                                </div>';
                //$reviews.= '</div><div class="col-md-6"></div></div></div>';
            }

            if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                $reviews .= '<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-top:22px;color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a>
                            </div>';
            } else if ($record['abused'] == 1) {
                $reviews .= '<div style="margin-bottom:0;margin-top:22px;" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg text-center"><span style="color:#f15922;font-style: italic;">Moderated by Admin</span></div>';
            } else {
                $reviews .= '<p style="margin-top:22px;color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
            }
            $reviews .= '</div></div>
                    </div>';
        }
    } else {
        $reviews = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $reviews;
    ?>
    <script>
                $("a.fancybox").fancybox();
                $(function() {
                $("div.load").lazyload({
                event: "sporty"
                });
                });
                $(window).bind("load", function() {
        var timeout = setTimeout(function() {
        $("div.load").trigger("sporty")
        }, 5000);
        });

    </script>
    <?php
    die;
}
