<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>
  <title><?php echo trim($header_title)?></title>
    <!-- Meta -->
    <meta charset="utf-8">
   <meta http-equiv="X-UA-Compatible" content="IE=5,8,9" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0"/>
    <!-- <meta name="description" content="Read our reviews to find the right retirement or restcare facilities for you or your parents. <?php echo trim($header_description_add)?>"> -->
    <meta name="description" content="<?php echo $header_description_add;?>">
    <meta name="keywords" content="<?php echo trim($header_keyword)?>">
    <meta name="author" content="Aged Advisor">
	<meta name="google-site-verification" content="L9IfRMIaqNvdOUAOJ3QhTd8H2blj-RBNnJiFHKC5_4M" />
	<meta name="google-site-verification" content="L9IfRMIaqNvdOUAOJ3QhTd8H2blj-RBNnJiFHKC5_4M" />
	<meta name="msvalidate.01" content="30B390D4BAB88CB4B9A74D04F42BB68B" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- CSS Global Compulsory -->
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">
     
   
	
  <link href="<?php echo HOME_PATH;?>includes/css/plugin.css" rel="stylesheet" />
    <!-- style CSS -->
    <link href="<?php echo HOME_PATH;?>includes/css/style.css" rel="stylesheet" />
	
	  <link href="<?php echo HOME_PATH;?>assets/css/style-pre.css" rel="stylesheet" />
	  
	 
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo HOME_PATH;?>assets/plugins/bootstrap/css/bootstrap.min.css">color switcher css-->
 <script  src="https://code.jquery.com/jquery-3.0.0.min.js" integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0="  crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo HOME_PATH; ?>js/jquery1.PrintArea.js"></script>
    <script type="text/javascript" src="<?php echo HOME_PATH; ?>js/jquery.tablesorter.min.js"></script>
    <script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!--FOR ON pAGE SEO mergerd the codes of this file in app.js <script type="text/javascript" src="<?php echo HOME_PATH; ?>js/script.js"></script>-->
    <script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/js/atc.min.js"></script>

    <script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/js/custom.js"></script>   
    <script type="text/javascript" src="<?php echo HOME_PATH; ?>js/validate.js"></script>
    <script type="text/javascript" src="<?php echo HOME_PATH; ?>js/additional_validation.js"></script>
    <script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/plugins/jquery/jquery-migrate.min.js"></script>
 <link rel="stylesheet" href="<?php echo HOME_PATH;?>includes/css/switcher/skin-red.css" media="screen" id="style-colors" />
<script>
 (adsbygoogle = window.adsbygoogle || []).push({
   google_ad_client: "ca-pub-2800188040402843",
   enable_page_level_ads: true
 });
</script>
 
   <!--Preloader starts-->
    <?php
		
        $currentPath = $_SERVER["HTTP_HOST"];
        $pathInfo = parse_path();
        $module = $pathInfo['call_parts']['0'];
        //echo (getMetaKeys($module));

/*reveiw coding*/
if(ISSET($_GET['id'])){$id = $_GET['id'];}
          else if($pathInfo['call_parts']['1']=='viewReview'){$id=md5($pathInfo['call_parts']['2']);}


else{
            // Check if in URL
            $pathInfo = parse_path();
            if(ISSET($pathInfo['call_parts']['2'])) {$id = 'search/'.$pathInfo['call_parts']['1'].'/'.$pathInfo['call_parts']['2'];}
        
        }
        $sql_query = "SELECT fdbk.*,
            memp.logo_enable,
            memp.map_enable,
            memp.address_enable,
            memp.phone_number_enable,
            memp.video_enable,
            memp.video_limit AS video_limit_enable,
            memp.youtube_video AS youtube_video_enable,
            memp.image_enable,
            memp.facility_desc_enable,
            memp.staff_enable,
            memp.management_enable,
            memp.activity_enable,
            memp.ext_url_enable,
            memp.image_limit,
            memp.product_limit,
            pein.certification_service_type,
            pein.certificate_license_end_date,
            pein.dhb_name,
            pein.certificate_name,
            pein.certification_period,
            pein.current_auditor,
            pein.email,
            pein.phone,
            pein.first_name,
            pein.last_name,
            pein.position,
        
        
 pr.*,pr.id AS proId, "
        		. "restCare.title as restcare_name,pr.id as pr_id,pr.title as facilityname, extinfo.*,extinfo.id as ex_id "
         		. " FROM " . _prefix("products") . " AS pr "
         		//        . " Left join " . _prefix("services") . " AS sr ON pr.facility_type=sr.id"
        //        . " Left join " . _prefix("cities") . " AS city ON pr.city_id=city.id"
        //        . " Left join " . _prefix("suburbs") . " AS suburb ON pr.suburb_id=suburb.id"
        . " Left join " . _prefix("rest_cares") . " AS restCare ON pr.restcare_id=restCare.id"
        //        . " Left join " . _prefix("services") . " AS facility ON pr.facility_type=facility.id"
        . " Left join " . _prefix("extra_facility") . " AS extinfo ON extinfo.product_id =pr.id"
        		. " Left join " . _prefix("feedbacks") . " AS fdbk ON fdbk.product_id =pr.id"
        				. " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id=pr.id"
        
        
        						. " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id =pr.id AND prpn.current_plan = 1"
        								. " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id =memp.id"
        
        
        										. " where md5(pr.id)='$id' OR pr.quick_url='$id' ";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        if (count($records)) {
        	foreach ($records as $record) {$prmid = $record['pr_id'];$facilityname=$record['facilityname'];}
        }
        $r_Count = overAllRatingonly($prmid);
/*reveiw coding*/


         if (isset($_SESSION['userId']) && $_SESSION['userId'] != '') {
            $sql_query = "SELECT * FROM " . _prefix('users') . " WHERE id = '{$_SESSION['userId']}' && deleted = '1'";
            $res = $db->sql_query($sql_query);
            $num = $db->sql_numrows($res);
            if ($num > 0) {
                session_destroy($_SESSION['userId']);
                session_destroy($_SESSION['name']);
                session_destroy($_SESSION['userType']);
                unset($_SESSION['userId']);
                unset($_SESSION['userEmail']);
                unset($_SESSION['name']);
                unset($_SESSION['userType']);
                $past = time() - 10;
                setcookie('remember', '0', $past);
                setcookie('AutoLoginId', '', $past);
                unset($_COOKIE['remember']);
                $url = HOME_PATH . '/supplier/logout';
                redirect_to($url);
            }
        }
        if (isset($_COOKIE) && array_key_exists('remember', $_COOKIE) && $_COOKIE['remember'] == '1' && strpos($_SERVER['HTTP_REFERER'], 'logout') <= 0 && $_SESSION['userId'] == '') {

            $sql_query = "SELECT * FROM " . _prefix('users') . " WHERE user_name = '{$_COOKIE['AutoLoginId']}' && deleted = '0'";
            $res = $db->sql_query($sql_query);
            $num = $db->sql_numrows($res);
            if ($num > 0) {
                $data = $db->sql_fetchrow($res);
                $_SESSION['userId'] = $data['id'];
                $_SESSION['id'] = $data['id'];
                $_SESSION['userEmail'] = $data['email'];
                $_SESSION['username'] = $data['first_name'] . ' ' . $data['last_name'];
                $_SESSION['userType'] = $data['user_type'];
                $year = time() + 7 * 24 * 3600;
                setcookie('remember', '1', $year);
            }
        }
 function remove_dashes($txt){
$txt=str_ireplace('--', ' ', $txt);
	 $txt=str_ireplace('-', ' ', $txt);
	 $txt=str_ireplace('_', ' ', $txt);
	 return ucwords(strtolower($txt));
 }       
        
        //$header_title="Aged Advisor - Find and Compare Retirement Villages and AgedCare";
 		$header_title = "AgedAdvisor: Find, Review and Compare Retirement Villages, Rest Homes NZ";
        $header_description_add='';
        $header_keyword = '';
        // Make a better page title
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0'])){$header_title="Aged Advisor Find and Compare - ".remove_dashes($pathInfo['call_parts']['0'])." ";$header_description_add=remove_dashes($pathInfo['call_parts']['0']);}
        if(ISSET($pathInfo['call_parts']['1']) && !is_integer($pathInfo['call_parts']['1'])){$header_title="Aged Advisor Find and Compare - ".remove_dashes($pathInfo['call_parts']['1'])." ";$header_description_add=remove_dashes($pathInfo['call_parts']['1']);}
        if(ISSET($pathInfo['call_parts']['2']) && !is_integer($pathInfo['call_parts']['2'])){$header_title="Aged Advisor Find and Compare - ".remove_dashes($pathInfo['call_parts']['2'])." ";$header_description_add=remove_dashes($pathInfo['call_parts']['2']);}
        if(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3'])){$header_title="Aged Advisor Find and Compare - ".remove_dashes($pathInfo['call_parts']['3'])." ";$header_description_add=remove_dashes($pathInfo['call_parts']['3']);}
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0'])) {
        	$header_title = "AgedAdvisor: Find, Review and Compare Retirement Villages, Rest Homes NZ";
        	$header_description_add = "Aged Advisor - New Zealand's largest review website to find and compare retirement villages, elderly care, rest homes and aged care facilities.";
        	$header_keyword = "retirement villages, residential care, elderly homes, resthomes, rest homes nz, aged care nz, nursing homes nz, aged care facilities, retirement homes, dementia care, geriatric homes, find aged care facility, retirement villages reviews, rest home subsidy, resthome reviews, reviews, rating, home, help, rest, homes, retired, retire, village, aged care, hospital, senior citizen, life care, assisted living, senior citizens, elders, elder, senior health, new zealand, nz, care, support, day care, respite care, elderly, geriatric, nursing home, home for the elderly, dementia, psychogeriatric, subsidy, agedadvisor, website, daycare, elder abuse, question of care, search, help, list, location, listing, complete list, complete, for grownups, for grown ups, old, old age, retirement, list of retirement villages";
        }
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0']) && $pathInfo['call_parts']['0']=='about-us'){
        	$header_title = "About AgedAdvisor: Find and Compare Retirement Villages";
        	$header_description_add = "About Aged Advisor - New Zealand’s largest website to search and compare retirement villages, rest homes, elderly homes and aged care facilities.";
        	$header_keyword = "about aged advisor, about us, retirement villages reviews, retirement villages nz, elderly homes nz, rest homes nz, aged care nz, nursing homes nz, residential care";
        }
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0']) && $pathInfo['call_parts']['0']=='rating-supplier'){
        	$header_title = "Write a Review | Retirement Villages and Aged Care Facilities";
        	$header_description_add = "Add your reviews about retirement villages, rest homes, nursing homes, elderly homes and aged care facilities in New Zealand.";
        	$header_keyword = "Write reviews, retirement village reviews, rest homes reviews, aged care reviews, add reviews, retirement village reviews NZ, rest homes reviews NZ, Advice, things to do";
        }
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0']) && $pathInfo['call_parts']['0']=='reviews'){
        	$header_title = "Read Reviews | Compare Retirement Villages and Aged Care Facilities";
        	$header_description_add = "Genuine reviews on choosing the best retirement villages, elderly homes, rest homes, nursing homes and aged care facilities in New Zealand.";
        	$header_keyword = "Retirement Villages Reviews, Aged Care Reviews, Elderly Homes Reviews, Rest home Reviews, Nursing Homes Reviews, Retirement Villages NZ, New Zealand Retirement Homes Reviews";
        }
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0']) && $pathInfo['call_parts']['0']=='sponsors-advertising'){
        	$header_title = "Sponsors Advertising - General and Media Enquiry";
        	$header_description_add = "Aged Advisor welcomes your participation and invites you to partner with us to take advantage of our many Sponsorship Advertising opportunities.";
        	$header_keyword = "Sponsors and Advertising, Media Enquiry, General Enquiry, Contact AgedAdvisor";
        }
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0']) && $pathInfo['call_parts']['0']=='contact-us'){
        	$header_title = "Contact Us: Retirement Villages, Aged Care Facilities | AgedAdvisor New Zealand";
        	$header_description_add = "Thank you for your interest in Aged Advisor’s services. We helping you find the right retirement villages, restcare or aged care facilities in New Zealand.";
        	$header_keyword = "Aged Advisor, Contact Aged Advisor, Retirement Villages, Residential Care, Elderly Homes, Rest Homes NZ, Aged Care NZ, Nursing Homes NZ, Restcare NZ, Aged Care Facilities";
        }
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0']) && $pathInfo['call_parts']['0']=='terms-and-conditions'){
        	$header_title = "Terms and Conditions: Aged Advisor";
        	$header_description_add = "Welcome to our Terms and Conditions of use. This is important and affects your legal rights, so please read them carefully.";
        	$header_keyword = "Terms and conditions, terms of use, AgedAdvisor, New Zealand, Aged Advisor NZ";
        }
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0']) && $pathInfo['call_parts']['0']=='faq'){
        	$header_title = "Frequently Asked Questions | Aged Advisor";
        	$header_description_add = "Find answers to all the Frequently Asked Questions about Aged Advisor ranging from services, membership, sponsorship, advertising and reviews.";
        	$header_keyword = "FAQs, Frequently Asked Questions, Aged Advisor, Aged Advisor NZ";
        }
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0']) && $pathInfo['call_parts']['0']=='articles'){
        	$header_title = "Aged Advisor Articles: Get News, Notes and Releases";
        	$header_description_add = "Read articles, notes, releases, tips and explore the stories.";
        	$header_keyword = "Articles, News, Stories, Releases ";
        }
        if(ISSET($pathInfo['call_parts']['0']) && !is_integer($pathInfo['call_parts']['0']) && $pathInfo['call_parts']['0']=='search'){
        	if(ISSET($pathInfo['call_parts']['1']) && !is_integer($pathInfo['call_parts']['1']) && $pathInfo['call_parts']['1']=='for'){
        		
        		if(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Apartment-1-bedroom')){
        			$header_title = "Find 1 Bedroom Apartment Rest Homes or Retirement Homes NZ";
        			$header_description_add = "Read our latest reviews to find best 1 Bedroom apartment retirement homes, elderly rest homes and Aged care facilities in New Zealand.";
        			$header_keyword = "1 Bedroom Apartment Rest Homes, 1 Bedroom Apartment Retirement Homes, 1 Bedroom Apartment Elderly Homes, 1 Bedroom Apartment Aged Care Facility, 1 Bedroom Apartment NZ, 1 Bedroom Apartment New Zealand";
        			}
        		elseif(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Apartment-2-bedroom')){
        			$header_title = "Find 2 Bedroom Apartment Rest Homes or Retirement Homes NZ";
        			$header_description_add = "Read our latest reviews to find best 2 Bedroom apartment retirement homes, elderly rest homes and Aged care facilities in New Zealand.";
        			$header_keyword = "2 Bedroom Apartment Rest Homes, 2 Bedroom Apartment Retirement Homes, 2 Bedroom Apartment Elderly Homes, 2 Bedroom Apartment Aged Care Facility, 2 Bedroom Apartment NZ, 2 Bedroom Apartment New Zealand";
        			}
        		elseif(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Apartment-3-bedroom')){
        			$header_title = "Find 3 Bedroom Apartment Rest Homes or Retirement Homes NZ";
        			$header_description_add = "Read our latest reviews to find best 3 Bedroom apartment retirement homes, elderly rest homes and Aged care facilities in New Zealand.";
        			$header_keyword = "3 Bedroom Apartment Rest Homes, 3 Bedroom Apartment Retirement Homes, 3 Bedroom Apartment Elderly Homes, 3 Bedroom Apartment Aged Care Facility, 3 Bedroom Apartment NZ, 3 Bedroom Apartment New Zealand";
        			}
        		elseif(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Rest-home-care')){
        			$header_title = "Find and Compare Rest Home and Residential Care NZ";
        			$header_description_add = "Read our latest reviews to find right rest homes, dementia care, geriatric homes, elderly homes and aged care facilities in New Zealand.";
        			$header_keyword = "Rest Homes NZ, Resthomes, Residential Care, Dementia Care, Geriatric Homes, find rest homes, Rest Homes New Zealand, New Zealand Rest Homes";
        			}
        		elseif(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Rest-home-care')){
        			$header_title = "Find and Compare Rest Home and Residential Care NZ";
        			$header_description_add = "Read our latest reviews to find right rest homes, dementia care, geriatric homes, elderly homes and aged care facilities in New Zealand.";
        			$header_keyword = "Rest Homes NZ, Resthomes, Residential Care, Dementia Care, Geriatric Homes, find rest homes, Rest Homes New Zealand, New Zealand Rest Homes";
        			}
        		elseif(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Medical-hospital-care')){
        			$header_title = "Find and Compare Medical Hospital Care and Nursing Homes NZ";
        			$header_description_add = "Read our latest reviews to find right Medical (Hospital Care) and nursing homes facilities in NZ (New Zealand).";
        			$header_keyword = "Medical Care, Hospital Care, Medical Hospital Care, Nursing Homes, Medical Care NZ, Hospital Care NZ, Nursing Homes NZ, Medical Care New Zealand, Hospital Care New Zealand";
        			}
        		elseif(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Dementia-care')){
        			$header_title = "Find and Compare Dementia Care NZ";
        			$header_description_add = "Read our latest reviews to find right Rest home Dementia Care facilities for you or your parents in NZ (New Zealand).";
        			$header_keyword = "Dementia Care, Dementia Care NZ, Dementia Care New Zealand, Find Dementia Care, Compare Dementia Care, Rest Home Dementia Care, Hospital Dementia Care";
        			}
        		elseif(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Geriatric')){
        			$header_title = "Find and Compare Geriatric Homes NZ";
        			$header_description_add = "Read our latest reviews to find right Geriatric homes facilities for you or your parents in NZ (New Zealand).";
        			$header_keyword = "Geriatric Homes, Geriatric Homes NZ, Geriatric Homes New Zealand, Find Geriatric Homes, Compare Geriatric Homes";
        		}
        		elseif(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Psychogeriatric')){
        			$header_title = "Find and Compare Psychogeriatric care NZ";
        			$header_description_add = "Read our latest reviews to find right Psychogeriatric care facilities for you or your parents in NZ (New Zealand).";
        			$header_keyword = "Psychogeriatric care, Psychogeriatric care NZ, Psychogeriatric care new Zealand";
        		}
elseif(ISSET($pathInfo['call_parts']['3']) && !is_integer($pathInfo['call_parts']['3']) && strtolower($pathInfo['call_parts']['3'])==strtolower('Studio-Unit')){
        			$header_title = "Find and Compare Studio Units Facilities NZ";
        			$header_description_add = "Read our latest reviews to find right studio units facilities for you and your parents in New Zealand.";
        			$header_keyword = "Studio Units, Studio Units Facilities, Studio Units NZ, Studio Units Facility, Find Studio Units";
        		}
        		else{
        			$header_title = remove_dashes($pathInfo['call_parts']['3'])." Retirement Villages and Rest Homes | AgedAdvisor";
        			$header_description_add = "Read our latest reviews to find right retirement villages, rest homes, elderly homes and aged care facilities in ".remove_dashes($pathInfo['call_parts']['3'])." NZ (New Zealand).";
        			$header_keyword = remove_dashes($pathInfo['call_parts']['3'])." Retirement Villages, Retirement Villages in ".remove_dashes($pathInfo['call_parts']['3']).", ".$pathInfo['call_parts']['3']." Rest Homes, Retirement homes ".remove_dashes($pathInfo['call_parts']['3']).", rest homes ".remove_dashes($pathInfo['call_parts']['3']).", residential care ".remove_dashes($pathInfo['call_parts']['3']).", aged care ".remove_dashes($pathInfo['call_parts']['3']).", reviews, rating, ratings, review, aged care, rest homes, resthomes, elder care, elderly, nursing home, rest home subsidy, old, old age, nz, new zealand, compare";
        		}
        		
        	}
        	else if(ISSET($pathInfo['call_parts']['1']) && !is_integer($pathInfo['call_parts']['1']) && $pathInfo['call_parts']['1']=='retirement-village'){
        		$header_title = remove_dashes($pathInfo['call_parts']['2'])." | Aged Advisor";
        		$header_description_add = "Read".$r_Count."reviews of ".remove_dashes($pathInfo['call_parts']['2']). ". See contact details, maps, reviews, ratings, features and district health board details.";
        		$header_keyword = remove_dashes($pathInfo['call_parts']['2']).", reviews, rating, home, help, resthomes, rest, homes, retired, retire, village, retirement village, retirement villages, aged care, hospital, home services, special needs, senior citizen, assisted living, life care, lifecare, senior citizens, elders, elder care, elder health, senior health, rest homes, supported living, assisted living, new zealand, region, guide book, guidebook, guide, handbook, care, support, day care, respite, services, elder, elderly, geriatric, rest home, resthome, nursing home, dementia care unit, dementia, unit, hospital, psychogeriatric, retirement, planning, retire, supports, residential care subsidy, rest home subsidy, database, daycare, dhb, district health board, residential care contract, nasc, care coordination centre, disability, disable, disabled, question of care, map, agedadvisor, website, search, help, list, location, listing, complete list, complete, complete list of, a retirement guide, for grownups, for grown ups, old, old age, nz, ratings, review";
        	}
            else if(ISSET($pathInfo['call_parts']['1']) && $pathInfo['call_parts']['1']=='viewReview'){// This is for the viewReviews pages
                $header_title = "View Reviews for ".$facilityname." | Aged Advisor";
                $header_description_add = "Read".$r_Count."reviews of ".$facilityname. ". See contact details, maps, reviews, ratings, features and district health board details.";
                $header_keyword = str_ireplace(' ', ', ', $facilityname).", reviews, rating, home, help, resthomes, rest, homes, retired, retire, village, retirement village, retirement villages, aged care, hospital, home services, senior citizen, assisted living, life care, lifecare, senior citizens, elders, elder care, elder health, senior health, rest homes, supported living, assisted living, new zealand, village guide, eldernet, guide, handbook, care, support, day care, respite, services, elder, elderly, geriatric, rest home, resthome, nursing home, dementia care unit, dementia, unit, hospital, psychogeriatric, retirement, planning, retire, supports, residential care subsidy, rest home subsidy, database, daycare, dhb, district health board, residential care contract, nasc, care coordination centre, disability, disable, disabled, question of care, map, agedadvisor, website, search, help, list, location, listing, complete list, complete, complete list of, a retirement guide, for grownups, for grown ups, old, old age, nz, ratings, review";
            }

        	else{
        		$header_title = remove_dashes($pathInfo['call_parts']['2'])." | Aged Advisor";
        		$header_description_add = "Read".$r_Count."reviews of ".remove_dashes($pathInfo['call_parts']['2']). ". See contact details, maps, reviews, ratings, features and district health board details.";
        		$header_keyword = remove_dashes($pathInfo['call_parts']['2']).", reviews, rating, home, help, resthomes, rest, homes, retired, retire, village, retirement village, retirement villages, aged care, hospital, home services, special needs, senior citizen, assisted living, life care, lifecare, senior citizens, elders, elder care, elder health, senior health, rest homes, supported living, assisted living, new zealand, region, guide book, guidebook, guide, handbook, care, support, day care, respite, services, elder, elderly, geriatric, rest home, resthome, nursing home, dementia care unit, dementia, unit, hospital, psychogeriatric, retirement, planning, retire, supports, residential care subsidy, rest home subsidy, database, daycare, dhb, district health board, residential care contract, nasc, care coordination centre, disability, disable, disabled, question of care, map, agedadvisor, website, search, help, list, location, listing, complete list, complete, complete list of, a retirement guide, for grownups, for grown ups, old, old age, nz, ratings, review";
        	}
        	
        }
        ?>
		
		<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','<?php echo HOME_PATH; ?>js/fbevents.js');
    
    fbq('init', '1815768811983088');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" class="fb-img" src="https://www.facebook.com/tr?id=1815768811983088&ev=PageView&noscript=1"
    alt="fb" /></noscript>
	
	
	
	 <style type="text/css">
    .awards-pages{
        max-width: 590px; 
        margin: 0px auto; 
        background-color: white; 
        border: 1px solid gray; 
        color: orange;
        line-height:23px;
    }
    .autocmplt{
        font-family: 'Open Sans', sans-serif;
    }
    .font-color-h2{
        color: white;
    }
  </style> 
<script type="application/ld+json">
{
  "@context" : "http://schema.org",
  "@type" : "WebSite", 
  "name" : "AgedAdvisor",
  "url" : "https://www.agedadvisor.nz/",
  "potentialAction" : {
    "@type" : "SearchAction",
    "target" : "https://www.agedadvisor.nz/search/for?cityLable={search_term}",
    "query-input" : "required name=search_term"
  }                     
}
</script>
  
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-61373903-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-61373903-1');
</script>
   
</head> 
<body>
 <div class="page-wrapper">
 <header class="header transparent scroll-hide">
 
 
            <!--Main Menu starts-->
            <div class="site-navbar-wrap v2">
                <div class="container">
                    <div class="site-navbar">
                        <div class="row align-items-center">
                            <div class="col-md-3 col-6">
                                <a class="navbar-brand" href="#"><img src="<?php echo HOME_PATH;?>modules/pages/images/find_agedadvisor_retirement_villages_agedcare_logo.png" alt="test" class="img-fluid"   ></a> 
								 
                            </div>
                            <div class="col-md-9 col-6">
                                <nav class="site-navigation float-left">
                                    <div class="container">
                                        <ul class="site-menu js-clone-nav d-none d-lg-block">
		 		 
                     <li><a href="<?php echo HOME_PATH; ?>">Home</a></li>       
 
                      <li><a href="<?php echo HOME_PATH; ?>about-us">About Us</a></li>
                       
						
                                            <li class="has-children">
                                                <a href="<?php echo HOME_PATH; ?>awards">Awards</a>
                                                <ul class="dropdown">
                                                   <li><a href="<?php echo HOME_PATH; ?>awards-best-retirement-villages-aged-care-2018">2018</a></li>
                            	<li><a href="<?php echo HOME_PATH; ?>awards-best-retirement-villages-aged-care-2017">2017</a></li>   	
                                 	<li><a href="<?php echo HOME_PATH; ?>awards-best-retirement-villages-aged-care-2015-2016">2015/16</a></li>
                                                </ul>
                                            </li>
                                            <li class="has-children">
                                                <a href="<?php echo HOME_PATH; ?>consumer-information"><!--COMMUNITY &AMP; -->Resources</a>
												
                                                <ul class="dropdown">
                                                    <li><a href="<?php echo HOME_PATH; ?>Compare-Retirement-Villages-Entry-Age-Costs-Fees-Conditions">Compare Villages</a></li>
                              	<li><a href="<?php echo HOME_PATH; ?>supplier/ranking">Compare Providers</a></li>
                            	<li><a href="<?php echo HOME_PATH; ?>community">Community</a></li>
                            	<li><a href="<?php echo HOME_PATH; ?>sponsors">Sponsors</a></li>
                            	<!--<li><a href="<?php echo HOME_PATH; ?>life-question">Life Questions</a></li>-->
                                <li><a href="<?php echo HOME_PATH; ?>events">Event Calendar</a></li>
                                <li><a href="<?php echo HOME_PATH; ?>articles">Articles</a></li>
                                <li><a href="<?php echo HOME_PATH; ?>consumer-information">Consumer Information</a></li>	
                                <li><a href="<?php echo HOME_PATH; ?>formal-complaints-hdc-dhb-moh">Voice a Complaint</a></li>	
                                                    
                                                </ul>
                                            </li>
											
										    <li class="languagesSelector">
                    <?php if(isset($_SESSION['csUserName'])){ ?>                   
                        <a href="<?php echo HOME_PATH . 'shortlist'; ?>" class="login-first"><span style="display: none;" class="number" id="sessionShortList"><?php echo isset($_SESSION['shortlist_count']) && $_SESSION['shortlist_count'] > 0 ? $_SESSION['shortlist_count'] : 0 ?></span> My shortlist</a>
                    <?php }else{ ?>
                        <a href="<?php echo HOME_PATH . 'shortlist'; ?>" class="login-first"><span style="display: none;" class="number" id="sessionCookieShortList">
                            <?php 
                            if (isset( $_COOKIE[ 'shortlist_cookie_count' ] )) {
                                $_SESSION['shortlist_cookie_count'] = $_COOKIE['shortlist_cookie_count'];
                            }
                            echo (isset($_SESSION['shortlist_cookie_count']) && $_SESSION['shortlist_cookie_count'] > 0) ? $_SESSION['shortlist_cookie_count'] : 0;
                            ?>
                            </span> My shortlist
                        </a>
                    <?php } ?>
                    
                   </li> 
                   <!--custom code end for add to shortlist notification  ***************-->
				   
                                              <li class="has-children">
                     <?php if (isset($_SESSION['userId'])) { ?>                       
                        <a>
						
						<span>
						<?php echo 'Welcome '?>
						</span> 
						<span id="SupUserName">
						<?php echo $_SESSION['SupUserName'];?>
						. LOGOUT
						</span>
						</a>
												
                                 <ul class="dropdown">
                               
                               	<li>
								 <a href="<?php echo HOME_PATH; ?>supplier/logout">Logout</a> 
								</li>
                            	<li>
								<a href="<?php echo HOME_PATH . 'supplier/changePassword'; ?>">Change&nbsp;Password</a>
								</li>
                            	<li>
								<a href="<?php echo HOME_PATH . 'supplier/channel?set=proOut'; ?>">Facility&nbsp;List</a>
								</li>
                            	   <?php if (isset($_SESSION['proEntry'])) { ?>
                                <li>
								<a href="#">Facility&nbsp;Register</a>
								</li>
								   <?php } ?>
								
								
								<li>
								<a href="#">Facility&nbsp;Register</a>
								</li>
							 
                                                    
                                                </ul>
                                            </li>
											
											 <?php } else if (isset($_SESSION['csId'])) { ?>
                         <a><?php  echo 'Welcome ' . $_SESSION['csUserName']?>. LOGOUT</a>
                        <ul class="dropdown">
                             <li><a href="<?php echo HOME_PATH . 'logOut'; ?>">Logout</a></li>
                            <li><a href="<?php echo HOME_PATH . 'changePassword'; ?>">Change&nbsp;Password</a></li>
                            <li><a href="<?php echo HOME_PATH . 'customerIssue'; ?>">Customer&nbsp;Issue</a></li>
                         </ul>
                                             
							<?php
 } else {
if (empty($_SESSION['csId'])) { ?>  
                         <a>LOGIN / JOIN</a>
                       <ul class="dropdown">
                            <li><a href="<?php echo HOME_PATH . 'login'; ?>">User&nbsp;Login</a></li>
                            <li><a href="<?php echo HOME_PATH . 'register'; ?>">User&nbsp;Register</a></li>
                            <li><a href="<?php echo HOME_PATH . 'supplier/login'; ?>">Facility&nbsp;Login</a></li>
                           <!-- <li><a href="<?php //echo HOME_PATH . 'contact-us'; ?>">Facility&nbsp;Register</a></li>-->
                            <li><a href="<?php echo HOME_PATH . 'supplier/register'; ?>">Facility&nbsp;Register</a></li>
                            <!-- <li><a href="<?php echo HOME_PATH . 'facility-login'; ?>">Dummy Login</a></li> -->
                         </ul>
<?php
     }
 } ?>				 
											 
  
                                            <li class="d-lg-none"><a class="btn v1" href="add-listing.html" style="margin-right: -78px;">Add Listing <i class="ion-plus-round"></i></a></li>
                                        </ul>
                                    </div>
                                </nav>
                                <div class="d-lg-none sm-right">
                                    <a href="#" class="mobile-bar js-menu-toggle">
                                        <span class="ion-android-menu"></span>
                                    </a>
                                </div>
                                <div class="add-list float-right">
                                    <a class="btn v3" href="<?php echo HOME_PATH; ?>rating-supplier">Write a Review <i class="ion-plus-round"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--mobile-menu starts -->
                    <div class="site-mobile-menu">
                        <div class="site-mobile-menu-header">
                            <div class="site-mobile-menu-close  js-menu-toggle">
                                <span class="ion-ios-close-empty"></span>
                            </div>
                        </div>
                        <div class="site-mobile-menu-body"></div>
                    </div>
                    <!--mobile-menu ends-->
                </div>
            </div>
            <!--Main Menu ends-->
        </header>
		<style>
		.add-list.float-right {
    margin-top: 6px;
   /* margin-right: -67px;*/
}

		</style>
		<!--22-08-2019 <script src="<?php echo HOME_PATH;?>includes/js/plugin.js"></script>-->
    <!--google maps-->
   
    <!--Main js-->
    <script src="<?php echo HOME_PATH;?>includes/js/main.js"></script>
	