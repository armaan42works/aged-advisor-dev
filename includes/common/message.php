<?php

    /*
     * Objective : error and general messages for client/admin panel
     * Filename : message.php
     * Created On :4 August 2014
     * Modified On : 14 August 2014
     */
    define('INVALID', 'Username/Password is incorrect, Please try again.');
    define('PASSWORD_CHANGED', 'Your Password has been successfully changed.');
    define('PASSWORD_INCORRECT', 'Your username OR Password is incorrect, Please try again!');
    define('INSERT', 'Your record has been saved successfully.');
    define('UPDATE', 'Your record has been updated successfully.');
    define('PASSWORD_LENGTH', 'Password length should more than equal to 6 characters and less than 30 characters.');
    define('ACCOUNT_VERIFY', 'Email Address has been verified.');
    define('ACCOUNT_NOT_VERIFY', 'Email Address has not been verified.');
    define('JOB_APPROVED', 'Job has been offered to SalesPerson.');
    define('JOB_DENIED', 'Job has been denied to SalesPerson.');
    define('MESSAGE', 'Message sent successfully.');
    define('INVALID_EMAIL', 'Email Address is not registered.');
    define('APPLICATION', 'Your Job application is submitted successfully.');
    define('FORGET_PASSWORD', 'Your password is generated please check your mail.');
    define('NOT_VALIDATE', 'Please validate your account.');
    // define('CHANGE_PASSWORD', 'Your password is changed, please login.');
    define('CHANGE_PASSWORD_SUCCESSFULLY', 'Your password has been changed successfully.');
    define('OLD_PASSWORD', 'Please enter correct old password.');
    define('SEND_MESSAGE', 'Your message has been sent.');
    define('CLIENT_FEEDBACK', 'Your feedback has been sent.');
    define('CHANGE_EMAIL', 'Your email has been successfully changed.');
    define('ADMIN_EMAIL_ID', 'Old email id is not correct.');
    define('Successfully Registered', ' Services/Products have been successfully registered.');
    define('Allready_inserted', 'Suburb is allready added in this city.');
    define('ALLREADY_INS_REST', 'This Rest care is allready added in this Suburb.');
    define('PAYMENT_UPGRADE', 'Your account has been successfully upgraded');
    define('ACCOUNT_INFO_UPDATE', 'Account information has been updated successfully.');
    define('ACCOUNT_TYPE_UPGRATE', 'Your Account Type has been upgrated successfully.');
    define('GALLARY_INPUT', 'Record has been inserted successfully in Gallery.');
    define('INSERT_ATWORK', 'Atwork record has been inserted successfully.');
    define('UPDATE_ATWORK', 'Atwork record has been updated successfully.');
    define('INSERT_ADBOOLING_PAYMENT', 'Ads Booking record has been booked successfully.');
    define('UPDATE_ADBOOLING', 'Ads Booking record has been updated successfully.');
    define('INSERT_PRODUCT', 'Service/Product has been inserted successfully.');
    define('UPDATE_PRODUCT', 'Service/Product has been updated successfully.');
    define('SUPPLIER_REGISTER', 'You have been registered successfully. Check mail and verify email for logIn.');
    define('SUPPLIER_REGISTER_EMAIL', 'You have been registered successfully. Check mail and verify email for logIn, But mail has not been sent to your email Id');
    define('CUSTOMER_REGISTER_EMAIL', 'You have been registered successfully. Check mail and verify email for logIn, But mail has not been sent to your email Id');
    define('CUSTOMER_FORGOT_PASSWORD', 'Password has been sent on your email Account. Check mail and <a href="' . HOME_PATH . 'login">Click here </a>to logIn');
    define('SUPPLIER_FORGOT_PASSWORD', 'Password has been sent on your email Account. Check mail and <a href="' . HOME_PATH . 'supplier/login">Click here </a>to logIn');
    define('CHANGE_PASSWORD', ' Password has been Change successfully. Log In with new password');
    define('ISSUE_SENT', 'Your message has been sent and we will be in touch shortly. Thank you.');
?>
