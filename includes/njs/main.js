jQuery(document).ready(function ()
{
  // slider js//


  jQuery('#main-slider').owlCarousel(
  {
    loop: true,
    margin: 0,
    nav: false,
    autoplay: true,
    dots: true,
    mouseDrag:false,
    autoplayTimeout: 8000,
    smartSpeed: 8000,
    animateOut: "fadeOut",
    animateIn: "fadeIn",
    responsive:
    {
      0:
      {
        items: 1
      },
      600:
      {
        items: 1
      },
      1000:
      {
        items: 1
      }
    }
  });


  jQuery('#regions_slider, #latest_village_videos_slider, #popular_catergories_slider, #top_ratedslider2, #trending_searches_slider, #hints_tips_videos_slider').owlCarousel(
  {
    loop: true,
    margin: 15,
    nav: false,
    autoplay: true,
    dots: false,
    mouseDrag:true,
    //animateOut: 'fadeOut',
    responsive:
    {
      0:
      {
        items: 2
      },
      768:
      {
        items: 3
      },
      1000:
      {
        items: 5
      }
    }
  });


  jQuery('#top_rated_facilities').owlCarousel(
  {
    loop: true,
    margin: 15,
    nav: false,
    autoplay: true,
    dots: false,
    mouseDrag:true,
    //animateOut: 'fadeOut',
    responsive:
    {
      0:
      {
        items: 1
      },
      768:
      {
        items: 2
      },
      1000:
      {
        items: 4
      }
    }
  });

  jQuery('#popular_posts_slider').owlCarousel(
  {
    loop: true,
    margin: 15,
    nav: false,
    autoplay: true,
    dots: false,
    mouseDrag:true,
    //animateOut: 'fadeOut',
    responsive:
    {
      0:
      {
        items: 1
      },
      768:
      {
        items: 2
      },
      1000:
      {
        items: 3
      }
    }
  });

  jQuery('#latest_reviews_slider').owlCarousel(
  {
    loop: true,
    margin: 15,
    nav: false,
    autoplay: true,
    dots: false,
    mouseDrag:true,
    //animateOut: 'fadeOut',
    responsive:
    {
      0:
      {
        items: 1
      },
      768:
      {
        items: 2
      },
      1000:
      {
        items: 3
      }
    }
  });








  $('.menu_icon').click(function(){
    $('.navication_wrap').toggleClass('navication_wrap_open'); 
  });

 // $(window).scroll(function ()
 //  {
 //    if ($(this).scrollTop() > 1)
 //    {
 //      $('header ').addClass("header_fixed");
 //    }
 //    else
 //    {
 //      $('header').removeClass("header_fixed");
 //    }
 //  });
});


