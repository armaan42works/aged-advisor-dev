<?php

require_once('../application_top.php');
global $db;
if (!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])) {
    // We've got everything we need
    $twitteroauth = new TwitterOAuth(YOUR_CONSUMER_KEY, YOUR_CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
// Let's request the access token
    $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
// Save it in a session var
    $_SESSION['access_token'] = $access_token;
// Let's get the user's info
    $user_info = $twitteroauth->get('account/verify_credentials');
    if (isset($user_info->error)) {
        // Something's wrong, go back to square 1  
        header('Location: login-twitter.php');
    } else {
        $twitter_otoken = $_SESSION['oauth_token'];
        $twitter_otoken_secret = $_SESSION['oauth_token_secret'];
        $email = '';
        $uid = $user_info->id;
        $username = $user_info->name;
        $pieces_name = explode(" ", $username, 2);
        $firstName = $pieces_name[0];
        $lastName = $pieces_name[1];
        // Check whether the twitter id exist or not
        $check_user = "SELECT * FROM " . _prefix('users') . " WHERE twitter_id= '$uid'";
        $res = $db->sql_query($check_user);
        $num = $db->sql_numrows($res);
        if ($num > 0) {
            $row = $db->sql_fetchrow($res);
            // if validate is 1 then redirect to registration page else to dashboard
            if ($row['validate'] <= 1) {
                $url = HOME_PATH . 'user/register/';
            } else {
                if ($row['user_type'] == 1) {
                    $_SESSION['username'] = $row['first_name'] . " " . $row['last_name'];
                    $_SESSION['userId'] = $row['id'];
                    $_SESSION['id'] = $row['id'];
                    $_SESSION['SupUserName'] = $firstName;
                } else {
                    $_SESSION['csUserFullName'] = $row['first_name'] . " " . $row['last_name'];
                    $_SESSION['csUserName'] = $firstName;
                    $_SESSION['csId'] = $row['id'];
                }
                $refererUrl = isset($_SESSION['redirect_url']) ? $_SESSION['redirect_url'] : '';
                $url = !empty($refererUrl) ? $refererUrl : HOME_PATH;
                unset($_SESSION['redirect_url']);
                /*$refererUrl = isset($_COOKIE['back_url']) ? $_COOKIE['back_url'] : '';                
                setcookie('back_url', '', time() - 3600);
                unset($_COOKIE['back_url']);
                $url = !empty($refererUrl) ? $refererUrl : HOME_PATH;*/
                
                redirect_to($url);
            }
        } else {
            // If the user is new and not registered
            $userData = array(
                'first_name' => $firstName,
                'last_name' => $lastName,
                'twitter_id' => $uid,
                'validate' => 2,
                'user_type' => 0,
                'password' => Random_Password(6),
                'unique_id' => randomId()
            );
            $insert_users = $db->insert(_prefix('users'), $userData);
            // Insert the details of user
            if ($insert_users) {
                // Check the record with reference to twitter id 
                $check_user = "SELECT id, first_name, user_name, last_name, user_type, validate FROM " . _prefix('users') . " WHERE twitter_id = '$uid'";
                $res = $db->sql_query($check_user);
                $row = $db->sql_fetchrow($res);
                // if validate is 1 then redirect to registration page
                if ($row['validate'] <= 1) {
                    $url = HOME_PATH . 'user/register';
                } else {
                    if ($row['user_type'] == 1) {
                        $_SESSION['username'] = $row['first_name'] . " " . $row['last_name'];
                        $_SESSION['userId'] = $row['id'];
                        $_SESSION['id'] = $row['id'];
                        $_SESSION['SupUserName'] = $firstName;
                    } else {
                        $_SESSION['csUserFullName'] = $row['first_name'] . " " . $row['last_name'];
                        $_SESSION['csUserName'] = $firstName;
                        $_SESSION['csId'] = $row['id'];
                    }
                    $refererUrl = isset($_COOKIE['back_url']) ? $_COOKIE['back_url'] : '';
                    setcookie('back_url', '', time() - 3600);
                    unset($_COOKIE['back_url']);
                    $url = !empty($refererUrl) ? $refererUrl : HOME_PATH;
                    redirect_to($url);
                }
            } else {
                // if the user data not inserted rediect to login page
                $url = HOME_PATH . 'login';
                redirect_to($url);
            }
        }
    }
} else {
    // Something's missing, go back to square 1
    header('Location: login-twitter.php');
}
?>
