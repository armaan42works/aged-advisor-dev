<?php
if ($_SESSION['AccountType'] == 1) {
    redirect_to(HOME_PATH . 'supplier/payment');
}
global $db;
$pro_id = $_REQUEST['pro_id'];
$id = base64_decode($_REQUEST['id']);
if (isset($submit) && $submit == 'Submit') {
    $fields_array = product_extra_info();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $fields = array(
            'pro_id' => trim(base64_decode($_POST['pro_id'])),
            'manager' => trim($_POST['manager']),
            'email' => trim($_POST['email']),
            'phone' => trim($_POST['phone']),
            'last_name' => trim($_POST['last_name']),
            'position' => trim($_POST['position']),
            'old_email' => trim($_POST['old_email']),
            'premises_name' => trim($_POST['premises_name']),
            'certification_service_type' => trim($_POST['certification_service_type']),
            //'service_types' => trim($_POST['service_types']),
            'premises_website' => trim($_POST['premises_website']),
            'premises_address_other' => trim($_POST['premises_address_other']),
            'dhb_name' => $_POST['dhb_name'],
            'certificate_name' => trim($_POST['certificate_name']),
            'certification_period' => trim($_POST['certification_period']),
            'certificate_license_end_date' => trim($_POST['certificate_license_end_date']),
            'current_auditor' => trim($_POST['current_auditor']),
            'created' => date('Y-m-d h:i:s', time())
        );
        $insert_result = $db->insert(_prefix('pro_extra_info'), $fields);
        if ($insert_result) {
            $field = array(
                'pro_extra_info' => mysqli_insert_id($db->db_connect_id)
            );
            $where = "where id=" . base64_decode($_POST['pro_id']) . " ";
            $update_result = $db->update(_prefix('products'), $field, $where);

            if ($update_result) {
                $msg = common_message_supplier(1, constant('INSERT_PRODUCT'));
                $_SESSION['msg'] = $msg;
                redirect_to(HOME_PATH . 'supplier/product-manager');
            }
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

if (isset($_GET['id']) && !empty($_GET['id'])) {
    $id = base64_decode($_GET['id']);
    $sql_query = "SELECT pr.* FROM " . _prefix("pro_extra_info") . " AS pr "
            . " where  pr.id='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    //prd($id);
    if (count($records)) {
        foreach ($records as $record) {
            $manager = $record['manager'];
            $email = $record['email'];
            $phone = $record['phone'];
            $last_name = $record['last_name'];
            $position = $record['position'];
            $old_email = $record['old_email'];
            $premises_name = $record['premises_name'];
            $certification_service_type = $record['certification_service_type'];
            //$service_types = $record['service_types'];
            $premises_website = $record['premises_website'];
            $premises_address_other = $record['premises_address_other'];
            $dhb_name = $record['dhb_name'];
            $certificate_name = $record['certificate_name'];
            $certification_period = $record['certification_period'];
            $certificate_license_end_date = $record['certificate_license_end_date'];
            $current_auditor = $record['current_auditor'];
            
        }
    }
}

if (isset($submit) && $submit == 'Update') {
    $fields_array = product_extra_info();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $fields = array(
            'pro_id' => trim(base64_decode($_POST['pro_id'])),
            'manager' => trim($_POST['manager']),
            'email' => trim($_POST['email']),
            'phone' => trim($_POST['phone']),
            'last_name' => trim($_POST['last_name']),
            'position' => trim($_POST['position']),
            'old_email' => trim($_POST['old_email']),
            'premises_name' => trim($_POST['premises_name']),
            'certification_service_type' => trim($_POST['certification_service_type']),
            //'service_types' => trim($_POST['service_types']),
            'premises_website' => trim($_POST['premises_website']),
            'premises_address_other' => trim($_POST['premises_address_other']),
            'dhb_name' => $_POST['dhb_name'],
            'certificate_name' => trim($_POST['certificate_name']),
            'certification_period' => trim($_POST['certification_period']),
            'certificate_license_end_date' => date('Y-m-d', time($_POST['certificate_license_end_date'])),
            'current_auditor' => trim($_POST['current_auditor']),
            'modified' => date('Y-m-d h:i:s', time())
        );
        $where = "where id= $id";
        $update_result = $db->update(_prefix('pro_extra_info'), $fields, $where);
        if ($update_result) {
            $msg = common_message_supplier(1, constant('UPDATE_PRODUCT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(HOME_PATH . "supplier/product-manager");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>

<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>


<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-pencil-square-o"></i> <span><?php echo (isset($_GET['id']) && !empty($_GET['id'])) ? 'Edit' : 'Add'; ?> Service/Product's Extra Information</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thumbnail adbooking_container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form name="addLogo" id="addLogo" action="" method="POST" class="form-horizontal form_payment" role="form" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="manager" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Manager</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="remises_name" name="manager" maxlength="50" class="form-control input_fild_1" type="text" value="<?php echo isset($manager) ? stripslashes($manager) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="email" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Email</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="remises_name" name="email" maxlength="100" class="form-control input_fild_1" type="email" value="<?php echo isset($email) ? stripslashes($email) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="last_name" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Last Name</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="remises_name" name="last_name" maxlength="100" class="form-control input_fild_1" type="text" value="<?php echo isset($last_name) ? stripslashes($last_name) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="position" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Position</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="remises_name" name="position" maxlength="100" class="form-control input_fild_1" type="text" value="<?php echo isset($position) ? stripslashes($position) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="old_email" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Old Email</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="remises_name" name="old_email" maxlength="100" class="form-control input_fild_1" type="email" value="<?php echo isset($old_email) ? stripslashes($old_email) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="remises_name" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Premises Name</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="remises_name" name="premises_name" maxlength="100" class="form-control input_fild_1" type="text" value="<?php echo isset($premises_name) ? stripslashes($premises_name) : ''; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="certification_service_type" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Certification Service Type</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="certification_service_type" name="certification_service_type" maxlength="100" class="form-control input_fild_1 " type="text" value="<?php echo isset($certification_service_type) ? stripslashes($certification_service_type) : ''; ?>">
                                                </div>
                                            </div>
<!--                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="service_types" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Service Type</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <select id="service_types" style="margin: 8px 0;" multiple="true" class=" form-control select_option_1 pull-left" name="service_types" style="width:235px;"><?php //echo serviceCatMultiple($service_types); ?></select>
                                                </div>
                                            </div>-->

                                            <script type="text/javascript">
                                                $(document).ready(function() {
                                                    $('#service_types').multiselect();
                                                })
                                            </script>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="premises_website" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Premises Website</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="premises_website" name="premises_website" maxlength="100" class="form-control input_fild_1" type="text" value="<?php echo isset($premises_website) ? stripslashes($premises_website) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">

                                                <label for="premises_address_other" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Premises Address Other</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <textarea rows="4" cols="50" maxlength="500" name="premises_address_other" class="form-control input_fild_1" id="description"><?php
                                                        if ((isset($premises_address_other)) && !empty($premises_address_other)) {
                                                            echo stripslashes($premises_address_other);
                                                        }
                                                        ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="dhb_name" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">DHB Name</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="dhb_name" name="dhb_name" maxlength="100" class="form-control input_fild_1" type="text" value="<?php echo isset($dhb_name) ? stripslashes($dhb_name) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="certificate_name" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Certificate Name</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="certificate_name" name="certificate_name" maxlength="100" class="form-control input_fild_1" type="text" value="<?php echo isset($certificate_name) ? stripslashes($certificate_name) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="certification_period" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration"> Certification Period (Months)</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="certification_period" name="certification_period" maxlength="5" class="form-control input_fild_1 number" type="text" value="<?php echo isset($certification_period) ? stripslashes($certification_period) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="certificate_license_end_date" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Certificate/License End Date</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="expiry_date" name="certificate_license_end_date"  class="form-control input_fild_1" type="text" value="<?php echo isset($certificate_license_end_date) ? stripslashes($certificate_license_end_date) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="current_auditor" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Current Auditor</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="current_auditor" name="current_auditor" maxlength="100" class="form-control input_fild_1" type="text" value="<?php echo isset($current_auditor) ? stripslashes($current_auditor) : ''; ?>">
                                                </div>
                                            </div>
                                            <input id="pro_id" name="pro_id"  type="hidden" value="<?php echo $pro_id; ?>">



                                            <div class="form-group text-center" style="margin-top:40px;">
                                                <?php if (isset($_GET['id']) && !empty($_GET['id'])) { ?>
                                                    <button type="submit" value="Update" name="submit" id="submit" class="btn btn-danger center-block btn_search btn_pay" style=" padding:6px 20px 6px; margin-bottom:2%;">UPDATE</button>
                                                <?php } else { ?>
                                                    <button type="submit" value="Submit" name="submit" id="submit" class="btn btn-danger center-block btn_search btn_pay" style=" padding:6px 20px 6px; margin-bottom:2%;">SUBMIT</button>
                                                <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;color: black;}
    label.error{
        color:red;
        font-size: 10px;

    }
    .redCol{
        color:red;
    }

</style>
<script type="text/javascript">
    $('#expiry_date').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        minDate: 0
    });
    $(document).ready(function() {
        $('#addLogo').validate();
        $(".fancybox").fancybox();
        $("#openImage").fancybox({
            width: 400, // or whatever
            height: 400,
            autoSize: false
        });

        $('#city').change(function() {
            var cityId = $('#city').val();
            var path = '<?php echo HOME_PATH; ?>';
//            alert(cityId);
//            alert(path);
            $.ajax({
                url: path + 'ajaxFront.php?' + new Date().getTime(),
                data: 'action=getSuburb&cityId=' + cityId,
                type: "POST",
                cache: false,
                success: function(response) {
                    try {
                        $('#suburb').html(response);
                    } catch (e) {
                    }
                }
            })
        });
        $('#addLogo').validate({
            rules: {
                staff_image: {
                    accept: 'jpg,png,jpeg,gif'

                },
                management_image: {
                    accept: 'jpg,png,jpeg,gif'

                },
                activities_image: {
                    accept: 'jpg,png,jpeg,gif'

                }

            },
            messages: {
                staff_image: {
                    accept: "Image type jpg/png/jpeg/gif is allowed"
                },
                management_image: {
                    accept: "Image type jpg/png/jpeg/gif is allowed"
                },
                activities_image: {
                    accept: "Image type jpg/png/jpeg/gif is allowed"
                }
            },
            submitHandler: function(form) {
                $('#submit').attr('disabled', 'disabled');
                form.submit();

            }


        });
        $('#suburb').change(function() {
            var suburbId = $('#suburb').val();
            var path = '<?php echo HOME_PATH; ?>';
            $.ajax({
                url: path + 'ajaxFront.php?' + new Date().getTime(),
                data: 'action=getRestCare&suburbId=' + suburbId,
                type: "POST",
                cache: false,
                success: function(response) {
                    try {
                        $('#restcare').html(response);
                    } catch (e) {
                    }
                }
            })
        });
        $('.geotag').click(function() {
            var geotag = $(this).val();
            if (geotag == 'yes') {
                $('#zipid').hide();
                $('#zip').removeClass('required');
            }
            if (geotag == 'no') {
                $('#zipid').show();
                $('#zip').addClass('required');
            }
        });

    });
//<?php if (file_exists(DOCUMENT_PATH . 'admin/files/product/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
        $('#image').removeClass('required');
        //<?php } ?>
</script>