<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '&id=53';
        changePagination(page, li, 'articles', data);
    });
</script>

  <div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH;?>/modules/pages/images/bg/memphis-colorful.png); height:250px;">
            <div class="overlay op-5 green"></div>
            <div class="container">
                <div class="row align-items-center  pad-top-80">
                    <div class="col-md-8 col-12">
                        <div class="breadcrumb-menu">
                            <h2 class="page-title" style="color:#141414"> Articles</h2>
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="breadcrumb-menu text-right sm-left">
                            <ul>
                                <li class="active"><a href="<?php echo HOME_PATH;?>">Home</a></li>
                                <li><a href="<?php echo HOME_PATH;?>articles">Articles</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!--MAIN BODY CODE ENDS-->

    <div class="blog-posts v1 pad-bot-60 pad-top-70">
            <div class="container">
                
                <div class="row" id="pageData">
                    
                  
                    
                </div>
            </div>
        </div>