<?php
require_once ('modules/fblogin/facebook-sdk-v5/autoload.php');
    
$fb = new Facebook\Facebook([
  'app_id' => '1409054779407381', // Replace {app-id} with your app id
  'app_secret' => 'e7dcb280e81129960a9a99d60b91c3f9',
  'default_graph_version' => 'v2.8'
]);

$helper = $fb->getRedirectLoginHelper();

$permissions = ['email','user_location','public_profile']; // Optional permissions
$loginUrl = $helper->getLoginUrl('https://www.agedadvisor.nz/facebook-login/', $permissions);
$loginUrl = htmlspecialchars($loginUrl);
$_SESSION['back_url'] = 'competition';


//error_reporting(E_ALL);
//ini_set('display_errors', '1');
global $db;
function dbconnecti(){global $db;return $db;}
// Set USERS, LANGUAGES, CURRENCIES, BASKET
$_SESSION['redirect_url'] = "https://".$_SERVER[HTTP_HOST].$_SERVER[REQUEST_URI];
if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
    $userId = $_SESSION['userId'];
} else if (isset($_SESSION['csId']) && !empty($_SESSION['csId'])) {
    $userId = $_SESSION['csId'];
}

$usernumber = $userId;





function adjustcompetitions() {
	$notify_message='';
  $sql = "select * from";
  $sql .= " competition where status in (0,1,2,3) order by status,enddate desc";

  $GLOBALS['DB'] = dbconnecti();
  $result = mysqli_query($db->db_connect_id,  $sql ) or die( "Error getting list" );
  ;
  while ( $myrow = mysqli_fetch_row( $result ) ) {
    $strID = $myrow[ 0 ];
    $strname = $myrow[ 1 ];
    $strdescription = $myrow[ 3 ];
    $strdescription = str_replace( "\n", "<br>", $strdescription );

    $strstartdate = $myrow[ 4 ];
    $strenddate = $myrow[ 5 ];
    $strvotedate = $myrow[ 6 ];
    $mediaallowed = $myrow[ 7 ];
    $strstatus = $myrow[ 8 ];
    $newstatus = $strstatus;
      if (time() <= (strtotime( "$strstartdate" ))) {
        $newstatus = 0;
      }

    if ($strstatus == 0) { // Has it started?
      if (time() > (strtotime( "$strstartdate" ))) {
        $newstatus = 1;
      }
    } // end of has it started
    if ($strstatus == 1) { // voting?
      if (time() > (strtotime( "$strvotedate" ) + 86400)) {
        $newstatus = 2;
      }
    } // end of start of voting
    if ($strstatus == 2) { // voting closed?
        $time_left_to_vote=((strtotime( "$strenddate" ) + 86400)-time())/60;//minutes
        if($time_left_to_vote<5000){$notify_message="Voting for $strname finishes in ".floor($time_left_to_vote/1440)." days.";}
        if($time_left_to_vote<1440){$notify_message="Voting for $strname finishes in ".floor($time_left_to_vote/60)." hours.";}
        if($time_left_to_vote<60){$notify_message="Voting for $strname finishes in ".ceil($time_left_to_vote)." minutes.";}
        
        
        
      if (time() > (strtotime( "$strenddate" ) + 86400)) {
        $newstatus = 3;
      }
    } // end of stop voting



    if ($strstatus !== $newstatus) { // we must update to new status
      $GLOBALS['DB'] = dbconnecti();
      $sqlinc = "update competition set status='$newstatus' where ID='$strID'";
      mysqli_query($db->db_connect_id,  $sqlinc ) or die( "Error updating status of competition" );

    } //end of update the status
  } // on to next competition
  return $notify_message;
} // end of function adjustcompetitions



function howmanyentries( $competition ) {
  $numresults = 0;
  // find out how many times anyone has entered this competition
  $sql = "select ID ";
  $sql .= "FROM competition_media ";
  $sql .= "WHERE competitionID=$competition";
  $GLOBALS['DB'] = dbconnecti();
  $result = mysqli_query($db->db_connect_id,  $sql );
  if ($result) { //  if (mysqli_num_rows($result)>0){
    $numresults = mysqli_num_rows( $result );
  }
 		$sql="update `competition` set number_of_entries = '$numresults' WHERE ID='$competition' ";
		$GLOBALS['DB'] = dbconnecti();
		mysqli_query($db->db_connect_id, $sql) or die("<br>$sql<br>Error updating table");
  
  
 
  return $numresults;
}


function Numbervotes( $competition ) {
  $GLOBALS['DB'] = dbconnecti();
  $sql = "Select SUM(votes) from competition_media where competitionID='$competition'";
  $result = mysqli_query($db->db_connect_id,  $sql ) or die( "$sql Error checking number of Entry" );
  if ($result) { //  if (mysqli_num_rows($result)>0){
    $myrow = mysqli_fetch_row( $result );
    $numvotes = $myrow[ 0 ];
  }
  if ($numvotes > 0) {
    $back = "Votes: $numvotes";
    return $back;
  } else {
    return;
  }
}


function printdate( $strdate ) {
  return date( "d F Y", strtotime( "$strdate" ) );
}




// If they are not logged in then they
// get set to the guest group

$strDate = date( 'F Y' );



$notify_message= adjustcompetitions();

$header_title="List of current competitions";

$responsivetabs=FALSE;
$home_banner=FALSE;
$feature_slider=FALSE;
$feature_slider=FALSE;


?>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
	                        <h3>Below is our list of current and archived competitions</h3>
                        </div>
                    </div>
<div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
	                        
                                                
<?php
  if (! $usernumber) {
    echo '<div class="alert alert-info">Anyone can vote. However only members can enter competitions below this line. Members must <a href="/login">login</a> first. You can <a href="/register">become a member</a> for free or <br><a href="https://www.agedadvisor.nz/login-twitter.php"><img class="btn" src="/images/sign_in_with_twitter.png" alt="twitter Login"/></a> &nbsp; &nbsp; &nbsp;<a href="'.$loginUrl.'"><img class="btn" src="/images/sign_in_with_facebook.png" alt="Facebook Login"/></a></div>';
  }
?></div></div><?php
  // Cycle through competitions here
  $sql = "select * from";
  $sql .= " competition where status in (1,2,3) order by status,enddate desc limit 0,2";

  $GLOBALS['DB'] = dbconnecti();
  $result = mysqli_query($db->db_connect_id,  $sql ) or die( "Error getting list" );
  
  while ( $myrow = mysqli_fetch_row( $result ) ) {
    $strID = $myrow[ 0 ];
    $strname = $myrow[ 1 ];
    $strname = str_replace( '"', "&quot;", $strname );
    $strdescription = $myrow[ 3 ];
    $strdescription = str_replace( "\n", "<br>", $strdescription );
    $strdescription = str_replace( "\r", '', $strdescription );

    $strstartdate = $myrow[ 4 ];
    $strenddate = $myrow[ 5 ];
    $strvotedate = $myrow[ 6 ];
    $mediaallowed = $myrow[ 7 ];
    $strstatus = $myrow[ 8 ];
 		$small_advert = $myrow[12];
 		// remove http
 		$small_advert = str_ireplace('http:', '', $small_advert);
		$banner_advert = $myrow[13];
		$finished_announcement = $myrow[14];
		$meta_tags = $myrow[15];
		$number_of_entries = $myrow[16];
   $strmedia = explode( ",", $mediaallowed );
    
      $text_color= '';$text_color_close='';$image_fade='';

    if ($strstatus == "3") {
      $text_color= '<span style="color:#B0B0B0;">';$text_color_close='</span>';$image_fade='style= "filter: alpha(opacity=40); opacity: 0.4;" ';
    }
    else{$number_of_entries = howmanyentries( $strID );}
   
 $img='';   
if($small_advert){$img= '<a href="https://www.agedadvisor.nz/view-all-entries/' . $strID . '">'."<img class=\"img-thumbnail\" src=\"$small_advert\" alt=\"$strname\" $image_fade></a>";}
$status='';


    if ($strstatus == "1") {
      $status.= "Active, voting starts ";
      $status.= printdate( $strvotedate );
    }
    if ($strstatus == "2") {
      $status.= "Voting, late entries still accepted, Voting until ";
      $status.= printdate( $strenddate );
    }
    if ($strstatus == "3") {
      $status.= "Finished: ";
      $status.= printdate( $strenddate );
    }
    $status.='.';

    $status.= " We have $number_of_entries ";
    if ($number_of_entries == 1) {
      $status.= "entry.";
    } else {
      $status.= "entries.";
    }
    $how_to_enter='';
    if ($strstatus == "1") {
      $how_to_enter.= '<a style="margin: 5px;" class="btn btn-danger" role="button" href="https://www.agedadvisor.nz/new-media/' . $strID . '">Upload your entry here</a>&nbsp; ';
    }
    if ($number_of_entries > 0 && $strstatus == "1") {
      $how_to_enter.=  '<a style="margin: 5px;" class="btn btn-info" role="button"   href="https://www.agedadvisor.nz/view-all-entries/' . $strID . '">View Entries</a>';
    }
    if ($strstatus == "2") {
      $how_to_enter.=  '<a style="margin: 5px;" class="btn btn-danger" role="button" href="https://www.agedadvisor.nz/new-media/' . $strID . '">Upload your entry here</a>&nbsp; 
      <a style="margin: 5px;" class="btn btn-info" role="button"  href="view-all-entries/' . $strID . '">View entries and vote here</a> ' . Numbervotes( $strID );
    }
    if ($strstatus == "3") {
      $how_to_enter.=  '<a style="margin: 5px;"" class="btn btn-info" role="button"  href="https://www.agedadvisor.nz/view-all-entries/' . $strID . '">View entries and results here</a>';
    }
$how_to_enter.='<br>';
?>
                    <div class="row">
                        <div class="col-md-3">
                        <?php echo $img?>
                        </div>
                        <div class="col-md-9">
                        <?php echo $text_color?><h2><?php echo $strname?></h2>
	                    <b>Status:</b> <?php echo $status?><br>
	                    <b>About:</b>  <?php echo $strdescription?><br> <br>
	                    <?php echo $how_to_enter?>   
                        <?php echo $text_color_close?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
	                        <hr>
                        </div>
                    </div>

<?php

  }
  // End of cycle through competitions
  ?>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                        </div>
                    </div>



