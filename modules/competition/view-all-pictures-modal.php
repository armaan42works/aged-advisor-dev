<?php

function showimagesbox($competition, $filetype = 0, $focus = 0, $userId = '') {
    $insert = '';
    if ($competition !== 0) {
        $GLOBALS['DB'] = dbconnecti();
        $sql = "SELECT * FROM competition where ID=$competition AND status in (1,2,3)";
        $result = mysqli_query($db->db_connect_id, $sql) or die("Error getting competition details");
        if ($result) {
            $myrow = mysqli_fetch_row($result);
            $name = $myrow[1];
            $description = $myrow[3];
            $description = str_replace("\n", "<br>", $description);

            $entriesclose = $myrow[6];
            $strstatus = $myrow[8];
            $mediaallowed = $myrow[7];
            $rules = $myrow[9];
            $maximumentries = $myrow[10];
            $finished_announcement = $myrow[14];
        }
    }
    if (!$name) {
        echo"Access denied - no such competition";
        exit;
    }// 
    ?><?php echo  $finished_announcement ?>
    <?php
//==================================
// Show list of media files entries that can be downloaded/viewed
//==================================
//Show list of media files that can be downloaded/viewed
    $GLOBALS['DB'] = dbconnecti();
    $imbeddedimage = "";
    $output = "";
    $counting = 0;
    $strtable = 1;
    $usernumber = $userId;



    $sql = "SELECT ID,title,userID,competitionID,votes,views,file,dateadded,length,type,file_small,approved,file_medium FROM competition_media WHERE competitionID='$competition' AND (approved>0 OR userID='$usernumber') ORDER BY views ASC,ID desc";
    if ($strstatus == 3) {
        $sql = "SELECT ID,title,userID,competitionID,votes,views,file,dateadded,length,type,file_small,approved,file_medium FROM competition_media WHERE competitionID='$competition'  AND (approved>0 OR userID='$usernumber') ORDER BY votes desc";
    }

//echo $sql;exit;

    $result = mysqli_query($db->db_connect_id, $sql) or die("Error finding entries");
    if ($result) {
        $c = 1;
        echo "<div class=\"row\">";
        if (mysqli_num_rows($result) > 0) {
            $mediatype = "";
            while ($myrow = mysqli_fetch_row($result)) {
                if ($strstatus == 3) {
                    $counting = $counting + 1;
                    $insert = "No. $counting<br>";
                }
                $approved = $myrow[11];
                $votes = $myrow[4];

                $strImage = "https://www.agedadvisor.nz/images/competition/" . $myrow[10];
                $strImage_medium = "https://www.agedadvisor.nz/images/competition/" . $myrow[12];
                $ajax_id = 'https://www.agedadvisor.nz/modules/competition/view-all-entries-modal.php/' . $competition . '-' . $myrow[0];
                $normal_href = 'https://www.agedadvisor.nz/view-all-entries/' . $competition . '-' . $myrow[0];
                $strName = $myrow[1];

                $strThumb = "<img class=\"img-responsive img-thumbnail\" src =\"" . $strImage . "\" alt=\"" . $strName . "\" title=\"" . $strName . "\" src=\"\">";



                if ($imbeddedimage == "" && $filetype == 0) {
                    $imbeddedimage = "Found";
                    $filetype = $myrow[0];
                }

                $highlight = '';
                if ($filetype == $myrow[0]) {
                    if ($focus) {
                        $highlight = ' bgcolor="#fefefe"';
                    }
                }
                echo '<div class="col-md-2 col-sm-4 col-xs-6"><div class="thumbnail" style="margin:10px auto;"><div class="text-center">';
                echo $insert;
                echo '<a class="fancybox" rel="lightbox" href="' . $normal_href . '" data-fancybox-href="' . $strImage_medium . '" data-ajax-id="' . $ajax_id . '" title="">';
                echo $strThumb;
                echo "</a>";
                if ($votes > 0 && $strstatus == '2') {
                    echo "<br>" . $votes . "&nbsp;Votes.";
                }
                if ($votes == 0 && $strstatus == '2') {
                    echo "<br>&nbsp;";
                }



                if (!$approved) {
                    echo "<br>Awaiting Approval";
                }
                echo '</div></div></div>';

                $strtable = $strtable + 1;
                $c = $c + 1;
                //echo"\n";
            }// end of while getentries
// Now tidy up the end table
            echo "</div>"; // end of row
        } else {
            ?><div class="row"><div class="col-md-12"><hr></div></div><?php
        }
    }
}

// end function show all images in table
?>