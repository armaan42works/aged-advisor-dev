<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

include("../../application_top.php");
//---------- Header Start -------------
include(INCLUDE_FILE . "common_ajaxPaging.php");

function facility_details($productID) {
    global $db;
    if ($productID) {
        $sql_query = "SELECT fdbk.*,
            AVG(NULLIF(fdbk.quality_of_care, 0)) as av_quality_of_care,
            AVG(NULLIF(fdbk.caring_staff,0)) as av_caring_staff,
            AVG(NULLIF(fdbk.responsive_management,0)) as av_responsive_management,
            AVG(NULLIF(fdbk.trips_outdoor_activities,0)) as av_trips_outdoor_activities,
            AVG(NULLIF(fdbk.indoor_entertainment,0)) as av_indoor_entertainment,
            AVG(NULLIF(fdbk.social_atmosphere,0)) as av_social_atmosphere,
            AVG(NULLIF(fdbk.enjoyable_food,0)) as av_enjoyable_food,            
            memp.logo_enable,
            memp.map_enable,
            memp.address_enable,
            memp.phone_number_enable,
            memp.video_enable,
            memp.video_limit AS video_limit_enable,
            memp.youtube_video AS youtube_video_enable,
            memp.image_enable,
            memp.facility_desc_enable,
            memp.staff_enable,
            memp.management_enable,
            memp.activity_enable,
            memp.ext_url_enable,
            memp.image_limit,
            memp.product_limit,
            pein.certification_service_type,
            pein.certificate_license_end_date,
            pein.dhb_name,
            pein.certificate_name,
            pein.certification_period,
            pein.current_auditor,
            pein.email,
            pein.phone,
            pein.first_name,
            pein.last_name,
            pein.position,
            usr.company,            
            pr.*,pr.id AS proId, "
                . "restCare.title as restcare_name,pr.id as pr_id, extinfo.*,extinfo.id as ex_id,pr.modified as last_update,usr.review_friendly AS head_office_review_friendly,pr.review_friendly AS facility_review_friendly, usr.review_friendly_text ,usr.review_friendly_detail "
                . " FROM " . _prefix("products") . " AS pr "
                //. " Left join " . _prefix("services") . " AS sr ON pr.facility_type=sr.id"
                //. " Left join " . _prefix("cities") . " AS city ON pr.city_id=city.id"
                //. " Left join " . _prefix("suburbs") . " AS suburb ON pr.suburb_id=suburb.id"
                . " Left join " . _prefix("rest_cares") . " AS restCare ON pr.restcare_id = restCare.id"
                //. " Left join " . _prefix("enquiries") . " AS enq ON pr.id = enq.prod_id"
                . " Left join " . _prefix("extra_facility") . " AS extinfo ON extinfo.product_id = pr.id"
                . " Left join " . _prefix("feedbacks") . " AS fdbk ON fdbk.product_id = pr.id  AND fdbk.deleted = 0"
                . " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id = pr.id"
                . " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id = pr.id AND prpn.current_plan = 1"
                . " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id = memp.id"
                . " Left join " . _prefix("users") . " AS usr ON usr.Id = pr.supplier_id"
                . " where pr.id='$productID' ";

        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);

        if (count($records)) {
            foreach ($records as $record) {
                $proId = $record['proId'];
                $quick_url = $record['quick_url'];
                $supp_id1 = $record['supplier_id'];  ///supplier id
                $id = $record['proId'];
                $ex_id = $record['ex_id'];
                $pr_id = $record['pr_id'];
                $city_id = $record['city_id'];
                $facility_review_friendly = $record['facility_review_friendly'];
                $head_office_review_friendly = $record['head_office_review_friendly'];
                $head_office_review_friendly_text = $record['review_friendly_text'];
                $head_office_review_friendly_detail = $record['review_friendly_detail'];
                $review_friendly = '';
                //filter
                $head_office_review_friendly_text = str_ireplace('"', '&quot;', $head_office_review_friendly_text);
                $head_office_review_friendly_detail = str_ireplace('"', '&quot;', $head_office_review_friendly_detail);
                if ($head_office_review_friendly == 1) {
                    // message
                    $review_friendly = '<p class="text-center" style="color:green;">-- This provider welcomes reviews. --</p>';
                }

                if ($head_office_review_friendly == 2) {
                    // message
                    $review_friendly = '<p class="text-center" style="color:red;">-- This provider does not welcome unsolicited feedback. --</p>';
                }

                $tooltip = '';
                if ($head_office_review_friendly_detail) {
                    $tooltip = ' <i class="fa fa-question-circle" data-toggle="tooltip" title="" data-original-title="' . $head_office_review_friendly_detail . '"></i>';
                }

                if (strlen($head_office_review_friendly_text) > 1 && $head_office_review_friendly == 2) {
                    // message
                    $review_friendly = '<p class="text-center" style="color:red;">' . $head_office_review_friendly_text . $tooltip . '</p>';
                }




                if ($facility_review_friendly == 1) {
                    // message
                    $review_friendly = '<p class="text-center" style="color:green;">-- This facility welcomes reviews. --</p>';
                    $review_friendly_badge = '<div class="review-friendly"> </div>';
                }


                $product_id = $pr_id;
                $title = $record['title'];
                $rank = $record['rank'];
                $award_text = $record['award_text'];
                $description = $record['description'];
                $description_our_version = $record['description_our_version'];
                $last_update = $record['last_update'];
                $keyword = $record['keyword'];
                $audit_url = $record['audit_url'];
                $image = $record['image'];
                $ext_url = $record['ext_url'];
                $youtube_video = $record['youtube_video'];
                $facility = $record['facility_type'];
                $address = $record['address'];
                $address_city = strip_tags(stripcslashes($record['address_city']));
                $address_suburb = strip_tags(stripcslashes($record['address_suburb']));
                $restcare_name = strip_tags(stripcslashes($record['restcare_name']));
                $zip = $record['zip'];
                $staff_comment = $record['staff_comment'];
                $management_comment = $record['management_comment'];
                $activities_comment = $record['activity_comment'];
                $staff_image = $record['staff_image'];
                $management_image = $record['management_image'];
                $activities_image = $record['activity_image'];
                $brief = $record['brief'];
                $noOfRooms = $record['no_of_room'];
                $noOfBeds = $record['no_of_beds'];
                $long = $record['longitude'];
                $lat = $record['latitude'];
                $company = $record['company'];
                $start_appointmnet_date = date('Y-m-d', strtotime($record['start_appointment_date']));
                $endt_appointmnet_date = date('Y-m-d', strtotime($record['end_appointment_date']));
                $position = $record['position'];
                $email = $record['email'];
                $phone = $record['phone'];
                $first_name = $record['first_name'];
                $last_name = $record['last_name'];
                $position = $record['position'];
                $dhb_name = $record['dhb_name'];
                $certification_period = $record['certification_period'];
                $current_auditor = $record['current_auditor'];
                $certificate_license_end_date = $record['certificate_license_end_date'];
                $certification_service_type = $record['certification_service_type'];
                $logo = $record['logo'];
                $quality_of_care = $record['av_quality_of_care'] * 20;
                $caring_staff = $record['av_caring_staff'] * 20;
                $avg_rating = $record['av_avg_rating'] * 20;
                $responsive_management = $record['av_responsive_management'] * 20;
                $trips_outdoor_activities = $record['av_trips_outdoor_activities'] * 20;
                $indoor_entertainment = $record['av_indoor_entertainment'] * 20;
                $social_atmoshphere = $record['av_social_atmosphere'] * 20;
                $enjoyable_food = $record['av_enjoyable_food'] * 20;
                $brief = str_ireplace("\n", "<br>", $brief);
                $description = str_ireplace("\n", "<br>", $description);
                $description_our_version = str_ireplace("\n", "<br>", $description_our_version);
                $staff_comment = str_ireplace("\n", "<br>", $staff_comment);
                $management_comment = str_ireplace("\n", "<br>", $management_comment);
                $activity_comment = str_ireplace("\n", "<br>", $activity_comment);
                $address_other = str_ireplace("\n", "<br>", $address_other);
                $address = str_ireplace("\n", "<br>", $address);
                $last_update_sec = strtotime($last_update);

                if ($last_update_sec > 0) {
                    $last_update = "(Last Updated: " . date('jS F Y', $last_update_sec) . ")";
                } else {
                    $last_update = '';
                }
                // permissions for viewing details
                $logo_enable = $record['logo_enable'];
                $map_enable = $record['map_enable'];
                $address_enable = $record['address_enable'];
                $phone_number_enable = $record['phone_number_enable'];
                $video_enable = $record['video_enable'];
                $video_limit_enable = $record['video_limit_enable']; // first x times videos
                $youtube_video_enable = $record['youtube_video_enable'];
                $image_enable = $record['image_enable'];
                $facility_desc_enable = $record['facility_desc_enable'];
                $staff_enable = $record['staff_enable'];
                $management_enable = $record['management_enable'];
                $activity_enable = $record['activity_enable'];
                $ext_url_enable = $record['ext_url_enable'];
                $image_limit = $record['image_limit']; // first x times images
                $product_limit = $record['product_limit']; // not used        
            }// FOR EACH
        }// Count record



        return array("<a href=\"https://www.agedadvisor.nz/" . $quick_url . "\" target=\"_blank\">$title</a><br>", $first_name);
    }// if productID exists
}

// End function

global $db;

function dbconnecti() {
    global $db;
    return $db;
}

//echo "hello:";
$filetype = '';
$focus = '';
$thistitle = '';
$competition = '';
$fancybox = TRUE;
$notify_message = '';
$alert_message = '';
$additional_jquery = '';
// Set USERS, LANGUAGES, CURRENCIES, BASKET
$userId = '';

if (isset($_SESSION['userId']) && !empty($_SESSION['userId'])) {
    $userId = $_SESSION['userId'];
} else if (isset($_SESSION['csId']) && !empty($_SESSION['csId'])) {
    $userId = $_SESSION['csId'];
}

$userId = '';

$usernumber = $userId;


$filetype = '';
$focus = '';
$thistitle = '';
$competition = '';

// Set USERS, LANGUAGES, CURRENCIES, BASKET

function show_messages($alert_message = '', $notify_message = '') {
// remove the last <br>

    if (trim($alert_message)) {
        echo '
                    <div class="row">
                        <div class="col-md-12"><?if(!$sent_a_new_password){?>
							<div class="alert alert-block">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
 ' . create_ul_list($alert_message) . '
						 	</div>
						</div>
					</div>';
    }
    if (trim($notify_message)) {
        echo '
                    <div class="row">
                        <div class="col-md-12"><?if(!$sent_a_new_password){?>
							<div class="alert alert-block alert-info">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
 ' . create_ul_list($notify_message) . '
 							</div>
 						</div>
 					</div>';
    }
}

// end show_messages
// turns messages in bullet lists
function create_ul_list($text) {
    $list = '<ul class="list-style">';
    $words = explode('<br>', $text);
    foreach ($words as $item) {
        $item = trim($item);
        if ($item) {
            $list .= '<li>' . $item . '</li>';
        }
    }
    $list = $list . '</ul>';

    return $list;
}

$allowededit = FALSE;
$strIP = $_SERVER['REMOTE_ADDR'];

function adjustcompetitions($id) {
    $notify_message = '';
    $sql = "select * from";
    $sql .= " competition where status in (0,1,2,3) AND ID='$id' order by status,enddate desc";

    $GLOBALS['DB'] = dbconnecti();
    $result = mysqli_query($db->db_connect_id, $sql) or die("Error getting list");
    ;
    while ($myrow = mysqli_fetch_row($result)) {
        $strID = $myrow[0];
        $strname = $myrow[1];
        $strdescription = $myrow[3];
        $strdescription = str_replace("\n", "<br>", $strdescription);

        $strstartdate = $myrow[4];
        $strenddate = $myrow[5];
        $strvotedate = $myrow[6];
        $mediaallowed = $myrow[7];
        $strstatus = $myrow[8];
        $newstatus = $strstatus;

        if ($strstatus == 0) { // Has it started?
            if (time() > (strtotime("$strstartdate"))) {
                $newstatus = 1;
            }
        } // end of has it started
        if ($strstatus == 1) { // voting?
            if (time() > (strtotime("$strvotedate") + 86400)) {
                $newstatus = 2;
            }
        } // end of start of voting
        if ($strstatus == 2) { // voting closed?
            $time_left_to_vote = ((strtotime("$strenddate") + 86400) - time()) / 60; //minutes
            if ($time_left_to_vote < 2880) {
                $notify_message = "Voting for $strname finishes in " . floor($time_left_to_vote / 1440) . " days.";
            }
            if ($time_left_to_vote < 1440) {
                $notify_message = "Voting for $strname finishes in " . floor($time_left_to_vote / 60) . " hours.";
            }
            if ($time_left_to_vote < 60) {
                $notify_message = "Voting for $strname finishes in " . ceil($time_left_to_vote) . " minutes.";
            }



            if (time() > (strtotime("$strenddate") + 86400)) {
                $newstatus = 3;
            }
        } // end of stop voting



        if ($strstatus !== $newstatus) { // we must update to new status
            $GLOBALS['DB'] = dbconnecti();
            $sqlinc = "update competition set status='$newstatus' where ID='$strID'";
            mysqli_query($db->db_connect_id, $sqlinc) or die("Error updating status of competition");
        } //end of update the status
    } // on to next competition
    return $notify_message;
}

// end of function adjustcompetitions

function howmanyentries($competition) {
// find out how many times anyone has entered this competition
    $sql = "select ID ";
    $sql .= "FROM competition_media ";
    $sql .= "WHERE competitionID=$competition";
    $GLOBALS['DB'] = dbconnecti();
    $result = mysqli_query($db->db_connect_id, $sql);
    if ($result) {  //  if (mysqli_num_rows($result)>0){
        $numresults = mysqli_num_rows($result);
    }
    return $numresults;
}

function getnamestring($userID) {
    return getubbname($userID);
}

function getubbname($userID) {
    global $db;

    $sql_query = "select first_name   from ad_users where id='$userID' ";
//echo "$sql_query";
    $res = mysqli_query($db->db_connect_id, $sql_query);

    $records = mysqli_fetch_assoc($res);
    $rows = mysqli_num_rows($res);
    if ($rows > 0) {
        $name = $records['first_name'];
    }

    return $name;
}

function printdate($strdate) {
    echo date("d F Y", strtotime("$strdate"));
}

// Require the tracking library

include ($_SERVER['DOCUMENT_ROOT'] . '/modules/competition/view-all-pictures.php');

$usernumber = 0;



if (isset($_SERVER['PATH_INFO'])) {
    $pathdetail = $_SERVER['PATH_INFO'];
// remove first forward slash
    $pathdetail = str_replace('/', '', $pathdetail);
    $viewcode = explode('-', $pathdetail);
    $num = count($viewcode);
    if ($num > 0) {
        $competition = $viewcode[0];
    }
    if ($num > 1) {
        $filetype = $viewcode[1];
        $focus = $filetype;
    }
}



// Clean and test variables in Header for security
if (!is_numeric($competition)) {
    echo"Access denied - incorrect data";
    exit;
}
if (!is_numeric($filetype) && $filetype) {
    echo"Access denied - incorrect data";
    exit;
}

// Show system variables
//echo " $ competition=".$competition." $ usernumber=".$usernumber."<br>";
if ($competition !== 0) {
    $GLOBALS['DB'] = dbconnecti();
    $sql = "SELECT * FROM competition where ID=$competition AND status in (1,2,3)";
    $result = mysqli_query($db->db_connect_id, $sql) or die("Error getting competition details");
    if ($result) {
        $myrow = mysqli_fetch_row($result);
        $competition_id = $myrow[0];
        $name = $myrow[1];
        $description = $myrow[3];
        $description = str_replace("\n", "<br>", $description);

        $entriesclose = $myrow[6];
        $strstatus = $myrow[8];

        $mediaallowed = $myrow[7];
        $rules = $myrow[9];
        $maximumentries = $myrow[10];
        $small_advert = $myrow[12];
        $banner_advert = $myrow[13];
    }
}
if (!$name) {
    echo"Access denied - no such competition";
    exit;
}//
// Voting script here
if (isset($_POST['enterdraw']) && $strstatus == '2') {

    $strIP = $_SERVER['REMOTE_ADDR'];


    $boolEntry = FALSE; // they have not voted
    $entry_number = $_POST['entry_number'];
    if (!is_numeric($entry_number)) {
        echo"Access denied - incorrect data";
        exit;
    }

    $sqlq = "Select * from competition_votes where (cookie='" . $GLOBALS['strBasketID'] . "' OR IP = '$strIP' ) and competitionID='$competition' and media='$entry_number'";
    $result = mysqli_query($db->db_connect_id, $sqlq) or die("Error checking user IP");
    if ($result) {
        if (mysqli_num_rows($result) > 0) {
            // Yes they have voted from this userID
            $alert_message .= 'You have already voted for this entry';
        } else {
            $notify_message .= 'Thank you for your vote.<br>';
            $GLOBALS['DB'] = dbconnecti();
            $sqlinc = "update competition_media set votes=votes+1 where ID='$entry_number'";
            mysqli_query($db->db_connect_id, $sqlinc) or die("Error increasing votes on media");
            $boolEntry = true;
// add them into the voted database
            $GLOBALS['DB'] = dbconnecti();
            $sql = "insert into competition_votes (media,userID,IP,competitionID,cookie) values ('$entry_number','$usernumber','$strIP','$competition','" . $GLOBALS['strBasketID'] . "')";
            mysqli_query($db->db_connect_id, $sql) or die("Error adding to the media list");

            //$strfile="https://www.homeofpoi.com/admin/CompetitionAdmin/fix_votes_instant.php?competition=".$competition;
//echo $strfile;exit;
            //$html = implode('', file($strfile));
        }
    }
}

$notify_message .= adjustcompetitions($competition_id);
$header_title = $name;

$responsivetabs = FALSE;
$home_banner = FALSE;
$feature_slider = FALSE;
$feature_slider = FALSE;
$breadcrumbs = $header_title;
//==================================
// Show list of media files entries that can be downloaded/viewed
//==================================
//Show list of media files that can be downloaded/viewed
$GLOBALS['DB'] = dbconnecti();
$imbeddedimage = "";
$insert = '';
$output = "";
$counting = 0;
$sql = "SELECT ID,title,userID,competitionID,votes,views,file,dateadded,length,type,file_medium,file_small,description,approved,productID FROM competition_media WHERE competitionID='$competition' ORDER BY views ASC, ID desc";
if ($strstatus == 3) {
    $sql = "SELECT ID,title,userID,competitionID,votes,views,file,dateadded,length,type,file_medium,file_small,description,approved,productID FROM competition_media WHERE competitionID='$competition'   AND (approved>0 OR userID='$usernumber')ORDER BY votes desc";
}

$they_are_all_pictures = TRUE;
//echo $sql;exit;
$result = mysqli_query($db->db_connect_id, $sql) or die("$sql Error finding entries");
if ($result) {
    if (mysqli_num_rows($result) > 0) {
        $mediatype = "";
        while ($myrow = mysqli_fetch_row($result)) {
            $votes = $myrow[4];

            if ($myrow[9] == 10) {// it is a jpg,gif
            } else {
                $they_are_all_pictures = FALSE;
            }

// Give owner of file option to edit/delete their media
//echo "$usernumber==".$myrow[2];exit;
            if ($usernumber == $myrow[2] || $allowededit) {
                $editlink = '[<a href="https://www.agedadvisor.nz/edit_media?item=' . $myrow[0] . '&amp;comp=' . $competition . '">Edit</a>]<br>';
            } else {
                $editlink = "";
            }



            $productID = $myrow[14];
            $description = $myrow[12];
            $approved = $myrow[13];
            if ($description) {
                $description = "&quot;$description&quot;";
            }

            if ($strstatus == 3) {
                $counting = $counting + 1;
                $insert = "No. $counting &nbsp;&nbsp;&nbsp;";
            }

//Diplay each media file
            $outputlink = $insert . '<b><a href="https://www.agedadvisor.nz/view_media?media=' . $myrow[0] . '" target="_blank" title="click here to download/view">' . $myrow[1] . '</a></b>';
// we embed this if it is first image file
            if ($myrow[9] < 11 || $myrow[9] == 13) {// it is a mopvie or jpg or gif
                $outputlink = $insert . '<b><a href="https://www.agedadvisor.nz/view-all-entries/' . $competition . '-' . $myrow[0] . '" title="click here to view">' . $myrow[1] . '</a></b>';
                if ($imbeddedimage == "" && $filetype == 0) {

                    $thistitle = '<h2>&quot;' . $myrow[1] . '&quot;</h2><i>' . $myrow[12] . '</i><br>Entered by ' . getnamestring($myrow[2]) . ' ';
                    $thistitle .= 'Submitted on ' . $myrow[7] . ' ';
                    $thistitle .= 'Views:' . $myrow[5] . ' ';
                    if ($votes > 0 && $strstatus < 3) {
                        $thistitle .= 'Votes:' . $votes . ' ';
                    }
                    if ($strstatus == '2') {
                        $sqlq = "Select * from competition_votes where (cookie='" . $GLOBALS['strBasketID'] . "' OR IP = '$strIP' ) and competitionID='$competition' and media='$myrow[0]'";
                        //echo $sqlq;exit;
                        $resultq = mysqli_query($db->db_connect_id, $sqlq) or die("Error checking user IP");
                        if ($resultq) {
                            if (mysqli_num_rows($resultq) > 0) {
                                // Yes they have voted from this userID
                                $thistitle .= '<br>You have already voted for this entry';
                            } else {
                                $thistitle .= '<br>
        <form method="post" action="https://www.agedadvisor.nz/view-all-entries/' . $competition . '-' . $myrow[0] . '">
        <input type="hidden" name="enterdraw" value="enterdraw">
        <input type="hidden" name="entry_number" value="' . $myrow[0] . '">
        <div class="text-center"><input style="width:300px;" class="btn btn-danger" id="button_upload" type="submit" value="Click here to vote"></div>
        </form>';
                            }
                        }
                    }




//$imbeddedimage=$editlink.'<img src="https://d2afwm0z543sjd.cloudfront.net/'.$myrow[10].'" border="0" alt="'.$myrow[1].'" title="'.$myrow[1].' uploaded by '.getnamestring($myrow[2]).'">';
                    $imbeddedimage = $editlink . '<img src="https://www.agedadvisor.nz/images/competition/' . $myrow[10] . '" border="0" alt="' . $myrow[1] . '" title="' . $myrow[1] . ' uploaded by ' . getnamestring($myrow[2]) . '">';


// increase file views
                    $GLOBALS['DB'] = dbconnecti();
                    $sqlinc = "update competition_media set views=views+1 where ID='$myrow[0]'";
                    mysqli_query($db->db_connect_id, $sqlinc) or die("Error increasing views of GIF or JPG media");

                    $thistitle = '<h2>&quot;' . $myrow[1] . '&quot;</h2><i>' . $myrow[12] . '</i><br>Entered by ' . getnamestring($myrow[2]) . ' ';
                    $thistitle .= 'Submitted on ' . $myrow[7] . ' ';
                    $thistitle .= 'Views:' . $myrow[5] . ' ';
                    if ($votes > 0 && $strstatus < 3) {
                        $thistitle .= 'Votes:' . $votes . ' ';
                    }
                    if ($strstatus == '2') {
                        $sqlq = "Select * from competition_votes where (cookie='" . $GLOBALS['strBasketID'] . "' OR IP = '$strIP' ) and competitionID='$competition' and media='$myrow[0]'";
                        $resultq = mysqli_query($db->db_connect_id, $sqlq) or die("Error checking user IP");
                        if ($resultq) {
                            if (mysqli_num_rows($resultq) > 0) {
                                // Yes they have voted from this userID
                                $thistitle .= '<br>You have already voted for this entry';
                            } else {
                                $thistitle .= '<br>
        <form method="post" action="https://www.agedadvisor.nz/view-all-entries/' . $competition . '-' . $myrow[0] . '">
        <input type="hidden" name="enterdraw" value="enterdraw">
        <input type="hidden" name="entry_number" value="' . $myrow[0] . '">
        <div class="text-center"><input style="width:300px;" class="btn btn-danger" id="button_upload" type="submit" value="Click here to vote"></div>
        </form>';
                            }
                        }
                    }



                    $outputlink = $insert . '<b><font color="blue">&gt; &gt; ' . $myrow[1] . '</font></b> *viewing now.';
                }
                if ($filetype !== 0 && $filetype == $myrow[0]) {
                    $thistitle = '&quot;' . $myrow[1] . '&quot; Entered by ' . getnamestring($myrow[2]);
                    $imbeddedimage = $editlink . '<img src="https://www.agedadvisor.nz/images/competition/' . $myrow[10] . '" border="0" alt="' . $myrow[1] . '" title="' . $myrow[1] . ' uploaded by ' . getnamestring($myrow[2]) . '">';

// increase file views
                    $GLOBALS['DB'] = dbconnecti();
                    $sqlinc = "update competition_media set views=views+1 where ID='$myrow[0]'";
                    mysqli_query($db->db_connect_id, $sqlinc) or die("Error increasing views of GIF or JPG media");

                    list($facility_data, $first_name) = facility_details($productID);

                    $submitted_by_first_name = getnamestring($myrow[2]);
                    if (!$submitted_by_first_name) {
                        $submitted_by_first_name = $first_name;
                    }

                    $thistitle = '<h2 style="margin-bottom:0px;">' . $myrow[1] . '</h2>' . $facility_data . '<span class="hidden-phone"><i>' . $myrow[12] . '</i><br></span><span style="font-size: 12px;">Entered by ' . $submitted_by_first_name . '</span> ';
                    $thistitle .= '<span style="font-size: 12px;" class="hidden-phone">&nbsp;&nbsp;|&nbsp;&nbsp;' . $myrow[7] . ' ';
                    $thistitle .= '&nbsp;&nbsp;|&nbsp;&nbsp;Views:' . $myrow[5] . ' </span>';
                    if ($votes > 0 && $strstatus < 3) {
                        $thistitle .= '<br>Votes:' . $votes . ' ';
                    }
                    if ($strstatus == '2') {
                        $sqlq = "Select * from competition_votes where (cookie='" . $GLOBALS['strBasketID'] . "' OR IP = '$strIP' ) and competitionID='$competition' and media='$myrow[0]'";
                        $resultq = mysqli_query($db->db_connect_id, $sqlq) or die("Error checking user IP");
                        if ($resultq) {
                            if (mysqli_num_rows($resultq) > 0) {
                                // Yes they have voted from this userID
                                $thistitle .= '<br>You have already voted for this entry';
                            } else {
                                $thistitle .= '<br>
        <form method="post" action="https://www.agedadvisor.nz/view-all-entries/' . $competition . '_' . $myrow[0] . '">
        <input type="hidden" name="enterdraw" value="enterdraw">
        <input type="hidden" name="entry_number" value="' . $myrow[0] . '">
        <div class="text-center"><input style="width:300px;" class="btn btn-danger" id="button_upload" type="submit" value="Click here to vote"></div>
        </form>';
                            }
                        }
                    }
                    $outputlink = '<b><font color="blue">' . $insert . '&gt; &gt; ' . $myrow[1] . '</font></b> *viewing now.';
                }
            }
            $output .= $outputlink;
            $output .= '<br>';
        }
    } else {
        $output .= 'None';
    }
}



//==================================
// End of Create list of media Files
//==================================

echo $thistitle;
//show_messages($alert_message,$notify_message);// Show any regular or important messages
//echo $imbeddedimage;

$share_url = "https://www.agedadvisor.nz/view-all-entries/" . $competition;
if ($focus) {
    $share_url .= "_$focus";
}
$share_url = urlencode($share_url);
$normal_href = 'https://www.agedadvisor.nz/view-all-entries/' . $competition . '-' . $focus;
?><br><div class="text-center"><a style="margin-bottom:3px;width:300px;" class="btn btn-info btn-mini " href="<?php echo  $normal_href ?>#image"><i class="icon icon-share"></i>Open in main window / Link to share</a></div>
