<div class="container">
    <div class="row articles_container">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <?php
                        // to submit comment
                        if (isset($submit) && $submit == 'submit') {
                            if (isset($_SESSION['id'])) {
                                // $blogId = isset($_GET[blogId]) ? $_GET['blogId'] : '';
                                $fields_array = comment();
                                $response = Validation::_initialize($fields_array, $_POST);
                                if (isset($response['valid']) && $response['valid'] > 0) {
                                    $tmp = strip_tags($_POST["comment"], '&nbsp;');
                                    $_POST['comment'] = trim($tmp, '&nbsp;') == '' ? '' : $_POST["comment"];
                                    $fields = array(
                                        'title' => $_POST['comment'],
                                        'type' => 1,
                                        'created' => date('Y-m-d H:i:s'),
                                        'modified' => date('Y-m-d H:i:s'),
                                        'created_by' => $_SESSION['id']
                                    );
                                    $insert_result = $db->insert(_prefix('blogs'), $fields);
                                    if ($insert_result) {
                                        // Message for insert
                                        $msg = common_message(1, constant('INSERT'));
                                        $_SESSION['msg'] = $msg;
                                        //ends here
                                        redirect_to(HOME_PATH . "forum");
                                    }
                                } else {
                                    $errors = '';
                                    foreach ($response as $key => $message) {
                                        $error = true;
                                        $errors .= $message . "<br>";
                                    }
                                }
                            } else {
                                // echo '<script> alert("Please login to comment on this post")</script>';
                                $url = HOME_PATH . 'supplier/login';
                                redirect_to($url);
                            }
                        }


                        //end submit comment
                    ?>

                    <!--MAIN BODY CODE STARTS-->

                    <!---left section starts-->
                    <div class="col-md-9 col-lg-9 col-sm-12 col-xs-12 pull-left container_wrapper" style="margin-top:45px;">
                        <div class="posted_col" style="border:none;">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4 class="comment_h4">Post Question</h4>
                                <form class="form-group" action="" method="post" id="blog_comment_reply">
                                    <textarea name="comment" class="form-control post_mail required" placeholder="Post your question..."></textarea>
                                    <input type="text" tabindex="11"  name="captcha" placeholder="Security Code" id="captcha-text" class="form-control input_fild">
                                    <div id="captcha-wrap" class="col-md-12 padding_none">
                                        <img src="<?php echo HOME_PATH ?>admin/captcha/captcha.php" alt="" id="captcha"/>
                                        <img src=" <?php echo HOME_PATH ?>images/captcha_refresh.gif" alt="refresh captcha" id="refresh-captcha" />
                                    </div>
                                    <button type="submit" name="submit" value="submit" class="btn btn-danger inner_btn comment_btn pull-right">SUBMIT</button>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!---right section starts-->
                    <div class="col-md-3 col-lg-3 col-sm-12 col-xs-12 pull-right padding_right article_innerbg">
                        <h2 class="hedding_tow">New Articles:</h2>
                        <ul class="list-unstyled right_nav article_nav">
                            <?php echo getArticles(); ?>
                            <div class="viewall"><a href="<?php echo HOME_PATH; ?>articles">View All</a></div>
                        </ul>
                        <h2 class="hedding_tow">Community<br>
                            Questions &amp; Answers</h2>
                        <ul class="list-unstyled right_nav article_nav">
                            <?php echo getBlogs(); ?>
                            <div class="viewall"><a href="<?php echo HOME_PATH; ?>forum">View All</a></div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MAIN BODY CODE ENDS-->
<script>
    //$('#blog_comment_reply').validate();
    var path = "<?php echo HOME_PATH; ?>admin/";
    $('#refresh-captcha').on('click', function() {
        $('#captcha').attr("src", path + "captcha/captcha.php?rnd=" + Math.random());
    });
    $('#blog_comment_reply').validate({
        rules: {
            captcha: {
                required: true,
                remote:
                        {
                            url: path + 'captcha/checkCaptcha.php',
                            type: "post",
                            data:
                                    {
                                        code: function()
                                        {
                                            return $('#captcha-text').val();
                                        }
                                    }
                        }
            }
        },
        messages: {
            captcha: {
                required: "Please enter the verifcation code.",
                remote: "Verication code incorrect, please try again."
            },
        }
    });
</script>
