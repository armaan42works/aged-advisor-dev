<?php
error_reporting(E_All);
if(!isset($_SESSION)){
	session_start();
}
require_once '../../application_top.php';

global $db;$add_subject='';



function get_userid( $email = '', $phone = '', $uname = '' ) {
    global $db;
    $userid = '';
    $email = trim($email);
    $phone = trim($phone);
    
    if ( $phone ) {
        $sql_query = "select id from ad_users where phone = '$phone' ";
    }
    if ( $email && $email != 'n/a' ) {
        $sql_query = "select id  from ad_users where email  = '$email'  ";
    }
    
    $res     = mysqli_query($db->db_connect_id,  $sql_query );
    $records = mysqli_fetch_assoc( $res );
    $rows    = mysqli_num_rows( $res );
    if ( $rows > 0 ) {
        $userid = $records[ 'id' ];
    }
    return $userid;
}

function get_count_of_enquiries($userId, $prod_id, $count_for_days = 30){
    // this function will check how many enquiries a user did in past $count_for_days 
    global $db;
    $userId = htmlspecialchars($userId);
    $time_of_enquiry = '';
    $sql     = "select * from ad_enquiries where user_id = '$userId' and prod_id = '$prod_id' order by id desc limit 0,1";   
    $result  = mysqli_query($db->db_connect_id, $sql);
    $records = mysqli_fetch_assoc( $result );
    $rows    = mysqli_num_rows( $result );
    $ip = $_SERVER['REMOTE_ADDR'];        
    if($rows){                
        $time_of_enquiry = $records['time_of_enquiry'];
    }
    return $time_of_enquiry;
}

//$con = mysqli_connect( "localhost", "agedadvisor", "f3Cdc7~3" ) or die( "error in connection" );
//mysqli_select_db( "new_agedadvisor_live", $con );

$enquirer_id        = trim($_REQUEST[ 'enquirer_id' ]);
$uname              = trim($_REQUEST[ 'enquirer_name' ]);
$uemail             = trim($_REQUEST[ 'enquirer_email' ]);
$prId               = trim($_REQUEST[ 'prod_id' ]);
$vacanncy_tel       = trim($_REQUEST[ "enquirer_tel" ]);
$opt_in             = $_REQUEST[ "opt_in" ];
$msg                = $_REQUEST[ "enq_specifics" ];
$valid_mobile_count = strlen( trim( $vacanncy_tel ) );
//$valid_email        = preg_match( "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $uemail );
if(filter_var($uemail, FILTER_VALIDATE_EMAIL) ){
    $valid_email = 1;
}
// found user
$founduser = get_userid( $uemail, $vacanncy_tel );


if( $founduser ){
    $time_of_enquiry = get_count_of_enquiries($founduser, $prId);    

    if( $time_of_enquiry != '' ){
        $time_of_enquiry = date("F j, Y, g:i a", strtotime($time_of_enquiry));
        $response = ['data'=>$time_of_enquiry];
        echo json_encode($response);
        exit();
    } else {
        $response = ['data'=>'ProdNotFound'];
        echo json_encode($response);
        exit();
    }
} else {
    $response = ['data'=>'ProdNotFound'];
    echo json_encode($response);
    exit();
}

?>