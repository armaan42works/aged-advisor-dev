
<script type="text/javascript" src="<?php echo HOME_PATH;?>assets/plugins/gmap/gmap.js"></script>
<script type="text/javascript" src="https://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerwithlabel/src/markerwithlabel.js"></script>

<style>
.google-map {margin:20px 0;border: 4px #e67e22 solid;width:100% !important; height:400px !important;}
.map-infow h5{font-weight:bold;text-transform:capitalize;border-bottom:2px solid #F15922;color:#042E6F;}
.map-infow p {margin:0;}
.labels {
  color: white;
  background-color: red;
  font-family: "Lucida Grande", "Arial", sans-serif;
  font-size: 20px;
  text-align: center;
  width: 40px;
  white-space:nowrap;
}
</style> 
      <div class="google-map-wrap" itemscope itemprop="hasMap" itemtype="http://schema.org/Map">
                <div id="map_canvas" class="google-map"></div>
                <!-- #google-map -->

            </div>

                <?php /* === MAP DATA === */ ?>
                <?php
                
                $ltd[] = array();
                $i=0;
                
                $locations = array();
                foreach($data as $info){
                    $latitude = $ltd[$i]["latitude"]=$info["latitude"];
                    $longitude = $ltd[$i]["longitude"]=$info["longitude"];
                    $ltd[$i]["title"]=$info["title"];
                    $ltd[$i]["id"]=$info["id"];
                    
                    $ltd[$i]["zip"]=$info["zip"];
                    $ltd[$i]["address"]=$info["address"];
                    $ltd[$i]["suburbTitle"]=$info["suburbTitle"];
                    $ltd[$i]["city"]=$info["city"];
                    $html='<h5>'.str_replace("'",'"',$info["title"]).'</h5>';
                    //$html .='<p>Average Rating:'.OverAllNEWRatingProduct($info['id']).'</p>';
          $html .='<p>Rating: '.OverAllNEWRatingProductForMap($info['id']).' <span>Reviews:'.str_replace("'",'"',overAllRatingProductCount($info['id'])).'</span></p>';
          
                    //$html .= '<p>Total reviews: <i class="fa fa-users"></i>  '.str_replace("'",'"',overAllRatingProductCount($info['id'])).'</p>';
                    $ltd[$i]["html"]='<div class="map-infow">'.$html.'</div>';
          $address = $info["address"].'+'.$info["city"].'+'.$info["zip"].'+'."New+Zealand";
          $prepAddr = str_replace(' ','+',$address);
          $prepAddr = str_replace('  ','+',$prepAddr);
          $prepAddr = str_replace('++','+',$prepAddr);
          //echo $prepAddr;
          
          $locations[] = array(
                    'google_map' => array(
                        'lat' => ''.$latitude.'',
                        'lng' => ''.$longitude.'',
                    ),
                    'location_address' => ''.$info["city"].'',
                    'location_name'    => ''.$ltd[$i]["html"].'',
                    'fulladdress' => $info["address"].','.$info["suburbTitle"].','.$info["city"].' '.$info["zip"].', New Zealand',
                    'title' => $info["title"]
                   );
           $i++;
                }   
            ?>


                <?php //print_r($locations); echo '<br>';/* === PRINT THE JAVASCRIPT === */ ?>

                <?php
                /* Set Default Map Area Using First Location */
                $map_area_lat = isset( $locations[0]['google_map']['lat'] ) ? $locations[0]['google_map']['lat'] : -43.513929;
                $map_area_lng = isset( $locations[0]['google_map']['lng'] ) ? $locations[0]['google_map']['lng'] : 172.65913;
               

                ?>

                
<script type="text/javascript"> 
jQuery( document ).ready( function($) {

    var  map;
    var bounds = [];
    
    <?php
   
  
    foreach( $locations as $location ){
                        $name = $location['location_name'];
                        $addr = $location['location_address'];
                        $map_lat = $location['google_map']['lat'];
                        $map_lng = $location['google_map']['lng'];
                        $title = $location["title"];
                  ?>  

    function initMap() {
            <?php 
                    if(!empty($map_lat)){
                        ?>
                      
               var latlng = new google.maps.LatLng(<?php echo $map_lat; ?>, <?php echo $map_lng; ?>);
               alert("latlang"+latlng);
               
             var map = new google.maps.Map(document.getElementById('map_canvas'), {
                      zoom: 10,
                   center: latlng
                   });

            // var i=1;
            var marker = new MarkerWithLabel({
                   position:latlng,
                   draggable: false,
                   raiseOnDrag: true,
                   map:map,
                   labelContent:"test",
                   labelAnchor: new google.maps.Point(22, 0),
                   labelClass: "labels", // the CSS class for the label
                   labelStyle: {opacity: 0.75}
            });
             
             var bounds = new google.maps.LatLngBounds();

                  bounds.extend(latlang);
                //bounds.extend(Item_1);
                map.fitBounds(bounds);
             

         

      <?php } ?>
         // alert(bounds); 
    }
 
    
<?php 
} ?>
  google.maps.event.addDomListener(window, 'load', initMap);
   // map.fitBounds(bounds);
});
          
</script>