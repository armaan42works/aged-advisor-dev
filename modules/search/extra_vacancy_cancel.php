<?php
include("../../application_top.php");
ini_set('display_errors', 1); 
global $db;

$base_url            = $_SERVER[ 'SERVER_NAME' ];
$ex_extra_facilities = $ex_sid = array();
$ex_facilities_id    = $_POST['extra_facilities_id'];
$ex_fname            = $_POST['fname'];
$ex_sid              = $_POST['sid'];
$ex_uscustomerid     = $_POST['uscustomerid'];
$ex_usname           = $_POST['usname'];
$ex_usmail           = $_POST['usmail'];
$ex_usphone          = $_POST['usphone'];
$ex_userid           = $_POST['usrid'];
$opt_in              = 0;
$today               = date( "Y-m-d H:i:s" );

function isValidEmail($email) {
    return filter_var($email, FILTER_VALIDATE_EMAIL) 
        && preg_match('/@.+\./', $email);
}

foreach ($ex_facilities_id as $key => $value) {
    $prId = $value;             
    
    $fields         = array(
        'user_id' => trim( $ex_userid ),
        'prod_id' => trim( $prId ),
        'Supplier_id' => trim( $ex_sid[$key] ),
        'facility_name' => trim( $ex_fname[$key] ),
        'time_of_enquiry' => $today 
    );
    $insert_result  = $db->insert( _prefix( 'enquiries' ), $fields );    

    $pro_query      = "select ad_products.*,ad_users.email AS headofficeemail ,ad_users.first_name AS headofficefname, ad_users.last_name AS headofficelname , "
                    . "ad_users.email_reviews_facility  , ad_users.email_reviews_head_office  , ad_users.agree_terms  , ad_users.phone AS headofficephone  "
                    . "from ad_products,ad_users where ad_products.id='$prId' AND ad_users.id = ad_products.supplier_id ";
    echo $pro_query;

    $pro_name_query = mysqli_query($db->db_connect_id, $pro_query );

    while ( $product_detail = mysqli_fetch_array( $pro_name_query ) ) {
        $headofficeemail           = $product_detail[ 'headofficeemail' ];
        $headofficefname           = $product_detail[ 'headofficefname' ];
        $headofficelname           = $product_detail[ 'headofficelname' ];
        $email_reviews_facility    = $product_detail[ 'email_reviews_facility' ];
        $email_reviews_head_office = $product_detail[ 'email_reviews_head_office' ];
        $agree_terms               = $product_detail[ 'agree_terms' ];
        $headofficephone           = $product_detail[ 'headofficephone' ];
        $product_name              = $product_detail[ 'title' ];
        $supplier_id               = $product_detail[ 'supplier_id' ];
        $product_url               = $product_detail[ 'quick_url' ];
        $quick_url                 = "agedadvisor.nz/" . $product_url;
        $user_name                 = "select  * from ad_pro_extra_info where id='$prId' ";
        $user_name1                = mysqli_query($db->db_connect_id, $user_name );       
        while ( $user_fullname = mysqli_fetch_array( $user_name1 ) ) {
            $user_fname = $user_fullname[ 'first_name' ];
            $user_lname = $user_fullname[ 'last_name' ];
            $user_email = $user_fullname[ 'email' ];
            if ( $user_fname ) {
                $user_name = $user_fname;
            } else {
                $user_name = $product_name . ' Admin';
            }
        }
        $upgrade2 = "select  * from ad_pro_plans where pro_id='$prId' AND current_plan=1";
        $upgrade1 = mysqli_query($db->db_connect_id, $upgrade2 );
        while ( $upgraded = mysqli_fetch_array( $upgrade1 ) ) {
            $upgrade = $upgraded[ 'plan_id' ];
            if ( $upgrade > 1 ) {
                $hasupgraded = TRUE;
            } else {
                $hasupgraded = FALSE;
            }
        }
    }


    if ( empty( $ex_usmail ) || empty( $ex_usname ) || empty( $ex_usphone )    ) {        
        echo '<font color="#FF0000">Please fill all the fields!</font>';
        exit();
    } else {           
            $to      = $user_email;
            $headers = "MIME-Version: 1.0\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\n";
            $headers .= 'From:   ';            
            $headers .= "reviews@agedadvisor.co.nz";           
            $headers .= "\n";
            $headers .= "Cc: $headofficeemail,reviews@agedadvisor.co.nz\r\n";
            if ( $hasupgraded ) {
                $emailTemplate = emailTemplate( 'Enq-availability' );
            } else {
                $emailTemplate = emailTemplate( 'Enq-availability-nonupgraded' );
            }
            if ( $emailTemplate[ 'subject' ] != '' ) {
                $sname      = substr( $ex_usname, 0, 3 ) . '..... .......';
                $semail     = substr( $ex_usmail, 0, 3 ) . '.......@.....';
                $smobile    = substr( $ex_usphone, 0, 5 ) . '.....';
                $message    = str_replace( array(
                    '{username}',
                    '{facility_url}',
                    '{name}',
                    '{facility_name}',
                    '{email}',
                    '{mobile}',
                    '{plan_id}',
                    '{sname}',
                    '{semail}',
                    '{smobile}' 
                ), array(
                     $user_name,
                    $quick_url,
                    $ex_usname,
                    $product_name,
                    $ex_usmail,
                    $ex_usphone,
                    $upgrade,
                    $sname,
                    $semail,
                    $smobile 
                ), stripslashes( $emailTemplate[ 'description' ] ) );
                $fQuery     = "SELECT `manager`,`email`,`phone` FROM `ad_pro_extra_info` WHERE pro_id='$prId'";
                $fQueryresL = $db->sql_query( $fQuery );
                $fcType     = $db->sql_fetchrowset( $fQueryresL );
                foreach ( $fcType as $fc ) {
                    $facilitymanager = $fc[ 'manager' ];
                    $facilityemail   = $fc[ 'email' ];
                    $facilityphone   = $fc[ 'phone' ];
                }
                $message = str_replace( array(
                     '{facilitymanager}',
                    '{facilityemail}',
                    '{facilityphone}',
                    '{headofficeemail}',
                    '{headofficefname}',
                    '{headofficelname}',
                    '{email_reviews_facility}',
                    '{email_reviews_head_office}',
                    '{agree_terms}',
                    '{headofficephone}' 
                ), array(
                     $facilitymanager,
                    $facilityemail,
                    $facilityphone,
                    $headofficeemail,
                    $headofficefname,
                    $headofficelname,
                    $email_reviews_facility,
                    $email_reviews_head_office,
                    $agree_terms,
                    $headofficephone 
                ), $message );
                $result  = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );
            }
            if ( $result ) {
                echo '1';
            } else {
                echo '<font color="#FF0000">Server Error!</font>';
            }
        
        
            $to      = 'reviews@agedadvisor.co.nz';
            $headers = "MIME-Version: 1.0\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\n";
            $headers .= 'From:   ';           
            $headers .= "$ex_usmail";            
            $headers .= "\n";
            $emailTemplate = emailTemplate( 'Enq-availability' );
            if ( $emailTemplate[ 'subject' ] != '' ) {
                $message    = str_replace( array(
                     '{username}',
                    '{facility_url}',
                    '{name}',
                    '{facility_name}',
                    '{email}',
                    '{mobile}',
                    '{plan_id}' 
                ), array(
                    $user_name,
                    $quick_url,
                    $ex_usname,
                    $product_name,
                    $ex_usmail,
                    $ex_usphone,
                    $upgrade 
                ), stripslashes( $emailTemplate[ 'description' ] ) );
                $fQuery     = "SELECT `manager`,`email`,`phone` FROM `ad_pro_extra_info` WHERE pro_id='$prId'";
                $fQueryresL = $db->sql_query( $fQuery );
                $fcType     = $db->sql_fetchrowset( $fQueryresL );
                foreach ( $fcType as $fc ) {
                    $facilitymanager = $fc[ 'manager' ];
                    $facilityemail   = $fc[ 'email' ];
                    $facilityphone   = $fc[ 'phone' ];
                }
                $message = str_replace( array(
                     '{facilitymanager}',
                    '{facilityemail}',
                    '{facilityphone}',
                    '{headofficeemail}',
                    '{headofficefname}',
                    '{headofficelname}',
                    '{email_reviews_facility}',
                    '{email_reviews_head_office}',
                    '{agree_terms}',
                    '{headofficephone}' 
                ), array(
                     $facilitymanager,
                    $facilityemail,
                    $facilityphone,
                    $headofficeemail,
                    $headofficefname,
                    $headofficelname,
                    $email_reviews_facility,
                    $email_reviews_head_office,
                    $agree_terms,
                    $headofficephone 
                ), $message );
                $result  = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );
            }
        
        
            $to      = $ex_usmail;
            $headers = "MIME-Version: 1.0\n";
            $headers .= "Content-type: text/html; charset=iso-8859-1\n";
            $headers .= 'From:   ';
            $headers .= "reviews@agedadvisor.co.nz";
            $headers .= "\n";
            $headers .= "Cc: reviews@agedadvisor.co.nz\r\n";
            if ( $hasupgraded ) {
                $emailTemplate = emailTemplate( 'Enq-availability-thanks' );
            } else {
                $emailTemplate = emailTemplate( 'Enq-availability-thanks-nonupgraded' );
            }
            if ( $emailTemplate[ 'subject' ] != '' ) {
                $message = str_replace( array(
                     '{username}',
                    '{facility_url}',
                    '{name}',
                    '{facility_name}',
                    '{email}',
                    '{mobile}',
                    '{plan_id}' 
                ), array(
                     $user_name,
                    $quick_url,
                    $ex_usname,
                    $product_name,
                    $ex_usmail,
                    $ex_usphone,
                    $upgrade 
                ), stripslashes( $emailTemplate[ 'description' ] ) );
                if ( $hasupgraded == FALSE ) {
                    $fQuery     = "SELECT `pro_id`,`certification_service_type`,`facility_type`,`address_suburb`,`address_city`,`zip` ,`facility_link` FROM `ad_products` WHERE md5(id)='$prId'";
                    $fQueryresL = $db->sql_query( $fQuery );
                    $fcType     = $db->sql_fetchrowset( $fQueryresL );
                    foreach ( $fcType as $fc ) {
                        $fpro_id                     = $fc[ 'pro_id' ];
                        $fCertification_service_type = $fc[ 'certification_service_type' ];
                        $fFacility_type              = $fc[ 'facility_type' ];
                        $fAddress_suburb             = $fc[ 'address_suburb' ];
                        $fAddress_city               = $fc[ 'address_city' ];
                        $fZip                        = $fc[ 'zip' ];
                        $other_facility_link         = $fc[ 'facility_link' ];
                    }
                    $similarFacilityQuery       = 'SELECT gall.file, fd.feedback, COUNT(fd.feedback) as count_feedback , pr.id, pr.quick_url,pr.title, pr.address_suburb, pr.address_city, fd.overall_rating,gall.file,gall.type FROM `ad_products` AS pr LEFT JOIN `ad_feedbacks` AS fd ON pr.id = fd.product_id LEFT JOIN `ad_galleries` as gall ON pr.id = gall.pro_id WHERE (`address_suburb` = "' . $fAddress_suburb . '" OR `address_city` = "' . $fAddress_city . '" OR `zip` = "' . $fZip . '") AND pr.quick_url <> "' . $prId . '" AND  gall.type=0 AND gall.file IS NOT NULL AND prod.id NOT IN($prId) AND pr.deleted=0 GROUP BY pr.id HAVING count_feedback >= 3 ORDER BY fd.overall_rating DESC LIMIT 5';
                    $similarFacilityQueryResult = mysqli_query($db->db_connect_id, $similarFacilityQuery );
                    if ( mysqli_num_rows( $similarFacilityQueryResult ) <= 0 ) {
                        $lat                  = -41.211722;
                        $long                 = 175.517578;
                        $similarFacilityQuery = "SELECT gall.file, fd.feedback, COUNT(fd.feedback) as count_feedback , pr.id, pr.quick_url,pr.title, pr.address_suburb, pr.address_city,pr.deleted,pr.latitude, pr.longitude, SQRT(POW(69.1 * (pr.latitude - ('$lat')), 2) + POW(69.1 * (('$long') - pr.longitude) * COS(pr.latitude / 57.3), 2)) AS `distance`, fd.overall_rating,gall.file,gall.type FROM `ad_products` AS pr LEFT JOIN `ad_feedbacks` AS fd ON pr.id = fd.product_id LEFT JOIN `ad_galleries` as gall ON pr.id = gall.pro_id GROUP BY pr.id HAVING distance < 500 and distance > 0  AND count_feedback >= 3 AND pr.deleted = 0 AND gall.type=0 AND gall.file IS NOT NULL ORDER BY distance LIMIT 5";
                    }
                    $similarFacilityQueryresL = $db->sql_query( $similarFacilityQuery );
                    $similarFacility          = $db->sql_fetchrowset( $similarFacilityQueryresL );
                    if ( count( $similarFacility ) > 1 ) { ?>                        
                    <?php
                        $you_may_also_like = '';
                        foreach ( $similarFacility as $sFac ) {
                            $fID        = $sFac[ 'id' ];
                            $fQuick_url = $sFac[ 'quick_url' ];
                            $fTitle     = $sFac[ 'title' ];
                            $fcount_feedback     = $sFac[ 'count_feedback' ];
                            if ( isset( $sFac[ 'file' ] ) && !empty( $sFac[ 'file' ] ) ) {
                                $fImageSet = $_SERVER[ 'DOCUMENT_ROOT' ] . '/admin/files/gallery/images/thumb_' . $sFac[ 'file' ];
                                $fImage    = 'thumb_' . $sFac[ 'file' ];
                            }
                            $fSuburb         = $sFac[ 'address_suburb' ];
                            $fCity           = $sFac[ 'address_city' ];
                            $fOverall_rating = simlilarFacilityRating( $fID );
                            
// Refer to /modules/search/ajax_vacancy.php to change code in emails. Not sure if below is being used.                            
                            
                            if ( file_exists( $fImageSet ) ) {
                                $similarFacility = '<img src="' . HOME_PATH . 'admin/files/gallery/images/' . $fImage . '" alt="" title="" />
                                                    <div class="facility-rating">

                                                        <a href="' . HOME_PATH . $fQuick_url . '"><span class="prcnt-icon">' . round( $fOverall_rating ) . '% rating from '.$fcount_feedback.' reviews</span><br/>' . " " . $fTitle . '<br/>

                                                        <span>' . $fSuburb . ', ' . $fCity . '</span>
                                                        <div class="star-inactive">
                                                            <div class="star-active" style="width:' . $fOverall_rating . '%;"></div>
                                                        </div>
                                                        </a>
                                                </div>
                                                <br>';
                            } else {
                                $you_may_also_like = '';
                            }
                            $you_may_also_like = $you_may_also_like . $similarFacility;
                        }
                    }
                    $message = str_replace( '{you-may-also-like}', $you_may_also_like, $message );
                }
                $result = @mail( $to, $emailTemplate[ 'subject' ] . ' ' . $facility_name . ' at ' . date( "g:i a d.m.y" ), $message, $headers );
            }
        
    }
}
?>