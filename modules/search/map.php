<script type="text/javascript" src="<?php  echo HOME_PATH;?>assets/markerwithlabel.js"></script>
<style>
.google-map {
 
 height:720px !important; max-width: none;
 
 }
.map-infow h5{font-weight:bold;text-transform:capitalize;border-bottom:2px solid #042E6F;color:#042E6F;}
.map-infow p {margin:0;}
.map-infos{
    color:#042E6F;font-weight:bold;
  }
img {max-width: 100%; height: auto;}

.labels {
  color: white;
  background-color:red;
  font-family: "Lucida Grande", "Arial", sans-serif;
  font-size: 12px;
  text-align: center;
  width: auto;
  white-space:nowrap;
}

.labels.hover .arrow_box {
    border-top-color: #ffffffff;
    border-right-color: rgba(255,255,0,0);
    border-bottom-color: rgba(255,255,0,0);
    border-left-color: rgba(255,255,0,0);
    background-color: #FFA500;
}

.labels.hover .arrow_boxes{
    border-top-color: #ffffffff;
    border-right-color: rgba(255,255,0,0);
    border-bottom-color: rgba(255,255,0,0);
    border-left-color: rgba(255,255,0,0);
    background-color: #FFA500;
}
.arrow_box {
   margin-top: -25px !important;
   margin-left: -65px !important;
   padding: 5px 10px;
   border-radius: 6px;
   overflow: visible !important;
   opacity: 1 !important;
   color: #ffffff;
   font-family:"Lucida Grande", "Arial", sans-serif;
   font-size: 14px;
   font-weight: bold;
   position: relative;
   background: #042E6F;
   border: 1px solid #042E6F;
   z-index: 999 !important;
}
.arrow_boxes {
   margin-top: -25px !important;
   margin-left: -65px !important; 
   padding: 5px 10px;
   border-radius: 6px;
   overflow: visible !important;
   opacity: 1 !important;
   color: #ffffff;
   font-family:"Lucida Grande", "Arial", sans-serif;
   font-size: 14px;
   font-weight: bold;
   position: relative;
   background: #042E6F;
   border: 1px solid #042E6F;
   z-index: 50 !important;
}
.arrow_box:hover{
  background: orange;
  border: 1px solid orange;
}
.arrow_boxes:hover{
  background: orange;
  border: 1px solid orange;  
}
.arrow_box:after, .arrow_box:before {
  top: 100%;
  left: 50%;
  border: solid transparent;
  content: " ";
  height: 0;
  width: 0;
  position: absolute;
  pointer-events: none;
}
.arrow_boxes:after, .arrow_boxes:before {
  top: 100%;
  left: 50%;
  border: solid transparent;
  content: " ";
  height: 0;
  width: 0;
  position: absolute;
  pointer-events: none;
}
.arrow_box:after {
  border-color: rgba(255, 0, 0, 0);
  border-top-color: #042E6F;
  border-width: 10px;
  margin-left: -10px;
}
arrow_boxes:after {
  border-color: rgba(255, 0, 0, 0);
  border-top-color: #042E6F;
  border-width: 10px;
  margin-left: -10px;
}

.arrow_box:hover::after {
  border-color: transparent;
  border-top-color: orange;
  border-width: 10px;
  margin-left: -10px;
}

.arrow_boxes:hover::after {
  border-color: transparent;
  border-top-color: orange;
  border-width: 10px;
  margin-left: -10px;
}

.arrow_box:hover::before {
  border-color: transparent;
  border-top-color: orange;
  border-width: 11px;
  margin-left: -11px;
}

.arrow_boxes:hover::before {
  border-color: transparent;
  border-top-color: orange;
  border-width: 11px;
  margin-left: -11px;
}

.arrow_box:before {
  border-color: rgba(181, 0, 0, 0);
  border-top-color: #042E6F;
  border-width: 11px;
  margin-left: -11px;
}

.arrow_boxes:before {
  border-color: rgba(181, 0, 0, 0);
  border-top-color: #042E6F;
  border-width: 11px;
  margin-left: -11px;
}

<?php 

/******************************logo for upgrade product**********/
function upgradeproduct_logo($pro_id)
{ 
     global $db;
     $upgrade_pro="select id, supplier_id from ad_products where id={$pro_id}";
    $res = $db->sql_query($upgrade_pro);
    $up_prod = $db->sql_fetchrowset($res);
    $count = $db->sql_numrows($res);
    if($count>0){
    foreach($up_prod  as $up_prods){
     $up_prods['supplier_id'];
    $supp_id=$up_prods['supplier_id'];
    $up_plan="select id,plan_id from ad_pro_plans where pro_id={$pro_id}  AND supplier_id={$supp_id} AND current_plan=1 AND status=1 AND deleted=0";
    $up_query=$db->sql_query($up_plan);
    $up_product_list = $db->sql_fetchrowset($up_query);
    foreach ($up_product_list as $up_product_lists) {
     $up_product_lists['id'];
     $pro_plan_id=$up_product_lists['plan_id'];
    if($pro_plan_id>1)
    {
        $pl_id="select logo from ad_products where id={$pro_id}";
        $pl_query= $db->sql_query($pl_id);
        $up_prod = $db->sql_fetchrowset($pl_query);
        foreach ($up_prod as $up_prods) {
        $up_prod_logo=$up_prods['logo'];
        if($up_prod_logo=='' || empty($up_prod_logo))
            {
               $logo_image='';
            }
            else
            {
                 $logo_image=$up_prod_logo;
            }
        }
    }

    return  $logo_image;
  }

}

 }
}
/***************************end of code for upgrade product***********************/
?>
</style> 
      <div class="google-map-wrap" itemscope itemprop="hasMap" itemtype="http://schema.org/Map">
                <div id="google-map" class="google-map"></div>
                <!-- #google-map -->

            </div>

                <?php /* === MAP DATA === */ ?>
                <?php

                
                $ltd[] = array();
                $i=0;
                
                $locations = array();

                foreach($data as $info){
                    $latitude = $ltd[$i]["latitude"]=$info["latitude"];
                    $longitude = $ltd[$i]["longitude"]=$info["longitude"];
                    $ltd[$i]["title"]=$info["title"];
                     $ltd[$i]["id"]=$info["id"];
                     $ltd[$i]["certification_service_type"]=$info["certification_service_type"];
                     
                     if($ltd[$i]["certification_service_type"] == "Aged Care") { 
                        

                        $certi_ser_type= 'AC';  
                    } 
                    if($ltd[$i]["certification_service_type"] == "Retirement Village") { 
                        $certi_ser_type= 'RV';    } 
                       $service_type_data=productServices($info["id"]);
                      $service_type11=preg_replace('/,/', ', ', $service_type_data);   
                    
                    $ltd[$i]["zip"]=$info["zip"];
                    $ltd[$i]["address"]=$info["address"];
                    $ltd[$i]["suburbTitle"]=$info["suburbTitle"];
                    $ltd[$i]["city"]=$info["city"];
                  
                     $logo_image_pro=upgradeproduct_logo($info['id']);
					 
                     if($logo_image_pro=='' || empty($logo_image_pro))
                     {
                      $logo='';
					  
                     }
                     else
                     {
                       $logo='<img src="https://agedadvisor.nz/admin/files/product/logo/thumb_'.$logo_image_pro.'"/>';
                     }
                  
                    $html='<h5>'.str_replace("'",'"',$info["title"]).'&nbsp;&nbsp;'.$certi_ser_type.'</h5>';
                  
          $html .='<p>Rating : '.OverAllNEWRatingProductForMap($info['id']).' <span>Reviews : '.str_replace("'",'"',overAllRatingProductCount($info['id'])). '</span></p>';
          $html.='<p>Service Type : '.$service_type11.'</p>';       
          $ltd[$i]["html"]='<div class="map-infow">'.$logo.''.$html.'</div>';
          $address = $info["address"].'+'.$info["city"].'+'.$info["zip"].'+'."New+Zealand";
          $quick_url=$info["quick_url"];
          $prepAddr = str_replace(' ','+',$address);
          $prepAddr = str_replace('  ','+',$prepAddr);
          $prepAddr = str_replace('++','+',$prepAddr);
          //echo $prepAddr;
          
          $locations[] = array(
                    'google_map' => array(
                        'lat' => ''.$latitude.'',
                        'lng' => ''.$longitude.'',
                    ),
                    'location_address' => ''.$info["city"].'',
                    'location_name'    => ''.$ltd[$i]["html"].'',
                    'fulladdress' => $info["address"].','.$info["suburbTitle"].','.$info["city"].' '.$info["zip"].', New Zealand',
                    'title' => $info["title"],
                    'certification_ser_type'=>$certi_ser_type,
                    'rating' =>round(simlilarFacilityRating($info['id'])),
                    'review' => overAllRatingProductCount($info['id']) ,
                    'quick'=>$info["quick_url"],
                    'upgrade_logo_product'=>upgradeproduct_logo($info['id']) 
                   );
           $i++;     
          }
                /* Set Default Map Area Using First Location */
                $map_area_lat = isset( $locations[0]['google_map']['lat'] ) ? $locations[0]['google_map']['lat'] : -43.513929;
                $map_area_lng = isset( $locations[0]['google_map']['lng'] ) ? $locations[0]['google_map']['lng'] : 172.65913;
               
$last_map_script="<script type=\"text/javascript\"> 
jQuery(document).ready( function($) {
   
      var mappingMarkers=[];
      var bounds = new google.maps.LatLngBounds();
      map = new google.maps.Map(document.getElementById('google-map'),{});
";
 
  $i=0;
  /* fetch all location to display on map */
    $locations_count=count($locations);
        
    foreach( $locations as $location ){
        $certi_ser_type=$location['certification_ser_type'];
        $name = $location['location_name'];
        $addr = $location['location_address'];
        $full_address_data=$location['fulladdress'];
        $full_address_detail=chop($full_address_data,',New Zealand');
        $full_address= preg_replace('/,/', ', ', $full_address_detail);
        $full_address = '<p class="map-infos">'.$full_address.'</p>';
        $map_lat = $location['google_map']['lat'];
        $map_lng = $location['google_map']['lng'];
        $title = $location["title"];
        $rating = $location["rating"];
        $review = $location["review"];
        $quick_url_list = $location["quick"];
        $upgrade_logo=$location['upgrade_logo_product'];
        $mapping_id=$i;

 if(!empty($map_lat) && $review>0){ $i++; $last_map_script.="
	               var latlng = new google.maps.LatLng(".$map_lat.",".$map_lng.");
              bounds.extend(latlng);
              var marker = new MarkerWithLabel({
              	   id: 'mapping_".$i."',
                   position:latlng,
                   draggable: false,
                   raiseOnDrag: true,
                   map:map,
                   icon:'a.png',
                   labelContent:'".$rating."% (".$review." reviews)"."',
                   labelAnchor: new google.maps.Point(02, 0),
                   labelClass: \"arrow_box\", // the CSS class for the label
                  
            });

       			var infowindow = new google.maps.InfoWindow({
       	          
                   content: '<p>".$name.'' . $full_address."</p>',
                   pixelOffset: new google.maps.Size(0, 0)
                   
              });
           
             google.maps.event.addListener(marker, \"click\", function (e) {

                infowindow.close();
                $(\".gm-style-iw\").next(\"div\").hide();

                infowindow = new google.maps.InfoWindow({
                  content: '<p>". $name . $full_address ."</p>',
                  pixelOffset: new google.maps.Size(30, 30)
                });

                infowindow.open(map,this);
                window.location=\"https://www.agedadvisor.nz/".$quick_url_list."\";
            });
            
             google.maps.event.addListener(marker, \"mouseover\", function (e) {

              infowindow.close();
              $(\".gm-style-iw\").next(\"div\").hide();

              infowindow = new google.maps.InfoWindow({
                content: '<p>". $name .$full_address."</p>',
                pixelOffset: new google.maps.Size(30, 30)
              });

              infowindow.open(map,this);
            });

             google.maps.event.addListener(marker, \"click\", function (e) {
             infowindow.close();
              
            });";

 }else if(!empty($map_lat) && $review==0){$i++; $last_map_script.="
	               var latlng = new google.maps.LatLng(".$map_lat.",".$map_lng.");
              bounds.extend(latlng);
              var marker = new MarkerWithLabel({
              	   id: 'mapping_".$i."',
                   position:latlng,
                   draggable: false,
                   raiseOnDrag: true,
                   map:map,
                   icon:'a.png',
                   labelContent:'',
                   labelAnchor: new google.maps.Point(02, 0),
                   labelClass: \"arrow_boxes\", // the CSS class for the label
                  
            });

       			var infowindow = new google.maps.InfoWindow({
       	          
                   content: '<p>".$name.'' . $full_address."</p>',
                   pixelOffset: new google.maps.Size(0, 0)
                   
              });
           
             google.maps.event.addListener(marker, \"click\", function (e) {

                infowindow.close();
                $(\".gm-style-iw\").next(\"div\").hide();

                infowindow = new google.maps.InfoWindow({
                  content: '<p>". $name . $full_address ."</p>',
                  pixelOffset: new google.maps.Size(30, 30)
                });

                infowindow.open(map,this);
                window.location=\"https://www.agedadvisor.nz/".$quick_url_list."\";
            });
            
             google.maps.event.addListener(marker, \"mouseover\", function (e) {

              infowindow.close();
              $(\".gm-style-iw\").next(\"div\").hide();

              infowindow = new google.maps.InfoWindow({
                content: '<p>". $name .$full_address."</p>',
                pixelOffset: new google.maps.Size(30, 30)
              });

              infowindow.open(map,this);
            });

             google.maps.event.addListener(marker, \"click\", function (e) {
             infowindow.close();
              
            });";
     
 }} if($locations_count>1) {
$last_map_script.="
map.fitBounds(bounds); ";//binding all location on the map.

} else {
$last_map_script.="
map.fitBounds(bounds);
google.maps.event.addListenerOnce(map, 'bounds_changed', function(event) {
  if (this.getZoom()) {
    this.setZoom(15);
  }
});";

 } 
$last_map_script.="    
});      
</script>";



