<?php 
function show_image($image_enabled = FALSE, $id = "'none'", $certification_service_type = '', $url = '') {
    record_mtime("/modules/search/search.php function show_image()");

    $return = '';
    global $db;

    $review_friendly_badge = $response_time_badge = $response_rate_badge = '';

    $response_rate_query = "SELECT COUNT(NULLIF(id,0)) as total_enq, (SELECT COUNT(NULLIF(id,0)) FROM ad_enquiries WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 7 DAY) and prod_id = $id AND facility_responded = 'y') AS replied FROM ad_enquiries WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 7 DAY) and prod_id = $id";
    $response_rate_res = $db->sql_query($response_rate_query);
    $response_rate_data = $db->sql_fetchrowset($response_rate_res);

//foreach($response_rate_data as $data_rate){    
//    if($data_rate['total_enq'] > 0){
//        $replied_enq = $data_rate['replied'];   
//        $total_enq = $data_rate['total_enq'];   
//        $response_rate = ($replied_enq/$total_enq)*100;   
//        $response_rate_round = round( $response_rate, 1, PHP_ROUND_HALF_UP);
//        if($response_rate_round > 79){
//            $response_rate_badge = '<div class="trusted-responder" style="margin-right:7px"> </div>';
//        }        
//    }
//}


    if ($certification_service_type == 'Retirement Village') {
        $query_reviews = "SELECT count(fdbk.id) as totalreview, fdbk.product_id,(extr.no_of_room) as totalRoom,"
                . "(count(fdbk.id)/(extr.no_of_room)) as cmost FROM ad_feedbacks AS fdbk LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id"
                . " LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id inner join ad_extra_facility as extr on (extr.product_id=pds.id)"
                . " WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND current_plan = 1 and pds.certification_service_type = 'Retirement Village' and (pds.deleted=0)"
                . " and (pds.status=1) GROUP BY product_id ORDER BY cmost DESC,plan_id DESC,product_id ASC LIMIT 50";
    } else {

        $query_reviews = "SELECT count(fdbk.id) as totalreview, fdbk.product_id,(extr.no_of_beds) as totalbeds,"
                . "(count(fdbk.id)/(extr.no_of_beds)) as cmost FROM ad_feedbacks AS fdbk LEFT JOIN ad_products AS pds ON fdbk.product_id= pds.id "
                . "LEFT JOIN ad_pro_plans AS plan ON fdbk.product_id= plan.pro_id inner join ad_extra_facility as extr on (extr.product_id=pds.id)"
                . " WHERE fdbk.status = 1 AND fdbk.deleted = 0 AND current_plan = 1 and pds.certification_service_type != 'Retirement Village' "
                . "and (pds.deleted=0) and (pds.status=1) GROUP BY product_id ORDER BY cmost DESC,plan_id DESC,product_id asc LIMIT 50";
    }
    $result_reviews = $db->sql_query($query_reviews);
    $count = $db->sql_numrows($result_reviews);
    $data_reviews = $db->sql_fetchrowset($result_reviews);
    $i = 0;
    if (count($data_reviews) > 0) {
        foreach ($data_reviews as $data_reviews_val) {
            if ($data_reviews_val['product_id'] == $id) {
                switch (true) {

                    case $i < 5 :
                        $most_review_badge = '<span class="most-reviewed-top5a most-reviewed-icon-size"> </span>';
                        break;

                    case $i < 10:
                        $most_review_badge = '<span class="most-reviewed-top10a most-reviewed-icon-size"> </span>';
                        break;

                    case $i < 25:
                        $most_review_badge = '<span class="most-reviewed-top25a most-reviewed-icon-size"> </span>';
                        break;

                    case $i < 50:
                        $most_review_badge = '<span class="most-reviewed-top50a most-reviewed-icon-size"> </span>';
                        break;

                    default :
                        $most_review_badge = '';
                        break;
                }
            }
            $i++;
        }
    }


    /*
      $response_duration_query = "SELECT time_of_enquiry, facility_responded FROM ad_enquiries WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND prod_id = $id ORDER BY time_of_enquiry DESC LIMIT 1";
      $response_duration_res = $db->sql_query($response_duration_query);
      $response_duration_data = $db->sql_fetchrowset($response_duration_res);

      foreach($response_duration_data as $data_duration){

      if($data_duration['facility_responded'] === 'y'){
      $response_time_badge = '<div class="response-time" style="margin-right:7px"> </div>';
      }
      }
     */

    $qry_dayResp = "SELECT facility_name, ( (5 * (DATEDIFF(DATE(time_of_response),"
            . " DATE(time_of_enquiry)) DIV 7) + MID('0123444401233334012222340111123400012345001234550', 7 * WEEKDAY(DATE(time_of_enquiry)) + WEEKDAY(DATE(time_of_response)) + 1, 1) ) - (SELECT COUNT(*) FROM ad_holidays WHERE holiday_date between DATE(time_of_enquiry)"
            . " and DATE(time_of_response) ) )as dayResp FROM ad_enquiries where prod_id='$id'"
            . " and DATE(time_of_enquiry) < DATE_SUB(CURDATE(),INTERVAL 4 DAY) and facility_responded='y' and time_of_enquiry!='' and time_of_response!='' order BY time_of_enquiry desc";

    $res_dayResp = $db->sql_query($qry_dayResp);
    $count_resp = $db->sql_numrows($res_dayResp);
    $row_dayResp = $db->sql_fetchrowset($res_dayResp);
    if ($count_resp > 0) {
        foreach ($row_dayResp as $cResp) {
            $numResp[] = $cResp['dayResp'];
        }

        $avg_resp = round(array_sum($numResp) / count($numResp));

        switch (true) {
            case ($avg_resp > 0 && $avg_resp < 2 ) :
                $response_time_badge = '<span class="response-time-one1 most-reviewed-icon-size"> </span>';
                break;
            case ($avg_resp > 1 && $avg_resp < 3):
                $response_time_badge = '<span class="response-time-two1 most-reviewed-icon-size"> </span>';
                break;
            case ($avg_resp > 2 && $avg_resp < 4):
                $response_time_badge = '<span class="response-time-three1 most-reviewed-icon-size"> </span>';
                break;
            case ($avg_resp > 3 && $avg_resp < 5):
                $response_time_badge = '<span class="response-time-four1 most-reviewed-icon-size"> </span>';
                break;
            case ($avg_resp > 4):
                $response_time_badge = '<span class="response-time-five1 most-reviewed-icon-size"> </span>';
                break;
            default :
                $response_time_badge = '<span class="response-time1 most-reviewed-icon-size"> </span>';
                break;
        }
    }

    $trustqry = "SELECT "
            . " COUNT(CASE WHEN facility_responded = 'y' then 1 ELSE NULL END) as trusted_count,"
            . " COUNT(CASE WHEN facility_responded = 'n' then 1 ELSE NULL END) as nottrusted_count"
            . " FROM ad_enquiries"
            . " WHERE DATE(time_of_enquiry) < DATE_SUB(CURDATE(), INTERVAL 4 DAY)"
            . " and prod_id = $id ORDER by time_of_enquiry DESC LIMIT 4";

    $result_Resp = $db->sql_query($trustqry);
    $row_resp = $db->sql_fetchrowset($result_Resp);

    if ($row_resp[0]['nottrusted_count'] >= 3 && $row_resp[0]['trusted_count'] < 3) {
        $response_rate_badge = '<div class="nottrusted-responder1 most-reviewed-icon-size" style="margin-right:7px"> </div>';
    } else if ($row_resp[0]['trusted_count'] >= 3 && $row_resp[0]['nottrusted_count'] < 3) {
        $response_rate_badge = '<div class="trusted-responder most-reviewed-icon-size"> </div>';
    } else {
        $response_rate_badge = '';
    }

    $query = "SELECT gal.id, gal.title,gal.file,gal.type,prd.review_friendly FROM " . _prefix("galleries") . "  as gal "
            . " LEFT JOIN " . _prefix("products") . " AS prd on prd.id = gal.pro_id "
            . " where gal.deleted = 0  AND pro_id=" . $id . " AND gal.type=0  ORDER BY gal.id LIMIT 0,1";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if (isset($data[0])) {
        if ($data[0]['review_friendly'] == 1) {
            $review_friendly_badge = '<span class="review-friendly most-reviewed-icon-size"> </span>';
        }
        if ($data[0]['review_friendly'] == 2) {
            $review_friendly_badge = '<span class="review-unfriendly1 most-reviewed-icon-size"> </span>';
        }
    }


    if ($image_enabled && isset($data[0])) {

        $record = $data[0];
        $pic_description = $data['title'];
        $fileImage = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
        $fileImageSrc = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
        $fileImageView = HOME_PATH . 'admin/files/gallery/images/' . $record['file'];

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/admin/files/gallery/images/thumb_' . $record['file'])) {

            $logo_image_pro = upgradeproduct_logos($id);
            if ($logo_image_pro == '' || empty($logo_image_pro)) {
                $logo = '';
            } else {
                $logo = '<div id="pro_logo" class="product_logo"><img src="https://agedadvisor.nz/admin/files/product/logo/thumb_' . $logo_image_pro . '"/></div>';
            }
            $return = '<div class="col-xs-12 col-sm-4 col-md-12">' . $logo . '
              <div class="association-img review-container"><a href="' . HOME_PATH . $url . '"><img alt="' . $certification_service_type . ' ' . $pic_description . '" title="' . $pic_description . '" src="' . $fileImageSrc . '" class="img-responsive" style="float:left; margin-right: 20px;margin-bottom:10px;"></a>' . $review_friendly_badge . $response_time_badge . $response_rate_badge . $most_review_badge . '</div></div>';
        } else {
            if ($review_friendly_badge != '' || $response_time_badge != '' || $response_rate_badge != '' || $most_review_badge != '') {
                $return = '<div class="col-xs-12 col-sm-4 col-md-12"><div class="association-img review-container">' . $review_friendly_badge . $response_time_badge . $response_rate_badge . $most_review_badge . '</div></div>';
            }
        }
    } else {
        if ($review_friendly_badge != '' || $response_time_badge != '' || $response_rate_badge != '' || $most_review_badge != '') {
            $return = '<div class="col-xs-12 col-sm-4 col-md-12"><div class="association-img review-container">' . $review_friendly_badge . $response_time_badge . $response_rate_badge . $most_review_badge . '</div></div>';
        }
    }
    return $return;
    record_mtime("/modules/search/search.php function show_image()");
}
