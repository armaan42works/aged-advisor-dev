<?php
    /*
     * Objective  : List of all the message with respect to project
     * Filename : project_message.php
     * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com> 
     * Created On : 28 August 2014
     */
    require_once("./admin/fckeditor/fckeditor.php");
?>
<!----Dashbord_main START--------------------->
<section id="body_container" class="Dashbord_main">
    <ul class="breadcrumb">
        <li><a href="<?php echo HOME_PATH ?>user"><strong>Home</strong></a></li>
        <li class="last"><a href="javascript:void(0);">Message box</a></li>
    </ul>
    <?php
        if (isset($submit) && $submit == 'Reply') {
            $getQuoteId = "SELECT id FROM " . _prefix('quotes') . " WHERE md5(id) = '{$pathInfo['call_parts']['2']}'";
            $resQuoteId = $db->sql_query($getQuoteId);
            $dataQuoteId = $db->sql_fetchrow($resQuoteId);
            $fields_array = reply_message();
            $response = Validation::_initialize($fields_array, $_POST);
            if (isset($response['valid']) && $response['valid'] > 0) {
                $body = $_POST["FCKeditor1"];
                $fields = array('parent_message_id' => $msg['id'],
                    'subject' => $subject,
                    'body' => $body,
                    'quote_id' => $dataQuoteId['id'],
                    'email_id' => ADMINNAME,
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'sender_id' => $_SESSION['userId'],
                    'receiver_id' => 0,
                    'user_type' => 0,
                    'is_read_client' => 1);

                $insert_result = $db->insert(_prefix('messages'), $fields);
                $message_id = mysqli_insert_id($db->db_connect_id);
                $attachment = '';
                if (isset($_FILES['file_name'])) {
                    for ($i = 0; $i < count($_FILES['file_name']['name']); $i++) {
                        if ($_FILES['file_name']['error'][0] == 0) {
                            $fileData = pathinfo($_FILES['file_name']["name"][$i]);
                            $target_path = DOCUMENT_PATH . '/admin/files/message/';
                            $filename = rand(1, 100000);
                            $date = new DateTime();
                            $timestamp = $date->getTimestamp();
                            $imageName = $filename . $timestamp . '_' . basename($_FILES['file_name']['name'][$i]);
                            $target_path = $target_path . $imageName;
                            $message_fields = array('message_id' => $message_id, 'filename' => $imageName);
                            if ($_FILES['file_name']['name'][$i] != "") {

                                $insert_attachment = $db->insert(_prefix('attachment'), $message_fields);
                            }

                            $var = move_uploaded_file($_FILES['file_name']["tmp_name"][$i], $target_path);
                            $attachment = ADMIN_PATH . 'files/message/' . $imageName;
                        }
                    }
                }
                // $send = sendmail($email_id, $subject, $body, $attachment);
                if ($insert_result) {
                    $msg = common_message(1, constant('SEND_MESSAGE'));
                    $_SESSION['msg'] = $msg;
                }
            } else {
                $errors = '';
                foreach ($response as $key => $message) {
                    $error = true;
                    $errors .= $message . "<br>";
                }
            }
        }
    ?>
    <div class="messag_send"></div>

    <!--message container code starts-->
    <div class="message_container session_container">
        <form id='replyForm' name='replyForm' method='POST' enctype="multipart/form-data">
            <div><input type="textbox"  id="subject" name="subject" value="" placeholder="Subject" class="subject_input"></div>
            <div class="sendMail">
                <?php
                    $oFCKeditor = new FCKeditor('FCKeditor1');
                    $oFCKeditor->BasePath = HOME_PATH . '/admin/fckeditor/';
                    $oFCKeditor->Height = '400px';
                    $oFCKeditor->Value = "";

                    $oFCKeditor->Create();
                ?>
                <div class="input text" >
                    <input type="file" name="file_name[]" id="">
                </div>
                <div class="input text add" >
                    <a href="javascript:void(0);" class="addFile">+Add</a>
                </div>
                <div class="reply_button" >
                    <input type="submit" value="Reply" name="submit" class="creat_account_btn"  style='float:left;'></div>
            </div>
        </form>
    </div>
        <?php
            $msg_query = "SELECT dm.*, du.name FROM " . _prefix('messages') . " AS dm LEFT JOIN " . _prefix('users') . " as du ON dm.sender_id = du.id  WHERE "
                    . "(dm.receiver_id ={$_SESSION['userId']} || dm.sender_id ={$_SESSION['userId']}) &&"
                    . "md5(dm.quote_id) = '{$pathInfo['call_parts']['2']}' && (dm.parent_message_id IS NULL || dm.parent_message_id = '0') ORDER BY dm.created DESC";
            $msgRes = $db->sql_query($msg_query);
            $msgDatas = $db->sql_fetchrowset($msgRes);
            $msgCount = $db->sql_numrows($msgRes);
            if ($msgCount > 0) {
                ?>
    <div class="message_container">
                <div class="row">
                    <div class="coll box_1 coll_hedding">From</div>
                    <div class="coll box_2 coll_hedding">Subject</div>
                    <div class="coll box_3 coll_hedding">Date</div>
                </div>
                <?php
                foreach ($msgDatas as $msgData) {
                    $getnom = "SELECT count(*) as total FROM " . _prefix("messages") . " WHERE parent_message_id = '{$msgData['id']}'";
                    $resNom = $db->sql_query($getnom);
                    $dataNom = $db->sql_fetchrow($resNom);
                    $nom = $dataNom['total'];
                    if ($nom > 0) {
                        $nom = ' (' . $nom . ')';
                    } else {
                        $nom = '';
                    }
                    $read = $msgData['is_read_client'] == 0 ? '' : 'show_box';
                    ?>
                    <div class="row <?php echo $read; ?>"><a href="<?php echo HOME_PATH ?>user/message/inbox/<?php echo md5($msgData['id']); ?>">
                            <div class="coll box_1 "><strong><?php echo $msgData['name'] == '' ? constant('ADMINNAME') . $nom : $msgData['name'] . $nom ?></strong></div>
                            <div class="coll box_2 "><strong><?php echo $msgData['subject']; ?></strong></div>
                            <div class="coll box_3 "><strong><?php echo date('M d, Y', strtotime($msgData['created'])); ?></strong> </div></a>
                    </div>
                    <?php
                }
                ?>


                <ul class="pagination">
                    <div class="prev"><a href="#"><i class="fa fa-angle-double-left"></i></a></div>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <div class="next"><a href="#"><i class="fa fa-angle-double-right"></i></a></div>
                </ul>
    </div>
                <?php
            }
        ?>

    

</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('.addFile').click(function() {
            var inputFile = '<div class="input text" ><input type="file" name="file_name[]" id="file_name"></div>';
            $('.add').before(inputFile);
        });
    })
</script>