<?php
    /*
     * Objective  : List of all the project progress
     * Filename : project_progress.php
     * Created By : Yogesh Chandra Joshi <yogesh.joshi@ilmp-tech.com> 
     * Created On : 28 August 2014
     */

    $sql_query = "SELECT * FROM " . _prefix('quotes') . " WHERE  status = 2 && client_id = '{$_SESSION['userId']}' ORDER BY id DESC";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    $datas = $db->sql_fetchrowset($res);
    
?>
<section id="body_container" class="Dashbord_main">
<ul class="breadcrumb">
 <li><a href="<?php echo HOME_PATH ?>user"><strong>HOME</strong></a></li>
 <li class="last"><a href="javascript:void(0);">Project Completed</a></li>
 </ul>
    <div class="row">
        <?php
            if ($num > 0) {
                ?>
              
                    <?php
                    $i = 1;
                    foreach ($datas as $data) {
                        
                        
                        $class = '';
                        
                        if ($i == 1 || $i % 2 == 1) {
                            $class = 'dashbord_tilles project_box';
                        } else if ($i % 2 == 0) {
                            $class = 'dashbord_tilles project_box last';
                        }
                        ?>
                        <div class="<?php echo $class; ?>"> 
                            <div class="top_bar">
                                <span class="date_coll"><strong>Project ID :</strong> <?php  echo 'P'.$data['unique_id'];  ?></span>
                                <span class="date_coll"><strong>DATE :</strong> <?php  echo date('M d, Y', strtotime($data['created']));  ?></span>
                            </div>
                            <div class="des_details">DESCRIPTION</div>
                            <?php
        
        if(strlen($data['project_desc']) > 58){
            $data['project_desc'] = substr($data['project_desc'], 0, 55).' ..';
        }
        ?>
                            <p><?php  echo $data['project_desc']; ?> </p>
                            <h4><strong>CODE :</strong> <?php  echo  $data['coupon_id'] != '' ? stripslashes($data['coupon_id']) : 'N/A';?></h4>
                            <ul class="myproduct">
                            <li><a href="<?php echo HOME_PATH ?>user/quote/<?php echo md5($data['id']); ?>"><img src="<?php echo HOME_PATH ?>images/product_img_4.png" alt=""/></a></li>
                            <li><a href="<?php echo HOME_PATH ?>user/projectMessage/<?php echo md5($data['id']); ?>"><img src="<?php echo HOME_PATH ?>images/product_img_5.png" alt=""/></a></li>
                         </ul>
                        </div>
                        <?php
                        $i++;
                    }
                    ?>
                
                <?php
            }else {
               echo "<div class='norecord'>No Record found</div>";
            }
        ?>

    </div>


 

</section>
</div>