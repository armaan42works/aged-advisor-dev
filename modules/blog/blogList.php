
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '';
        changePagination(page, li, 'blog', data);
    });
</script>

<!--
<div class="container">
    <div class="row articles_container">
        <h1 class="theme_color_inner" style="padding-left:5px; margin-bottom: 5px;">Blogs</h1>

        <div class="col-md-12 col-sm-12 col-xs-12 community_wrapper" style="padding:20px 5px;">
            <div class="list-group" id="pageData">
            </div>
        </div>
    </div>
</div>-->

<!--------------------------code for blog list--------------------------->
<div class="container">
    <div class="row articles_container">
    	<div class="headline blog-headline">
        	<h1 class="theme_color_inner">Life Questions</h1>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 community_wrapper" style="padding:0;">
            <div class="list-group" id="pageData">
            </div>
        </div>
    </div>
</div>
<!----------------------------code for blog list end -------------------->