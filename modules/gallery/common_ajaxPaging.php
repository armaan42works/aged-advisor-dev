<?php
require_once 'constant.php';
$uid = $_SESSION['id'];
$proId = $_SESSION['proId'];
$proName = $_SESSION['proName'];
$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : '';
$pageIdRequested = isset($_REQUEST['pageId']) ? $_REQUEST['pageId'] : '';
$search_input = isset($_REQUEST['search_input']) ? $_REQUEST['search_input'] : '';
$search_type = isset($_REQUEST['search_type']) ? $_REQUEST['search_type'] : '';
global $db;
switch ($action) {
    case 'artwork':
        manage_artWork();
        break;
    case 'product':
        manage_product($uid, $search_input, $search_type);
        break;
    case 'productviews':
        manage_productviews($uid);
        break;
    case 'order':
        manage_order($uid);
        break;
    case 'partwork':
        manage_personalArtWork($uid);
        break;
    case 'gallery':
        manage_gallery($uid);
        break;
    case 'articles':
        articles_list();
        break;
    case 'globalsearch':
        global_search($searchvalue);
        break;
    case 'reviews':
        review_list();
        break;
    case 'viewReviews':
        viewReview($id);// Main Facility details Pages
        break;
    case 'viewFeedback':
        viewFeedback($id);
    case 'viewFeedbackdDetail':
        viewFeedbackdDetail($id);
        break;
    case 'feedback':
        feedback_center($uid,$proId);
        break;
    case 'forum':
        manage_forum();
    case 'blog':
        manage_blog();
        break;
    case 'adsBookingList':
        manage_adsBookingList($uid,$proId,$proName);
        break;
    default :
        test();
        break;
}

function test() {
    
}


function get_first_image_from_gallery($pid){
	$image_file='';
	global $db;

        $sql_query = "select file from " . _prefix('galleries') . " where pro_id = '$pid' AND type = 0 AND status = 1 AND deleted = 0 ORDER BY Favourite_image DESC LIMIT 0,1 ";


    $res = mysqli_query($db->db_connect_id, $sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
		 $image_file= $records['file'];
	}
	return $image_file;
}

function manage_forum($search_input, $search_type) {
    global $db;
    $condition = " where bg.deleted = 0 AND bg.type= 1 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' && bg.title like "%' . $search_input . '%" ';
                break;
            case "author":
                $condition .= ' && user.first_name like "%' . $search_input . '%" ';
                $condition .= ' && user.last_name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && ( bg.title like "%' . $search_input . '%" OR user.name like "%' . $search_input . '%" )';
        }
    }

    $query = "SELECT bg.*, user.user_name AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . " $condition GROUP BY bg.id ORDER BY bg.id DESC ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT bg.*, user.user_name AS author , count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . "$condition  GROUP BY bg.id ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);

    if ($count > 0) {

        foreach ($data as $key => $blog) {
            $description = strip_tags($blog['title']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $output .= '<div class="group_one">
                    <div class="text-right"><a href="' . HOME_PATH . 'forum/view?id=' . md5($blog['id']) . '">Post Comment</a></div>
                <div class="line_bottom">
                    <div class="width_90">Subject :</div>
                    <div class="margin_left_125">' . $description . '</div>
                </div>

                <div class="line_bottom">
                    <div class="width_90">Posted By :</div>
                    <div class="margin_left_125">' . $blog['author'] . '</div>
                </div>
                <div class="line_bottom">
                    <div class="width_90">Last Post :</div>
                    <div class="margin_left_125">' . date('M d, Y', strtotime($blog['modified'])) . '</div>
                </div>
                <div class="line_bottom">
                    <div class="width_90">Comments :</div>
                    <div class="margin_left_125">' . $blog['comment'] . '</div>
                </div>
            </div>';
        }
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $pageRequested = isset($_POST['pageId']) && !empty($_POST['pageId']) ? $_POST['pageId'] : $pageIdRequested;
            $pageRequested = 1;
            $output .= get_sp_pagination_link($paginationCount, 'forum', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;

    die;
}

function manage_blog($search_input, $search_type) {
    global $db;
    $condition = " where bg.deleted = 0 AND bg.type= 0 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' && bg.title like "%' . $search_input . '%" ';
                break;
            case "author":
                $condition .= ' && user.first_name like "%' . $search_input . '%" ';
                $condition .= ' && user.last_name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && ( bg.title like "%' . $search_input . '%" OR user.name like "%' . $search_input . '%" )';
        }
    }

    $query = "SELECT bg.*, user.user_name AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . "$condition  GROUP BY bg.id ORDER BY bg.id DESC ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT bg.*, user.user_name AS author, count(cm.id) AS comment FROM " . _prefix("blogs") . " AS bg "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=bg.created_by "
            . "  LEFT JOIN " . _prefix("blog_comments") . " AS cm ON cm.blog_id=bg.id "
            . "$condition  GROUP BY bg.id ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        foreach ($data as $key => $blog) {
            $description = strip_tags($blog['title']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
			$content= $blog['content'];
			 if($blog['img_name']!='')
			 {
				 $blog_img_name=$blog['img_name'];
			 }
			 else
			 
			 {
				 $blog_img_name='default.png';
			  }
            /*$output .= '<div class="group_one">
                    <div class="text-right"><a href="' . HOME_PATH . 'blog/view?id=' . md5($blog['id']) . '">Post Comment</a></div>
                <div class="line_bottom">
                    <div class="width_90">Subject :</div>
                    <div class="margin_left_125">' . $description . '</div>
                </div>

                <div class="line_bottom">
                    <div class="width_90">Posted By :</div>
                    <div class="margin_left_125">' . $blog['author'] . '</div>
                </div>
                <div class="line_bottom">
                    <div class="width_90">Last Post :</div>
                    <div class="margin_left_125">' . date('M d, Y', strtotime($blog['modified'])) . '</div>
                </div>
                <div class="line_bottom">
                    <div class="width_90">Comments :</div>
                    <div class="margin_left_125">' . $blog['comment'] . '</div>
                </div>
            </div>';*/
			
					/**************************************laout for blog section*****************************************/
		 $output .= '<div class="association-section blog-content">
		 
		 <div class="associate-content-box">
		 
                 <div class="col-xs-3"><div class="association-img"><img title="Community Lorem Ipsum" src="admin/files/pages/image/'.$blog_img_name.'" alt="logo Image"></div></div>  
                 <div class="col-xs-9">
			     <div class="association-text">
				   <div class="headline">
				 	<h2>'. $description .'</h2>
					 </div>
                      ' .$content . '
                
				
			
                    <p>Posted By : &nbsp;' . $blog['author'] . '</p>
                
               
                    <p>Last Post : &nbsp;' . date('M d, Y', strtotime($blog['created'])) . '</p>
            
            
                    <p>Comments : &nbsp;' . $blog['comment'] . '</p>
				 <p><a href="' . HOME_PATH . 'blog/view/'.md5($blog['id']).'">Read More</a></p>
          
				
				
				<div class="clearfix"></div>
				
				</div>
				<div class="clearfix"></div>
            </div>
			<div class="clearfix"></div>
			</div>
			
			</div>';
              
		
		
		
		/***********************************end of layout of blog sectiion**********************************/
			
        }
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'blog', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;

    die;
}

function manage_artWork() {
    global $db;
    $condition = " where deleted = 0 ";
    $query = "SELECT id  FROM " . _prefix("artwork_users") . "$condition";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT aUsers.* , CONCAT(IFNULL(users.first_name,''),'',IFNULL(users.last_name,'')) AS user_name  FROM " . _prefix("artwork_users") . " AS aUsers   "
            . "LEFT JOIN " . _prefix("users") . " AS users ON  aUsers.user_id= users.id "
            . "where aUsers.deleted=0 ORDER BY aUsers.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<table class="table table-striped table-hover  table-bordered">
                    <thead>
                    <tr><th width="5%" align="left">S.No</th>
                    <th width="20%" align="left">Artwork Description</th>
                    <th width="15%" align="left">Client Expected Date</th>
                    <th width="15%" align="left">Client Given Artwork</th>
                    <th width="15%" align="left">Admin Expected Date</th>
                    <th width="15%" align="left">Admin Given Artwork</th>
                    <th width="20%" align="left">Admin Description</th>
                    <th width="10%" align="left">Action</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $lastChanges = ($record['modified'] == '0000-00-00 00:00:00') ? date('M d, Y', strtotime($record['created'])) : date('M d, Y', strtotime($record['modified']));
            $output .= '<tr class="row_' . $record['id'] . '" >';
            $output .= '<td>' . $i . ' .</td>';
            $output .= '<td>' . $record['user_name'] . '</td>';
            $output .= '<td><a href="' . HOME_PATH_URL . 'artwork.php?partwork&id=' . md5($record['id']) . '"><i class="fa fa-envelope"></i>&nbsp;View</a></td>';
            $output .= '<td>' . $record['total_requested'] . '</td>';
            $output .= '<td>' . $record['total_completed'] . '</td>';
            $output .= '<td>' . (($record['total_requested'] - $record['total_requested']) >= 0 ? ($record['total_requested'] - $record['total_requested']) : 0) . '</td>';
            $output .= '<td>' . $lastChanges . '</td>';
            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="artwork_users-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-artwork_users-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $output .='</tbody></table>';
        $paging_query = "SELECT id, user_id, artw_user_id, client_given, admin_given, client_description, admin_description, expected_date, admin_expected_date, status, created  FROM " . _prefix("artwork_users") . "   " . "$condition ";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_pagination_link($paginationCount, 'client', $data, $colspan);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    die;
}

function manage_personalArtWork($uid) {
    global $db;
    $condition = " where aUsers.deleted = 0 AND aUsers.user_id='" . $uid . "' AND aUsers.pro_id='" . $_SESSION['proId'] . "' ";
    $query = "SELECT aUsers.* , CONCAT(IFNULL(users.first_name,''),'',IFNULL(users.last_name,'')) AS user_name  FROM " . _prefix("artworks") . " AS aUsers   "
            . "LEFT JOIN " . _prefix("users") . " AS users ON  aUsers.user_id= users.id "
            . "$condition ORDER BY aUsers.id DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT aUsers.* , CONCAT(IFNULL(users.first_name,''),'',IFNULL(users.last_name,'')) AS user_name  FROM " . _prefix("artworks") . " AS aUsers   "
            . "LEFT JOIN " . _prefix("users") . " AS users ON  aUsers.user_id= users.id "
            . "$condition ORDER BY aUsers.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord">
   <thead>
      <tr>
         <th class="col-md-1">S.No</th>
                    <th class="col-md-2">Title</th>
                    <th class="col-md-2">Expected Date</th>
                    <th class="col-md-2">Client Artwork</th>
                    <th class="col-md-1">Admin Return</th>
                    <th class="col-md-1">Artwork Status</th>
                    <th class="col-md-1">Download</th>
                    <th class="col-md-2">Action</th>
         </tr>
   </thead>
   <tbody>';

        foreach ($data as $key => $record) {
            $verified_status = ($record['approve_status'] == 1) ? 'Recieved' : 'Requested';
            $verified = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img1 = $verified_status . ' <img src="' . $verified . '" title="' . $verified_status . '" />';
            $client_description = (strlen($record['client_description']) > 40) ? substr($record['client_description'], 0, 40) . '...' : $record['client_description'];
            $output .= '<tr class="row_' . $record['id'] . '" >';
            $output .= '<td class= "text-center">' . $i . ' .</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td class= "text-center">' . date('M d, Y', strtotime($record['expected_date'])) . '</td>';
            $fileImage = ADMIN_PATH . 'files/artwork/thumb_' . $record['client_given'];
            $fileImageView = HOME_PATH . 'admin/files/artwork/' . $record['client_given'];
            if (file_exists($fileImage)) {
                $output .=!empty($record['client_given']) ? '<td class= "text-center"><a  class="fancybox" title="' . $record['client_given'] . '" href="' . $fileImageView . '"><img  title="View Image"  src="' . MAIN_PATH . '/files/artwork/thumb_' . $record['client_given'] . '"></a><br/><br/></td>' : '';
            } else {
                $output .= '<td class= "text-center">N/A</td>';
            }
            $fileImageadmin = ADMIN_PATH . 'files/artwork/thumb_' . $record['admin_returned'];
            $fileImageViewadmin = HOME_PATH . 'admin/files/artwork/' . $record['admin_returned'];
            if (file_exists($fileImageadmin)) {
                $output .=!empty($record['admin_returned']) ? '<td class= "text-center"><a  class="fancybox" title="' . $record['client_given'] . '" href="' . $fileImageViewadmin . '"><img  title="View Image"  src="' . MAIN_PATH . '/files/artwork/thumb_' . $record['admin_returned'] . '"></a><br/><br/></td>' : '';
            } else {
                $output .= '<td class= "text-center">Pending</td>';
            }
            $output .= '<td class= "text-center">' . $verified_status . '</td>';
            if (file_exists($fileImageadmin)) {
                $output .=!empty($record['admin_returned']) ? '<td class= "text-center"><a href="' . MAIN_PATH . 'download.php?type=artwork&file=' . $record['admin_returned'] . '"><button style="padding:6px 8px; font-size:13px; margin-bottom:0px; " class="btn btn-danger center-block btn_search btn_pay" >DOWNLOAD</button></a></td>' : 'N/A';
            } else {
                $output .= '<td class= "text-center">N/A</td>';
            }
// $output .= '<td class= "text-center"><a href="' . MAIN_PATH . 'download.php?type=artwork&file=' . $record['client_given'] . '"><button style="padding:6px 8px; font-size:13px; margin-bottom:0px; " class="btn btn-danger center-block btn_search btn_pay" >DOWNLOAD</button></a></td>';
            $output .= '<td><span style="margin-right:5px;"><a href="' . HOME_PATH . 'supplier/artwork?edit&id=' . md5($record['id']) . '"><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-artworks-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a></span>'
                    . '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $paging_query = "SELECT id, user_id, artw_user_id, client_given, admin_returned , client_description, admin_description, expected_date, admin_expected_date, status, created  FROM " . _prefix("artworks") . "   " . "Where deleted=0 AND md5(artw_user_id)='" . $rid . "'";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 8;
            $output .= get_sp_pagination_link($paginationCount, 'partwork', $data, $colspan, $_POST['pageId']);
        }
        $output .='</tbody></table>';
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script>

        $(".fancybox").fancybox();
        $(".fancybox1").fancybox();
        $(document).ready(function() {

            var path = '<?php echo HOME_PATH; ?>ajaxFront.php';
            /*
             * Code for Delete
             */

            $('.delete').live('click', function() {
                var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
                    var details = $(this).attr('id').split('-');
                    var table = details[1];
                    var id = details[2];
                    $.ajax({
                        type: 'POST',
                        url: path,
                        data: {action: 'delete', id: id, table: table},
                        success: function() {
                            $('.row_' + id).hide();
                        }
                    });
                }
            });

        });
    </script>

    <?php
    die;
}

function feedback_center($uid ,$proId) {
    global $db;
     $getAllRating = overAllRating($uid);
	
	// echo $getAllRating1 = OverAllNEWRatingProduct($proId);
	$condition = " where fdbk.sp_id='" . $uid . "' AND pds.id='" . $_SESSION['proId'] . "' AND fdbk.deleted=0";
   // $condition = " where fdbk.sp_id='" . $uid . "' AND pds.id='" . $_SESSION['proId'] . "' AND fdbk.deleted=0 AND fdbk.status=1 ";
     $query = "SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "$condition ORDER BY fdbk.created DESC";
    $res = $db->sql_query($query);
	$count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "$condition ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    //prd($data);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
     $overAllRatingWgt = $dataRating[0]['overall_rating'];
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord">
   <thead>
      <tr>
         <!----<th class="col-md-1 text-center">S.No</th>---->
                    <!----<th class="col-md-3">Service/Product</th>-->
                    <th class="col-md-5">Comment to management</th>
                    <th class="col-md-2">Mark as read</th>
                    <th class="col-md-5">Rating</th>
         </tr>
   </thead>
   <tbody>';
        $TotalRating = 0;
        foreach ($data as $key => $record) {
            $i = 1;
            $verified_status = ($record['approve_status'] == 1) ? 'Completed' : 'Pending';
            $verified = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $img1 = $verified_status . ' <img src="' . $verified . '" title="' . $verified_status . '" />';
            $client_description = (strlen($record['client_description']) > 40) ? substr($record['client_description'], 0, 40) . '...' : $record['client_description'];
            $feedback_reply = (strlen($record['feedback_reply']) > 40) ? substr($record['feedback_reply'], 0, 40) . '...' : $record['feedback_reply'];
			$reviewStatus = $record['status'];
            $TotalRating = $TotalRating + (($QualityOfServiceWgt * $record['quality_of_care']) + ($cStaffWgt * $record['caring_staff']) + ($resManWgt * $record['responsive_management']) + ($tripWgt * $record['trips_outdoor_activities']) + ($indoorEntWgt * $record['indoor_entertainment']) + ($socialWgt * $record['social_atmosphere']) + ($foodWgt * $record['enjoyable_food']) + ($overAllRatingWgt * $record['overall_rating']));
            switch ($record['quality_of_care']) {
                case 1:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $service_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $service_div = 'Not Rated';
                    break;
            }
            switch ($record['caring_staff']) {
                case 1:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $value_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $value_div = 'Not Rated';
                    break;
            }
            switch ($record['responsive_management']) {
                case 1:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $activity_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $activity_div = 'Not Rated';
                    break;
            }
            switch ($record['trips_outdoor_activities']) {
                case 1:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $management_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $management_div = 'Not Rated';
                    break;
            }
            switch ($record['indoor_entertainment']) {
                case 1:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $indoorEnt_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $indoorEnt_div = 'Not Rated';
                    break;
            }
            switch ($record['social_atmosphere']) {
                case 1:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $social_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $social_div = 'Not Rated';
                    break;
            }
            switch ($record['enjoyable_food']) {
                case 1:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $food_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $food_div = 'Not Rated';
                    break;
            }
            switch ($record['overall_rating']) {
                case 1:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $overall_div = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $overall_div = 'Not Rated';
                    break;
            }

            $output .= '<tr class="row_' . $record['id'] . '" >';
            // $output .= '<td class= "text-center">' . $i . ' .</td>';
            //$output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>';

            if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                $output .='
            <p class="text-center abuse_div abuse-' . $record['id'] . '"><a style="color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a></p>
                            ';
            } else if ($record['abused'] == 1) {
                $output .='<p style="color:#f15922;font-style: italic;" class="text-center">Moderated by Admin</p>';
            } else {
                $output.='<p style="color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
            }     
           
             $output.= $record['3'].'<br/>'.$record['feedback'] . '<br><a href = "' . HOME_PATH . 'supplier/feedback-view?id=' . md5($record['id']) . '">View full comments</a><br>';
            
  if($feedback_reply){$output .= '<strong><i>Your reply:</i></strong> '.$feedback_reply.'<br><i><a href = "' . HOME_PATH . 'supplier/feedback-view?id=' . md5($record['id']) . '#reply_here">Edit reply</a></i>';}
        else{    
$output .= '            <i><a href = "' . HOME_PATH . 'supplier/feedback-view?id=' . md5($record['id']) . '#reply_here">Reply to review</a></i>';
			}
			if($reviewStatus==0){
				$output .='<p style="font-size:13px;">Note: This review is not yet online as it is currently awaiting Aged Advisor admin approval.</p>';
			}
            $output .='</td>';
            if ($record['read_status'] == 0) {
                $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;" ><i class ="read_status fa fa-file-text fa-2x" title="Un Read" id="feedbacks-' . $record['id'] . '-' . $record['read_status'] . '" style="color:red;cursor:pointer;"></i></span></td>';
            } else {
                $output .= '<td><i class = "fa fa-check-square fa-2x" title="Read"></i></td>';
            }
             $output .= '<td>Quality of Care : ' . $service_div . ' </br>'
                    . 'Caring/Helpful Staff : ' . $value_div . '</br>'
                    . 'Responsive Management : ' . $activity_div . '</br>'
                    . 'Trips/Outdoor Activities : ' . $management_div . '</br>'
                    . 'Indoor Entertainment : ' . $indoorEnt_div . '</br>'
                    . 'Social Atmosphere : ' . $social_div . '</br>'
                    . 'Enjoyable Food : ' . $food_div . '</br>'
                    . 'Overall Rating : ' . $overall_div . '</td>';
            $output .= '</td>';
            $output .= '</tr>';

            $i ++;
        }
//            $Rating = ($TotalRating / 4);
//            $overallRating = round($Rating);
//            switch ($overallRating) {
//                case 1:
//                    $all_Rating = '<i class = "fa fa-star star"></i> <i class = "fa fa-star"></i> <i class = "fa fa-star"></i> <i class = "fa fa-star"></i> <i class = "fa fa-star"></i>';
//                    break;
//                case 2:
//                    $all_Rating = '<i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i> <i class = "fa fa-star"></i> <i class = "fa fa-star"></i> <i class = "fa fa-star"></i>';
//                    break;
//                case 3:
//                    $all_Rating = '<i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i> <i class = "fa fa-star"></i> <i class = "fa fa-star"></i>';
//                    break;
//                case 4:
//                    $all_Rating = '<i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i> <i class = "fa fa-star"></i>';
//                    break;
//                case 5:
//                    $all_Rating = '<i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i> <i class = "fa fa-star star"></i>';
//                    break;
//                default:
//                    $all_Rating = '<i class = "fa fa-star"></i> <i class = "fa fa-star"></i> <i class = "fa fa-star"></i> <i class = "fa fa-star"></i> <i class = "fa fa-star"></i>';
//                    break;
//            }

        $paging_query = "SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
                . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
                . "$condition ORDER BY fdbk.created DESC";
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 8;
            $output .= get_sp_pagination_link($paginationCount, 'feedback', $data, $colspan, $_POST['pageId']);
        }
        $output .='</tbody></table>';
        $output .='<div class = "col-sm-offset-2 col-sm-8 margin">
            <span>Overall Rating: ' .  OverAllNEWRatingProduct($proId). '</span>
            </div>';
    } else {
        $output = '<table class = "table table-bordered table_bord"><tbody><tr><td class = "text-center text-danger">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script>

        $(".fancybox").fancybox();
        $(".fancybox1").fancybox();
        $(document).ready(function() {

            var path = '<?php echo HOME_PATH; ?>ajaxFront.php';

            $('.read_status').live('click', function() {
                var details = $(this).attr('id').split('-');
                var table = details[0];
                var id = details[1];
                var read_status = details[2];
                var image = "<?php echo ADMIN_IMAGE . 'approval.gif' ?>";
                $('.status-' + id).html('<img src=' + image + ' >');

                //alert(table);
                //return false;
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'readStatus', id: id, table: table, read_status: read_status},
                    success: function(response) {
                        $('.status-' + id).html($.trim(response));
                    }
                });
            });
            /*
             * Code for Delete
             */

            $('.delete').live('click', function() {
                var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
                    var details = $(this).attr('id').split('-');
                    var table = details[1];
                    var id = details[2];
                    $.ajax({
                        type: 'POST',
                        url: path,
                        data: {action: 'delete', id: id, table: table},
                        success: function() {
                            $('.row_' + id).hide();
                        }
                    });
                }
            });

        });
    </script>

    <?php
    die;
}

function manage_gallery($uid) {
    global $db;
    $gallery_type = 0;
    $condition = " where deleted = 0 ";
    $query = "SELECT id, title, file FROM " . _prefix("galleries") . " where deleted = 0 AND pro_id=".$_SESSION['proId']." AND user_id=" . $uid . ' AND type=' . $gallery_type;
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
 
    $pageLimit = PAGE_PER_NO * $id;

    /*$query = "SELECT id, title,file, status,type, created FROM " . _prefix("galleries") . "   "
            . " where deleted = 0 AND pro_id=".$_SESSION['proId']." AND user_id=" . $uid . ' AND type=' . $gallery_type . "  ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;*/
			
		   $query_check_radio_value = "SELECT * FROM ad_galleries 
            where deleted = '0' AND pro_id={$_SESSION['proId']} AND user_id={$uid}  AND type= {$gallery_type} AND status_first_img='1'" ;
			$check_radio=mysqli_query($db->db_connect_id, $query_check_radio_value);

			while($check_radio_value=mysqli_fetch_array($check_radio))
			{
				 $check_radio_file=  $check_radio_value['file'];
				  $check_radio_status_first_img= $check_radio_value['status_first_img'];
                   $img_id= $check_radio_value['id'];

					 if( $check_radio_status_first_img==1)
					 {
						$checkvalue= "checked='checked'";
					 }
					 else
					 {
						  $checkvalue= '';
					}
		    }
        
			
	
	
	
	 $query = "SELECT id, title,file, status,type, created FROM " . _prefix("galleries") . "   "
            . " where deleted = 0 AND pro_id=".$_SESSION['proId']." AND user_id=" . $uid . ' AND type=' . $gallery_type . "  ORDER BY status_first_img
 DESC limit " . $pageLimit . ',' . PAGE_PER_NO;		

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    $output = '<div class="tab-pane fade in active" id="home">';
    if ($count > 0) {
        $output .= '<div class="table-responsive">
                        <table class="table table-bordered table_bord gallery_bord">
                        <thead><tr>
                        <th class="th_text" align="center">Thumbnail</th>
                        <th class="th_text">Images title</th>
						<th class="th_text">Select Ist Image</th>
                        <th class="th_text">Options</th>
                        </tr></thead>
                  <tbody>';
        foreach ($data as $key => $record) {

                if($record['id']==$img_id){
                    $checked = $checkvalue;
                }
                else{
                $checked=' ';
                }
			
			 $record['file'];
             $fileImage = ADMIN_PATH . 'files/gallery/images/thumb_' . $record['file'];
             $fileImageSrc = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
             $fileImageView = HOME_PATH . 'admin/files/gallery/images/' . $record['file'];

             $output.='<tr class="row_' . $record['id'] . '" >';
            $output.='<td style="width:20%;">';
            $output.='<div class="row">';
            $output.='<div class="col-md-12 col-sm-12 col-xs-12 text-center">';
            

            if (file_exists($fileImage)) {
                $output .=!empty($record['file']) ? '<img title="View Image"  src="' . $fileImageSrc . '" alt="" >' : "";
            } else {
                $output .= 'N/A';
            }
                
            $output.='</div>';
            $output.='</div>';
            $output.='</td>';
            $output.='<td style="width:40%;">' . $record['title'] . ' </td>';
			$output.='<td style="width:40%;"><input type="radio" id="galley_image" name="gallery_image" class="galley_image" value='.$record['id'].' '.$checked.'> </td>';
           
		    $output.='<td class="table_view text-center" style="width:22%;"><a id="edit" class= title="' . $record['file'] . '" href="editGallery">Edit</a> <a id="imagefirst_id"   class="fancybox" title="' . $record['file'] . '" href="' .$fileImageView . '">View</a> <a class="last delete"  href="javascript:void(0);"  id="del-galleries-' . $record['id'] . '">Delete</a></td>';
            $output.='</tr>';

            $i ++;
        }
         $output.='</tbody></table>
                       </div>';

        $paging_query = "SELECT id, title,file FROM " . _prefix("galleries") . "   " . " where deleted = 0 AND pro_id=".$_SESSION['proId']." AND user_id=" . $uid . ' AND type=' . $gallery_type;
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            if (isset($search_type)) {
                $data .="&search_input=$search_input&search_type=$search_type";
            }
            $output .= get_pagination_link($paginationCount, 'gallery', $data, $colspan);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    $output .= '</div>';

    $query = "SELECT id, title, file FROM " . _prefix("galleries") . " where deleted = 0 AND pro_id=".$_SESSION['proId']." AND user_id=" . $uid . ' AND type IN (1,2)';
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
 
    $pageLimit = PAGE_PER_NO * $id;

    $query = "SELECT id, title,file,type FROM " . _prefix("galleries") . "   "
            . " where deleted = 0  AND user_id=" . $uid . " AND type IN (1,2) ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    $output .= '<div class="tab-pane fade" id="ios">';
    if ($_SESSION['videoEnable'] == 1) {
        if ($count > 0) {
            $output .= '<div class="table-responsive">
                            <table class="table table-bordered table_bord gallery_bord" id="pageData">
                            <thead><tr>
                            <th class="th_text">Video Title</th>
                            <th class="th_text">Video Type</th>
                            
							<th class="th_text">Options</th> 
                       </tr></thead>
                       <tbody>';
            $j=1;
            foreach ($data as $key => $record) { 
                $playImage = HOME_PATH . 'images/play.png';
                $fileVideo = ADMIN_PATH . 'files/gallery/videos/' . $record['file'];
                $output.='<tr class="row_' . $record['id'] . '" >';
                $output.='<td style="width:30%;">' . $record['title'] . ' </td>';
				
                if ($record['type'] == 1) {
                    if (file_exists($fileVideo)) {
                        $output.='<td style="width:40%;text-align:center;">Uploaded Video </td>';

                        $output .=!empty($record['file']) ? '<td class="table_view" style="width:22%;"><a  class="videoPlayer" href="#videoShow'.$j.'"><img  title="Play Video" src="' . $playImage . '"></a> <a class="last delete"  href="javascript:void(0);" id="del-galleries-' . $record['id'] . '">&nbsp;Delete</a><div id="videoShow'.$j.'" style="display:none" >'
                                . '<object id="flowplayer'.$j.'" width="400" height="288" data="'.HOME_PATH.'js/flowplayer-3.2.7.swf" type="application/x-shockwave-flash">'
                                . '<param name="allowfullscreen" value="true" />'
                                . '<param name="flashvars" value="config={\'clip\':{\'url\':\''.HOME_PATH . 'admin/files/gallery/videos/' . $record['file'].' \',\'autoPlay\':false}}">'
                                . '</object>'
                                . '</div></td>' : '';
                    } else {
                        $output .= '<td>N/A</td><td><a class="last delete"  href="javascript:void(0);" id="del-galleries-' . $record['id'] . '">&nbsp;Delete</a></td>';
                    }
                } else if ($record['type'] == 2) {
                    $output.='<td style="width:40%;">Youtube Video </td>';

                    $output .=!empty($record['file']) ? '<td class="table_view" style="width:20%;"><a  class="youtube" href="' . $record['file'] . '"><img  title="Play Video" src="' . $playImage . '"></a> <a class="last delete"  href="javascript:void(0);" id="del-galleries-' . $record['id'] . '">&nbsp;Delete</a></td>' : '';
                } else {
                    $output .= '<td>N/A</td><td><a class="last delete"  href="javascript:void(0);" id="del-galleries-' . $record['id'] . '">&nbsp;Delete</a></td>';
                }
                $output.='</tr>';

                $i ++;
                $j++;
            }
            $output.='</tbody> </table></div>';

            $paging_query = "SELECT id, title,file FROM " . _prefix("galleries") . "   " . " where deleted = 0  AND user_id=" . $uid . ' AND type IN (1,2)';
            $paging_res = $db->sql_query($paging_query);
            $pagingCount = $db->sql_numrows($paging_res);
            if ($pagingCount > 0) {
                $data = '';
                $paginationCount = getPagination($pagingCount);
                $colspan = 6;
                if (isset($search_type)) {
                    $data .="&search_input=$search_input&search_type=$search_type";
                }
                $output .= get_sp_pagination_link($paginationCount, 'gallery', $data, $colspan, $_POST['pageId']);
            }
        } else {
            $output.= '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
        }
    } else {
        $output.= '<div class="tab-pane" id="ios"><div class="table-responsive"><table class="table table-bordered table_bord"><tbody><tr><td class="text-center">To Upload Video. Please Upgrade Plan</td></tr></tbody></table></div></div>';
    }

    $output .= '</div>';

    echo $output;
    ?>
     <input type="hidden" value="<?php echo $uid; ?>" id= "user_id">
     <input type="hidden" value="<?php  echo $_SESSION['proId'];?>" id="product_id">
    <script type="text/javascript">
        $(".image").colorbox({
            width: '600px',
            height: '500px;'
        });
        $(document).ready(function() {
            $(".videoPlayer").fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'speedIn': 600,
                'speedOut': 200
            });
            $('.youtube').fancybox({
                openEffect: 'none',
                closeEffect: 'none',
                prevEffect: 'none',
                nextEffect: 'none',
                arrows: false,
                helpers: {
                    media: {},
                    buttons: {}
                }
            });
            $(".fancybox").fancybox();

            var path = '<?php echo HOME_PATH;?>ajaxFront.php';

            $('.status').live('click', function() {
                var details = $(this).attr('id').split('-');
                var table = details[0];
                var id = details[1];
                var status = details[2];
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'status', id: id, table: table, status: status},
                    success: function(response) {
                        $('.status-' + id).html($.trim(response));
                    }
                });
            });
            /*
             * Code for Delete
             */

            $('.delete').live('click', function() {
                var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
                    var details = $(this).attr('id').split('-');
                    var table = details[1];
                    var id = details[2];
                    $.ajax({
                        type: 'POST',
                        url: path,
                        data: {action: 'deleteFile', id: id, table: table},
                        success: function() {
                            $('.row_' + id).hide();
                        }
                    });
                }
            });

            
    });

        </script>
    <?php 
    die;
}

function articles_list() {

    global $db;
    $query = "SELECT page.*, concat(user.first_name,' ',user.last_name) AS editor FROM " . _prefix('pages') . " AS page LEFT JOIN " . _prefix('users') . " AS user ON page.created_by=user.id  WHERE page.status=1 && page.deleted= 0 ORDER BY page.id DESC ";
    $resArt = $db->sql_query($query);
    $count = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getArticles = "SELECT page.*, concat(user.first_name,' ',user.last_name) AS editor FROM " . _prefix('pages') . " AS page LEFT JOIN " . _prefix('users') . " AS user ON page.created_by=user.id  WHERE page.status=1 && page.deleted= 0 ORDER BY page.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resArticles = $db->sql_query($getArticles);
    $count = $db->sql_numrows($resArticles);
    $dataArticles = $db->sql_fetchrowset($resArticles);
    if ($count > 0) {
        foreach ($dataArticles as $article) {
            $desClass = 'col-lg-10 col-md-10 ';
            $pullLeft = 'padding-left:0px; ';
            $short_description = (strlen($article['content']) > 150) ? substr($article['content'], 0, 150) . '...' : $article['content'];
            if ($article['banner_image'] == '') {
                $image = '';
                $desClass = 'col-lg-12 col-md-12 ';
                $pullLeft = '';
            } else {

                if (@getimagesize(DOCUMENT_PATH . 'admin/files/pages/banner/thumb_' . $article['banner_image'])) {
                    $href = MAIN_PATH . 'files/pages/banner/thumb_' . $article['banner_image'];
                    $image = '<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 col_lg_bg padding_left">
                        <a class="pull-left img_post" href="#"><img class="img-responsive" src="' . $href . '"></a>
                    </div>';
                }
            }
            $output .= '<div class="col-md-12">
                <div class="media">
                    <h2 class="col_h2" style="padding-left:0px;"><a href="' . HOME_PATH . 'articles/view/' . base64_encode($article['id']) .'/'.str_replace(' ', '-', $article['page_title']).'">' . $article['page_title'] . '</a> </h2>
                    <div class="posted" style="padding-left:0px;">Posted on : <span>' . date('M d, Y', strtotime($article['created'])) . '</div>
                    ' . $image . '
                    <div class="' . $desClass . 'col-sm-12 col-xs-12 padding_left">

                            <p class="p_text" style="' . $pullLeft . '">' . $short_description . '</p>
                            <a class="pull-right read_more" href="' . HOME_PATH . 'articles/view/' . base64_encode($article['id']) .'/'.str_replace(' ', '-', $article['page_title']).'">Read more
                            </a>

                    </div>
                </div>
            </div>';
        }
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'articles', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;

    die;
}

function global_search($searchvalue) {
    global $db;
    if ($searchvalue != '') {
        $condition = " where prd.deleted = 0 && ( prd.title like  '%" . $searchvalue . "%' OR cit.title like '%" . $searchvalue . "%' OR srv.name like '%" . $searchvalue . "%' OR prd.keyword like '%" . $searchvalue . "%')";
        $query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " ORDER BY prd.id DESC ";
        $resArt = $db->sql_query($query);
        $count = $db->sql_numrows($resArt);
        $i = 1;
        if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
            $id = $_POST['pageId'];
            $i = $i + PAGE_PER_NO * $id;
        } else {
            $id = '0';
            $i = 1;
        }
        $pageLimit = PAGE_PER_NO * $id;
        $getArticles = "SELECT prd.id, prd.title,sp.first_name AS spFName, sp.last_name AS spLName, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd "
                . "Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type "
                . "LEFT JOIN " . _prefix("users") . " AS sp ON  prd.supplier_id= sp.id "
                . "Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " ORDER BY prd.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
        $resArticles = $db->sql_query($getArticles);
        $count = $db->sql_numrows($resArticles);
        $dataArticles = $db->sql_fetchrowset($resArticles);
        if ($count > 0) {
            foreach ($dataArticles as $article) {
                // $short_description = (strlen($article['short_description']) > 150) ? substr($article['short_description'], 0, 150) . '...' : $article['short_description'];
                if ($article['banner_image'] == '') {
                    $image = '';
                    $desClass = 'col-lg-12 col-md-12 ';
                    $pullLeft = '';
                } else {

                    if (file_exists(DOCUMENT_PATH . 'admin/files/gallery/images/' . $article['image'])) {
                        $href = HOME_PATH . 'admin/files/gallery/images/' . $article['image'];
                        $widthStyle = '';
                    }
                    $image = '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col_lg_bg" style="' . $widthStyle . '">
                        <a href="' . HOME_PATH . "search/productDetail?&id=" . md5($article['id']) . '"><img class="img-responsive" src="' . $href . '"></a>
                    </div>';
                }
                $output .= '<div class="col-md-12">
                <div class="media">
                    <h2 class="col_h2"><a href="' . HOME_PATH . "search/productDetail?&id=" . md5($article['id']) . '">' . $article['title'] . '</a> </h2>
                    <div class="posted" style="padding-bottom:0;">Facility Type: <span>' . $article['name'] . '</span></div>
                    <div class="posted" style="padding-bottom:0;">Location: <span>' . $article['city'] . '</span></div>
                    <div class="posted">Supplier Name: ' . $article['spFName'] . ' ' . $article['spLName'] . '</div>

                    ' . $image . '
                    <div class="' . $desClass . 'col-sm-12 col-xs-12 padding_left"><p class="p_text" style="' . $pullLeft . '"">' . $article['description'] . '</p></div>
                </div>
            </div>';
            }
            $pagingCount = $count;
            if ($pagingCount > 0) {
                $data = $searchvalue;
                $paginationCount = getPagination($pagingCount);
                $colspan = 6;
                $output .= get_sp_pagination_link($paginationCount, 'globalsearch', $data, $colspan, $_POST['pageId']);
            }
        } else {
            $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    die;
}

function review_list() {

    global $db;
    $query = "SELECT fdbk.* , pds.id as prId, pds.title,pds.image as pImage, sp.user_name as spUsername,  sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername,  cs.first_name AS csFName, cs.last_name AS csLName,pds.quick_url  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
            . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . "WHERE  pds.title !='' AND fdbk.status=1 AND  fdbk.deleted=0  ORDER BY fdbk.id DESC";
    $resArt = $db->sql_query($query);
    $count1 = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getReviews = "SELECT fdbk.* ,IFNULL(fdbk.title,'N/A') as title,pds.id as prId, pds.title as pdtitle, pds.id AS pImage, sp.id as sp_id, sp.user_name as spUsername,  sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername,  cs.first_name AS csFName, cs.last_name AS csLName,pds.quick_url  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
            . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . "WHERE  pds.title !='' AND fdbk.status=1 AND  fdbk.deleted=0 ORDER BY fdbk.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $data = $db->sql_fetchrowset($resReviews);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSum = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';
    $qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
    $resultKnowMe = $db->sql_query($qryKnowMe);
    $knowMeData = $db->sql_fetchrowset($resultKnowMe);
    foreach ($knowMeData as $key => $record) {
        $knowMe[$record['id']] = $record['title'];
    }
    $qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
    $resultDuration = $db->sql_query($qryDuration);
    $durationVisitData = $db->sql_fetchrowset($resultDuration);
    foreach ($durationVisitData as $key => $record) {
        $durationVisit[$record['id']] = $record['title'];
    }
    $lastVisit = array('','Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
    $recommended = array('No', 'Yes', 'Not sure');
    
    if ($count > 0) {
        foreach ($data as $key => $record) {
            if ($record['feedback'] != '') {
                if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) {
                    $image = '<a href="'. HOME_PATH .$record['quick_url'].'"><img src="' . HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']) . '" alt="product image"  /></a>';
                } else {
                    $image = '<a href="'. HOME_PATH .$record['quick_url'].'"><img src="' . HOME_PATH . 'images/no_image_available.png" alt="" /></a>';
                }
                $TotalRating = $record['overall_rating'];
                $Rating = $TotalRating;
                $overallRating = round($Rating);
                switch ($overallRating) {

                    case 1:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 2:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 3:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 4:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 5:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                        break;
                    default:
                        $all_Rating = 'Not Rated';
                        break;
                }
 $rattingpercent1 = $Rating*20;
  $str1 = '<div class="star-inactive"><div class="star-active" style="width:'.$rattingpercent1.'%;"></div></div>';
                $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
                //  $supplierName = (empty($record['spFName'])) ? 'N/A' : $record['spFName'] . '&nbsp;' . $record['spLName'];
                $title = (empty($record['title'])) ? 'N/A' : $record['title'];
                $feedback = (strlen($record['feedback']) > 20) ? substr($record['feedback'], 0, 20) . '...' : $record['feedback'];
                $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
                $reviews .= '<div class="col-md-12"><div class="col-md-2 col-sm-2 col-xs-12 pdding_none " style="margin-bottom:2%;">' . $image . '</div>
                    <div class="col-md-10 col-sm-10 col-xs-12 pdding_right padding_left_col">
                        <h3 class="hedding_three">' . $record['title'] . '</h3>
                        <p style="margin:0">' . $str1 . '</p>
                            <p><span style="color:rgb(138, 138, 138);
                            "  >Product/Service Name:&nbsp;<a href="'. HOME_PATH .$record['quick_url'].'">' . substr($record['pdtitle'], 0, 60) . '</a></span></br>
                                <!--span style="color:rgb(138, 138, 138);
                            " >Supplier:&nbsp;' . $supplierName . '</span><br/-->
                            <span style="color:rgb(138, 138, 138);
                            "  >Commented By:&nbsp;' . $commenter . '</span>';
                            
               if ($record['feedback_reply']){ $reviews .= '<br><span style="color:rgb(138, 138, 138);
                            "  ><i>Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</i></span>';  }          
                          $reviews .= '</p>';  
                if ($record['abused_request'] == 0 || $record['abused'] == 1) {
                    $reviews .= '<p><span class="margin_bottom feedback">' . ((empty($record['pros'])) ? 'N/A' : $pros) . '</span><br><a href="javascript:void(0)" class="read_more">Read more</a></p>'
                            . '<div style="display: none;" class="reads_more col-md-12 padding_none"><div class="css_apply row" style="border-top:1px solid #ddd; border-bottom:1px solid #ddd; margin:22px 0px;">';
//                    if (isset($record['feedback'])) {
//
//                        $reviews.= $record['feedback'] . '<br/>';
//                    } else {
//                        $reviews.= 'No record found <br/>';
//                    }

                    $reviews.='     <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Pros</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Cons</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . $record['feedback'] . '</div> 
                                    </div> '; 
                                    
            if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                $reviews .='<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-top:22px;color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a>
                            </div>';
            } else if ($record['abused'] == 1) {
                $reviews .='<div style="margin-bottom:0;margin-top:22px;" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg text-center"><span style="color:#f15922;font-style: italic;">Moderated by Admin</span></div>';
            } else {
                $reviews .='<p style="margin-top:22px;color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
            }                                    
                                    
                                    
                                    
                                    
                                       
 if ($record['feedback_reply']){
	 $record['feedback_reply']=str_ireplace("\n", '<br>', $record['feedback_reply']);
	 $reviews.=' <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left alert alert-info" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px;font-style: italic;">Reply from  ' . substr($record['pdtitle'], 0, 60) . '</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><i>' . $record['feedback_reply'] . '</i></div> 
                                    </div> ';}
                                        
$reviews.='                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
                                        <table class="table table-bordered table_bord">                   
                                            <thead>
                                                <tr>
                                                    <th width="" class="th_text">Categories</th>
                                                    <th width="" class="th_text">Rating</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <tr>
                                                    <td>Quality of Care</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['quality_of_care']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Caring/Helpful Staff</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['caring_staff']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Responsive Management</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['responsive_management']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>Trips/Outdoor Activities</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['trips_outdoor_activities']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Indoor Entertainment</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['indoor_entertainment']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>Social Atmosphere</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['social_atmosphere']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enjoyable Food</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['enjoyable_food']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>OVERALL RATING</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['overall_rating']) . '</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">How do you know about this place?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) . '</div> 
                                    </div> 

                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I have lived at / visited this facility for</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I last lived at / visited this facility</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . (($record['last_visit']=='') ? 'N/A' : $lastVisit[$record['last_visit']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">Would you recommend this facility to a friend?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . (($record['recommended'] =='') ? 'N/A' : $recommended[$record['recommended']]) . '</div> 
                                    </div>                    
                                </div>';
                    $reviews.= '</div><div class="col-md-6"></div></div>';
                }
if ($record['abused'] == 1) {
                $reviews .='<p style="color:#f15922;font-style: italic;">Moderated by Admin</p>';
            } else if ($record['abused_request'] == 1){
                $reviews .='<p style="color:#f15922;font-style: italic;" >This comment has been flagged for moderation</p>';
            }         
                $reviews .='</div>
                    </div>';
            }
        }
        $pagingCount = $count1;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $reviews .= get_sp_pagination_link($paginationCount, 'reviews', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $reviews = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $reviews;
    ?>
    <script>
        $(".fancybox").fancybox();
        $('.reads_more').hide();
        $('.read_more').click(function() {
            $(this).siblings('.feedback').toggle();
            //alert($(this).text());
            $(this).text() == 'Hide' ? $(this).text('Read more') : $(this).text('Hide');
            //$(".feedback").html("")
            // $(".css_apply").css({'background-color': '#e5eecc', 'border': 'solid 1px #c3c3c3', 'padding': '10px', 'width': '50%'});
            $(this).parent().next('div.reads_more').slideToggle("slow", function() {
                //$(this).text('Read More');
            });
        });
    </script>
    <?php
    die;
}

function review_listold() {


    // function getReviews() {
    global $db;
    $query = "SELECT * FROM " . _prefix('reviews') . " WHERE status=1 && deleted= 0 ORDER BY id DESC ";
    $resArt = $db->sql_query($query);
    $count = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getReviews = "SELECT * FROM " . _prefix('reviews') . " WHERE status=1 && deleted= 0 ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $dataReviews = $db->sql_fetchrowset($resReviews);
    $reviews = '';
//    if ($count > 0) {
//        foreach ($dataReviews as $review) {
//            $reviews .= '<div class="col-md-2 col-sm-2 col-xs-12 pdding_none" style="margin-bottom:2%;"><img src="images/img_project.jpg" alt=""></div>
//                    <div class="col-md-10 col-sm-10 col-xs-12 pdding_right padding_left_col">
//                        <h3 class="hedding_three">“' . $review['title'] . '”</h3>
//                        <p><img src="images/star.jpg" alt=""></p>
//                        <p class="margin_bottom">' . $review['short_description'] . '</p>
//                    </div>
//                    <div class="espesar"></div>';
//        }
//    }
//    return $reviews;
//}
//    global $db;
//    $query = "SELECT page.*, concat(user.first_name,' ',user.last_name) AS editor FROM " . _prefix('pages') . " AS page LEFT JOIN " . _prefix('users') . " AS user ON page.created_by=user.id  WHERE page.status=1 && page.deleted= 0 ORDER BY page.id DESC ";
//    $resArt = $db->sql_query($query);
//    $count = $db->sql_numrows($resArt);
//    $i = 1;
//    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
//        $id = $_POST['pageId'];
//        $i = $i + PAGE_PER_NO * $id;
//    } else {
//        $id = '0';
//        $i = 1;
//    }
//    $pageLimit = PAGE_PER_NO * $id;
//    $getArticles = "SELECT page.*, concat(user.first_name,' ',user.last_name) AS editor FROM " . _prefix('pages') . " AS page LEFT JOIN " . _prefix('users') . " AS user ON page.created_by=user.id  WHERE page.status=1 && page.deleted= 0 ORDER BY page.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
//    $resArticles = $db->sql_query($getArticles);
//    $count = $db->sql_numrows($resArticles);
//    $dataArticles = $db->sql_fetchrowset($resArticles);
    if ($count > 0) {
        foreach ($dataReviews as $article) {
            $abuse = ($article['abused'] == 1) ? 'disabused' : 'abused';
            $short_description = (strlen($article['short_description']) > 150) ? substr($article['short_description'], 0, 150) . '...' : $article['short_description'];
            if ($article['banner_image'] == '') {
                $image = '';
            } else {
                if (file_exists(DOCUMENT_PATH . 'admin/files/pages/feature_image/' . $article['banner_image'])) {
                    $href = MAIN_PATH . '/files/pages/feature_image/' . $article['banner_image'];
                    $image = '<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 col_lg_bg">
                        <a class="pull-left img_post" href="#"><img class="img-responsive" src="' . $href . '"></a>
                    </div>';
                } else {
                    $image = '<div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 col_lg_bg">
                        <a class="pull-left img_post" href="#"><img class="img-responsive" src="images/img_project.jpg"></a>
                    </div>';
                }
            }
            $output .= '<div class="col-md-12">
                <div class="media">
                    <h2 class="col_h2">' . $article['page_title'] . ' </h2>
                    <div class="posted">Posted on : <span>' . $article['created'] . ' </span><a href="#">' . $article['editor'] . '</a></div>
                    ' . $image . '
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 col_lg_bg">
                        <div class="media-body">
                            <p>' . $short_description . '</p>
                                <p><img src="images/star.jpg" alt=""></p>
                                </div>';
            if ($article['abused'] == 0) {
                $output .='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
            <span class="abuse_div abuse-' . $article['id'] . '"><a href="javascript:void(0);" id="reviews-' . $article['id'] . '-' . $article['abused'] . '" class="inner_btn abuse">Set Abuse</a>
                     </div>';
            } else {
                $output .='<div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left "><span class="abuse_div inner_btn">Abused</span></div>';
            }
            $output .='</div>
                </div>
            </div>';
        }

        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'articles', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script>
        $('.abuse').live('click', function() {
            var details = $(this).attr('id').split('-');
            var table = details[0];
            var id = details[1];
            var abuse = details[2];
            $.ajax({
                type: 'POST',
                url: '<?php echo HOME_PATH; ?>ajaxFront.php',
                data: {action: 'abuse', id: id, table: table, abuse: abuse},
                success: function(response) {
                    $('.abuse-' + id).html($.trim(response));
                }
            });
        });
    </script>
    <?php
    die;
}

function manage_product($uid, $search_input, $search_type) {
    global $db;
    $condition = "where prd.deleted = 0 AND prd.status=1 AND prd.id='" . $_SESSION['proId'] . "'";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' && prd.title like "%' . $search_input . '%" ';
                break;
            case "keyword":
                $condition .= ' && prd.keyword like "%' . $search_input . '%" ';
                break;
            case "author":
                $condition .= ' && cit.title like "%' . $search_input . '%" ';
                break;
            case "facility_type":
                $condition .= ' && srv.name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && ( prd.title like "%' . $search_input . '%" OR cit.title like "%' . $search_input . '%" OR srv.name like "%' . $search_input . '%" OR prd.keyword like "%' . $search_input . '%"  )';
        }
    }
    $query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC ";
    //  $query = "SELECT id  FROM " . _prefix("products") . " Where deleted=0 AND supplier_id=" . $uid;
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT prd.id,prd.address_city, prd.address, prd.title, prd.description, prd.keyword,prd.pro_extra_info, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
//        $query = "SELECT id, title, description, image, status FROM " . _prefix("products") . "   "
//                . "$condition ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
//    prd($data);
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord ">
                    <thead>
                    <tr>
                    <th class="col-md-1">S.No</th>
                    <th class="col-md-2">Title</th>
                    <th class="col-md-3">Address</th>
                   <th class="col-md-2">City</th>
                   <th class="col-md-2">Extra Info</th>
                    <th class="col-md-2">Action</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['address']);
            //$description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . ' .</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $description . '</td>';
            $output .= '<td>' .  $record['address_city'] . '</td>';
            if (empty($record['pro_extra_info'])) {
                $output .= '<td><a href=' . HOME_PATH . 'supplier/extraInfo?edit&pro_id=' . base64_encode($record['id']) . '&id=' . base64_encode($record['pro_extra_info']) . '>Add Extra Info</a></td>';
            } else {
                $output .= '<td><a href=' . HOME_PATH . 'supplier/extraInfo?edit&pro_id=' . base64_encode($record['id']) . '&id=' . base64_encode($record['pro_extra_info']) . '>Edit Extra Info</a></td>';
            }

            $output .= '<td><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="products-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
//                    . '<span style="margin-right:5px;"><a href=' . HOME_PATH . 'supplier/addProduct?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-products-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $output .='</tbody></table>';
        $paging_query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC ";
        // $paging_query = "SELECT *  FROM " . _prefix("products") . " Where deleted=0 AND supplier_id=" . $uid;
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'product', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script type="text/javascript">
        $(".image").colorbox({
            width: '600px',
            height: '500px;'
        });
        $(document).ready(function() {
            $(".videoPlayer").fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'speedIn': 600,
                'speedOut': 200
            });
            $(".fancybox").fancybox();

            var path = '<?php echo HOME_PATH; ?>ajaxFront.php';

            $('.status').live('click', function() {
                var details = $(this).attr('id').split('-');
                var table = details[0];
                var id = details[1];
                var status = details[2];

                //return false;
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'status', id: id, table: table, status: status},
                    success: function(response) {
                        $('.status-' + id).html($.trim(response));
                    }
                });
            });
            /*
             * Code for Delete
             */

            $('.delete').live('click', function() {
                var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
                    var details = $(this).attr('id').split('-');
                    var table = details[1];
                    var id = details[2];
                    $.ajax({
                        type: 'POST',
                        url: path,
                        data: {action: 'delete', id: id, table: table},
                        success: function() {
                            $('.row_' + id).hide();
                        }
                    });
                }
            });

        });
    </script>

    <?php
    die;
}

function manage_productviews($uid) {
    global $db;
    $condition = " where prd.deleted = 0 ";
    if (isset($search_type) && $search_input != '') {
        switch ($search_type) {
            case "title":
                $condition .= ' && prd.title like "%' . $search_input . '%" ';
                break;
            case "keyword":
                $condition .= ' && prd.keyword like "%' . $search_input . '%" ';
                break;
            case "author":
                $condition .= ' && cit.title like "%' . $search_input . '%" ';
                break;
            case "facility_type":
                $condition .= ' && srv.name like "%' . $search_input . '%" ';
                break;
            default :
                $condition .= ' && ( prd.title like "%' . $search_input . '%" OR cit.title like "%' . $search_input . '%" OR srv.name like "%' . $search_input . '%" OR prd.keyword like "%' . $search_input . '%"  )';
        }
    }
    $query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC ";
    //  $query = "SELECT id  FROM " . _prefix("products") . " Where deleted=0 AND supplier_id=" . $uid;
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT prd.id, prd.title, prd.description, prd.keyword,prd.pro_extra_info, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
//        $query = "SELECT id, title, description, image, status FROM " . _prefix("products") . "   "
//                . "$condition ORDER BY id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord ">
                    <thead>
                    <tr>
                    <th class="col-md-1">S.No</th>
                    <th class="col-md-2">Title</th>
                    <th class="col-md-2">Description</th>
                    <th class="col-md-2">Number of Reviews</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $key => $record) {

            $description = strip_tags($record['description']);
            $description = (strlen($description) > 60) ? substr($description, 0, 60) . '...' : $description;
            $totalviews = overAllRatingProductCount($record['id']);
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . ' .</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . $description . '</td>';

            $output .= '<td><a href="' . HOME_PATH . 'search/viewReview?id=' . md5($record['id']) . '" target=_blank><i class="fa fa-users"></i>&nbsp;' . $totalviews . '</a></td>';

            $output.= '</td>';
            $output .= '</tr>';

            $i ++;
        }
        $output .='</tbody></table>';
        $paging_query = "SELECT prd.id, prd.title, prd.description, prd.keyword, prd.image, srv.name, prd.status, cit.title as city FROM " . _prefix("products") . " AS prd Left join " . _prefix("services") . " AS srv ON srv.id=prd.facility_type Left join " . _prefix("cities") . " AS cit ON cit.id=prd.city_id " . $condition . " && prd.supplier_id = " . $uid . "  ORDER BY prd.id DESC ";
        // $paging_query = "SELECT *  FROM " . _prefix("products") . " Where deleted=0 AND supplier_id=" . $uid;
        $paging_res = $db->sql_query($paging_query);
        $pagingCount = $db->sql_numrows($paging_res);
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'productviews', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>


    <?php
    die;
}

function manage_order($uid) {
    global $db;

    $query = "SELECT " . _prefix("payments_test") . ".payid  FROM " . _prefix("payments_test") . "," . _prefix("direct_credit") . " Where " . _prefix("payments_test") . ".items =  " . _prefix("direct_credit") . ".our_order_number AND " . _prefix("direct_credit") . ".deleted=0 AND " . _prefix("direct_credit") . ".product_id='" . $_SESSION['proId'] . "' ORDER BY date DESC";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    //echo $query;
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }

    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT " . _prefix("payments_test") . ".*  FROM " . _prefix("payments_test") . "," . _prefix("direct_credit") . " Where " . _prefix("payments_test") . ".items =  " . _prefix("direct_credit") . ".our_order_number AND " . _prefix("direct_credit") . ".deleted=0 AND " . _prefix("direct_credit") . ".product_id='" . $_SESSION['proId'] . "' ORDER BY date DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        $output = '<table class="table table-bordered table_bord ">
                    <thead>
                    <tr>
                    <th class="col-md-4">Invoice (click to open)</th>
                    <th class="col-md-1">Amount</th>
                    <th class="col-md-1">Method</th>
                    <th class="col-md-3">Action</th>
                    <th class="col-md-2">Status</th>
                    <th class="col-md-2">DATE PAID</th>
                    </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $key => $records) {
//            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
//            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            

                  $output .=  '<tr>';
                        
       			
                      
					  $invoicenumber = $records['items'];
					  if(!is_numeric($invoicenumber)){$invoicenumber=0;}
					  if($invoicenumber<100000){$invoicenumber=0;}
					  
					  /*print_r($itemarray); 
					  echo "<br />";*/
					 if($records['token']!=''){$paymentmethod="PAYPAL";}else{$paymentmethod="Direct Credit";}	
					   	
					  
					 /* echo "<br />";
					 print_r($jarray);
					 */
					 
					 // $finalitem = implode(", ", $jarray);
					
                        $output .=  '<td class="col-md-4 text-left"><a href="/modules/supplier/process_display_invoice.php?on='.$invoicenumber.'" target="_blank">'.$invoicenumber.'</a></td>
                        <td class="col-md-1">$<span id="total">'.$records['amount'].'</span></td>
                        <td class="col-md-1">'.$paymentmethod.'</td>
                        <td class="col-md-3 text-center">';
                        if($records['status']=='Active'){$output .=  '<a href="/modules/supplier/cancel-profile.php?token='.$records['token'].'&profileid='.$records['profileid'].'&amount='.$records['amount'].'&on='.$invoicenumber.'" onclick="return confirm(\'are you sure want to Cancel this Subscription\')">Cancel</a>';
	                        }else{$output .=  "Cancelled on ".$records['canceldate'];}
		                        $output .=  '</td>
                        <td class="col-md-2 text-center">'.$records['status'].'</td>
                        <td class="col-md-2 text-center">'.$records['date'].'</td>
                    </tr>';

            $i ++;
        }
        $output .='</tbody></table>';
        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $output .= get_sp_pagination_link($paginationCount, 'order', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center text-danger">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <script type="text/javascript">
        $(".image").colorbox({
            width: '600px',
            height: '500px;'
        });
        $(document).ready(function() {

            $(".fancybox").fancybox();

            var path = '<?php echo HOME_PATH; ?>ajaxFront.php';



        });
    </script>

    <?php
    die;
}

function manage_adsBookingList($uid,$pro_id,$proName) {
    global $db;
    $condition = "WHERE bg.deleted = 0 AND bg.payment_status=1 AND bg.supplier_id=$uid AND bg.pro_id=$pro_id ";
    $query = "SELECT bg.id FROM " . _prefix("ads_masters") . " AS bg "
            . "  LEFT JOIN " . _prefix("services") . " AS service ON service.id=bg.service_category_id "
            . " $condition ";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $query = "SELECT bg.id,bg.status, bg.title,bg.from_date,bg.to_date,bg.approve_status,bg.image,service.name AS cat_title  FROM " . _prefix("ads_masters") . " AS bg "
            . "  LEFT JOIN " . _prefix("services") . " AS service ON service.id=bg.service_category_id "
            . " $condition  ORDER BY bg.id DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);

// prd($data['0']['cat_title']);
    if ($count > 0) {
        $output = '<table class="table table-striped table-hover table-bordered">
                    <thead><tr>
                                 <th class="col-md-1 text-center">S.No</th>
                                 <th class="col-md-2 text-center">Title</th>
                                 <th class="col-md-1 text-center">From Date</th>
                                 <th class="col-md-1 text-center">To Date</th>
                                 <th class="col-md-2 text-center">Facility Name</th>
                                 <th class="col-md-1 text-center">Image</th>
                                 <th class="col-md-1 text-center">Approve Status</th>
                                 <th class="col-md-2 text-center">Action</th>
                   </tr></thead>
                    <tbody>';
        foreach ($data as $key => $record) {
            $status = ($record['status'] == 1) ? 'Active' : 'InActive';
            $approve_status = ($record['approve_status'] == 1) ? 'Approved' : 'Disapproved';
            $active_status = ($record['status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $approved = ($record['approve_status'] == 1) ? ADMIN_IMAGE . 'activate.png' : ADMIN_IMAGE . 'deactivate.png';
            $description = strip_tags($record['description']);
            $description = (strlen($description) > 40) ? substr($description, 0, 40) . '...' : $description;
            $img = '<img src="' . $active_status . '" title="' . $status . '" />';
            $img1 = $approve_status . ' <img src="' . $approved . '" title="' . $approve_status . '" />';
            $ImageList = MAIN_PATH . 'files/ads_image/thumb_' . $record['image'];
            $ImageView = MAIN_PATH . 'files/ads_image/' . $record['image'];

            $output .= '<tr class="row_' . $record['id'] . '" style="margin-top: 10px;">';
            $output .= '<td>' . $i . ' .</td>';
            $output .= '<td>' . $record['title'] . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['from_date'])) . '</td>';
            $output .= '<td>' . date('M d, Y', strtotime($record['to_date'])) . '</td>';
            $output .= '<td>' . $proName . '</td>';
            $fileImage = ADMIN_PATH . 'files/ads_image/thumb_' . $record['image'];
            if (file_exists($fileImage)) {
                $output .=!empty($record['image']) ? '<td align="center"><a  class="fancybox" title="' . $record['image'] . '" href="' . $ImageView . '"><img  title="View Image"  src="' . $ImageList . '"></a></td>' : '';

//$output .=!empty($record['image'])? '<td><a  class="image" href="' . HOME_PATH_URL . 'popup.php?adsImage&id=' . md5($record['id']) . '"><img  title="View image" src="' . MAIN_PATH . '/files/ads_image/thumb_' . $record['image'] . '"></a><br/><a href="' . MAIN_PATH . '/download.php?type=ads_image&file=' . $record['image'] . '">Download<i class="fa fa-download"></i></a></td>' : '';
            } else {
                $output .= '<td><img  title="View image" src="' . HOME_PATH . 'images/notavail.gif"></a></td>';
            }
            $output .=!empty($approve_status) ? '<td>' . $approve_status . '</td>' : '<td>N/A</td>';
            $output .= '<td class="text-center"><span class="status-' . $record['id'] . '" style="margin-right:5px;"><a href="javascript:void(0);" id="ads_masters-' . $record['id'] . '-' . $record['status'] . '" class="status">' . $img . '</a></span>'
//                        . '<span style="margin-right:5px;"><a href=' . HOME_PATH . 'supplier/editAddsBooking?edit&id=' . md5($record['id']) . '><img src="' . ADMIN_IMAGE . 'li_edit.png" title="Edit"></a></span>'
                    . '<span><a href="javascript:void(0);" class="delete" id="del-ads_masters-' . $record['id'] . '"><img src="' . ADMIN_IMAGE . 'li_delete.png" title="Delete" ></a> </span>';
            $output.= '</td>';
            $output .= '</tr>';
            $i ++;
        }

        $pagingCount = $count;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 8;
            $output .= get_sp_pagination_link($paginationCount, 'adsBookingList', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $output = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $output;
    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo HOME_PATH; ?>css/pagination.css" media="screen" />
    <script>
        $(document).ready(function() {
            $('.load').click(function() {
                var value = $(this).attr('id').split('-');
                $('#loading-' + value[1]).show();
            });

            $(".fancybox").fancybox();

            var path = '<?php echo HOME_PATH; ?>ajaxFront.php';

            $('.status').live('click', function() {
                var details = $(this).attr('id').split('-');
                var table = details[0];
                var id = details[1];
                var status = details[2];
                //return false;
                $.ajax({
                    type: 'POST',
                    url: path,
                    data: {action: 'status', id: id, table: table, status: status},
                    success: function(response) {
                        $('.status-' + id).html($.trim(response));
                    }
                });
            });
            /*
             * Code for Delete
             */

            $('.delete').live('click', function() {
                var sure = confirm('Are you sure, you want to delete?');
                if (sure) {
                    var details = $(this).attr('id').split('-');
                    var table = details[1];
                    var id = details[2];
                    $.ajax({
                        type: 'POST',
                        url: path,
                        data: {action: 'delete', id: id, table: table},
                        success: function() {
                            $('.row_' + id).hide();
                        }
                    });
                }
            });
        });

    </script>
    <?php
    die;
}

function viewReview($pid) {
    global $db;
    $query = " SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " WHERE md5(fdbk.product_id) ='" . $pid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC";
    $resArt = $db->sql_query($query);
    $count1 = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getReviews = " SELECT fdbk.*, IFNULL(fdbk.title,'N/A') as title , pds.title as pdtitle , pds.id AS pImage, cs.user_name AS csUsername, pds.quick_url FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . " WHERE md5(fdbk.product_id) ='" . $pid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $data = $db->sql_fetchrowset($resReviews);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSum = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';
    $qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
    $resultKnowMe = $db->sql_query($qryKnowMe);
    $knowMeData = $db->sql_fetchrowset($resultKnowMe);
    foreach ($knowMeData as $key => $record) {
        $knowMe[$record['id']] = $record['title'];
    }
    $qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
    $resultDuration = $db->sql_query($qryDuration);
    $durationVisitData = $db->sql_fetchrowset($resultDuration);
    foreach ($durationVisitData as $key => $record) {
        $durationVisit[$record['id']] = $record['title'];
    }
    $lastVisit = array('','Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
    $recommended = array('No', 'Yes', 'Not sure');
    
    
    
       
    
    if ($count > 0) {
        foreach ($data as $key => $record) {
            if ($record['feedback'] != '') {
                if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) {
                    $image = '<a href="'. HOME_PATH .$record['quick_url'].'"><img src="' . HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']) . '" alt="product image" /></a>';
                } else {
                    $image = '<a href="'. HOME_PATH .$record['quick_url'].'"><img src="' . HOME_PATH . 'images/no_image_available.png" alt="" /></a>';
                }
                $TotalRating = $record['overall_rating'];
                $Rating = $TotalRating;
                $overallRating = round($Rating);
                switch ($overallRating) {

                    case 1:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 2:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 3:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 4:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                        break;
                    case 5:
                        $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                        break;
                    default:
                        $all_Rating = 'Not Rated';
                        break;
                }
                $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
                //  $supplierName = (empty($record['spFName'])) ? 'N/A' : $record['spFName'] . '&nbsp;' . $record['spLName'];
                $title = (empty($record['title'])) ? 'N/A' : $record['title'];
                $feedback = (strlen($record['feedback']) > 20) ? substr($record['feedback'], 0, 20) . '...' : $record['feedback'];
                $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
                $reviews .= '<div class="col-md-12"><div class="col-md-2 col-sm-2 col-xs-12 pdding_none " style="margin-bottom:2%;">' . $image . '</div>
                    <div class="col-md-10 col-sm-10 col-xs-12 pdding_right padding_left_col">
                        <h3 class="hedding_three">' . $record['title'] . '</h3>
                        <p style="margin:0" itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">' . $all_Rating . '<span class="text-hide" content='.$overallRating.' data-rating='.$overallRating.' itemprop="ratingValue">'.$overallRating.' stars</span></p>
                            <p><span style="color:rgb(138, 138, 138);
                            "  >Product/Service Name:&nbsp;<a href="'. HOME_PATH .$record['quick_url'].'">' . substr($record['pdtitle'], 0, 60) . '</a></span></br>
                                <!--span style="color:rgb(138, 138, 138);
                            " >Supplier:&nbsp;' . $supplierName . '</span><br/-->
                            <span style="color:rgb(138, 138, 138);
                            "  >Commented By:&nbsp;<span itemprop="author">' . $commenter . '</span></span>';
                            
               if ($record['feedback_reply']){ $reviews .= '<br><span style="color:rgb(138, 138, 138);
                            "  ><i>Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</i></span>';  }          
                          $reviews .= '</p>';  
                if ($record['abused_request'] == 0 || $record['abused'] == 1) {
                    $reviews .= '<p><span class="margin_bottom feedback"  itemprop="reviewBody">' . ((empty($record['pros'])) ? 'N/A' : $pros) . '</span><br><a href="javascript:void(0)" class="read_more">Read more</a></p>'
                            . '<div style="display: none;" class="reads_more col-md-12 padding_none"><div class="css_apply row" style="border-top:1px solid #ddd; border-bottom:1px solid #ddd; margin:22px 0px;">';
//                    if (isset($record['feedback'])) {
//
//                        $reviews.= $record['feedback'] . '<br/>';
//                    } else {
//                        $reviews.= 'No record found <br/>';
//                    }

                    $reviews.='     <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Pros</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Cons</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . $record['feedback'] . '</div> 
                                    </div> '; 
                                    
            if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                $reviews .='<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-top:22px;color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a>
                            </div>';
            } else if ($record['abused'] == 1) {
                $reviews .='<div style="margin-bottom:0;margin-top:22px;" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg text-center"><span style="color:#f15922;font-style: italic;">Moderated by Admin</span></div>';
            } else {
                $reviews .='<p style="margin-top:22px;color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
            }
                                       
 if ($record['feedback_reply']){
	 $record['feedback_reply']=str_ireplace("\n", '<br>', $record['feedback_reply']);
	 $reviews.=' <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left alert alert-info" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px;font-style: italic;">Reply from  ' . substr($record['pdtitle'], 0, 60) . '</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><i>' . $record['feedback_reply'] . '</i></div> 
                                    </div> ';}
                                        
$reviews.='                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
                                        <table class="table table-bordered table_bord">                   
                                            <thead>
                                                <tr>
                                                    <th width="" class="th_text">Categories</th>
                                                    <th width="" class="th_text">Rating</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <tr>
                                                    <td>Quality of Care</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['quality_of_care']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Caring/Helpful Staff</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['caring_staff']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Responsive Management</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['responsive_management']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>Trips/Outdoor Activities</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['trips_outdoor_activities']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Indoor Entertainment</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['indoor_entertainment']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>Social Atmosphere</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['social_atmosphere']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enjoyable Food</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['enjoyable_food']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                        <td>OVERALL RATING</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['overall_rating']) . '</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">How do you know about this place?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) . '</div> 
                                    </div> 

                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I have lived at / visited this facility for</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I last lived at / visited this facility</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . (($record['last_visit']=='') ? 'N/A' : $lastVisit[$record['last_visit']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">Would you recommend this facility to a friend?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . (($record['recommended'] =='') ? 'N/A' : $recommended[$record['recommended']]) . '</div> 
                                    </div>                    
                                </div>';
                    $reviews.= '</div><div class="col-md-6"></div></div>';
                }

            if ($record['abused'] == 1) {
                $reviews .='<p style="color:#f15922;font-style: italic;">Moderated by Admin</p>';
            } else if($record['abused_request'] == 1){
                $reviews .='<p style="color:#f15922;font-style: italic;" >This comment has been flagged for moderation</p>';
            }
                                  

                $reviews .='</div>
                    </div>';
            }
        }
        $pagingCount = $count1;
        if ($pagingCount > 0) {
            $data = '';
            $paginationCount = getPagination($pagingCount);
            $colspan = 6;
            $reviews .= get_sp_pagination_link($paginationCount, 'viewReviews', $data, $colspan, $_POST['pageId']);
        }
    } else {
        $reviews = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $reviews;
    ?>
    <script>
        $(".fancybox").fancybox();
        $('.reads_more').hide();
        $('.read_more').click(function() {
            $(this).siblings('.feedback').toggle();
            //alert($(this).text());
            $(this).text() == 'Hide' ? $(this).text('Read more') : $(this).text('Hide');
            //$(".feedback").html("")
            // $(".css_apply").css({'background-color': '#e5eecc', 'border': 'solid 1px #c3c3c3', 'padding': '10px', 'width': '50%'});
            $(this).parent().next('div.reads_more').slideToggle("slow", function() {
                //$(this).text('Read More');
            });
        });
    </script>
    <?php
    die;
}

function viewFeedback($pid) {
    global $db;
    $query = " SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " WHERE md5(fdbk.product_id) ='" . $pid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC";
    $resArt = $db->sql_query($query);
    $count1 = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
    $getReviews = " SELECT fdbk.*, IFNULL(fdbk.title,'N/A') as title , pds.title as pdtitle , pds.image AS pImage, cs.user_name AS csUsername FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . " WHERE md5(fdbk.product_id) ='" . $pid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO;
    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $data = $db->sql_fetchrowset($resReviews);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSum = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';
    $qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
    $resultKnowMe = $db->sql_query($qryKnowMe);
    $knowMeData = $db->sql_fetchrowset($resultKnowMe);
    foreach ($knowMeData as $key => $record) {
        $knowMe[$record['id']] = $record['title'];
    }
    $qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
    $resultDuration = $db->sql_query($qryDuration);
    $durationVisitData = $db->sql_fetchrowset($resultDuration);
    foreach ($durationVisitData as $key => $record) {
        $durationVisit[$record['id']] = $record['title'];
    }
    $lastVisit = array('','Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
    $recommended = array('No', 'Yes', 'Not sure');
    if ($count > 0) {
        foreach ($data as $key => $record) {
            if ($record['feedback'] != '') {

                $reviews .= '<div class="load col-md-12 col-sm-12 col-xs-12 thumbnail" id="'.md5($record['id']).'"><div col-md-12 col-sm-12 col-xs-12 style="padding:20px;">
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Pros</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Cons</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . $record['feedback'] . '</div> 
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
                                        <table class="table table-bordered table_bord">                   
                                            <thead>
                                                <tr>
                                                    <th width="" class="text-left">Categories</th>
                                                    <th width="" class="text-left">Rating</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <tr>
                                                    <td>Quality of Care</td>
                                                    <td>' . getStars($record['quality_of_care']) . '</td>
                                                </tr>
                                                <tr>
                                                    <td>Caring/Helpful Staff</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['caring_staff']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Responsive Management</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['responsive_management']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Trips/Outdoor Activities</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['trips_outdoor_activities']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Indoor Entertainment</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['indoor_entertainment']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Social Atmosphere</td>
                                                    <td class"text-center"> 
                                                        <div >' . getStars($record['social_atmosphere']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enjoyable Food</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['enjoyable_food']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>OVERALL RATING</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['overall_rating']) . '</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">How do you know about this place?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) . '</div> 
                                    </div> 

                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I have lived at / visited this facility for</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I last lived at / visited this facility</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['last_visit'])) ? 'N/A' : $lastVisit[$record['last_visit']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">Would you recommend this facility to a friend?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['recommended'])) ? 'N/A' : $recommended[$record['recommended']]) . '</div> 
                                    </div>                    
                                </div></div>';
                //$reviews.= '</div><div class="col-md-6"></div></div></div>';
            }
//                if ($record['abused'] == 0 && $record['abused_request'] == 0) {
//                    if (isset($_SESSION['csId']) || isset($_SESSION['userId'])) {
//                        $path_url = HOME_PATH . 'modules/review/report_reason.php?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
//                        $reviews .='<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
//            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-bottom:0;
//                            margin-top:22px;
//                            color:#f15922;" class="fancybox" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '" class=" abuse">Report this comment</a>
//                            </div>';
//                    } else {
////                        $msg = "First you login then Report a comment";
////                        $_SESSION['msg'] = $msg;
//                        $path_url = HOME_PATH . 'login?return=true&msgR='.base64_encode('First you login then Report a comment');
//                        $reviews .='<div class = "col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
//                            <span class = "abuse_div abuse-' . $record['id'] . '"><a style = "margin-bottom:0;margin-top:22px;color:#f15922;" href = "' . $path_url . '" id = "feedbacks-' . $record['id'] . '-' . $record['abused'] . '" class = " abuse">Report this comment</a></span>
//                            </div>';
//                    }
//                } else if ($record['abused'] == 1) {
//                    $reviews .='<div style="margin-bottom:0;margin-top:22px;" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg  "><span style="color:#f15922;font-style: italic;">Moderated by Admin</span></div>';
//                } else {
//                    $reviews .='<p style="margin-bottom:0;margin-top:22px;color:#f15922;font-style: italic;">This comment has been flagged for moderation</p>';
//                }
            $reviews .='</div>
                    </div>';
        }
    } else {
        $reviews = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $reviews;
    ?>
    <script>
        $(function() {
            $("div.load").lazyload({
                event: "sporty"
            });
        });

        $(window).bind("load", function() {
            var timeout = setTimeout(function() {
                $("div.load").trigger("sporty")
            }, 5000);
        });

    </script>
    <?php
    die;
}

function viewFeedbackdDetail($fid) {
    global $db;
    $query = " SELECT fdbk.* , pds.title  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " WHERE md5(fdbk.id) ='" . $fid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC";
    $resArt = $db->sql_query($query);
    $count1 = $db->sql_numrows($resArt);
    $i = 1;
    if (isset($_POST['pageId']) && !empty($_POST['pageId'])) {
        $id = $_POST['pageId'];
        $i = $i + PAGE_PER_NO * $id;
    } else {
        $id = '0';
        $i = 1;
    }
    $pageLimit = PAGE_PER_NO * $id;
  /*  $getReviews = " SELECT fdbk.*, IFNULL(fdbk.title,'N/A') as title , pds.title as pdtitle , pds.image AS pImage, cs.user_name AS csUsername FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . " WHERE md5(fdbk.id) ='" . $fid . "' AND fdbk.status = 1 AND fdbk.deleted = 0 ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO; */

	$getReviews = " SELECT fdbk.*, IFNULL(fdbk.title,'N/A') as title , pds.title as pdtitle , pds.image AS pImage, cs.user_name AS csUsername FROM " . _prefix("feedbacks") . " AS fdbk   "
            . " LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . " LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . " WHERE md5(fdbk.id) ='" . $fid . "' AND fdbk.deleted = 0 ORDER BY fdbk.created DESC limit " . $pageLimit . ',' . PAGE_PER_NO;

    $resReviews = $db->sql_query($getReviews);
    $count = $db->sql_numrows($resReviews);
    $data = $db->sql_fetchrowset($resReviews);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSum = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';
    $qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
    $resultKnowMe = $db->sql_query($qryKnowMe);
    $knowMeData = $db->sql_fetchrowset($resultKnowMe);
    foreach ($knowMeData as $key => $record) {
        $knowMe[$record['id']] = $record['title'];
    }
    $qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
    $resultDuration = $db->sql_query($qryDuration);
    $durationVisitData = $db->sql_fetchrowset($resultDuration);
    foreach ($durationVisitData as $key => $record) {
        $durationVisit[$record['id']] = $record['title'];
    }
    $lastVisit = array('','Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
    $recommended = array('No', 'Yes', 'Not sure');
    if ($count > 0) {
        foreach ($data as $key => $record) {
            if ($record['feedback'] != '') {
                //Mark as read that is set read_status field of feedback table to 1
                if ($record['read_status'] == 0) {
                    $UpdateSql = "UPDATE " . _prefix("feedbacks") . " SET read_status =1 WHERE md5(id)='" . $fid . "' ";
                    $updateResult = $db->sql_query($UpdateSql);
                }

                $reviews .= '<div class="load col-md-12 col-sm-12 col-xs-12 thumbnail"><div col-md-12 col-sm-12 col-xs-12 style="padding:20px;">';
                
            if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                $reviews .='<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-top:0px;color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a>
                            </div>';
            } else if ($record['abused'] == 1) {
                $reviews .='<div style="margin-bottom:0;margin-top:0px;" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg text-center"><span style="color:#f15922;font-style: italic;">Moderated by Admin</span></div>';
            } else {
                $reviews .='<p style="margin-top:0px;color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
            }
                
                
                
                
                
                  $reviews .= '     <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Pros</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-2 col-sm-2 col-xs-12 padding_none"><strong style="font-size:15px;">Cons</strong></div> 
                                        <div class="col-md-10 col-sm-10 col-xs-12 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions to Management or General Comment</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . $record['feedback'] . '</div> 
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
                                        <table class="table table-bordered table_bord">                   
                                            <thead>
                                                <tr>
                                                    <th width="" class="text-left">Categories</th>
                                                    <th width="" class="text-left">Rating</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <tr>
                                                    <td>Quality of Care</td>
                                                    <td>' . getStars($record['quality_of_care']) . '</td>
                                                </tr>
                                                <tr>
                                                    <td>Caring/Helpful Staff</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['caring_staff']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Responsive Management</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['responsive_management']) . '</div>
                                                    </td>
                                                 </tr>
                                                <tr>
                                                   <td>Trips/Outdoor Activities</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['trips_outdoor_activities']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Indoor Entertainment</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['indoor_entertainment']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Social Atmosphere</td>
                                                    <td class"text-center"> 
                                                        <div >' . getStars($record['social_atmosphere']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enjoyable Food</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['enjoyable_food']) . '</div>
                                                    </td>
                                                 </tr>
                                                <tr>
                                                   <td>OVERALL RATING</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['overall_rating']) . '</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">How do you know about this place?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) . '</div> 
                                    </div> 

                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I have lived at / visited this facility for</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">I last lived at / visited this facility</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['last_visit'])) ? 'N/A' : $lastVisit[$record['last_visit']]) . '</div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><strong style="font-size:15px">Would you recommend this facility to a friend?</strong></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none">' . ((empty($record['recommended'])) ? 'N/A' : $recommended[$record['recommended']]) . '</div> 
                                    </div>                    
                                </div>';
                //$reviews.= '</div><div class="col-md-6"></div></div></div>';
            }

            if ($record['abused'] == 0 && $record['abused_request'] == 0) {
                $path_url = HOME_PATH . 'reportabuse?cust_id=' . $record['id'] . '&abuse=' . $record['abused'];
                $reviews .='<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg padding_left">
            <span class="abuse_div abuse-' . $record['id'] . '"><a style="margin-top:22px;color:#f15922;" href="' . $path_url . '"  id="feedbacks-' . $record['id'] . '-' . $record['abused'] . '">Report this comment</a>
                            </div>';
            } else if ($record['abused'] == 1) {
                $reviews .='<div style="margin-bottom:0;margin-top:22px;" class="col-lg-4 col-md-12 col-sm-12 col-xs-12 col_lg_bg text-center"><span style="color:#f15922;font-style: italic;">Moderated by Admin</span></div>';
            } else {
                $reviews .='<p style="margin-top:22px;color:#f15922;font-style: italic;" class="text-center">This comment has been flagged for moderation</p>';
            }
            $reviews .='</div></div>
                    </div>';
        }
    } else {
        $reviews = '<table class="table table-bordered table_bord"><tbody><tr><td class="text-center">No record Found</td></tr></tbody></table>';
    }
    echo $reviews;
    ?>
    <script>
        $("a.fancybox").fancybox();
        $(function() {
            $("div.load").lazyload({
                event: "sporty"
            });
        });

        $(window).bind("load", function() {
            var timeout = setTimeout(function() {
                $("div.load").trigger("sporty")
            }, 5000);
        });
		
		
    </script>

    <?php
    die;
}
