<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
    label.error{
        color:red;
        font-size: 10px;
    }
    .redCol{
        color:red;
    }
    label{
        margin-left: 50px;
    }
    #group{
        border: 1px solid green;
    }

</style>
<?php
global $db;
if (isset($submit) && $submit == 'submit') {
//    $fields_array = gallery();
//    prd($fields_array);
//    $response = Validation::_initialize($fields_array, $_POST);
//    if (isset($response['valid']) && $response['valid'] > 0) {
    $usersid = $_SESSION['id'];
    if (isset($_FILES['file']['name'])) {
        if ($_FILES['file']['error'] == 0) {
            $ext = end(explode('.', $_FILES['file']['name']));
            if ($file_type) {
                $target_pathL = ADMIN_PATH . 'files/gallery/videos/';
            } else {
                $target_pathL = ADMIN_PATH . 'files/gallery/images/';
            }
            $date = new DateTime();
            $timestamp = $date->getTimestamp();
            $logoName = $timestamp . '_' . imgNameSanitize(basename($_FILES['file']['name']), 20);
            $target_path = $target_pathL . $logoName;
            move_uploaded_file($_FILES['file']["tmp_name"], $target_path);
            if ($file_type == 0) {
                $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                ak_img_resize($target_path, $target_path_thumb, 30, 30, $ext);
            }
            $_POST['file'] = $logoName;
        }
    }

    $getMax = "SELECT max(order_by) as count from " . _prefix("galleries") . " WHERE status = 1 && deleted = 0";
    $resMax = $db->sql_query($getMax);
    $dataMax = mysqli_fetch_assoc($resMax);
    $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
    $fields = array(
        'user_id' => $usersid,
        'category_id' => '1',
        'title' => trim($title),
        'type' => trim($file_type),
        'file' => $_POST["file"]
    );
    $insert_result = $db->insert(_prefix('galleries'), $fields);
    if ($insert_result) {
        // Message for insert
        $msg = common_message(1, constant('INSERT'));
        $_SESSION['msg'] = $msg;
        //ends here
        redirect_to(HOME_PATH . "supplier/gallery-manager");
    }
} else {
    $errors = '';
    foreach ($response as $key => $message) {
        $error = true;
        $errors .= $message . "<br>";
    }
}
//}


if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("galleries") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {

            $title = $record['title'];
            $file_type = $record['type'];
            $file = $record['file'];
        }
    }
}
if (isset($update) && $update == 'update') {

    if (isset($_FILES['file']['name'])) {

        if ($_FILES['file']['error'] == 0) {
            $ext = end(explode('.', $_FILES['file']['name']));
            if ($_POST["file_type"]) {
                $target_pathL = ADMIN_PATH . 'files/gallery/videos/';
            } else {
                $target_pathL = ADMIN_PATH . 'files/gallery/images/';
            }
            $date = new DateTime();
            $modified_on = $date->format('Y-m-d H:i:s');
            $timestamp = $date->getTimestamp();
            $logoName = $timestamp . '_' . imgNameSanitize(basename($_FILES['file']['name']), 20);
            $target_path = $target_pathL . $logoName;
            $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
            move_uploaded_file($_FILES['file']["tmp_name"], $target_path);
            if ($_POST["file_type"] == 0) {
                $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                ak_img_resize($target_path, $target_path_thumb, 30, 30, $ext);
            }
            list($w_orig, $h_orig) = getimagesize($target_path);
            if ($w_orig > 1280 || $h_orig > 720) {
                ak_img_resize($target_path, $target_path, 1280, 720, $ext);
            }
            $_POST['file'] = $logoName;
        }
    }
    $title = $_POST["title"];
    $type = $_POST["file_type"];
    $file = $_POST["file"];
    $fields = array('title' => trim($title),
        'type' => trim($type),
        'modified' => $modified_on,
    );
    if ($_POST['file'] != '') {
        $fields['file'] = $file;
    }

    $where = "where md5(id)= '$id'";
    $update_result = $db->update(_prefix('galleries'), $fields, $where);
    if ($update_result) {
        // Message for insert
        $msg = common_message(1, constant('UPDATE'));
        $_SESSION['msg'] = $msg;
        //ends here
        redirect_to(HOME_PATH . "supplier/gallery-manager");
    }
} else {
    $errors = '';
    foreach ($response as $key => $message) {
        $error = true;
        $errors .= $message . "<br>";
    }
}
?>

<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<div class="container" style="width:100%; border: 1px solid rgb(204, 204, 204);">
    <?php require_once 'includes/sp_left_navigation.php'; ?>
    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12" id="group">
        <div class="row" style="margin-bottom: 20px;" >
            <div class="col-sm-8 text-center" style="color: #F25822;">
                <h2 class=""> <i class="fa fa-picture-o"></i>&nbsp;<?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Image/Video in Gallery </h2>
            </div>
            <div class="col-sm-4 text-left">
                <h2 style="float:right;color:red" class="redCol small">* fields required</h2>
            </div>
            <div class="clearfix"></div>
        </div>
        <p class="clearfix"></p>
        <form name="addLogo" id="addLogos" action="" method="POST" class=" thumbnail form-horizontal" role="form" enctype="multipart/form-data" style="padding-top:20px;">
            <div class="form-group">
                <label for="title" class="col-sm-2  text-left">Title&nbsp;<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="title" id="title" maxlength="50" value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="plan_type" class="col-sm-2 ">File Type&nbsp;<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    <?php $select_type = array("0" => "Image", "1" => "Video"); ?>
                    <select name="file_type" id='file_type' class=" required form-control" style="width:235px;">
                        <?php
                        $file_type = $file_type != '' ? $file_type : $_POST['type'];
                        echo options($select_type, $file_type);
                        ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="image" class="col-sm-2 ">File</label>
                <div class="col-sm-8">
                    <input type="file" name="file" >
                </div>
            </div>
            <div class="form-group">
                <!--<label for="image" class="col-sm-2 control-label">Logo image<span class="redCol">* </span></label>-->
                <div class="col-sm-8 text-center">
                    <?php if (file_exists(ADMIN_PATH . 'files/gallery/images/' . $file) && $file != '' && key($_REQUEST) == 'edit') { ?>
                        <img width="100px;" height="100px;" title="feature" src="<?php echo HOME_PATH . 'admin/files/gallery/images/' . $file ?>"/>
                    <?php } ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();

//        $('#addLogos').validate({
//            rules: {
//                title: {
//                    required: true,
//                },
//                file_type: {
//                    required: true,
//                },
//            },
//            messages: {
//                title: {
//                    required: "Enter title of the Image/Video"
//                },
//                file_type: {
//                    required: "Select a file type"
//                }
//
//            }
//
//        });
//        $('input[type="submit"]').click(function() {
//            alert($("#file_type").val());
//            if ($("#file_type").val()) {
//                $('#addLogos').validate({
//                    rules: {
//                        'file': {
//                            required: true,
//                            accept: 'ogg|ogv|avi|mpe?g|mov|wmv|flv|mp4'
//                        }
//                    },
//                    messages: {
//                        file: {
//                            required: "Select Video",
//                            accept: "Only video type ogg|ogv|avi|mpe?g|mov|wmv|flv|mp4 is allowed"
//                        }
//
//                    }
//
//                });
//            } else {
//                $('#addLogo').validate({
//                    rules: {
//                        'file': {
//                            required: true,
//                            accept: "jpg,png,jpeg,gif"
//                        }
//                    },
//                    messages: {
//                        file: {
//                            required: "Select Image",
//                            accept: "Only image type jpg/png/jpeg/gif is allowed"
//                        }
//
//                    }
//
//                });
//            }
//        });
    });
    $('#addLogos').validate();
<?php if (file_exists(DOCUMENT_PATH . 'admin/files/company_logo/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
        $('#image').removeClass('required');
<?php } ?>
</script>
