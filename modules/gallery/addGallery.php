<?php
//error_reporting(E_ALL); 

function resizeImage($target_pathL,$type='0') {
  //  echo "HERE = $target_pathL";
    $ext = end(explode('.', $target_pathL));
    $target_path = $target_pathL;
        $image = imagecreatefromstring(file_get_contents($target_path));
    $exif = exif_read_data($target_path);
    if (!empty($exif['Orientation'])) {
        switch ($exif['Orientation']) {
            case 8:
                $image = imagerotate($image, 90, 0);
                break;
            case 3:
                $image = imagerotate($image, 180, 0);
                break;
            case 6:
                $image = imagerotate($image, -90, 0);
                break;
        }
        imagejpeg($image, $target_path);
    }
    if ($type == 0) {
        
      list($save_strwidth, $save_strheight) = getimagesize($target_path);
      
      
     // echo "Size =$save_strwidth x $save_strheight";
      // Make the largest Image size for zoom
      
      $maxsquareheight=720;
      $maxsquarewidth=1280;
      $strwidth=$save_strwidth;
      $strheight=$save_strheight;  
      if ($strheight>$maxsquareheight){
      $strfactor = $maxsquareheight/$strheight;
      $strheight = round($strheight*$strfactor);
      $strwidth = round($strwidth*$strfactor);
       
      }
      
      if ($strwidth>$maxsquarewidth){
      $strfactor = $maxsquarewidth/$strwidth;
      $strheight = round($strheight*$strfactor);
      $strwidth = round($strwidth*$strfactor);
      
      }
      
      // Make the largest Image size for zoom
       ak_img_resize($target_path, $target_path, $strwidth, $strheight, $ext);

      // Make Thumb  
       $maxsquareheight=166;
       $maxsquarewidth=264;
     
       $target_path_thumb = str_ireplace('/images/', '/images/thumb_', $target_pathL);
       $target_pathL . 'thumb_' . $logoName;
        
       ak_img_thumb($target_path, $target_path_thumb, $maxsquarewidth,$maxsquareheight, $ext);
        

         // Make image for facility slider  5/2/16
	   
       $maxsquareheight=400;
       $maxsquarewidth=750;
     
       $target_path_slider = $target_pathL;
        
       ak_img_thumb($target_path, $target_path_slider, $maxsquarewidth,$maxsquareheight, $ext);
      
		// Make Thumb   for Feedback
       $maxsquareheight=85;
       $maxsquarewidth=103;
 
       $target_path_thumbR = str_ireplace('/images/', '/images/thumb_feedback_', $target_pathL);
        ak_img_thumb($target_path, $target_path_thumbR, $maxsquarewidth,$maxsquareheight, $ext);
    }

}


 function make_url_clean($url){
            $url=trim(str_replace(','," ",$url));
        $url=trim(str_replace('/'," ",$url));
        
        $url=trim(str_replace("&reg;","",$url));
        $url=trim(str_replace("&amp;"," ",$url));
        
        $url = preg_replace("/\<.*?\>/", "",$url);
        $url = preg_replace('/[^a-z 0-9*]/i', '', $url);
        $url=trim(str_replace("  "," ",$url));
        $url=trim(str_replace("  "," ",$url));
        $url=trim(str_replace("  "," ",$url));
        // Finished clean of funny characters
        $url=str_replace(" ","-",$url);
        //echo "$url<br>";
 return $url;
}


    
function make_image_quick_url($target_pathL,$ext){
    global $db;
    $pro_id = cleanthishere($_SESSION['proId']);
    $sql_queryL = "SELECT id,title,certification_service_type,address_suburb,address_city FROM " . _prefix("products") . " WHERE id = '$pro_id' ";
    $resL = $db->sql_query($sql_queryL);
    $missing = $db->sql_fetchrow($resL);
        
        $id = $missing['id'];
        $title = make_url_clean(ucwords(strtolower($missing['title'])));
        $address_suburb = make_url_clean($missing['address_suburb']);
        $address_city = make_url_clean($missing['address_city']);
        $certification_service_type =make_url_clean(strtolower($missing['certification_service_type']));
        $url=$certification_service_type.'-'.$title.'-'.$address_suburb.'-'.$address_city;
        $folder = $target_pathL;

/// If it does not exist we can use it
          //***********Find them a valid quickurl or add number to the end of the chosen name
          $found = FALSE;
          $count = "";
          while ( ! $found ) {
              if (is_file ($folder.$url.$count.'.'.$ext)) {
                  //echo $folder.$url.$count.'.'.$ext."<br>";
                // Nope someone else using this uname
                $found = FALSE;
                $count = $count + 1;
              } else {
                $found = TRUE;
                $url = $url.$count.'.'.$ext;
              }
          //***********Found them a valid quick_url
          } // end while
   
    return $url;
    
}// end function
    
  
    
global $db;
$pro_id = $_SESSION['proId'];
$pro_name = $_SESSION['proName'];
if (isset($submit) && $submit == 'submit') {
    $usersid = $_SESSION['id'];
    if (isset($_FILES['file']['name'])) {
        if ($_FILES['file']['error'] == 0) {
            $ImageExt = array("jpg", "png", "jpeg", "gif");
            $VideoExt = array("ogg", "ogv", "avi", "mpe?g", "mov", "wmv", "flv", "mp4");
            $ext = strtolower(end(explode('.', $_FILES['file']['name'])));
            if (in_array($ext, $VideoExt)) {
                if (($_SESSION['videoEnable'] == 1 && 1==2)) {
                    $target_pathL = ADMIN_PATH . 'files/gallery/videos/';
                    $type = 1;
                    uploadImageVideo($target_pathL,$type);
                } else {
                    $NotPermittedMsg = "You are not permitted to upload video.";
                    $msg = common_message_supplier(0, $NotPermittedMsg);
                    $_SESSION['msg'] = $msg;
                    redirect_to(HOME_PATH . "supplier/gallery-manager");
                }
            } else if (in_array($ext, $ImageExt)) {
                $target_pathL = ADMIN_PATH . 'files/gallery/images/';
                $type = 0;
                uploadImageVideo($target_pathL,$type);
            } else {
                $_SESSION['msg'] = "Please select a valid type of Image/Video";
            }
        }
    }
    if (!empty($youtube_url)) {
        $_POST['file'] = $youtube_url;
        $type = 2;
    }
    $getMax = "SELECT max(order_by) as count from " . _prefix("galleries") . " WHERE status = 1 && deleted = 0";
    $resMax = $db->sql_query($getMax);
    $dataMax = mysqli_fetch_assoc($resMax);
    $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
    $fields = array(
        'user_id' => $usersid,
        'category_id' => '1',
        'title' => trim($title),
        'type' => trim($type),
        'file' => $_POST["file"],
        'comment' => $comment,
        'pro_id' => $pro_id
    );
    $insert_result = $db->insert(_prefix('galleries'), $fields);
    if ($insert_result) {
        // Message for insert
        $msg = common_message_supplier(1, constant('GALLARY_INPUT'));
        $_SESSION['msg'] = $msg;
        //ends here
        redirect_to(HOME_PATH . "supplier/gallery-manager");
    }
} else {
    $errors = '';
    foreach ($response as $key => $message) {
        $error = true;
        $errors .= $message . "<br>";
    }
}

function uploadImageVideo($target_pathL,$type='0') {
    
    $ext = end(explode('.', $_FILES['file']['name']));
    $logoName = make_image_quick_url($target_pathL,$ext);
    $target_path = $target_pathL . $logoName;
    move_uploaded_file($_FILES['file']["tmp_name"], $target_path);
    $image = imagecreatefromstring(file_get_contents($target_path));
    $exif = exif_read_data($target_path);
    if (!empty($exif['Orientation'])) {
        switch ($exif['Orientation']) {
            case 8:
                $image = imagerotate($image, 90, 0);
                break;
            case 3:
                $image = imagerotate($image, 180, 0);
                break;
            case 6:
                $image = imagerotate($image, -90, 0);
                break;
        }
        imagejpeg($image, $target_path);
    }
    if ($type == 0) {
        
      list($save_strwidth, $save_strheight) = getimagesize($target_path);
      
      // Make the largest Image size for zoom
      
      $maxsquareheight=720;
      $maxsquarewidth=1280;
      $strwidth=$save_strwidth;
      $strheight=$save_strheight;  
      if ($strheight>$maxsquareheight){
      $strfactor = $maxsquareheight/$strheight;
      $strheight = round($strheight*$strfactor);
      $strwidth = round($strwidth*$strfactor);
       
      }
      
      if ($strwidth>$maxsquarewidth){
      $strfactor = $maxsquarewidth/$strwidth;
      $strheight = round($strheight*$strfactor);
      $strwidth = round($strwidth*$strfactor);
      
      }
      
      // Make the largest Image size for zoom
       ak_img_resize($target_path, $target_path, $strwidth, $strheight, $ext);

      // Make Thumb  
       $maxsquareheight=166;
       $maxsquarewidth=264;
     
       $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
        
       ak_img_thumb($target_path, $target_path_thumb, $maxsquarewidth,$maxsquareheight, $ext);
        

         // Make image for facility slider  5/2/16
	   
       $maxsquareheight=400;
       $maxsquarewidth=750;
     
       $target_path_slider = $target_pathL  . $logoName;
        
       ak_img_thumb($target_path, $target_path_slider, $maxsquarewidth,$maxsquareheight, $ext);
      
		// Make Thumb   for Feedback
       $maxsquareheight=85;
       $maxsquarewidth=103;
 
        $target_path_thumbR = $target_pathL . 'thumb_feedback_' . $logoName;
        ak_img_thumb($target_path, $target_path_thumbR, $maxsquarewidth,$maxsquareheight, $ext);
    }
    $_POST['file'] = $logoName;
}
/******code for update image title***/
global $db;

 


if(key($_REQUEST) == 'edit'){
$gallery_img_id=base64_decode($_REQUEST['gallid']);
$gallery_image_title=base64_decode($_REQUEST['gtitle']);
}
if (isset($update) && $update == 'update') {
    $update_title=$_POST['old_title'];
    if(isset($update_title) && !empty($update_title)){
        $gallery_img_title=$update_title;
    }else{
        $gallery_img_title=$_POST['old_title'];
    }
    $query_update="update ad_galleries set `title`='$gallery_img_title' where `id`='$gallery_img_id'";
    $res_update = $db->sql_query($query_update);
    if($res_update){
        
    }else{}



   $fetch_query="select title,file from  ad_galleries where id='$gallery_img_id'";
   $fetch_title = $db->sql_query($fetch_query);
   $fetch_image_title = $db->sql_fetchrow($fetch_title);
   $fetch_newimg_title=$fetch_image_title['title'];
   $image_file=$fetch_image_title['file'];
   $target_pathL = ADMIN_PATH . 'files/gallery/images/'.$image_file;
                $type = 0;
                resizeImage($target_pathL,$type);

   
   
   
   
   if(isset($fetch_newimg_title) && !empty($fetch_newimg_title))
   {
       $img_title_name=$fetch_newimg_title;
   }
   else
   {
     $img_title_name=$_POST['old_title'];
   }
 }
    /******DELETE IMAGE code *****/
 if(key($_REQUEST) == 'delete'){
$gallery_img_id=base64_decode($_REQUEST['gallid']);
$gallery_image_title=base64_decode($_REQUEST['gtitle']);



    $query_update="update ad_galleries set deleted = 1 where `id`='$gallery_img_id'";
    $res_update = $db->sql_query($query_update);
    if($res_update){?>

   <script> alert("image deleted");</script>
<?php  
header('Location: http://www.agedadvisor.nz/supplier/gallery-manager');
    }else{
      
    }
}
?>

<div class="row">
  <div id="success" style="font-family:Neo Sans; margin: 0 auto;width:70%" ></div>
	<div class="dashboard_container">
		<?php require_once 'includes/sp_left_navigation.php'; ?>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="dashboard_right_col">
				<h2 class="hedding_h2"><i class="fa fa-picture-o"></i> <span>Gallery Manager</span></h2>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thumbnail adbooking_container">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<form class="form-horizontal form_payment" id="galleryForm" action="" method="POST" role="form" enctype="multipart/form-data">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
										<?php if (key($_REQUEST) == 'edit') {?>

									   <!-- <div class="form-group" style="margin-bottom:6px;">
											<label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">New Image Title&nbsp;<span class="redCol"></span></label>
											<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
												<input id="name" class="form-control input_fild_1" type="text" name="up_title">
											</div>
										</div>-->
										<!--**************code for Title name of image**-->
										 <div class="form-group" style="margin-bottom:6px;">
											<label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Image Title&nbsp;<span class="redCol"></span></label>
											<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
												<input id="name_title" class="form-control input_fild_1" type="text" name="old_title" value="<?php echo $gallery_image_title ?>" >
											</div>
										</div>
									   <!-- **************end of code for title of image*****--> 
									   

										<div class="form-group" style="margin-bottom:6px;">
											<label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Facility Name&nbsp;<span class="redCol"></span></label>
											<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
												<input id="name" class="form-control input_fild_1 required" value="<?php echo $pro_name ?>" type="text" readonly name="pro_name">
												<input id="name" class="form-control input_fild_1 required" value="<?php echo $pro_id ?>" type="hidden" name="pro_id">
											</div>
										</div>
										 <?php } else { ?>

										<div class="form-group" style="margin-bottom:6px;">
											<label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Image Title&nbsp;<span class="redCol">*</span></label>
											<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
												<input id="name" class="form-control input_fild_1 required" type="text" name="title">
											</div>
										</div>
										<div class="form-group" style="margin-bottom:6px;">
											<label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Facility Name&nbsp;<span class="redCol">*</span></label>
											<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
												<input id="name" class="form-control input_fild_1 required" value="<?php echo $pro_name ?>" type="text" readonly="true" name="pro_name">
												<input id="name" class="form-control input_fild_1 required" value="<?php echo $pro_id ?>" type="hidden" name="pro_id">
											</div>
										</div>
										<div class="form-group" style="margin-bottom:6px;">
											<label for="image_video" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Upload Image<?php if ($_SESSION['videoEnable'] == 1 && 1==2) { ?>/Video<?php } ?>(.jpg)<span class="redCol">*</span></label>
											<div  class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
												<input type="file" name="file" class="form-control_browse input_fild_img_browse" id="image_video" >

												<!--<button type="file" class="btn btn-danger center-block browse_btn">Browse</button>-->
											</div>
										</div>
										 <?php } ?>
										<!--                                            <div class="form-group" style="margin-bottom:6px;">
																						<label for="firstname" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration"></label>
																						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 text-center">
																							<spna class="text-center">OR</span>
																						</div>
																					</div>
										
																					<div class="form-group" style="margin-bottom:6px;">
																						<label for="youtubeId" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Youtube Video Id</label>
																						<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
																							<input id="name" class="form-control input_fild_1" type="text" name="youtube_url" id="youtube_url">
																						</div>
																					</div>-->
										<!--<div class="form-group" style="margin-bottom:6px;">
											<label for="comments" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Add Comments/Keywords</label>
											<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
												<textarea class="form-control input_fild_1" rows="7" name="comment"></textarea>
											</div>
										</div>-->
										<div class="form-group text-center total_bg" style="width:93%; padding:2% 0 0 0;">
											<?php
											if (key($_REQUEST) == 'edit') {
												?>
												<button type="submit" id="submit" name="update" value="update" class="btn btn-danger center-block btn_search btn_pay pull-right" style="margin: 0 0 0 5px;">Update</button><a class="pull-right btn btn-info" href="<?php  echo HOME_PATH ?>supplier/gallery-manager">Go Back</a> 

												<?php
											} else {
												?>
												<button type="submit" id="submit" name="submit" value="submit" class="btn btn-danger center-block btn_search btn_pay pull-right" style="margin: 0 0 0 5px;">SUBMIT</button><a class="pull-right btn btn-info" href="<?php  echo HOME_PATH ?>supplier/gallery-manager">Go Back</a> 
												<?php
											}
											?>
										</div>
									</div>  
								</div>    
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
    label.error{
        color:red;
        font-size: 10px;

    }
    input.error{
        color:#000;

    }
    .redCol{
        color:red;
    }

</style>
<script type="text/javascript">
    $(document).ready(function() {
//        $('#image_video').bind('change', function() {
//            var req_size = 1024 * 1024;
//            var filesize = this.files[0].size;
//            if (filesize > req_size) {
//                alert('Filesize must be of 2 MB or below');
//                return false;
//            }
//            //this.files[0].size gets the size of your file.
//            //alert((this.files[0].size) / 1024 / 1024 + ' MB');
//        });
        $('#galleryForm').validate({
            rules: {
                title: {
                    required: true,
                    minlength: 3,
                    maxlength: 50
                },
                comment: {
                    minlength: 3,
                    maxlength: 500
                },
                file: {
//                    required: function() {
//                        return $('#youtube_url').val() == '';
//
//                    },
                    required: true,
                    accept: 'webm,flv,mp4,jpg,png,jpeg,gif'

                }
//                youtube_url: {
//                    required: function() {
//                        return $('#image_video').val() == '';
//                    },
//                    url: true
//                }
            },
            messages: {
                title: {
                    required: "Enter title of the Image/Video",
                    minlength: "Title must be contain atleast 3 chars",
                    maxlength: "Title should not be contain more than 50 chars"
                },
                comment: {
                    minlength: "Comment must be contain atleast 3 chars",
                    maxlength: "Comment should not be contain more than 500 chars"
                },
                file: {
                    required: "Select a video/Image file",
                    accept: "Either video type webm/flv/mp4 is allowed or image type jpg/png/jpeg/gif is allowed"
                }
//                youtube_url: {
//                    required: "Either select a video/Image file or inter youtube Url",
//                    url: "Inter a valid  youtube Url"
//                }



            },
            submitHandler: function(form) {
                $('#submit').attr('disabled', 'disabled');
                form.submit();

            }
        });
    });

</script>
<script type="text/javascript">
$(document).ready(function(){
    <?php if($res_update) {?>
        $("#success").addClass("successForm");
        $("#success").html("Record has been updated successfully");
        setTimeout(function() {
            $('#success').fadeOut('fast');
             
            }, 3000); 
        updatetitle();
    <?php } ?>
    function updatetitle() {
        var image_title="<?php echo  $img_title_name; ?>";
        var first_image_blank=$("#name_title").val("");
        var first_image_blank=$("#name_title").val();
        $("#name_title").val(image_title);
    }
});
</script>

