<?php
    /*
     * Objective  : user profile is shown here
     * Filename   : profile.php
     * Created By : Yogesh Chandra Joshi <yogesh.joshi@ilmp-tech.com> 
     * Created On : 25 August 2014
     */
?>

<?php
    global $db;
    $id = $_SESSION["userId"];
    $source = array('0' => 'Google Search', '1' => 'Sales Person', '2' => 'Sales Person', '3' => 'Online Ads', '4' => 'Existing clients', '5' => 'Newspaper', '6' => 'Facebook', '7' => 'LinkedIn', '8' => 'Twitter', '9' => 'Other');

    $query = "SELECT * FROM " . _prefix('users') . " WHERE id = '$id'";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $data = $db->sql_fetchrow($res);


    if (isset($update) && $update == 'update') {


        $fields = array(
            'name' => trim($_POST['name']),
            'business' => trim($business),
            'industry' => trim($industry),
            'website' => trim($website),
            'address' => trim($address),
            'country_id' => $country_id,
            'state_id' => $state_id,
            'zipcode' => $zipcode,
            'phone' => $phone,
            'cellphone' => $cellphone,
            'email' => $email
        );

        $where = "where id= '$id'";
        $update_result = $db->update(_prefix('users'), $fields, $where);

        if ($update_result) {
            $_SESSION['name'] = trim($_POST['name']);
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;

            $data = array(
                'name' => trim($_POST['name']),
                'business' => trim($business),
                'industry' => trim($industry),
                'website' => trim($website),
                'address' => trim($address),
                'country_id' => $country_id,
                'state_id' => $state_id,
                'zipcode' => $zipcode,
                'phone' => $phone,
                'cellphone' => $cellphone,
                'email' => $email
            );
        }
    }
?>
<section id="body_container" class="Dashbord_main " >
<ul class="breadcrumb">
 <li><a href="<?php echo HOME_PATH ?>user"><strong>HOME</strong></a></li>
 <li class="last"><a href="javascript:void(0);">Profile</a></li>
 </ul>
    <div class="messag_send"></div>
    <div class="message_container">
    <div class="login_container chang_pass_main">
         <div><?php
                        if (isset($_SESSION['msg'])) {
                            echo $_SESSION['msg'];
                            unset($_SESSION['msg']);
                        }
                    ?></div>
      <form id="userProfile" name="userProfile" method="post" class="login_account request_form"> 
           
            <div class="col">
                <label class="label_text">Name <span class="redCol">* </span></label> 
                <input type="text" name="name" id="name"  class="input_width input_login required" maxlength="50" size="50" value="<?php echo stripslashes($data['name']); ?>" /> 

            </div> 
            <div class="col">
                <label class="label_text">Business <span class="redCol">* </span></label>  
                <input type="text" name="business" id="business" class="input_width input_login required" maxlength="50" size="50" value="<?php echo stripslashes($data['business']) ?>" /> 
            </div>
            <div class="col" >
                <label class="label_text">Industry </label>  
                <input type="text" name="industry" id="industry" class="input_width input_login"  maxlength="50" size="50" value="<?php echo stripslashes($data['industry']) ?>" /> 
            </div>     
            <div class="col">
                <label class="label_text">Website </label> 
                <input type="text" name="website" id="website" class="input_width input_login"  maxlength="50" size="50" value="<?php echo stripslashes($data['website']) ?>" /> 
            </div>

            <div class="col">
                <label class="label_text">Email <span class="redCol">* </span> </label> 
                <input type="text" name="email" id="email"  class="email required input_login input_width" maxlength="50" size="50" value="<?php echo stripslashes($data['email']) ?>" />  
            </div>
            <div class="col">
                <label class="label_text">Address <span class="redCol">* </span> </label>
                <textarea name="address" id="address" cols="60" rows="8" class="required textarea input_login input_width" ><?php echo stripslashes($data['address']) ?></textarea>
            </div>
            <div class="col">
                <label class="label_text">Country <span class="redCol">* </span> </label> 
                <select id="c_country" class="required input_text countryApply input_width" name="country_id"><?php
                        echo countrylist($data['country_id']);
                    ?></select>
            </div>
            <div class="col"> 
                <label class="label_text">State <span class="redCol">* </span></label>
                <select id="c_state" class="required input_text input_width" name="state_id">
                    <?php echo stateList($data['country_id'], $data['state_id']); ?>
                </select> 
            </div>
            <div class="col">
                <label class="label_text">Phone Number <span class="redCol">* </span> </label>  
                <input type="text" name="phone" id="phone" class="required phoneUS input_login input_width" maxlength="50" size="50" value="<?php echo stripslashes($data['phone']); ?>" />
            </div>
            <div class="col">
                <label class="label_text">Cell Phone Number </label>  
                <input type="text" name="cellphone" id="cellphone" class="input_login input_width" maxlength="50" size="50" value="<?php echo stripslashes($data['cellphone']); ?>" />  
            </div>
            <div class="col">
                <label class="label_text">Zip Code <span class="redCol">* </span> </label>  
                <input type="text" name="zipcode" id="zipcode" class="required input_login input_width" maxlength="50" size="50" value="<?php echo stripslashes($data['zipcode']); ?>" />  
            </div>

            <div class="col">
                <input type="submit" value="update" name="update"class="creat_account_btn" >
            </div>
        </form>
    </div>
</div>
</section>

<script type="text/javascript">
    $(document).ready(function() {
        $('#userProfile').validate();
        
        if ($('#c_country').val() == 229) {
            $('input[name="zipcode"]').rules('remove');
            $('input[name="zipcode"]').rules('add', {
                digits: true,
                required: true,
                maxlength: 5
            });
        }
        
        
        $('.countryApply').on('change', function() {
            var country = $(this).val();
            if (country != 229) {
                $('#countryCode').show();
                $('#countryCode').val('');
                $('input[name="zipcode"]').rules('remove');
                $('input[name="zipcode"]').rules('add', {
                    required: true
                });
            } else {
                $('#countryCode').hide();
                $('#countryCode').val('');
                $('input[name="zipcode"]').rules('remove');
                $('input[name="zipcode"]').rules('add', {
                    digits: true,
                    required: true,
                    maxlength: 5
                });
            }
            $.ajax({
                type: 'POST',
                url: '<?php echo HOME_PATH ?>admin/ajax.php',
                data: {action: 'stateList', country: country},
                success: function(response) {
                    $('#c_state').html($.trim(response));

                }
            });
        });
    });
</script>
