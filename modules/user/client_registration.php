<?php
    /*     * ***************************************
     * Add Soil Offer Page 
     * *************************************** */

    global $db;
   $user_type = 1;
    //$records = array();
   
    if (isset($submit) && $submit == 'submit') {
      $fields_array = client_registration();
      $response = Validation::_initialize($fields_array, $_POST);
      
      if(isset($response['valid']) && $response['valid']>0){
          
          $fields = array(
                'user_type' => $user_type,                
                'business' => trim($business),
                'industry' => trim($industry),
                'website' => trim($website),
                'name' => trim($name),
                'refferal_id' => trim($refferal_id),
                'address' => trim($address),
                'country_id' => $country_id,
                'state_id' => $state_id,
                'zipcode' => $zipcode,
                'phone' => $phone,
                'cellphone' => $cellphone,
                'email' => $email,
                'password' => $password,
                'source_id' => $source_id,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'validate' => '0');
          
           $insert_result = $db->insert(_prefix('users'), $fields);
            if ($insert_result) {
                $msg = common_message(1, constant('INSERT'));
                foreach ($_POST as $key => $value) {
                    $$key = '';
                }
            }          
      }
      else{
        $errors =   '';
        foreach($response as $key=>$message){
            $error = true;
            $errors .= $message."<br>";
        }
    }
       
    }
?>  
<form name="AddClient" id="AddClient" action="" method="post">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>
           
           
            <tr>
                <td class="sublinks" height="40" valign="bottom"></td>
            </tr>
         
            <tr>
                <td class="bdrTab">
                    <table style="border: 1px solid rgb(184, 184, 184);" border="0"
                           cellpadding="5" cellspacing="0" width="100%">
                        <tbody>
                            <tr>
                                <td>
                                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                                           width="98%">

                                        <tbody>
                                        
                                            <tr><td class="marginleft" align="right" height="26"><span class="redCol">* </span>Business Name:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="business" id="business" maxlength="50" size="30" class="registrationTextbox" value="<?php echo isset($records['business']) ? stripslashes($records['business']) : ''; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr><td class="marginleft" align="right" height="26">Industry:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="industry" id="industry" maxlength="50" size="30" class="registrationTextbox" value="<?php echo isset($records['industry']) ? stripslashes($records['industry']) : ''; ?>"/>
                                                    </div></td>
                                            </tr>
                                             <tr><td class="marginleft" align="right" height="26">Website:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="website" id="website" maxlength="50" size="30" class="registrationTextbox" value="<?php echo isset($records['website']) ? stripslashes($records['website']) : ''; ?>"/>
                                                    </div></td>
                                            </tr>
                                             <tr><td class="marginleft" align="right" height="50"><span
                                                        class="redCol">* </span>Are you referred by a salesperson:</td>
                                                <td class="marginleft"> <div class="input text">
                                                        <?php    
                                                            $check2 ='checked';
                                                            if(isset($_POST["sp_refral"])){
                                                             
                                                                if($_POST["sp_refral"] ==1){
                                                                   $check1 = "checked";
                                                                   $check2 ="";
                                                                }
                                                                else
                                                                {
                                                                    $check1 = "";
                                                                   $check2 ="checked";
                                                                }
                                                            
                                                        }
                                                            ?>
                                                       <label for="TheRadioSP1">  <input type="radio" id="TheRadioSP1" name="sp_refral" value="1" class="send_type" <?php echo $check1; ?>>Yes</label> <label for="TheRadioSP2"><input type="radio" id="TheRadioSP2" class="send_type" name="sp_refral" value="2" <?php echo $check2; ?>>No</label>
                                                    </div></td>
                                            </tr>
                                            <tr class="sp_id_textbox">
                                                <td class="marginleft" align="right" height="50"><span
                                                        class="redCol">* </span>Enter your SP ID:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="refferal_id" id="" maxlength="50" size="30" class="registrationTextbox" value="<?php echo isset($records['name']) ? stripslashes($records['name']) : ''; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr class="client_refral">
                                                <td class="marginleft" align="right" height="50"><span
                                                        class="redCol">* </span>Are you referred by a one of your clients:</td>
                                                <td class="marginleft"><div class="input text">
                                                       <label for="TheRadioClient1">  <input type="radio" id="TheRadioClient1" name="client_refral" value="1" class="send_type2" >Yes</label> <label for="TheRadioClient2"><input type="radio" id="TheRadioClient2" class="send_type2" name="client_refral" value="2" checked>No</label>
                                                    </div></td>
                                            </tr>
                                            <tr class ="client_id_textbox">
                                                <td class="marginleft" align="right" height="50"><span
                                                        class="redCol">* </span>Enter our Client ID:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="refferal_id" id="" maxlength="50" size="30" class="registrationTextbox" value="<?php echo isset($records['name']) ? stripslashes($records['name']) : ''; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>First and Last name:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="name" id="name" maxlength="50" size="30" class="registrationTextbox" value="<?php echo isset($records['name']) ? stripslashes($records['name']) : ''; ?>"/>
                                                    </div></td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>Street Address:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <textarea name="address" id="address" rows="4" class="registrationTextbox"><?php echo isset($records['address'])  ? stripslashes($records['address']) :'' ?></textarea>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>Country:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <select id="country_id" class="country" name="country_id" style="width:200px;"><?php
                                                                echo countrylist($records['country_id']);
                                                            ?></select>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>State:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <select id="state_id" class="" name="state_id" class="state" style="width:200px;">
                                                            <?php echo stateList($records['country_id'], $records['state_id']); ?>
                                                        </select></td>

                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>Zip Code:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="zipcode" id="zipcode" maxlength="50" size="30" class="" value="<?php echo isset($records['zipcode']) ? $records['zipcode'] :''; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>Best Phone Number:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="phone" id="phone" maxlength="50" size="30" value="<?php echo isset($records['phone']) ? $records['phone'] : ''; ?>" class="registrationTextbox phoneUS"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>Cell Phone:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="cellphone" id="cellphone" maxlength="50" size="30" value="<?php echo isset($records['cellphone']) ? $records['cellphone'] :''; ?>" class="registrationTextbox  phoneUS"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>Email Address:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="email" id="email" maxlength="50" size="30" class="registrationTextbox email" value="<?php echo isset($records['email']) ? $records['email'] : '' ?>"/>
                                                    </div></td>
                                            </tr>

                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>Confirm Email Address:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="confirmemail" id="confirmemail" maxlength="50" size="30" class="registrationTextbox email" value="<?php echo isset($records['confirmemail']) ? $records['confirmemail'] : '' ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>Password:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="password" id="password" maxlength="50" size="30" class="registrationTextbox email" value="<?php echo isset($records['password']) ? $records['password'] : '' ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>Confirm Password:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="confirmpassword" id="confirmpassword" maxlength="50" size="30" class="registrationTextbox email" value="<?php echo isset($records['confirmpassword']) ? $records['confirmpassword'] : '' ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"><span
                                                        class="redCol">* </span>How did you hear about us:</td>
                                                <td class="marginleft"> <div class="input text">
                                                      <select name="source_id">
                                                       <?php                                                        
                                                        echo options($source, $_POST['source_id']);
                                                        ?>
                                                        
                                                        </select>
                                                        
                                                        
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="marginleft r" align="right" height="26" width="31%"></td>
                                                <td class="r marginleft" width="69%">
                                                <input type="submit" value="submit" name="submit" src="<?php echo ADMIN_IMAGE; ?>submit.gif" name="offer">     
                                                </td> 
                                            </tr>

                                            <!-- content area ends here --></td>
                                            </tr>
                                        </tbody>

                                    </table>
                                </td>
                            </tr>
                        </tbody></table></td></tr></tbody></table></form>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>
<script type="text/javascript">
    $(document).ready(function() {
        $('.sp_id_textbox').hide();
        $('.client_id_textbox').hide();
        
        
          $('.send_type').live('click',function(){
        var value = $(this).val();
        
         if(value == 1){
            $('.sp_id_textbox').show();
            $('.client_refral').hide();
        } 
        else {
             $('.sp_id_textbox').hide();
            $('.client_refral').show();
            $('#receiver_selected').removeClass('required');
            
        }
    })
    
     $('.send_type2').live('click',function(){
        var value2 = $(this).val();
        
         if(value2 == 1){                        
            $('.client_id_textbox').show();
           
           
        } else {
            
            $('.client_id_textbox').hide();
        }
    })

        $('#AddClient').validate();

        if ($('#country_id').val() == 229) {
            $('input[name="zipcode"]').rules('remove');
            $('input[name="zipcode"]').rules('add', {
                digits: true,
                required: true,
                maxlength: 5
            });
        }

    });

</script>