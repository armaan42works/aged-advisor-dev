<link rel="stylesheet" href="<?php echo HOME_PATH; ?>assets/plugins/bxslider/jquery.bxslider.css">
<script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyDkggLcQVkN0AF-fx-RNUOqeduvD6BgYRo"></script>
<script type="text/JavaScript" src="<?php echo HOME_PATH; ?>assets/js/jquery.PrintArea.js"></script>
<script type="text/JavaScript" src="<?php echo HOME_PATH; ?>assets/js/html2canvas.js"></script>
<script type="text/javascript" src="<?php echo HOME_PATH; ?>assets/plugins/bxslider/jquery.bxslider.js"></script>


<style >
    .provider-compair-update-box .provider-compair-update-list, .provider-compair-update-box .provider-compair-update-list .provider-heading-list, .provider-compair-update-box .provider-compair-update-list .provider-main-list, .provider-compair-update-box .provider-compair-update-list .provider-section-list, .provider-compair-update-box .provider-compair-update-list .provider-section-list .provider-section-drop-list, .provider-compair-update-box .provider-compair-update-list .provider-footer-list{
        line-height: 40px !important;
    }

    .general-comment {
        display: block; /* Fallback for non-webkit */
        display: -webkit-box;    
        -webkit-line-clamp: 3;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis;
    }
    a.thumbnail{
        margin-bottom: 0px !important;
    }
    .copyright{
        display: none  !important;
    }
    .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10{
        padding-right: 2px !important;
    }    
    h3{    
        line-height: 10px !important;
    }    
    .review-content{
        padding-bottom: 5px !important;
    }
    #map1 {
        width: 500px;
        height: 400px;
    }
    p#map_canvas2>div{ display: block !important;}
    .vacancy-radio{ 
        width:100%;
        display:block;
    }

    .top-header-bg{
        display:none;
    }
    .container{
        margin-top:0 !important;
    }
    h3{
        /*line-height: 0px !important;*/
    }
    #bottom {
        display: none !important;
    }
    .slider-lightbox {
        margin: 0px;
    }
    .bx-viewport{
        height : 380px !important;
    }
    span.prcnt-icon{
        background: #e67e22 !important;
    }
    .reviewing-top ul li{
        float:left;
        width:100%;
    }
    .provider-compair-update-box.provider-slide .provider-list-heading .provider-heading-logo .logo img{
        max-width:135px !important;
    }
    .provider-compair-update-box .provider-list-heading .provider-heading-logo {

        height: 60px !important;
    }

    @import url(//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css);

    fieldset, label { margin: 0; padding: 0; }

    /****** Style Star Rating Widget *****/

    .rating { 
        border: none;
        padding-right: 10px;
        vertical-align:top;
        display: inline-block;
        font-size: 13px !important;
    }

    .rating > input { display: none; } 
    .rating > label:before { 
        margin: 2px;
        font-size: 1.25em;
        font-family: FontAwesome;
        display: inline-block;
        content: "\f005";
    }

    .rating > .half:before { 
        content: "\f089";
        position: absolute;
    }

    .rating > label { 
        /*color: #ddd; */
        color: #f5f5f5 !important;
        color-adjust: exact;
        float: right; 
    }
    .rating > input:checked ~ label /* show gold star when clicked */
    { color: #f26431 !important;  } /* hover previous stars in list */

    .rating > input:checked + label:hover, /* hover current star when changing rating */
    .rating > input:checked ~ label:hover,
    .rating > label:hover ~ input:checked ~ label, /* lighten current selection */
    .rating > input:checked ~ label:hover ~ label { color: #f26431 !important;  } 
    #dateheader{
        background-color:#edf2f8 !important;
    }
    @media print { 

        @page { 
            margin: 0px 15px 0px; 
            size: A4 !important;        
        }  
        body {
            color: #f5f5f5 !important;
            margin: 0cm !important; 
        }
        #dateheader{
            background-color:#edf2f8 !important;
        }
        .container{
            margin-right: 0px !important;
            margin-left: 0px !important;
        }
    }

    @media print and (color) {
        * {
            -webkit-print-color-adjust: exact;
            -moz-print-color-adjust: exact;
            print-color-adjust: exact;
            color-adjust: exact;
        }
    }
    @media (max-width: 767px){
        .provider-compair-update-box .provider-compair-update-list li .provider-heading-logo {
            width: 100%;
            float: none;
            border-right: none;
            margin-top: 0px;
        }
        .m-pvd-heading, .m-pvd-cont {
            text-align: center;
            width: 100%;
            float: none;
            font-weight: normal;
        }
    }
</style>

<?php
global $db;

function get_services_name($id) {
    global $db;
    $name = '&nbsp;';
    $sql_query = "select name from ad_services where id='$id'";
    $res = mysqli_query($db->db_connect_id, $sql_query);

    while ($records = mysqli_fetch_assoc($res)) {
        $name = $records['name'];
    }
    return $name;
}

$_SESSION['csUserName'];
$_SESSION['SupUserName'];
$supp_id1 = $record['supplier_id'];

$pathInfo = parse_path();
if (ISSET($pathInfo['call_parts']['2'])) {
    $id = $pathInfo['call_parts']['2'];
}

$sql_query_compare = "select prd.* ,prd.id AS mypr_id, min_deff_sort AS 'deff_sort', usr.user_name, usr.user_image,extra.no_of_room as rooms from ad_products as prd, ad_users as usr, ad_extra_facility as extra  WHERE prd.supplier_Id=usr.id AND extra.product_id=prd.id AND md5(prd.id) = '$id'";
$res_compare = mysqli_query($db->db_connect_id, $sql_query_compare);

$image = '';
//include_once("./fckeditor/fckeditor.php");
if ($_SERVER['HTTP_X_FORWARDED_FOR']) {
    $IPaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $IPaddress = $_SERVER['REMOTE_ADDR'];
}

/* Default value */
$lat = -41.211722;
$long = 175.517578;
$zip = 5792;

$fQuery = "SELECT `certification_service_type`,`facility_type`,`address_suburb`,`address_city`,`zip` ,`facility_link` FROM `ad_products` WHERE md5(id)='$id' OR quick_url='$id'";
$fQueryresL = $db->sql_query($fQuery);
$fcType = $db->sql_fetchrowset($fQueryresL);

if (!ISSET($fcType[0])) {
    $idsearch = str_ireplace('-', '%', $id) . '%';
    $idsearch = str_ireplace('/', '%', $idsearch) . '%';
    $fQuery = "SELECT `certification_service_type`,`facility_type`,`address_suburb`,`address_city`,`zip` ,`facility_link` ,`quick_url` FROM `ad_products` WHERE quick_url LIKE '%$idsearch'";
    $fQueryresL = $db->sql_query($fQuery);
    $fcType = $db->sql_fetchrowset($fQueryresL);
}

foreach ($fcType as $fc) {
    $fCertification_service_type = $fc['certification_service_type'];
    $fFacility_type = $fc['facility_type'];
    $fAddress_suburb = $fc['address_suburb'];
    $fAddress_city = $fc['address_city'];
    $fZip = $fc['zip'];
    $other_facility_link = $fc['facility_link'];
    if (ISSET($fc['quick_url'])) {
        $id = $fc['quick_url'];
        redirect_to(HOME_PATH . $id);
        exit;
    }
}

$sql_query = "SELECT fdbk.*,
            AVG(NULLIF(fdbk.quality_of_care, 0)) as av_quality_of_care,
            AVG(NULLIF(fdbk.caring_staff,0)) as av_caring_staff,
            AVG(NULLIF(fdbk.responsive_management,0)) as av_responsive_management,
            AVG(NULLIF(fdbk.trips_outdoor_activities,0)) as av_trips_outdoor_activities,
            AVG(NULLIF(fdbk.indoor_entertainment,0)) as av_indoor_entertainment,
            AVG(NULLIF(fdbk.social_atmosphere,0)) as av_social_atmosphere,
            AVG(NULLIF(fdbk.enjoyable_food,0)) as av_enjoyable_food,
            memp.logo_enable,
            memp.map_enable,
            memp.address_enable,
            memp.phone_number_enable,
            memp.video_enable,
            memp.video_limit AS video_limit_enable,
            memp.youtube_video AS youtube_video_enable,
            memp.image_enable,
            memp.facility_desc_enable,
            memp.staff_enable,
            memp.management_enable,
            memp.activity_enable,
            memp.ext_url_enable,
            memp.image_limit,
            memp.product_limit,
            pein.certification_service_type,
            pein.certificate_license_end_date,
            pein.dhb_name,
            pein.certificate_name,
            pein.certification_period,
            pein.current_auditor,
            pein.email,
            pein.phone,
            pein.first_name,
            pein.last_name,
            pein.position,
            usr.company,
            
        pr.*,pr.id AS proId, "
        . "restCare.title as restcare_name,pr.id as pr_id, extinfo.*,extinfo.id as ex_id,pr.modified as last_update "
        . " FROM " . _prefix("products") . " AS pr "
        . " Left join " . _prefix("rest_cares") . " AS restCare ON pr.restcare_id = restCare.id"
        . " Left join " . _prefix("extra_facility") . " AS extinfo ON extinfo.product_id = pr.id"
        . " Left join " . _prefix("feedbacks") . " AS fdbk ON fdbk.product_id = pr.id  AND fdbk.deleted = 0"
        . " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id = pr.id"
        . " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id = pr.id AND prpn.current_plan = 1"
        . " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id = memp.id"
        . " Left join " . _prefix("users") . " AS usr ON usr.Id = pr.supplier_id"
        . " where md5(pr.id)='$id'";

$res = $db->sql_query($sql_query);
$records = $db->sql_fetchrowset($res);

if (count($records) && !empty($records)) {
    foreach ($records as $record) {
        $proId = $record['proId'];
        $quick_url = $record['quick_url'];
        $supp_id1 = $record['supplier_id'];
        $id = $record['proId'];
        $ex_id = $record['ex_id'];
        $pr_id = $record['pr_id'];
        $product_id = $pr_id;
        $title = $record['title'];
        $description = $record['description'];
        $description_our_version = $record['description_our_version'];
        $last_update = $record['last_update'];
        $keyword = $record['keyword'];
        $audit_url = $record['audit_url'];
        $image = $record['image'];
        $ext_url = $record['ext_url'];
        $youtube_video = $record['youtube_video'];
        $facility = $record['facility_type'];
        $address = $record['address'];
        $address_city = strip_tags(stripcslashes($record['address_city']));
        $address_suburb = strip_tags(stripcslashes($record['address_suburb']));
        $restcare_name = strip_tags(stripcslashes($record['restcare_name']));
        $zip = $record['zip'];
        $staff_comment = $record['staff_comment'];
        $management_comment = $record['management_comment'];
        $activities_comment = $record['activity_comment'];
        $staff_image = $record['staff_image'];
        $management_image = $record['management_image'];
        $activities_image = $record['activity_image'];
        $brief = $record['brief'];
        $noOfRooms = $record['no_of_room'];
        $noOfBeds = $record['no_of_beds'];
        $long = $record['longitude'];
        $lat = $record['latitude'];
        $company = $record['company'];

        /*         * ***********************************appointment date of the product************************ */

        $start_appointmnet_date = date('Y-m-d', strtotime($record['start_appointment_date']));
        $endt_appointmnet_date = date('Y-m-d', strtotime($record['end_appointment_date']));

        /*         * *******************************end  ppointment date of the product************************ */

        $position = $record['position'];
        $email = $record['email'];
        $phone = $record['phone'];
        $first_name = $record['first_name'];
        $last_name = $record['last_name'];
        $position = $record['position'];
        $dhb_name = $record['dhb_name'];
        $certification_period = $record['certification_period'];
        $current_auditor = $record['current_auditor'];
        $certificate_license_end_date = $record['certificate_license_end_date'];
        $certification_service_type = $record['certification_service_type'];
        $logo = $record['logo'];
        $quality_of_care = $record['av_quality_of_care'] * 20;
        $caring_staff = $record['av_caring_staff'] * 20;
        $avg_rating = $record['av_avg_rating'] * 20;
        $responsive_management = $record['av_responsive_management'] * 20;
        $trips_outdoor_activities = $record['av_trips_outdoor_activities'] * 20;
        $indoor_entertainment = $record['av_indoor_entertainment'] * 20;
        $social_atmoshphere = $record['av_social_atmosphere'] * 20;
        $enjoyable_food = $record['av_enjoyable_food'] * 20;

        // Convert new lines
        $brief = str_ireplace("\n", "<br>", $brief);
        $description = str_ireplace("\n", "<br>", $description);
        $description_our_version = str_ireplace("\n", "<br>", $description_our_version);
        $staff_comment = str_ireplace("\n", "<br>", $staff_comment);
        $management_comment = str_ireplace("\n", "<br>", $management_comment);
        $activity_comment = str_ireplace("\n", "<br>", $activity_comment);
        $address_other = str_ireplace("\n", "<br>", $address_other);
        $address = str_ireplace("\n", "<br>", $address);

        $last_update_sec = strtotime($last_update);

        if ($last_update_sec > 0) {
            $last_update = "(Last Updated: " . date('jS F Y', $last_update_sec) . ")";
        } else {
            $last_update = '';
        }
        $logo_enable = $record['logo_enable'];
        $map_enable = $record['map_enable'];
        $address_enable = $record['address_enable'];
        $phone_number_enable = $record['phone_number_enable'];
        $video_enable = $record['video_enable'];
        $video_limit_enable = $record['video_limit_enable']; // first x times videos
        $youtube_video_enable = $record['youtube_video_enable'];
        $image_enable = $record['image_enable'];
        $facility_desc_enable = $record['facility_desc_enable'];
        $staff_enable = $record['staff_enable'];
        $management_enable = $record['management_enable'];
        $activity_enable = $record['activity_enable'];
        $ext_url_enable = $record['ext_url_enable'];
        $image_limit = $record['image_limit'];
        $product_limit = $record['product_limit'];


        if (empty($lat) && empty($long)) {
            $lat = -41.211722;
            $long = 175.517578;
        }

        /*         * **************************code for analytics or No of visit page*********************** */
        if (!empty($_SESSION['csId']) && isset($_SESSION['csId'])) {
            $ana_user_id = $_SESSION['csId'];
        } elseif (!empty($_SESSION['userId']) && isset($_SESSION['userId'])) {
            $ana_user_id = $_SESSION['userId'];
        } else {
            $ana_user_id = 0;
        }
        $today = date('Y-m-d');
        $todays = date('d-m-y');


        // Use address
        $note_map = '';
        $map_address = 'New Zealand';
        $map_address_zoom = 5;
        $map_address_title = "New Zealand";
        if ($address_city) {
            $map_address = $address_city . ', ' . $map_address;
            $map_address_zoom = 10;
            $map_address_title = "$map_address" . '<br>Centre of City only shown.';
        }
        if ($address_suburb) {
            $map_address = $address_suburb . ', ' . $map_address;
            $map_address_zoom = 13;
            $map_address_title = "$map_address" . '<br>Centre of Suburb only shown.';
        }
        if ($address && $address_enable) {
            $map_address = $address . ', ' . $map_address;
            $map_address_zoom = 15;
            $map_address_title = "$map_address";
        }

//<!--**********************************code for similarfacility*****************************************--> 

        $qryToGetPlanID = "SELECT pln.plan_id as planId FROM ad_pro_plans AS pln WHERE pln.pro_id ='$proId'  AND pln.supplier_id='$supp_id1' AND  pln.current_plan = 1 AND pln.status = 1 AND pln.deleted = 0";

        $qryToGetPlanIDexe = mysqli_query($db->db_connect_id, $qryToGetPlanID);

        while ($qryToGetPlanIDresult = mysqli_fetch_array($qryToGetPlanIDexe)) {
            $plan_id_of_facility = $qryToGetPlanIDresult['planId'];
        }

        if ($plan_id_of_facility != 1) {

            $similarFacilityQuery = "select  ad_pro_plans.plan_id ,(select count(feedback) from ad_feedbacks fd where fd.product_id = prod.id and fd.status=1 and fd.deleted=0) as no_of_review,  ad_pro_plans.pro_id, prod.id, prod.quick_url, prod.title,prod.supplier_id, prod.address_suburb , prod.address_city ,gall.file,gall.type from ad_products AS prod  LEFT JOIN `ad_galleries` as gall ON prod.id = gall.pro_id LEFT JOIN  `ad_pro_plans` ON prod.id= ad_pro_plans.pro_id where (`address_suburb` ='$fAddress_suburb' OR `address_city` ='$fAddress_city' OR `zip` ='$fZip') AND ad_pro_plans.plan_id <>1 AND ad_pro_plans.current_plan = 1 AND ad_pro_plans.status = 1 AND ad_pro_plans.deleted = 0 AND prod.supplier_id='$supp_id1' AND prod.status='1' AND prod.deleted = 0 AND gall.type=0 AND gall.file IS NOT NULL AND prod.id NOT IN($proId) GROUP BY prod.id HAVING  no_of_review >= 3 ORDER BY no_of_review DESC LIMIT 5";
            $similarFacilityQueryResult = mysqli_query($db->db_connect_id, $similarFacilityQuery);
            if (mysqli_num_rows($similarFacilityQueryResult) <= 0) {
                
            }
        } else {

            $similarFacilityQuery = 'SELECT gall.file, fd.feedback, COUNT(fd.feedback) as count_feedback , pr.id, pr.quick_url,pr.title, pr.address_suburb, pr.address_city, fd.overall_rating,gall.file,gall.type FROM `ad_products` AS pr LEFT JOIN `ad_feedbacks` AS fd ON pr.id = fd.product_id LEFT JOIN `ad_galleries` as gall ON pr.id = gall.pro_id WHERE (`address_suburb` = "' . $fAddress_suburb . '" OR `address_city` = "' . $fAddress_city . '" OR `zip` = "' . $fZip . '") AND pr.quick_url <> "' . $id . '" AND  gall.type=0 AND gall.file IS NOT NULL AND prod.id NOT IN($id) AND pr.deleted=0 GROUP BY pr.id HAVING count_feedback >= 3 ORDER BY fd.overall_rating DESC LIMIT 5';
            $similarFacilityQueryResult = mysqli_query($db->db_connect_id, $similarFacilityQuery);
            if (mysqli_num_rows($similarFacilityQueryResult) <= 0) {

                $similarFacilityQuery = "SELECT gall.file, fd.feedback, COUNT(fd.feedback) as count_feedback , pr.id, pr.quick_url,pr.title, pr.address_suburb, pr.address_city,pr.deleted,pr.latitude, pr.longitude, SQRT(POW(69.1 * (pr.latitude - ('$lat')), 2) + POW(69.1 * (('$long') - pr.longitude) * COS(pr.latitude / 57.3), 2)) AS `distance`, fd.overall_rating,gall.file,gall.type FROM `ad_products` AS pr LEFT JOIN `ad_feedbacks` AS fd ON pr.id = fd.product_id LEFT JOIN `ad_galleries` as gall ON pr.id = gall.pro_id GROUP BY pr.id HAVING distance < 500 and distance > 0  AND count_feedback >= 3 AND pr.deleted = 0 AND gall.type=0 AND gall.file IS NOT NULL ORDER BY distance LIMIT 5";
            }
        }

        $similarFacilityQueryresL = $db->sql_query($similarFacilityQuery);  //  function of similar facility
        $similarFacility = $db->sql_fetchrowset($similarFacilityQueryresL); //function of similar facility


        $query = "SELECT id, title,file,type FROM " . _prefix("galleries") . "   "
                . " where deleted = 0  AND pro_id=" . $id . " AND type=0  ORDER BY status_first_img DESC LIMIT 0,$image_limit";


        $res = $db->sql_query($query);
        $data = $db->sql_fetchrowset($res);

// If no images then do not show heading
        if (count($data) == 0) {
            $image_enable = 0;
        }

        $sql_queryF = "select planS.id, service.name from " . _prefix('pro_services') . " AS planS "
                . "INNER JOIN " . _prefix('services') . " AS service  ON planS.service_id = service.id"
                . " where planS.product_id=$pr_id AND planS.deleted=0 AND planS.status = 1 ORDER BY id DESC";
        $resL = $db->sql_query($sql_queryF);
        $facilityType = $db->sql_fetchrowset($resF);
    }if (empty($proId) || $proId == NULL) {
        redirect_to(HOME_PATH);
        exit;
    }
}
?>
<?php
/* * **********************Search Query for associated Facility*********************** */

$sql_queryAssciated = "select plan_id from ad_pro_plans where pro_id = '$id' and current_plan = 1 and status =1 and deleted = 0 limit 1";
$resAssociated = $db->sql_query($sql_queryAssciated);
$associatedFacilityType = $db->sql_fetchrowset($resAssociated);
foreach ($associatedFacilityType as $aFT) {
    $showSimilarfacility = $aFT['plan_id'];
}

/* * **********************Search Query for associated Facility*********************** */
?>
<div class="content refined" id="report-print">        
    <div class="row" style="width:1170px; float:left; margin-left: -25px; margin-right: -25px">
        <div class="col-sm-12" style="width:1170px; float:left;" id="dateheader">
            <div style="width:900px; float:right; text-align:right;">
                <p style="margin:10px 0;">Copyright Aged Advisor. Data printed on <?php echo $todays ?>. Details Subject to change. E&amp;OE. <strong> info@agedadvisor • 0800 243 323 </strong></p>

            </div>
            <div class="logo" style="width:144px; float:left; margin-bottom:10px;">
                <img src="https://www.agedadvisor.nz/images/find_agedadvisor_retirement_villages_agedcare_logo.png" alt="" style="width:144px;" />
            </div>
        </div>
        <div class="col-md-8" style="width:780px; float:left;">
            <div class="star-detail">                  
                <h2 class="headline shortlist-heading" itemprop="name">
                    <?php echo isset($title) ? stripslashes($title) : ''; ?>
                    <?php if ($certification_service_type == "Home Services") { ?> 
                        <div class="hs"> </div> 
                    <?php } if ($certification_service_type == "Aged Care") { ?> 
                        <div class="ac"> </div> 
                    <?php } if ($certification_service_type == "Retirement Village") { ?> 
                        <div class="rv"> </div> 
                    <?php } ?>                     
                    <?php if (isset($_SESSION['searchid']) && is_numeric($_SESSION['searchid'])) {
                        $sid = $_SESSION['searchid']; ?>
                    <?php /*<a href="<?php echo HOME_PATH ?>search/search/<?php echo $sid ?>">&lt; Return to Search</a> &nbsp; */ ?>
                    <?php } ?>                      
                </h2>                    
                <div class="row">
                    <div class="col-sm-6 col-lg-7" style="width:455px; float:left;">
                        <div class="add-review" itemprop="aggregateRating" itemscope="" itemtype="http://schema.org/AggregateRating">
                            <meta itemprop="bestRating" content="5"/>
                            <meta itemprop="worstRating" content="0"/>
                            <span style="display:none;" itemprop="itemReviewed"><?php echo $title ?></span>
                            <span>Average Rating:
                                <span style="margin-right:10px">
                                <?php if (simlilarFacilityRating($record['pr_id']) == 0) { ?>
                                        Not Rated
                                <span class="text-hide" itemprop="ratingValue">0</span>
                                <?php } else { 
                                    echo OverAllNEWRatingProductReport($record['pr_id']);
                                    echo simlilarFacilityRating($record['pr_id']) ?>%
                                <?php } ?>
                                </span>
                            </span>
                            <?php if (overAllRatingProductCount($record['pr_id']) != '') { ?>
                                <a href="<?php echo HOME_PATH; ?>search/viewReview/<?php echo $record['pr_id']; ?>/Reviews-for-<?php echo str_ireplace('search/', '', $quick_url); ?>">
                                    <i class="fa fa-users mr-5"></i>
                                    <span itemprop="reviewCount">
                                        <?php echo overAllRatingProductCount($record['pr_id']); ?> Reviews
                                    </span>
                                </a>
                            <?php } else { ?>
                                <i class="fa fa-users mr-5"></i>
                                <span itemprop="reviewCount">
                                    <?php echo overAllRatingProductCount($record['pr_id']); ?>
                                </span>
                            <?php } ?>
                            <?php
                            $sup_id = $record['supplier_id'];
                            $opt_email = mysqli_query($db->db_connect_id, "select * from ad_users where id='$sup_id'");
                            while ($opt_email1 = mysqli_fetch_array($opt_email)) {
                                $opt_email2 = $opt_email1['email_updates_facility'];
                            }
                            ?>
                            <!-- supplier id -->
                            <input type="hidden" id="supp_messid" value="<?php echo $record['supplier_id']; ?>"/>
                            <input type="hidden" id="pro_fac_id" value="<?php echo $record['pr_id']; ?>"/>
                            <input type="hidden" id="customer_user_name" value="<?php echo $_SESSION['csUserName'] ?>"/>
                            <!-- close here supplier id -->
                        </div>
                        <?php if ($brief) { ?>
                            <p itemprop='description'><?php echo stripslashes($brief) ?></p>
                        <?php } ?>
                        <h3>Facility services offered</h3>
                        <ul>
                            <?php
                                $i = 1;
                                foreach ($facilityType as $record) {
                                    echo '<li>' . $record['name'] . '</li>';
                                    $i += 1;
                                }
                            ?>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-lg-5" style="width:325px; float:left;"> 
                        <div class="reviewing-top">
                            <ul>
                                <?php
                                echo '<li><p>Quality of Care <span class="rating" style="float:right;">
                                <input type="radio" id="star5" name="ratings-qc" value="5" ' . (($quality_of_care > 95) ? "checked" : "") . ' /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="ratings-qc" value="4 and a half" ' . (($quality_of_care > 80 && $quality_of_care < 95) ? "checked" : "") . ' /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="ratings-qc" value="4" ' . ($quality_of_care == 80 ? "checked" : "") . '/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="ratings-qc" value="3 and a half" ' . (($quality_of_care > 60 && $quality_of_care < 80) ? "checked" : "") . '/><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="ratings-qc" value="3" ' . (($quality_of_care == 60) ? "checked" : "") . '/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="ratings-qc" value="2 and a half" ' . (($quality_of_care > 40 && $quality_of_care < 60) ? "checked" : "") . ' /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="ratings-qc" value="2" ' . ($quality_of_care == 40 ? "checked" : "") . '/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="ratings-qc" value="1 and a half" ' . (($quality_of_care > 20 && $quality_of_care < 40) ? "checked" : "") . '/><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="ratings-qc" value="1" ' . (($quality_of_care == 20) ? "checked" : "") . '/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="ratings-qc" value="half" ' . (($quality_of_care > 10 && $quality_of_care < 20) ? "checked" : "") . '/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                </span></p></li>'
                                . '<li><p>Caring Staff<span class="rating"style="float:right;">
                                <input type="radio" id="star5" name="ratings-cs" value="5" ' . (($caring_staff > 95) ? "checked" : "") . ' /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="ratings-cs" value="4 and a half" ' . (($caring_staff > 80 && $caring_staff < 95) ? "checked" : "") . ' /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="ratings-cs" value="4" ' . ($caring_staff == 80 ? "checked" : "") . '/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="ratings-cs" value="3 and a half" ' . (($caring_staff > 60 && $caring_staff < 80) ? "checked" : "") . '/><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="ratings-cs" value="3" ' . (($caring_staff == 60) ? "checked" : "") . '/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="ratings-cs" value="2 and a half" ' . (($caring_staff > 40 && $caring_staff < 60) ? "checked" : "") . ' /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="ratings-cs" value="2" ' . ($caring_staff == 40 ? "checked" : "") . '/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="ratings-cs" value="1 and a half" ' . (($caring_staff > 20 && $caring_staff < 40) ? "checked" : "") . '/><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="ratings-cs" value="1" ' . (($caring_staff == 20) ? "checked" : "") . '/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="ratings-cs" value="half" ' . (($caring_staff > 10 && $caring_staff < 20) ? "checked" : "") . '/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                </span></p></li>'
                                . '<li><p>Responsive Mgmt<span class="rating" style="float:right;">
                                <input type="radio" id="star5" name="ratings-rm" value="5" ' . (($responsive_management > 95) ? "checked" : "") . ' /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="ratings-rm" value="4 and a half" ' . (($responsive_management > 80 && $responsive_management < 95) ? "checked" : "") . ' /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="ratings-rm" value="4" ' . ($responsive_management == 80 ? "checked" : "") . '/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="ratings-rm" value="3 and a half" ' . (($responsive_management > 60 && $responsive_management < 80) ? "checked" : "") . '/><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="ratings-rm" value="3" ' . (($responsive_management == 60) ? "checked" : "") . '/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="ratings-rm" value="2 and a half" ' . (($responsive_management > 40 && $responsive_management < 60) ? "checked" : "") . ' /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="ratings-rm" value="2" ' . ($responsive_management == 40 ? "checked" : "") . '/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="ratings-rm" value="1 and a half" ' . (($responsive_management > 20 && $responsive_management < 40) ? "checked" : "") . '/><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="ratings-rm" value="1" ' . (($responsive_management == 20) ? "checked" : "") . '/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="ratings-rm" value="half" ' . (($responsive_management > 10 && $responsive_management < 20) ? "checked" : "") . '/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                </span></p></li>'
                                . '<li><p>Outdoor Activity <span class="rating" style="float:right;">
                                <input type="radio" id="star5" name="ratings-oa" value="5" ' . (($trips_outdoor_activities > 95) ? "checked" : "") . ' /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="ratings-oa" value="4 and a half" ' . (($trips_outdoor_activities > 80 && $trips_outdoor_activities < 95) ? "checked" : "") . ' /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="ratings-oa" value="4" ' . ($trips_outdoor_activities == 80 ? "checked" : "") . '/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="ratings-oa" value="3 and a half" ' . (($trips_outdoor_activities > 60 && $trips_outdoor_activities < 80) ? "checked" : "") . '/><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="ratings-oa" value="3" ' . (($trips_outdoor_activities == 60) ? "checked" : "") . '/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="ratings-oa" value="2 and a half" ' . (($trips_outdoor_activities > 40 && $trips_outdoor_activities < 60) ? "checked" : "") . ' /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="ratings-oa" value="2" ' . ($trips_outdoor_activities == 40 ? "checked" : "") . '/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="ratings-oa" value="1 and a half" ' . (($trips_outdoor_activities > 20 && $trips_outdoor_activities < 40) ? "checked" : "") . '/><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="ratings-oa" value="1" ' . (($trips_outdoor_activities == 20) ? "checked" : "") . '/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="ratings-oa" value="half" ' . (($trips_outdoor_activities > 10 && $trips_outdoor_activities < 20) ? "checked" : "") . '/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                </span></p></li>'
                                . '<li><p>Indoor Entertainment <span class="rating" style="float:right;">
                                <input type="radio" id="star5" name="ratings-ie" value="5" ' . (($indoor_entertainment > 95) ? "checked" : "") . ' /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="ratings-ie" value="4 and a half" ' . (($indoor_entertainment > 80 && $indoor_entertainment < 95) ? "checked" : "") . ' /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="ratings-ie" value="4" ' . ($indoor_entertainment == 80 ? "checked" : "") . '/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="ratings-ie" value="3 and a half" ' . (($indoor_entertainment > 60 && $indoor_entertainment < 80) ? "checked" : "") . '/><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="ratings-ie" value="3" ' . (($indoor_entertainment == 60) ? "checked" : "") . '/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="ratings-ie" value="2 and a half" ' . (($indoor_entertainment > 40 && $indoor_entertainment < 60) ? "checked" : "") . ' /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="ratings-ie" value="2" ' . ($indoor_entertainment == 40 ? "checked" : "") . '/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="ratings-ie" value="1 and a half" ' . (($indoor_entertainment > 20 && $indoor_entertainment < 40) ? "checked" : "") . '/><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="ratings-ie" value="1" ' . (($indoor_entertainment == 20) ? "checked" : "") . '/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="ratings-ie" value="half" ' . (($indoor_entertainment > 10 && $indoor_entertainment < 20) ? "checked" : "") . '/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                </span></p></li>'
                                . '<li><p>Social Atmosphere <span class="rating" style="float:right;">
                                <input type="radio" id="star5" name="ratings-sa" value="5" ' . (($social_atmoshphere > 95) ? "checked" : "") . ' /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="ratings-sa" value="4 and a half" ' . (($social_atmoshphere > 80 && $social_atmoshphere < 95) ? "checked" : "") . ' /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="ratings-sa" value="4" ' . ($social_atmoshphere == 80 ? "checked" : "") . '/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="ratings-sa" value="3 and a half" ' . (($social_atmoshphere > 60 && $social_atmoshphere < 80) ? "checked" : "") . '/><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="ratings-sa" value="3" ' . (($social_atmoshphere == 60) ? "checked" : "") . '/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="ratings-sa" value="2 and a half" ' . (($social_atmoshphere > 40 && $social_atmoshphere < 60) ? "checked" : "") . ' /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="ratings-sa" value="2" ' . ($social_atmoshphere == 40 ? "checked" : "") . '/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="ratings-sa" value="1 and a half" ' . (($social_atmoshphere > 20 && $social_atmoshphere < 40) ? "checked" : "") . '/><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="ratings-sa" value="1" ' . (($social_atmoshphere == 20) ? "checked" : "") . '/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="ratings-sa" value="half" ' . (($social_atmoshphere > 10 && $social_atmoshphere < 20) ? "checked" : "") . '/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                </span></p></li>'
                                . '<li><p>Enjoyable Food <span class="rating" style="float:right;">
                                <input type="radio" id="star5" name="ratings-ef" value="5" ' . (($enjoyable_food > 95) ? "checked" : "") . ' /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half" name="ratings-ef" value="4 and a half" ' . (($enjoyable_food > 80 && $enjoyable_food < 95) ? "checked" : "") . ' /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4" name="ratings-ef" value="4" ' . ($enjoyable_food == 80 ? "checked" : "") . '/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half" name="ratings-ef" value="3 and a half" ' . (($enjoyable_food > 60 && $enjoyable_food < 80) ? "checked" : "") . '/><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3" name="ratings-ef" value="3" ' . (($enjoyable_food == 60) ? "checked" : "") . '/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half" name="ratings-ef" value="2 and a half" ' . (($enjoyable_food > 40 && $enjoyable_food < 60) ? "checked" : "") . ' /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2" name="ratings-ef" value="2" ' . ($enjoyable_food == 40 ? "checked" : "") . '/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half" name="ratings-ef" value="1 and a half" ' . (($enjoyable_food > 20 && $enjoyable_food < 40) ? "checked" : "") . '/><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1" name="ratings-ef" value="1" ' . (($enjoyable_food == 20) ? "checked" : "") . '/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf" name="ratings-ef" value="half" ' . (($enjoyable_food > 10 && $enjoyable_food < 20) ? "checked" : "") . '/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                </span></p></li>';
                                ?>
                            </ul>
                            <div class="clearfix"></div>	
                        </div>
                        <p style="font-size: 15px; white-space: nowrap;">Contact : 
                            <?php if (isset($first_name) && isset($last_name) && !empty($first_name) && !empty($last_name)) { ?>
                                <?php echo $first_name ?> <?php echo $last_name ?>
                                <?php if (isset($position) && !empty($position)) { ?>
                                    <span> ,<?php echo $position ?></span>
                                <?php } ?>
                                <br>
                            <?php } ?>                   
                            <?php if (isset($phone) && !empty($phone)) { ?><span class="fa fa-mobile"></span> <?php echo $phone ?><br><?php } ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
                <?php if ($data) { ?>
                    <div class="slider-lightbox">
                        <div class="slider1">
                            <?php
                            foreach ($data as $key => $record) {
                                $pic_description = $record['title'];
                                $fileImage = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
                                $fileImageSrc = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
                                $fileImageView = HOME_PATH . 'admin/files/gallery/images/' . $record['file'];
                                if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/admin/files/gallery/images/' . $record['file'])) {
                                ?>
                                    <div class="slide"><img src="<?php echo $fileImageView; ?>" alt="" title=""/></div>
                                <?php
                                }
                                break;
                            }
                            ?>
                        </div>
                    </div>
                <?php } ?>
            <div class="row">       
                <div class="col-md-12 col-sm-12 col-xs-12 container_wrapper" style="width:780px; float:left;">        

                    <div class="headline" style="margin:10px 0 -5px;">
                        <div class="pull-right"><?php echo $reviewlink; ?></div>

                        <div class="col-md-12 col-sm-12 col-xs-12 container_wrapper">
                            <?php
                            $curr_date = date('Y-m-d H:i:s');
                            if (isset($_SESSION['csUserName']) && isset($proId) && isset($supp_id1)) {
                                $cs_username = $_SESSION['csUserName'];
                                $visited_query = "select MAX(created)  from ad_feedbacks where sp_id={$supp_id1} AND cs_id={$_SESSION['csId']} AND product_id={$proId}";
                                $visit_query = mysqli_query($db->db_connect_id, $visited_query);
                                if (mysqli_num_rows($visit_query) > 0) {
                                    while ($visited_query_dt = mysqli_fetch_array($visit_query)) {
                                        $visit_facility_review_dt = $visited_query_dt['MAX(created)'];
                                    }

                                    if ($visit_facility_review_dt != '' && $visit_facility_review_dt != NULL) {

                                        $dteStart = new DateTime($curr_date);
                                        $dteEnd = new DateTime($visit_facility_review_dt);
                                        $dteDiff1 = $dteStart->diff($dteEnd);

                                        $date_diff_hr = $dteDiff1->format("%H");
                                        $date_diff_day = $dteDiff1->format("%D");
                                    }
                                }
                            }
                            ?>
                            <?php
                            if (isset($_SESSION['csUserName']) && isset($proId) && isset($supp_id1)) {
                                $visit_query_data = "select MAX(created)  from ad_feedbacks where sp_id={$supp_id1} AND cs_id={$_SESSION['csId']} AND product_id={$proId}";
                                $visiting_query = mysqli_query($db->db_connect_id, $visit_query_data);
                                if (mysqli_num_rows($visiting_query) > 0) {
                                    while ($visited_query_dtt = mysqli_fetch_array($visiting_query)) {
                                        $visit_facility_review_dtt = $visited_query_dtt['MAX(created)'];
                                    }
                                    if ($visit_facility_review_dtt == '' && $visit_facility_review_dtt == NULL) {
                                        ?>
                                                <div class="row">

                                                </div>
                                    <?php }
                                }
                            } else {
                                if (!isset($_SESSION['csUserName'])) {
                                }
                            }
                            ?>
                        </div>
                    </div> 
                    <?php checkfuncRep(md5($product_id)); ?>
                </div>

            </div>

            <div class="clearfix"></div>
            <div class="right-photo-gallery mt-30" id="similarfacility">
                    <?php if ($plan_id_of_facility == 1) { ?>

                    <div class="headline" style="display:none;">
                        <a name="images"></a>
                        <h4>You might also like...</h4>
                    </div>
                    <ul class="facilities-list img-rtng">
                        <?php
                        if (count($similarFacility)) {
                            $radio_similar = 0;
                            foreach ($similarFacility as $sFac) {
                                $fID = $sFac['id'];
                                $fQuick_url = $sFac['quick_url'];
                                $fTitle = $sFac['title'];

                                if (isset($sFac['file']) && !empty($sFac['file'])) {
                                    $fImageSet = $_SERVER['DOCUMENT_ROOT'] . '/admin/files/gallery/images/thumb_' . $sFac['file'];
                                    $fImage = 'thumb_' . $sFac['file'];
                                }

                                $fSuburb = $sFac['address_suburb'];
                                $fCity = $sFac['address_city'];
                                $fOverall_rating = simlilarFacilityRating($fID);
                                if (file_exists($fImageSet)) {
                                    $similarFacilityHtml = '<li class="col-sm-6" style="width:375px; float:left;">
                                        <div class="col-sm-12">
                                            <div style="position:relative;">
                                                <img src="' . HOME_PATH . 'admin/files/gallery/images/' . $fImage . '" alt="" title="" />
                                                <div class="facility-rating" style="position: absolute; width: 100%; top: 0; background: rgba(255,255,255,.8); padding: 15px;">

                                                    <a href="' . HOME_PATH . $fQuick_url . '"><span class="prcnt-icon">' . round($fOverall_rating) . '%</span>' . $fTitle . '<br/>
                                                    <span>' . $fSuburb . ', ' . $fCity . '</span>
                                                    <fieldset class="rating">
                                                        <input type="radio" id="star5" name="ratings-smf-' . $radio_similar . '" value="5" ' . (($fOverall_rating > 95) ? "checked" : "") . ' /><label class = "full" for="star5" title="Awesome - 5 stars"></label>
                                                        <input type="radio" id="star4half" name="ratings-smf-' . $radio_similar . '" value="4 and a half" ' . (($fOverall_rating > 80 && $fOverall_rating < 95) ? "checked" : "") . ' /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" id="star4" name="ratings-smf-' . $radio_similar . '" value="4" ' . ($fOverall_rating == 80 ? "checked" : "") . '/><label class = "full" for="star4" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" id="star3half" name="ratings-smf-' . $radio_similar . '" value="3 and a half" ' . (($fOverall_rating > 60 && $fOverall_rating < 80) ? "checked" : "") . '/><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" id="star3" name="ratings-smf-' . $radio_similar . '" value="3" ' . (($fOverall_rating == 60) ? "checked" : "") . '/><label class = "full" for="star3" title="Meh - 3 stars"></label>
                                                        <input type="radio" id="star2half" name="ratings-smf-' . $radio_similar . '" value="2 and a half" ' . (($fOverall_rating > 40 && $fOverall_rating < 60) ? "checked" : "") . ' /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" id="star2" name="ratings-smf-' . $radio_similar . '" value="2" ' . ($fOverall_rating == 40 ? "checked" : "") . '/><label class = "full" for="star2" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" id="star1half" name="ratings-smf-' . $radio_similar . '" value="1 and a half" ' . (($fOverall_rating > 20 && $fOverall_rating < 40) ? "checked" : "") . '/><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" id="star1" name="ratings-smf-' . $radio_similar . '" value="1" ' . (($fOverall_rating == 20) ? "checked" : "") . '/><label class = "full" for="star1" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" id="starhalf" name="ratings-smf-' . $radio_similar . '" value="half" ' . (($fOverall_rating > 10 && $fOverall_rating < 20) ? "checked" : "") . '/><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                    </a>
                                                </div> 
                                            </div>
                                        </div>
                                    </li>';
                                } else {
                                    $similarFacilityHtml = '';
                                }
                                echo $similarFacilityHtml;
                                $radio_similar++;
                            }
                        }
                    }
                    ?>
                </ul>  

            </div> 
            <div style="display:block; margin-top:15px;">
                <img src="/images/pimgpsh_fullsize_distr.jpg" alt="" style="width:100%;" />
            </div>
        </div>

        <div class="col-md-4" style="float: left; width:360px;">
            <div class="right-side-cover" style="width:360px;">
                <div class="clearfix"></div>

                <div class="left-map" style="width:360px; height:auto">
                    <div class="headline">
                        <h4><?php //echo isset($title) ? stripslashes($title) : '';  ?><a style="font-size: 14px;"><?php echo $map_address_title ?></a></h4>
                    </div>
                    <div id="map" class="map" style="width:360px; height:auto; display:block; margin-bottom: 20px;"></div>
                </div>

                <div class="right-photo-gallery" style="width:360px;margin-top: 0px;">
                    <?php if ($image_enable) { ?>
                        <div class="headline" style="display:none;"><a name="images"></a><h4><?php echo $title ?> Images</h4></div>
                        <div class="gallery-page">
                            <div class="row">
                                <?php
                                $countImg = 0;
                                foreach ($data as $key => $record) {
                                    $pic_description = $record['title'];
                                    $fileImage = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
                                    $fileImageSrc = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
                                    $fileImageView = HOME_PATH . 'admin/files/gallery/images/' . $record['file'];
                                    if (file_exists($_SERVER['DOCUMENT_ROOT'] . '/admin/files/gallery/images/thumb_' . $record['file'])) {
                                    ?>
                                        <div class="col-xs-6"><?php if ($pic_description) { ?> <p><?php echo $pic_description; ?></p> <?php } ?>
                                            <a class="thumbnail fancybox-button zoomer" data-rel="fancybox-button" title="<?php echo $pic_description; ?>" href="<?php echo $fileImageView ?>">
                                                <span class="overlay-zoom">  
                                                    <img alt="<?php echo $certification_service_type ?> <?php echo $pic_description ?>" title="<?php echo $pic_description ?>" src="<?php echo $fileImage; ?>" class="img-responsive">
                                                    <span class="zoom-icon"></span>
                                                </span>                                              
                                            </a>
                                        </div>
                                        <?php $countImg++;
                                        if ($countImg > 5) {
                                            break;
                                        }
                                    }
                                } ?>
                            </div>
                        </div>                          
                    <?php } ?>
                </div>

                <div class="clearfix"></div>
            </div>
            <?php if (!$image_enable) { ?>
                <div class="row" style="width:370px;">
                    <div class="right-img-box col-xs-12 text-left" style="width:370px;padding-right: 0px;">				
                        <?php
                        echo getRightBannerNew1($city_name);
                        ?>
                    </div>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
            <div id="widget"></div>
            <div class="provider-slider-box" style="margin-top: 15px; width:390px; margin-left:-15px;">
                <?php
                while ($records_compare = mysqli_fetch_assoc($res_compare)) {

                    $get_review = "select count(product_id) from  ad_feedbacks  WHERE product_id=" . $records_compare['pro_extra_info'];
                    $totalreview = mysqli_fetch_row(mysqli_query($db->db_connect_id, $get_review));

                    $sql_query_country = "select  count(supplier_Id) from  ad_products  WHERE supplier_Id=" . $records_compare['supplier_id'] . " AND `certification_service_type` = 'Retirement Village'";

                    $totalInCountry = mysqli_fetch_row(mysqli_query($db->db_connect_id, $sql_query_country));
                    ?>
                        <div class="provider-compair-update-box provider-slide selected" style="width: 100%;">
                            <div class="provider-compair-update-box-in ">
                                <ul class="provider-compair-update-list ">
                                    <li class="provider-list-heading">
                                        <ul class="provider-heading-list">
                                            <li class="provider-heading-head <?php if ((($amount < $records_compare['min_entry_value']) || ($records_compare['min_entry_sort'] == 999999999)) && ($amount > 0)) { echo "provider-compare-column-back"; } ?>" style="display:none;">
                                                <span class="catg"> Entry & Fee Information </span> 
                                                <span class="glyphicon glyphicon-move" aria-hidden="true" style="color: #f5f5f5;"></span>
                                            </li>
                                            <?php if (!empty($records_compare['user_image'])) { ?>                                        
                                            <li class="provider-heading-logo" style="min-height: 80px;">
                                                <div class="logo">
                                                    <span>
                                                        <img src ="<?php if ($records_compare['user_image']) { echo "https://www.agedadvisor.nz/admin/files/user/" . $records_compare['user_image']; } else { echo "https://www.agedadvisor.nz/images/notlogo-pc-page.png"; } ?>"  width="100%" height="auto" style="margin-right: 10px;border-right: 2px solid #555;padding-right: 8px;" />
                                                        <?php echo "(" . $totalInCountry[0] . " village"; if ($totalInCountry[0] > 1) { echo "s"; } echo " in NZ)"; ?>
                                                    </span>
                                                </div>
                                            </li>
                                            <?php } else { ?>
                                            <li class="provider-heading-logo">
                                                <span>
                                                    <?php echo "(" . $totalInCountry[0] . " village";
                                                    if ($totalInCountry[0] > 1) {
                                                        echo"s";
                                                    } echo " in NZ)"; ?>
                                                </span>
                                            </li>
                                            <?php } ?>
                                            <?php if (!empty($records_compare['fee_model'])) { ?> 
                                            <li class="provider-heading-name" style="height:50px;">
                                                <strong>
                                                    <?php
                                                    if ($records_compare['fee_model'] == 'ORA / LTO') {
                                                        $decimalplaces = 2;
                                                        if ($records_compare['deff_fee'] * 10 == ceil($records_compare['deff_fee'] * 10)) {
                                                            $decimalplaces = 1;
                                                        }
                                                        if ($records_compare['deff_fee'] == ceil($records_compare['deff_fee'])) {
                                                            $decimalplaces = 0;
                                                        }
                                                        echo $records_compare['fee_model'] . " <div>" . number_format($records_compare['deff_fee'], $decimalplaces) . "% per year, for " . $records_compare['max_year'] . " years";
                                                        if ($records_compare['initial_value'] == 0 && $records_compare['admin_fee_exit'] == 0) {
                                                            echo ". ";
                                                        }
                                                        if ($records_compare['initial_value'] > 0) {
                                                            echo" + " . $records_compare['initial_value'] . "% upfront";
                                                        }
                                                        if (!$records_compare['admin_fee_exit'] && $records_compare['initial_value']) {
                                                            echo ". ";
                                                        }
                                                        if ($records_compare['admin_fee_exit'] > 0) {
                                                            echo" + " . $records_compare['admin_fee_exit'] . "% on exit.";
                                                        }

                                                        if ($records_compare['deff_fee'] > 0) {
                                                            echo "Maximum " . ceil($records_compare['deff_fee'] * $records_compare['max_year'] + $records_compare['initial_value'] + $records_compare['admin_fee_exit']) . "%";
                                                        }
                                                        echo "</div>";
                                                    } else {
                                                        echo $records_compare['fee_model'];
                                                    }
                                                    ?>
                                                </strong>
                                            </li> 
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="provider-list-main">
                                        <ul class="provider-main-list">
                                            <li class="provider-main-minmum-age">
                                                <span class="m-pvd-heading">Minimum Age Entry : </span>
                                                <span class="m-pvd-cont min-age">
                                                    <?php if ($records_compare['min_age'] > 0) {
                                                        echo $records_compare['min_age'];
                                                    } else {
                                                        echo "Undisclosed";
                                                    } ?>
                                                </span>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="provider-list-section">
                                        <ul class="provider-section-list">                                        
                                            <li> <span class="m-pvd-heading">Lowest known price : </span>
                                                <?php if ($records_compare['min_entry_value'] > 0) {
                                                    echo "$" . number_format($records_compare['min_entry_value']);
                                                } else {
                                                    echo "Undisclosed";
                                                } ?><?php if ($records_compare['dwelling_low_price']) {
                                                    echo '<div style="margin-top:-25px;">' . get_services_name($records_compare['dwelling_low_price']) . '</div>';
                                                } ?>
                                            </li>                                                                                
                                            <li> <span class="m-pvd-heading">Highest known price : </span><?php if ($records_compare['max_entry_value'] > 0) {
                                                    echo "$" . number_format($records_compare['max_entry_value']);
                                                } else {
                                                    echo "Undisclosed";
                                                } ?><?php if ($records_compare['dwelling_high_price']) {
                                                    echo '<div style="margin-top:-25px;">' . get_services_name($records_compare['dwelling_high_price']) . '</div>';
                                                } ?>
                                            </li>                                                 
                                        </ul>
                                    </li>
                                    <li class="provider-section-capital">
                                        <span class="m-pvd-heading-full drop">
                                            <span class="m-pvd-heading">Maintenance Fees from : </span>
                                            <?php
                                            $fee_how_often = $records_compare['fee_how_often'];
                                            $fee = $records_compare['weekly_fee'];
                                            $period = '<div style="margin-top:-25px;">(Paid Weekly)</div>';
                                            if ($fee_how_often == 'Fortnightly') {
                                                $fee = $records_compare['weekly_fee'] / 2;
                                                $period = '<div style="margin-top:-25px;">(Paid Fortnightly)</div>';
                                            }
                                            if ($fee_how_often == 'Monthly') {
                                                $fee = $records_compare['weekly_fee'] * 12 / 52;
                                                $period = '<div style="margin-top:-25px;">(Paid Monthly)</div>';
                                            }
                                            if ($fee) {
                                                $decimalplaces = 2;
                                                if ($fee == ceil($fee)) {
                                                    $decimalplaces = 0;
                                                }
                                                echo "$" . number_format($fee, $decimalplaces) . " per week";
                                                ?><br><?php echo $period ?><?php
                                            } else {
                                                echo 'Undisclosed';
                                            }
                                            ?>
                                        </span>
                                        <div class="clearfix"></div>
                                        <ul class="provider-section-drop-list">
                                            <?php
                                            if ($records_compare['fixed'] == $records_compare['fixed']) {
                                                $fee = $records_compare['convert_to_weekly'];
                                                $fees = 52 * $fee;
                                                for ($year = 1; $year < 2; $year++) {
                                                    if ($year == 1) {
                                                        $totalf = $fees;
                                                    } else {
                                                        $fee = ($fees * ($cpi / 100)) + $fees;
                                                        $fees = $fee;
                                                        $totalf = $totalf + $fees;
                                                    }
                                                    ?>

                                                <?php
                                                }
                                                if ($records_compare['max_year'] == "OFF") {
                                                    for ($l = $records_compare['max_year']; $l < 2; $l++) {
                                                        ?>
                                                        <li class="m-hide"><?php echo "$" . number_format($totalf, 2, '.', ','); ?></li>
                                                    <?php
                                                    }
                                                }
                                            } else {
                                                for ($year = 1; $year < 2; $year++) {
                                                    ?>
                                                    <li>
                                                        <span class="m-pvd-heading"><?php echo $year . " Year"; ?></span>
                                                        <span class="m-pvd-cont"><?php echo "---" ?></span>
                                                    </li>
                                                <?php }
                                            }
                                            ?>
                                        </ul>
                                    </li>
                                    <li>
                                        <span class="m-pvd-heading-full">
                                            <span class="m-pvd-heading">Any Maintenance Fee Increases?  : </span>
                                            <span class="m-pvd-cont">&nbsp;
                                                <?php
                                                if (!empty($records_compare['fixed'])) {
                                                    if ($records_compare['fixed'] == 'Undisclosed') {
                                                        echo $records_compare['fixed'] . " (CPI used)";
                                                    } else {
                                                        echo $records_compare['fixed'];
                                                    }
                                                } else {
                                                    echo "N/A";
                                                }
                                                ?>&nbsp;
                                            </span>
                                        </span>
                                    </li>
                                    <li class="provider-list-footer">
                                        <ul class="provider-footer-list">
                                            <li class="provider-footer-facilties">
                                                <span class="m-pvd-heading">Extra Costs : </span>
                                                <span class="m-pvd-cont">&nbsp;<?php echo empty($records_compare['extra_fee']) ? 'N/A' : $records_compare['extra_fee']; ?></span>
                                            </li>

                                            <li class="provider-footer-facilties">
                                                <span class="m-pvd-heading">Number of dwellings : </span>
                                                <span class="m-pvd-cont">&nbsp;
                                                    <?php if ($records_compare['rooms'] > 0) {
                                                        echo $records_compare['rooms'];
                                                    } else {
                                                        echo "Undisclosed";
                                                    } ?>
                                                </span>
                                            </li>
                                            <li class="provider-footer-star">
                                                <span class="m-pvd-heading">Data current as of : </span>
                                                <span class="m-pvd-cont">
                                                    <span class="start-box">
                                                <?php
                                                if ($records_compare['last_update'] == '0000-00-00') {
                                                    echo "Unknown";
                                                } else {
                                                    echo date('j M, Y', strtotime($records_compare['last_update']));
                                                }
                                                ?>
                                                    </span>
                                                </span>
                                            </li>
                                            <li class="provider-footer-providers">
                                                <span class="m-pvd-heading"> Data supplied by : </span>
                                                <span class="m-pvd-cont">&nbsp;<?php echo empty($records_compare['info_by']) ? 'N/A' : $records_compare['info_by']; ?></span>
                                            </li>

                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                <?php } ?>
            </div>
        </div>                                                      
    </div>
    <div class="clearfix"></div>
    <!--/row-->
<!--/container-->
<!--=== End Content Part ===-->
<div style="position: relative; float: left; margin-top: 15px; z-index: 99; background: #fff; font-size: 12px; padding-right: 15px; border-right: 1px solid #ccc; text-transform:uppercase;">
    <p style="margin:0;margin-left: 15px;">SPECIAL THANKS <br />TO OUR TRUSTED <br />
        PARTNERS:</p>
</div>
<div class="flexslider" style="margin-left: 115px;">
    <ul class="slides">
        <li style="width: 150px; float: left; display: block;"><a href="http://www.caxton.co.nz" target="_blank"><img title="Caxton" src="https://www.agedadvisor.nz/admin/files/company_logo/1437104058_caxton_print.png" alt="logo Image" draggable="false"></a></li>
        <li style="width: 150px; float: left; display: block;"><a href="http://www.homeofpoi.com?friend=hop15" target="_blank"><img title="Home of Poi" src="https://www.agedadvisor.nz/admin/files/company_logo/1437036717_Home_of_Poi.png" alt="logo Image" draggable="false"></a></li>
        <li style="width: 150px; float: left; display: block;"><a href="http://www.bdo.co.nz" target="_blank"><img title="BDO Accountants" src="https://www.agedadvisor.nz/admin/files/company_logo/1439178368_BDO_logo_NZ.png" alt="logo Image" draggable="false"></a></li>
        <li style="width: 150px; float: left; display: block;"><a href="http://hearingtech.co.nz/" target="_blank"><img title="Hearing Technology" src="https://www.agedadvisor.nz/admin/files/company_logo/1460431102_hearing-technology-l.jpg" alt="logo Image" draggable="false"></a></li>
        <li style="width: 150px; float: left; display: block;"><a href="http://www.lawlink.co.nz/elder-law-specialists/" target="_blank"><img title="Lawlink" src="https://www.agedadvisor.nz/admin/files/company_logo/1460431672_lawlink-logo.jpg" alt="logo Image" draggable="false"></a></li>
        <li style="width: 150px; float: left; display: block;"><a href="http://www.caxton.co.nz" target="_blank"><img title="Caxton" src="https://www.agedadvisor.nz/admin/files/company_logo/1437104058_caxton_print.png" alt="logo Image" draggable="false"></a></li>
    </ul>
</div></div>  


<script type="text/javascript">
    $(document).ready(function (e) {
        window.print();

        $('.slider1').bxSlider({
            slideWidth: 750,
            minSlides: 1,
            maxSlides: 1,
            slideMargin: 10
        });

        var map;

        $(document).ready(function () {
            <?php if ($map_address !== "New Zealand") { ?>
                GMaps.geocode({
                    address: '<?php echo $map_address ?>',
                    callback: function (results, status) {
                        if (status == 'OK') {
                            var latlng = results[0].geometry.location;

                            var url = GMaps.staticMapURL({
                                size: [360, 300],
                                lat: latlng.lat(),
                                lng: latlng.lng(),
                                markers: [
                                    {lat: latlng.lat(), lng: latlng.lng()}
                                ],
                                key: 'AIzaSyDkggLcQVkN0AF-fx-RNUOqeduvD6BgYRo'
                            });
                            $('<img/>').attr('src', url)
                                    .appendTo('#map');
                        }
                    }
                });
            <?php } ?>
        });
    });

</script>
<!--=== End Content Part ===-->
<style>
    .headline a.pull-right.add-to-shortlist{display:block;}#bottom.headline a.pull-right.add-to-shortlist.hide{display:none}.headline a.remove_shortlist{float:right;display:block;cursor: pointer;}.headline a.remove_shortlist.hides{display:none;}.headline a.pull-right.add-to-shortlist.flt-l-ml-10{float:left; margin-left:10px;}.headline a.remove_shortlist.flt-r{float:right;}.headline a.pull-right.add-to-shortlist.ml-10{margin-left:10px;}.border-0{border:none;}.supp-message-c{text-align:center; color:#F00;}.vacanncy-txt-c{text-align: center;color:#FF0000;}.next-days.hide{display:none;}.mt-20-b-25-pdl-0{margin:20px 0px 25px; padding-left:0px;}.dis-inline{display:inline;}
</style>