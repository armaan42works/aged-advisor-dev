<?php
/*
 * Objective: Listing of all the coupons for new Clients
 * Filename: coupon.php
 * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
 * Created On : 25 August 2014
 */
?>
<?php
global $db;
$image = '';
$useId = $_SESSION['csId'];
if (isset($submit) && $submit == 'send_mail') {
    //  $fields_array = artwork();
    $to = ADMIN_EMAIL;
    $qry = 'select * from ' . _prefix('users') . ' where id="' . $useId . '" && deleted=0 && status=1';
    $result = $db->sql_query($qry);
    $data = $db->sql_fetchrow($result);
    $name = ucwords($data['first_name'] . " " . $data['last_name']);
    $useremail = $data['email'];
    $issue = $description;
    $email = emailTemplate('issue');
    if ($email['subject'] != '') {
        $message = str_replace(array('{username}', '{issue}'), array($name, $issue), $email['description']);
        $send = sendmail($to, $email['subject'], $message);
        if ($send) {
            // Message for insert
            $msg = common_message_supplier(1, constant('ISSUE_SENT'));
            $_SESSION['msg'] = $msg;
            //ends here
        } else {
            $_SESSION['msg'] = common_message_supplier(0, 'Mail has not sent to your email Id');
            //ends here
        }
    }
//        redirect_to(HOME_PATH . "supplier/issue_mail");
}
?>
<div class="row" style="margin-top:10px;margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <?php echo $_SESSION['msg']; ?><br>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>

<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <!--<h4 class="text-center hedding_main">Artwork Manager</h4>-->
        <div class="dashboard_container">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-envelope-o"></i> <span>Send Issue</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left"   id="pageData" >
                        <div class="container col-md-12 col-md-offset-2" style=" border: 1px solid rgb(204, 204, 204);">
                            <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
                                <div class="col-sm-6">
                                    <h2 class="">Send Mail</h2>
                                </div>

                                <div class="col-sm-6">
                                    <h2 style="float:right;" class="redCol small">* fields required</h2>
                                </div>
                            </div>
                            <p class="clearfix"></p>
                            <form name="sendIssue" id="sendIssue" action="" method="POST" class="form-horizontal form_payment" role="form" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">

                                        <div class="form-group" style="margin-bottom:6px;">
                                            <label for="description" class="col-lg-4 col-md-5 col-sm-10 col-xs-12 label_list text-right duration">Description&nbsp;<span class="redCol">*</span></label>
                                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                <textarea name="description" class="form-control input_fild_1" placeholder="Enter description" rows="10"></textarea>
                                            </div>
                                        </div>
                                        <div style="margin-top:6px;" class="col-md-12 form-group ">
                                            <label for="ecurity" class="col-lg-4 col-md-5 col-sm-10 col-xs-12 label_list text-right duration">Security Code&nbsp;<span class="redCol">*</span></label>
                                            <div class="col-md-7 col-md-7 col-sm-12 col-xs-12">
                                                <input type="text" tabindex="11"  name="captcha" placeholder="Security Code" id="captcha-text" class="form-control input_fild">
                                            </div>
                                        </div>
                                        <div style="margin-top:6px;margin-bottom: 40px;" class="col-md-12 form-group ">
                                            <label for="ecurity" class="col-lg-4 col-md-5 col-sm-10 col-xs-12 label_list text-right duration"></label>
                                            <div id="captcha-wrap" class="col-md-7 col-md-7 col-sm-12 col-xs-12">
                                                <img src="<?php echo HOME_PATH ?>admin/captcha/captcha.php" alt="" id="captcha"/>
                                                <img src="<?php echo HOME_PATH ?>images/captcha_refresh.gif" alt="refresh captcha" id="refresh-captcha" />
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom:20px;">
                                            <div class="col-lg-4 col-md-5 col-sm-10 col-xs-12"></div>
                                            <div class="col-md-7 col-md-7 col-sm-12 col-xs-12">
                                                <button type="submit" value="send_mail" name="submit" class="col-md-4 btn btn btn-danger">Send Mail</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
    label.error{
        color:red;
        font-size: 10px;

    }
    .redCol{
        color:red;
    }
    input,textarea{
        color:black;
        font-size: 15px;
    }

</style>
<script type="text/javascript">
//    $('#expectDate').datepicker({
//        dateFormat: 'dd/mm/yy',
//        changeMonth: true,
//        changeYear: true,
//        yearRange: '2010',
//        minDate: 0,
//    });
    $(document).ready(function() {
        setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);
        var path = "<?php echo HOME_PATH; ?>admin/";
        $('#refresh-captcha').on('click', function() {
            $('#captcha-text').val("");
            $('#captcha').attr("src", path + "captcha/captcha.php?rnd=" + Math.random());
        });
        $('#sendIssue').validate({
            rules: {
                description: {
                    required: true,
                    minlength: 3,
                    maxlength: 500
                },
                captcha: {
                    required: true,
                    remote:
                            {
                                url: path + 'captcha/checkCaptcha.php',
                                type: "post",
                                data:
                                        {
                                            code: function()
                                            {
                                                return $('#captcha-text').val();
                                            }
                                        }
                            }
                }
            },
            messages: {
                description: {
                    required: "This field is required ",
                    minlength: "Description must contain at least 3 chars",
                    maxlength: "Description should not be more than 500 characters"
                },
                captcha: {
                    required: "Please enter the verifcation code.",
                    remote: "Verication code incorrect, please try again."
                }
            },
            submitHandler: function(form) {
                $('#sendIssue').attr('disabled', 'disabled');
                form.submit();

            }
        });

//        $('input[type="submit"]').focus();
    });
//    $('#addLogo').validate();
</script>

<!--</div>-->