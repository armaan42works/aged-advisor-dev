<?php
include( "../../application_top.php" );
if($_SERVER['REMOTE_ADDR']=="103.87.57.142"){       
    error_reporting(E_ALL);    
}
global $db;
$modal_code_auto_open="";
$user_id = $_GET['sq'];
$pr_id = $_GET['ug'];
$enq_id = $_GET['sf'];
$dateTime = date('Y-m-d H:i:s');    


$qrychk="select enquiry_paid_status from ad_enquiries where md5(id)='$enq_id'"; 
$reschk=$db->sql_query($qrychk);
$rowchk=$db->sql_fetchrowset($reschk);
 if($rowchk[0]['enquiry_paid_status']==2){
$qryupdates="update ad_enquiries set time_of_response='$dateTime' where md5(id)='$enq_id'"; 
$resqrychk=$db->sql_query($qryupdates);
 }

$is_upgraded = "SELECT plan_id FROM ad_pro_plans WHERE md5(pro_id)='$pr_id' AND current_plan = 1";
$plan_data = $db->sql_query($is_upgraded);
while($result = $db->sql_fetchrow($plan_data)) { 
    $current_plan = $result[0];
} 


if($current_plan > 1){
    $UpdateSql = "UPDATE " . _prefix("enquiries") . " SET clicked_display = 1, facility_responded = 'y', clicked_display_time = '".$dateTime."'  WHERE md5(id)= '$enq_id' AND clicked_display = '0' ";    
    $updateResult = $db->sql_query($UpdateSql);//echo "$UpdateSql";exit;
}else{
    $UpdateSql = "UPDATE " . _prefix("enquiries") . " SET clicked_display = 1, clicked_display_time = '".$dateTime."'  WHERE md5(id)= '$enq_id' AND clicked_display = '0' ";    
    $updateResult = $db->sql_query($UpdateSql);//echo "here2";exit;
}

if(!empty($user_id) && !empty($pr_id) && !empty($enq_id) ){   

    function durationago( $datetime )
    {
        $now = new DateTime();       
        $interval = $datetime->diff($now);
       
        if ( $interval->d < 1 && $interval->h < 1 ) {
            return $datetime->diff($now)->format("%m minutes").' after enquiry made';
        }
        if ( $interval->d < 1 && $interval->h > 1 ) {
            return $datetime->diff($now)->format("%h hours").' after enquiry made';
        }
        if ( $interval->d > 1 && $interval->h < 1) {
            return $datetime->diff($now)->format("%d days").' after enquiry made';
        }
        if ( ($interval->h > 1) && ($interval->d > 1)) {
            return $datetime->diff($now)->format("%d days, %h hours").' after enquiry made';
        }
        if ( $interval->d == 1 && $interval->h == 1) {
            return $datetime->diff($now)->format("%d day, %h hour").' after enquiry made';
        }
        if ( $interval->d == 1 && $interval->h > 1) {
            return $datetime->diff($now)->format("%d day, %h hours").' after enquiry made';
        }        
    }
    
    $roomsQuery = "SELECT no_of_room, exfac.no_of_beds nobeds, certification_service_type from ad_extra_facility AS exfac"
            . " LEFT JOIN ad_products AS adpro ON adpro.id = exfac.product_id "
            . "where md5(product_id)='$pr_id' AND exfac.status=1 AND adpro.status=1";
        
    
    $roomsQueryRes = $db->sql_query($roomsQuery);   

    while($roomsQueryData = $db->sql_fetchrow($roomsQueryRes)) { 
        $aprtmt = $roomsQueryData['no_of_room'];
        $beds = $roomsQueryData['nobeds'];
        $fac_type = $roomsQueryData['certification_service_type'];
    }   
    
    $fmt = new NumberFormatter( 'en_US', NumberFormatter::CURRENCY );
    $fmt->setAttribute(NumberFormatter::FRACTION_DIGITS, 2);
    
    function nigel_format($value){
        if(floor($value)==$value){
            return $value;            
        }else{
            return number_format($value,2);            
        }
    }            
        
        
    if($fac_type == "Aged Care"){         
        if($beds >=100){
            $amt = 16.67;
            $gst = nigel_format(14.50);
            $paylatergst = $fmt->formatCurrency($gst*10,'USD');
            $amtforupgrade = $fmt->formatCurrency($gst*2,'USD');
            $paylateramt = $fmt->formatCurrency(10*$amt,'USD');
        }
        if($beds >= 40 && $beds <= 99){
            $amt = 10.92;
            $gst = nigel_format(9.50);
            $paylatergst = $fmt->formatCurrency($gst*10,'USD');
            $amtforupgrade = $fmt->formatCurrency($gst*2,'USD');
            $paylateramt = $fmt->formatCurrency(10*$amt,'USD');
        }
        if($beds < 40 ){
            $amt = 5.72;
            $gst = nigel_format(4.97);
            $paylatergst = $fmt->formatCurrency($gst*10,'USD');
            $amtforupgrade = $fmt->formatCurrency($gst*2,'USD');
            $paylateramt = $fmt->formatCurrency(10*$amt,'USD');
        }
    }

    if($fac_type == "Retirement Village" || $fac_type == "Home Care" ){
        if($aprtmt >=30 && $aprtmt <= 79 ){
            $amt = 28.17;
            $gst = nigel_format(24.50);
            $paylatergst = $fmt->formatCurrency($gst*10,'USD');
            $amtforupgrade = $fmt->formatCurrency($gst*2,'USD');
            $paylateramt = $fmt->formatCurrency(10*$amt,'USD');
        }
        if($aprtmt >= 80){
            $amt = 33.92;
            $gst = nigel_format(29.50);
            $paylatergst = $fmt->formatCurrency($gst*10,'USD');
            $amtforupgrade = $fmt->formatCurrency($gst*2,'USD');
            $paylateramt = $fmt->formatCurrency(10*$amt,'USD');
        }
        if($aprtmt < 30 ){
            $amt = 22.42;
            $gst = nigel_format(19.50);
            $paylatergst = $fmt->formatCurrency($gst*10,'USD');
            $amtforupgrade = $fmt->formatCurrency($gst*2,'USD');
            $paylateramt = $fmt->formatCurrency(10*$amt,'USD');
        }
    }
   
    $detailsQuery ="SELECT enq.id as enqid, enq_specifics, usr.id as usrid, enq.Supplier_id as supid, `first_name`,`last_name`,`email`,`phone`,`facility_name`,`time_of_enquiry` FROM `ad_users` as `usr`"
            . " left join ad_enquiries as `enq` on enq.user_id = usr.id"            
            . " WHERE md5(enq.id)='$enq_id' and md5(enq.prod_id)='$pr_id'";
    
    
    $detailsQueryRes = mysqli_query($db->db_connect_id, $detailsQuery);    
    while ( $data = mysqli_fetch_array( $detailsQueryRes,MYSQLI_ASSOC ) ) {
        $title= $data['facility_name'];
        $tempfname = $fullname = $data['first_name']." ".$data['last_name'];
        $tempemail = $email = $data['email'];         
        $phone = $data['phone'];
        $dateTime = $data['time_of_enquiry'];
        $sup_id = $data['supid'];
        $msg = $data['enq_specifics'];
    }
    
    $supQuery ="SELECT `first_name`,`last_name`,`email`,`phone` FROM `ad_users` WHERE id = $sup_id";
    $supQueryRes = mysqli_query($db->db_connect_id, $supQuery);    
    while ( $supdata = mysqli_fetch_array( $supQueryRes ) ) {        
        $supfullname = $supdata['first_name']." ".$supdata['last_name'];
        $supemail = $supdata['email'];         
        $supphone = $supdata['phone'];        
    }
    
    $enqPaymentStatusQuery = "select enquiry_paid_status,clicked_display_time from ad_enquiries where md5(id)='$enq_id'";     
    $enqPaymentStatusRes = mysqli_query($db->db_connect_id,  $enqPaymentStatusQuery );
    while ( $statusData = mysqli_fetch_array( $enqPaymentStatusRes ) ) {
        $status = $statusData['enquiry_paid_status'];
        $clicked_display_time = $statusData['clicked_display_time'];
    }
    
    $ugQuery = "select plan_id from ad_pro_plans where md5(pro_id)='$pr_id' AND current_plan=1";     
    $ugQueryRes = mysqli_query($db->db_connect_id,  $ugQuery );
    while ( $upgraded = mysqli_fetch_array( $ugQueryRes ) ) {
        $upgrade = $upgraded[ 'plan_id' ];
        if ( $upgrade > 1 || $status == 1 || $status == 2 || $status == 3) {            
            $hasupgraded = TRUE; 
            $email='<a href="mailto:'.$email.'">'.$email.'</a>';
            $duration = durationago(new DateTime($dateTime));                  
            $dateview = date("g:ia d/m/Y ",strtotime($clicked_display_time));
            $displayButton = "<div class='col-sm-12 text-center'>
                                <!--<button style='margin-top: 45px; width:100%;' value='".$enq_id."' class='displayButton btn btn-danger inner_btn comment_btn' type='button'>Click to Display Contact Details</button>-->
                                <p style='margin-bottom: 15px; margin-left: 5px; margin-right: 5px; margin-top: 15px;'><i>First time displayed : ".$dateview."</i></p>
                                <p style='margin-bottom: 15px; margin-left: 5px; margin-right: 5px; margin-top: 15px;'><i>".$duration.".</i></p>
                            </div>";            
        } else {
            $hasupgraded = FALSE;              
            $fullname    = substr( $fullname, 0, 3 ) . '..... .......';
            $email       = substr( $email, 0, 3 ) . '.......@.....';
            $phone       = substr( $phone, 0, 5 ) . '.....'; 
            $modal_code_auto_open="$('#myModal').modal('show');";
            $displayButton = "<div class='col-sm-12 text-center'>                
                                <button style='margin-top: 10px; margin-bottom: 10px; width:30%;' value='".$enq_id."' class='nonUpgradeddisplayButton btn btn-danger inner_btn comment_btn' type='submit'>Click to Display Contact Details</button>                            
                            </div>";
        }
    }    
    
    
?>       
        <div class="dashboard_right_col" style="width: 80%; margin-left: 9%;">                
            <h2 class="hedding_h2"><i class="fa fa-pencil-square-o"></i> <span style="padding: 0;">Enquiries</span> </h2>
            <h4 class="logo_bottom_color font_size_16">Facility Name: <?php echo $title;?></h4>
            <div class="row enquiries-box" style="margin-top: 15px; margin-bottom: 5px; border: 1px solid rgb(204, 204, 204);">
	            <?php echo $displayButton;?>
                <div class="col-sm-12">
                	<div class="row">
                    	<div class="col-sm-4">
                    		<p>The persons name is : </p>
                        </div>
                        <div class="col-sm-8">
                    		<p style="color:#f25822;font-weight: 900;"><?php echo $fullname; ?> </p>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-4">
                    		<p>The persons email address is : </p>
                        </div>
                        <div class="col-sm-8">
                    		<p style="color:#f25822;font-weight: 900;"><?php echo $email;?></p>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-4">
                    		<p>And their contact no. is : </p>
                        </div>
                        <div class="col-sm-8">
                    		<p style="color:#f25822;font-weight: 900;"><?php echo $phone;?></p>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-4">
                    		<p>Enquiry Specifics: </p>
                        </div>
                        <div class="col-sm-8">
                    		<p><?php echo $msg;?> </p>
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-4">
                    		<p>Date/Time of Enquiry : </p> 
                        </div>
                        <div class="col-sm-8">
                    		<p><?php $time = strtotime($dateTime); echo $myFormatForView = date("g:ia d/m/Y ", $time);?> </p>
                        </div>
                    </div>
                    
                </div>
                
                <?php echo $displayButton;?>
            </div>
        </div>
        <div class="dashboard_right_col">                
            <div id="myModal" class="modal fade" role="dialog">
                <div class="modal-dialog" style="width:890px;">
                    <div class="modal-content">                            
                        <div class="provider-compair-heading">
                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>To access the enquirers details, simply choose one of these options.. <i class="fa fa-times-circle" aria-hidden="true"style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                        </div>
                        <div class="provider-compair-form">                            
                                <div class="form-group" style="display:inline-block;">
                                    <div class="col-sm-9">
                                        <p><strong>1. ACCESS ENQUIRY NOW FOR FREE*.</strong><br>
                                        * Access the enquiry now and only pay if the lead turns into a
                                        sale / bed admission. Click [Access Enquiry Now] button to view terms and any success fee.</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-dangers" id="paylater_button" style="width: 182px; color: #fff;background-color: #042e6f;border-color: rgba(0,0,0,.5);margin-top: 17px;"> Access Enquiry Now </button>                                    
                                        <p style="text-align:center;color: #FF9800;">Free</p>                                      
                                    </div>
                                                                    </div>
                           
                                <div class="form-group" style="display:inline-block;margin-top:20px;">
                                    <div class="col-sm-9">
                                        <p><strong>2. UPGRADE NOW.</strong><br>
                                            This will give you immediate access to enquiries plus
                                            increase your conversion rates with extra photos, a website
                                            link, extra content and more. Either login to upgrade or send
                                            an 'Upgrade' request and we'll contact you back.</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-dangers" id="upgrade_button" style="width: 182px; color: #fff;background-color: #f15922;border-color: rgba(0,0,0,.5);margin-top: 10px;"> Request Upgrade </button>
                                        <p style="text-align:center;color: #FF9800;">From <?php echo $amtforupgrade?> + gst</p>
                                        <p style="text-align:center; margin-top: -15px;">OR</p>
                                        <a href="https://www.agedadvisor.nz/supplier/login" style="width: 182px; margin-top: -7px;background-color: #f15922;color: #fff;border-color: rgba(0,0,0,.5);" class="btn btn-dangers">Login to Upgrade</a>                                        
                                    </div>    
                                </div>
                            
                            
                                <div class="form-group" style="display:inline-block;">
                                    <div class="col-sm-9">
                                        <p><strong>2. PAY NOW FOR THE ENQUIRY ONLY.</strong><br>
                                        Pay only 50% of the Bronze upgrade cost and gain
                                        immediate access to this enquiry now, without upgrading.</p>
                                    </div>
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-dangers" id="paynow" style="width: 182px; color: #fff;background-color: #042e6f;border-color: rgba(0,0,0,.5);margin-top: 20px;"> Pay for Enquiry Now </button>
                                        <p style="text-align:center;color: #FF9800;">Only $<?php echo $gst;?> + gst</p>
                                    </div>    
                                </div>
                            
                            
                               
                                   
                                    <p style="text-align:center;color: #FF9800; margin-bottom: 0px;"><a href="https://www.agedadvisor.nz/why_am_i_being_charged" target="_blank">Why am I being charged for some options?</a></p>
                              
                            <div class="divider">
                                <hr class="left"/>If you are not taking enquiries, then select from below<hr class="right" />
                            </div>
                            
                                <div class="form-group" style="display:inline-block;">
                                    <div class="col-sm-9">
                                        <p><strong>NOT 'CURRENTLY' TAKING ENQUIRIES.</strong><br>
                                        If you're full, we will advise the enquirer but continue to
                                        keep you informed when another enquiry comes in.</p>
                                    </div>
                                    <div class="col-sm-3">
                                        
                                        <button type="button" class="nottaking btn btn-dangers" value="not" style="width: 182px; color: #fff;background-color: #4f5e94;border-color: rgba(0,0,0,.5);margin-top: 15px;"> Not Taking Enquiries </button>
                                    </div>
                                </div>
                           
                                <div class="form-group" style="display:inline-block;">
                                    <div class="col-sm-9">
                                        <p><strong>NOT TAKING ENQUIRIES - PLEASE STOP EMAILS.</strong><br>
                                        We will advise the enquirer and stop sending any future
                                        enquiries through to you, unless you advise us otherwise.</p>
                                    </div>
                                    <div class="col-sm-3">                                        
                                        <button type="button" class="notinterested btn btn-dangers" value="nin" style="width: 182px; color: #fff;background-color: #4f5e94;border-color: rgba(0,0,0,.5);margin-top: 15px;"> Stop ALL Enquiry Emails </button>
                                    </div>
                                </div>                                                                                                                                         
                                                    
                            <div style='height:0px;'>&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
        <div class="dashboard_right_col">                
            <div id="notModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content blue-light-gradient">
                        <div class="provider-compair-heading">
                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>ARE YOU SURE? <i class="fa fa-times-circle" aria-hidden="true"style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                        </div>
                        <div class="provider-compair-form">                            
                            <form name="cancelform" method="post" id="cancelform">                                                             
                                <div style='height:20px;'>&nbsp;</div>                                                                                                                             
                                <input type="hidden" name="enquiry_id" id="enquiry_id" value="<?php echo $enq_id;?>">
                                <input type="hidden" name="prid" id="prid" value="<?php echo $pr_id;?>">
                                <input type="hidden" name="uid" id="uid" value="<?php echo $user_id;?>">
                                <input type="hidden" name="suid" id="suid" value="<?php echo $sup_id;?>">
                                <input type="hidden" name="sec-modal" id="sec-modal">
                                <button type="submit" class="btn btn-danger">Yes</button>
                                <button type="button" class="btn btn-info" data-dismiss="modal" style="margin-left:15px;" id="cancel-button">Cancel</button>
                            </form>
                            <div style='height:0px;'>&nbsp;</div>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
        <div class="dashboard_right_col">                
            <div id="payNowModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content blue-light-gradient">             
                        <div class="provider-compair-heading">
                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>Pay Now: Secure Credit Card Payment <i class="fa fa-times-circle" aria-hidden="true"style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                        </div>
                        <div class="provider-compair-form">
                            <!--<h4 id="info" style="color: #f9920d; margin-left: 15px;"></h4>-->
                            <div class="alert alert-success" role="alert" id="infos" style="display:none; text-align: center;">                                
                            </div>
                            <div class="alert alert-danger" role="alert" id="infof" style="display:none; text-align: center;">
                                
                            </div>
                            <!--AMIT ENC KEY
                            <form name="payNowForm" id="payNowForm" data-eway-encrypt-key="u3ZrcmZagKZdU1YAFvb0OloniFNRIqNOyB4/mFimdczXLKJa18XfJdYOOZX7Aw1qo98ALfuzxEZxLDeDVQFCgdQ2XOJjTebZBwY/g9743yzG8R8FfUmjNG9AdLjx/F9kGuXpiWOtmhQoIHt5P/loOY2wi/d1ZKccdyDXZZ7MEWzV83BsK4T9kjBBjXZ+khDViTfQHlVa/GVGLZUHGGYUtPlt+VFJn01LHnBrvtSya91/MA+8ymEP3cQ9jLnqEng8vS2CxcT0SH0mifv5uH0qzTqVh3bQ13gyaJCR+4fh82YBu6U1Ui83oGQaaGTH1yPAYd2GqrqADrZM2rGpSBm4/w==">-->
                            <form name="payNowForm" id="payNowForm" data-eway-encrypt-key="u7kmQFBSy9ZWHMj+5q+uxkJhbMalzh8Ni5DiNn75MG4NmxFufiEqtAX/GNW2cmSUACk42g0MwBDD+mETaiEJlfNyhMCqYU/eWk+jMY/raxxQFlzfMtDhRUIPm7xkJDeucAvvqx15L5mg5efpNJBxnkTwxwIGMjDvTMCt4kmcCrtLaIFRvHyX9OSqczetvlzWA1NzTyhbyw1z/o45FwQy9smE+QiRvLF8PH5Zt+yNelPf55ksKCxEIzkJmmumtz7dZJgCj6qHs/6j8E5wW6V0qs653Y4lK9W9rYSNQdt5Ks6f+pRrBUlb7USKyC8CLtkuysrRrFIliz8tfONr9gGihw==">
                                <div class="form-group" style="min-height: 50px;">
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Name on Card</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="text" value="" class="form-control" id="card_name" placeholder="Name">
                                    </div>                        
                                </div>                                        
                                <div class="form-group" style="min-height: 50px;">
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Card Number</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="number" value="" class="form-control" id="card_number" placeholder="Number">
                                    </div>
                                </div>                                       
                                <div class="form-group" style="min-height: 50px;">                                    
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">CVV</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="text" value="" class="form-control" id="cvv" placeholder="CVV">
                                    </div>
                                </div>                                 
                                <div class="form-group" style="min-height: 50px;">                                    
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Valid Till</label>
                                    <div class="col-sm-4" style="margin-bottom: 15px;">                                        
                                        <select name='month' id='month' style="width:100%;min-height: 34px;" required>
                                            <option value=''>Month</option>
                                            <option value='01'>January</option>
                                            <option value='02'>February</option>
                                            <option value='03'>March</option>
                                            <option value='04'>April</option>
                                            <option value='05'>May</option>
                                            <option value='06'>June</option>
                                            <option value='07'>July</option>
                                            <option value='08'>August</option>
                                            <option value='09'>September</option>
                                            <option value='10'>October</option>
                                            <option value='11'>November</option>
                                            <option value='12'>December</option>
                                        </select> 
                                    </div>
                                    <div class="col-sm-5" style="margin-bottom: 15px;">
                                        <input type="text" class="form-control" id="year" placeholder="Year(yy or yyyy)" required maxlength="4" pattern="\d{4}">
                                    </div>
                                </div>
                                <div class="form-group" style="min-height: 50px;">
                                    <div class="col-sm-3">
                                        <label class="col-form-label">Total Amount</label>
                                        <p style="font-size: 13px;margin-top: -10px;">*Including gst</p>
                                    </div>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="text" value="<?php echo $amt;?>" class="form-control" placeholder="Amount" disabled>
                                    </div>
                                </div> 
                                <div class="form-group"> 
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;"></label>                                    
                                    <button type="submit" id="submit_payment" class="btn btn-danger" style="width:120px;margin-left: 15px;">Make Payment </button>
                                    <p style="display: none;width: 140px; margin-left: 15px;" id="sendingmsgpay" class="btn btn-danger">Processing..<span> 
                                        <img src="../../assets/img/72.gif"> </span>
                                    </p>
                                    <p style="display: none;width: auto;" id="sentmsgpay" class="btn btn-danger">Transaction Successful! Click here to see the enquirer details.</p>
                                    <input type="hidden" name="supid" id="supid" value="<?php echo $sup_id;?>"> 
                                    <input type="hidden" name="enqid" id="enqid" value="<?php echo $enq_id;?>">
                                    <input type="hidden" name="prodid" id="prodid" value="<?php echo $pr_id;?>">
                                </div>                                                            
                                <div style='height:0px;'>&nbsp;</div>
                            </form>
                        </div>
                        <div >
                                                        
                                <img src="https://seeklogo.com/images/M/mastercard-logo-473B8726A9-seeklogo.com.png" class="images" alt="master">
                            
                                <img src="https://seeklogo.com/images/V/VISA-logo-62D5B26FE1-seeklogo.com.png" class="images">
                            
                                <img src="https://eway.io/images/eway-logos/eway-logo-1499-630.png" class="images">
                            
                        </div>
                    </div>
                </div>
            </div>                
        </div>

        <div class="dashboard_right_col">                
            <div id="upgradeModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content blue-light-gradient">             
                        <div class="provider-compair-heading">
                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>1.Please enter the following details <i class="fa fa-times-circle" aria-hidden="true" style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                        </div>
                        <div class="provider-compair-form">
                            <div class="alert alert-success" role="alert" id="upgradeinfos" style="display:none; text-align: center;">
                                <strong>Thank You!</strong> Your details has been sent to AgedAdvisor!
                            </div>
                            <div class="alert alert-danger" role="alert" id="upgradeinfof" style="display:none; text-align: center;">
                                <strong>Oh snap!</strong> Server error. Please try again!
                            </div>
                            <form name="upgradeform" id="upgradeform">                                 
                                <div class="form-group" style="min-height: 50px;">
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Name</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="text" value="<?php echo $supfullname;?>" class="form-control" id="upgradename" placeholder="Name" required>
                                    </div>                        
                                </div>                                        
                                <div class="form-group" style="min-height: 50px;">
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Facility Name</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="text" value="<?php echo $title;?>" class="form-control" id="upgradefname" placeholder="Facility Name" disabled>
                                    </div>
                                </div>                                       
                                <div class="form-group" style="min-height: 50px;">                                    
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Phone</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="number" value="<?php echo $supphone;?>" class="form-control" id="upgradephone" placeholder="Phone" required>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;"></label>
                                    <button type="submit" id="upgrade_submit" class="btn btn-danger" style="margin-left: 15px;">Request Upgrade </button>
                                    <p style="display: none;width: 140px; margin-left: 15px;" id="sendingmsgupgrade" class="btn btn-danger">Sending Now<span> 
                                        <img src="../../assets/img/72.gif"> </span>
                                    </p>
                                    <p style="display: none;width: 110px; margin-left: 15px;" id="sentmsgupgrade" class="btn btn-danger">Request Sent!</p>
                                    <p style="margin-left: 15px; margin-top: 15px; font-size: 17px;color: #000;">We will automatically display the enquirers details once this is sent.</p>                                                                        
                                </div>                                 
                                <div style="height:0px;">&nbsp;</div>
                            </form>
                        </div>
                        <div class="provider-compair-headings" style="text-align:center;">
                            <p>or</p>
                            <h4>Free Phone 0800 243323</h4>
                            <h4><a href="https://www.agedadvisor.nz/Upgrade_options_jul2017.pdf" target="_blank">Download Prices and Options PDF</a></h4>
                        </div>
                    </div>
                </div>
            </div>                
        </div>

        <div class="dashboard_right_col">                
            <div id="paylaterModal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content blue-light-gradient">     <!-- When NOT logged in to their account paylater option-->         
                        <div class="provider-compair-heading">
                            <h2><i class="fa fa-hand-o-right" style="margin-right: 30px;"></i>Complete details to access enquiry<i class="fa fa-times-circle" aria-hidden="true" style="float: right; cursor: pointer;" data-dismiss="modal"></i></h2>
                        </div>
                        <div class="provider-compair-form">                                                        
                            <div class="alert alert-success" role="alert" id="paylaterinfos" style="display:none; text-align: center;">
                                <strong>Thank You! Details below</strong>                                
                            </div>
                            <div class="alert alert-danger" role="alert" id="paylaterinfof" style="display:none; text-align: center;">
                                <strong>Oh snap!</strong> Server error. Please try again!
                            </div>
                            <form name="paylaterform" id="paylaterform">
                                <div class="form-group" style="min-height: 50px;">                                    
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Facility Name</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="text" value="<?php echo $title;?>" class="form-control" placeholder="Facility Name" disabled="disabled">
                                    </div>
                                </div>
                                <div class="form-group" style="min-height: 50px;">
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Name</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="text" value="" class="form-control" id="latername" placeholder="Your Name" required>
                                    </div>                        
                                </div>                                                                                                               
                                <div class="form-group" style="min-height: 50px;">                                    
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Position</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
                                        <input type="text" value="" class="form-control" id="laterposition" placeholder="Your Position" required>
                                    </div>
                                </div>
                                <div class="form-group" style="min-height: 50px;">Get the enquiry now for FREE, and only pay if the enquiry converts into a sale or bed admission. A payment, only on invoice of <?php echo $paylateramt;?>, is payable to Agedadvisor on confirmation of sale or bed admission.                                  
                                   <!-- <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;">Total Amount</label>
                                    <div class="col-sm-9" style="margin-bottom: 15px;">
	                                    
                                        <input type="text" value="<?php echo $paylateramt;?>" class="form-control" placeholder="Amount" disabled>
                                    </div>-->
                                </div>
                                
                                <div class="form-group" style="min-height: 50px;">                                    
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;"></label>
                                    <div class="col-sm-1" style="margin-bottom: 15px;">
                                        <input type="checkbox" value="" class="form-control" id="terms"  checked="checked" required>
                                    </div>
                                    <div class="col-sm-8" style="margin-bottom: 15px; padding: 0;">
                                        <span><a href="https://www.agedadvisor.nz/terms-and-conditions" target="_blank">I am authorised, and agree to the Terms and Conditions.</a></span>
                                    </div>
                                </div>
                                <div class="form-group"> 
                                    <label class="col-sm-3 col-form-label" style="margin-bottom: 15px;"></label>
                                    <button type="submit" id="paylater_submit" class="btn btn-danger" style="width:112px;margin-left: 15px;">Submit </button>
                                    <p style="display: none;width: 140px; margin-left: 15px;" id="sendingmsgpaylater" class="btn btn-danger">Sending Now <span> 
                                        <img src="../../assets/img/72.gif"> </span>
                                    </p>
                                    <p style="display: none;width: 110px; margin-left: 15px;" id="sentmsgpaylater" class="btn btn-danger">Request Sent!</p>                                    
                                </div>                                                            
                                <div style="height:0px;">&nbsp;</div>                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>                
        </div>
    
<?php }else{
    echo "No Records Found";
}
?>
<style>
    .btn-dangers:hover, .btn-dangers:focus, .btn-dangers:active, .btn-dangers.active {
        color: #fff;
        background-color: #e62117 !important;
    }    
    .divider {	
        text-align:center;
        margin-bottom: 30px;
    }

.divider hr {
	margin-left:auto;
	margin-right:auto;
	width:25%;
}

hr {
    margin: 15px 0;
}

.left {
	float:left;
}

.right {
	float:right;
}
.provider-compair-headings {
    position: relative;
    border-bottom: 1px solid #ddd;
    padding: 8px 15px;
    box-shadow: 0 -3px 3px rgba(0,0,0,.2);
}
.images {
   display: inline;
    margin: 5px;
    padding: 10px;
    vertical-align: middle;
    width: 185px;
}

/*input:valid {
  color: green;
}
input:invalid {
  color: red;
}*/

.enquiries-box{
	position:relative;
	padding:15px 0;
}
.enquiries-box p{
	margin:0 5px 15px;
}
</style>
<script src="https://secure.ewaypayments.com/scripts/eCrypt.min.js"></script>
<script>
    $(document).ready(function(){
        var path = '<?php echo HOME_PATH; ?>ajaxFront.php'; 
        var paymentPath = '<?php echo HOME_PATH; ?>enquiry-payment/index.php'; 
        <?php echo $modal_code_auto_open?>
        
        
        $('.nonUpgradeddisplayButton').on('click',function(){            
            $('#myModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        
        $('#paynow').on('click',function(){
            $('#myModal').modal('hide');
            $('#payNowModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        
        $('#upgrade_button').on('click',function(){
            $('#myModal').modal('hide');
            $('#upgradeModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        });
        $('#paylater_button').on('click',function(){
            $('#myModal').modal('hide');
            $('#paylaterModal').modal({
                backdrop: 'static',
                keyboard: false
            });
        });        
                
        $("#submit_payment").click(function(event){
            $('#notModal').modal('hide');
            $("#sendingmsgpay").show();
            $("#submit_payment").hide();
            $.ajax({
                type: 'POST',
                url: paymentPath,
                 data: {
                    card_name: $('#card_name').val(),
                    card_number: eCrypt.encryptValue($('#card_number').val()),
                    card_cvn: eCrypt.encryptValue($('#cvv').val()),
                    month: $('#month').val(),
                    year: $('#year').val(),
                    supid : $('#supid').val(),
                    prodid : $('#prodid').val(),
                    enqid : $('#enqid').val()
                  },
                success: function(response) {                    
                    var result = jQuery.parseJSON( response );
                    if( result.status == 1 ){                        
                        $('#infos').html(result.response_message); 
                        $('#infos').show();
                        $('#infof').hide();
                        $('#payNowForm').get(0).reset();                        
                        $("#sentmsgpay").show();                       
                        $("#sendingmsgpay").hide();
                        $("#sentmsgpay").click(function(){
                            location.reload();
                        });                        
                    } else if( result.status == 2 ){
                        $("#sendingmsgpay").hide();
                        $("#submit_payment").show();
                        var error_message="";                       
                        error_message+="Error: "+result.response_message +"<br>";                        
                        $('#infof').html(error_message);
                        $('#infof').show();
                        $('#infos').hide();
                    }
                }
            });            
            event.preventDefault();
        });
        
        $('.nottaking, .notinterested').click(function(event){            
            var reason = $(this).val();            
            $('#sec-modal').val(reason);
            $('#myModal').modal('hide');
            $('#notModal').modal({
                backdrop: 'static',
                keyboard: false
            });
            event.preventDefault();
        });
        
        $("#cancelform").submit(function(event){                       
            $.ajax({
                type: 'POST',
                url: path,
                data: {action: 'extra-enquiry-not-upgraded-not-interested', data : $("#cancelform").serialize()},
                success: function(response) {                    
                }
            });
            $('#notModal').modal('hide');
            event.preventDefault();
        });
        
        $("#upgradeform").submit(function(event){            
            var supid = $('#supid').val();
            var prodid = $('#prodid').val();
            var enqid = $('#enqid').val();
            var supname = $('#upgradename').val();
            var supphone = $('#upgradephone').val();
            $("#sendingmsgupgrade").show();
            $("#upgrade_submit").hide();
            $.ajax({
                type: 'POST',
                url: path,
                data: {action: 'new-enquiry-to-upgrade', prodid: prodid, supid: supid, enqid: enqid, supname:supname, supphone:supphone},
                success: function(response) {
                    if(response == 1){
                        $("#upgradeinfos").show();
                        $("#sentmsgupgrade").show();                       
                        $("#sendingmsgupgrade").hide();
                        setTimeout(function(){                            
                            location.reload();
                        },3000);
                    }else{
                        $("#upgradeinfof").show();                        
                        setTimeout(function(){ 
                            $("#sendingmsgupgrade").hide(); 
                            $("#upgradeinfof").hide();
                            $("#upgrade_submit").show();
                        },1500);
                    }                    
                }
            });            
            event.preventDefault();
        });
        
        $("#paylaterform").submit(function(event){            
            var supid = $('#supid').val();
            var prodid = $('#prodid').val();
            var enqid = $('#enqid').val();
            var supname = $('#latername').val();
            var supposition = $('#laterposition').val();
            $("#sendingmsgpaylater").show();
            $("#paylater_submit").hide();
            $.ajax({
                type: 'POST',
                url: path,
                data: {action: 'new-enquiry-to-paylater', prodid: prodid, supid: supid, enqid: enqid, supname:supname, supposition:supposition},
                success: function(response) {
                    if(response == 1){
                        $("#paylaterinfos").show();                        
                        $("#sentmsgpaylater").show();                       
                        $("#sendingmsgpaylater").hide();
                        setTimeout(function(){                             
                            location.reload();
                        },8000);
                    }else{
                        $("#paylaterinfof").show();
                    }                    
                }
            });            
            event.preventDefault();
        });
    })
</script>
  