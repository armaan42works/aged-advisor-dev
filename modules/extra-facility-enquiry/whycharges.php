<?php
include( "../../application_top.php" );        
?>
<h2 style="color:#e67e22; margin-top:25px">"Why am I being charged"</h2>

<p>Aged Advisor was setup as a social enterprise specifically to help create and administrate a NZ based visiting programme to residents in aged care facilities throughout the country. It's still early days, but we see the difference it is making to those that wouldn't normally get visitors.</p>

<p>Rather than be reliant on grants and trusts for funding (which may be there one year and gone the next), we wanted a self sustaining model that would generate annual funds for our not-for-profit programme. Aged Advisor is part of that model.</p>

<p>Within the last 12 months AgedAdvisor has become one of the highest displayed sites on Google for NZ retirement and aged care searches receiving over 200,000 monthly impressions (Aug 2017) and generating 100's of quality leads each month from people looking for the right facility... your facility.</p>

<p>We simply ask that providers, like you, pay a small amount to either upgrade their listing or to pay for the lead enquiry that we have collected. Yes, there are many ways that facilities can advertise and we understand that Aged Advisor is only one of those options for consideration, which is why we have given you a range of options for you to access an enquirers details.</p>

<p>Aged Advisor is here to help people share their experiences on facilities all over New Zealand so we can all make better informed decisions on where we or a loved one might spend their retirement years - so that we can then help those that are in aged care stay connected to the community.</p>

<p>We hope you will be a part of the journey.</p>

<p>Regards,</p>

<p style="color:#042e6f">Nigel Matthews<br><span>Founder.</span> </p>