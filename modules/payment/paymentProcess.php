<?php
$id = $_SESSION['id'];
$p_id = $_SESSION['proId'];


$sql_queryL = "SELECT member.title, member.price, member.type FROM " . _prefix("users") . " AS user"
        . " Left join " . _prefix("membership_prices") . " AS member ON member.id=user.account_type WHERE user.id='$id'";
$resL = $db->sql_query($sql_queryL);
$accountType = $db->sql_fetchrow($resL);
if ($accountType['type'] == 1) {
    $bus_checked = 'checked';
    $pre_checked = '';
}
if ($accountType['type'] == 2) {
    $pre_checked = 'checked';
    $bus_checked = '';
}
if (isset($submit) && $submit == 'Payment') {
    require_once 'includes/constants.php';
    require_once 'includes/paypal.php';
    $paymentInfo = array('Order' =>
        array(
            'first_name' => $_POST['first_name'],
            'theTotal' => $_POST['amount'],
        ),
        'CreditCard' =>
        array(
            'card_number' => $_POST['card_number'],
            'expiration_month' => $_POST['expiration_month'],
            'expiration_year' => $_POST['expiration_year'],
            'cv_code' => $_POST['cv_code'],
            'credit_type' => $_POST['credit_type'],
        )
    );
    $paypal = new Paypal();
    $result2 = $paypal->DoDirectPayment($paymentInfo);
    //prd($result2);
    //echo $ack = strtoupper($result2["ACK"]);
    //echo $result2["ACK"];
    //echo $result2["ACK"];
    if ($result2["ACK"] != 'Success') {
        $msg = common_message_supplier(0, "Transaction failed.Please try again! " . $result2["L_LONGMESSAGE0"]);
        $_SESSION['msg'] = $msg;

        //ends here
        redirect_to(HOME_PATH . "supplier/payment");
    } else {


        $_SESSION['AccountType'] = $_POST['plan_id'];
        $status = '1';
        $fields = array(
            'user_id' => $_SESSION['id'], //trim($_POST["amount"]),
            'product_id' => $_SESSION['proId'],
            'amount' => $result2["AMT"], //trim($_POST["amount"]),
            'CORRELATIONID' => $result2["CORRELATIONID"], //trim($_POST["amount"]),
            'ACK' => $result2["ACK"], //trim($_POST["amount"]),
            'CURRENCYCODE' => $result2["CURRENCYCODE"], //trim($_POST["amount"]),
            'TRANSACTIONID' => $result2["TRANSACTIONID"], //trim($_POST["amount"]),
            'payment_type' => $_POST["payment_type"], //trim($_POST["amount"]),
            'payment_date' => date('Y/m/d h:i:s', time()),
            'status' => $status
        );
        $insert_result = $db->insert(_prefix('sp_payments'), $fields);

        if ($insert_result) {

            $sql_queryP = "SELECT plan.* FROM " . _prefix("membership_prices") . " AS plan  WHERE plan.id=" . $_POST['plan_id'] . " ";
            $resP = $db->sql_query($sql_queryP);
            $planInfo = $db->sql_fetchrow($resP);

            $plan_month = "+ " . $planInfo['duration'] . " months";
            $start_time = date('Y/m/d h:i:s', time());
            $end_time = date('Y/m/d h:i:s', strtotime($plan_month, time()));
            if (planExist($p_id)) {
                //update current_plan fields of current supplier to 0

                $update_fields = array(
                    'current_plan' => 0
                );
                $where = "where supplier_id= '$id' AND pro_id='$p_id'";
                $update_plan = $db->update(_prefix('pro_plans'), $update_fields, $where);
            }
            $plan_fields = array(
                'supplier_id' => $id,
                'plan_id' => $_POST['plan_id'],
                'pro_id' => $_SESSION['proId'],
                'plan_start_date' => $start_time,
                'plan_end_date' => $end_time,
                'current_plan' => 1
            );

            $to = $_SESSION['userEmail'];
            $transaction_id = $result2["TRANSACTIONID"];
            $transaction_time = date('Y/m/d h:i:s', time());
            $transaction_ammount = $result2["AMT"];
            $username = ucwords($first_name . " " . $last_name);
            $email = emailTemplate('payment_transaction');
            if ($email['subject'] != '') {
                $message = str_replace(array('{name}', '{transaction _id}', '{time}', '{amount}'), array($username, $transaction_id, $transaction_time, $transaction_ammount), $email['description']);
                $send = sendmail($to, $email['subject'], $message);
            }
            $insert_plan = $db->insert(_prefix('pro_plans'), $plan_fields);
            $_SESSION['planId'] = !empty($_POST['plan_id']) ? $_POST['plan_id']:  $data['planID'];
            $msg = common_message_supplier(1, constant('ACCOUNT_TYPE_UPGRATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(HOME_PATH . "supplier/accountInfo");
        }
    }
    //prd($result2);
}
?>
<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success" style="margin-top:10px;">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $error; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>

    </div>
</div>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-money"></i> <span>Payment</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left thumbnail">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="pull_left_bg" style="border:none;">
                                    <h3 class="hedding_h3">Make Payment</h3><hr>
                                    <form class="form-horizontal form_payment" action="" role="payment" method="POST" name='payment' id='payment'>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
                                                <div class="form-group" style="margin-bottom:6px;">
                                                    <label for="name" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Card Type&nbsp;<span class="redCol">*</span></label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <select name="credit_type" class="form-control input_fild_1" >
                                                            <option value="">Select a Card Type</option>
                                                            <option value="MasterCard">Master Card</option>
                                                            <option value="Visa">Visa</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="margin-bottom:10px;">
                                                    <label for="card_number" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Card Number&nbsp;<span class="redCol">*</span></label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <input type="text" name="card_number" class="form-control input_fild_1">
                                                    </div>
                                                </div>
                                                <div class="form-group" style="margin-bottom:10px;">
                                                    <label for="expiration_month" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Expiry (Month/Year)&nbsp;<span class="redCol">*</span></label>
                                                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
                                                        <select name="expiration_month" class="form-control input_fild_1 expiremonth">
                                                            <option value="">Month</option>
                                                            <option value="01">January</option>
                                                            <option value="02">February</option>
                                                            <option value="03">March</option>
                                                            <option value="04">April</option>
                                                            <option value="05">May</option>
                                                            <option value="06">June</option>
                                                            <option value="07">July</option>
                                                            <option value="08">August</option>
                                                            <option value="09">September</option>
                                                            <option value="10">October</option>
                                                            <option value="11">November</option>
                                                            <option value="12">December</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                                        <select name="expiration_year" class="form-control input_fild_1 expireyear" >

                                                            <option value="">Year</option>
                                                            <?php for ($i = date("Y"); $i <= date("Y") + 10; $i++) { ?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group"  style="margin-bottom:10px;">
                                                    <label for="cv_code" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration ">CVV Code&nbsp;<span class="redCol">*</span></label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <input type="text" name="cv_code" class="form-control input_fild_1">
                                                    </div>
                                                </div>
                                                <div class="form-group"  style="margin-bottom:10px;">
                                                    <label for="amount" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Amount :</label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" style="margin-top:25px;font-weight: bold;">
                                                        $ <?php echo $_POST['amount']; ?>
                                                    </div>
                                                    <input type="hidden" name="amount" value="<?php echo $_POST['amount']; ?>">
                                                    <input type="hidden" name="currency_code" value="USD">
                                                    <input type="hidden" name="userid" value="<?php echo $id; ?>">
                                                    <input type="hidden" name="plan_id" value="<?php echo $plan_id; ?>">
                                                    <input type=hidden name='first_name' value='<?php echo $_SESSION['username']; ?>'>
                                                    <input type=hidden name='userEmail' value='<?php echo $_SESSION['userEmail']; ?>'>
                                                    <input type=hidden name='payment_type' value='<?php echo $_POST['payment_type']; ?>'>
                                                    <input type=hidden name='red_page' value='<?php echo $_POST['red_page']; ?>'>
                                                </div>
                                                <div class="form-group text-center total_bg" style="width:100%; padding:2% 0 0 0;">
                                                    <button class="btn btn-danger center-block btn_search btn_pay" style=" padding:6px 20px 6px; margin-bottom:2%;margin-top: 5%;" value="Payment" id="submit" name="submit" type="submit">SUBMIT</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    label.error{
        color:red;
        font-size: 10px;

    }
    input.error{
        color:#000;

    }
    select.error{
        color:#000;

    }
    div.row div.form-group span.redCol{
        color:#FF0000;
    }

</style>

<script>
    $(document).ready(function() {
        setTimeout(function() {

            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);
        $('#payment').validate({
            rules: {
                credit_type: {
                    required: true
                },
                card_number: {
                    required: true,
                    creditcard: true
                },
                expiration_month: {
                    required: true
                },
                expiration_year: {
                    required: true
                },
                cv_code: {
                    required: true,
                    minlength: 3,
                    maxlength: 4,
                    digits: true
                }
            },
            messages: {
                credit_type: {
                    required: "You must select a credit card type"
                },
                card_number: {
                    required: "This field is required"

                },
                expiration_month: {
                    required: " Select Expiry month"
                },
                expiration_year: {
                    required: " Select Expiry year"
                },
                cv_code: {
                    required: "Please enter the CVV code",
                    minlength: "CVV must include minimum three numbers",
                    maxlength: "CVV must include maximum four numbers",
                    digits: "You must enter numbers"
                }

            },
            submitHandler: function(form) {
                $('#submit').attr('disabled', 'disabled');
                form.submit();

            }
        });
    });
    $('.plan').on('change', function() {
        var type = $(this).val();
        $.ajax({
            type: 'POST',
            url: '<?php echo HOME_PATH; ?>ajaxFront.php',
            data: {action: 'plan', type: type},
            success: function(response) {
                $('#durationL').html(response);
                $('#paybleAmount').val('');
            }
        });
    });
    $('.payment').on('change', function() {
        var id = $(this).val();
        $.ajax({
            type: 'POST',
            url: '<?php echo HOME_PATH; ?>ajaxFront.php',
            data: {action: 'paid', id: id},
            success: function(response) {
                $('#paybleAmount').val(response);
            }
        });
    });
    $('.expiremonth').on('change', function() {
        var month = $(this).val();
        var year = $('.expireyear').val();
        var currentMonth = new Date().getMonth() + 1;
        var currentYear = new Date().getFullYear();
        if (year == currentYear && month < currentMonth && year != '')
        {
            $(this).val('');
            alert("Your card has been expired !");
        }

    });
    $('.expireyear').on('change', function() {
        var year = $(this).val();
        var month = $('.expiremonth').val();
        var currentMonth = new Date().getMonth() + 1;
        var currentYear = new Date().getFullYear();
        if (year == currentYear && month < currentMonth && month != '')
        {
            $(this).val('');
            alert("Your card has been expired !");
        }

    });

</script>




