<?php
$id = $_SESSION['id'];
$pr_id = $_SESSION['proId'];
$pr_name = $_SESSION['proName'];
//prd($pr_name);
global $db;
//prd($_SESSION);
$userid = $_SESSION['id'];
$sql_query = "SELECT * FROM " . _prefix("users") . " where  id =$userid";
$res = $db->sql_query($sql_query);
$userData = $db->sql_fetchrow($res);
//prd($userData);
$first_name = $userData['first_name'];
$last_name = $userData['first_name'];
$email = $userData['email'];
$address = $userData['physical_address'];
$city = $userData['address_city'];
//$state = $userData['address_city'];
$countryCode = 'NZ';
$pin = $userData['zipcode'];

function optionDuration($options, $selected = NULL) {
    $select = '';
    $option = '<option value=""> Duration (In months)</option>';
    foreach ($options as $key => $value) {
        if ($selected != null) {
            $select = $selected == $key ? 'selected' : '';
        }
        if ($_SESSION['planId'] >= $key) {
            $option .= '<option value="' . $key . '" ' . $select . ' disabled="disable">' . $value . '</option>';
        } else {
            $option .= '<option value="' . $key . '" ' . $select . '>' . $value . '</option>';
        }
    }
    return $option;
}

function durationList($type = NULL) {
    $durationList = array();
    $sql_query = "select id, duration,title from " . _prefix('membership_prices') . " where status = 1 AND type=" . $_SESSION['planType'] . " ORDER BY id ASC";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    while ($records = mysqli_fetch_assoc($res)) {
        $durationList[$records['id']] = $records['title'] . " (" . $records['duration'] . " months)";
    }
    return optionDuration($durationList);
}

function payableAmount($durationId = null) {
    $sql_query = "select price from " . _prefix('membership_prices') . " where status = 1";
    $res = mysqli_query($db->db_connect_id,$sql_query);
    return $res;
}

$querys = " SELECT member.price , member.title,member.type   FROM " . _prefix("pro_plans") . " AS pln   "
        . " LEFT JOIN " . _prefix("membership_prices") . " AS member ON member.id=pln.plan_id "
        . " WHERE pln.pro_id =" . $pr_id . " AND pln.current_plan = 1 AND pln.status = 1 AND pln.deleted = 0 ORDER BY pln.id DESC";
$planres = $db->sql_query($querys);
$plndata = $db->sql_fetchrow($planres);

if ($plndata['type'] == 1) {
    $bus_checked = 'checked';
    $pre_checked = '';
} else if ($plndata['type'] == 2) {
    $pre_checked = 'checked';
    $bus_checked = '';
} else {
    $bus_checked = 'checked';
}

$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_id = 'nigel@agedadvisor.co.nz'; // Business email ID
?>
<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <?php echo $_SESSION['msg']; ?><br>
            <?php
            unset($_SESSION['msg']);
        }
        ?>

    </div>
</div>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-money"></i> <span>Payment</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left thumbnail">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="pull_left_bg" style="border:none;">
                                    <h3 class="hedding_h3">Upgrade Your Account</h3>
                                    <h4 class="hedding_h4 text-center">Facility Name: <?php echo $pr_name; ?></h4>
                                    <p class="p_text">Currently you are <?php echo!empty($plndata['title']) ? $plndata['title'] : 'Free'; ?> account user</p>
                                    <form class="text-center form_payment" action="<?php echo $paypal_url; ?>" role="form" method="POST" name='choosePlan' id='choosePlan'>
                                        <?php
                                        if ($pre_checked == 'checked') {
                                            
                                        } else {
                                            ?>
                                            <label class="checkbox-inline">
                                                <input type="radio" name="type" class="plan" id="optionsRadios3" value="1"  <?php echo $bus_checked; ?>><span>AGED CARE Upgrade</span>
                                            </label>
                                        <?php } ?>
                                        <label class="checkbox-inline">
                                            <input type="radio" name="type" class="plan" id="optionsRadios4" value="2" <?php echo $pre_checked; ?>><span>RETIREMENT VILLAGE Upgrade</span>
                                        </label>
                                        <div class="row text-center row_control">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                                <select class="payment form-control select_option" name="item_name" id='durationL'>
                                                    <?php echo durationList($supplier_id); ?>
                                                </select>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                                <div class="input-group ">
                                                    <span class="input-group-addon" style="background-color: rgb(250, 250, 250); border: 1px solid rgb(234, 237, 239);">$</span>
                                                    <!--<input type="text" name="price" style="width:400px;" maxlength="10" class="number col-sm-4 required form-control" id="price" value="<?php echo isset($plndata) ? $plndata['price'] : ''; ?>"/>-->
                                                    <input style="margin: 0px; width: 100%;" type="text" name="amount" readonly="readonly" id="paybleAmount" placeholder="Payble Amount" value="" class="input_fild_1">
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="red_page" value="accountInfo">
                                        <input type=hidden name='payment_type' value='1'>

                                        <input type="hidden" name="notify_url" value="<?php echo HOME_PATH . "supplier/notifyPayment"?>">
                                        <input type="hidden" name="business" value="<?php echo $paypal_id; ?>">
                                        <input type="hidden" name="cmd" value="_xclick">
                                        <!--<input type="hidden" name="item_name" value="<?php echo $pr_name; ?>">-->
                                        <input type="hidden" name="item_number" value="<?php echo $pr_id; ?>">
                                        <input type="hidden" name="credits" value="510">
                                        <input type="hidden" name="quantity" value="1">
                                        <input type="hidden" name="userid" value="<?php echo $id; ?>">
                                        <input type="hidden" name="cpp_header_image" value="paypal-test-image.jpg.jpg">
                                        <input type="hidden" name="no_shipping" value="1">
                                        <input type="hidden" name="currency_code" value="NZD">
                                        <input type="hidden" name="handling" value="0">
                                        <input type="hidden" name="cancel_return" value="<?php echo HOME_PATH . "supplier/payment"?>">
                                        <input type="hidden" name="return" value="<?php echo HOME_PATH . "supplier/paymentSuccess"?>">
                                        <!--<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">-->
                                        <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">


                                        <input name="no_note" type="hidden" value="1" />
                                        <input name="lc" type="hidden" value="UK" />
<!--                                        <input name="bn" type="hidden" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />-->
                                        <input name="first_name" type="hidden" value="<?php echo $first_name; ?>" />
                                        <input name="last_name" type="hidden" value="<?php echo $last_name; ?>" />
                                        <input name="address1" type="hidden" value="<?php echo $address; ?>" />
                                        <input name="city" type="hidden" value="<?php echo $city; ?>" />
                                        <input name="country" type="hidden" value="NZ" />
                                        <input name="payer_email" type="hidden" value="<?php echo $email; ?>" />
                                        <input name="zip" type="hidden" value="<?php echo $pin; ?>" />


                                        <button type="submit"   name="submit1" id="submit1" class="btn btn-danger center-block btn_search">Make Payment by Paypal</button>
                                        <span>OR</span>
                                        <button type="submit" value='directCredit' name="directCredit" id="directCredit" class="btn btn-danger center-block btn_search">Direct Credit by Admin Approval</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#paybleAmount').val("");
        $('#directCredit').click(function() {
            $('#choosePlan').attr('action', 'directCredit')
//            $(this).attr('disabled', 'disabled');
        });
        setTimeout(function() {

            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);
        $('#choosePlan').validate({
            rules: {
                plan_id: {
                    required: true
                }
            },
            messages: {
                plan_id: {
                    required: " Select a plan"
                }
            },
            submitHandler: function(form) {
                $('#submit').attr('disabled', 'disabled');
                form.submit();

            }
        });
    });
    $('.plan').on('change', function() {
        var type = $(this).val();
        $.ajax({
            type: 'POST',
            url: '<?php echo HOME_PATH; ?>ajaxFront.php',
            data: {action: 'plan', type: type},
            success: function(response) {
                $('#durationL').html(response);
                $('#paybleAmount').val('');
            }
        });
    });
    $('.payment').on('change', function() {
        var id = $(this).val();
        $.ajax({
            type: 'POST',
            url: '<?php echo HOME_PATH; ?>ajaxFront.php',
            data: {action: 'paid', id: id},
            success: function(response) {
                $('#paybleAmount').val(response);
            }
        });
    });
</script>
<style type="text/css">
    label.error{
        color:red;
        font-size: 10px;
    }
</style>




