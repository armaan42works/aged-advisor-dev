<?php
global $db;
$sql_query = "SELECT * FROM " . _prefix("dc_header");
$res = $db->sql_query($sql_query);
$records = $db->sql_fetchrowset($res);
if (count($records)) {
    foreach ($records as $record) {
        $header = $record['header'];
    }
}
$sp_id = $_SESSION['id'];
$sql_chaeck = "select id from " . _prefix("direct_credit") . " where approve_status=0 AND plan_id='$plan_id' AND  	supplier_id='$sp_id'";
$sgl_run = $db->sql_query($sql_chaeck);
$count = $db->sql_numrows($sgl_run);
if ($count > 0) {
    $msg = common_message(0, "You have already sent Admin approval for this plan, Please wait for admin approval.");
    $_SESSION['msg'] = $msg;
    redirect_to(HOME_PATH . "supplier/payment");
}

if (isset($submit1) && $submit1 == 'Payment') {
    $fields_array = direct_credits();
    $response = Validation::_initialize($fields_array, $_POST);
   
    if (isset($response['valid']) && $response['valid'] > 0) {
       
        $fields = array(
            'supplier_id' => $sp_id,
            'product_id' => $_SESSION['proId'],
            'plan_id' => trim($_POST['plan_id']),
            'amount' => trim($_POST['amount']),
            'order_no' => trim($_POST['order_no']),
            'description' => trim($_POST['description'])
        );
        $insert_result = $db->insert(_prefix('direct_credit'), $fields);
        if ($insert_result) {
            $to = ADMIN_EMAIL;
            $sql_guery = "select CONCAT(IFNULL(user.first_name, ''), ' ', IFNULL(user.last_name, '')) AS supplierName FROM " . _prefix("users") . " AS user where user.id = '$sp_id' ";
            $res = $db->sql_query($sql_guery);
            $user_data = $db->sql_fetchrowset($res);
            $supplier_name = $user_data[0]['supplierName'];
            $email = emailTemplate('request_payment');
            if ($email['subject'] != '') {
                $message = str_replace(array('{supplier_name}'), array($supplier_name), $email['description']);
                $send = sendmail($to, $email['subject'], $message);
                if ($send) {
                    // Message for insert
                    
                }
                $msg = common_message_supplier(1, "Your request has been sent for Admin approval");
                    $_SESSION['msg'] = $msg;
            }
            //ends here
            redirect_to(HOME_PATH . "supplier/payment");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-money"></i> <span>Payment</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left thumbnail">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="pull_left_bg" style="border:none;">
                                    <h3 class="hedding_h3"><?php echo isset($header) ? $header : 'Send to Admin' ?></h3><hr>
                                    <form class="form-horizontal form_payment" action="" role="payment" method="POST" name='direct_credit' id='direct_credit'>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
                                                <div class="form-group" style="margin-bottom:10px;">
                                                    <label for="order_no" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Order No.<span class="redCol">*</span></label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <input type="text"  name="order_no"  class="form-control input_fild_1"/>
                                                    </div>
                                                </div>
                                                <div class="form-group" style="margin-bottom:10px;">
                                                    <label for="description" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Description<span class="redCol">*</span></label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                        <textarea  name="description" id="description" class="form-control input_fild_1"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group"  style="margin-bottom:10px;">
                                                    <label for="amount" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Amount :</label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" style="margin-top:25px;font-weight: bold;">
                                                        $ <?php echo $_POST['amount']; ?>
                                                    </div>
                                                    <input type="hidden" name="amount" value="<?php echo $_POST['amount']; ?>">
                                                    <input type="hidden" name="currency_code" value="USD">
                                                    <input type="hidden" name="userid" value="<?php echo $userid; ?>">
                                                    <input type="hidden" name="plan_id" value="<?php echo $item_name; ?>">
                                                    <input type=hidden name='first_name' value='<?php echo $_SESSION['username']; ?>'>
                                                    <input type=hidden name='userEmail' value='<?php echo $_SESSION['userEmail']; ?>'>
                                                    <input type=hidden name='payment_type' value='<?php echo $_POST['payment_type']; ?>'>
                                                    <input type=hidden name='red_page' value='<?php echo $_POST['red_page']; ?>'>
                                                </div>
                                                <div class="form-group text-center total_bg" style="width:100%; padding:2% 0 0 0;">
                                                    <input type="hidden" name="submit1" value="Payment" />
                                                    <button class="btn btn-danger center-block btn_search btn_pay" style=" padding:6px 20px 6px; margin-bottom:2%;margin-top: 5%;" value="Payment" id="submit2" name="submit2" type="submit">SUBMIT</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    label.error{
        color:red;
        font-size: 10px;

    }
    input.error{
        color:#000;

    }
    select.error{
        color:#000;

    }
    div.row div.form-group span.redCol{
        color:#FF0000;
    }

</style>

<script>
    $(document).ready(function() {

        $('#direct_credit').validate({
            rules: {
                order_no: {
                    required: true,
                    maxlength: 50
                },
                description: {
                    required: true,
                    minlength: 4,
                    maxlength: 500
                }
            },
            messages: {
                order_no: {
                    required: "This field is required",
                    maxlength: "Max 50 chars is allowed"
                },
                description: {
                    required: "This field is required",
                    minlength: "Enter at least 4 chars",
                    maxlength: "Max 500 chars is allowed"


                }

            },
            submitHandler: function(form) {
            $('#submit2').attr('disabled', 'disabled');
            form.submit();
        }

            }
        });
    });




</script>




