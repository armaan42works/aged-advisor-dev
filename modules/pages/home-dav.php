<?php
//ini_set('display_errors', '1');
$ip = $_SERVER['REMOTE_ADDR']; // the IP address to query
$start_event_fetch_date = '';
$monthName = '';
$start_event_fetch_year = '';
$day_dff = '';
$city_name = '';


function cmp($a, $b) {
    //echo $a['id'];
    if ($a['ratingValue'] == $b['ratingValue']) {
        return 0;
    }
    return ($a['ratingValue'] < $b['ratingValue']) ? -1 : 1;
}

function topFiveReview() {
    global $db;
    $resHTML = '<div class="headline"><h4>Top Rated Facilities:</h4></div><ul class="facilities-list home-facility">';
    
    $query = "SELECT (COUNT(fd.product_id)/extf.no_of_beds) as review, prd.id,prd.title,prd.quick_url,prd.address_city,prd.address_suburb,extf.no_of_room,extf.no_of_beds,fd.overall_rating from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id WHERE prd.deleted=0 AND extf.no_of_beds IS NOT NULL GROUP BY fd.product_id HAVING COUNT(fd.product_id) > 10 AND overall_rating > 4  ORDER BY overall_rating DESC  limit 10";
    
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if (count($data) > 0) {
        foreach ($data as $datares) {
            $star_rating = round(simlilarFacilityRating($datares["id"]));
            echo "<style type='text/css'>"
            . ".star-width-".$star_rating
            . "{ width: ".$star_rating."%;}"
            . "</style>";
            
            if (isset($datares['address_suburb']) &&!empty($datares['address_suburb'])) {
                $addr_sububrb_htm = '<span class="text-cornflowerblue">' . $datares['address_suburb'] . ',</span>';
            } else {
                $addr_sububrb_htm = '';
            }
            
            if (isset($datares['address_city']) &&!empty($datares['address_city'])) {
                $addr_city_htm = '<span class="text-cornflowerblue">' . $datares['address_city'] . '</span>';
            } else {
                $addr_city_htm = '';
            }
            
            $resHTML .= '<li><a href="' . $datares["quick_url"] . '"><span class="prcnt-icon">' . $star_rating . '%</span>' . $datares["title"] . '<div class="text-14">' . $addr_sububrb_htm . '' . $addr_city_htm . '</div><div class="star-inactive"><div class="star-active star-width-' . $star_rating . '" ></div></div></a></li>';
        }
    } else {
        //echo "We have more than 5 records with more than five review";
        $query = "SELECT (COUNT(fd.product_id)/extf.no_of_beds) as review, prd.id,prd.title,prd.quick_url,extf.no_of_room,extf.no_of_beds from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id WHERE extf.no_of_beds IS NOT NULL AND  prd.deleted=0 AND extf.no_of_beds <> 0 GROUP BY fd.product_id ORDER BY review HAVING COUNT(fd.product_id) > 5 DESC LIMIT 9 ";
        $res = $db->sql_query($query);
        $data = $db->sql_fetchrowset($res);
        //echo count($data).'awa';
        if (count($data) > 0) {
            foreach ($data as $datares) {   
                $resHTML .= '<li><i class="fa padding-right-10 fa-angle-right"></i><a href="' . $datares["quick_url"] . '">' . $datares["title"] . '</a></li>';
            }
        } else {
            //echo "we are going  to check the we have more than who have review and number of room";
            $query = "SELECT (COUNT(fd.product_id)/extf.no_of_room) as review, prd.id,prd.title,prd.quick_url,extf.no_of_room,extf.no_of_beds from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id WHERE extf.no_of_room IS NOT NULL OR extf.no_of_room <> 0 GROUP BY fd.product_id  ORDER BY review DESC LIMIT 9";
            $res = $db->sql_query($query);
            $data = $db->sql_fetchrowset($res);
            if (count($data) >= 0) {
                foreach ($data as $datares) {
                    $resHTML .= '<li><i class="fa padding-right-10 fa-angle-right"></i><a href="' . $datares["quick_url"] . '">' . $datares["title"] . '</a></li>';
                }
            } else {
                //echo "we are going to get it through stars";
                $query = "SELECT AVG(fd.overall_rating) as review, prd.id,prd.title,prd.quick_url,extf.no_of_room,extf.no_of_beds from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id GROUP BY fd.product_id ORDER BY review DESC LIMIT 9 ";
                $res = $db->sql_query($query);
                $data = $db->sql_fetchrowset($res);
                if (count($data) >= 0) {
                    foreach ($data as $datares) {
                    $resHTML .= '<li><i class="fa padding-right-10 fa-angle-right"></i><a href="' . $datares["quick_url"] . '">' . $datares["title"] . '</a></li>';
                    }
                }
            }
        }
    }

    $resHTML .= '</ul><hr><div class="viewall">
	&lt;&lt;<a href="/supplier/ranking">Click Here to Compare National Providers</a>&gt;&gt;
    </div><hr>';
    echo $resHTML;
}

function getWeekReviews() {
    global $db;
    $query = "SELECT fdbk.* , pds.title as pdtitle,pds.id as prId,pds.id AS pImage, sp.user_name as spUsername, sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername, cs.first_name AS csFName, cs.last_name AS csLName ,pds.quick_url FROM " . _prefix("feedbacks") . " AS fdbk   "
    . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
    . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
    . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
    . "WHERE fdbk.abused_request=0 AND fdbk.abused=0 AND fdbk.week_review=1 AND fdbk.status=1 AND fdbk.deleted=0  ORDER BY fdbk.id DESC LIMIT 4";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res); //echo '<pre>',print_r($data),'</pre>';die;

    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
    . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    //print_r($dataRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSumation = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';


    foreach ($data as $key => $record) {
        if ($record['feedback'] != '') {
            if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) {
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']) . '" alt="product image" /></a>';
            } else {
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'images/no_image_available.png" alt="" /></a>';
            }
            $TotalRating = $record['overall_rating'];

            $totratingpercent = $TotalRating * 20;
            $str = '<div class="star-inactive"><div class="star-active" style="width:' . $totratingpercent . '%;"></div></div>';
            $overallRating = round($TotalRating);
            switch ($overallRating) {
                case 1:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $all_Rating = 'Not Rated';
                    break;
            }
            $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
            // $feedback = (strlen($record['feedback']) > 60) ? substr($record['feedback'], 0, 60) . '...' : $record['feedback'];
            $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
            $reviews .= '<div class="col-md-2 col-sm-2 col-xs-12 review_thumb">' . $image . '</div>
                            <div class="col-md-10 col-sm-10 col-xs-12 pdding_right padding_left_col">
                                    <h3 class="hedding_three"><a href="' . HOME_PATH . $record['quick_url'] . '#reviews">' . $record['title'] . '</a></h3>
                                    <p class="m-0">' . $str . '</p>
                                    <p><span class="dark-text">Product/Service Name:&nbsp;<a href="' . HOME_PATH . $record['quick_url'] . '">' . substr($record['pdtitle'], 0, 60) . '</a><br/>
                                            <span class="dark-text">Commented By:&nbsp;' . $commenter . '</span>';
            if ($record['feedback_reply']) {
                $reviews .= '<br><span class="dark-text">Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</span>';
            }

            $timeDiff = (strtotime(date("Y-m-d h:i:sa")) - strtotime($record['week_review_time'])) / (60 * 60 * 24);
            $reviews .= '</p><p class="margin_bottom">' . $pros . '</p></div><div class="espesar"></div>';
        }
    }
    //     usort($data, "cmp");
    if ($timeDiff > 7)
        $reviews = '';
    
    return $reviews;
}

function getReviews() {
    global $db;
    $query = "SELECT fdbk.* , pds.title as pdtitle,pds.id as prId,pds.id AS pImage, sp.user_name as spUsername, sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername, cs.first_name AS csFName, cs.last_name AS csLName ,pds.quick_url,  pds.address_city FROM " . _prefix("feedbacks") . " AS fdbk   "
    . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
    . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
    . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
    . "WHERE fdbk.abused_request=0 AND fdbk.abused=0 AND fdbk.week_review=0 AND fdbk.status=1 AND fdbk.deleted=0  ORDER BY fdbk.id DESC LIMIT 14";

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res); //echo '<pre>',print_r($data),'</pre>';die;
    
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
    . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSumation = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';

    foreach ($data as $key => $record) {
        $know_me_id = $record['know_me'];

        if ($know_me_id == 1) {
            $know_me_title = "Resident";
        }
        if ($know_me_id == 2) {
            $know_me_title = "Family member / Friend";
        }
        if ($know_me_id == 3) {
            $know_me_title = "Staff Member";
        }
        if ($know_me_id == 4) {
            $know_me_title = "Visitor";
        }
        if ($know_me_id == 0 || $know_me_id > 4) {
            $know_me_title = "Unspecified";
        }
        if ($record['visit_duration'] == 1) {
            $visit_duration = "< 10 days / visits";
        }
        if ($record['visit_duration'] == 2) {
            $visit_duration = "10 - 100 days / visits";
        }
        if ($record['visit_duration'] == 3) {
            $visit_duration = "> 100 days / visits";
        }
        if ($record['visit_duration'] == 0 || $record['visit_duration'] > 3) {
            $visit_duration = "Unspecified";
        }
        $last_visit = $record['last_visit'];

        $feedback_created = $record['created'];
        $feedback_date = date("Y-m-d", strtotime($feedback_created));
        $feedback_fetch_date = date("d", strtotime($feedback_date));
        $feedback_fetch_month = date("m", strtotime($feedback_date));
        $feedback_monthName = date("M", mktime(0, 0, 0, $feedback_fetch_month, 10));
        $feedback_fetch_year = date("Y", strtotime($feedback_date));
        
        if ($record['feedback'] != '') {
             
            if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) {
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']) . '" alt="product image" /></a>';
            } else {
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'images/no_image_available.png" alt="" /></a>';
            }
           
            $TotalRating = $record['overall_rating'];
            $totratingpercent = $TotalRating * 20;
            echo "<style type='text/css'>"
            . ".star-width-".$totratingpercent
            . "{ width:".$totratingpercent."%;"
            . "} </style>";
            
            $str = '<div class="star-inactive"><div class="star-active star-width-' . $totratingpercent . '"></div></div>';
            $overallRating = round($TotalRating);
            
            
            $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
            // $feedback = (strlen($record['feedback']) > 60) ? substr($record['feedback'], 0, 60) . '...' : $record['feedback'];
            $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
            $reviews .= '<div itemscope itemtype="http://schema.org/Product"><meta itemprop="name" content = "'.$record['pdtitle'].'"/><meta itemprop="url" content = "' . HOME_PATH . $record['quick_url'] . '"/>
			<div itemprop="review" itemscope itemtype="http://schema.org/Review"><div class="review-content"><div class="col-md-2 no-space"><p class="review-by">Reviewed by : <span itemprop="author">' . $commenter . '</span><br><meta itemprop="datePublished" content="'.$feedback_date.'">' . $feedback_monthName . ' ' . $feedback_fetch_year . '<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                        <meta itemprop="worstRating" content = "1"/>
                        <meta itemprop="ratingValue" content = "'.$overallRating.'"/>
                        <meta itemprop="bestRating" content = "5"/>
                        </div></p><ul class="review-img-list m-hide">    <li>' . $know_me_title . '</li><li>' . $visit_duration . '</li></ul></div>
                        <div class="col-xs-10 m-widthfull pdding_right padding_left_col">
                        <h3 class="hedding_three"><a href="' . HOME_PATH . $record['quick_url'] . '#reviews"><span itemprop="name">' . $record['title'] . '</span></a></h3>
                        <p class="m-0">' . $str . '</p>
                        <p><span class="dark-text">Facility Name:&nbsp;<a href="' . HOME_PATH . $record['quick_url'] . '">' . substr($record['pdtitle'], 0, 60) . ', ' . $record['address_city'] . '</a><br/>';
            if ($record['feedback_reply']) {
                $reviews .= '<br><span class="dark-text">Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</span>';
            }


            $reviews .= '</p><p class="margin_bottom"><span itemprop="description">' . $pros . '</span></p></div></div></div></div>';
        }
    }
    return $reviews;
}

/* * *********************************end  of  code for get right banner according city name of current user************* */
?>
<style>.viewall{margin-top:20px}.review_thumb{margin-bottom:2%}.m-0{margin:0}.dark-text{color:#8a8a8a}.display-inline{display:inline}.loading-hide{display:none}.noresult{text-align:center;color:#F30}.view-all{color:#000;text-decoration:underline}.right-banner-cover{margin:20px 0 25px;padding-left:0}.mt-20{margin-top:20px}.text-cornflowerblue{color:cornflowerblue}.text-14{font-size:14px}</style>
<div class="container">
    <div itemscope="" itemtype="https://schema.org/AggregateRating">
        <meta itemprop="reviewCount" content="2450">
        <meta itemprop="ratingValue" content="4.8">
        <meta itemprop="bestRating" content="5">
        <meta itemprop="worstRating" content="1">
        <meta itemprop="itemReviewed" content="www.agedadvisor.nz">
    </div>
    <script type="application/ld+json">
        {
        "@context": "http://schema.org",
        "@type": "Organization",
        "url": "http://localhost/aged",
        "logo": "http://localhost/images/find_agedadvisor_retirement_villages_agedcare_logo.png"
        }
    </script>
    <div class="row">
        <div class="col-md-8 col-lg-8 col-sm-12 col-xs-12 pull-left">
            <div class="row">
                <div class="col-sm-12">
                    <?php include_once('modules/competition/view-competitions-home-page-include.php'); ?>                    
                    <div id="loading" class="loading-hide hide">
                        <img src="ring.gif" alt="loading">
                    </div>
                    <div class="headline hide">
                        <h2>Upcoming Events-</h2>
                    </div>
                    <div id="noresult" class="loading-hide hide">
                        <p class="noresult">No Events Found!</p>
                    </div>
                    <div class="event-blog-container hide" id="event-container">
                        <div class="event-blog-list-box design-2">
                            <div class="event-blog-date-box">
                                <div class="event-blog-date">
                                    <p><?php echo $start_event_fetch_date; ?> </p>
                                </div>
                                <div class="event-blog-date-month">
                                    <p><?php echo $monthName; ?> <span><?php echo $start_event_fetch_year; ?></span></p>
                                </div>
                            </div>
                            <div class="event-blog-cont-box">
                                <div class="event-blog-cont-box-in">
                                    <ul class="event-list">
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="show-more" id="show-more">
                            <span><a href="<?php //echo HOME_PATH;    ?>events" target="_blank">Future Events</a></span>
                        </div>
                    </div>
                    <!--***************event blog container close here**************-->
                    <!--************************div message for no upcoming events*************-->
                    <div class="shortlist-message hide" <?php if ($day_dff > 2) { ?> style="display:none" <?php } else { ?> style="display:block" <?php } ?> >There are currently no up-coming events listed. Check back again later.</div>
                    <!--*********************end of  div message for no upcoming events*************-->
                </div>
            </div>
            <div class="row">
                <div id="reviewList" class="col-md-12 col-sm-12 col-xs-12 padding_left new_speceing">
                    <div class="headline">
                        <h2>Latest Reviews:</h2>
                    </div>
                    <?php echo getReviews(); ?>
                    <div class="espesar text-center m-0"><a class="view-all" href="<?php echo HOME_PATH; ?>reviews">View All</a></div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12 pull-right padding_right">
            <div class="col-md-12 col-sm-12 col-xs-12 text-left home-right-banner">
                <?php echo getRightBannerNEW1($city_name); ?>
            </div>
            <div class="right-photo-gallery mt-50" id="topFacilities">
			<?php topFiveReview(); ?>
			</div>
            <h2 class="hedding_tow">Articles:</h2>
            <ul class="list-unstyled right_nav">
                <?php echo getArticles(); ?>
                <div class="viewall"><a href="<?php echo HOME_PATH; ?>articles">View All</a></div>
            </ul>
            <h2 class="hedding_tow">Consumer Information:</h2>
            <ul class="list-unstyled right_nav">
                <?php echo getConsumerInfo(); ?>
                <div class="viewall"><a href="<?php echo HOME_PATH; ?>consumer-information">View All</a></div>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12 text-center"><p>&nbsp;</p><div class="well well-sm">
        <p>Know someone that would like to place a review but doesn't have access to a computer?<br><a href="https://www.agedadvisor.nz/images/agedadvisor-Review-Sheet-A4-PRINT.pdf">Download a printable FREEPOST form here</a></p></div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="headline">Associations we are involved with <a href="<?php echo  HOME_PATH ?>community?#community-frm" target="_blank">To Get involved Click Here</a></h1>
        </div>
    </div>
</div>
<script type="text/javascript">
    /*<![CDATA[*/$(document).ready(function(a){$("#sRegion, #sPtype").on("change", function(){$("#loading").show(); $("#noresult").hide(); if ($(this).attr("id") == "sRegion"){var c = $(this).val(); var b = $("#sPtype").val()} else{var b = $(this).val(); var c = $("#sRegion").val()}if (c != "NA" || b != "NA"){addtocalendar.load(); $.ajax({type:"POST", url:"<?php echo  HOME_PATH ?>modules/filter/filter_home.php", data:{pType:b, region:c}, dataType:"JSON", success:function(d){var e = d.events; var f = d.topFac; if (e == "NA"){$("#noresult").show(); $("#loading").hide(); $("#topFacilities").html(f); setTimeout(function(){addtocalendar.load()}, 5000)} else{$("#event-container").html(e); setTimeout(function(){addtocalendar.load(); $("#loading").hide()}, 2000)}if (f == "NA"){$("#loading").hide(); $("#topFacilities").html('<div class="headline"><h4>Top Rated Facilities:</h4></div><p class="noresult">No Facilities Found!</p><hr><div class="viewall mt-20"><a href="/supplier/ranking">Click Here to Compare National Providers</a></div><hr>')} else{$("#topFacilities").html(f)}}, error:function(e, d, f){alert("Status: " + d); alert("Error: " + f); $("#loading").hide()}})} else{$("#loading").hide(); setTimeout(function(){addtocalendar.load()}, 5000)}})}); /*]]>*/
</script>
	<style>
	.star-inactive {
    background-image: url(https://www.agedadvisor.nz//images/starbg.png);
    background-repeat: no-repeat;
    background-size: 91px;
    background-position: 0 0;
    display: inline-block;
    height: 20px;
    width: 92px;
}

.star-active {
    background: url(https://www.agedadvisor.nz//images/starbg.png) 0 -21px no-repeat;
    background-size: 91px;
    height: 20px;
    width: 50%;
}

.coupon-content {
 
    min-height: 338px;
}
	</style>
<script type="text/javascript">
    <?php
    // To prevent error for unknown vars futer_event_row AND    row.
    $futer_event_row = 0;
    $row = 0;
    ?>
    $(document).ready(function(){
        var next_event = '<?php echo $futer_event_row; ?>';
        var check_event = '<?php echo $row; ?>';
        if (check_event > 0)
        {
            $("#event-container").show(200);
            $(".shortlist-message").hide(200);
            $("#show-more_second").hide(200);
        } else {
            if (check_event == 0)
            {
                $(".shortlist-message").show(200);
                $("#event-container").hide(200);
            }
        }
    });
</script>
