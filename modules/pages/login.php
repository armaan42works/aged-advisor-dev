<?php
    /*
     * Objective : Login Panel for User
     * Filename : login.php
     * Created By : Sanket Khanna<sanket.khanna@ilmp-tech.com>
     * Created On : 20 August 2014
     * Modified : 22 August 2014
     */
    global $db;
?>
<section id="slider_container">
    <div class="slider_bg">
        <ul class="slider">
            <li><img src="<?php echo HOME_PATH; ?>images/slider_img.png" alt=""></li>
        </ul>

    </div>
</section>

<section id="body_container">
    <div class="wrapper">

        <div class="main_box">
            <!----left container STARTS------>
            <div class="left_container">
                <div class="login_container">
                    <?php
                       if (isset($button) && ($button == 'Submit Query' || $button = 'Submit')) {
                            $sql_query = "SELECT * FROM "._prefix('users')." WHERE email = '$user_id' && (password = '$password' || password = MD5('$password')) && user_type = '$user_type'";
                            $res = $db->sql_query($sql_query);
                            $num = $db->sql_numrows($res);
                            if ($num > 0) {
                                $data = $db->sql_fetchrow($res);
                                if ($data['status'] == 0) {
                                    $msg = common_message(0, constant('INVALID'));
                                    $_SESSION['msg'] = $msg;
                                } else if ($data['deleted'] == 1) {
                                    $msg = common_message(0, constant('DEACTIVE'));
                                    $_SESSION['msg'] = $msg;
                                } else if ($data['validate'] == 0) {
                                    $msg = common_message(0, constant('NOT_VALIDATE'));
                                    $_SESSION['msg'] = $msg;
                                }
                                else {
                                    if ($data['validate'] == 1) {
                                        $url = HOME_PATH . 'user/registration/'.md5($data['id']);
                                    } else {
                                       if($_POST['checkbox']== 'on'){
                                            $year = time() + 31536000;
                                            echo $email;
                                            setcookie('remember_me', $user_id, $year);
                                        } else {
                                            if(isset($_COOKIE['remember_me'])) {
		                             $past = time() - 100;
		                          setcookie('remember_me', '', $past);
	}
                                        }
                                        
                                    $_SESSION['userId'] = $data['id'];
                                    $_SESSION['name'] = $data['name'];
                                    $_SESSION['userType'] = $user_type;
                                    $url = HOME_PATH . 'user/dashboard';
                                    }
                                    redirect_to($url);
                                }
                            } else {
                                $msg = common_message(0, constant('INVALID'));
                                $_SESSION['msg'] = $msg;
                            }
                        }
                    ?>
                    <h3>Welcome back, sign in to your account here!</h3>
                    <form id="loginForm" name="loginForm" method="post" class="login_account">

                        <div><?php
                                if (isset($_SESSION['msg'])) {
                                    echo $_SESSION['msg'];
                                    unset($_SESSION['msg']);
                                }
                            ?></div>
                        <div class="col">
                            <div class="col_small col_small_new"><input type="radio" name="user_type" id="client"  class="type input_text" value="0" checked="checked"><label for="client">Login as Client</label></div>
                            <div class="col_small col_small_new"><input type="radio" name="user_type" id="sp"  class="type input_text" value="1"><label for="sp">Login as Sales Professional</label></div>
                        </div>
                        <div class="col">
                            <input type="text" name="user_id" id="user_id" class="input_login required" placeholder="Email Address" value="<?php echo $_COOKIE['remember_me']; ?>">
                        </div>
                        <div class="col">
                            <input type="password" name="password" id="password" class="input_login required" placeholder="Password">
                        </div>
                        <div class="col">
                            <span class="forgot_text">
                                <a href="<?php echo HOME_PATH ?>forgetPassword">I forgot my Password</a><br>
                            </span>
                            <span class="remember">
                                <label for="checkbox" class="remember_me">Remember Me </label>
                                <input type="checkbox" name="checkbox" id="checkbox" class="checkbox" <?php echo isset($_COOKIE['remember_me']) ? "checked = 'checked'"  : '';?>>
                            </span>
                        </div>
                        <div class="col">
                            <input type="submit" name="button" id="button"  class="login_btn" onclick="javascript:this.loginForm.submit();">
                        </div>

                    </form>
                    <div class="sign_area">
                        <div class="col">
                            <a href="facebook_login" class="sign_pic"><img src="images/signin_facebook.png" alt=""></a>
                        </div>
                        <div class="col">
                            <a href="login-twitter.php" class="sign_pic"><img src="images/signin_twitter.png" alt=""></a>
                        </div>

                    </div>
                </div>

            </div>
            <!----right container STARTS------>
            <div class="right_container">
                <div class="about_col none signup_login willget_new">
                    <h1 style="margin-top:0px;">You don't have a client account with us ?</h1>
                    <a href="<?php echo HOME_PATH; ?>register" class="creat_account_btn">Create your free account now!</a>  

                    <h1 style="margin-top:0px;">Would you like to join as sales professional?</h1>
                    <a href="<?php echo HOME_PATH; ?>career" class="creat_account_btn">Checkout our current openings</a>  






                </div>




            </div>

        </div>
    </div>
</div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('#loginForm').validate();
    });


</script>
