 <?php
//ini_set('display_errors', '1');
$ip = $_SERVER['REMOTE_ADDR']; // the IP address to query
$start_event_fetch_date = '';
$monthName = '';
$start_event_fetch_year = '';
$day_dff = '';
$city_name = '';


function cmp($a, $b) {
    //echo $a['id'];
    if ($a['ratingValue'] == $b['ratingValue']) {
        return 0;
    }
    return ($a['ratingValue'] < $b['ratingValue']) ? -1 : 1;
}

function topFiveReview() {
    global $db;

    
    $query = "SELECT (COUNT(fd.product_id)/extf.no_of_beds) as review, prd.id,prd.title,prd.quick_url,prd.address_city,prd.address_suburb,extf.no_of_room,extf.no_of_beds,fd.overall_rating from ad_products as prd 
	LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id 
	LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id WHERE prd.deleted=0 AND extf.no_of_beds IS NOT NULL GROUP BY fd.product_id HAVING COUNT(fd.product_id) > 10 AND overall_rating > 4  ORDER BY overall_rating DESC  limit 10";
    
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    if (count($data) > 0) 
	{
        foreach ($data as $datares) {
            $star_rating = round(simlilarFacilityRating($datares["id"]));
            echo "<style type='text/css'>"
            . ".star-width-".$star_rating
            . "{ width: ".$star_rating."%;}"
            . "</style>";
            
            if (isset($datares['address_suburb']) &&!empty($datares['address_suburb'])) 
			{
                $addr_sububrb_htm =  $datares['address_suburb'];
            } else {
                $addr_sububrb_htm = '';
            }
            
            if (isset($datares['address_city']) &&!empty($datares['address_city'])) {
                $addr_city_htm = $datares['address_city'];
            } else {
                $addr_city_htm = '';
            }
            
            $resHTML .= '<div class="swiper-slide trending-place-item">
                                    <div class="trending-img">
                                        <img src="https://www.agedadvisor.nz/admin/files/gallery/images/retirement-village-Princess-Alexandra-Retirement-Village-Ahuriri-Napier.jpg" alt="#">
                                        <span class="trending-rating-orange">' . $star_rating . '%</span>
                                        <span class="save-btn"><i class="icofont-heart"></i></span>
                                    </div>
                                    <div class="trending-title-box">
                                        <h4><a href="' . $datares["quick_url"] . '">' . ucwords(strtolower($datares["title"])) . '</a></h4>
                                        <div class="customer-review">
                                            <div class="rating-summary float-left">
                                                <div class="rating-result" title="' . $star_rating . '%">
                                                    <div class="star-inactive">
													<div style="width:'.$star_rating.'%;"
													
													class="star-active" >
													</div>
													</div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <ul class="trending-address">
                                            <li><i class="ion-ios-location"></i>
                                                <p>'.$addr_sububrb_htm.'</p>
                                            </li>
                                            <li><i class="ion-ios-location"></i>
                                                <p>'.$addr_city_htm.'</p>
                                            </li>
                                           
                                        </ul>
                                       
                                    </div>
                                </div>';
        }
    } else {
        //echo "We have more than 5 records with more than five review";
        $query = "SELECT (COUNT(fd.product_id)/extf.no_of_beds) as review, prd.id,prd.title,prd.quick_url,extf.no_of_room,extf.no_of_beds from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id WHERE extf.no_of_beds IS NOT NULL AND  prd.deleted=0 AND extf.no_of_beds <> 0 GROUP BY fd.product_id ORDER BY review HAVING COUNT(fd.product_id) > 5 DESC LIMIT 9 ";
        $res = $db->sql_query($query);
        $data = $db->sql_fetchrowset($res);
        //echo count($data).'awa';
        if (count($data) > 0) {
            foreach ($data as $datares) {   
                $resHTML .= '<li><i class="fa padding-right-10 fa-angle-right"></i><a href="' . $datares["quick_url"] . '">' . $datares["title"] . '</a></li>';
            }
        } else {
            //echo "we are going  to check the we have more than who have review and number of room";
            $query = "SELECT (COUNT(fd.product_id)/extf.no_of_room) as review, prd.id,prd.title,prd.quick_url,extf.no_of_room,extf.no_of_beds from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id WHERE extf.no_of_room IS NOT NULL OR extf.no_of_room <> 0 GROUP BY fd.product_id  ORDER BY review DESC LIMIT 9";
            $res = $db->sql_query($query);
            $data = $db->sql_fetchrowset($res);
            if (count($data) >= 0) {
                foreach ($data as $datares) {
                    $resHTML .= '<li><i class="fa padding-right-10 fa-angle-right"></i><a href="' . $datares["quick_url"] . '">' . $datares["title"] . '</a></li>';
                }
            } else {
                //echo "we are going to get it through stars";
                $query = "SELECT AVG(fd.overall_rating) as review, prd.id,prd.title,prd.quick_url,extf.no_of_room,extf.no_of_beds from ad_products as prd LEFT JOIN ad_feedbacks as fd ON fd.product_id = prd.id LEFT JOIN ad_extra_facility as extf ON prd.id=extf.product_id GROUP BY fd.product_id ORDER BY review DESC LIMIT 9 ";
                $res = $db->sql_query($query);
                $data = $db->sql_fetchrowset($res);
                if (count($data) >= 0) {
                    foreach ($data as $datares) {
                    $resHTML .= '<li><i class="fa padding-right-10 fa-angle-right"></i><a href="' . $datares["quick_url"] . '">' . $datares["title"] . '</a></li>';
                    }
                }
            }
        }
    }

    $resHTML .= '</ul><hr><div class="viewall">
	&lt;&lt;<a href="/supplier/ranking">Click Here to Compare National Providers</a>&gt;&gt;
    </div><hr>';
    echo $resHTML;
}

function getWeekReviews() {
    global $db;
    $query = "SELECT fdbk.* , pds.title as pdtitle,pds.id as prId,pds.id AS pImage, sp.user_name as spUsername, sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername, cs.first_name AS csFName, cs.last_name AS csLName ,pds.quick_url FROM " . _prefix("feedbacks") . " AS fdbk   "
    . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
    . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
    . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
    . "WHERE fdbk.abused_request=0 AND fdbk.abused=0 AND fdbk.week_review=1 AND fdbk.status=1 AND fdbk.deleted=0  ORDER BY fdbk.id DESC LIMIT 4";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res); //echo '<pre>',print_r($data),'</pre>';die;

    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
    . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    //print_r($dataRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSumation = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';


    foreach ($data as $key => $record) {
        if ($record['feedback'] != '') {
            if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) {
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']) . '" alt="product image" /></a>';
            } else {
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'images/no_image_available.png" alt="" /></a>';
            }
            $TotalRating = $record['overall_rating'];

            $totratingpercent = $TotalRating * 20;
            $str = '<div class="star-inactive"><div class="star-active" style="width:' . $totratingpercent . '%;"></div></div>';
            $overallRating = round($TotalRating);
            switch ($overallRating) {
                case 1:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 2:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 3:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>';
                    break;
                case 4:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i>';
                    break;
                case 5:
                    $all_Rating = '<i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i>';
                    break;
                default:
                    $all_Rating = 'Not Rated';
                    break;
            }
            $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
            // $feedback = (strlen($record['feedback']) > 60) ? substr($record['feedback'], 0, 60) . '...' : $record['feedback'];
            $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
            $reviews .= '<div class="col-md-2 col-sm-2 col-xs-12 review_thumb">' . $image . '</div>
                            <div class="col-md-10 col-sm-10 col-xs-12 pdding_right padding_left_col">
                                    <h3 class="hedding_three"><a href="' . HOME_PATH . $record['quick_url'] . '#reviews">' . $record['title'] . '</a></h3>
                                    <p class="m-0">' . $str . '</p>
                                    <p><span class="dark-text">Product/Service Name:&nbsp;<a href="' . HOME_PATH . $record['quick_url'] . '">' . substr($record['pdtitle'], 0, 60) . '</a><br/>
                                            <span class="dark-text">Commented By:&nbsp;' . $commenter . '</span>';
            if ($record['feedback_reply']) {
                $reviews .= '<br><span class="dark-text">Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</span>';
            }

            $timeDiff = (strtotime(date("Y-m-d h:i:sa")) - strtotime($record['week_review_time'])) / (60 * 60 * 24);
            $reviews .= '</p><p class="margin_bottom">' . $pros . '</p></div><div class="espesar"></div>';
        }
    }
    //     usort($data, "cmp");
    if ($timeDiff > 7)
        $reviews = '';
    
    return $reviews;
}

function getReviews() 
{
    global $db;
    $query = "SELECT fdbk.* , pds.title as pdtitle,pds.id as prId,pds.id AS pImage, sp.user_name as spUsername, sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername, cs.first_name AS csFName, cs.last_name AS csLName ,pds.quick_url,  pds.address_city FROM " . _prefix("feedbacks") . " AS fdbk   "
    . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
    . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
    . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
    . "WHERE fdbk.abused_request=0 AND fdbk.abused=0 AND fdbk.week_review=0 AND fdbk.status=1 AND fdbk.deleted=0  ORDER BY fdbk.id DESC LIMIT 14";

    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res); //echo '<pre>',print_r($data),'</pre>';die;
    
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
    . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSumation = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';

    foreach ($data as $key => $record) {
        $know_me_id = $record['know_me'];

        if ($know_me_id == 1) {
            $know_me_title = "Resident";
        }
        if ($know_me_id == 2) {
            $know_me_title = "Family member / Friend";
        }
        if ($know_me_id == 3) {
            $know_me_title = "Staff Member";
        }
        if ($know_me_id == 4) {
            $know_me_title = "Visitor";
        }
        if ($know_me_id == 0 || $know_me_id > 4) {
            $know_me_title = "Unspecified";
        }
        if ($record['visit_duration'] == 1) {
            $visit_duration = "< 10 days / visits";
        }
        if ($record['visit_duration'] == 2) {
            $visit_duration = "10 - 100 days / visits";
        }
        if ($record['visit_duration'] == 3) {
            $visit_duration = "> 100 days / visits";
        }
        if ($record['visit_duration'] == 0 || $record['visit_duration'] > 3) {
            $visit_duration = "Unspecified";
        }
        $last_visit = $record['last_visit'];

        $feedback_created = $record['created'];
        $feedback_date = date("Y-m-d", strtotime($feedback_created));
        $feedback_fetch_date = date("d", strtotime($feedback_date));
        $feedback_fetch_month = date("m", strtotime($feedback_date));
        $feedback_monthName = date("M", mktime(0, 0, 0, $feedback_fetch_month, 10));
        $feedback_fetch_year = date("Y", strtotime($feedback_date));
        
        if ($record['feedback'] != '') 
		{
             
            if (@getimagesize(DOCUMENT_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']))) 
			{
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'admin/files/gallery/images/thumb_feedback_' . get_first_image_from_gallery($record['pImage']) . '" alt="product image" />
				</a>';
            } else {
                $image = '<a href="' . HOME_PATH . $record['quick_url'] . '"><img src="' . HOME_PATH . 'images/no_image_available.png" alt="" /></a>';
            }
           
            $TotalRating = $record['overall_rating'];
            $totratingpercent = $TotalRating * 20;
            
            echo "<style type='text/css'>"
            . ".star-width-".$totratingpercent
            . "{ width:".$totratingpercent."%;"
            . "} </style>";
            
            $str = '<div class="star-inactive">
			<div class="star-active star-width-' . $totratingpercent . '">
			</div>
			</div>';
            $overallRating = round($TotalRating);
             
            $commenter = ($record['anonymous'] == 1) ? '[Name withheld at users request]' : $record['csUsername'];
            // $feedback = (strlen($record['feedback']) > 60) ? substr($record['feedback'], 0, 60) . '...' : $record['feedback'];
            $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
            $reviews .= ' 
			<div itemscope itemtype="http://schema.org/Product" class="swiper-slide coupon-content">
             <meta itemprop="name" content = "'.$record['pdtitle'].'"/>
              <meta itemprop="url" content = "' . HOME_PATH . $record['quick_url'] . '"/>
               <div  itemprop="review" itemscope itemtype="http://schema.org/Review">
                                    <div class="row" >
                                         
                                        <div class="col-md-12">
										<div class="coupon-owner">
                                               <h3 class="hedding_three">
		<a href="' . HOME_PATH . $record['quick_url'] . '#reviews">
		<span itemprop="name">' . $record['title'] . '</span>
		</a></h3>
		
		<p style="width:"'.$totratingpercent.'"%;">' . $str . '</p>
		 
		 
				<p class="review-by">By : <span itemprop="author">' . $commenter . '</span>
			                 <meta itemprop="datePublished" content="'.$feedback_date.'">on&nbsp;' . $feedback_monthName . ' ' . $feedback_fetch_year . '
							 ' . $know_me_title . ' / ' . $visit_duration . ' 
						<div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                        <meta itemprop="worstRating" content = "1"/>
                        <meta itemprop="ratingValue" content = "'.$overallRating.'"/>
                        <meta itemprop="bestRating" content = "5"/>
                        </div>	

 
							 
					 						
		</p>
                                            </div>
										</div>
										
                                       
                                    </div>
                                    <div class="row align-items-center">
                                         
                                        <div class="col-md-12">
                                            <div class="float-left">
                                                <p><span class="dark-text"> <a href="' . HOME_PATH . $record['quick_url'] . '">' . substr($record['pdtitle'], 0, 60) . ', ' . $record['address_city'] . '</a><br/>';
            if ($record['feedback_reply']) {
                $reviews .= '<br><span class="dark-text">Includes reply from ' . substr($record['pdtitle'], 0, 60) . '</span>';
            }


            $reviews .= '</p>
			<p class="margin_bottom">
			<span itemprop="description">' . $pros . '</span></p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                </div>';
        }
    }
    return $reviews;
}

/* * *********************************end  of  code for get right banner according city name of current user************* */
?>
<style>.viewall{margin-top:20px}.review_thumb{margin-bottom:2%}.m-0{margin:0}.dark-text{color:#8a8a8a}.display-inline{display:inline}.loading-hide{display:none}.noresult{text-align:center;color:#F30}.view-all{color:#000;text-decoration:underline}.right-banner-cover{margin:20px 0 25px;padding-left:0}.mt-20{margin-top:20px}.text-cornflowerblue{color:cornflowerblue}.text-14{font-size:14px}</style>
        <div class="hero v2 section-padding" style="background-image: url(<?php echo HOME_PATH; ?>/modules/pages/images/header/hero-2.jpg)">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h1 class="hero-title v2">
                            FIND, REVIEW & COMPARE REST HOMES, RETIREMENT VILLAGES & AGEDCARE
                        </h1>
                         
                    </div>
                    <div class="col-md-6 offset-md-3 text-center mar-top-20">
                        <form class="hero__form v2">
                            <div class="row">
                                <div class="col-lg-8 col-md-12">
                                    <input class="hero__form-input custom-select" type="text" name="place-event" id="place-event" placeholder="What are you looking for?">
                                </div>
                                
                                 
                                <div class="col-lg-4 col-md-12">
                                    <div class="submit_btn text-right md-left">
                                        <button class="btn v3  mar-right-5" type="submit"><i class="ion-search" aria-hidden="true"></i> Search</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-10 offset-md-1" style="display:none;">
                        <div class="hero-catagory-menu text-center">
                            <p>Or browse Popular Categories</p>
                            <ul>
                                <li><a href="all-categories.html"><i class="ion-android-restaurant"></i> Restaurant</a></li>
                                <li><a href="all-categories.html"><i class="ion-ios-musical-notes"></i> Event</a></li>
                                <li><a href="all-categories.html"><i class="ion-ios-home-outline"></i> Hotel</a></li>
                                <li><a href="all-categories.html"><i class="ion-ios-cart-outline"></i> Shopping</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Hero section ends-->
		 
        <!--Popular City starts-->
        <div class="popular-cities section-padding mar-top-20">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h2 class="section-title v1">Search your chosen region</h2>
                    </div>
                    <div class="col-md-12">
                        <div class="swiper-container popular-place-wrap v2">
                            <div class="swiper-wrapper">
							<?php 
							 
							      $query = "select * from  ad_region ";
                                   $res = $db->sql_query($query);
                                  $data = $db->sql_fetchrowset($res);
								  
								   foreach ($data as $datares) {
							?>
                                <div class="swiper-slide popular-item">
                                    <div class="single-place">
                                        <img class="single-place-image" src="<?php echo HOME_PATH; ?>/modules/pages/images/category/<?php echo $datares['image'] ?>" alt="place-image">
                                        <div class="single-place-content">
                                            <h2 class="single-place-title">
                                                <a href="grid-fullwidth-map.html"><?php echo $datares['region'] ?></a>
                                            </h2>
                                            <ul class="single-place-list">
                                                <li><span>5</span> Cities</li>
                                                <li><span>255</span> Listing</li>
                                            </ul>
                                            <a class="btn v6 explore-place" href="grid-fullwidth-map.html">Explore</a>
                                        </div>
                                    </div>
                                </div>
								   <?php }?>
                          
                            </div>
                        </div>
                        <div class="slider-btn v1 popular-next style2"><i class="ion-arrow-right-c"></i></div>
                        <div class="slider-btn v1 popular-prev style2"><i class="ion-arrow-left-c"></i></div>
                    </div>
                </div>
            </div>
        </div>
        <!--Popular City ends-->
		  <!--Call to action starts-->
        <div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH; ?>/modules/pages/images/bg/memphis-colorful.png)">
            <div class="overlay op-8 green"></div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="action-title">
                            <h2>About AgedAdvisor</h2>
                            <p>
                                Helping you find the right retirement or restcare facilities.The team at Aged Advisor are locals that have first hand experience with Retirement Villages, Rest Homes and Aged Care facilities, either through our line of work (providing services to residents within these facilities) or by having family or loved ones living there. 
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="call-to-action text-right sm-left">
                            <a class="btn v9" href="<?php echo HOME_PATH; ?>/about-us">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Call to action ends-->
        <!--Trending events starts-->
		
        <div class="trending-places section-padding pad-bot-130">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h2 class="section-title v1">Top Rated Facilities</h2>
                    </div>
                    <div class="col-md-12">
                        <div class="swiper-container trending-place-wrap">
                            <div class="swiper-wrapper">
                            
                            <?php echo topFiveReview();?>
				  
								
                            </div>
                        </div>
                        <div class="trending-pagination"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--Trending events ends-->
		 <!--Partner section starts-->
		 <div class="call-to-action pad-tb-20" style="background-image: url(<?php echo HOME_PATH; ?>/modules/pages/images/bg/memphis-colorful.png)">
            <div class="overlay op-8 green"></div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <div class="action-title">
                            <h2>Know someone that would like to place a review but doesn't have access to a computer? <div class="call-to-action text-right sm-left">						 
                            <a class="btn v9" href="https://www.agedadvisor.nz/images/agedadvisor-Review-Sheet-A4-PRINT.pdf">Download a printable FREEPOST form here</a>
                        </div></h2>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>       
        <!--Partner section ends-->
        
        
        <!--Coupon starts-->
        <div class="coupon-section section-padding">
            <div class="container ">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h2 class="section-title v1"> Latest Reviews</h2>
                    </div>
                    <div class="col-md-12">
                        <div class="swiper-container coupon-wrap">
                            <div class="swiper-wrapper">
							 <?php echo getReviews(); ?>
                             
                            </div>
                        </div>
						
						
						
                        <div class="slider-btn v1 coupon-next">
						<i class="ion-arrow-right-c"></i></div>
                        <div class="slider-btn v1 coupon-prev"><i class="ion-arrow-left-c"></i></div>
                        <div class="modal fade" id="coupon_wrap">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel">Get a Coupon</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="ion-ios-close-empty"></i></span></button>
                                    </div>
                                    <div class="coupon-bottom">
                                        <div class="float-left"><a href="single-listing-one.html" class="btn v1">Go to Deal</a></div>
                                        <button type="button" class="btn v1 float-right" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Coupon ends-->
		  <!--mobile app start-->
        <div class="app-section section-padding pad-top-70" style="background-image: url(<?php echo HOME_PATH; ?>/modules/pages/images/bg/bg2.png)">
            <div class="container">
                <div class="row">
				<div class="col-md-8 offset-md-2 text-center">
                        <h2 class="section-title v1"> Our award winning facilities</h2>
                    </div>
					<div class="col-md-7">
                        <div class="text-center">
                            <img src="<?php echo HOME_PATH; ?>/modules/pages/images/team/Cheviot-Rest-Home-Cheviot-Cheviot.jpg" alt="...">
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="app-content">
                            <h2>South Island Awards</h2>
                            <p>
                                Cheviot Resthome: Manager / Owner, Sue Coleman with some of the team and their Awards for Best Small Aged Care Facility - South Island (3 Years in a row!).
                            </p>
							<div class="pad-top-30"><a class="btn v8" href="<?php echo HOME_PATH; ?>/awards">More Awards</a></div>
                            
                        </div>
                    </div>
                    
                </div>
				
            </div>
        </div>
        <!--mobile app ends-->
		 <!--Partner section starts-->
		 <div class="call-to-action pad-tb-20" style="background-image: url(<?php echo HOME_PATH; ?>/modules/pages/images/bg/memphis-colorful.png)">
            <div class="overlay op-8 green"></div>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-8">
                        <div class="action-title">
                            <h2>POPULAR! Village Comparison</h2>
                            <p>
                                 Compare entry ages, costs, fees and conditions of Retirement Villages
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="call-to-action text-right sm-left">
                            <a class="btn v9" href="<?php echo HOME_PATH; ?>Compare-Retirement-Villages-Entry-Age-Costs-Fees-Conditions">Explore</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>       
        <!--Partner section ends-->
		<!--Blog Posts starts-->
        <div class="blog-posts v1 pad-bot-60 pad-top-70">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 offset-md-2 text-center">
                        <h2 class="section-title v1">Popular Posts</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <div class="card single-blog-item v1">
                            <img src="<?php echo HOME_PATH; ?>/modules/pages/images/blog/news_7.jpg" alt="...">
                            <a href="#" class="blog-cat btn v6 red">Hotel</a>
                            <div class="card-body">
                                <h4 class="card-title"><a href="single-news-one.html">Top 10 Homestay in London That you don't miss out</a></h4>
                                <div class="bottom-content">
                                    <p class="date">Sep 28th , 2018 by <a href="#" class="text-dark">Louis Fonsi</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card single-blog-item v1">
                            <img src="<?php echo HOME_PATH; ?>/modules/pages/images/blog/news_8.jpg" alt="...">
                            <a href="#" class="blog-cat btn v6 red">Restaurant</a>
                            <div class="card-body">
                                <h4 class="card-title"><a href="single-news-one.html">Cappuccino Coffee at Broklyn for Coffee Lover.</a></h4>
                                <div class="bottom-content">
                                    <p class="date">Dec 5th , 2018 by <a href="#" class="text-dark">Adam D'Costa</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="card single-blog-item v1">
                            <img src="<?php echo HOME_PATH; ?>/modules/pages/images/blog/news_8.jpg" alt="...">
                            <a href="#" class="blog-cat btn v6 red">Travel</a>
                            <div class="card-body">
                                <h4 class="card-title"><a href="single-news-one.html">Top 50 Greatest Street Arts in Paris</a></h4>
                                <div class="bottom-content">
                                    <p class="date">Mar 13th , 2018 by <a href="#" class="text-dark">Mark Henri</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Blog Posts ends-->
         
      <!-- Scroll to top starts-->
        <span class="scrolltotop"><i class="ion-arrow-up-c"></i></span>
        <!-- Scroll to top ends-->
  
    <!--Page Wrapper ends-->
    <!--Footer Starts-->
     
	
	<style>
	.star-inactive {
    background-image: url(https://www.agedadvisor.nz//images/starbg.png);
    background-repeat: no-repeat;
    background-size: 91px;
    background-position: 0 0;
    display: inline-block;
    height: 20px;
    width: 92px;
}

.star-active {
    background: url(https://www.agedadvisor.nz//images/starbg.png) 0 -21px no-repeat;
    background-size: 91px;
    height: 20px;
 
}

.coupon-content {
 
    min-height: 338px;
}

	</style>
	<script type="text/javascript">
    <?php
     
    $futer_event_row = 0;
    $row = 0;
    ?>
    $(document).ready(function(){
        var next_event = '<?php echo $futer_event_row; ?>';
        var check_event = '<?php echo $row; ?>';
        if (check_event > 0)
        {
            $("#event-container").show(200);
            $(".shortlist-message").hide(200);
            $("#show-more_second").hide(200);
        } else {
            if (check_event == 0)
            {
                $(".shortlist-message").show(200);
                $("#event-container").hide(200);
            }
        }
    });
</script>

    <!--Footer ends-->
   <!--Color switcher starts-->
     
    <!--Color switcher ends-->
    <!--Scripts starts-->
    <!--plugin js-->
    <script src="<?php echo HOME_PATH;?>modules/pages/js/plugin.js"></script>
    <!--google maps-->
   
    <!--Main js-->
    <script src="<?php echo HOME_PATH;?>modules/pages/js/main.js"></script>
    <!--Scripts ends-->
 