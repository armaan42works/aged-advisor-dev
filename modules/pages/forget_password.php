<!---------------
Objective: Provide form to send request to change password
filename : forget_password.php
Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
Created : 14 August 2014
-->
<?php
    global $db;
    // getting the email id from forgot password page
    if (isset($submit) && $submit == 'Submit') {
        $userEmail = $email;
        //getting the passowrd of the submitted email id. 
        $sql_query = "SELECT name,password FROM " . _prefix("users") . " where email ='$userEmail'";
        $res = $db->sql_query($sql_query);
        $num = mysqli_num_rows($res);
        $records = $db->sql_fetchrowset($res);

        if (count($records)) {

            foreach ($records as $record) {

                $password = $record['password'];
                $userName = $record['name'];
            }
        }
        if ($password != "") {
            $link = "<a href='" . HOME_PATH . "changePassword/" . $password . "' title='Click Here to change password'>Click Here</a>";
            $email = emailTemplate('forgot-password');
            if ($email['description'] != '') {
                $message = str_replace(array('{full_name}', '{forgotpass_link}'), array($userName, $link), $email['description']);
                sendmail($userEmail, $email['subject'], $message);
                $msg = common_message(1, constant('FORGET_PASSWORD'));
                $_SESSION['msg'] = $msg;
                $url = HOME_PATH.'login';
                redirect_to($url);
            }
        }
    }
?>

<section id="slider_container">
    <div class="slider_bg">
        <ul class="slider">
            <li><img src="<?php echo HOME_PATH; ?>images/slider_img.png" alt=""></li>
        </ul>

    </div>
</section>

<section id="body_container">
    <div class="wrapper">

        <div class="main_box">
            <!----left container STARTS------>
            <div class="left_container">
                <div class="login_container">
                    <h3>Forget Password</h3>
                    <form id="forgetPassword" name="forgetPassword" method="post" class="login_account">
                        <div class="col">
                            <input type="text" name="email" id="email" class="input_login" placeholder="Email Address">
                        </div>
                        <div class="col">
                            <input type="submit" name="submit" id="button" class="creat_account_btn" value="Submit">                            
                             
                        </div>
                       
                    </form>

                </div>

            </div>
            <!----right container STARTS------>
            <div class="right_container">
                <div class="about_col none signup_login">
                    <h3>News letter Sign Up</h3>
                    <p>Want to keep up to date with all our latest news and information? Enter your name and email below to be added to our mailing list.</p>
                    <div class="newletter_bg">
                        <h3>Join our Newsletter</h3>
                        <form id="form1" name="form1" method="post" class="joinus">
                            <input type="text" placeholder="Your Name">
                            <input type="password" placeholder="Your E-mail">
                            <a href="#" class="subscribe">Subscribe</a>
                        </form>






                    </div>




                </div>

            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('#forgetPassword').validate({
            rules: {
                "email": {
                    "required": true,
                    "email": true,
                    "remote": {
                        url: '<?php echo HOME_PATH ?>admin/ajax.php',
                        type: 'POST',
                        data: {action: "emailExist"
                        }
                    }
                }
            },
            messages: {
                "email": {
                    "required": "This field is required.",
                    "email": "Please enter a valid email address.",
                    "remote": "Email Address not registered."
                }
            }
        });
    });
</script>
