<?php

/* * ************************************ */
/* Logout Page facility
 */
include("application_top.php");
unset($_SESSION['facility_name']);
unset($_SESSION['sp_id']);
session_destroy();
header("Location:" . HOME_PATH . 'facility-login');
?>