<?php
session_start();
/***************************************
* page name: accountinfo.php           *
* updated date: 24-1-16                *
* update code: appointment date        *
*line no.   490-516                    *
*                                       *
*                                       *
/****************************************/

$update_to_enable=' <a href="'.HOME_PATH . 'supplier/payment">Upgrade Facility to enable.</a>';
   

$facilty_name= $_SESSION['facilty_name'];
$p_id=$_SESSION['pr_id'];
$sp_id=$_SESSION['sp_id'];
$password=$_SESSION['password'];
  


      //  echo  $pr_id;
      // echo  $facilty_name;
       // exit;
         
$sql_spId= "select supplier_id from ad_products where id ='$p_id' AND facility_password='$password '";
$sp_id=mysqli_query($db->db_connect_id, $sql_spId);
$count_data=mysqli_num_rows($sp_id);

if($count_data==0)
{   ?>
        <script> document.onload = setTimeout(function () { alert('wrong password'); }, 5000);
           <?php header("Location:" . HOME_PATH .'facility-login');?>

        </script>
    <?php 
}
else 
{

    function get_permissions_for_showing(){
          $p_id=$_SESSION['pr_id'];
          $sp_id=$_SESSION['sp_id'];
      
    
    global $db;

     $sql_query = "SELECT fdbk.*,
            memp.logo_enable,
            memp.map_enable,
            memp.address_enable,
            memp.phone_number_enable,
            memp.video_enable,
            memp.video_limit AS video_limit_enable,
            memp.youtube_video AS youtube_video_enable,
            memp.appointment_enable,
            memp.vacancy_enable,
            memp.send_message_enable,
            memp.events_calendar_enable,
            memp.image_enable,
            memp.facility_desc_enable,
            memp.staff_enable,
            memp.management_enable,
            memp.activity_enable,
            memp.ext_url_enable,
            memp.image_limit,
            memp.product_limit,
            memp.number_of_review_response,
            pein.certification_service_type,
            pein.certificate_license_end_date,
            pein.dhb_name,
            pein.certificate_name,
            pein.certification_period,
            pein.current_auditor,
            pein.email,
            pein.phone,
            pein.first_name,
            pein.last_name,
            pein.position,
            
            
 pr.*,pr.id AS proId, "
        . "restCare.title as restcare_name,pr.id as pr_id, extinfo.*,extinfo.id as ex_id "
        . " FROM " . _prefix("products") . " AS pr "
//        . " Left join " . _prefix("services") . " AS sr ON pr.facility_type=sr.id"
//        . " Left join " . _prefix("cities") . " AS city ON pr.city_id=city.id"
//        . " Left join " . _prefix("suburbs") . " AS suburb ON pr.suburb_id=suburb.id"
        . " Left join " . _prefix("rest_cares") . " AS restCare ON pr.restcare_id=restCare.id"
//        . " Left join " . _prefix("services") . " AS facility ON pr.facility_type=facility.id"
        . " Left join " . _prefix("extra_facility") . " AS extinfo ON extinfo.product_id =pr.id"
        . " Left join " . _prefix("feedbacks") . " AS fdbk ON fdbk.product_id =pr.id"
        . " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id=pr.id"
        . " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id =pr.id AND prpn.current_plan = 1"
        . " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id = memp.id "


        . "where pr.id='$p_id' AND pr.supplier_id='$sp_id'";


$res = $db->sql_query($sql_query);
$records = $db->sql_fetchrowset($res);
//print_r($records);
//prd($sql_query);
//echo $sql_query;//exit;
if (count($records)) {
    foreach ($records as $record) {
        $proId = $record['proId'];
        $id = $record['proId'];
        $ex_id = $record['ex_id'];
        $pr_id = $record['pr_id'];
        $product_id=$pr_id;   
        $title = $record['title'];
        $description = $record['description'];
        $keyword = $record['keyword'];
        $image = $record['image'];
        $ext_url = $record['ext_url'];
        $youtube_video = $record['youtube_video'];
        $address = $record['address'];
        $address_city = strip_tags(stripcslashes($record['address_city']));
        $address_suburb = strip_tags(stripcslashes($record['address_suburb']));
        $restcare_name = strip_tags(stripcslashes($record['restcare_name']));
        $zip = $record['zip'];
        $staff_comment = $record['staff_comment'];
        $management_comment = $record['management_comment'];
        $activities_comment = $record['activity_comment'];
        $staff_image = $record['staff_image'];
        $management_image = $record['management_image'];
        $activities_image = $record['activity_image'];
        $brief = $record['brief'];
        $noOfRooms = $record['no_of_room'];
        $noOfBeds = $record['no_of_beds'];
        $long = $record['longitude'];
        $lat = $record['latitude'];
        $position = $record['position'];
	    $email = $record['email'];
	    $phone = $record['phone'];
	    $first_name = $record['first_name'];
	    $last_name = $record['last_name'];
        $position = $record['position'];
        $dhb_name = $record['dhb_name'];
        $certification_period = $record['certification_period'];
        $current_auditor = $record['current_auditor'];
        $certificate_license_end_date = $record['certificate_license_end_date'];
        $certification_service_type = $record['certification_service_type'];
        $logo = $record['logo'];
        $start_appointment_date_info= $record['start_appointment_date'];
        $start_appointment_date=date('m/d/Y',strtotime($start_appointment_date_info));
        $end_appointment_date_info= $record['end_appointment_date'];
        $end_appointment_date=date('m/d/Y',strtotime($end_appointment_date_info));  
        $appointment_day1=explode(",",$start_appointment_date_info);
        
         // Convert new lines
        $brief=str_ireplace("\n", "<br>", $brief);
        $description=str_ireplace("\n", "<br>", $description);
        $staff_comment=str_ireplace("\n", "<br>", $staff_comment);
        $management_comment=str_ireplace("\n", "<br>", $management_comment);
        $activity_comment=str_ireplace("\n", "<br>", $activity_comment);
        $address_other =str_ireplace("\n", "<br>", $address_other);
        $address =str_ireplace("\n", "<br>", $address);
        
        // end conver newline to <br>
       
        // permissions for viewing details
        $vacancy_enable = $record['vacancy_enable'];
        $appointment_enable = $record['appointment_enable'];
        $send_message_enable = $record['send_message_enable'];
        $events_calendar_enable = $record['events_calendar_enable'];
        $logo_enable = $record['logo_enable'];
     	$map_enable = $record['map_enable'];

       $plan_id = $record['plan_id'];
        $address_enable = $record['address_enable'];
        $phone_number_enable = $record['phone_number_enable'];
        $video_enable = $record['video_enable'];
        $video_limit_enable = $record['video_limit_enable'];// first x times videos
        $youtube_video_enable = $record['youtube_video_enable'];
        $image_enable = $record['image_enable'];
        $facility_desc_enable = $record['facility_desc_enable'];
        $staff_enable = $record['staff_enable'];
        $management_enable = $record['management_enable'];
        $activity_enable = $record['activity_enable'];
        $ext_url_enable = $record['ext_url_enable'];
        $image_limit = $record['image_limit'];// first x times images
        $product_limit = $record['product_limit'];// not used
        $number_of_review_response = $record['number_of_review_response'];
        
}
}
    
return array($title,$logo_enable,$map_enable,$address_enable,$phone_number_enable,$youtube_video_enable,$image_enable,$facility_desc_enable,$staff_enable,$management_enable,$activity_enable,$ext_url_enable,$image_limit,$appointment_enable,$vacancy_enable,$send_message_enable,$events_calendar_enable);
 
// list($title,$logo_enable,$map_enable,$address_enable,$phone_number_enable,$youtube_video_enable,$image_enable,$facility_desc_enable,$staff_enable,$management_enable,$activity_enable,$ext_url_enable,$image_limit)=get_permissions_for_showing();   
    
}    
 $list= array();
$list= get_permissions_for_showing();
// print_r($list);
 //exit();

$p_id=$_SESSION['pr_id'];
$sp_id=$_SESSION['sp_id'];

$ipa = gethostbyaddr($_SERVER['REMOTE_ADDR']);
global $db;
// Make sure proextra info table row exists
                $now_date= date("Y-m-d H:i:s");
           $qry = "select id from " . _prefix('pro_extra_info') . "  where pro_id=$p_id ";
            $result = $db->sql_query($qry);
            $data = $db->sql_fetchrow($result);
            if ($db->sql_numrows($result) == 0) {
                     $fields = array(
                    'id' => $p_id,
                    'pro_id' => $p_id,
                    'created' => $now_date,
                    'modified' => $now_date
                    );
                $insert_result = $db->insert(_prefix('pro_extra_info'), $fields);
 
                
            }


// Make sure pro_plans info table row exists
                
           $qry = "select id from " . _prefix('pro_plans') . "  where pro_id=$p_id ";
            $result = $db->sql_query($qry);
            $data = $db->sql_fetchrow($result);
            if ($db->sql_numrows($result) == 0) {

// ID of the facility =  $last_product_id       
$end_of_this_month = date("Y-m-d H:i:s",mktime(23, 59, 59, date("m")+1, 0, date("Y")));
$now =date('Y-m-d h:i:s', time());
            $plan_fields = array(
                'supplier_id' => $id,
                'pro_id ' => $p_id,
                'plan_start_date' => '$now',
                'plan_end_date' => '$end_of_this_month',
                'current_plan' => '1',
                'created' => '$now',
                'plan_id' => '1',
                'response_of_review' => $number_of_review_response
            );

            $insert_plans = $db->insert(_prefix('pro_plans'), $plan_fields);
}



//Finding the plan type of current supplier for the current services
 $querys = " SELECT pro.*,exinfo.*,member.title as plan,member.id as plan_id,
            member.appointment_enable,
            member.vacancy_enable,
            member.send_message_enable,
            member.events_calendar_enable,
            member.logo_enable,
            member.map_enable,
            member.address_enable,
            member.phone_number_enable,
            member.video_enable,
            member.video_limit AS video_limit_enable,
            member.youtube_video AS youtube_video_enable,
            member.image_enable,
            member.facility_desc_enable,
            member.staff_enable,
            member.management_enable,
            member.activity_enable,
            member.ext_url_enable,
            member.image_limit,
            member.product_limit,



exfaci.no_of_beds,exfaci.no_of_room,exfaci.extra_info1,exfaci.brief FROM " . _prefix("products") . " AS pro"
        . " LEFT JOIN " . _prefix("pro_extra_info") . " AS exinfo ON exinfo.pro_id=pro.id"
        . " LEFT JOIN " . _prefix("extra_facility") . " AS exfaci ON exfaci.product_id=pro.id"
        . " LEFT JOIN " . _prefix("pro_plans") . " AS pln ON pln.pro_id=pro.id"
        . " LEFT JOIN " . _prefix("membership_prices") . " AS member ON member.id=pln.plan_id "
        . " WHERE pro.id =" . $p_id . " AND pln.supplier_id=" . $sp_id . " AND pln.status = 1 AND pln.deleted = 0 ORDER BY pln.id DESC";
        
        
//echo $querys.'<hr>'; 
$planres = $db->sql_query($querys);
$dataplans = $db->sql_fetchrow($planres);
//print_r($dataplans);exit;
//prd($dataplans);
//User Account Details of current supplier
$sql_query = "SELECT user.*  FROM " . _prefix("users") . " AS user"
        . " Left join " . _prefix("sp_payments") . " AS spamount ON spamount.user_id =user.id  WHERE user.id='$id'";
$res = $db->sql_query($sql_query);
$record = $db->sql_fetchrow($res);
//End

//$name = $record['first_name'] . " " . $record['last_name'];
$name=$dataplans['title'];
//$user_name = $record['user_name'];
//$email = $record['email'];
$accountType = !empty($dataplans) ? $dataplans['plan'] : 'Free';
$no_of_beds = $dataplans['bed'];
$premises_website = $dataplans['premises_website'];
$address = $dataplans['address'];
$address_city = $dataplans['address_city'];
$address_suburb = $dataplans['address_suburb'];
$dhb_name = $dataplans['dhb_name'];
$position = $dataplans['position'];
$email = $dataplans['email'];
$phone = $dataplans['phone'];
$first_name = $dataplans['first_name'];
$last_name = $dataplans['last_name'];
$position = $dataplans['position'];
//$IPaddress = $record['ip_address'];
$amount = $dataplans['price'];
 $productLogo = $dataplans['logo'];
$zipcode = $dataplans['zip'];
//$overAll_Rating = overAllRating($id);
$overAll_Rating = OverAllNEWRatingProduct($id);// Simple average of users overall ratings

$lat = $dataplans['latitude'];
$long = $dataplans['longitude'];
$certification_service_type = $dataplans['certification_service_type'];
$certificate_name = $dataplans['certificate_name'];
$certification_period = $dataplans['certification_period'];
$certificate_license_end_date = $dataplans['certificate_license_end_date'];
$current_auditor = $dataplans['current_auditor'];
$youtube_video = $dataplans['youtube_video'];
$ext_url = $dataplans['ext_url']; // premises website


$brief = $dataplans['brief'];
$description = $dataplans['description'];
$staff_comment = $dataplans['staff_comment'];
$management_comment = $dataplans['management_comment'];
$activity_comment = $dataplans['activity_comment'];
$no_of_room = $dataplans['no_of_room'];
$no_of_beds = $dataplans['no_of_beds'];
$quick_url = $dataplans['quick_url'];

// permissions for viewing details
$plan_id = $dataplans['plan_id'];

$appointment_enable = $dataplans['appointment_enable'];
$vacancy_enable =  $dataplans['vacancy_enable'];
$send_message_enable =  $dataplans['send_message_enable'];
$events_calendar_enable =  $dataplans['events_calendar_enable'];

$logo_enable = $dataplans['logo_enable'];
$map_enable = $dataplans['map_enable'];
$address_enable = $dataplans['address_enable'];
$phone_number_enable = $dataplans['phone_number_enable'];
$video_enable = $dataplans['video_enable'];
$video_limit_enable = $dataplans['video_limit_enable'];// first x times videos
$youtube_video_enable = $dataplans['youtube_video_enable'];
$image_enable = $dataplans['image_enable'];
$facility_desc_enable = $dataplans['facility_desc_enable'];
$staff_enable = $dataplans['staff_enable'];
$management_enable = $dataplans['management_enable'];
$activity_enable = $dataplans['activity_enable'];
$ext_url_enable = $dataplans['ext_url_enable'];
$image_limit = $dataplans['image_limit'];// first x times images
$product_limit = $dataplans['product_limit'];// not used

//echo "<h1>".$plan_id."</h1>";
// Convert new lines
$brief=str_ireplace("\n", "<br>", $brief);
$description=str_ireplace("\n", "<br>", $description);
$staff_comment=str_ireplace("\n", "<br>", $staff_comment);
$management_comment=str_ireplace("\n", "<br>", $management_comment);
$activity_comment=str_ireplace("\n", "<br>", $activity_comment);
$address_other =str_ireplace("\n", "<br>", $address_other);
$address =str_ireplace("\n", "<br>", $address);

// end conver newline to <br>

   // Use address
  $map_address='New Zealand';$map_address_zoom=5;$map_address_title="Location: New Zealand";
  if($address_city){$map_address=$address_city.', '.$map_address;$map_address_zoom=10;$map_address_title="Location: $map_address";}  
  if($address_suburb){$map_address=$address_suburb.', '.$map_address;$map_address_zoom=13;$map_address_title="Location: $map_address";}  
  if($address && $address_enable){$map_address=$address.', '.$map_address;$map_address_zoom=15;$map_address_title="Location: $map_address";}  
 



//if (empty($lat) && empty($long) && !empty($zipcode)) {
//    $val = getLnt($zipcode);
//    $lat = $val['lat'];
//    $long = $val['lng'];
//    
//}


// for Service Cernter Location(s)
$sql_queryL = "SELECT sl.locations AS locationId,city.title AS locations FROM " . _prefix("service_locations") . " AS sl "
        . " Left join " . _prefix("cities") . " AS city ON city.id=sl.locations "
        . " where sl.user_id ='$id'";
$resL = $db->sql_query($sql_queryL);
$LocationRecords = $db->sql_fetchrowset($resL);
// for Services Listed
//    $sql_queryF = "SELECT ft.id, sft.name FROM " . _prefix("sp_facility_type") . " AS ft "
//            . "LEFT JOIN  " . _prefix("services") . " sft ON sft.id=ft.facility_type where ft.user_id = '$id'";

$sql_queryF = "select planS.id, service.name from " . _prefix('pro_services') . " AS planS "
        . "INNER JOIN " . _prefix('services') . " AS service  ON planS.service_id = service.id"
        . " where planS.product_id='$p_id' AND planS.deleted=0 AND planS.status = 1 ORDER BY id DESC";
$resL = $db->sql_query($sql_queryF);
$facilityType = $db->sql_fetchrowset($resF);

//prd($sql_queryF);

list($title,$logo_enable,$map_enable,$address_enable,$phone_number_enable,$youtube_video_enable,$image_enable,$facility_desc_enable,$staff_enable,$management_enable,$activity_enable,$ext_url_enable,$image_limit,$appointment_enable,$vacancy_enable,$send_message_enable,$events_calendar_enable)=get_permissions_for_showing(); 
//echo '<pre>';print_r(get_permissions_for_showing()); echo '</pre>';
?>
<script type="text/javascript">
    var map;
    $(document).ready(function(){
 
         setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 15000);
     
       map = new GMaps({
        div: '#map',
        lat: -43.3744881,
        lng: 172.4662705,
        zoom: <?php echo $map_address_zoom?>
      });
<?php if($map_address!=="New Zealand"){?>
        GMaps.geocode({
          address: '<?php echo $map_address?>',
          callback: function(results, status){
            if(status=='OK'){
              var latlng = results[0].geometry.location;
              map.setCenter(latlng.lat(), latlng.lng());
              map.addMarker({
                lat: latlng.lat(),
                lng: latlng.lng()
              });
            }
          }
        });
<?php } ?>

    });
</script>

<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <?php echo $_SESSION['msg']; ?><br>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php //require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_right_col"> 
                        <h2 class="hedding_h2 pull-left"><i class="fa  fa-folder-open-o "></i> <span>Individual Facility Information</span> &nbsp;&nbsp;&nbsp;<a class="btn btn-info center-block btn_search btn_pay pull-right text-right" href="<?php echo HOME_PATH .$quick_url ?>">View Live Page</a>
                        <a class="btn btn-danger center-block btn_search btn_pay pull-right text-right" href="<?php echo HOME_PATH . 'edit-facility-info' ?>" style="margin: 0 5px 0 5px;">Edit</a>&nbsp;&nbsp;&nbsp;<!-- <a class="pull-right btn btn-info" href="<?php // echo HOME_PATH ?>supplier/channel?set=proOut">Go Back</a> --></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left">
                        <ul class="list-group">
                            <?php // 	if ($_SESSION['planId'] != '1') { ?>
                            
                            
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Facility Logo:</span>
                                        </div> 
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <?php if (file_exists(DOCUMENT_PATH . 'admin/files/product/logo/' . $productLogo) && $productLogo != ''  && $plan_id!=1 )  { ?>
                                                <img title="User Image" src="<?php echo MAIN_PATH . 'files/product/logo/' . $productLogo ?>"/>
                                            <?php } else { ?>
                                                <a href="<?php echo HOME_PATH . 'supplier/editAccountInfo' ?>"><img  title="User Image" src="<?php echo IMAGES; ?>notlogo.jpeg"/></a>
                                            <?php } ?>
                                            <?php if (!$logo_enable){?><br>
                                                <!-- <a href="<?php// echo HOME_PATH . 'supplier/payment' ?>">Apply for Upgrade</a> to show Logo on site. -->
                                            <?php }?>

                                        </div>
                                    </div>
                                </li>
                            <?php // } ?>
                            <li class = "list-group-item">
                                <div class = "row">
                                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class = "pull-left left_td">Facility Name:</span>
                                    </div>
                                    <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class = "pull-left right_td"><?php echo $facilty_name;?>
                             </span>
                                    </div>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Services Listed:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td">
                                            <?php
                                            $i = 1;
                                            foreach ($facilityType as $record) {
                                                echo $i++ . '. ' . $record['name'] . '<br>';
                                            }
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Overall Rating:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo OverAllNEWRatingProduct($p_id);?></span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                              <!--code for appointment available date  of product-->
                        <h4 class="logo_bottom_color font_size_16n ">Appointment Day</h4>
                        <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td"> Available Day</span>
                                        </div>
                                        
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <?php if($appointment_enable) { 
                                          $check_day=mysqli_query($db->db_connect_id, "select * from ad_products where id='$p_id'") or mysqli_error("error problem in appointment");
                                         while($check_avail_day=mysqli_fetch_array($check_day))
                                         {
                                              $check_avail_day1=$check_avail_day['appointment_day'];
                                              
                                         }
                                          ?>
                                         <span class="pull-left right_td"><input id="appointment_day" type='text' class="col-sm-4 form-control"  name="appointment_day" value="<?php echo $check_avail_day1 ;?>" style="width:215%;" disabled></span>
                                         <?php } else { ?>
                                            <!-- <a href="<?php //echo HOME_PATH . 'supplier/payment' ?>">Apply for Upgrade</a> to let user enquire about availability on site. -->
                                         <?php } ?>
                                        </div>
                                    </div>
                                </li>
                        </ul>
                         
                         
                        <h4 class="logo_bottom_color font_size_16n ">Vacancy </h4>
                        <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td"> Vacancy Detail</span>
                                        </div>
                                        <?php
                                        
                                            if ($vacancy_enable){

                                            $vacanny_query="select `vacancy`,`vacancy_external_link`  from ad_products where id='$p_id'"; 
                                            $vaccancy=mysqli_query($db->db_connect_id, $vacanny_query);
                                            {
                                                while($vacanncy1=mysqli_fetch_array($vaccancy))
                                                {
                                                       $vacanncy1_info=$vacanncy1['vacancy'];
                                                        $vacancy_ext_link=$vacanncy1["vacancy_external_link"];
                                                }
                                            }
                                        ?>

                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                           
                                           <input style="width:50%" id="vacanncy"  name="vacanncy" type='text' class="col-sm-4 form-control"  value="<?php echo $vacanncy1_info;?>" disabled></br>
                                            <input style="width:60%" id="vacanncy_link_info"  name="vacanncy" type='text' class="col-sm-4 form-control"  value="<?php echo $vacancy_ext_link;?>" disabled>
                                            
                                        </div>
                                        
                                        
                                        <?php }else{ ?>

                                          <!--  <a href="<?php //echo HOME_PATH . 'supplier/payment' ?>">Apply for Upgrade</a> to show Vacancy details on site.
 -->
                                        <?php } ?>
 
                                        
                                    </div>
                                </li>
                         <!--<li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">End Available Date</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="end_appointment" type='date' class="col-sm-4 form-control" value="<?php //echo $end_appointment_date; ?>" name="end_appointment"></span>
                                        </div>
                                    </div>
                                </li>-->
                         </ul>
                         
                         <!-- end of  code for vacanncy --> 
                        <h4 class="logo_bottom_color font_size_16n ">Account Plan</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Account Type:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $accountType; ?></span>
                                    </div>
                                </div>
                            </li>

                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Last Payment Made:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo isset($amount) ? '$' . stripslashes($amount) : 'N/A'; ?> then Date</span>
                                    </div>
                                </div>
                            </li>
                           
                             
                            <?php if($plan_id==1){?><li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Apply for Upgrade:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <!-- <span class="pull-left right_td"><a href="<?php// echo HOME_PATH . 'supplier/payment' ?>">Return to Facility List</a> to upgrade</span> -->
                                    </div>
                                </div>
                            </li><?php }?>
                         </ul>
                        <h4 class="logo_bottom_color font_size_16n ">Contact Details</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">First Name:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $first_name; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Last Name:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $last_name; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Position:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $position; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Email:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $email; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Phone:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $phone; ?><?php if (!$phone_number_enable){?><br>
                                        <!-- 
                                        <a href="<?php// echo HOME_PATH . 'supplier/payment'?>">Apply for Upgrade</a> to show Phone number on site. -->
                                        <?php }?>
</span>
                                    </div>
                                </div>
                            </li>
                         <h4 class="logo_bottom_color font_size_16n ">Address Details</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Street/Road:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $address; ?>
                                        <?php if (!$address_enable){?><br>
                                        
                                        <!-- <a href="<?php// echo HOME_PATH . 'supplier/payment' ?>">Apply for Upgrade</a> to show full address on site. -->
                                        <?php }?>
                                        
                                        </span>
                                    </div>
                                </div>
                            </li>
                            
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Suburb:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $address_suburb; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Town/City:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $address_city; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Post Code:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $zipcode; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Country:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td">New Zealand</span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <span class="pull-left left_td">Geographical Presence:</span><br/>
                                        <span class="pull-left left_td">(If not showing correct location, Edit profile and set your location.)</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="overflow: hidden;">
                                        <span class="pull-left right_td wd-100">
                                                   <!-- Basic Map -->
                                                    <div>&nbsp;<br>
                                                    <?php echo $map_address_title?>
                                                    <?php if (!$address_enable){?><br>
                                        
                                        <!-- <a href="<?php// echo HOME_PATH . 'supplier/payment' ?>">Apply for Upgrade</a> to show your full street address on the map. -->
                                        <?php }?></div>
                                                    <div id="map" class="map"></div>
                                                    <!-- End Basic Map -->
                                        </span>
                                    </div>
                                </div>
                            </li>
                           </li>
                         <h4 class="logo_bottom_color font_size_16n ">Other Media Links</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Website URL:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $ext_url; ?>
                                        <?php if (!$ext_url_enable){?><br>
                                        
                                        <!-- <a href="<?php //echo HOME_PATH . 'supplier/payment' ?>">Apply for Upgrade</a> to show your web address on site. -->
                                        <?php }?>
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Youtube Video URL:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $youtube_video; ?><br>
                                        <?php if (!$youtube_video_enable){?><br>
                                        
                                        <!-- <a href="<?php //echo HOME_PATH . 'supplier/payment' ?>">Apply for Upgrade</a> to show video on site. -->
                                        <?php }?>                                  
                                        
                                        <?php if($youtube_video){?>
                                            <div class="responsive-video"><a name="video"></a>
                                                <iframe width="100%" src="<?php echo $youtube_video?>" frameborder="0" allowfullscreen></iframe> 
                                            </div>
                                    <?php } ?>
                                        </span>
                                    </div>
                                </div>
                            </li>
                        </li>
                         <h4 class="logo_bottom_color font_size_16n ">Facility Description</h4>
                        <ul class="list-group">
                             <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Brief Facility Description:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $brief; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Full Facility Description:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $description; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Staff Description:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $staff_comment; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Management Description:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $management_comment; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Activities Description:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $activity_comment; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Total Rooms:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $no_of_room; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Total Beds:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $no_of_beds; ?></span>
                                    </div>
                                </div>
                            </li>
                         <h4 class="logo_bottom_color font_size_16n ">DHB Information (Applies only to Aged Care)</h4>
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">DHB Name:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $dhb_name; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Certificate Name:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $certificate_name; ?></span>
                                    </div>
                                </div>
                            </li>
 
                             <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Certification Service Type:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $certification_service_type; ?></span>
                                    </div>
                                </div>
                            </li>
                            
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Certification Period (Months):</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td">
                                             <?php if($certification_period=='12a'){echo"12 months (due to new manager employed)";}
                                             if($certification_period=='12b'){echo"12 months (due to new ownership)";}
                                             if($certification_period=='12c'){echo"12 months (due to new level of care introduced)";}
                                             if($certification_period=='12d'){echo"12 months (due to being a new facility)";}
                                             if($certification_period=='12'){echo"12 months";}
                                             if($certification_period=='24'){echo"24 months";}
                                             if($certification_period=='36'){echo"36 months";}
                                             if($certification_period=='48'){echo"48 months";}
                                             if($certification_period=='60'){echo"60 months";}
                                    ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Certificate/License End Date:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $certificate_license_end_date; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Current Auditor:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $current_auditor; ?></span>
                                    </div>
                                </div>
                            </li> 
                            <!--<li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Service Centre Location(s):</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td">
                                            <?php
                                            $i = 1;
                                            foreach ($LocationRecords as $record) {
                                                echo $i++ . '. ' . $record['locations'] . '<br>';
                                            }
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </li>-->                           
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }} ?>