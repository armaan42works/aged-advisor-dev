 
<input type="hidden"  id ="ajax_result"  value=""/>

<?php
/* * ************************************* * page name:
  editaccountinfo.php       * * updated date: 24-1-16                * * update
  code: appointment date        * *line no.   966-991                    * *
 * *                                       *
  /*************************************** */

function pre_check_before_update($table, $product_missing, $productWhere) {
    global $db;

    $return = FALSE;
    $sql_queryL = "SELECT * FROM " . $table . " $productWhere ";
    //echo $sql_queryL;
    $resL = $db->sql_query($sql_queryL);
    while ($missing = $db->sql_fetchrow($resL)) {
        $return = TRUE;
    }

    return $return;
}

$product_missing = '';
//$facilty_name= $_SESSION['facilty_name'];
//$pr_id=$_SESSION['pr_id'];
//$sp_id=$_SESSION['sp_id'];
$id = $_SESSION['sp_id'];
$productId = $_SESSION['pr_id'];
$productWhere = "where pro_id= '$productId' AND supplier_id= '$id' ";
if (pre_check_before_update(_prefix('pro_plans'), $product_missing, $productWhere)) {
    //OK
} else {
    sendmail('Nigel@agedadvisor.co.nz', 'Error Supplier ID incorrect AUTO REPAIRED', 'AUTO REPAIRED On page /modules/editAccountInfo.php pro_plans entry had incorrect supplier_id. ' . $productWhere, '');

    //echo "pro_plans entry is missing. Notify Web Admin: accounts@agedadvisor.co.nz";
    $pro_plans_info = array(
        'supplier_id' => $id
    );
    $extraWhere = "where pro_id= '$productId'";



    $product_beds = $db->update(_prefix('pro_plans'), $pro_plans_info, $extraWhere);
}
// Check for Pro_extra_info row exists
// Make sure proextra info table row exists
$now_date = date("Y-m-d H:i:s");
$qry = "select id from " . _prefix('pro_extra_info') . "  where pro_id=$productId ";
$result = $db->sql_query($qry);
$data = $db->sql_fetchrow($result);
if ($db->sql_numrows($result) == 0) {
    $fields = array(
        'id' => $productId,
        'pro_id' => $productId,
        'created' => $now_date,
        'modified' => $now_date
    );
    $insert_result = $db->insert(_prefix('pro_extra_info'), $fields);
}

//$update_to_enable='	<a href="'.HOME_PATH . 'supplier/payment">Upgrade Facility to enable.</a>';

function get_permissions_for_showing() {
    $id = $_SESSION['sp_id'];
    $productId = $_SESSION['pr_id'];
    global $db;


    $sql_query = "SELECT fdbk.*,
            memp.logo_enable,
            memp.map_enable,
            memp.address_enable,
            memp.phone_number_enable,
            memp.video_enable,
            memp.video_limit AS video_limit_enable,
            memp.youtube_video AS youtube_video_enable,
            memp.appointment_enable,
            memp.vacancy_enable,
            memp.send_message_enable,
            memp.events_calendar_enable,
            memp.image_enable,
            memp.facility_desc_enable,
            memp.staff_enable,
            memp.management_enable,
            memp.activity_enable,
            memp.ext_url_enable,
            memp.image_limit,
            memp.product_limit,
            pein.certification_service_type,
            pein.certificate_license_end_date,
            pein.dhb_name,
            pein.certificate_name,
            pein.certification_period,
            pein.current_auditor,
            pein.email,
            pein.phone,
            pein.first_name,
            pein.last_name,
            pein.position,
            
            
 pr.*,pr.id AS proId, "
            . "restCare.title as restcare_name,pr.id as pr_id, extinfo.*,extinfo.id as ex_id "
            . " FROM " . _prefix("products") . " AS pr "
//        . " Left join " . _prefix("services") . " AS sr ON pr.facility_type=sr.id"
//        . " Left join " . _prefix("cities") . " AS city ON pr.city_id=city.id"
//        . " Left join " . _prefix("suburbs") . " AS suburb ON pr.suburb_id=suburb.id"
            . " Left join " . _prefix("rest_cares") . " AS restCare ON pr.restcare_id=restCare.id"
//        . " Left join " . _prefix("services") . " AS facility ON pr.facility_type=facility.id"
            . " Left join " . _prefix("extra_facility") . " AS extinfo ON extinfo.product_id =pr.id"
            . " Left join " . _prefix("feedbacks") . " AS fdbk ON fdbk.product_id =pr.id"
            . " LEFT JOIN " . _prefix("pro_extra_info") . " AS pein ON pein.pro_id=pr.id"
            . " Left join " . _prefix("pro_plans") . " AS prpn ON prpn.pro_Id =pr.id AND prpn.current_plan = 1"
            . " Left join " . _prefix("membership_prices") . " AS memp ON prpn.plan_Id =memp.id "
            . " where pr.id='$productId' AND pr.supplier_id='$id' ";

    $res = $db->sql_query($sql_query);


    $records = $db->sql_fetchrowset($res);
//prd($sql_query);
//echo $sql_query;exit;


    if (count($records)) {
        foreach ($records as $record) {
            $proId = $record['proId'];
            $id = $record['proId'];
            $ex_id = $record['ex_id'];
            $pr_id = $record['pr_id'];
            $product_id = $pr_id;
            $title = $record['title'];
            $description = $record['description'];
            $keyword = $record['keyword'];
            $image = $record['image'];
            $ext_url = $record['ext_url'];
            $youtube_video = $record['youtube_video'];
//        $facility = $record['facility_type'];
            $address = $record['address'];
            $address_city = strip_tags(stripcslashes($record['address_city']));
            $address_suburb = strip_tags(stripcslashes($record['address_suburb']));
            $restcare_name = strip_tags(stripcslashes($record['restcare_name']));
            $zip = $record['zip'];
            $staff_comment = $record['staff_comment'];
            $management_comment = $record['management_comment'];
            $activities_comment = $record['activity_comment'];
            $staff_image = $record['staff_image'];
            $management_image = $record['management_image'];
            $activities_image = $record['activity_image'];
            $brief = $record['brief'];
            $noOfRooms = $record['no_of_room'];
            $noOfBeds = $record['no_of_beds'];
            $long = $record['longitude'];
            $lat = $record['latitude'];

            $position = $record['position'];
            $email = $record['email'];
            $phone = $record['phone'];
            $first_name = $record['first_name'];
            $last_name = $record['last_name'];
            $position = $record['position'];
            $dhb_name = $record['dhb_name'];
            $certification_period = $record['certification_period'];
            $current_auditor = $record['current_auditor'];
            $certificate_license_end_date = $record['certificate_license_end_date'];
            $certification_service_type = $record['certification_service_type'];
            $logo = $record['logo'];

            // Convert new lines
            $brief = str_ireplace("\n", "<br>", $brief);
            $description = str_ireplace("\n", "<br>", $description);
            $staff_comment = str_ireplace("\n", "<br>", $staff_comment);
            $management_comment = str_ireplace("\n", "<br>", $management_comment);
            $activity_comment = str_ireplace("\n", "<br>", $activity_comment);
            $address_other = str_ireplace("\n", "<br>", $address_other);
            $address = str_ireplace("\n", "<br>", $address);

            // end conver newline to <br>
// permissions for viewing details
            $vacancy_enable = $record['vacancy_enable'];
            $appointment_enable = $record['appointment_enable'];
            $send_message_enable = $record['send_message_enable'];
            $events_calendar_enable = $record['events_calendar_enable'];
            $logo_enable = $record['logo_enable'];
            $map_enable = $record['map_enable'];
            $address_enable = $record['address_enable'];
            $phone_number_enable = $record['phone_number_enable'];
            $video_enable = $record['video_enable'];
            $video_limit_enable = $record['video_limit_enable']; // first x times videos
            $youtube_video_enable = $record['youtube_video_enable'];
            $image_enable = $record['image_enable'];
            $facility_desc_enable = $record['facility_desc_enable'];
            $staff_enable = $record['staff_enable'];
            $management_enable = $record['management_enable'];
            $activity_enable = $record['activity_enable'];
            $ext_url_enable = $record['ext_url_enable'];
            $image_limit = $record['image_limit']; // first x times images
            $product_limit = $record['product_limit']; // not used
        }
    }





    return array($title, $logo_enable, $map_enable, $address_enable, $phone_number_enable, $youtube_video_enable, $image_enable, $facility_desc_enable, $staff_enable, $management_enable, $activity_enable, $ext_url_enable, $image_limit, $appointment_enable, $vacancy_enable, $send_message_enable, $events_calendar_enable);
// list($title,$logo_enable,$map_enable,$address_enable,$phone_number_enable,$youtube_video_enable,$image_enable,$facility_desc_enable,$staff_enable,$management_enable,$activity_enable,$ext_url_enable,$image_limit)=get_permissions_for_showing();	
}

// Make sure all have appropriate QUICK URL
function does_quick_url_exist($name = '', $id = '') {
    global $db;

    $return = FALSE;
    $sql_queryL = "SELECT id,quick_url FROM " . _prefix("products") . " WHERE quick_url = '$name'  AND id != $id ";
    //echo $sql_queryL;
    $resL = $db->sql_query($sql_queryL);
    while ($missing = $db->sql_fetchrow($resL)) {
        $return = TRUE;
    }

    return $return;
}

function make_url_clean($url) {
    $url = trim(str_replace(',', " ", $url));
    $url = trim(str_replace('/', " ", $url));

    $url = trim(str_replace("&reg;", "", $url));
    $url = trim(str_replace("&amp;", " ", $url));

    $url = preg_replace("/\<.*?\>/", "", $url);
    $url = preg_replace('/[^a-z 0-9*]/i', '', $url);
    $url = trim(str_replace("  ", " ", $url));
    $url = trim(str_replace("  ", " ", $url));
    $url = trim(str_replace("  ", " ", $url));
    // Finished clean of funny characters
    $url = str_replace(" ", "-", $url);
    //echo "$url<br>";
    return $url;
}

function find_missing_quick_url($thisone = '') {
    global $db;

    if ($thisone == '') {
        $sql_queryL = "SELECT id,title,certification_service_type,address_suburb,address_city FROM " . _prefix("products") . " WHERE quick_url = '' ";
    } else {
        $sql_queryL = "SELECT id,title,certification_service_type,address_suburb,address_city FROM " . _prefix("products") . " WHERE id = '$thisone' ";
    }
//    $sql_queryL = "SELECT id,title,certification_service_type,address_suburb,address_city FROM " . _prefix("products");
    $resL = $db->sql_query($sql_queryL);
    while ($missing = $db->sql_fetchrow($resL)) {

        $id = $missing['id'];
        $title = make_url_clean(ucwords(strtolower($missing['title'])));
        $address_suburb = make_url_clean($missing['address_suburb']);
        $address_city = make_url_clean($missing['address_city']);
        $certification_service_type = make_url_clean(strtolower($missing['certification_service_type']));
        $url = 'search/' . $certification_service_type . '/' . $title . '-' . $address_suburb . '-' . $address_city;

        //***********Find them a valid quickurl or add number to the end of the chosen name
        $found = FALSE;
        $count = "";
        while (!$found) {
            if (does_quick_url_exist($url . $count, $id)) {
                // Nope someone else using this uname
                $found = FALSE;
                $count = $count + 1;
            } else {
                $found = TRUE;
                $url = "$url$count";
            }
            //***********Found them a valid quick_url
        } // end while
        //echo "$url<br>";
        //echo "$url<br>";

        $product_missing = array(
            'quick_url' => "$url"
        );
        $productWhere = "where id= '$id'";
        if (pre_check_before_update(_prefix('products'), $product_missing, $productWhere)) {
            $product_update = $db->update(_prefix('products'), $product_missing, $productWhere);
        } else {
            sendmail('Nigel@agedadvisor.co.nz', 'Major Error', 'NO AUTO REPAIR On page /modules/editAccountInfo.php Products entry is missing. Notify Web Admin: accounts@agedadvisor.co.nz', '');

            echo "Products entry is missing. Notify Web Admin: accounts@agedadvisor.co.nz";
            exit;
        }
    }// next while product
}

// end find missing quick_url  
// make sure no one is missing a quick_url   
find_missing_quick_url();









$id = $_SESSION['sp_id'];
$productId = $_SESSION['pr_id'];
$querys = " SELECT pro.*,exinfo.*,member.title as plan,exfaci.no_of_beds,exfaci.no_of_room,exfaci.extra_info1,exfaci.brief FROM " . _prefix("products") . " AS pro"
        . " LEFT JOIN " . _prefix("pro_extra_info") . " AS exinfo ON exinfo.pro_id=pro.id"
        . " LEFT JOIN " . _prefix("extra_facility") . " AS exfaci ON exfaci.product_id=pro.id"
        . " LEFT JOIN " . _prefix("pro_plans") . " AS pln ON pln.pro_id=pro.id"
        . " LEFT JOIN " . _prefix("membership_prices") . " AS member ON member.id=pln.plan_id "
        . " WHERE pro.id =" . $productId . " AND pln.supplier_id=" . $id . " AND pln.status = 1 AND pln.deleted = 0 ORDER BY pln.id DESC";

//echo $querys."<hr>";
$planres = $db->sql_query($querys);
$dataplans = $db->sql_fetchrow($planres);
//print_r($dataplans);
//User Account Details of current supplier
$sql_query = "SELECT user.*  FROM " . _prefix("users") . " AS user"
        . " Left join " . _prefix("sp_payments") . " AS spamount ON spamount.user_id =user.id  WHERE user.id='$id'";
$res = $db->sql_query($sql_query);
$record = $db->sql_fetchrow($res);

//print_r($dataplans);EXIT;
list($title, $logo_enable, $map_enable, $address_enable, $phone_number_enable, $youtube_video_enable, $image_enable, $facility_desc_enable, $staff_enable, $management_enable, $activity_enable, $ext_url_enable, $image_limit, $appointment_enable, $vacancy_enable, $send_message_enable, $events_calendar_enable) = get_permissions_for_showing();
//echo $title;exit;
//prd($record);
//$name = $record['first_name'] . " " . $record['last_name'];
$name = $dataplans['title'];
//echo $name;exit;
//$user_name = $record['user_name'];
//$email = $record['email'];
$accountType = !empty($dataplans) ? $dataplans['plan'] : 'Free';
$ext_url = $dataplans['ext_url']; // Premises website
$youtube_video = $dataplans['youtube_video'];


//$premises_website = $dataplans['premises_website'];
$address = $dataplans['address'];
$address_city = $dataplans['address_city'];
$address_suburb = $dataplans['address_suburb'];
$DHB_name = $dataplans['DHB_name'];
$position = $dataplans['position'];

$email = $dataplans['email'];
$phone = $dataplans['phone'];
$first_name = $dataplans['first_name'];
$last_name = $dataplans['last_name'];
$position = $dataplans['position'];



$brief = $dataplans['brief'];
$description = $dataplans['description'];
$staff_comment = $dataplans['staff_comment'];
$management_comment = $dataplans['management_comment'];
$activity_comment = $dataplans['activity_comment'];
$no_of_room = $dataplans['no_of_room'];
$no_of_beds = $dataplans['no_of_beds'];


//print_r($dataplans);
//$IPaddress = $record['ip_address'];
$amount = $record['price'];
$productLogo = $dataplans['logo'];
$zipcode = $dataplans['zip'];
//$overAll_Rating = overAllRating($id);
$overAll_Rating = OverAllRating($id); // Simple average of users overall ratings

$lat = $dataplans['latitude'];
$long = $dataplans['longitude'];
$certification_service_type = $dataplans['certification_service_type'];
$certificate_name = $dataplans['certificate_name'];
$certification_period = $dataplans['certification_period'];
$certificate_license_end_date = $dataplans['certificate_license_end_date'];
$current_auditor = $dataplans['current_auditor'];
$supplier_website = $record['website'];



if (empty($lat) || empty($long) || $lat == 0 || $long == 0) {
    $coordinates = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address . ', ' . $address_city) . '&sensor=true');

    $coordinates = json_decode($coordinates);
    $lat = isset($coordinates->results[0]->geometry->location->lat) ? $coordinates->results[0]->geometry->location->lat : 0;
    $long = isset($coordinates->results[0]->geometry->location->lng) ? $coordinates->results[0]->geometry->location->lng : 0;
}
//if (empty($lat) && empty($long) && !empty($zipcode)) {
//    $val = getLnt($zipcode);
//    
//    $lat = $val['lat'];
//    $long = $val['lng'];
//}
$sql_service = "select service_id from " . _prefix("pro_services") . " where product_id=" . $productId;
$res_service = $db->sql_query($sql_service);
$records_service = $db->sql_fetchrowset($res_service);
foreach ($records_service as $ids) {
    $service_ids[] = $ids['service_id'];
}

if (isset($update) && $update == 'update') {


    $fields_array = account_info();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        if ($_FILES['productLogo']['error'] == 0) {
            $ImageExt = array("JPG", "PNG", "JPEG", "GIF", "jpg", "png", "jpeg", "gif");
            $ext = end(explode('.', $_FILES['productLogo']['name']));
            if (in_array($ext, $ImageExt)) {
                $file_tmp = $_FILES['productLogo']["tmp_name"];
                $target_pathL = ADMIN_PATH . 'files/product/logo/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $logoName = $timestamp . '_' . imgNameSanitize(basename($_FILES['productLogo']['name']), 20);
                $target_path = $target_pathL . $logoName;
                $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                if (move_uploaded_file($file_tmp, $target_path)) {

                    list($save_strwidth, $save_strheight) = getimagesize($target_path);

                    // Make the largest Image size for zoom

                    $maxsquareheight = 60;
                    $maxsquarewidth = 150;
                    $strwidth = $save_strwidth;
                    $strheight = $save_strheight;
                    if ($strheight > $maxsquareheight) {
                        $strfactor = $maxsquareheight / $strheight;
                        $strheight = round($strheight * $strfactor);
                        $strwidth = round($strwidth * $strfactor);
                    }

                    if ($strwidth > $maxsquarewidth) {
                        $strfactor = $maxsquarewidth / $strwidth;
                        $strheight = round($strheight * $strfactor);
                        $strwidth = round($strwidth * $strfactor);
                    }


                    ak_img_resize($target_path, $target_path_thumb, $strwidth, $strheight, $ext);
                    list($w_orig, $h_orig) = getimagesize($target_path);
                    if ($w_orig > $strwidth || $h_orig > $strheight) {
                        $maxsquareheight = $strheight;
                        $maxsquarewidth = $strwidth;
                        $strwidth = $save_strwidth;
                        $strheight = $save_strheight;
                        if ($strheight > $maxsquareheight) {
                            $strfactor = $maxsquareheight / $strheight;
                            $strheight = round($strheight * $strfactor);
                            $strwidth = round($strwidth * $strfactor);
                        }

                        if ($strwidth > $maxsquarewidth) {
                            $strfactor = $maxsquarewidth / $strwidth;
                            $strheight = round($strheight * $strfactor);
                            $strwidth = round($strwidth * $strfactor);
                        }

                        ak_img_resize($target_path, $target_path, $strwidth, $strheight, $ext);
                    }
                    $productLogoes = $logoName;
                }
            }
//            else {
//                $_SESSION['msg'] = "Please select a valid type of Image";
//            }
        }
        if ($productLogoes == "" || empty($productLogoes)) {
            $productLogoes = $productLogo;
        } else {
            @unlink(ADMIN_PATH . 'files/product/logo/' . $productLogo);
            @unlink(ADMIN_PATH . 'files/product/logo/thumb_' . $productLogo);
        }
// ad_products table
// Use correect syntax of youtube for embedding
        $link = $_POST["youtube_video"];
        $youtube_video = "";
//echo $link;
        preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $link, $matches);
        $matches[0];
//echo "<pre>"; print_r($matches); echo "</pre>";
        if (ISSET($matches[0])) {
            $youtube_video = "https://www.youtube.com/embed/" . $matches[0];
        } else {
            preg_match("/https?:\/\/.*vimeo\.com\/.*\d+/", $link, $matches);
            if (ISSET($matches[0])) {
                $youtube_video = $link;
            }
        }


// Clean up url ext
        $ext_url = $_POST["ext_url"];
        $ext_url = str_ireplace('http://', '', $ext_url);
        $ext_url = str_ireplace('https://', '', $ext_url);
        $ext_url = str_ireplace('ftp://', '', $ext_url);

        $product_logo = array(
            'address' => $_POST["address"],
            'address_suburb' => $_POST["address_suburb"],
            'address_city' => $_POST["address_city"],
            'city_id' => getCityId(trim($_POST["address_city"])),
            //'title' => $_POST["facility_name"],
            // 'user_name' => $_POST["user_name"],
            // 'email' => $_POST["email"],
            'zip' => $_POST["zipcode"],
//            'latitude' => $_POST["latitude"],
//            'longitude' => $_POST["longitude"],
            'logo' => $productLogoes,
            'ext_url' => $ext_url,
            'youtube_video' => "$youtube_video",
            'staff_comment' => $_POST["staff_comment"],
            'management_comment' => $_POST["management_comment"],
            'activity_comment' => $_POST["activity_comment"],
            'description' => $_POST["description"],
            'certification_service_type' => $_POST["certification_service_type"],
            'modified' => date('Y-m-d h:i:s', time())
        );
//        prd($product_logo);
        $productWhere = "where id= '$productId'";
        $product_update = $db->update(_prefix('products'), $product_logo, $productWhere);

///************************inseret appointment date in to ad_product table **************************/
        if (empty($_POST['appoint_day'])) {
            $appoint_day_value = $_POST['appointment_day'];
        } else {
            $appoint_day_value = $_POST['appoint_day'];
        }
        $appointment_date = array(
            'appointment_day' => $appoint_day_value
        );
        $appointment_date_info = "where id='$productId'";
        $appoint_date_update = $db->update(_prefix('products'), $appointment_date, $appointment_date_info);


        /*         * **********************for vacanncy******************************* */
        $vacanncy = array(
            'vacancy' => $_POST['vacanncy']
//'end_appointment_date'=>$_POST['end_appointment']
        );



        $vacanncy_info = "where id='$productId'";
        $vacanncy_info_update = $db->update(_prefix('products'), $vacanncy, $vacanncy_info);

        if (empty($_POST['vacancy_link'])) {
            $vacanncy_link_url = $_POST['vacancy_url'];
        } else {
            $vacanncy_link_url = $_POST['vacancy_link'];
        }

        $vacanncy_link = array(
            'vacancy_external_link' => $vacanncy_link_url
//'end_appointment_date'=>$_POST['end_appointment']         
        );
        $vacanncy_link_info = "where id='$productId'";
        $vacanncy_info_update = $db->update(_prefix('products'), $vacanncy_link, $vacanncy_link_info);




        /*         * ******************end of vacanccy************************************* */



///***********************end query inseret appointment date in to ad_product table **************************/
// ad_extra_facilty table
        $extra_info = array(
            'no_of_beds' => $_POST["no_of_beds"],
            'no_of_room' => $_POST["no_of_room"],
            'brief' => $_POST["brief"],
        );
        $extraWhere = "where product_id= '$productId'";


        if (pre_check_before_update(_prefix('extra_facility'), '', $extraWhere)) {
            $product_beds = $db->update(_prefix('extra_facility'), $extra_info, $extraWhere);
        } else {
            // No the row is missing
            sendmail('Nigel@agedadvisor.co.nz', 'Missing Table Row AUTO FIXED', 'On page /modules/editAccountInfo.php Extra_Facility entry is missing...<br>AUTO FIXED', '');

            echo "Extra_Facility entry is missing...";
            $insert_new_row_extra_facility = array(
                'product_id' => $productId,
                'sp_id' => $_SESSION['id'],
                'status' => 1,
                'created ' => date('Y-m-d h:i:s', time())
            );
            // Create the missing row
            $insert_service = $db->insert(_prefix('extra_facility'), $insert_new_row_extra_facility);
            // Now we can save the results
            $product_beds = $db->update(_prefix('extra_facility'), $extra_info, $extraWhere);
        }
        $queryD = "DELETE FROM " . _prefix("service_locations") . " WHERE `ad_service_locations`.`user_id` = '$id'";
        mysqli_query($db->db_connect_id, $queryD);
        foreach ($locations as $location) {
//                $fields = array(
            $user_id = $_SESSION['id'];
            $location1 = trim($location);
            $created = date('Y-m-d h:i:s', time());
//                );
            if ($location1 != '') {
                $query = 'Insert into ' . _prefix("service_locations") . ' (user_id, locations,created) values("' . $user_id . '","' . $location1 . '","' . $created . '")';
                //$insert_result = $db->insert(_prefix("service_locations"), $fields);
                $insert_result = mysqli_query($db->db_connect_id, $query);
            }
        }
//            $queryD = "DELETE FROM " . _prefix("sp_facility_type") . " WHERE `user_id` = '$id'";
//            mysqli_query($db->db_connect_id, $queryD);
//            foreach ($service_list as $s_list) {
////                $fields = array(
//                $user_id = $_SESSION['id'];
////                $location1 = trim($location);
//                $facility_type = $s_list;
//                $created = date('Y-m-d h:i:s', time());
////                );
//                if ($facility_type != '') {
//                    $query = 'Insert into ' . _prefix("sp_facility_type") . ' (user_id, facility_type, created) values("' . $user_id . '","' . $facility_type . '","' . $created . '")';
//                    //$insert_result = $db->insert(_prefix("service_locations"), $fields);
//                    $insert_result = mysqli_query($db->db_connect_id, $query);
//                }
//            }
        $queryD = "DELETE FROM " . _prefix("pro_services") . " WHERE product_id = '$productId'";
        $delete_service = mysqli_query($db->db_connect_id, $queryD);
        //Inserting services type ids in pro_services table
        if ($delete_service) {
            foreach ($_POST['facility_type'] as $servise_id) {
                $services_field = array(
                    'supplier_id' => $id,
                    'product_id' => $productId,
                    'service_id' => $servise_id,
                    'modified ' => date('Y-m-d h:i:s', time())
                );
                // Message for insert
                $insert_service = $db->insert(_prefix('pro_services'), $services_field);
            }
        }



        // First check is row exists in table ad_pro_extra_info

        $querys = " SELECT id FROM " . _prefix("pro_extra_info") . " WHERE pro_id =" . $productId;
        $find_row = $db->sql_query($querys);
        $found_rows = $db->sql_fetchrowset($find_row);
        if (!is_array($found_rows)) {
            // Insert missing row into table
            $query = 'Insert into ' . _prefix("pro_extra_info") . ' (pro_id,created) values("' . $productId . '","' . date('Y-m-d h:i:s', time()) . '")';
            $insert_result = mysqli_query($db->db_connect_id, $query);
        }// Completed of inserting missing row
        // Update their quickurl
        find_missing_quick_url($productId);

// ad_pro_extra_info  

        if (ISSET($_POST['certificate_license_end_date']) && $_POST['certificate_license_end_date']) {
            $enddate = date("jS F Y", strtotime($_POST['certificate_license_end_date']));
            if ($enddate == '1st January 1970') {
                $enddate = '';
                $_SESSION['msg'] = 'Incorrect date format for Certificate/License End Date. <br>Please re-enter. e.g. 15 March 2021';
            }
        } else {
            $enddate = '';
        }

        $fields = array(
            'email' => trim($_POST['email']),
            'phone' => trim($_POST['phone']),
            'first_name' => trim($_POST['first_name']),
            'last_name' => trim($_POST['last_name']),
            'position' => trim($_POST['position']),
//            'old_email' => trim($_POST['old_email']),
//            'premises_name' => trim($_POST['premises_name']),
//            'certification_service_type' => trim($_POST['certification_service_type']),
            //'service_types' => trim($_POST['service_types']),
            'premises_website' => trim($_POST['premises_website']),
            'premises_address_other' => trim($_POST['premises_address_other']),
//            'dhb_name' => $_POST['DHB_name'],
            'certificate_name' => trim($_POST['certificate_name']),
            'certification_period' => trim($_POST['certification_period']),
            'certificate_license_end_date' => $enddate,
            'dhb_name' => $_POST["DHB_name"],
            'certification_service_type' => $_POST["certification_service_type"],
            'current_auditor' => $_POST["current_auditor"],
            //'current_auditor' => trim($_POST['current_auditor']),
            'modified' => date('Y-m-d h:i:s', time())
        );




        $where = "where pro_id = $productId";

        //
//        prd($fields);
        if (pre_check_before_update(_prefix('pro_extra_info'), '', $where)) {
            // Yes it exists
            $update_result = $db->update(_prefix('pro_extra_info'), $fields, $where);
        } else {
            // No the row is missing
            sendmail('Nigel@agedadvisor.co.nz', 'Missing Table Row AUTO FIXED', 'On page /modules/editAccountInfo.php Pro_Extra_Info data is missing...<br>AUTO FIXED', '');


            $insert_new_row = array(
                'pro_id' => $productId,
                'deleted' => 0,
                'status' => 1,
                'created ' => date('Y-m-d h:i:s', time())
            );
            // Create the missing row
            $insert_service = $db->insert(_prefix('pro_extra_info'), $insert_new_row);
            // Now save the results
            $update_result = $db->update(_prefix('pro_extra_info'), $fields, $where);
        }
        if ($insert_result || $user_update || $product_update) {
            // Message for insert
            if ($_SESSION['msg'] == '') {
                $msg = common_message(1, constant('ACCOUNT_INFO_UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(HOME_PATH . "facility-info");
            }
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

function fList() {
    global $db;
    $id = $_SESSION['id'];
    $sql_queryCategory = "SELECT id, name FROM " . _prefix("services") . " where status = 1 AND deleted = 0";
    $resCategory = $db->sql_query($sql_queryCategory);
    $CategoryRecords = $db->sql_fetchrowset($resCategory);

    $sql_queryFacility = "SELECT facility_type FROM " . _prefix("sp_facility_type") . " where status = 1 AND deleted = 0 AND user_id = '$id'";
    $resFacility = $db->sql_query($sql_queryFacility);
    $ListFacilitys = $db->sql_fetchrowset($resFacility);
    $facility = array();
    foreach ($ListFacilitys as $ListFacility) {
        $facility[] = $ListFacility['facility_type'];
    }
    foreach ($CategoryRecords as $categoryrecord) {
        if (in_array($categoryrecord['id'], $facility)) {
            $selected = "selected";
        } else {
            $selected = '';
        }
        $option .= '<option value="' . $categoryrecord['id'] . '"' . $selected . '>' . $categoryrecord['name'] . ' </option>';
    }
    return $option;
}

function cityListselect() {
    global $db;
    $id = $_SESSION['id'];
    $cityList = array();
    $sql_queryCity = "select id, title from " . _prefix('cities') . " ORDER BY title ASC";
    $resCity = $db->sql_query($sql_queryCity);
    $CityRecords = $db->sql_fetchrowset($resCity);
    $sql_queryL = "SELECT locations FROM " . _prefix("service_locations") . " where deleted=0 AND user_id ='$id'";
    $resL = $db->sql_query($sql_queryL);
    $LocationRecords = $db->sql_fetchrowset($resL);
    $city = array();
    foreach ($LocationRecords as $LocationRecord) {
        $city[] = $LocationRecord['locations'];
    }
    foreach ($CityRecords as $cityrecord) {
        if (in_array($cityrecord['id'], $city)) {
            $selected = "selected";
        } else {
            $selected = '';
        }
        $option .= '<option value="' . $cityrecord['id'] . '"' . $selected . '>' . $cityrecord['title'] . ' </option>';
    }
    return $option;
    // return options($cityList, $selected);
}
?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">

    var map;
    var marker;
    var lat = <?php echo $lat; ?>;
    var lang = <?php echo $long; ?>;
    var myLatlng = new google.maps.LatLng(lat, lang);
    var geocoder = new google.maps.Geocoder();
    var infowindow = new google.maps.InfoWindow();
    function initialize() {
        var mapOptions = {
            zoom: 7,
            center: myLatlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        map = new google.maps.Map(document.getElementById("map"), mapOptions);

        marker = new google.maps.Marker({
            map: map,
            position: myLatlng,
            draggable: true
        });

        geocoder.geocode({'latLng': myLatlng}, function (results, status) {
            console.log(results);
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $('#address').val(results[0].formatted_address);
                    $('#latitude').val(marker.getPosition().lat());
                    $('#longitude').val(marker.getPosition().lng());
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                }
            }
        });


        google.maps.event.addListener(marker, 'dragend', function () {

            geocoder.geocode({'latLng': marker.getPosition()}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        $('#address').val(results[0].formatted_address);
                        $('#latitude').val(marker.getPosition().lat());
                        $('#longitude').val(marker.getPosition().lng());
                        var latlng = new google.maps.LatLng($('#latitude').val(), $('#longitude').val());
                        geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'latLng': latlng}, function (results, status) {
//                            if (status == google.maps.GeocoderStatus.OK) {
//                                if (results[0]) {
//                                    for (j = 0; j < results[0].address_components.length; j++) {
//                                        if (results[0].address_components[j].types[0] == 'postal_code') {
//                                            $('#zipcodeValue').val(results[0].address_components[j].short_name);
//                                        }
//                                    }
//                                }
//                            }
                        });


                        infowindow.setContent(results[0].formatted_address);
                        infowindow.open(map, marker);
                    }
                }
            });
        });

    }

    google.maps.event.addDomListener(window, 'load', initialize);


</script>

<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success"v>
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php // require_once 'includes/sp_left_navigation.php';  ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-pencil-square-o"></i> <span>Edit Facility Information</span></h2>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left">
                        <form method="POST" name="accountAddress" id="accountAddress" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
                            <input type="hidden" id="address" value=""/>
                            <input type="hidden" id="latitude" name="latitude" value="<?php echo $lat; ?>"/>
                            <input type="hidden" id="longitude" name="longitude" value="<?php echo $long; ?>" />
                            <div class="row margin-bottom-20">
                                <div style="width:100%;"><button type="submit" id="submit1" value="update" name="update" class="btn btn-danger center-block btn_search btn_pay pull-right" style="margin: 0 0 0 5px">update</button><a class="pull-right btn btn-info" href="<?php echo HOME_PATH ?>facility-info">Go Back</a></div>

                            </div>

                            <ul class="list-group">

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Upload Facility logo</span>
                                        </div>    
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <?php if ($logo_enable) { ?>
                                                <input name="productLogo" id="image" class="form-control_browse input_fild_img_browse" type="file">
                                            <?php } else {
                                                echo $update_to_enable;
                                            } ?>
                                        </div>
                                        <div class="col-md-12 text-center ">
                                            <?php if (file_exists(DOCUMENT_PATH . 'admin/files/product/logo/' . $productLogo) && $productLogo != '' && $accountType != 'Free') { ?>
                                                <img title="User Image" src="<?php echo MAIN_PATH . 'files/product/logo/' . $productLogo ?>"/>
                                            <?php } else { ?>
                                                <a href="<?php echo HOME_PATH . 'supplier/editAccountInfo' ?>"><img  title="User Image" src="<?php echo IMAGES; ?>notlogo.jpeg"/></a>
<?php } ?>
                                        </div>
                                    </div>
                                </li>


                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Facility Name</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control" value='<?php echo $name; ?>' name='facility_name' maxlength="100" readonly>To update Facility Name please contact our <a href="<?php echo HOME_PATH . 'supplier/issue_mail' ?>">Administration Team</a></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Facility Type/s<br></span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <?php
                                            global $db;

                                            $sql_queryL = "SELECT * FROM " . _prefix("services") . " WHERE  status=1 AND deleted=0 ORDER BY order_by DESC";

                                            $results = $db->sql_query($sql_queryL);
                                            if ($results) {
                                                while ($rows = mysqli_fetch_row($results)) {
                                                    ?>
                                                    <input type="checkbox" name="facility_type[]" value="<?php echo  $rows[0] ?>"  id="id<?php echo  $rows[0] ?>"
                                                    <?php
                                                    if (is_array($service_ids)) {
                                                        foreach ($service_ids as $item) {
                                                            if ($item == $rows[0]) {
                                                                echo "checked ";
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                           >&nbsp;<label for="id<?php echo  $rows[0] ?>"><?php echo  $rows[1] ?></label><br>
                                                           <?php
                                                       }
                                                   }
                                                   ?>                                    </div>
                                    </div>
                                </li>

                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Overall Rating</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><?php echo $overAll_Rating; ?></span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
<?php /* -----------------------code for appointment available date  of product---------------------------- */ ?>
                            <h4 class="logo_bottom_color font_size_16n ">Appointment Date</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Start Available Day</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <?php
                                            if (!$appointment_enable) {
                                                echo $update_to_enable;
                                            } else {
                                                ?>

                                                <span class="right_td"><input id="datePick" type="text"  name ="appoint_day"/></span>

                                                <?php
                                                $check_day = mysqli_query($db->db_connect_id, "select `appointment_day` from ad_products where id='$productId'") or mysqli_error("error problem in appointment");

                                                while ($check_avail_day = mysqli_fetch_array($check_day)) {
                                                    $check_avail_day1 = $check_avail_day['appointment_day'];
                                                }
                                                ?>

                                                <div class="clearfix"></div>
                                                <span class="right_td"><input id="appointment_day" type='text' class="col-sm-4 form-control"  name="appointment_day" value="<?php echo $check_avail_day1; ?>" readonly="readonly"></span>

                                            <?php } ?>
       <!-- <span class="pull-left right_td"><input id="start_appointment" type='datetime-local' class="col-sm-4 form-control" value="" name='start_appointment'></span>
                                            -->
                                            <?php
                                            /*
                                              $check_day=mysqli_query($db->db_connect_id, "select * from ad_products where id='$proId'") or mysqli_error("error problem in appointment");
                                              while($check_avail_day=mysqli_fetch_array($check_day))
                                              {
                                              $check_avail_day1=$check_avail_day['appointment_day'];
                                              ?>
                                              <?php
                                              //if (preg_match('/are/',$a))
                                              if($check_value=preg_match("/Monday/",$check_avail_day1)){
                                              ?>
                                              <input id="start_appointment1"  name="appoint_day[]" type='checkbox' class="col-sm-4 form-control"  value="Monday"<?php if($check_value==1)echo 'checked="checked"';?>  >&nbsp;<label for="mon">Monday</label></br>
                                              <?php  }  else {?>
                                              <input id="start_appointment1"  name="appoint_day[]" type='checkbox' class="col-sm-4 form-control"  value="Monday">&nbsp;<label for="mon">Monday</label></br>
                                              <?php }
                                              if($check_value=preg_match("/Tuesday/",$check_avail_day1)){
                                              ?>
                                              <input name="appoint_day[]" id="start_appointment2" type='checkbox' class="col-sm-4 form-control"  value="Tuesday"<?php if($check_value==1)echo 'checked="checked"';?>> &nbsp; <label for="tue">Tuesday</label></br>
                                              <?php } else { ?>
                                              <input name="appoint_day[]" id="start_appointment2" type='checkbox' class="col-sm-4 form-control"  value="Tuesday"> &nbsp; <label for="tue">Tuesday</label></br>
                                              <?php }
                                              if($check_value=preg_match("/Wednesday/",$check_avail_day1)){
                                              ?>
                                              <input name="appoint_day[]" id="start_appointment3" type='checkbox' class="col-sm-4 form-control"  value="Wednesday" <?php if($check_value==1)echo 'checked="checked"';?>> &nbsp; <label for="wed">Wednesday</label></br>
                                              <?php }
                                              else { ?>
                                              <input name="appoint_day[]" id="start_appointment3" type='checkbox' class="col-sm-4 form-control"  value="Wednesday"> &nbsp; <label for="wed">Wednesday</label></br>
                                              <?php  }
                                              if($check_value=preg_match("/Thursday/",$check_avail_day1)){
                                              ?>
                                              <input name="appoint_day[]" id="start_appointment4" type='checkbox' class="col-sm-4 form-control"  value="Thursday" <?php if($check_value==1)echo 'checked="checked"';?>> &nbsp; <label for="Thur">Thursday</label></br>

                                              <?php } else { ?>
                                              <input name="appoint_day[]" id="start_appointment4" type='checkbox' class="col-sm-4 form-control"  value="Thursday"> &nbsp; <label for="Thur">Thursday</label></br>
                                              <?php } if($check_value=preg_match("/Friday/",$check_avail_day1)){
                                              ?>
                                              <input name="appoint_day[]" id="start_appointment5" type='checkbox' class="col-sm-4 form-control"  value="Friday" <?php if($check_value==1)echo 'checked="checked"';?>> &nbsp; <label for="fri">Friday</label></br>
                                              <?php } else { ?>
                                              <input name="appoint_day[]" id="start_appointment5" type='checkbox' class="col-sm-4 form-control"  value="Friday"> &nbsp; <label for="fri">Friday</label></br>
                                              <?php } if($check_value=preg_match("/Saturday/",$check_avail_day1)){
                                              ?>
                                              <input name="appoint_day[]" id="start_appointment6" type='checkbox' class="col-sm-4 form-control"  value="Saturday" <?php if($check_value==1)echo 'checked="checked"';?>> &nbsp; <label for="sat"> Saturday</label></br>
                                              <?php } else {
                                              ?>
                                              <input name="appoint_day[]" id="start_appointment6" type='checkbox' class="col-sm-4 form-control"  value="Saturday"> &nbsp; <label for="sat"> Saturday</label></br>
                                              <?php } if($check_value=preg_match("/Sunday/",$check_avail_day1))  {
                                              ?>
                                              <input name="appoint_day[]" id="start_appointment7" type='checkbox' class="col-sm-4 form-control"  value="Sunday" <?php if($check_value==1)echo 'checked="checked"';?>> &nbsp; <label for="sun"> Sunday</label>
                                              <?php  } else { ?>
                                              <input name="appoint_day[]" id="start_appointment7" type='checkbox' class="col-sm-4 form-control"  value="Sunday"> &nbsp; <label for="sun"> Sunday</label>
                                              <?php
                                              }
                                              } */
                                            ?>  
                                        </div>
                                    </div>
                                </li>
                                <!-- <li class="list-group-item">
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <span class="pull-left left_td">Start Available Date</span>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <span class="pull-left right_td"><input id="end_appointment" type='datetime-local' class="col-sm-4 form-control" value="" name='end_appointment'  ></span>
                                                </div>
                                            </div>
                                        </li>-->
                            </ul>
                            <!--end of  code for appointment available date  of product--> 
                            <!--code for vacanncy available date  of product-->
                            <h4 class="logo_bottom_color font_size_16n ">Vacancy</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Vacancy</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

                                            <?php
                                            // $check_avail_day=mysqli_fetch_array($check_day)
                                            // echo $vacancy_enable ;
                                            if ($vacancy_enable) {


                                                $check_day = mysqli_query($db->db_connect_id, "select `vacancy`,`vacancy_external_link` from ad_products where id='$productId'") or mysqli_error("error problem in appointment");
                                                while ($check_avail_day = mysqli_fetch_array($check_day)) {
                                                    $vacancy_day = $check_avail_day['vacancy'];
                                                    // echo hello ;
                                                    $vacancy_day_link = $check_avail_day["vacancy_external_link"];


                                                    if ($check_value = preg_match("/Enquire about availability/", $vacancy_day)) {
                                                        ?> 

                                                        <input id="vacanncy1"  name="vacanncy" type='radio' class="vacancy-button"  value="Enquire about availability"<?php if ($check_value == 1) echo 'checked="checked"'; ?>  >&nbsp;<label for="enq">Enquire about availability</label></br>

        <?php } else { ?>

                                                        <input id="vacanncy1"  name="vacanncy" type='radio' class="vacancy-button"  value="Enquire about availability">&nbsp;<label for="mon">Enquire about availability</label></br>  

        <?php } if ($check_value = preg_match("/Vacancy soon/", $vacancy_day)) { ?>

                                                        <input name="vacanncy" id="vacanncy2" type='radio' class="vacancy-button"  value="Vacancy soon"<?php if ($check_value == 1) echo 'checked="checked"'; ?>> &nbsp; <label for="tue">Vacancy soon</label></br>

        <?php } else { ?>

                                                        <input name="vacanncy" id="vacanncy2" type='radio' class="vacancy-button"  value="Vacancy soon"> &nbsp; <label for="tue">Vacancy soon</label></br>	 

        <?php } if ($check_value = preg_match("/Selling Now/", $vacancy_day)) { ?>

                                                        <input name="vacanncy" id="vacanncy3" type='radio' class="vacancy-button"  value="Selling Now" <?php if ($check_value == 1) echo 'checked="checked"'; ?>> &nbsp; <label for="wed">Selling Now</label></br>

        <?php } else { ?>

                                                        <input name="vacanncy" id="vacanncy3" type='radio' class="vacancy-button"  value="Selling Now"> &nbsp; <label for="wed">Selling Now</label></br>

        <?php } if ($check_value = preg_match("/Beds Available/", $vacancy_day)) { ?>

                                                        <input name="vacanncy" id="vacanncy4" type='radio' class="vacancy-button"  value="Beds Available" <?php if ($check_value == 1) echo 'checked="checked"'; ?>> &nbsp; <label for="Thur">Beds Available</label></br>

        <?php } else { ?>

                                                        <input name="vacanncy" id="vacanncy4" type='radio' class="vacancy-button"  value="Beds Available"> &nbsp; <label for="Thur">Beds Available</label></br>  

        <?php } if ($check_value = preg_match("/Rooms Available/", $vacancy_day)) { ?>

                                                        <input name="vacanncy" id="vacanncy5" type='radio' class="vacancy-button"  value="Rooms Available" <?php if ($check_value == 1) echo 'checked="checked"'; ?>> &nbsp; <label for="fri">Rooms Available</label></br>

                                                    <?php } else { ?>
                                                        <input name="vacanncy" id="vacanncy5" type='radio' class="vacancy-button"  value="Rooms Available"> &nbsp; <label for="fri">Rooms Available</label></br>

                                                        <?php
                                                    }
                                                }
                                            } else {
                                                echo $update_to_enable;
                                            }
                                            ?>  
                                        </div>
                                    </div>
                                </li>
                                <!-- ***************hidden field for text******************-->
                                <!-- ****************end of code for text****************-->
                                <li class="list-group-item" id="vacancy_link">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Vacanncy External Link</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull"><input  type='text' class="col-sm-4 form-control" value="" name='vacancy_link' style="width:100%"></span></br>

                                            <span class="pull">
                                                <input  type='text' style="width:100%"  name="vacancy_url" id="vacancy_ext_link1" class="col-sm-4 form-control" value="<?php echo $vacancy_day_link; ?>" readonly="readonly" >
                                            </span>        
                                        </div>
                                    </div>
                                </li>

                            </ul>


<?php /* ---------------------------end of code for vacanncy detail ----------------------------------- */ ?>
                            <h4 class="logo_bottom_color font_size_16n ">Account Plan</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Account Type</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><?php echo $accountType; ?></span>
                                        </div>
                                    </div>
                                </li>
                                <?php if($logo_enable==0){?><li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Apply for Upgrade</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <!-- <span class="pull-left right_td"><a href="<?php //echo HOME_PATH . 'supplier/payment'   ?>">Return to Facility Listing</a> to Apply</span> -->
                                        </div>
                                    </div>
                                </li><?php } ?>
                            </ul>
                            <h4 class="logo_bottom_color font_size_16n ">Contact Details</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">First Name</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control" value='<?php echo $first_name; ?>' name='first_name' maxlength="100" ></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Last Name</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control" value='<?php echo $last_name; ?>' name='last_name' maxlength="100" ></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Position</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control" value='<?php echo $position; ?>' name='position' maxlength="255" ></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Email</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control" value='<?php echo $email; ?>' name='email' maxlength="255" ></span>
                                        </div>
                                    </div>
                                </li>
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Phone</span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control" value='<?php echo $phone; ?>' name='phone' maxlength="21" ><?if($phone_number_enable){echo 'Displayed on Listing Page';}else{echo 'Hidden on Listing Page. '.$update_to_enable;}?></span>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            <h4 class="logo_bottom_color font_size_16n ">Address Details</h4>
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <span class="pull-left left_td">Street/Road<span class="redCol">* </span></span>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <span class="pull-left right_td"><input type="text" maxlength="255"  name="address" class="form-control col-sm-4 required" id="description" value="<?php
                                                                                    if ((isset($address)) && !empty($address)) {
                                                                                        echo stripslashes($address);
                                                                                    }
                                                                                    ?>">
<?php if (!$address_enable) { ?><br>Exact address hidden on listing page. Only suburb shown. <?php echo  $update_to_enable ?><?php }?></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Suburb<span class="redCol">* </span></span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control  required" value='<?php echo $address_suburb; ?>' name='address_suburb' maxlength="255" ></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Town/City<span class="redCol">* </span></span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control required" value='<?php echo $address_city; ?>' name='address_city' maxlength="255" ></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Post Code</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control" value='<?php echo $zipcode; ?>' name='zipcode' maxlength="14" ></span>
                                            </div>
                                        </div>
                                    </li>                                
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Country</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td">New Zealand</span>
                                            </div>
                                        </div>
                                    </li>                                
                                </ul>
                                <h4 class="logo_bottom_color font_size_16n ">Other Media Links</h4>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td marginleft8">Website URL:</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="ext_url" type='text' class="col-sm-4 form-control" value='<?php echo $ext_url ?>' name='ext_url' maxlength="255" ><?if($ext_url_enable){echo 'Displayed on Listing Page';}else{echo 'Hidden on Listing Page. '.$update_to_enable;}?></span><span class="pull-left right_td">
                                                    <input type="checkbox" name="supwebsite" id="supwebsite" class="form-control" value="<?php echo $supplier_website; ?>"> <label for="supwebsite">Click the above check box to use Supplier website as default.</label></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Youtube Video URL:</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="zipcodeValue" placeholder='e.g. https://www.youtube.com/watch?v=e-ORhEE9VVg' type='text' class="col-sm-6 col-md-6 col-lg-6 form-control" value='<?php echo $youtube_video; ?>' name='youtube_video' maxlength="60" ><?if($youtube_video_enable){echo 'Displayed on Listing Page';}else{echo 'Hidden on Listing Page. '.$update_to_enable;}?></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <h4 class="logo_bottom_color font_size_16n ">Facility Description</h4>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Brief Facility Description<span class="redCol">* </span></span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><textarea rows="4" cols="39" maxlength="255"  name="brief" class="form-control input_fild_1 " id="brief"><?php
                                                        if ((isset($brief)) && !empty($brief)) {
                                                            echo stripslashes($brief);
                                                        }
                                                        ?></textarea></span>
                                            </div>
                                        </div>
                                    </li>


                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Full Facility Description</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><textarea rows="4" cols="39" maxlength="750"  name="description" class="form-control input_fild_1 " id="description"><?php
                                                        if ((isset($description)) && !empty($description)) {
                                                            echo stripslashes($description);
                                                        }
                                                        ?></textarea></span>
                                            </div>
                                        </div>
                                    </li>


                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Staff Description</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><textarea rows="4" cols="39" maxlength="750"  name="staff_comment" class="form-control input_fild_1" id="staff_comment"><?php
                                                        if ((isset($staff_comment)) && !empty($staff_comment)) {
                                                            echo stripslashes($staff_comment);
                                                        }
                                                        ?></textarea></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Management Description</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><textarea rows="4" cols="39" maxlength="750"  name="management_comment" class="form-control input_fild_1 " id="management_comment"><?php
                                                        if ((isset($management_comment)) && !empty($management_comment)) {
                                                            echo stripslashes($management_comment);
                                                        }
                                                        ?></textarea></span>
                                            </div>
                                        </div>
                                    </li>

                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Activities Description</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><textarea rows="4" cols="39" maxlength="750"  name="activity_comment" class="form-control input_fild_1 " id="activity_comment"><?php
                                                        if ((isset($activity_comment)) && !empty($activity_comment)) {
                                                            echo stripslashes($activity_comment);
                                                        }
                                                        ?></textarea></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Total Rooms</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="no_of_room" type='text' class="col-sm-4 form-control" value='<?php echo $no_of_room; ?>' name='no_of_room' maxlength="10" ></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Total Beds</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control" value='<?php echo $no_of_beds; ?>' name='no_of_beds' maxlength="10" ></span>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <h4 class="logo_bottom_color font_size_16n ">DHB Information (Applies only to Aged Care)</h4>
                                <ul class="list-group">

                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">DHB Name</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="DHB_name" type='text' class="col-sm-4 form-control" value='<?php echo $DHB_name; ?>' name='DHB_name' maxlength="255" ></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Certificate Name</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="zipcodeValue" type='text' class="col-sm-4 form-control" value='<?php echo $certificate_name; ?>' name='certificate_name' maxlength="255" ></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Certification Service Type</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td">
                                                    <select class="form-control select_option" name="certification_service_type" id='certification_service_type'>
                                                        <option value="" <?if($certification_service_type==''){echo"selected";}?>>--------Select--------</option>
                                                        <option value="Aged Care" <?if($certification_service_type=='Aged Care'){echo"selected";}?>>Aged Care</option>
                                                        <option value="Retirement Village" <?if($certification_service_type=='Retirement Village'){echo"selected";}?>>Retirement Village</option>
                                                        <option value="Other" <?if($certification_service_type=='Other'){echo"selected";}?>>Other</option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Certification Period (Months)</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td">
                                                    <select class="form-control select_option" name="certification_period" id='certification_period'>
                                                        <option value="12a" <?if($certification_period=='12a'){echo"selected";}?>>12 months (due to new manager employed)</option>
                                                        <option value="12b" <?if($certification_period=='12b'){echo"selected";}?>>12 months (due to new ownership)</option>
                                                        <option value="12c" <?if($certification_period=='12c'){echo"selected";}?>>12 months (due to new level of care introduced)</option>
                                                        <option value="12d" <?if($certification_period=='12d'){echo"selected";}?>>12 months (due to being a new facility)</option>
                                                        <option value="12" <?if($certification_period=='12'){echo"selected";}?>>12 months</option>
                                                        <option value="24" <?if($certification_period=='24'){echo"selected";}?>>24 months</option>
                                                        <option value="36" <?if($certification_period=='36'){echo"selected";}?>>36 months</option>
                                                        <option value="48" <?if($certification_period=='48'){echo"selected";}?>>48 months</option>
                                                        <option value="60" <?if($certification_period=='60'){echo"selected";}?>>60 months</option>

                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Certificate/License End Date</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="expiry_date" name="certificate_license_end_date"  class="col-sm-4 form-control" type="text" value="<?php
                                                                                        if ($certificate_license_end_date) {
                                                                                            echo stripslashes($certificate_license_end_date);
                                                                                        } else {
                                                                                            echo 'i.e. 15 March 2021';
                                                                                        }
                                                                                        ?>"  onfocus="this.value = '';"></span>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Current Auditor</span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td"><input id="current_auditor" type='text' class="col-sm-4 form-control" value='<?php echo $current_auditor; ?>' name='current_auditor' maxlength="255" ></span>
                                            </div>
                                        </div>
                                    </li>
                                    <!--<li class="list-group-item">
                                        <div class="row">
                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <span class="pull-left left_td">Service Centre Location(s)<span class="redCol">* </span></span>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <span class="pull-left right_td">
                                                    <select class="" name="locations[]" multiple="true"><?php echo cityListselect(); ?></select>
                                                </span>

                                            </div>-->
                                    <!--                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                        <a class="add_locations" id="<?php // echo ($i > 1) ? $i - 1 : '1';                                                                                                                                   ?>" href="#"> Add Location</a>
                                                                        <a class="add_locations" id="<?php // echo count($LocationRecords);                                                                        ?>" href="javascript:void(0)"> Add Location</a>
                                                                        <span id="contain_input" class="pull-left right_td">
                                    <?php
//                                            $i = 1;
//                                            foreach ($LocationRecords as $record) {
//                                                echo '<input type="text" name="locations[]" id="locations_"' . $i . ' value="' . $record['locations'] . '" ><br>';
//                                                $i++;
//                                            }
                                    ?>
                                                                        </span>
                                                                    </div>
                                </div>
                            </li>-->

                                    <!--                                <li class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                                <span class="pull-left left_td">Last Payment Made</span>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                                <span class="pull-left right_td">$<?php echo isset($amount) ? stripslashes($amount) : '0'; ?></span>
                                                                            </div>
                                                                        </div>
                                                                    </li>-->


                                    <!--                                <li class="list-group-item">
                                                                        <div class="row">
                                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                                                <span class="pull-left left_td">Services Listed<span class="redCol">* </span></span>
                                                                            </div>
                                                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                                                <span class="pull-left right_td">
                                                                                    <select class="required" name="service_list[]" multiple="true"><?php // echo fList();                                  ?></select>
                                                                                </span>
                                    
                                                                            </div>
                                                                        </div>
                                                                    </li>-->
                                    <!--Descriptive details here -->                                                               
                                </ul>
                                <div class="row pull-right">
                                    <div style="width:100%;"><input type="submit" id="submit1" value="update" name="update" class="btn btn-danger center-block btn_search btn_pay"></div>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
        label.error{
            color:red;
            font-size: 10px;
        }
        input{
            color:black;
            font-size: 15px;
        }

    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var id = '<?php echo $_SESSION['id']; ?>';
            setTimeout(function () {

                $("#success").hide('slow');
                $('#error').hide('slow');
            }, 5000);
            var path = "<?php echo HOME_PATH; ?>admin/";
            jQuery.validator.addMethod("alphaNumSpL", function (value, element) {
                return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
            }, "Please enter valid last name");

            jQuery.validator.addMethod("noSpace", function (value, element) {
                return value.indexOf(" ") < 0 && value != "";
            }, "Space is not allowed");
            $('#accountAddress').validate({
                rules: {

                    certification_service_type: {
                        required: true
                    },
                    address: {
                        required: true,
                        minlength: 3,
                        maxlength: 255
                    },
                    email: {
                        //required: true,
                        email: true,
                        remote: {
                            url: path + 'ajax.php',
                            type: 'POST',
                            data: {action: "lookUnquieEmail", email: function () {
                                    return $('#email').val();
                                }
                            }
                        }
                    },
                    user_name: {
                        required: true,
                        minlength: 6,
                        maxlength: 50,
                        remote: {
                            url: path + 'ajax.php',
                            type: 'POST',
                            data: {action: "lookUnquieUsername", username: function () {
                                    return $('#username').val();
                                }
                            }
                        }
                    }
                },
                messages: {
                    certification_service_type: {
                        required: " Certification Service Type is required"
                    },
                    address: {
                        required: "This field is required",
                        minlength: "Address must be contain at least 3 chars",
                        maxlength: "Address must be contain maximum 255 chars"
                    }, email: {
                        required: "This field is required",
                        email: "Please enter a valid email address.",
                        remote: "Email address already in use. Please use other email."
                    },
                    user_name: {
                        required: "This field is required",
                        remote: "User name already in use. Please use other User name",
                        minlength: "User name  must contain at least 6 chars",
                        maxlength: "User name  should not be more than 30 characters"
                    }
                }
                , submitHandler: function (form) {
                    $('#submit1').attr('disabled', 'disabled');
                    form.submit();

                }
            });

            $('.add_locations').click(function () {
                var id = parseInt($(this).attr('id')) + 1;
    //            alert("fhgfyh" + id);
    //            die;
    //            var cssClass = 'AlTr1';
    //            var addClass = 'AlTr2';
                text = '';
                text += '<input type="text" name="locations[]" id="locations_"' + id + ' value="" /><br>';
    //            text += '<tr class="' + cssClass + '"><td>' + id + '.</td>';
    //            text += '<td><input id="location_"' + id + ' type="text" name="locations[]"  value="" style="width :206px" class="txtbox location" placeholder="Skill Name"></td>';
    //            text += '<td>Experience &nbsp;<input type="text" name="exp_month[]"  value="" style="width :60px;" class="txtbox number" placeholder="Months"> &nbsp; (in months)</td>';
    //            text += '</tr>';
                $('#contain_input').append(text);
    //            $('.add_locations').attr('id', id);
    //            $('.add').attr(' class', "add " + cssClass + "");
    //            $('.submit_buttons').attr(' class', "submit_buttons " + addClass + "");
            });
            var defaultval = '<?php echo $supplier_website; ?>';
            $("#supwebsite").change(function () {
                if (this.checked) {
                    $("#ext_url").val($(this).val());
                } else {
                    $("#ext_url").val(defaultval);
                }
            });

            $('.submit_add').click(function () {

                var location = 0;
                $('.location').each(function () {
                    var data = $.trim($(this).val());
                    if (data == '') {
                        alert('Skill is required !!!');
                        location++;
                        return false;
                    }
                });
                if (location > 0) {
                    return false;
                }
                var count = 0;
                $('.number').each(function () {
                    var data = $.trim($(this).val());
                    if (data == '') {
                        alert('Experience in month is required !!!');
                        count++;
                        return false;
                    }
                    if (isNaN(data) == true || data.indexOf('.') != -1) {
                        alert('Experience in month should be Numeric !!!');
                        var id = $(this).attr('id');
                        $('#' + id + '').focus();
                        count++;
                        return false;
                    }
                });
                if (count > 0 || location > 0) {
                    return false;
                }
            });
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            var vac_value = $("input[type='radio']:checked").val();
            if (vac_value == 'Vacancy soon' || vac_value == 'Enquire about availability' || vac_value == '' || vac_value == null)
            {
                $("#vacancy_link").hide(200);
            } else
            {
                $("#vacancy_link").show(200);
            }
            $("input[type='radio']").change(function () {
                var vacancy_value = $("input[type='radio']:checked").val();
                dataString = 'val=' + vacancy_value + "&product_id=" +<?php echo $productId; ?>;
            url = '/modules/facility_login/ajax_vaca.php';
            $.ajax({
                url: url,
                type: "POST",
                data: dataString,
                success: function (result) {
                    // console.log(result);
                    $("#ajax_result").val(vacancy_value);
                    var vac_ext_link = $("#ajax_result").val();
                    if (vac_ext_link == 'Vacancy soon' || vac_ext_link == 'Enquire about availability')
                    {
                        $("#vacancy_link").hide(200);
                    } else
                    {
                        $("#vacancy_link").show(200);
                        $("#vacancy_ext_link1").val(result)
                    }
                }
            });
        });
    });
</script>

<script src="http://multidatespickr.sourceforge.net/jquery-ui.multidatespicker.js"></script>
<script>
    $(function () {
        $("#datePick").datepicker({minDate: 0});
    });
</script>
<!--<script type="text/javascript">
    $(document).ready(function() {
        $('#facility_type_id').multiselect();
    })
</script>-->

