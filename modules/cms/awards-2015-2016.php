 
        <div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH;?>/modules/pages/images/bg/memphis-colorful.png);height:250px;">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row align-items-center  pad-top-80">
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu">
						<h2 class="page-title" style="font-size: 30px; color:#141414;">Awards 2019</h2>
                            
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu text-right sm-left">
                            <ul>
                                <li class="active"><a href="<?php echo HOME_PATH;?>">Home</a></li>
                                <li><a href="<?php echo HOME_PATH;?>awards">Awards</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->
        <!--Blog post starts-->
        <div class="blog-area section-padding pad-bot-40 mar-top-20">
            <div class="container">
                <div class="row">
				
				
				
                    <div class="col-md-12">
                        <div class="single-blog-item v3">
						 <div class="single-blog-item v3">
						 	 	 <div class="row">
					         <div class="col-md-12">
					     
                            <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Finalists &amp; Winners of 2015/16</span><br>
                               <span style="color:#696969">'Peoples Choice Awards'</span><br>
                                 <span style="color:#FF0000">for Best Retirement Villages and Best Aged Care.</span></h1>
                              </div>
                                   </div>
								    <div class="row">
					         <div class="col-md-12">
					       
                               <div class="text-center">
                             <img src="<?php echo HOME_PATH; ?>images/2017awards.jpg" alt="...">  
                                 </div> 
                              </div>
                                   </div>
					          <div class="row">
					 
					         <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                    <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Thank you </span><span style="color:#696969">to all of you...</span></h1>
                                         <p class="date"> 
                                                <span style="line-height:20.8px"><strong>...</strong>that took the time to review and rate the retirement village and aged care facility that you know. We have the facilities that New Zealanders speak highly of.<br>
<br>
<strong>Congratulations</strong> to the following Winners &amp; Finalists of the 'Peoples Choice' Awards of 2015&nbsp;for Best Retirement or Lifestyle Villages &amp;&nbsp;Aged Care Facilities.</span><br style="line-height: 20.8px;">
                                            </p>
							                   <br>
                                
<div style="text-align: center;"><br>
<span style="color:#FF0000"><strong style="line-height:20.8px">WINNERS - NORTH ISLAND</strong></span><br>
<strong>Lynton Lodge Hospital (Auckland)</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong>Gracelands Rest Home and Village (Hastings)</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong>Abingdon Retirement Village (Wanganui)</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
<br style="line-height: 20.8px;">
<span style="color:#FF0000"><strong style="line-height:20.8px">WINNERS - SOUTH ISLAND</strong></span><br>
<strong>Cheviot Rest Home (Cheviot)</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong>Archer Home (Christchurch)</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br style="line-height: 20.8px;">
<strong>Diana Isaac Retirement Village (Christchurch)</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
[Not Awarded]<br style="line-height: 20.8px;">
<em>Best Multi-Facility Provider (Nationwide)</em><br>
<br>
----------------------<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - NORTH ISLAND</strong></span><br>
<strong>Mitchell Court (Tauranga)</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong>Summerset in the River City (Wanganui)<br>
Selwyn Heights (Auckland)<br>
Maureen Plowman Rest Home (Auckland)<br>
Pakuranga Metlifecare (Auckland)</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong>Maygrove Village (Auckland)<br>
Telford Retirement Village (New Plymouth)<br>
Kowhainui Retirement Village (Wanganui)<br>
Gracelands Retirement Village (Hastings)<br>
Cantabria Retirement Village (Rotorua)</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - SOUTH ISLAND</strong></span><br>
<strong>Homestead Ilam Home &amp; Hospital (Christchurch)</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong>Camellia Court Rest Home (Christchurch)<br>
Diana Issac (Christchurch)<br>
Annaliese Haven Rest Home (Kaiapoi)</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong>Merivale Retirement Village (Christchurch)<br>
Ngaio Marsh Retirement Village (Christchurch)<br>
Rosebank Retirement Village (Ashburton)<br>
Alpine View Retirement Village (Christchurch)</strong><br>
<em>Best Retirement / Lifestyle Village</em>&nbsp;<br>
&nbsp;</div>

<div style="text-align: center;"><span style="line-height:20.8px">Special thanks goes to our major Awards Sponsor&nbsp;for 2015.</span></div>
 

 
                                    </div>
                                </div>
                                </div>		
				 		
			   
                        <div class="single-blog-item v3">
						
							 
					 <div class="row">
					 <div class="col-md-12">
                        <a href="https://hearinglife.co.nz/">
						 <div class="text-center">
                            <img src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/dbe8d60a-3e3f-4889-869b-275a54610dd2.jpg" alt="...">
                        </div></a>
                    </div>
				 
                    </div>
				 	 <div class="row">
					<div class="col-md-12">
					<a href="https://www.homeofpoi.com/us/">
                       <div class="text-center">
                            <img src="<?php echo HOME_PATH; ?>images/homeofpoi-logo.png" alt="...">  
                        </div></a>
                    </div>
                    </div>
					 <div class="row">
					 
					         <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Special thanks to our Sponsors.</h3>
                                         <p class="date"> 
  
Note: Aged Advisor winners are based on independent reviews and opinions from people that live, visit or work at the retirement/lifestyle villages and residential care facilities throughout New Zealand.

The team at Aged Advisor are thrilled to celebrate the exceptional organisations who offer independent living options or care for our older generation, and are proud to award them with the recognition they deserve.

Entry is now open for the 2016 Awards and we look forward to announcing the winners later in the year. So make sure to get those reviews in.
                            </p>
							<br>
                            
                                    </div>
                                </div>
 
                    
                </div>		
							
							
							
							
							
                        </div>
				
						
                    </div>
					<div class="row">
                             
                                <div class="col-md-8 offset-md-1">
                                        <h2>South Island Award Presentations...</h2> 
                                        <p>With our Aged Advisor head office based in Christchurch, we quietly presented the South Island Awards to the three Canterbury based facilities late last week (one even had their welcoming committee ready to greet us!). Although they were all told that this was "secret squirrel" until this week - we have a hunch one or two couldn't help it, and proudly shared their great news! </p> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>https://hearinglife.co.nz/" target="_blank">
                             <img style="width: 100% !important;" src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/29671041-21be-4b8e-8d7e-367f24ffb874.jpg" alt="..."></a>
                                   <a   class="blog-cat btn v6 red">Nationwide Award...</a>  
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                     
                                        <h3>Cheviot Resthome: Manager / Owner, Sue Coleman with some of her team and their Award for Best Small Aged Care Facility - South Island.</h3>
                                        <h3><b>(left to right) Elysia Hepi, Janice Hall, Sandy Nesbitt, Sue Coleman<b></h3>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						<br>
						  <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>https://hearinglife.co.nz/" target="_blank">
                             <img style="width: 100% !important;" src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/4ddb82ca-e039-4945-a43c-e6147f7b481e.jpeg" alt="..."></a>
                                   <a   class="blog-cat btn v6 red">South Island Award Presentations</a>  
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                     
                                        <h3>Archer Home. Best Med/Large Aged Care Facility Award - South Island</h3>
                                        <p>(left to right) John Robertson (Managing Director, Hearing Technology - Sponsor), Wayne Hodge (longest residing resident), Graeme Mitchell (General Manager, Archer), Nigel Matthews (General Manager, AgedAdvisor)</p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						<br>
						
						
						  <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>https://hearinglife.co.nz/" target="_blank">
                             <img style="width: 100% !important;" src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/9be0e5ea-68a9-40d7-8a58-c1712531a823.jpeg" alt="..."></a>
                                   <a   class="blog-cat btn v6 red">South Island Award Presentations</a>  
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                     
                                        <h3>Some of the staff and residents of Archer Home</h3>
                                        
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						<br>
						<div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>https://hearinglife.co.nz/" target="_blank">
                             <img style="width: 100% !important;" src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/6a9d570a-0b37-406a-be02-0cbeb37ce09c.jpg" alt="..."></a>
                                   <a   class="blog-cat btn v6 red">South Island Award Presentations</a>  
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                     
                                        <h3>Diana Isaac Retirement Village, Christchurch.
                                       Presentation of Best Retirement or Lifestyle Village Award - South Island.</h3>
                                        <p>
										(left to right) Nigel Matthews (General Manager, AgedAdvisor), George Smith (Village Manager, Diana Isaac Village), John Robertson (Managing Director, Hearing Technology - Sponsor)</p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						<br>
						
						
						<div class="row">
                             
                                <div class="col-md-8 offset-md-1">
                                        <h2>North Island Award Presentations...</h2> 
                                        <p>We were thrilled to be able to personally make presentations to the North Island winners (and several finalists) between 29th Feb and 2nd March. Congratulations to all of you. </p> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>https://hearinglife.co.nz/" target="_blank">
                             <img style="width: 100% !important;" src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/a59ae09d-bc66-45dd-83d3-2bb685234db6.jpg" alt="..."></a>
                                   <a   class="blog-cat btn v6 red">North Island Award Presentations</a>  
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                     
                                        <h3>Lynton Lodge Hospital, Auckland.

                                    Presentation of Best Small Aged Care Facility Award - North Island.</h3>
                                        <p>Deborah Hogan (from Hearing Technology) presents certificate and award to some of the team and management with Village Manager, Elaine (3rd from right) </p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						<br>
						
						
					  <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>https://hearinglife.co.nz/" target="_blank">
                             <img style="width: 100% !important;" src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/ad6a8f8a-3ce7-4def-a4c2-b245f115ee1d.jpg" alt="..."></a>
                                   <a   class="blog-cat btn v6 red">North Island Award Presentations</a>  
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                     
                                        <h3>Gracelands Resthome and Hospital, Hastings.

                                      Presentation of Best Med / Large Aged Care Facility Award - North Island.</h3>
                                        <p>
                                    Some of the team and management with Village Manager, Phil Harman (far right) </p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						<br>	
						
							
						
					  <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>https://hearinglife.co.nz/" target="_blank">
                             <img style="width: 100% !important;" src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/d417b5be-2805-4185-bed4-c69bfc6c80ee.jpg" alt="..."></a>
                                   <a   class="blog-cat btn v6 red">North Island Award Presentations</a>  
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                     
                                        <h3>Abingdon Retirement Village, Wanganui.

                                       Presentation of Best Retirement or Lifestyle Village Award - North Island.</h3>
                                        <p>
                                  Some of the Abingdon residents with Village Manager, Des McGrath (far right)</p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						<br>
             
      
    </div>
    </div>
    
 
