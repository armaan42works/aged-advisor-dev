 
    <div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH;?>/modules/pages/images/bg/memphis-colorful.png);height:250px;">
            <div class="overlay op-5"></div>
            <div class="container">
                <div class="row align-items-center  pad-top-80">
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu">
						<h2 class="page-title" style="font-size: 30px; color:#141414;">Awards 2018</h2>
                        
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu text-right sm-left">
                            <ul>
                                <li class="active"><a href="<?php echo HOME_PATH;?>">Home</a></li>
                                <li><a href="<?php echo HOME_PATH;?>awards-best-retirement-villages-aged-care-2018">Awards</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Breadcrumb section ends-->
        <!--Blog post starts-->
        <div class="blog-area section-padding pad-bot-40 mar-top-20">
            <div class="container">
                <div class="row">
				
				
				
                    <div class="col-md-12">
                        <div class="single-blog-item v3">
						 <div class="single-blog-item v3">
						 	 	 <div class="row">
					         <div class="col-md-12">
					     
                            <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Finalists &amp; Winners of 2018</span><br>
                               <span style="color:#696969">'Peoples Choice Awards'</span><br>
                                 <span style="color:#FF0000">for Best Retirement Villages and Best Aged Care.</span></h1>
                              </div>
                                   </div>
								    <div class="row">
					         <div class="col-md-12">
					       
                               <div class="text-center">
                             <img src="<?php echo HOME_PATH; ?>images/2017awards.jpg" alt="...">  
                                 </div> 
                              </div>
                                   </div>
					          <div class="row">
					 
					         <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                       <h1 style="text-align: center;margin: 0;padding: 0;display: block;font-family: Helvetica;font-size: 40px;font-style: normal;font-weight: bold;line-height: 125%;letter-spacing: -1px;color: #606060 !important;"><span style="color:#FF0000">Thank you </span><span style="color:#696969">again...</span></h1>
                                         <p class="date"> 
                              
                                           ...for sharing your experiences. We have received nearly 2000 reviews and ratings for this years awards.

                                             Results of the Winners & Finalists for the 2017 "Peoples' Choice Awards" for Best Retirement or Lifestyle Villages & Aged Care Facilities:
                                            </p>
							                   <br>
                                   
                                <span style="color:#FF0000"> WINNER - BEST PROVIDER NATIONWIDE </span><br>

                                <a href="<?php echo HOME_PATH;?>search/for/75000/Retirement-Villages-Rest-Homes-Aged-Care-RYMAN+VILLAGES" target="_blank">            RYMAN HEALTHCARE</a>, New Zealand <br>
                               Best Multi-Facility / Group Provider (Nationwide)<br>
 
                                     ----------------------<br>
                                    <br>




                                       <span style="color:#FF0000"> WINNERS - NORTH ISLAND </span><br>
                                       <strong><a href="<?php echo HOME_PATH;?>search/aged-care/St-Josephs-Home-Of-Compassion-Heretaunga-Upper-Hutt" target="_blank">St Joseph's Home and Hospital</a>, Auckland</strong><br>
                                   <em>Best Aged Care upto 40 beds</em><br>
                                         <br>
                                   <strong><a href="<?php echo HOME_PATH;?>search/aged-care/Malyon-House-Mount-Manganui-Tauranga" target="_blank">Malyon House</a>, Tauranga</strong><br>
                                        <em>Best Aged Care over 40 beds</em><br>
                                        <br>
                                       <strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Madison-Retirement-Village--Levin" target="_blank">Madison Retirement Village</a>, Levin</strong><br>
                                         <em>Best Retirement / Lifestyle Village</em><br>
                                            <br>
                                         <br style="line-height: 20.8px;">
                                                <span style="color:#FF0000"><strong style="line-height:20.8px">WINNERS - SOUTH ISLAND</strong></span><br>
                                            <strong><a href="<?php echo HOME_PATH;?>search/aged-care/Cheviot-Rest-Home-Cheviot-Cheviot" target="_blank">Cheviot Rest Home</a>, Canterbury</strong><br>
                                           <em>Best Aged Care upto 40 beds</em><br>
                                             <br>
                                             <strong><a href="<?php echo HOME_PATH;?>search/aged-care/Archer-Archer-Home-Beckenham-Christchurch" target="_blank">Archer Home</a>, Christchurch</strong><br>
                                             <em>Best Aged Care over 40 beds</em><br>
                                                 <br style="line-height: 20.8px;">

                                              <strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Diana-Isaac-Retirement-Village-Mairehau-Christchurch" target="_blank">Diana Isaac Retirement Village</a>, Christchurch</strong><br>
                                                 <em>Best Retirement / Lifestyle Village</em><br>
                                                   <br>

                                                   ----------------------<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - NORTH ISLAND</strong></span><br>
<strong><a href="<?php echo HOME_PATH;?>search/aged-care/Greendale-Residential-Care-Centre-Greenmeadows-Napier" target="_blank">Greendale Residential Care</a>, Napier</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/aged-care/Wimbledon-Villa--Feilding" target="_blank">Wimbledon Villa Rest Home</a>, Fielding</strong><br>
<strong><a href="/search/aged-care/Lynton-Lodge-Hospital-Westmere-Auckland" target="_blank">Lynton Lodge Hospital</a>, Auckland</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/aged-care/Pohlen-Hospital--Matamata" target="_blank">Pohlen Hospital</a>, Matamata</strong><br>

<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="<?php echo HOME_PATH;?>search/aged-care/Heretaunga-Rest-Home-And-Village-Silverstream-Upper-Hutt" target="_blank">Heretaunga Rest Home</a>, Upper Hutt</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/aged-care/Te-Mana-Rest-Home-Birkdale-Auckland" target="_blank">Te Mana Rest Home</a>, Auckland</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/aged-care/Rangiura-Rest-Home-Retirement-Village--Putaruru" target="_blank">Rangiura Rest Home</a>, Putaruru</strong><br>

<em>Best Aged Care over 40 beds</em><br>
<br>
<strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Kiri-Te-Kanawa-Retirement-Village-Taruheru-Gisborne" target="_blank">KIRI TE KANAWA RETIREMENT VILLAGE</a>, Gisborne</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Maygrove-Village-Orewa-Auckland" target="_blank">MAYGROVE VILLAGE</a>, Orewa</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Mt-Eden-Gardens-Mt-Eden-Auckland" target="_blank">MT EDEN GARDENS</a>, Auckland</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Bob-Owens-Retirement-Village-Bethlehem-Tauranga" target="_blank">BOB OWENS RETIREMENT VILLAGE</a>, Tauranga</strong><br>
<em>Best Retirement / Lifestyle Village</em><br>
<br>
<br>
<span style="color:#FF0000"><strong>FINALISTS - SOUTH ISLAND</strong></span><br>
<strong><a href="<?php echo HOME_PATH;?>search/aged-care/Waikiwi-Gardens-Rest-Home-Waikiwi-Invercargill" target="_blank">Waikiwi Gardens Rest Home</a>, Invercargill</strong><br>
<em>Best Aged Care upto 40 beds</em><br>
<br>
<strong><a href="<?php echo HOME_PATH;?>search/aged-care/Kauri-Lodge-Rest-Home-Riccarton-Christchurch" target="_blank">Kauri Lodge</a>, Christchurch</strong><br>
<em>Best Aged Care over 40 beds</em><br>
<br>
<strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Charles-Upham-Retirement-Village-Waimakariri-Rangiora" target="_blank">CHARLES UPHAM RETIREMENT VILLAGE</a>, Rangiora</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Summerset-At-Wigram-Wigram-Christchurch" target="_blank">SUMMERSET at WIGRAM RETIREMENT VILLAGE</a>, Christchurch</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Archer-Archer-Lifestyle-Village-Beckenham-Christchurch" target="_blank">ARCHER LIFESTYLE VILLAGE</a>, Christchurch</strong><br>
<strong><a href="<?php echo HOME_PATH;?>search/retirement-village/Lady-Wigram-Retirement-Village-Wigram-Christchurch" target="_blank">LADY WIGRAM RETIREMENT VILLAGE</a>, Christchurch</strong><br>

<em>Best Retirement / Lifestyle Village</em>&nbsp;<br>
 
                                    </div>
                                </div>
                                </div>		
				 		
			  
						 
						 
						 
						 
						 
							  <div class="row">
                             
                                <div class="col-md-8 offset-md-1">
                                        <h2>Nationwide Award...</h2> 
                                        <p>Congratulations to Ryman Healthcare for being the inaugural winner in this category</p> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>search/for/75000/Retirement-Villages-Rest-Homes-Aged-Care-RYMAN+VILLAGES" target="_blank">
                             <img style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/ryman-healthcare-award-winner-2018-agedadvisor.jpg" alt="..."></a>
                                   <a   class="blog-cat btn v6 red">Nationwide Award...</a>  
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                     
                                        <h3>Katrin Hobson (Hearing Technology) presents Group Provider Award (Nationwide) to Nicki Brown & Adrienne Sincock, on behalf of Ryman Healthcare.</h3>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						<br>
                        <div class="single-blog-item v3">
						
							 <div class="single-blog-item v3">
							  <div class="row">
                             
                                <div class="col-md-8 offset-md-1">
                                        <h2>  South Island Awards...</h2> 
                                        <p>Christchurch currently has the most multiple Award-Winning Facilities.</p> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>search/aged-care/Cheviot-Rest-Home-Cheviot-Cheviot" target="_blank">
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/Cheviot-Rest-Home-Cheviot-Cheviot.jpg" alt="..."></a>
                                    <a   class="blog-cat btn v6 red">
                                       South Island Awards...</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                        <p class="date">Christchurch currently has the most multiple Award-Winning Facilities.</p>
                                        <h3>Cheviot Resthome: Manager / Owner, Sue Coleman with some of the team and their Awards for Best Small Aged Care Facility - South Island (3 Years in a row!).</h3>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    <br>
                        <div class="single-blog-item v3">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								  
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/St-Josephs-Home-Of-Compassion-Heretaunga-Upper-Hutt.jpg" alt="..."> 
                                    <a  class="blog-cat btn v6 red">
                                       South Island Awards...</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
									   <h3>Archer Home. Best Med/Large Aged Care Facility Award - South Island</h3>
                                        <p class="date">Graeme Mitchell, Village Manager (left) with residents delighted to receive their 3rd award from Nigel Matthews of Aged Advisor NZ</p>
                                     
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						                    <br>
						 <div class="single-blog-item v3">
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>search/aged-care/Cheviot-Rest-Home-Cheviot-Cheviot" target="_blank">
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/diana-issac-retirement-village.jpg" alt="..."></a>
                                    <a   class="blog-cat btn v6 red">
                                       South Island Awards...</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Diana Isaac Retirement Village, Christchurch.
Presentation of Best Retirement or Lifestyle Village Award - South Island.</h3>
     <p class="date">Nicki Brown, Village Manager (centre left) and some her team receiving the award from Nigel Matthews, AgedAdvisor (left).</p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
                        </div>
						
						                    <br>
						
							  <div class="row">
                             
                                <div class="col-md-8 offset-md-1">
                                        <h2>
                                      North Island Awards...</h2> 
                                        <p>Presentations to the North Island winners. Congratulations to all of you.</p> 
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-10 offset-md-1">
								<a href="<?php echo HOME_PATH;?>search/aged-care/St-Josephs-Home-Of-Compassion-Heretaunga-Upper-Hutt" target="_blank">
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/St-Josephs-Home-Of-Compassion-Heretaunga-Upper-Hutt.jpg" alt="..."></a>
                                    <a   class="blog-cat btn v6 red">
                                     
                                      North Island Awards...</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>St Josephs Home of Compassion, Auckland.

                                    Presentation of Best Small Aged Care Facility Award - North Island.</h3>
                                         <p class="date">Residents, staff and Sisters of Little Sisters of the Poor</p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
							
							
							
							
							                    <br>
							
							
							    <div class="row">
                                <div class="col-md-10 offset-md-1">
							 
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/maylon-house-tauranga.jpg" alt="..."> 
                                    <a   class="blog-cat btn v6 red">
                                     
                                      North Island Awards...</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Malyon House, Tauranga.

Presentation of Best Med / Large Aged Care Facility Award - North Island.</h3>
                                         <p class="date">Amy Munro (Owner) with some of the team and their latest award.</p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
							
							
							
							                    <br>
							
							
							
							
							
							  <div class="row">
                                <div class="col-md-10 offset-md-1">
							 
                                    <img  style="width: 100% !important;" src="<?php echo HOME_PATH;?>images/Madison-Retirement-Village--Levin.jpg" alt="..."> 
                                    <a   class="blog-cat btn v6 red">
                                     
                                      North Island Awards...</a>
                                </div>
                                <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Madison Retirement Village, Auckland.

                                           Presentation of Best Retirement or Lifestyle Village Award - North Island.</h3>
                                         <p class="date">Sonya, Village Manager (center) with some of the team..</p>
                                        <!--<a href="single-news-three.html" class="btn v4">Know More</a>-->
                                    </div>
                                </div>
                            </div>
							
							
							
							  <br>
							
						 
							
							
							
					 <div class="row">
					 <div class="col-md-12">
                        <a href="https://hearinglife.co.nz/">
						 <div class="text-center">
                            <img src="https://gallery.mailchimp.com/794c7fae79b8e528f8be29233/images/dbe8d60a-3e3f-4889-869b-275a54610dd2.jpg" alt="...">
                        </div></a>
                    </div>
				 
                    </div>
				 	 <div class="row">
					<div class="col-md-12">
					<a href="https://www.homeofpoi.com/us/">
                       <div class="text-center">
                            <img src="<?php echo HOME_PATH; ?>images/homeofpoi-logo.png" alt="...">  
                        </div></a>
                    </div>
                    </div>
					 <div class="row">
					 
					         <div class="col-md-8 offset-md-2">
                                    <div class="blog-content text-center pad-top-30">
                                   
                                        <h3>Special thanks to our Sponsors.</h3>
                                         <p class="date"> 
                              
Note: Aged Advisor winners are based on independent reviews and opinions from people that live, visit or work at the retirement/lifestyle villages and residential care facilities throughout New Zealand.

The team at Aged Advisor are thrilled to celebrate the exceptional organisations who offer independent living options or care for our older generation, and are proud to award them with the recognition they deserve.

Entry is now open for next years Awards. Make sure to <a  style="color: #6DC6DD;"href="<?php echo HOME_PATH; ?>rating-supplier">write your review.</a>
                            </p>
							<br>
                                        <a href="<?php echo HOME_PATH; ?>awards-best-retirement-villages-aged-care-2017" class="btn v4">Click here to view previous years awards</a> 
                                    </div>
                                </div>
 
                    
                </div>		
							
							
							
							
							
                        </div>
				
						
                    </div>
                </div>
             
            </div>
      
    </div>
    </div>
    
 
