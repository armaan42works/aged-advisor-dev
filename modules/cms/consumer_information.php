<?php
error_reporting(0);
session_start();
global $db;

   $prefix = 'ad_';
		
$con=mysqli_connect("localhost","agedadvisor","f3Cdc7~3") or die("error in connection");
mysqli_select_db("new_agedadvisor_live",$con);

function emailTemplate1($alias) {
    $sql_query = "SELECT subject, description FROM  ad_email_templates where alias='$alias'";
    $res = mysqli_query($db->db_connect_id, $sql_query);
    while ($record = mysqli_fetch_array($res)) {
        $subject = $record['subject'];
        $description = $record['description'];
    }
    $email_fields = array();
    $email_fields['subject'] = $subject;
    $email_fields['description'] = $description;
    return $email_fields;
}

$csuser = $_SESSION['csId'];
$supplier_userid = $_SESSION['id'];

if (isset($csuser)) {
    $session_user_id = $csuser;
}
if (isset($supplier_userid)) {
    $session_user_id = $supplier_userid;
}
/* * **********end of code of get user id *** */

/* * ***************************TO fetch user type from ******************* */
if (isset($csuser)) {
    $getUserType = "select user_type,email,user_name from ad_users WHERE  id='$csuser'";
    $getUserType = $db->sql_query($getUserType);
    $userType = $db->SQL_FetchRow($getUserType);
    $u_fname = $userType['user_name'];
    $_SESSION['u_name'] = $u_fname;
    $user_type = $userType['user_type'];
    $u_email = $userType['email'];
}

if (isset($supplier_userid)) {
    $getUserType = "select user_type ,email,user_type  from ad_users WHERE  id='$supplier_userid'";
    $getUserType = $db->sql_query($getUserType);
    $userType = $db->SQL_FetchRow($getUserType);
    $user_type = $userType['user_type'];
    $u_fname = $userType['user_name'];
    $u_email = $userType['email'];
}

?>

<div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH;?>/modules/pages/images/bg/memphis-colorful.png);height:250px;">
            <div class="overlay op-5 green"></div>
            <div class="container">
                <div class="row align-items-center  pad-top-80">
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu">
						<h2 class="page-title" style="font-size: 30px; color:#141414;">Consumer Information</h2>
                          
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu text-right sm-left">
                            <ul>
                                <li class="active"><a href="<?php echo HOME_PATH;?>">Home</a></li>
                                <li><a href="<?php echo HOME_PATH;?>consumer_information">Consumer Information</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		
		
<?php		
/* * ******************end of code for fetch data ************* */
$category_info = "select * from  ad_category_content_list where status='1' AND deleted='0'";
$category_information = mysqli_query($db->db_connect_id, $category_info);
echo '<div class="user-login-section section-padding bg-fixed">
	<div class="container">
   <div class="row login-wrapper">
  
   <div class="col-sm-12">
   	<div class="association-section11">';
while ($category_info_detail = mysqli_fetch_array($category_information)) {
    $category_title = $category_info_detail['title'];
    $category_topic = $category_info_detail['category'];
    $category_content = $category_info_detail['category_content'];
    $category_image = $category_info_detail['category_img_name'];
    $category_link = $category_info_detail['category_link'];
    $category_article_pdf = $category_info_detail['category_article_pdf'];
    $category_article_type = $category_info_detail['category_article_type'];

    if (!empty($category_link)) {
        $cat_link = $category_link;
        $read_more = '<p><a target="_blank" href="' . $cat_link . '"><strong>Read More</strong></a></p>';
    } else {
        $cat_link = '';
        $read_more = '';
    }
    /*     * ********for category article type ****** */
    if (!empty($category_article_type)) {
        $category_article_type = $category_article_type;
    } else {
        $category_article_type = 'Article';
    }

    if (!empty($category_article_pdf)) {
        $article_url = '<p><button data-id="' . $category_article_pdf . '"  data-toggle="modal" data-target="#myModal" class="btn btn-danger subs_link" value="' . $category_article_type . '">Click here to download your ' . $category_article_type . '</button></p>';
    } else {
        $article_url = '';
    }
    if (!empty($category_image)) {

        $cimage = '<div class="col-xs-3"><div class="association-img"><img src="admin/files/pages/banner/' . $category_image . '" alt="logo Image"></div></div>';
    } else {
        $cimage = '';
    }
    /*     * ************end of code for article type********** */
    echo '<div style="height:50px" id="' . $category_info_detail['id'] . '"></div>';
    echo '<div class="associate-content-box">';
    echo $cimage;
    echo '<div class="col-xs-9">
      <div class="association-text">
      <h2>' . $category_title . '</h2>
      <p>' . strip_tags($category_content, '<div>') . '</p>
       <p><strong>Category</strong>: &nbsp;' . $category_topic . '</p>';
    echo $read_more;

    echo $article_url;
    echo '</div>
     </div>';
    echo '<div class="clearfix"></div></div>';
}
echo '<div class="clearfix"></div></div></div></div></div></div>';
?>

<!--Pop-up code -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Please enter a name and email address so we can send you a link to download the <span id="art_type"></span></h4>
            </div>
            <div class="modal-body">
                <!--<div id="messages" class="hide" role="alert"></div>-->
                <span id="comm_message" style="color:red; text-center;"></span> 
                <form  name="subscriber" id="subscriber" method="post" action="">
                    <input type="text" name="name" id="name" placeholder="Enter Your Name" required="true">
                    <input type="email" name="email" id="email" placeholder= "Enter Your E-mail" required="true">
                    <input type="hidden" name="article_link" id="article_link">

                    <input type="button" id="send" name="submit" class="btn btn-danger" value="">
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(document).on("click", ".subs_link", function () {
            var article_link = $(this).data('id');
            var article_type = $(this).attr('value');
            $("#art_type").html(article_type);
            $("#send").attr('value', 'Yes, please email me the ' + article_type);
            var email_id = "<?php echo $u_email; ?>";
            $('#messages').removeClass('hide').addClass('alert alert-success alert-dismissible').slideDown().show();
            $(".modal-body #article_link").val(article_link);
        });
    </script>

    <script type="text/javascript">
        $(document).ready(function (e) {
            var path_article = "/modules/cms/ajax-message.php";

            $("#send").click(function (e) {
                var subs_name = $("#name").val();
                var subs_email = $("#email").val();
                var article_link = $("#article_link").val();
                var datastring = "subs_name=" + subs_name + "&subs_email=" + subs_email + "&article_link=" + article_link;
                $.ajax({
                    type: "POST",
                    url: path_article,
                    data: datastring,
                    success: function (result) {
                        //alert(result);
                        if (result == "Mail sent") {
                            $("#comm_message").html('<h4 class="alert alert-success text-center">A download link has been sent to ' + subs_email + '</h4>');
                            $("#comm_message").show();
                            $('#subscriber')[0].reset();

                            setTimeout(function () {
                                $("#myModal").modal('hide');
                                $("#comm_message").hide();
                            }, 3000);
                        } else {
                            $("#comm_message").html('<h4 class="alert alert-danger text-center">' + result + '</h4>');
                            $("#comm_message").show();
                            setTimeout(function () {
                                $("#comm_message").hide();
                            }, 3000);
                        }

                    }

                });

            });

        });

    </script>