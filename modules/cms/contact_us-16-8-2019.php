<?php
    $sql_query = "SELECT * FROM " . _prefix("about_us") . " where id=2";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {
            $id = $record['id'];
            $title = $record['title'];
            $content = $record['content'];
        }
    }
?>
<div class="call-to-action pad-tb-70" style="background-image: url(<?php echo HOME_PATH;?>/modules/pages/images/bg/memphis-colorful.png);margin-top: -76px;height:250px;">
            <div class="overlay op-5 green"></div>
            <div class="container">
                <div class="row align-items-center  pad-top-80">
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu">
                            <h2 class="page-title a"><?php echo $title; ?></h2>
                        </div>
                    </div>
                    <div class="col-md-6 col-12">
                        <div class="breadcrumb-menu text-right sm-left">
                            <ul>
                                <li class="active"><a href="<?php echo HOME_PATH;?>">Home</a></li>
                                <li><a href="<?php echo HOME_PATH;?>contact-us">Contact Us</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		<br>
		<br>
<!--MAIN BODY CODE STARTS-->
<div class="container">
    <div class="row articles_container">
        
        <div class="col-md-12 col-sm-12 col-xs-12 ">
            <p><?php echo $content; ?></p>
             
        </div>
    </div>
</div>
<!--MAIN BODY CODE ENDS-->
<style>
h2.page-title.a {
    color: black !important;
}
</style>
