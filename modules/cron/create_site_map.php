<?php

ini_set('error_reporting', E_ALL);
ini_set("display_errors", 1);
ini_set('max_execution_time', 600); //600 seconds = 10 minutes
set_time_limit(600); //ten minutes	

$countsmap = 0;
$date = date("Y-m-d");    //YYYY-MM-DD
$six_month_ago = mktime(0, 0, 0, date("m"), date("d"), date("Y") - 2);
$delete_date = date("Y-m", $six_month_ago);
//echo $_SERVER['DOCUMENT_ROOT']."<br>";


include("../../application_top.php");
//---------- Header Start -------------
include(INCLUDE_FILE . "common_ajaxPaging.php");
global $db;

function FileSizeConvert($bytes) {
    $bytes = floatval($bytes);
    $arBytes = array(
        0 => array(
            "UNIT" => "Tb",
            "VALUE" => pow(1024, 4)
        ),
        1 => array(
            "UNIT" => "Gb",
            "VALUE" => pow(1024, 3)
        ),
        2 => array(
            "UNIT" => "Mb",
            "VALUE" => pow(1024, 2)
        ),
        3 => array(
            "UNIT" => "Kb",
            "VALUE" => 1024
        ),
        4 => array(
            "UNIT" => "bytes",
            "VALUE" => 1
        ),
    );

    foreach ($arBytes as $arItem) {
        if ($bytes >= $arItem["VALUE"]) {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", ".", strval(round($result, 2))) . " " . $arItem["UNIT"];
            break;
        }
    }
    return $result;
}

function changelangurl($easy_change_lang_url) {
    $easy_change_lang_url = str_replace('/us/', '/##lang##/', $easy_change_lang_url);
    $easy_change_lang_url = str_replace('/en/', '/##lang##/', $easy_change_lang_url);
    $easy_change_lang_url = str_replace('/jp/', '/##lang##/', $easy_change_lang_url);
    $easy_change_lang_url = str_replace('/ru/', '/##lang##/', $easy_change_lang_url);
    $easy_change_lang_url = str_replace('/de/', '/##lang##/', $easy_change_lang_url);
    $easy_change_lang_url = str_replace('/fr/', '/##lang##/', $easy_change_lang_url);
    $easy_change_lang_url = str_replace('/zh/', '/##lang##/', $easy_change_lang_url);
    return $easy_change_lang_url;
}

function update_insert_delete($loc, $lastmod, $changefreq, $priority, $all_languages, $type, $relative_url) {


    $sql = "delete from sitemap where loc = '" . mysqli_escape_string($db->db_connect_id, $loc) . "' ";
    mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

    // now insert into site map table
    $sql = "insert into sitemap(loc,lastmod,changefreq,priority,all_languages,type) ";
    $sql .= " values";
    $sql .= "('" . mysqli_escape_string($db->db_connect_id, $loc) . "','$lastmod','$changefreq','$priority','$all_languages','$type')";
    mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");
// Now delete from access urls table

    $sql = "delete from access_urls where url = '" . mysqli_escape_string($db->db_connect_id, $relative_url) . "' ";
    mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");
}

// end function
// DELETE old sitemap urls

$sql = "delete from sitemap where lastmod LIKE '" . $delete_date . "%' "; // Deletes if over 2 years  (was  6 months) old
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");


// First DELETE all product links from table


$sql = "delete from sitemap where type = 'product' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

// Now get all products and make priority value between 1 and 0.75

$sql = "select quick_url  from ad_products  WHERE deleted=0  ";
$sql .= " ";
$result = mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");
if ($result) {
    $products_number = mysqli_num_rows($result);
    $count = 1; // for scoring priority
    while ($myrow = mysqli_fetch_row($result)) {
        $loc = 'https://www.agedadvisor.nz/' . $myrow[0];
        $priority = 0.9;
        $count = $count + 0.25;

        //echo "$priority $loc<br>";
        // now insert into site map table
        $sql = "insert into sitemap(loc,lastmod,changefreq,priority,all_languages,type) ";
        $sql .= " values";
        $sql .= "('" . mysqli_escape_string($db->db_connect_id, $loc) . "','$date','daily','$priority','no','product')";
        mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");
    }
}// end if result
// Now go through LOGS and get unique urls recorded


$sql = "delete from access_urls where url LIKE '%comhttp%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from access_urls where url LIKE '%&%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from access_urls where url LIKE '%,%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from access_urls where url LIKE '%-\&-%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");


$sql = "delete from sitemap where loc LIKE '%comhttp%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from sitemap where loc LIKE '%,%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from sitemap where loc LIKE '%-\&-%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from sitemap where loc LIKE '%shortsearch%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from sitemap where loc LIKE '%search2%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from sitemap where loc LIKE '%Compare-Retirement-Villages-Entry-Age-Fees-Conditions2%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from sitemap where loc LIKE '%" . mysqli_real_escape_string('/thank-you?id=') . "%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from sitemap where loc LIKE '%" . mysqli_real_escape_string('/index.php?') . "%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from sitemap where loc LIKE '%" . mysqli_real_escape_string('/>') . "%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

$sql = "delete from sitemap where loc LIKE '%" . mysqli_real_escape_string('&') . "%' ";
mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");

//NEVER DO THIS AGAIN IT DELETES ALL!!!!	$sql = "delete from sitemap where loc LIKE '%".mysqli_real_escape_string('%')."%' "; 
//	mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");



$sql = "select distinct (url) from access_urls ";
$result = mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");
if ($result) {
    $urls_number = mysqli_num_rows($result);

    //		echo "<hr>NUMBER NEW UNIQUE URLS = $urls_number<hr>";


    $count = 1; // for scoring priority
    while ($myrow = mysqli_fetch_row($result)) {
        $relative_url = $myrow[0];
        $loc = 'https://www.agedadvisor.nz' . $myrow[0];
        // DEFAULTS
        $priority = 0.5;
        $changefreq = 'monthly';
        $all_languages = 'no';
        $type = 'other';

        if (stristr($loc, '/articles/')) {
            $priority = 0.6;
            $changefreq = 'weekly';
            $all_languages = 'no';
            $type = 'article';
        }
        if (stristr($loc, '/search/for/')) {
            $priority = 0.7;
            $changefreq = 'monthly';
            $all_languages = 'no';
            $type = 'search';
        }
        if (stristr($loc, '/forum/')) {
            $priority = 0.6;
            $changefreq = 'monthly';
            $all_languages = 'no';
            $type = 'forum';
        }
        if (stristr($loc, '/blog/')) {
            $priority = 0.6;
            $changefreq = 'monthly';
            $all_languages = 'no';
            $type = 'blog';
        }
        if (stristr($loc, '/events/')) {
            $priority = 0.6;
            $changefreq = 'monthly';
            $all_languages = 'no';
            $type = 'events';
        }
        if (stristr($loc, 'information')) {
            $priority = 0.6;
            $changefreq = 'monthly';
            $all_languages = 'no';
            $type = 'events';
        }
        if ($loc == 'https://www.agedadvisor.nz/') {
            $priority = 0.7;
            $changefreq = 'daily';
            $all_languages = 'no';
            $type = 'main';
        }
        $lastmod = $date;
        update_insert_delete($loc, $lastmod, $changefreq, $priority, $all_languages, $type, $relative_url);
        //	echo "$priority $loc<br>";
    } // while       
}// end if result

$strMessage = '';
$count = 60000;
$output2 = '';

$output2 .= '<?xml version="1.0" encoding="UTF-8"?>
<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';


for ($x = 0; $count > 49999; $x++) {


    $output = '';

    $output .= '<?xml version="1.0" encoding="UTF-8"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">' . "\n";



    $count = 0;
// MAX LIMIT is 50,000 for text xml file
    $sql = "select * from sitemap ORDER BY priority DESC LIMIT " . ($x * 50000) . ",50000 ";
    $result = mysqli_query($db->db_connect_id, $sql) or die("$sql<br>Error number:" . mysqli_errno() . "");
    if ($result) {
        while ($myrow = mysqli_fetch_row($result)) {
            $loc = $myrow[1];
            $lastmod = $myrow[2];
            $changefreq = $myrow[3];
            if (!$changefreq) {
                $changefreq = 'monthly';
            }
            $priority = $myrow[4];
            $all_languages = $myrow[5];
            $type = $myrow[6];

            $count += 1;




            $output .= '<url>' . "\n" . '<loc>' . $loc . '</loc>' . "\n";
            if ($all_languages != 'no') {
                $easy_change_lang_url = changelangurl($loc);
                $output .= '<xhtml:link rel="alternate" hreflang="en-us" href="' . str_replace('##lang##', 'us', $easy_change_lang_url) . '"  />' . "\n";
                $output .= '<xhtml:link rel="alternate" hreflang="en" href="' . str_replace('##lang##', 'en', $easy_change_lang_url) . '"  />' . "\n";
                $output .= '<xhtml:link rel="alternate" hreflang="ja" href="' . str_replace('##lang##', 'jp', $easy_change_lang_url) . '"  />' . "\n";
                $output .= '<xhtml:link rel="alternate" hreflang="ru" href="' . str_replace('##lang##', 'ru', $easy_change_lang_url) . '"  />' . "\n";
                $output .= '<xhtml:link rel="alternate" hreflang="de" href="' . str_replace('##lang##', 'de', $easy_change_lang_url) . '"  />' . "\n";
                $output .= '<xhtml:link rel="alternate" hreflang="fr" href="' . str_replace('##lang##', 'fr', $easy_change_lang_url) . '"  />' . "\n";
                $output .= '<xhtml:link rel="alternate" hreflang="zh" href="' . str_replace('##lang##', 'zh', $easy_change_lang_url) . '"  />' . "\n";
            }//end if all languages
            $output .= '<lastmod>' . $lastmod . '</lastmod>' . "\n" . '<changefreq>' . $changefreq . '</changefreq>' . "\n" . '<priority>' . $priority . '</priority>' . "\n" . '</url>' . "\n";
        }// end while
    }// end result

    $output .= '</urlset>' . "\n ";

// Now save file
    //  ##-- Where to save excel file on the disc --##
    $tmpfname = $_SERVER['DOCUMENT_ROOT'] . "/sitemaps.xml";
    if ($x > 0) {
        $tmpfname = $_SERVER['DOCUMENT_ROOT'] . "/sitemaps" . $x . ".xml";
    }
    $update = ' but FAILED updating file modification time.';

    $fp = fopen($tmpfname, "w");
    fwrite($fp, $output);
    fclose($fp);
    if (touch($tmpfname)) {
        $update = " and updated file modification time.";
    }

    $data = $tmpfname . ': ' . FileSizeConvert(filesize($tmpfname)) . ' (Max 10Mb)<br>Number of URLs = ' . $count . ' (Our Max 25,000)';

    $sitemapurl = str_ireplace($_SERVER['DOCUMENT_ROOT'] . '/', 'https://www.agedadvisor.nz/', $tmpfname);
    $output2 .= '
	<sitemap>
		<loc>' . $sitemapurl . '</loc>
		<lastmod>' . date("Y-m-d") . '</lastmod>
	</sitemap>';



// Grab file size and number of URLs to send in Email


    $strMessage .= $data . "Saved to disc" . $update . "<br>";
    $countsmap = $countsmap + 1;
}// END FOR NEXT
$strMessage .= '<hr>Created by https://www.agedadvisor.nz/modules/cron/create_site_map.php (Cron :uses /6month_email_referral.php daily)' . "<br>";

$output2 .= '
</sitemapindex>
';

$tmpfname = $_SERVER['DOCUMENT_ROOT'] . "/sitemapindex.xml";
$update = ' but FAILED updating file modification time.';

$fp = fopen($tmpfname, "w");
fwrite($fp, $output2);
fclose($fp);
if (touch($tmpfname)) {
    $update = " and updated file modification time.";
}

$data = $tmpfname . ': ' . FileSizeConvert(filesize($tmpfname)) . ' Number of Sitemaps = ' . $countsmap . ' ';

$strMessage .= '<hr>CREATED INDEX of SITEMAPs ' . $data . "Saved to disc" . $update . "<br>";



echo $strMessage;
$title = "Created $tmpfname " . FileSizeConvert(filesize($tmpfname));


//====================================================
//Setting the email attributes
//====================================================
$strSenderName = "Server";
$strSenderEmail = "reviews@agedadvisor.co.nz";
$strName = "SiteMap";
$to = "reviews@agedadvisor.co.nz";

//====================================================
//Sending the email
//====================================================
$headers = 'From: server@agedadvisor.co.nz' . "\r\n" .
        'X-Mailer: PHP/' . phpversion();

sendmail($to, $title, $strMessage, '', $headers);

