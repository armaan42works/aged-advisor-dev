<?php
    global $db;
    if (isset($submitForm2) && $submitForm2 == 'CREATE') {
        $fields_array = userSupplier();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            $fields = array(
                'first_name' => trim($first_name),
                'last_name' => trim($last_name),
                'email' => trim($email),
                'phone' => trim($phone),
                'password' => md5(trim($password)),
                'company' => trim($company),
                'trade' => trim($trade),
                'user_name' => trim($user_name),
                'zipcode' => trim($zipcode),
                'user_type' => '1',
                'validate' => '1'
            );
            $insert_result = $db->insert(_prefix('users'), $fields);
            $id = $db->last_id();
            if ($insert_result) {
                $to = trim($email);
                $rl_password = trim($password);
                $username = ucwords($first_name . " " . $last_name);
                $url = ' <a href="' . HOME_PATH . 'verification?id=' . md5($id) . '">Click me</a> ';
                $email = emailTemplate('validate_account');
                if ($email['subject'] != '') {
                    $message = str_replace(array('{password}', '{name}', '{useremail}', '{url}'), array($rl_password, $username, $to, $url), $email['description']);

                    $send = sendmail($to, $email['subject'], $message);
                    if ($send) {
                        // Message for insert
                        $msg = common_message_supplier(1, constant('SUPPLIER_REGISTER'));
                        $_SESSION['msg'] = $msg;
                        //ends here
                        redirect_to(HOME_PATH . 'supplier/register');
                    } else {
                        $msg = common_message_supplier(1, constant('SUPPLIER_REGISTER_EMAIL'));
                        $_SESSION['msg'] = $msg . 'But mail has not been sent to your email Id';
                        //ends here
                        redirect_to(HOME_PATH . 'supplier/register');
                    }
                }
            } else {
                $msg = "Form has not been saved";
                $_SESSION['msg'] = $msg;
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>
<div class="row" >
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <?php echo $_SESSION['msg']; ?><br>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>
<div class="container">
    <div class="row row_col">
        <div class=" col-md-12 col-sm-12 col-xs-12 registration_container">
            <div class=" col-md-12 col-sm-12 col-xs-12 ">
            <div class="row">
                <div class=" col-md-8 col-sm-12 col-xs-12 padding_none" >
                    <h1 class="theme_color">Create a supplier account</h1>
                    <p>Enter your details and we will create a supplier account for you</p>
                </div>
                <div class=" col-md-4 col-sm-4 col-xs-12">
                    <h2 style="float:right;color:red;" class="redCol small">* Fields are required</h2>
                </div>
            </div></div>
            <div>
                <form name="sregister" id="sregister" method="POST" action=""  class="form-horizontal" role="form">
                    <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 pull-left">
                        <input type="text" tabindex="1" placeholder="First Name*" name="first_name" maxlength="50"  class="required form-control input_fild alphaNumSpF">
                        <input type="text" tabindex="2" placeholder="Last Name" name="last_name" maxlength="50"  class="form-control input_fild alphaNumSpL">
                        <input type="text" tabindex="3" placeholder="Email*" name="email" id="email" class=" form-control input_fild alphaNumSp">
                        <input type="text" tabindex="4" placeholder="User Name*" name="user_name" id="username" maxlength="30" class="form-control input_fild noSpace">
                        <p style="font-size:12px;">(This will be the name that displays for any comments or posts that you make. It must be 6-30 characters in length.)</p>
                        <input type="password" tabindex="5" placeholder="Password*" name="password" id="password"   class="required form-control input_fild">
                        <input type="password" tabindex="6" placeholder="Confirm Password*"  name="conform_password" class="required form-control input_fild">
<!--                        <div style="text-align:right;" class="col-md-5 col-sm-5 col-xs-12 padding_left padding_right pull-right"><img alt="" src="<?php echo HOME_PATH; ?>images/securitycode_img.jpg" class="img-responsive"></div>-->
                    </div>

                    <div class="col-md-5 col-lg-5 col-sm-12 col-xs-12 pull-right">
                        <input type="text" tabindex="7" placeholder="Phone No.*" name="phone" maxlength="19" class="form-control input_fild phonePattern">
                        <input type="text" tabindex="8" placeholder="Company*" name="company" maxlength="50"  class="required form-control input_fild">
                        <input type="text" tabindex="9" placeholder="Trade Name*" maxlength="50"  name="trade"class="required form-control input_fild">
                        <input type="text" tabindex="10" placeholder="Zipcode*" name="zipcode" class="form-control input_fild alphaNumeric">
                        <input type="text" tabindex="11"  name="captcha" placeholder="Security Code*" id="captcha-text" class="form-control input_fild">

                        <div style="margin-top:10px;" class="col-md-12 ">

                            <div id="captcha-wrap" class="col-md-12 padding_none">
                                <img src="<?php echo HOME_PATH ?>admin/captcha/captcha.php" alt="" id="captcha"/>
                                <img src="<?php echo HOME_PATH ?>images/captcha_refresh.gif" alt="refresh captcha" id="refresh-captcha" />
                            </div>
                        </div>
                    </div>
                    <div class=" col-md-12 col-sm-12 col-xs-12 text-center">
                        <input type="hidden" name="submitForm2" id="submitFor2" value="CREATE">
                        <input type="submit" tabindex="12" name="submitForm" id="submitForm1" class="btn btn-danger center-block btn_search" value="CREATE">
                        <p style="padding-top:8px;" class="text_login">Already have an account? <a href="<?php echo HOME_PATH . 'supplier/login' ?>">Login</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var path = "<?php echo HOME_PATH; ?>admin/";
        jQuery.validator.addMethod("phonePattern", function(value, element) {
            return this.optional(element) || /^[0-9\+\- ]+$/.test(value);
        }, "Enter valid phone number.");
        jQuery.validator.addMethod("alphaNumeric", function(value, element) {
            return this.optional(element) || /^[0-9a-zA-Z]+$/.test(value);
        }, "Only  Alphanumeric.");
        jQuery.validator.addMethod("NumericSpace", function(value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, "Only  Numeric.");
        jQuery.validator.addMethod("alphaNumSpF", function(value, element) {
            return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
        }, "Please enter valid first name");
        jQuery.validator.addMethod("alphaNumSpL", function(value, element) {
            return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
        }, "Please enter valid last name");

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space is not allowed");

        setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);
        $('#refresh-captcha').on('click', function() {
            $('#captcha-text').val("");
            $('#captcha').attr("src", path + "captcha/captcha.php?rnd=" + Math.random());
        });
        $('#sregister').validate({
            rules: {
                zipcode: {
                    required: true,
                    minlength: 3,
                    maxlength: 10
                },
                phone: {
                    required: true,
//                    NumericSpace: true,
                    minlength: 7,
                    maxlength: 19
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                conform_password: {
                    equalTo: "#password"
                },
                email: {
                    required: true,
                    email: true,
                    remote: {
                        url: path + 'ajax.php',
                        type: 'POST',
                        data: {action: "unquieEmail", email: function() {
                                return $('#email').val();
                            }
                        }
                    }
                },
                user_name: {
                    required: true,
                    minlength: 6,
                    maxlength: 30,
                    remote: {
                        url: path + 'ajax.php',
                        type: 'POST',
                        data: {action: "unquieUsername", username: function() {
                                return $('#username').val();
                            }
                        }
                    }
                }, captcha: {
                    required: true,
                    remote:
                            {
                                url: path + 'captcha/checkCaptcha.php',
                                type: "post",
                                data:
                                        {
                                            code: function()
                                            {
                                                return $('#captcha-text').val();
                                            }
                                        }
                            }
                }
            },
            messages: {
                zipcode: {
                    required: "This field is required ",
                    minlength: "Zipcode must contain at least 3 chars",
                    maxlength: "Zipcode should not be more than 10 characters"
                },
                phone: {
                    required: "This field is required ",
//                    NumericSpace: "Phone must be numeric",
                    minlength: "Phone must contain at least 7 chars",
                    maxlength: "Phone should not be more than 19 characters"
                },
                password: {
                    required: " This field is required",
                    minlength: "Password must contain at least 6 chars",
                    maxlength: "Password should not be more than 20 characters"
                },
                conform_password: {
                    equalTo: " Enter Confirm Password should be same as password."
                },
                email: {
                    required: "This field is required",
                    email: "Please enter a valid email address.",
                    remote: "Email address already in use. Please use other email."
                },
                user_name: {
                    required: "This field is required",
                    remote: "User name already in use. Please use other User name",
                    minlength: "User name  must contain at least 6 chars",
                    maxlength: "User name  should not be more than 30 characters"
                },
                captcha: {
                    required: "Please enter the verifcation code.",
                    remote: "Verication code incorrect, please try again."
                }
            },
            submitHandler: function(form) {
                    $('#submitForm1').attr('disabled', 'disabled');
                    form.submit();

            }
        });

    });
</script>
<style type="text/css">
    label.error{
        color:red;
        font-size: 10px;
    }
    input{
        color:black;
        font-size: 15px;
    }

</style>
