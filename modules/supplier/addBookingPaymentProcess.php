
<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
    label.error{
        color:red;
        font-size: 10px;
    }
    div.row div.form-group span.redCol{
        color:#FF0000;
    }
    label{
        margin-left: 50px;
    }
    #group{
        border: 1px solid green;
    }

</style>
<?php
//prd($_POST);
global $db;
$description = '';
include_once("./fckeditor/fckeditor.php");

$sql_query = "SELECT price  FROM " . _prefix("space_prices") . " where id=1";
$res = $db->sql_query($sql_query);
$priceRecord = $db->sql_fetchrow($res);
$sp_id = $_SESSION['id'];
$userid = $_SESSION['id'];
$pro_id = $_SESSION['proId'];
$pro_name = $_SESSION['proName'];
$sql_query = "SELECT * FROM " . _prefix("users") . " where  id =$userid";
$res = $db->sql_query($sql_query);
$userData = $db->sql_fetchrow($res);
//    prd($userData);
$first_name = $userData['first_name'];
$last_name = $userData['first_name'];
$address = $userData['physical_address'];
$city = $userData['address_city'];
$countryCode = 'NZ';
$pin = $userData['zipcode'];
$Tempimage = "";
//if (isset($submit)) {
    $fields_array = ads_masters();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($_FILES['image']['name'])) {
        if ($_FILES['image']['error'] == 0) {
            $ext = end(explode('.', $_FILES['image']['name']));
            $target_pathL = ADMIN_PATH . 'files/ads_image/';
            $date = new DateTime();
            $timestamp = $date->getTimestamp();
            $adsImage = $timestamp . '_' . imgNameSanitize(basename($_FILES['image']['name']), 20);
            $target_path = $target_pathL . $adsImage;
            $target_path_thumb = $target_pathL . 'thumb_' . $adsImage;
            //move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
            move_uploaded_file($_FILES['image']['tmp_name'], $target_path);
            ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
            list($w_orig, $h_orig) = getimagesize($target_path);
                    if ($w_orig > 1280 || $h_orig > 720) {
                        ak_img_resize($target_path, $target_path, 500, 400, $ext);
                    }
            $Tempimage = $adsImage;
        }
    }
//}

$getMax = "SELECT max(order_by) as count from " . _prefix("ads_masters") . " WHERE status = 1 && deleted = 0";
$resMax = $db->sql_query($getMax);
$dataMax = mysqli_fetch_assoc($resMax);
$order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
$fields = array(
    'title' => trim($_POST['title']),
    'from_date' => trim($_POST['start_date']),
    'to_date' => trim($_POST['end_date']),
    'price' => trim($_POST['price']),
    'description' => trim($_POST['description']),
    'image' => $Tempimage,
    'space_id' => trim($_POST["space_id"]),
    'supplier_id' => $sp_id,
    'pro_id' => $pro_id,
    'ads_mode' => trim($_POST["ads_mode"]),
    'service_category_id' => trim($_POST["service_category_id"]),
    'payment_status' => 0,
    'created' => date('Y/m/d h:i:s', time())
);
$insert_result = $db->insert(_prefix('ads_masters'), $fields);
$adId=$db->last_id();
if ($insert_result) {
$msg = common_message(1, constant('INSERT'));
 $_SESSION['msg'] = $msg;    
} else {
    redirect_to(HOME_PATH . "supplier/add-manager");
}


$paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
$paypal_id = 'nigel@agedadvisor.co.nz'; // Business email ID
?>



<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-money"></i> <span>Payment</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left thumbnail">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="pull_left_bg" style="border:none;">
                                    <h3 class="hedding_h3">Make Payment By PayPal</h3><hr>
                                    <form class="form-horizontal form_payment" action="<?php echo $paypal_url; ?>" role="payment" method="POST" name='payment' id='payment'>
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">

                                                <div class="form-group"  style="margin-bottom:10px;">
                                                    <label for="amount" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Amount :</label>
                                                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12" style="margin-top:25px;font-weight: bold;">
                                                        $ <?php echo $_POST['price']; ?>
                                                    </div>
                                                    <input type="hidden" name="amount" value="<?php echo $_POST['price']; ?>">


                                                    <input type="hidden" name="notify_url" value="<?php echo HOME_PATH . "supplier/addNotifyPayment" ?>">
                                                    <input type="hidden" name="business" value="<?php echo $paypal_id; ?>">
                                                    <input type="hidden" name="cmd" value="_xclick">
                                                    <input type="hidden" name="item_name" value="<?php echo $pro_name; ?>">
                                                    <input type="hidden" name="item_number" value="<?php echo $adId; ?>">
                                                    <input type="hidden" name="credits" value="510">
                                                    <input type="hidden" name="quantity" value="1">
                                                    <input type="hidden" name="userid" value="<?php echo $userid; ?>">
                                                    <input type="hidden" name="cpp_header_image" value="paypal-test-image.jpg.jpg">
                                                    <input type="hidden" name="no_shipping" value="1">
                                                    <input type="hidden" name="currency_code" value="NZD">
                                                    <input type="hidden" name="handling" value="0">
                                                    <input type="hidden" name="cancel_return" value="<?php echo HOME_PATH . "supplier/add-manager" ?>">
                                                    <input type="hidden" name="return" value="<?php echo HOME_PATH . "supplier/addPaymentSuccess" ?>">

                                                    <img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">


                                                    <input name="no_note" type="hidden" value="1" />
                                                    <input name="lc" type="hidden" value="UK" />
            <!--                                        <input name="bn" type="hidden" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest" />-->
                                                    <input name="first_name" type="hidden" value="<?php echo $first_name; ?>" />
                                                    <input name="last_name" type="hidden" value="<?php echo $last_name; ?>" />
                                                    <input name="address1" type="hidden" value="<?php echo $address; ?>" />
                                                    <input name="city" type="hidden" value="<?php echo $city; ?>" />
                                                    <input name="country" type="hidden" value="NZ" />
                                                    <input name="payer_email" type="hidden" value="<?php echo $email; ?>" />
                                                    <input name="zip" type="hidden" value="<?php echo $pin; ?>" />
                                                </div>
                                                <div class="form-group text-center total_bg" style="width:100%; padding:2% 0 0 0;">

                                                    <input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <style type="text/css">
        label.error{
            color:red;
            font-size: 10px;

        }
        input.error{
            color:#000;

        }
        select.error{
            color:#000;

        }
        .redCol{
            color:red;
        }

    </style>


