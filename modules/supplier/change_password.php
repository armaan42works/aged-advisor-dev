<?php
include("../application_top.php");

$agree_terms_already=0;
$id = md5($_SESSION['userId']);
// Have they agreed to terms already?
    $sql_query = "select agree_terms from " . _prefix("users") . " where md5(id)='$id' && user_type=1 ";
    
    
    //echo $sql_query;exit;
    
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count(array_filter($records))) {
        $agree_terms_already = $records[0]['agree_terms'];
	}








// Review Email Settings

	if(isset($_POST['allow_emails_reviews']) && $_POST['allow_emails_reviews'] == 'on'){$allow_emails_reviews=1;}else{$allow_emails_reviews=0;}
	if(isset($_POST['email_reviews_facility']) && $_POST['email_reviews_facility'] == 'on'){$email_reviews_facility =1;}else{$email_reviews_facility=0;}
	if(isset($_POST['email_reviews_head_office']) && $_POST['email_reviews_head_office'] == 'on'){$email_reviews_head_office =1;}else{$email_reviews_head_office=0;}

// Updates Email settings

	if(isset($_POST['allow_emails_updates']) && $_POST['allow_emails_updates'] == 'on'){$allow_emails_updates=1;}else{$allow_emails_updates=0;}
	if(isset($_POST['email_updates_facility']) && $_POST['email_updates_facility'] == 'on'){$email_updates_facility =1;}else{$email_updates_facility=0;}
	if(isset($_POST['email_updates_head_office']) && $_POST['email_updates_head_office'] == 'on'){$email_updates_head_office =1;}else{$email_updates_head_office=0;}






	if(isset($_POST['agree_terms']) && $_POST['agree_terms'] == 'on'){$agree_terms=1;}else{$agree_terms=0;}


if ($_REQUEST['submit']) {
    // prd($_POST);
    // $useremail = $_REQUEST['user_email'];
    $id = md5($_SESSION['userId']);
    //prd($id);
    $sql_query = "select * from " . _prefix("users") . " where md5(id)='$id' && user_type=1 && password='" . md5($old_password) . "'";
    
    
    //echo $sql_query;exit;
    
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count(array_filter($records))) {
        $to = $records[0]['email'];
        
        $new_password = md5($new_password);

if($agree_terms_already){
	        $fields = array('password' => $new_password,
            'first_login' => 1,
            'validate' => 2);           
	}else{            
        $fields = array('password' => $new_password,
            'first_login' => 1,
            'validate' => 2,
            'agree_terms' => 1,
            'allow_emails_reviews' => $allow_emails_reviews,
            'email_reviews_facility' => $email_reviews_facility,
            'email_reviews_head_office' => $email_reviews_head_office,
            'allow_emails_updates' => $allow_emails_updates,
            'email_updates_facility' => $email_updates_facility,
            'email_updates_head_office' => $email_updates_head_office
        );}
        
        
        
        
        $where = "where md5(id)='$id'";
        $update_result = $db->update(_prefix('users'), $fields, $where);
        if ($update_result) {
            $msg =  'Your Password has now been updated.';
            $_SESSION['msg'] = $msg;
            $_SESSION['SupValidate'] = 2;
            redirect_to(HOME_PATH . 'supplier/productList');
        }
    } else {
        $msg = common_message_supplier(0, 'Please enter correct current (or temporary) password');
        $_SESSION['msg'] = $msg;
    }

    if (isset($_SESSION['xlsUser'])) {
        //prd($_SESSION['xlsUser']);
        //$msg = common_message_supplier(1, constant('CHANGE_PASSWORD'));
        // $_SESSION['msg'] = $msg;
        $_SESSION['msg'] = '';
        
        unset($_SESSION['xlsUser']);
        redirect_to(HOME_PATH . 'supplier/productList');
    }
}
?>
    <!--=== Content Part ===-->
    <div>
    <div class="container content ">
        <div class="row" style="    margin-top: 40px;">
            <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2" style="    margin: 0 auto;">
                
                
                
                        <?php if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                        ?>
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo $_SESSION['msg']; ?>
                        </div>
                        <?php
                        unset($_SESSION['msg']);
                    }
                     if (isset($errors) && !empty($errors)) {
                        ?>
                        <div class="alert alert-warning alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo $errors; ?>
                        </div>
                        <?php
                    }
                    ?>

                
                
                
                
                <form name="form1" id="changePassword" action="" method="post" onsubmit="return Email_valid()">
                    <div class="reg-header">
                        <h2 class="text-center">Please update your password</h2>
                    </div>
                    <input type="password" placeholder="Old Password (or Temporary Password)*" name="old_password"  id="old_password" maxlength="50"  class="required form-control input_fild"><br>

                    <input type="password" placeholder="New Password *" name="new_password"  id="new_password" maxlength="50"  class="required form-control input_fild margin-top-20" value="<?php if(isset($_POST['new_password'])){echo $_POST['new_password']; }?>"><br>

                    <input type="password" placeholder="Repeat new Password *" name="conf_password"  id="conf_password" maxlength="50"  class="required form-control input_fild margin-top-20" value="<?php if(isset($_POST['conf_password'])){echo $_POST['conf_password']; }?>"><br>

                      <?php if(!$agree_terms_already){?>                   
	                    <h3>Email Options</h3>
                    <div id="show_change_reviews" style="margin: 0px 0px 10px 40px;"><strong>When someone posts a review...</strong><br>
	                    <input id="email_reviews_facility" type="checkbox" class="margin-top-20" name="email_reviews_facility" value="on" <?php
                                    if (isset($email_reviews_facility) && $email_reviews_facility) {
                                        echo 'checked="checked"';
                                    } else {
                                        echo '';
                                    }
                                    ?>><label for="email_reviews_facility" style="display: inline;font-weight: normal;"> <span>Email a copy to the Facility.</span></label><br>
									<input id="email_reviews_head_office" type="checkbox" class="margin-top-20" name="email_reviews_head_office" value="on" <?php
                                    if (isset($email_reviews_head_office) && $email_reviews_head_office) {
                                        echo 'checked="checked"';
                                    } else {
                                        echo '';
                                    }
                                    ?>><label for="email_reviews_head_office" style="display: inline;font-weight: normal;"> <span>Email a copy to Head Office.</span></label><br>
                    </div>
                        
                                        <div id="show_change_updates" style="margin: 0px 0px 10px 40px;"><strong>For important news or updates...</strong><br>
	                    <input id="email_updates_facility" type="checkbox" class="margin-top-20" name="email_updates_facility" value="on" <?php
                                    if (isset($email_updates_facility) && $email_updates_facility) {
                                        echo 'checked="checked"';
                                    } else {
                                        echo '';
                                    }
                                    ?>><label for="email_updates_facility" style="display: inline;font-weight: normal;"> <span>Email a copy to the Facility.</span></label><br>
									<input id="email_updates_head_office" type="checkbox" class="margin-top-20" name="email_updates_head_office" value="on" <?php
                                    if (isset($email_updates_head_office) && $email_updates_head_office) {
                                        echo 'checked="checked"';
                                    } else {
                                        echo '';
                                    }
                                    ?>><label for="email_updates_head_office" style="display: inline;font-weight: normal;"> <span>Email a copy to Head Office.</span></label><br>
                    </div>
					<br>
                    <input id="agree_terms" type="checkbox" class="margin-top-20" name="agree_terms" value="on" <?php
                                    if (isset($agree_terms) && $agree_terms) {
                                        echo 'checked="checked"';
                                    } else {
                                        echo '';
                                    }
                                    ?>><label for="agree_terms" style="display: inline;font-weight: normal;"> <span>I agree with <a href="<?php echo HOME_PATH?>terms_and_conditions" target="_blank">Terms and Conditions</a> of Aged Advisor.</span></label>
 <?php }else{?><input type="hidden" name="agree_terms" id="agree_terms" value="on"><?php }?>  
 </br>                                 
									<div class="submit-div"> <input type="submit" name="submit" class="btn v3" value="Submit">
										<a href="javascript:history.back()"  >Cancel</a></div>
                    
                </form>
                
            </div>
        </div>
    </div>
    </div>



<script type="text/javascript">

	$("#allow_emails_reviews").change(function() {
	if($(this).is(':checked')){
	$('#show_change_reviews').slideDown('slow');
	}else{
	$('#show_change_reviews').slideUp('slow');
	}});
		
	$("#allow_emails_updates").change(function() {
	if($(this).is(':checked')){
	$('#show_change_updates').slideDown('slow');
	}else{
	$('#show_change_updates').slideUp('slow');
	}});
		
	
	
    $(document).ready(function() {




        $('#changePassword').validate({
            rules: {
                old_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                new_password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20
                },
                conf_password: {
                    equalTo: "#new_password"
                },
                agree_terms: {
                    required: true
                }

            },
            messages: {
                old_password: {
                    required: " This field is required",
                    minlength: "Password must be contain at least 6 chars",
                    maxlength: "Password must be contain maximum 20 chars"
                },
                new_password: {
                    required: " This field is required",
                    minlength: "Password must be contain at least 6 chars",
                    maxlength: "Password must be contain maximum 20 chars"
                }, conf_password: {
                    equalTo: " Enter Confirm Password Same as Password"
                },
                agree_terms: {
                    required: "You must agree to our Terms and conditions&nbsp;&nbsp;&nbsp;"
				}
            }
        });
    });
</script>
<style>

    label.error{
        color:red;
        font-size: 10px;

    }
    .redCol{
        color:red;
    }

</style>