<?php
    global $db;
    $image = '';
    $useId = $_SESSION['id'];
    include_once("./fckeditor/fckeditor.php");
    if (isset($submit1) && $submit1 == 'submit') {
        $fields_array = artwork();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
                        echo 1;
            $qry = 'select * from ' . _prefix('artwork_users') . ' where user_id="' . $useId . '" && deleted=0 && status=1 limit 1';
            $result = $db->sql_query($qry);
            $data = $db->sql_fetchrow($result);
            if ($db->sql_numrows($result) > 0) {
                $artw_user_id = $data['id'];
                $total_requested = $data['total_requested'] + 1;

                $fields = array(
                    'total_requested' => $total_requested,
                    'modified' => date('Y-m-d h:i:s', time())
                );
                $where = "where id= '$artw_user_id'";
                $update_result = $db->update(_prefix('artwork_users'), $fields, $where);
            } else {
                $fields = array(
                    'user_id' => $useId,
                    'created' => date('Y-m-d h:i:s', time())
                );
                $insert_result = $db->insert(_prefix('artwork_users'), $fields);
                $artw_user_id = $db->last_id();
            }


            if (isset($_FILES['image']['name'])) {

                if ($_FILES['image']['error'] == 0) {
                    $ext = end(explode('.', $_FILES['image']['name']));
                    //prd($_FILES['image']['name']);
                    $target_pathL = 'admin/files/artwork/';

                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $logoName = $timestamp . '_' .imgNameSanitize(basename($_FILES['image']['name']));
                    $target_path = $target_pathL . $logoName;
                    $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                    move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                    ak_img_resize($target_path, $target_path_thumb, 60, 60, $ext);
                    $_POST['image'] = $logoName;
                }
            }
            $date1 = str_replace("/", "-", $_POST['expected_date']);
            $expected_date = date('Y-m-d', strtotime($date1));
            $fields = array(
                'user_id' => $useId,
                'artw_user_id' => $artw_user_id,
                'pro_id'=>$_SESSION['proId'],
                'title' => trim($_POST['title']),
                'client_description' => trim($_POST['description']),
                'expected_date' => $expected_date,
                'client_given' => $_POST["image"],
                'created' => date('Y-m-d h:i:s', time()),
                'approve_status' => 0
            );
            $insert_result = $db->insert(_prefix('artworks'), $fields);
            if ($insert_result) {
                // Message for insert
                $msg = common_message_supplier(1, constant('INSERT_ATWORK'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(HOME_PATH . "supplier/artwork-manager");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }

    if (key($_REQUEST) == 'edit') {
        $id = $_GET['id'];
        $sql_query = "SELECT * FROM " . _prefix("artworks") . " where md5(id)='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        if (count($records)) {
            foreach ($records as $record) {
                $name = $record['name'];
                $description = $record['client_description'];
                $image = $record['client_given'];
                $expected_date =date('d/m/y', strtotime($record['expected_date']));
                $title = $record['title'];
            }
        }
    }

    if (isset($submit1) && $submit1 == 'update') {
        $fields_array = artwork();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            if (isset($_FILES['image']['name'])) {
                    $ext = end(explode('.', $_FILES['image']['name']));

                if ($_FILES['image']['error'] == 0) {
                    //prd($_FILES['image']['name']);
                    $target_pathL = 'admin/files/artwork/';
                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $logoName = $timestamp . '_' . basename($_FILES['image']['name']);
                    $target_path = $target_pathL . $logoName;
                    $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                    move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                    ak_img_resize($target_path, $target_path_thumb, 60, 60,$ext);
                    $_POST['image'] = $logoName;
                }
            }
            $image = $_POST["image"];

            $date1 = str_replace("/", "-", $_POST['expected_date']);
            $expected_date = date('Y-m-d', strtotime($date1));
            $fields = array(
//                    'user_id' => $useId,
                'title' => trim($_POST['title']),
                'client_description' => trim($_POST['description']),
                'expected_date' => $expected_date,
                'modified' => date('Y-m-d h:i:s', time())
            );
            if ($_POST['image'] != '') {
                $fields['client_given'] = $image;
            }
            $where = "where md5(id)= '$id'";
            $update_result = $db->update(_prefix('artworks'), $fields, $where);
            if ($update_result) {
                // Message for insert
                $msg = common_message_supplier(1, constant('UPDATE_ATWORK'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(HOME_PATH . "supplier/artwork-manager");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>

<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <!--<h4 class="text-center hedding_main">Artwork Manager</h4>-->
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                <div class="dashboard_right_col">
                    <h2 class="hedding_h2"><i class="fa fa-briefcase"></i> <span>Artwork Manager</span></h2>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left"   id="pageData" >
                        <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
                            <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
                                <div class="col-sm-6">
                                    <?php if (key($_REQUEST) == 'edit') { ?>
                                            <h2 class="">Edit Artwork</h2>
                                        <?php } else { ?>
                                            <h2 class="">Add Artwork</h2>
                                        <?php } ?>
                                </div>

                                <div class="col-sm-6">
                                    <h2 style="float:right;" class="redCol small">* fields required</h2>
                                </div>
                            </div>
                            <p class="clearfix"></p>
                            <form name="addLogo" id="addLogo" action="" method="POST" class="form-horizontal form_payment" role="form" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
                                        <div class="form-group" style="margin-bottom:6px;">
                                            <label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Title&nbsp;<span class="redCol">*</span></label>
                                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                <input id="title" name="title" maxlength="50" class="form-control input_fild_1 required" type="text" value="<?php echo isset($title) ? stripslashes($title) : ''; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom:6px;">
                                            <label for="description" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Description&nbsp;</label>
                                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                <textarea name="description" class="form-control input_fild_1" maxlength="500" rows="5"><?php
                                                        if ((isset($description)) && !empty($description)) {
                                                            echo stripslashes($description);
                                                        }
                                                    ?></textarea>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom:6px;">
                                            <label for="expected_date" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Expected Date&nbsp;<span class="redCol">*</span></label>
                                            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                <input type="text"  class="form-control input_fild_1 required" name="expected_date" readonly="" id="expectDate" maxlength="50" value="<?php echo isset($expected_date) ? stripslashes($expected_date) : ''; ?>"/>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom:6px;">
                                            <label for="expected_date" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Artwork image&nbsp;<span class="redCol">*</span></label>
                                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12">
                                                <input name="image" id="image" class="form-control_browse input_fild_img_browse required" type="file">
                                            </div>
                                            <div class="col-md-12 text-center ">
                                                <?php if (file_exists(DOCUMENT_PATH . 'admin/files/artwork/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
                                                        <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/artwork/' . $image ?>"/>
                                                    <?php } ?>
                                            </div>
                                        </div>

                                        <div class="form-group" style="margin-bottom:6px;">
                                            <div class="col-sm-offset-2 col-sm-8">
                                                <?php if (key($_REQUEST) == 'edit') { ?>
                                                        <button type="submit" value="update" name="submit2" id="submit1" class="btn btn-danger center-block btn_search">Update</button>
                                                        <input type="hidden" name="submit1" value="update"/>
                                                    <?php } else { ?>
                                                        <button type="submit" value="submit" name="submit2" id="submit1" class="btn btn-danger center-block btn_search">Submit</button>
                                                        <input type="hidden" name="submit1" value="submit"/>
                                                    <?php } ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </form>
                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
    label.error{
        color:red;
        font-size: 10px;

    }
    .redCol{
        color:red;
    }

</style>
<script type="text/javascript">
    $('#expectDate').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '2010',
        minDate: 0,
    });
    $(document).ready(function() {
        $("#error").fadeOut(8000);

        $('input[type="submit"]').focus();
    });
    $('#addLogo').validate({
            submitHandler: function(form) {
                    $('#submit1').attr('disabled', 'disabled');
                    form.submit();

            }
        });
<?php if (file_exists(DOCUMENT_PATH . 'admin/files/artwork/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
            $('#image').removeClass('required');
    <?php } ?>
</script>

<!--</div>-->