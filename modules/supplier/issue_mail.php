<?php
/*
 * Objective: Listing of all the coupons for new Clients
 * Filename: coupon.php
 * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
 * Created On : 25 August 2014
 */
?>
<?php
global $db;
$image = '';
$useId = $_SESSION['id'];
if (isset($submit) && $submit == 'send_mail') {
    //  $fields_array = artwork();
    $to = ADMIN_EMAIL;
    $qry = 'select * from ' . _prefix('users') . ' where id="' . $useId . '" && deleted=0 && status=1';
    $result = $db->sql_query($qry);
    $data = $db->sql_fetchrow($result);
    $name = ucwords($data['first_name'] . " " . $data['last_name']);
    $usercompany = $data['company'];
    $useremail = $data['email'];
    $userid = $data['id'];
    $userphone = $data['phone'];
    $bcc = (isset($cc_me) && $cc_me == 1 ) ? $_SESSION['userEmail'] : NULL;
    $issue = $description;
    $email = emailTemplate('issue');
    if ($email['subject'] != '') {
        $message = str_replace(array('{username}', '{issue}','{id}','{phone}','{company}'), array($name, $issue,$userid,$userphone,$usercompany), $email['description']);
        /* @var $send type */
        $send = sendmail($to, $email['subject'], $message, '', $bcc);
        if ($send) {
            // Message for insert
            $msg = common_message_supplier(1, constant('ISSUE_SENT'));
            $_SESSION['msg'] = $msg;
            //ends here
        } else {
            $_SESSION['msg'] = common_message_supplier(0, 'Mail has not been sent to your email Id');
            //ends here
        }
    }
    $succesCount++;
    redirect_to(HOME_PATH . "supplier/issue_mail");
}
?>
<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success" style="margin-top: 10px;">
        <?php
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <?php echo $_SESSION['msg']; ?><br>
            <?php
            unset($_SESSION['msg']);
        }
        ?>

    </div>
</div>

<div class="row">
	<!--<h4 class="text-center hedding_main">Artwork Manager</h4>-->
	<div class="dashboard_container">
		<?php require_once 'includes/sp_left_navigation.php'; ?>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="dashboard_right_col">
				<h2 class="hedding_h2"><i class="fa fa-envelope-o"></i> <span>Email Aged Advisor</span></h2>
				
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="pageData" style="padding:0 15px;">
						<div style="border: 1px solid rgb(204, 204, 204); padding:0 15px;">
							<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
								<div class="col-sm-9">
									<h2 class="">Suggestions / Feedback / Assistance?</h2>
								</div>
	
								<div class="col-sm-3">
									<h2 style="float:right;" class="redCol small">* fields required</h2>
								</div>
							</div>
							<p class="clearfix"></p>
							<form name="issue_mail" id="issue_mail" action="" method="POST" class="form-horizontal form_payment" role="form" enctype="multipart/form-data">
								<div class="row">
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
	
										<div class="form-group" style="margin-bottom:6px;">
											<label for="description" class="col-lg-4 col-md-4 col-sm-10 col-xs-12">Message&nbsp;<span class="redCol">*</span></label>
											<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
												<textarea name="description" class="required form-control" minlength="3" maxlength="500" rows="10"><?php
													if ((isset($description)) && !empty($description)) {
														echo stripslashes($description);
													}
													?></textarea>
											</div>
										</div>
										<div class="form-group" style="margin-top:26px;">
											<label for="cc_me" class="col-lg-4 col-md-4 col-sm-10 col-xs-12" style="margin-top: 6px;">CC  (<?php echo $_SESSION['userEmail']?>)<span class="redCol">*</span></label>
											<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" >
												<label class="radio-inline"><input type="radio" name="cc_me" class="cc_me" value="1"/>Yes</label> &nbsp;<label class="radio-inline"><input type="radio" name="cc_me" class="cc_me" value="0" checked="checked" />No</label></div>
										</div>
	
										<div class="form-group" style="margin-top:20px;margin-bottom:16px;">
											<div class="col-sm-offset-2 col-lg-8 col-md-9 col-xs-12 col-sm-12 text-center">
												<button type="submit" value="send_mail" name="submit" class="btn btn-danger center-block browse_btn">Send Mail</button>
	
											</div>
										</div>
									</div>
								</div>
	
							</form>
						</div>
	
					</div>
				</div>
			</div>

		</div>
	</div>
</div>

<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
    label.error{
        color:red;
        font-size: 10px;

    }
    .redCol{
        color:red;
    }

</style>
<script type="text/javascript">

    $(document).ready(function() {
        $('#issue_mail').validate();
        setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 15000);
    });
</script>

<!--</div>-->