<?php 
$send = FALSE;
global $db;
if (isset($submitForm2) && $submitForm2 == 'CREATE') {
    $fields_array = userSupplier();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        if (isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response'])) {
            $secret = '6Ldc7iAUAAAAAJKYL3I8xHTHxQCgcTyNLl24NNey';
            //get verify response data
            $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $secret . '&response=' . $_POST['g-recaptcha-response']);
            $responseData = json_decode($verifyResponse);
            
            if ($responseData->success) {
                // SE+ND AN EMAIL
                $message = "<br>A facility has requested their username and password. You may want to check that they are in the system first.";
                $message.="<br>";
                $message.=$first_name;
                $message.=$last_name."<br>";
                $message.=$company."<br>";
                $message.=$email."<br>";
                $message.=$phone."<br>";
                $message.="<br>";
                $message.="<br>Aged Advisor New Zealand | www.agedadvisor.nz | Private Bag 4707 | Christchurch | 0800 243 323 (AGEDADV)";

                $send = sendmail('nigel@agedadvisor.co.nz', '!!!Request for access.', $message);

                $send = TRUE;
                $_SESSION['msg'] = "We have sent your request.";
            } else {
                $errors = '';
                $errors = 'Robot verification failed, please try again.';
            }            
        }
        
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>





<div class="user-login-section section-padding bg-fixed">
  <div class="container" style=" ">
<div class="row"  >
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <h3 class="bg-success"><?php echo $_SESSION['msg']; ?></h3>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>
<script src='https://www.google.com/recaptcha/api.js'></script>
  <div class="row">
 
  
  
 	<div class="col-sm-12 login-wrapper1" id="community-frm" style="padding: 46px;">
   <h1 class="theme_color">Request your login details</h1>
                    <p>Enter your details and we will contact you shortly.</p><br>
     <span id="comm_message" style="color:red; text-center;"></span>   
              <form name="sregister" id="sregister" method="POST" action=""  class="form-horizontal" role="form">
		
		<div class="row">
          <div class="col-md-6">
            <div class="form-group">
           <label>First Name</label>
       <input type="text" tabindex="1" placeholder="First Name*" name="first_name" maxlength="50"  class="required form-control filter-input input_fild alphaNumSpF">
               </div>
            </div>
			 <div class="col-md-6">
             <div class="form-group">
              <label>Last Name</label>
             <input type="text" tabindex="2" placeholder="Last Name" name="last_name" maxlength="50"  class="form-control filter-input input_fild alphaNumSpL">
                </div>
                 </div>
				  <div class="col-md-6">
             <div class="form-group">
              <label>Email</label>
       	 
                        <input type="text" tabindex="3" placeholder="Email*" name="email" id="email" class=" form-control filter-input input_fild alphaNumSp">
                </div>
                 </div>
				 
				 
				  <div class="col-md-6">
             <div class="form-group">
              <label>Phone</label>
              <input type="text" tabindex="4" placeholder="Phone No.*" name="phone" maxlength="19" class="form-control filter-input input_fild phonePattern">
                </div>
                 </div>
				 
			 
                
		 	 	  <div class="col-md-6">
             <div class="form-group">
              <label>Company</label>
           <input type="text" tabindex="5" placeholder="Company*" name="company" maxlength="50"  class="required form-control filter-input input_fild">
                </div>
                 </div>
 
			<div class="form-group" >
				<div class="row">
					<div class="col-sm-6" style="  margin-left:15px">
					 <div class="g-recaptcha" data-sitekey="6Ldc7iAUAAAAANMNq065X8wNJOepTkdzXO0pa57C"></div>
					</div>
				</div>
			</div>
			<div class="form-group" >
				<div class="row">
					<div class="col-sm-6" style="padding-left: 30px;">
					  <input type="hidden" name="submitForm2" id="submitFor2" value="CREATE">
                        <input type="submit" tabindex="12" name="submitForm" id="submitForm1" class="btn v3" value="SUBMIT">
                       
					</div>
				</div>
				 <p style="padding-top:8px; margin-left:17px" class="text_login">Already have an account? <a href="<?php echo HOME_PATH . 'supplier/login' ?>">Login</a></p>
			</div>
			
		 
        </form> 
        
   </div>
  
  </div>
  </div>
  </div>
 
 
<script type="text/javascript">
    $(document).ready(function() {
        var path = "<?php echo HOME_PATH; ?>admin/";
        jQuery.validator.addMethod("phonePattern", function(value, element) {
            return this.optional(element) || /^[0-9\+\- ]+$/.test(value);
        }, "Enter valid phone number.");
        jQuery.validator.addMethod("alphaNumeric", function(value, element) {
            return this.optional(element) || /^[0-9a-zA-Z]+$/.test(value);
        }, "Only  Alphanumeric.");
        jQuery.validator.addMethod("NumericSpace", function(value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, "Only  Numeric.");
        jQuery.validator.addMethod("alphaNumSpF", function(value, element) {
            return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
        }, "Please enter valid first name");
        jQuery.validator.addMethod("alphaNumSpL", function(value, element) {
            return this.optional(element) || /^[a-zA-Z\'\.\-\ ]+$/.test(value);
        }, "Please enter valid last name");

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "Space is not allowed");

        setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);
        /*
        $('#refresh-captcha').on('click', function() {
            $('#captcha-text').val("");
            $('#captcha').attr("src", path + "captcha/captcha.php?rnd=" + Math.random());
        });*/
        $('#sregister').validate({
            rules: {
                 phone: {
                    required: true,
//                    NumericSpace: true,
                    minlength: 7,
                    maxlength: 19
                },
               email: {
                    required: true,
                    email: true,
                    remote: {
                        url: path + 'ajax.php',
                        type: 'POST',
                        data: {action: "unquieEmail", email: function() {
                                return $('#email').val();
                            }
                        }
                    }
                }, 
                captcha: {
                    required: true,
                    remote:
                    {
                        url: path + 'captcha/checkCaptcha.php',
                        type: "post",
                        data:
                            {
                                code: function()
                                {
                                    return $('#captcha-text').val();
                                }
                            }
                    }
                }
            },
            messages: {
                phone: {
                    required: "This field is required ",
//                    NumericSpace: "Phone must be numeric",
                    minlength: "Phone must contain at least 7 chars",
                    maxlength: "Phone should not be more than 19 characters"
                },
                email: {
                    required: "This field is required",
                    email: "Please enter a valid email address.",
                    remote: "Email address registered to an account.<br>Please use <a href=\"https://www.agedadvisor.nz/supplier/forgetPassword\">Retrieve Password</a> to gain access."
                },
                captcha: {
                    required: "Please enter the verifcation code.",
                    remote: "Verication code incorrect, please try again."
                }
            },
            submitHandler: function(form) {
                    $('#submitForm1').attr('disabled', 'disabled');
                    form.submit();

            }
        });

    });
</script>
<style type="text/css">
    label.error{
        color:red;
        font-size: 15px;
    }
    input{
        color:black;
        font-size: 15px;
    }

</style>
