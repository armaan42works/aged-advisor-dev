<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 5000);
        var page = 0;
        var li = '0_no';
        var data = '&id=53';
        changePagination(page, li, 'partwork', data);
    });
</script>
<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <?php echo $_SESSION['msg']; ?><br>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">


                    <div class="row">
                        <div class="col-md-8 padding_left">
                            <h2 class="hedding_h2"><i class="fa fa-briefcase"></i> <span>Artwork Manager</span></h2>
                        </div>
                        <div class="btn_listing pull-right text-right" style="display:inline-block;"><a class="btn btn-danger center-block btn_search btn_pay" href="<?php echo HOME_PATH . 'supplier/artwork' ?>">Add New</a></div>
                    </div>
                    <h4 class="logo_bottom_color font_size_16">Facility Name: <?php echo $_SESSION['proName']; ?></h4>
<!--                    <h2 class="hedding_h2"><i class="fa fa-briefcase"></i> <span>Artwork Manager</span></h2>
<div class="btn_listing pull-right text-right" style="width:15%; margin-right:2%;"><a href="artwork" class="btn btn-danger center-block btn_search btn_pay">Add New</a></div>-->
                    <!--            <div class="col-sm-offset-2 col-sm-8 padding_right">
                                    <a href="artwork"><input type="submit" value="ADD" name="submit" class="submit_btn btn" src="<?php // echo ADMIN_IMAGE;        ?>submit.gif"></a>

                                </div>-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdding_none scroll_2" id="pageData" >

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
