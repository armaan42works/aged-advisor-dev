<?php

$id = $_SESSION['id'];
$session_type = $_GET['set'];
$pr_id = $_GET['pro_id'];
//prd($_GET);
if ($session_type == 'proIn') {
    $sql_query = "SELECT pln.plan_id as planID,prd.id AS proId,prd.title AS proTitle,member.video_enable, member.youtube_video, member.image_enable, member.image_limit, "
            . " member.ext_url_enable,member.facility_desc_enable,member.staff_enable,member.management_enable,member.activity_enable, member.type FROM " . _prefix("products") . " AS prd "
            . "Left join " . _prefix("pro_plans") . " AS pln ON pln.pro_id=prd.id "
            . "Left join " . _prefix("membership_prices") . " AS member ON member.id=pln.plan_id "
            . "where md5(prd.id)='" . $pr_id . "' AND prd.supplier_id='$id' ORDER BY pln.id DESC";
    $res = $db->sql_query($sql_query);
    $data = $db->sql_fetchrow($res);
    $_SESSION['videoEnable'] = $data['video_enable'];
    $_SESSION['youtubeEnable'] = $data['youtube_video'];
    $_SESSION['imageEnable'] = $data['image_enable'];
    $_SESSION['mediaLimit'] = $data['image_limit'];
    $_SESSION['extUrlEnable'] = $data['ext_url_enable'];
    $_SESSION['facilityEnable'] = $data['facility_desc_enable'];
    $_SESSION['staffEnable'] = $data['staff_enable'];
    $_SESSION['managementEnable'] = $data['management_enable'];
    $_SESSION['activityEnable'] = $data['activity_enable'];
    $_SESSION['planType'] = !empty($data['type']) ? $data['type'] : '1';
    $_SESSION['planId'] = !empty($data['planID']) ? $data['planID'] : '1';
    $_SESSION['proEntry'] = 1;
    $_SESSION['proId'] = $data['proId'];
    $_SESSION['proName'] = $data['proTitle'];
    //prd($_SESSION);
    if(ISSET($_GET['edit'])){
        redirect_to(HOME_PATH . 'supplier/editAccountInfo');        
    }else if(ISSET($_GET['readreviews'])){
        redirect_to(HOME_PATH . 'supplier/feedback-center');
    }else if(ISSET($_GET['readqueries'])){
        redirect_to(HOME_PATH . 'supplier/enquiry-center');    
    }else{
    redirect_to(HOME_PATH . 'supplier/accountInfo');}
} elseif ($session_type == 'proOut') {
    unset($_SESSION['videoEnable']);
    unset($_SESSION['youtubeEnable']);
    unset($_SESSION['imageEnable']);
    unset($_SESSION['mediaLimit']);
    unset($_SESSION['extUrlEnable']);
    unset($_SESSION['planType']);
    unset($_SESSION['proEntry']);
    unset($_SESSION['proId']);
    unset($_SESSION['proTitle']);
    unset($_SESSION['rf']);
    redirect_to(HOME_PATH . 'supplier/productList');
} else {
    redirect_to(HOME_PATH . 'supplier/productList');
}
?>
