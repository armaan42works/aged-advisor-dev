<?php
$ipa = gethostbyaddr($_SERVER['REMOTE_ADDR']);
$id = $_SESSION['userId'];
$sql_query = "SELECT user.*,member.title AS acountType, member.price FROM " . _prefix("users") . " AS user"
        . " Left join " . _prefix("membership_prices") . " AS member ON member.id=user.account_type"
        . " WHERE user.id='$id'";
$res = $db->sql_query($sql_query);
$record = $db->sql_fetchrow($res);
// prd($record);
$name = $record['first_name'] . " " . $record['last_name'];
$user_name = $record['user_name'];
$organization = $record['company'];
$logo = $record['user_image'];
$physicalAddress = $record['physical_address'];
$address_other = $record['address_other'];
$address_suburb = $record['address_suburb'];
$address_city = $record['address_city'];
$postal_address_suburb = $record['postal_address_suburb'];
$postal_address_city = $record['postal_address_city'];
$postal_address_zip = $record['postal_address_zip'];
$contactPerson = $record['contact_person'];
$accounts_person = $record['accounts_person'];
$accounts_email = $record['accounts_email'];
$website = $record['website'];
$email = $record['email'];
$phone = $record['phone'];
$accountType = $record['acountType'];
$postalAddress = $record['address'];
$amount = $record['price'];
$user_image = $record['user_image'];
$zipcode = $record['zipcode'];
$overAll_Rating = overAllRating($id);
$lat = $record['latitude'];
$long = $record['longitude'];
 $_SESSION['SupImage'] =$user_image;
 $_SESSION['SupUserName'] = $user_name;
$allow_emails_reviews = $record['allow_emails_reviews'];
$email_reviews_facility = $record['email_reviews_facility'];
$email_reviews_head_office = $record['email_reviews_head_office'];
$allow_emails_updates = $record['allow_emails_updates'];
$email_updates_facility = $record['email_updates_facility'];
$email_updates_head_office = $record['email_updates_head_office'];


//if (empty($lat) && empty($long) && !empty($zipcode)) {
//    $val = getLnt($zipcode);
//    $lat = $val['lat'];
//    $long = $val['lng'];
//}


// for Service Cernter Location(s)
//$sql_queryL = "SELECT sl.locations AS locationId,city.title AS locations FROM " . _prefix("service_locations") . " AS sl "
//        . " Left join " . _prefix("cities") . " AS city ON city.id=sl.locations "
//        . " where sl.user_id ='$id'";
//$resL = $db->sql_query($sql_queryL);
//$LocationRecords = $db->sql_fetchrowset($resL);
// for Services Listed
//    $sql_queryF = "SELECT ft.id, sft.name FROM " . _prefix("sp_facility_type") . " AS ft "
//            . "LEFT JOIN  " . _prefix("services") . " sft ON sft.id=ft.facility_type where ft.user_id = '$id'";

?>
<script type="text/javascript">
    $(document).ready(function() {
        setTimeout(function() {
            $("#success").hide('slow');
            $('#error').hide('slow');
        }, 15000);
    });
</script>

<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <?php echo $_SESSION['msg']; ?><br>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<div class="container" style="padding:0px 3% 0">
    <div class="row">
        <div class="dashboard_container">
            <?php // require_once 'includes/sp_left_navigation.php'; ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="dashboard_right_col">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-md-9 col-lg-9 col-sm-9 col-xs-12">
                            <h2 class="hedding_h2"><i class="fa fa-book"></i> <span>Supplier/Legal (Owner) Profile Details</span></h2>
                        </div>
                        <div class="" style="width:100%;"><a class="btn btn-danger btn_search btn_pay margin-bottom-20 pull-right" href="<?php echo HOME_PATH . 'supplier/editProfile' ?>">Edit</a><a class="pull-left btn btn-info" href="<?php echo HOME_PATH ?>supplier/productList">Go Back</a></div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Supplier Logo:</span>
                                    </div> 
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <?php if (file_exists(DOCUMENT_PATH . 'admin/files/user/' . $user_image) && $user_image != '') { ?>
                                            <img title="User Image" src="<?php echo MAIN_PATH . 'files/user/thumb_' . $user_image ?>"/>
                                        <?php } else { ?>
                                            <img title="User Image" src="<?php echo IMAGES; ?>notlogo.jpeg"/>
                                        <?php } ?>
                                    </div>
                                </div>
                            </li>
                            <li class = "list-group-item">
                                <div class = "row">
                                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class = "pull-left left_td">User Name (Login Name):</span>
                                    </div>
                                    <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class = "pull-left right_td"><?php echo  empty($user_name)?'N/A':$user_name;  ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class = "list-group-item">
                                <div class = "row">
                                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class = "pull-left left_td">Organisation:</span>
                                    </div>
                                    <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class = "pull-left right_td"><?php echo empty($organization)?'N/A':$organization; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class = "list-group-item">
                                <div class = "row">
                                    <div class = "col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class = "pull-left left_td">User Email:</span>
                                    </div>
                                    <div class = "col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class = "pull-left right_td"><?php echo empty($email)?'N/A':$email; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Contact Person:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($contactPerson)?'N/A':$contactPerson; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Accounts Person:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($contactPerson)?'N/A':$accounts_person; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Accounts Email:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($contactPerson)?'N/A':$accounts_email; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Phone:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($phone)?'N/A':$phone; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Email Options:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td">
                       <?php
                                    if (isset($allow_emails_reviews) && $allow_emails_reviews) {
                                        echo 'Receive email when someone enters a review.<br>';}
                                    if (isset($email_reviews_facility) && $email_reviews_facility) {
                                        echo '&nbsp;&nbsp;Send Review Email to Facility.<br>';}
                                    if (isset($email_reviews_head_office) && $email_reviews_head_office) {
                                        echo '&nbsp;&nbsp;Send Review Email to Head Office.<br>';}
                                    if (isset($allow_emails_updates) && $allow_emails_updates) {
                                        echo 'Receive email for updates / new features / news.<br>';}
                                    if (isset($email_updates_facility) && $email_updates_facility) {
                                        echo '&nbsp;&nbsp;Send News Email to Facility.<br>';}
                                    if (isset($email_updates_head_office) && $email_updates_head_office) {
                                        echo '&nbsp;&nbsp;Send News Email to Head Office.<br>';}?>
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Website:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($website)?'N/A':$website; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Physical Address:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($physicalAddress)?'N/A':$physicalAddress; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Address Suburb/Road:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($address_suburb)?'N/A':$address_suburb; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Address Town/City:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($address_city)?'N/A':$address_city; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Address Post Code:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($zipcode)?'N/A':$zipcode; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Postal Address:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($postalAddress)?'N/A':$postalAddress; ?></span>
                                    </div>
                                </div>
                            </li>
                            
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Postal Address Suburb/Road:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($postal_address_suburb)?'N/A':$postal_address_suburb; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Postal Address Town/City:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($postal_address_city)?'N/A':$postal_address_city; ?></span>
                                    </div>
                                </div>
                            </li>
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Postal Address Post Code:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo empty($postal_address_zip)?'N/A':$postal_address_zip; ?></span>
                                    </div>
                                </div>
                            </li>
                            
                            <!--<li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <span class="pull-left left_td">Geographical Presence:</span><br/>
                                        <span class="pull-left left_td">(If not showing currect location, Edit profile and set your location.)</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12" style="overflow: hidden;">
                                        <span class="pull-left right_td"><div id="map" class="img-responsive" style="width:350px;height:200px; margin-top:10px;"></div></span>
                                    </div>
                                </div>
                            </li>-->
<!--                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Service Centre Location(s):</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td">
                                            <?php
//                                            $i = 1;
//                                            foreach ($LocationRecords as $record) {
//                                                echo $i++ . '. ' . $record['locations'] . '<br>';
//                                            }
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </li>-->

<!--                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Last Payment Made:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo isset($amount) ? '$' . stripslashes($amount) : 'N/A'; ?></span>
                                    </div>
                                </div>
                            </li>-->

<!--                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Overall Rating:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><?php echo $overAll_Rating; ?></span>
                                    </div>
                                </div>
                            </li>-->

<!--                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Services Listed:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td">
                                            <?php
                                            $i = 1;
                                            foreach ($facilityType as $record) {
                                                echo $i++ . '. ' . $record['name'] . '<br>';
                                            }
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </li>-->
<!--                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <span class="pull-left left_td">Apply for Upgrade:</span>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                        <span class="pull-left right_td"><a href="<?php echo HOME_PATH . 'supplier/payment' ?>">Apply</a></span>
                                    </div>
                                </div>
                            </li>-->
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>