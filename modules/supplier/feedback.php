
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '&id=53';
        changePagination(page, li, 'feedback', data);
    });
</script>

<div class="row">
	<div class="dashboard_container">
		<?php require_once 'includes/sp_left_navigation.php'; ?>
		<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
			<div class="dashboard_right_col">
				<h2 class="hedding_h2"><i class="fa fa-twitch"></i> <span>Read Reviews / Comments</span> </h2>
				<h4 class="logo_bottom_color font_size_16">Facility Name: <?php echo $_SESSION['proName']; ?></h4>
<!--                    <div class="col-sm-offset-2 col-sm-8 margin">
					<span>Overall Rating: <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i></span>
				</div>-->
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pdding_none scroll" id="pageData" >

				</div>
			</div>

		</div>
	</div>
</div>
