<?php
/*
 * Objective : All the popup on admin panel
 * Filename : popup.php
 * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
 * Created On : 7 August 2014
 * Modified On : 01 September 2014
 */
include("../application_top.php");

switch (key($_REQUEST)) {
    case "adsImage":
        echo adsImage($id);
        break;
    case "client_given":
        echo client_given($id);
        break;
    case "sliderImage":
        echo sliderImage($id);
        break;
    case "profile":
        echo userProfile();
        break;
    case "comment":
        echo blogComment();
        break;
    case "quoteDetail":
        echo quoteDetail($id);
        break;
    case "file":
        echo fileViewer($id);
        break;
    case "updateProgress":
        echo updateProgress($id);
        break;
    case "adminQuotes":
        echo adminQuotes($id);
        break;
    case "jobs":
        echo adminjobs($id);
        break;
    case "jobApprove":
        echo jobApprove($id, $userid);
        break;
    case "jobDeny":
        echo jobDeny($id, $userid);
        break;
    case "applicationMaterial":
        echo application_material($id);
        break;
    case "messageBody":
        echo messageBody($id);
        break;
    case "fileShow":
        echo fileShow($id);
        break;
    case "paymentApproval":
        echo paymentApproval($id);
        break;
    case "reviews":
        echo reviews($id);
        break;
}

function adsImage($id) {
    global $db;
    $id = $_GET["id"];
    $query = "SELECT image FROM `" . _prefix("ads_masters") . "`WHERE md5(id) = '$id'";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $data = $db->sql_fetchrow($res);
    // pr($data);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <div class="text-center">
        <img style="" src="<?php echo HOME_PATH_URL . 'files/ads_image/' . $data['image']; ?>" />
    </div>
    <?php
}

function client_given($id) {
    global $db;
    $id = $_GET["id"];
    $query = "SELECT client_given FROM `" . _prefix("artworks") . "`WHERE md5(id) = '$id'";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $data = $db->sql_fetchrow($res);
    // pr($data);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <div class="text-center">
        <img style="" src="<?php echo HOME_PATH_URL . 'files/artwork/' . $data['client_given']; ?>" />
    </div>
    <?php
}

function blogComment($id) {
    global $db;
    $id = $_GET["id"];
    $query = "SELECT comment FROM `" . _prefix("blog_comments") . "`WHERE md5(id) = '$id'";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $data = $db->sql_fetchrow($res);
    // pr($data);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <div class="" style="margin-top: 40px;margin-left: 20px;">

        <?php echo $data['comment'] != '' ? stripslashes($data['comment']) : 'N/A'; ?>

    </div>
    <?php
}

function sliderImage($id) {
    global $db;
    $id = $_GET["id"];
    $query = "SELECT image FROM `" . _prefix("sliders") . "`WHERE md5(id) = '$id'";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $data = $db->sql_fetchrow($res);
    // pr($data);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <div class="text-center">
        <img style="" src="<?php echo HOME_PATH_URL . 'files/slider/' . $data['image']; ?>" />
    </div>
    <?php
}

function userProfile() {
    global $db;
    $source = array('0' => 'Google Search', '1' => 'Sales Person', '2' => 'Sales Person', '3' => 'Online Ads', '4' => 'Existing clients', '5' => 'Newspaper', '6' => 'Facebook', '7' => 'LinkedIn', '8' => 'Twitter', '9' => 'Other');
    $id = $_GET["id"];
    $query = "SELECT * FROM `" . _prefix("users") . "`WHERE md5(id) = '$id'";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $data = $db->sql_fetchrow($res);
    // pr($data);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>

    <table align="center" border="0" cellpadding="0" cellspacing="0"
           width="98%" style="margin:5px 5px 0px;">

        <tbody>


            <tr>
                <td  colspan="2" width="100%">
                    <h3 style="color: #265190;text-align: center;">User profile:</h3><hr style="width: 100%;">

                </td>
            </tr>


            <tr>
                <td class="marginleft" align="right" height="26"><strong> User Name:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['first_name'] != '' ? stripslashes($data['first_name'] . ' ' . $data['last_name']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> IP Address:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['ip_address'] != '' ? stripslashes($data['ip_address']) : 'N/A'; ?>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26"><strong> Business Name:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['business'] != '' ? stripslashes($data['business']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Website:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['website'] != '' ? stripslashes($data['website']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Email:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['email'] != '' ? stripslashes($data['email']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Address:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['address'] != '' ? stripslashes($data['address']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Country:</strong></td>
                <?php
                $country_id = $data['country_id'];
                $cquery = "SELECT * FROM `" . _prefix("countries") . "` WHERE id = $country_id";
                $cres = $db->sql_query($cquery);
                $sdata = $db->sql_fetchrow($cres);
                //pr($sdata);
                ?>
                <td class="marginleft"><div class="input text">
                        <?php echo stripslashes($sdata["name"]); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> State:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php
                        $state_id = $data['state_id'];
                        $Squery = "SELECT * FROM `" . _prefix("states") . "` WHERE id = $state_id";
                        $sres = $db->sql_query($Squery);
                        $sdata = $db->sql_fetchrow($sres);
                        //pr($sdata);
                        ?>
                        <?php echo stripslashes($sdata["name"]); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Phone:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['phone'] != '' ? stripslashes($data['phone']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Cellphone:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['cellphone'] != '' ? stripslashes($data['cellphone']) : 'N/A'; ?>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26"><strong> Zip Code:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['zipcode'] != '' ? stripslashes($data['zipcode']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Source:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php $sid = $data['source_id']; ?>
                        <?php echo $source[$sid] != '' ? stripslashes($source[$sid]) : 'N/A'; ?>
                    </div></td>
            </tr>
        </tbody>

    </table>

    <?php
}

function quoteDetail($id) {
    global $db;
    $datasize = array('0' => 'Small', '1' => 'Medium', '2' => 'Large', '3' => 'Big Data');
    $getQuote = "SELECT dq.*,du.name, du.business FROM " . _prefix("quotes") . " as dq inner join " . _prefix("users") . " as du on dq.client_id = du.id WHERE md5(dq.id) = '$id'";
    $res = $db->sql_query($getQuote);
    $data = $db->sql_fetchrow($res);
    if ($data['coupon_id'] != '') {
        $getCoupon = "SELECT * FROM " . _prefix("coupons") . " WHERE code = '{$data['coupon_id']}'";
        $resCoupon = $db->sql_query($getCoupon);
        $dataCoupon = $db->sql_fetchrow($resCoupon);
        $subtype = $dataCoupon['subtype'] == '0' ? '$' : '%';
        $coupon .= "Coupon Code = {$dataCoupon['code']} <br>";
        $coupon .= "Amount($subtype) = {$dataCoupon['value']} <br>";
        $coupon .= "Expiration Date = " . date('M d, Y', strtotime($dataCoupon['expire_date']));
    } else {
        $coupon = 'N/A';
    }
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>

    <table align="center" border="0" cellpadding="0" cellspacing="0"
           style="margin-top:10px;">

        <tbody>

            <tr>
                <td  colspan="2" width="100%">
                    <h3 style="color: #265190;text-align: center;">Quote Detail:</h3><hr style="width: 100%;">

                </td>
            </tr>




            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> Client Name :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo stripslashes($data['name']); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> IP Address :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo stripslashes($data['ip_address']); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> Quote ID :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['unique_id'] == '' ? 'N/A' : stripslashes($data['unique_id']); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> Submitted On :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo date('M d, Y H :i', strtotime($data['created'])); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> Company :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo stripslashes($data['business']); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> Project Description :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo stripslashes($data['project_desc']); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> Data Size :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['datasize'] != '' ? $datasize[$data['datasize']] : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> Minimum Budget($) :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['min_budget'] != '' ? stripslashes($data['min_budget']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> Maximum Budget($) :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['max_budget'] != '' ? stripslashes($data['max_budget']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26" width="185" style="float:left;"><strong> Coupon Used By :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $coupon; ?>
                    </div></td>
            </tr>
            <?php
            if (isset($data['file_name']) && $data['file_name'] != '' && file_exists(DOCUMENT_PATH . 'admin/files/quote_files/' . $data['file_name'])) {
                ?>
                <tr>
                    <td class="marginleft" align="right" height="26" width="185" style="float:left;"></td>
                    <td class="marginleft"><div class="input text">
                            <a href="<?php echo HOME_PATH_URL ?>download.php?type=quote_files&file=<?php echo $data['file_name'] ?>" target="_blank">Document</a> |
                            <a href="<?php echo HOME_PATH_URL ?>project.php?file&id=<?php echo md5($data['id']); ?>" target="_blank">Open Document</a>
                        </div></td>
                </tr>
                <?php
            }
            ?>

        </tbody>

    </table>
    <?php
}

function fileViewer($id) {
    global $db;
    $getfilename = "SELECT file_name FROM " . _prefix("quotes") . " WHERE md5( id ) = '$id'";
    $resfile = $db->sql_query($getfilename);
    $record = $db->sql_fetchrow($resfile);
    $file = HOME_PATH_URL . 'files/quote_files/' . $record['file_name']
    ?>
    <div style='background-color:whiteSmoke; height: 36px; position: absolute; left:990px; width: 33px;z-index: 2147483647;'><iframe src="http://docs.google.com/viewer?url=<?php echo rawurlencode($file); ?>&embedded=true&rm=minimal" style="width:1000px;height:1000px;"></iframe></div>
    <?php
}

function updateProgress($id) {
    global $db;
    $sql_query = "SELECT id, unique_id, progress FROM " . _prefix("quotes") . " WHERE md5(id) = '$id'";
    $res = $db->sql_query($sql_query);
    $data = $db->sql_fetchrow($res);
    $options = array();
    $selected = $data['progress'];
    for ($i = 0; $i <= 100; $i +=10) {
        $options[$i] = $i;
    }
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>

    <table align="center" border="0" cellpadding="0" cellspacing="0"
           width="98%">

        <tbody>
            <tr>
                <td  colspan="2" width="100%">
                    <h3 style="color: #265190;text-align: center;">Upload Project Progress:</h3><hr style="width: 100%;">

                </td>
            </tr>


        <input type="hidden" id="id" value="<?php echo $data['id']; ?>">
        <tr>
            <td class="marginleft" align="right" height="26"><strong> Project ID :</strong></td>
            <td class="marginleft"><div class="input text">
                    <?php echo 'P' . $data['unique_id']; ?>
                </div></td>
        </tr>
        <tr>
            <?php ?>
            <td class="marginleft" align="right" height="26"><strong> Update Progress :</strong></td>
            <td class="marginleft"><div class="input text">
                    <select id="update_progress">
                        <?php
                        $select = '';
                        $option = '<option value="">---------------Select-------------------</option>';
                        foreach ($options as $key => $value) {
                            if ($selected != null) {
                                $select = $selected == $key ? 'selected' : '';
                            }
                            $option .= '<option value="' . $key . '" ' . $select . '>' . $value . ' %</option>';
                        }
                        echo $option;
                        ?>
                    </select>
                </div></td>
        </tr>
        <tr>
            <td class="marginleft" align="right" height="26"></td>
            <td class="marginleft"><div class="input text">
                    <input type="submit" value="close" name="close" class="closeprogresss">
                </div></td>
        </tr>


    </tbody>

    </table>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#update_progress').on('change', function() {
                var progress = $(this).val();
                var id = $('#id').val();
                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: {action: 'updateProgress', id: id, progress: progress},
                    success: function() {
                        window.parent.$('#close_' + id + ' #update_' + id).html('<br>' + progress + '% <br> Last Updated : <?php echo date('M d, Y H:i'); ?>');
                    }
                });
            });

            $('.closeprogresss').live('click', function() {
                $(window).colorbox.close();


            });
        });

    </script>

    <?php
}

function adminQuotes($id) {
    global $db;
    $getAdminQuotes = "SELECT dq.coupon_id as userCoupon,daq.*, du.name, du.business, dq.payment_status  FROM " . _prefix("quotes") . " as dq "
            . "inner join " . _prefix("admin_quotes") . " as daq on dq.id = daq.quotes_id "
            . "inner join " . _prefix("users") . " as du on dq.client_id = du.id "
            . "where md5(dq.id) = '$id' ";
    $res = $db->sql_query($getAdminQuotes);
    $data = $db->sql_fetchrow($res);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>

    <table align="center" border="0" cellpadding="0" cellspacing="0"
           width="98%" style="margin-top:10px;">

        <tbody>
            <tr>
                <td  colspan="2" width="100%">
                    <h3 style="color: #265190;text-align: center;">AdminQuote Detail:</h3><hr style="width: 100%;">

                </td>
            </tr>
            <tr>
                <td width="400px;" class="marginleft" align="right" height="26"><strong> Quote request by :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo stripslashes($data['name']); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Submitted On :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo ($data['modified'] == '0000-00-00 00:00:00') ? date('M d, Y', strtotime($data['created'])) : date('M d, Y', strtotime($data['modified'])); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Company :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['business']; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Quote Title :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo stripslashes($data['quote_title']); ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Quote Description :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo nl2br(stripslashes($data['quotre_desc'])); ?>
                    </div></td>
            </tr>

            <?php
            if (($data['coupon_assigned'] == 1 && ($data['coupon_id'] != 0)) || (isset($data['userCoupon']) && $data['userCoupon'] != '')) {
                if ($data['coupon_id'] == 0 || $data['coupon_id'] == '') {
                    $data['coupon_id'] = $data['userCoupon'];
                }
                $getCoupon = "SELECT code, value, expire_date, subtype from " . _prefix("coupons") . " where code= '{$data['userCoupon']}'";
                $resgetCoupon = $db->sql_query($getCoupon);
                $dataGetCoupon = $db->sql_fetchrow($resgetCoupon);
                $subtype = $dataGetCoupon['subtype'] == 0 ? '$' : '%'
                ?>
                <tr>
                    <td class="marginleft" align="right" height="26"><strong> Subtotal :</strong></td>
                    <td class="marginleft"><div class="input text">
                            <?php echo '$' . $data['subtotal']; ?>
                        </div></td>
                </tr>
                <tr>
                    <td class="marginleft" align="right" height="26"><strong> Coupon/Gift card detail :</strong></td>
                    <td class="marginleft"><div class="input text">
                            <?php
                            echo 'Code :' . $dataGetCoupon['code'] . "<br> Amount : ($subtype)" . $dataGetCoupon['value'] . "<br> Expiration Date : " . date('M d, Y', strtotime($dataGetCoupon['expire_date']));
                            echo date('Ymd', strtotime($dataGetCoupon['expire_date'])) <= date('Ymd') ? '<br><label class="redCol">Expired</label>' : '';
                            ?>
                        </div></td>
                </tr>
            <?php } ?>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Total :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo date('Ymd', strtotime($dataGetCoupon['expire_date'])) <= date('Ymd') && $data['payment_status'] == 0 ? '$' . $data['subtotal'] : '$' . $data['total']; ?>
                    </div></td>
            </tr>
            <?php
            if (isset($data['filename']) && $data['filename'] != '' && file_exists(DOCUMENT_PATH . 'admin/files/' . $data['filename'])) {
                ?>
                <tr>
                    <td class="marginleft" align="right" height="26"></td>
                    <td class="marginleft"><div class="input text">
                            <a href="<?php echo HOME_PATH_URL ?>download.php?file=<?php echo $data['filename'] ?>">Document</a>
                        </div></td>
                </tr>
                <?php
            }
            ?>

        </tbody>

    </table>
    <?php
}

function adminjobs($id) {
    global $db;
    $getPostionDescription = "SELECT description from  " . _prefix("position") . " where md5(id) = '$id' ";

    $res = $db->sql_query($getPostionDescription);
    $data = $db->sql_fetchrow($res);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>

    <table align="center" border="0" cellpadding="0" cellspacing="0"
           width="98%" style="margin-top:10px;">

        <tbody>
            <tr>
                <td  colspan="2" width="100%">
                    <h3 style="color: #265190;text-align: center;">Jobs Detail:</h3><hr style="width: 100%;">

                </td>
            </tr>
            <tr>
                <td width="400px;" class="marginleft" align="right" height="26"><strong> Description :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo stripslashes($data['description']); ?>
                    </div></td>
            </tr>

        </tbody>

    </table>


    <?php
}

function jobApprove($id, $userid) {


    global $db;
    $sql_query = "Select name, email from " . _prefix("users") . " where md5(id) = '$userid'";
    $res = $db->sql_query($sql_query);
    $data = $db->sql_fetchrow($res);
    $subject = "KA Analytics & Technology| New Job offered to you";
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <form id="jobApprove" class="jobApprove" method="post">
        <input type="hidden" value="<?php echo $subject ?>" name="subject">
        <input type="hidden" value="<?php echo $data['email']; ?>" name="email">
        <input type="hidden" value="2" name="jobAction">
        <input type="hidden" value="<?php echo $id; ?>" name="id">
        <table align="center" border="0" cellpadding="0" cellspacing="0"
               width="98%" style="margin:5px 5px 0px;">

            <tbody>
            <tbody>
                <tr>
                    <td width="40%">
                        <h2>Offer Salesperson the job :</h2>

                    </td>
                </tr>
            </tbody>


            <tr>
                <td class="marginleft" align="right" height="26"><strong>SalesPerson Name :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['name']; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong>Email Address:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['email']; ?>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26"><strong>Subject :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $subject; ?>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26"><strong>Message :</strong></td>
                <td class="marginleft"><div class="input text">
                        <textarea cols="40" rows="13" id="message" name="message" class="required"></textarea>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"></td>
                <td class="marginleft"><div class="input text">
                        <input type="submit" name="submit" value="submit" class="submitLoad">
                        <input type="button" name="close" value="close" onclick="parent.$.colorbox.close();">
                    </div><div id="loading" style="display:none;"><img src="<?php echo ADMIN_IMAGE ?>loading.gif"></div></td>
            </tr>

            </tbody>

        </table>
    </form>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#jobApprove').validate();
            $('.submitLoad').click(function() {
                if ($('#message').val() != '') {
                    $('#loading').show();
                } else {
                    $('#loading').hide();
                }
            });
        });

    </script>

    <?php
}

function jobDeny($id, $userid) {
    global $db;
    $sql_query = "Select name, email from " . _prefix("users") . " where md5(id) = '$userid'";
    $res = $db->sql_query($sql_query);
    $data = $db->sql_fetchrow($res);
    $subject = "KA Analytics & Technology| Job has been denied";
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <form id="jobDeny" name="jobDeny" method="post">
        <input type="hidden" value="<?php echo $subject ?>" name="subject">
        <input type="hidden" value="<?php echo $data['email']; ?>" name="email">
        <input type="hidden" value="3" name="jobAction">
        <input type="hidden" value="<?php echo $id; ?>" name="id">
        <table align="center" border="0" cellpadding="0" cellspacing="0"
               width="98%" style="margin:5px 5px 0px;">

            <tbody>
            <tbody>
                <tr>
                    <td width="40%">
                        <h2>Deny Salesperson the job :</h2>

                    </td>
                </tr>
            </tbody>


            <tr>
                <td class="marginleft" align="right" height="26"><strong>SalesPerson Name :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['name']; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong>Email Address:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['email']; ?>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26"><strong>Subject :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $subject; ?>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26"><strong>Message :</strong></td>
                <td class="marginleft"><div class="input text">
                        <textarea cols="40" rows="13" id="message" name="message" class="required"></textarea>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"></td>
                <td class="marginleft"><div class="input text">
                        <input type="submit" name="submit" value="submit" class="submitLoad">
                        <input type="button" name="close" value="close" onclick="parent.$.colorbox.close();">
                    </div><div id="loading" style="display:none;"><img src="<?php echo ADMIN_IMAGE ?>loading.gif"></div></td>
            </tr>

            </tbody>

        </table>
    </form>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#jobDeny').validate();

            $('.submitLoad').click(function() {
                if ($('#message').val() != '') {
                    $('#loading').show();
                } else {
                    $('#loading').hide();
                }
            });
        });

    </script>

    <?php
}

function application_material($id) {
    global $db;
    $sql_query = "Select coverletter , resumefile from " . _prefix("job_application") . " where md5(id) = '$id'";
    $res = $db->sql_query($sql_query);
    $data = $db->sql_fetchrow($res);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <form id="jobDeny" name="jobDeny" method="post">
        <table align="center" border="0" cellpadding="0" cellspacing="0"
               width="98%" style="margin:5px 5px 0px;">

            <tbody>
            <tbody>
                <tr>
                    <td width="40%">
                        <h2>SP Application Material:</h2>

                    </td>
                </tr>
            </tbody>


            <tr>
                <td class="marginleft" align="right" height="26"><strong>Cover Letter :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo ($data['coverletter'] != '') ? $data['coverletter'] : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong>Download Resume:</strong></td>
                <td class="marginleft"><div class="input text">
                        <a href="<?php echo HOME_PATH_URL ?>download.php?type=jobs&file=<?php echo $data['resumefile'] ?>" class="getFile" id="<?php echo $data['resumefile']; ?>">Download Resume</a>
                    </div></td>
            </tr>



            </tbody>

        </table>
    </form>

    <?php
}

function messageBody($id) {
    global $db;
    $sql_query = "Select subject , body from " . _prefix("messages") . " where md5(id) = '$id'";
    $res = $db->sql_query($sql_query);
    $data = $db->sql_fetchrow($res);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <form id="jobDeny" name="jobDeny" method="post">
        <table align="center" border="0" cellpadding="0" cellspacing="0"
               width="98%" style="margin:5px 5px 0px;">

            <tbody>
            <tbody>
                <tr>
                    <td width="40%">
                        <h2>Message Details:</h2>

                    </td>
                </tr>
            </tbody>


            <tr>
                <td class="marginleft" align="right" height="26"><strong>Subject :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['subject']; ?>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26"><strong>Body :</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['body']; ?>
                    </div></td>
            </tr>


            </tbody>

        </table>
    </form>

    <?php
}

function fileShow($id) {

    global $db;
    $sql_query = "SELECT qu.id, qu.file_name, aq.filename, qa.filename  FROM " . _prefix("quotes") . " as qu "
            . "LEFT JOIN " . _prefix("admin_quotes") . " as aq ON qu.id = aq.quotes_id "
            . "LEFT JOIN " . _prefix("quote_attachment") . " as qa ON qa.quote_id = qu.id "
            . " where md5(qu.id) = '$id'";
    $res = $db->sql_query($sql_query);
    $num = $db->sql_numrows($res);
    $datas = $db->sql_fetchrowset($res);
    $quote_id = $datas[0]['id'];


    if (isset($_POST['submit']) && $_POST['submit'] == 'submit') {


        if ($_FILES['quoteFile']['error'] == 0) {

            $target_path = ADMIN_PATH . 'files/quote_files/';
            $date = new DateTime();
            $timestamp = $date->getTimestamp();
            $fileName = $timestamp . '_' . basename($_FILES['quoteFile']['name']);
            $target_path = $target_path . $fileName;
            move_uploaded_file($_FILES['quoteFile']["tmp_name"], $target_path);
            $_POST['filename'] = $fileName;
        }

        $field = array(
            'filename' => $_POST['filename'],
            'quote_id' => $quote_id
        );
        if ($_FILES['quoteFile']['name'] != '') {
            $insert_quotes = $db->insert(_prefix('quote_attachment'), $field);

            if ($insert_quotes) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
            }
        }
    }
    ?>

    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <table align="center" border="0" cellpadding="0" cellspacing="0"
           width="98%" style="margin:5px 5px 0px;">

        <tbody>
        </tbody>

    </table>

    <br>            <table align="center" border="0" cellpadding="0" cellspacing="0"
                           width="98%" style="margin:5px 5px 0px;">

        <tbody>
        <tbody>
            <tr>
                <td width="40%">
                    <h2>Files Details:</h2>

                </td>
            </tr>
        </tbody>


        <tr>

            <td class="marginleft"><div class="input text">
                    <?php
                    foreach ($datas as $records) {
                        foreach ($records as $val) {
                            $fileArray[] = $val;
                        }
                    }

                    $result = array_unique($fileArray);

                    if (count($result) > 0) {

                        foreach ($result as $key => $value) {

                            if (isset($value) && $value != '' && file_exists(DOCUMENT_PATH . 'admin/files/quote_files/' . $value)) {
                                ?>  <div class="col">
                                    <span class=""><a href="<?php echo HOME_PATH_URL ?>download.php?type=quote_files&file=<?php echo $value; ?>" class="getFile" id="<?php echo $value ?>"> <?php echo get_file_name($value); ?></a></span>

                                </div>
                                <?php
                            }
                        }
                    }
                    ?>
                </div></td>
        </tr>




    </tbody>

    </table>

    <?php
}

function paymentApproval() {
    global $db;
    $id = $_GET["id"];
    $query = "SELECT dc.*,plan.title as planTitle,pro.title as proTitle,plan.duration , user.email AS supplier ,CONCAT(IFNULL(user.first_name,''),' ',IFNULL(user.last_name,'')) AS supplierName  FROM " . _prefix("direct_credit") . " AS dc "
            . "  LEFT JOIN " . _prefix("users") . " AS user ON user.id=dc.supplier_id "
            . "  LEFT JOIN " . _prefix("membership_prices") . " AS plan ON plan.id=dc.plan_id "
            . "  LEFT JOIN " . _prefix("products") . " AS pro ON pro.id=dc.product_id "
            . " where dc.deleted=0 AND md5(dc.id)='$id'";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $data = $db->sql_fetchrow($res);
    // prd($data);
//    if ($data['plan_id'] == 2) {
//        $plan = '12 Months';
//    } else if ($data['plan_id'] == 3) {
//        $plan = '6 Months';
//    } else if ($data['plan_id'] == 4) {
//        $plan = '3 Months';
//    } else {
//        $plan = 'N/A';
//    } 
    $approve_status = ($data['approve_status'] == 1) ? 'Approved' : 'Disapproved';
    $plan = $data['planTitle'] . '&nbsp;' . $data['duration'] . ' Months';

    //prd($approve_status);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>

    <table align="center" border="0" cellpadding="0" cellspacing="0"
           width="98%" style="margin:5px 5px 0px;">

        <tbody>


            <tr>
                <td  colspan="2" width="100%">
                    <h3 style="color: #265190;text-align: center;">Requested Payment:</h3><hr style="width: 100%;">

                </td>
            </tr>


            <tr>
                <td class="marginleft" align="right" height="26"><strong> Supplier Name:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['supplierName'] != '' ? stripslashes($data['supplierName']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Facility Name:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['proTitle'] != '' ? stripslashes($data['proTitle']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong>Plan Type:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $plan; ?>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26"><strong>Amount:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['amount'] != '' ? stripslashes($data['amount']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Description:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['description'] != '' ? stripslashes($data['description']) : 'N/A'; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Approval Status:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $approve_status; ?>
                    </div></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"><strong> Created:</strong></td>
                <td class="marginleft"><div class="input text">
                        <?php echo $data['created'] != '' ? stripslashes($data['created']) : 'N/A'; ?>
                    </div></td>
            </tr>        
        </tbody>

    </table>

    <?php
}

function reviews($id) {
    global $db;
    $id = $_GET['id'];
    $query = "SELECT fdbk.* , pds.id as prId, pds.title as pTitle,pds.image as pImage, sp.user_name as spUsername,  sp.first_name AS spFName, sp.last_name AS spLName, cs.user_name AS csUsername,  cs.first_name AS csFName, cs.last_name AS csLName  FROM " . _prefix("feedbacks") . " AS fdbk   "
            . "LEFT JOIN " . _prefix("products") . " AS pds ON  fdbk.product_id= pds.id "
            . "LEFT JOIN " . _prefix("users") . " AS sp ON  fdbk.sp_id= sp.id "
            . "LEFT JOIN " . _prefix("users") . " AS cs ON  fdbk.cs_id= cs.id "
            . "WHERE  pds.title !='' AND md5(fdbk.id)='$id' AND  fdbk.deleted=0  ORDER BY fdbk.id DESC";
    $resArt = $db->sql_query($query);
    $count = $db->sql_numrows($resArt);
    $data = $db->sql_fetchrowset($resArt);
    //prd($data);
    $queryRating = "SELECT  `quality_of_care`, `caring_staff`, `responsive_management`, `trips_outdoor_activities`,"
            . " `indoor_entertainment`, `social_atmosphere`, `enjoyable_food`, `overall_rating` FROM " . _prefix("rating_score");
    $resRating = $db->sql_query($queryRating);
    $dataRating = $db->sql_fetchrowset($resRating);
    $QualityOfServiceWgt = $dataRating[0]['quality_of_care'];
    $cStaffWgt = $dataRating[0]['caring_staff'];
    $resManWgt = $dataRating[0]['responsive_management'];
    $tripWgt = $dataRating[0]['trips_outdoor_activities'];
    $indoorEntWgt = $dataRating[0]['indoor_entertainment'];
    $socialWgt = $dataRating[0]['social_atmosphere'];
    $foodWgt = $dataRating[0]['enjoyable_food'];
    $overAllRatingWgt = $dataRating[0]['overall_rating'];
    $TotalRating = 0;
    $weightSum = $QualityOfServiceWgt + $cStaffWgt + $resManWgt + $tripWgt + $indoorEntWgt + $socialWgt + $foodWgt + $overAllRatingWgt;
    $reviews = '';
    $qryKnowMe = "select id, title from " . _prefix('know_me') . ' where  status=1 AND deleted=0';
    $resultKnowMe = $db->sql_query($qryKnowMe);
    $knowMeData = $db->sql_fetchrowset($resultKnowMe);
    foreach ($knowMeData as $key => $record) {
        $knowMe[] = $record['title'];
    }
    $qryDuration = "select id, title from " . _prefix('durations') . ' where  status=1 AND deleted=0';
    $resultDuration = $db->sql_query($qryDuration);
    $durationVisitData = $db->sql_fetchrowset($resultDuration);
    foreach ($durationVisitData as $key => $record) {
        $durationVisit[] = $record['title'];
    }
    $lastVisit = array('Within the last 12 months', '1 - 3 years ago', 'More than 3 years ago');
    $recommended = array('No', 'Yes', 'Not sure');
    if ($count > 0) {
        foreach ($data as $key => $record) {
            $supplierName = (empty($record['spFName'])) ? 'N/A' : $record['spFName'] . '&nbsp;' . $record['spLName'];
            $title = (empty($record['title'])) ? 'N/A' : $record['title'];
            $feedback = (strlen($record['feedback']) > 20) ? substr($record['feedback'], 0, 20) . '...' : $record['feedback'];
            $pros = (strlen($record['pros']) > 20) ? substr($record['pros'], 0, 200) . '...' : $record['pros'];
            $reviews = '<div class="col-md-12 text-center" style="color: #265190;"><h2>Reviews</h2><hr></div>';


            $reviews.='<div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_none"><strong style="font-size:15px;">Review title</strong></div> 
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_left">' . ((empty($record['title'])) ? 'N/A' : $record['title']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_none"><strong style="font-size:15px;">Product/Services Name</strong></div> 
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_left">' . ((empty($record['pTitle'])) ? 'N/A' : $record['pTitle']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_none"><strong style="font-size:15px;">Supplier Name</strong></div> 
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_left">' . ((empty($supplierName)) ? 'N/A' : $supplierName) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_none"><strong style="font-size:15px;">Pros</strong></div> 
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_left">' . ((empty($record['pros'])) ? 'N/A' : $record['pros']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_none"><strong style="font-size:15px;">Cons</strong></div> 
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_left">' . ((empty($record['cons'])) ? 'N/A' : $record['cons']) . '</div> 
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_none pull-left" style=" margin-bottom: 10px;"><strong style="font-size:15px">Suggestions </strong></div> 
                                        <div class="col-md-6 col-sm-6 col-xs-12 padding_none">' . $record['feedback'] . '</div> 
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left scroll" style="width:100%; margin-bottom:10px;">
                                        <table class="table table-bordered table_bord">                   
                                            <thead>
                                                <tr>
                                                    <th width="" class="th_text col-md-3">Categories</th>
                                                    <th width="" class="th_text col-md-3">Rating</th>
                                                    <th width="" class="th_text col-md-3 ">Categories</th>
                                                    <th width="" class="th_text col-md-3">Rating</th>
                                                </tr>
                                            </thead> 
                                            <tbody>
                                                <tr>
                                                    <td>Quality of Care</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['quality_of_care']) . '</div>
                                                    </td>
                                                    <td>Caring/Helpful Staff</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['caring_staff']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Responsive Management</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['responsive_management']) . '</div>
                                                    </td>
                                                    <td>Trips/Outdoor Activities</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['trips_outdoor_activities']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Indoor Entertainment</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['indoor_entertainment']) . '</div>
                                                    </td>
                                                    <td>Social Atmosphere</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['social_atmosphere']) . '</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Enjoyable Food</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['enjoyable_food']) . '</div>
                                                    </td>
                                                    <td>OVERALL RATING</td>
                                                    <td>
                                                        <div class"text-center">' . getStars($record['overall_rating']) . '</div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>     
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin-bottom:10px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><h4>How do you know about this place?</h4></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><strong>' . ((empty($record['know_me'])) ? 'N/A' : $knowMe[$record['know_me']]) . '</strong></div> 
                                    </div> 

                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><h4>I have lived at / visited this facility for</h4></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><strong>' . ((empty($record['visit_duration'])) ? 'N/A' : $durationVisit[$record['visit_duration']]) . '</strong></div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><h4>I last lived at / visited this facility</h4></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none"><strong>' . ((empty($record['last_visit'])) ? 'N/A' : $lastVisit[$record['last_visit']]) . '</strong></div> 
                                    </div>                    
                                    <div class="col-md-12 col-sm-12 col-xs-12 padding_none pull-left" style="width:100%; margin:10px 0px;">
                                        <div class="col-md-12 col-sm-12 col-xs-12 pull-left padding_none" style=" margin-bottom: 10px;"><h4>Would you recommend this facility to a friend?</h4></div> 
                                        <div class="col-md-12 col-sm-12 col-xs-12 padding_none" style=" margin-bottom: 20px;"><strong>' . ((empty($record['recommended'])) ? 'N/A' : $recommended[$record['recommended']]) . '</strong></div> 
                                    </div>                    
                                </div>';
            $reviews.= '</div><div class="col-md-6"></div></div>';
            $reviews .='</div>
                    </div>';
        }
    } else {
        $reviews = '<tdiv class="col-md-12 text-center text-danger>No record Found</div>';
    }
    echo $reviews;
    ?>
    <style>
        .star{
            color:red;
        }
    </style>
    <?php
}
?>


