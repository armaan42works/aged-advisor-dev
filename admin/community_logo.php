<?php /*
     * Objective  : Used for community logo links
     * Filename :   community_logo.php
     
     * Created On : 15-1-16
     */
 
    include("../application_top.php");
    include(INCLUDE_PATH . "header.php");
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="lt_nav" height="455" valign="top" width="17%">
                <!-- left navigation -->
                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                <!-- left navigation ends here -->
            </td>
            <td valign="top">
                <!-- content area here -->
                <div id="body_content">
                    <?php
                        switch (key($_REQUEST)) {
                            case 'add':
                                include(MODULE_PATH . "community_logo/add.php");
                                break;
                            case 'edit' :
                                include(MODULE_PATH . "community_logo/add.php");
                                break;
                            case 'manage':
                                include(MODULE_PATH . "community_logo/manage.php");
                                break;
                            default:
                                include(MODULE_PATH . "community_logo/manage.php");
                                break;
                        }
                    ?>
                </div>
                <!-- content area ends here -->
            </td>
        </tr>
    </tbody>
</table>

