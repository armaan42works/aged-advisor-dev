<?php

    /*
     * Objective : Create Captcha
     * Filename : captcha.php
     * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
     * Created On : 13 August 2014
     */
    session_start();

    $string = '';
    for ($i = 0; $i < 4; $i++) {
        $string .= chr(rand(97, 122));
    }

    $_SESSION['captcha'] = $string; //store the captcha

    $dir = '../../font/';
    $image = imagecreatetruecolor(165, 50); //custom image size
    $font = "YoungDantes_by_laura_kristen.ttf"; // custom font style
    $color = imagecolorallocate($image, 113, 193, 217); // custom color
    $white = imagecolorallocate($image, 0, 0, 255); // custom background color
    imagefilledrectangle($image, 0, 0, 399, 99, $white);
    imagettftext($image, 30, 0, 10, 40, $color, $dir . $font, $_SESSION['captcha']);

    header("Content-type: image/png");
    imagepng($image);
?>