<?php 
/**************Admin registration Message Page***************************
 * this is used for edit registration message for show in front end when user is registered
 *****************************************/
if($_POST['message']){
		$id			=	$_POST['msg_id'];
		$message	=	$_POST['message'];
		$check_duplicate=	mysqli_num_rows(mysqli_query($db->db_connect_id,"select *from admin_register_msg"));
		if(!empty($check_duplicate)){
			$result			= 	mysqli_query($db->db_connect_id,"update admin_register_msg set message='{$message}' where id='{$id}'")or die("File ".__FILE__.mysqli_errno($db->db_connect_id));
			$msg			=	"Register Message has been successfully update";
		}
		else{
			$msg			=	"Register Message already added,Please edit!";
		}	
}
$result_data	=	mysqli_query($db->db_connect_id,"select *from admin_register_msg") or die("File ".__FILE__.mysqli_errno($db->db_connect_id));
$result			=	mysqli_fetch_array($result_data);
?>
<form name="reg" action="" method="post" onsubmit="return reg_msg();">
<fieldset style="display:none;">
<input type="hidden" name="msg_id" value="<?php echo $result['id'];?>">
</fieldset>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
	
	<tbody>
		<tr>
			<td height="10">&nbsp;</td>
		</tr>

		<tr>
			<td>
			<div class="breadcrumb"><a href="<?php echo MAIN_PATH;?>/admin.php">Home </a>>><a href="<?php echo HOME_PATH_URL."admin.php"?>">Manage Register Message</a>>><b>Edit</b></div>
			</td>
		</tr>
		<tr>

			<td align="left">&nbsp;</td>
		</tr>
		<tr>
			<td bgcolor="#dedede" height="1"><img src="<?php echo ADMIN_IMAGE;?>sp.gif"
				height="1" width="1"></td>
		</tr>
		<tr>
			<td class="sublinks" height="40" valign="bottom"></td>
		</tr>
		<tr>

			<td height="1"><img src="<?php echo ADMIN_IMAGE;?>sp.gif" height="1" width="1"></td>
		</tr>
		<?php if(isset($msg)&&!empty($msg)){?>
  		<tr><td><div style="color: rgb(255, 0, 0);"><?php echo $msg;?></div></td></tr>
  		<?php }?>
		
		<tr>
			<td class="bdrTab">
			<table style="border: 1px solid rgb(184, 184, 184);" border="0"
				cellpadding="5" cellspacing="0" width="100%">
				<tbody>
					<tr>
						<td>
						<table align="center" border="0" cellpadding="0" cellspacing="0"
							width="98%">

							<tbody>
								<tr>
									<td colspan="2" class="bdr" height="25">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">

										<tbody>
											<tr>
												<td width="27%">
												<h2>Edit Register Message</h2>

												</td>
												<td class="fs10" align="right" width="73%"><span
													class="redCol">* fields required</span> &nbsp;&nbsp;</td>
											</tr>
										</tbody>
									</table>
									</td>
								</tr>
								<tr>

									<td class="marginleft r1" align="right" height="26" width="31%"><span
										class="redCol">* </span>Register Message:</td>
									<td class="r1 marginleft" width="69%">
									<div class="input text">
									<?php 
										$FCKeditor = new FCKeditor('message');
										$FCKeditor->BasePath = MAIN_PATH.'/fckeditor/';
										$FCKeditor->Value 	 =$result['message'];
										$FCKeditor->Width="500";
										$FCKeditor->Height="500";
										$FCKeditor->Create();
										?>
									</div></td>
								</tr>
								
								<tr>
									<td class="marginleft r" align="right" height="26" width="31%"></td>
									<td class="r marginleft" width="69%">
									
									<input type="image" src="<?php echo ADMIN_IMAGE;?>submit.gif" name="ads"></td>
								</tr>
							
						<!-- content area ends here --></td>
					</tr>
				</tbody>

			</table>
			</td>
		</tr>
</tbody></table></td></tr></tbody></table></form>



