<script>
function change_page(val,page,qry_st)
{
window.location = page+'?pagenum='+val+qry_st;
 } 
 </script>
<?php 
       

      // for pagination of
	 //  two types ,nomal link and dropdown menu
	// for normal links pass 'links' as the third parameter while creating the object and for dropdown pagination  pass 'dropdown'.
class pagination
{
 var $max_results_per_page=0; // no. of results to be shown per page
 var $total_results=0;        //no. of all  results to be shown over all pages 
 var $max_pages =0;          // max. pages i.e the maximum number of pages for a particular query , calculated in the constructor
 var $current_page=0 ;      // the page no. that is currently rendered  
 var $pagination_string='';  // the output string , which contains , the actual pagination  links/dropdown 
 var $page_x_of_y = '';     // the output string , which contains  the string like 'page 1 of 2' 
 var $qry_string = '';     // the query string with the address ,useful when the pagination is done on pages which has search feature as the GET parameters are not lost

 function __construct($total_res,$res_per_page,$pagination_type)
	{
     $this->max_results_per_page = $res_per_page;
	 
	 $this->total_results = $total_res;
	 
	 $this->max_pages = ceil($total_res/$res_per_page);
	 
	 $query_string = '';
			if(is_array($_GET))
             {
			 foreach($_GET as $key=>$value)
			  {
			  if($key!='pagenum')
			     $query_string .= $key;   
			  }
			 }
	$this->qry_string = $query_string;		 
	
		if(isset($_REQUEST['pagenum'])) 
				{
					if($_REQUEST['pagenum']>$this->max_pages)
						{
							$this->current_page = $this->max_pages;
						}
					else if($_REQUEST['pagenum']<1)
						{
							$this->current_page = 1;
						}
					else
						{
							$this->current_page = $_REQUEST['pagenum'];
						}
				}
		else
			{
				$this->current_page = 1;
			}
			$this->page_x_of_y = 'page '.$this->current_page.' of '.$this->max_pages;
			switch($pagination_type)
			{
			case 'links':
						$this->page_links();
                         break;			
			case 'dropdown':
                        $this->page_dropdown();
						break;
			default : die('Sorry no such pagination style as "'.$pagination_type.'"  exist');			
			}
	}
public function page_links()
        {		
	if ($this->current_page > 1)
		{
			$page  = $this->current_page-1;
			

			// for image arrows
         // use this		
		//	$prev  = "<a href='".$_SERVER['PHP_SELF']."?pagenum=$page".$qry_string."'><img src='../images/reverse.gif' width='10px' height='10px'></a> ";
		
		//if you dont have images use this
		
		//$prev  = "<a href='".$_SERVER['PHP_SELF']."?pagenum=$page".$this->qry_string."'>&lt;&lt;</a> ";
		$prev  = "<a href='".$_SERVER['PHP_SELF']."?".$this->qry_string."&pagenum=".$page."'>&lt;&lt;</a> ";
		}
	else
		{
			$prev  = '&nbsp;'; // we're on page one, don't print previous link
			$first = '&nbsp;'; // nor the first page link
		}

	if ($this->current_page < $this->max_pages  )
		{
			$page = $this->current_page + 1;
		// for image arrows
         // use this		
		//	$next = "<a href='".$_SERVER['PHP_SELF']."?pagenum=".$page."'><img src='../images/forward.gif' width='10px' height='10px'></a> ";
		
		//if you dont have images use this
		
	//$next = "<a href='".$_SERVER['PHP_SELF']."?pagenum=".$page.$this->qry_string."'>&gt;&gt;</a> ";
	$next = "<a href='".$_SERVER['PHP_SELF']."?".$this->qry_string."&pagenum=".$page."'>&gt;&gt;</a> ";
		}
	else
		{
			$next = '&nbsp;'; // we're on the last page, don't print next link
			$last = '&nbsp;'; // nor the last page link
		}
		
		for($page = 1; $page <= $this->max_pages; $page++)
		{    
			if ($page == $this->current_page)
				{
					$nav .= " $page "; // no need to create a link to current page
				}
			else
				{  
					if($page>=$this->current_page-5 && $page<=$this->current_page+5)  
						//$nav .= "<a href='".$_SERVER['PHP_SELF']."?pagenum=$page"."$this->qry_string'>".$page."</a> ";
						$nav .= "<a href='".$_SERVER['PHP_SELF']."?".$this->qry_string."&pagenum=".$page."'>$page</a> ";
				}
   		}
				$this->pagination_string = $prev.$nav.$next;
		  
		}	
	public function page_dropdown()
        { $nav = "<select name='pagenum' onChange=change_page(this.value,'".$_SERVER['PHP_SELF']."','".$this->qry_string."')>";	
		for($page = 1; $page <= $this->max_pages; $page++)
		{    
		   
			if ($page == $this->current_page)
				{
						$nav .= "<option value='".$page."' selected>".$page."</option> ";
				}
				else
				{
						$nav .= "<option value='".$page."' >".$page."</option> ";
				}
   		}
		$nav .= "</select>";	
		$this->pagination_string = $nav;		
		}	
	public function get_pagination_query($qry)
		{
		$offset = ($this->current_page - 1) * $this->max_results_per_page;
        $max=" LIMIT $offset, $this->max_results_per_page";
		$new_query = $qry."".$max;
		return $new_query;
		}
		  
}?>
