<?php
include("../application_top.php");
include(INCLUDE_PATH . "header.php");
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="lt_nav" height="455" valign="top" width="17%">
                <!-- left navigation -->
                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                <!-- left navigation ends here -->
            </td>
            <td valign="top">
                <!-- content area here -->
                <div id="body_content">
                    <?php
                    switch (key($_REQUEST)) {
                        case 'about_us':
                            include(MODULE_PATH . "cms/about_us.php");
                            break;
                        case 'awards':
                            include(MODULE_PATH . "cms/awards.php");
                            break;
                        case 'contact_us':
                            include(MODULE_PATH . "cms/contact_us.php");
                            break;
                        case 'Complaint':
                            include(MODULE_PATH . "cms/complaint.php");
                            break;
                        case 'sponsors_advertising':
                            include(MODULE_PATH . "cms/sponsors_advertising.php");
                            break;
                        case 'term_con':
                            include(MODULE_PATH . "cms/term_con.php");
                            break;
                        case 'manage_FAQ':
                            include(MODULE_PATH . "cms/manage_faq.php");
                            break;
                        case 'add_Faq':
                            include(MODULE_PATH . "cms/add_faq.php");
                            break;
                        case 'edit_Faq' :
                                include(MODULE_PATH . "cms/add_faq.php");
                                break;
                        default:
                            include(MODULE_PATH . "ads/manage.php");
                            break;
                    }
                    ?>
                </div>
                <!-- content area ends here -->
            </td>
        </tr>
    </tbody>
</table>

