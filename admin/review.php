<?php
    include("../application_top.php");
    include(INCLUDE_PATH . "header.php");
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="lt_nav" height="455" valign="top" width="17%">
                <!-- left navigation -->
                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                <!-- left navigation ends here -->
            </td>
            <td valign="top">
                <!-- content area here -->
                <div id="body_content">
                    <?php
                        switch (key($_REQUEST)) {
                            case 'add':
                                include(MODULE_PATH . "review/add.php");
                                break;
                            case "review" :
                                include(MODULE_PATH . "review/manage.php");
                                break;
                            case "abused_review" :
                                include(MODULE_PATH . "review/abusedReview.php");
                                break;
                            case "edit" :
                                include(MODULE_PATH . "review/add.php");
                                break;
                            case "edit_abuse" :
                                include(MODULE_PATH . "review/edit_abusive_review.php");
                                break;
                            default:
                                include(MODULE_PATH . "review/manage.php");
                                break;
                        }
                    ?>
                </div>
                <!-- content area ends here -->
            </td>
        </tr>
    </tbody>
</table>

