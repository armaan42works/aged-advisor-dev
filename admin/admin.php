<?php
    /*     * **************ADMIN HOME PAGE*****************************
     * ********************************************* */
    include("../application_top.php");
    include(INCLUDE_PATH . "header.php");
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="lt_nav" height="455" valign="top" width="17%">
                <!-- left navigation -->
                <!--echo INCLUDE_PATH; -->
                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                
                <!-- left navigation ends here -->
            </td>
            <td valign="top">
                <!-- content area here -->
                <div id="body_content">
                    <?php
                        if (isset($_SESSION['Admin']) && !empty($_SESSION['Admin'])) {
                            switch (key($_REQUEST)) {
                                case 'change_password':
                                    include_once ACCOUNT_PATH . 'change_password.php';
                                    break;
                                case 'manage_address':
                                    include_once ACCOUNT_PATH . 'address.php';
                                    break;
                                case 'change_email':
                                    include_once ACCOUNT_PATH . 'email.php';
                                    break;
                                default:
                                    include_once ACCOUNT_PATH . 'home.php';

                                    break;
                            }
                        } else {
                            $main_obj->redirect("index.php");
                        }
                    ?>
                </div>
                <!-- content area ends here -->
            </td>
        </tr>
    </tbody>
</table>
