<?php
    /*     * **************ADMIN HOME PAGE*****************************
      MODULE INCLUDED
      1.MANAGE OFFER
      2.MANAGE DEMAND
      3.CHANGE PASSWORD
      4.LOGOUT
     * ********************************************* */
?>
<?php
    include("../application_top.php");
//require_once('../include/methods.php');  
    require("../phpmailer/class.phpmailer.php");
    require_once("fckeditor_php5.php");
?>
<?php //print_r($_REQUEST);                   ?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <link rel="stylesheet" type="text/css" href="css/calendar.css">
        <script src="../js/validations.js" type="text/javascript"></script>
        <script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>
        <script src="js/jquery-migrate-1.1.1.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
        <script src="js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
        <script src="js/jquery.validate.js" type="text/javascript"></script>
        <script src="js/jquery.colorbox-min.js" type="text/javascript"></script>
        <script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>

        <title>:::Administration:::</title>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <meta content="MShtml 6.00.2600.0" name="GENERATOR">
    </head>
    <body>
        <table border="1" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
                <tr>
                    <td><!-- header -->
                        <?php include(INCLUDE_PATH . "header.php"); ?>
                        <!-- header ends here -->
                    </td>
                </tr>
                <tr>
                    <td>

                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td class="lt_nav" height="455" valign="top" width="17%">
                                        <!-- left navigation -->
                                        <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                                        <!-- left navigation ends here -->
                                    </td>
                                    <td valign="top">
                                        <!-- content area here -->
                                        <div id="body_content">
                                            <?php if (isset($_SESSION['username']) && !empty($_SESSION['username'])) {
                                                       echo "Welcome Admin";
                                                } else {
                                                    $main_obj->redirect("index.php");
                                                }
                                            ?>
                                        </div>
                                        <!-- content area ends here -->
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <!-- footer -->
                    <td align="center" bgcolor="#ebeaeb" height="20"></td>
                    <!-- footer ends here -->
                </tr>
            </tbody>
        </table>
    </body>
</html>
