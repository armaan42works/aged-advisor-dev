
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        changePagination(page, li, 'service');
    });
</script>
<div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
    <?php
    if (isset($error)) {
        echo '<div style="color:#FF0000">' . $errors . '</div>';
    }
    if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    ?>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>> Manage Services Categories</li>
                        <li class="pull-right "><div id="link"><a href="<?php echo HOME_PATH_URL; ?>service.php?add">Add/New</a></div></li>
                    </ul>
                </div>
            </td>
        </tr>
    <tbody>
        <tr>
            <td>
                <table class="table table-striped table-hover">
                    <tbody  id="pageData">
                        <?php // echo '<div id="pageData"></div>'; ?>
                    </tbody>

                </table>
            </td>
        </tr>
    </tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

