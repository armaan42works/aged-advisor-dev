<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
global $db;
$description = '';


if (isset($submit) && $submit == 'Submit') {

    $fields_array = services();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $checkQ = "SELECT id from " . _prefix("services") . " WHERE name='" . $name . "' AND plan_type =" . $_POST['plan_type'];
        $checkC = $db->sql_query($checkQ);
        $check = mysqli_num_rows($checkC);
//        prd($check);
        if ($check == 0) {
            $getMax = "SELECT max(order_by) as count from " . _prefix("services") . " WHERE status = 1 && deleted = 0";
            $resMax = $db->sql_query($getMax);
            $dataMax = mysqli_fetch_assoc($resMax);
            $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
            $fields = array(
                'name' => trim($name),
                'description' => trim($_POST['description']),
                'order_by' => $order
            );


            $insert_result = $db->insert(_prefix('services'), $fields);
            if ($insert_result) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/service.php?manage");
            }
        } else {
            $error = true;
            $errors = 'Duplicate title entry.';
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (isset($update) && $update == 'Update') {

    $fields_array = services();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $checkQ = "SELECT id from " . _prefix("services") . " WHERE name='" . $name . "' AND plan_type =" . $_POST['plan_type'] . " AND  md5(id)<>'" . $id . "'";
        $checkC = $db->sql_query($checkQ);
        $check = mysqli_num_rows($checkC);
        if ($check == 0) {
            $name = $_POST["name"];
            $description = $_POST["description"];
//            $plan_type = $_POST["plan_type"];
            $fields = array('name' => trim($name),
                'description' => trim($description)
            );

            $where = "where md5(id)= '$id'";
            $update_result = $db->update(_prefix('services'), $fields, $where);
            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/service.php?manage");
            }
        } else {
            $error = true;
            $errors = 'Duplicate title entry.';
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("services") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {

            $name = $record['name'];
            $description = $record['description'];
//            $plan_type = $record['plan_type'];
        }
    }
}
?>
<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "service.php?manage" ?>">Manage Services Categories</a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Services Categories </h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form method="POST" name="form1" id="addPlans" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="form-group">
            <label for="name" class="col-sm-2 control-label">Name<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="name" id="name" minlength="2" maxlength="50" value="<?php echo isset($name) ? stripslashes($name) : ''; ?>"/>
            </div>
        </div>
<!--        <div class="form-group">
            <label for="plan_type" class="col-sm-2 control-label">Plan Type<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <?php // $select_type = array("0" => "Basic", "1" => "Business", "2" => "Premium"); ?>
                <select name="plan_type" id='plan_type' class="required form-control" style="width:235px;">
                    <?php
//                    $plan_type = $plan_type != '' ? $plan_type : $_POST['plan_type'];
//                    echo options($select_type, $plan_type);
                    ?>
                </select>
            </div>
        </div>-->
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea rows="4" cols="50" name="description" maxlength="500" class="required form-control" id="description"><?php
                    if (isset($description) && (!empty($description))) {
                        echo stripslashes($description);
                    }
                    ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                if (key($_REQUEST) == 'edit') {
                    ?>
                    <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                    <?php
                } else {
                    ?>
                    <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                    <?php
                }
                ?>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#addPlans').validate();
</script>
