<?php
    /*
     * Objective : Add Customer details on admin Panel
     * Filename : add.php
     * Created By : Vinod Kumar singh<vinod.kumar@ilmp-tech.com>
     * Created On : 2 September  2014
     * Modified : 2 September 2014
     */
?>
<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
    global $db;
    $records = array();
    $disable = '';
    if (isset($submit) && $submit == 'Submit') {
        $fields_array = user();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            $fields = array('user_type' => $user_type,
                'name' => trim($name),
                'business' => trim($business),
                'industry' => trim($industry),
                'website' => trim($website),
                'address' => trim($address),
                'country_id' => $country_id,
                'state_id' => $state_id,
                'zipcode' => $zipcode,
                'phone' => $phone,
                'cellphone' => $cellphone,
                'email' => $email,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'validate' => '0');
            $insert_result = $db->insert(_prefix('users'), $fields);
            if ($insert_result) {
                $record_id = mysqli_insert_id($db->db_connect_id);
                VerifyUser($record_id);
                if ($user_type == 0) {
                    // Message for insert
                    $msg = common_message(1, constant('INSERT'));
                    $_SESSION['msg'] = $msg;
                    //ends here           
                    redirect_to(MAIN_PATH . "/user.php?customer");
                } else {
                    $msg = common_message(1, constant('INSERT'));
                    $_SESSION['msg'] = $msg;
                    //ends here           
                    redirect_to(MAIN_PATH . "/user.php?supplier");
                }

                foreach ($_POST as $key => $value) {
                    $$key = '';
                }
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }

    if (isset($update) && $update == 'Update') {
        $fields_array = user();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            $fields = array('user_type' => $user_type,
                'name' => trim($name),
                'business' => trim($business),
                'industry' => trim($industry),
                'website' => trim($website),
                'address' => trim($address),
                'country_id' => $country_id,
                'state_id' => $state_id,
                'zipcode' => $zipcode,
                'phone' => $phone,
                'cellphone' => $cellphone,
                'ip_address' => $_SERVER['REMOTE_ADDR']
            );
            $where = "where md5(id)= '$id'";
            $update_result = $db->update(_prefix('users'), $fields, $where);

            if ($update_result) {
                if ($user_type == 0) {
                    // Message for insert
                    $msg = common_message(1, constant('UPDATE'));
                    $_SESSION['msg'] = $msg;
                    //ends here                     
                    redirect_to(MAIN_PATH . "/user.php?customer");
                } else {
                    // Message for insert
                    $msg = common_message(1, constant('UPDATE'));
                    $_SESSION['msg'] = $msg;
                    //ends here 
                    redirect_to(MAIN_PATH . "/user.php?supplier");
                }
                $msg = common_message(1, constant('UPDATE'));
                foreach ($_POST as $key => $value) {
                    $$key = '';
                }
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }

    if (key($_REQUEST) == 'edit') {
        $sql_query = "SELECT * FROM " . _prefix("users") . " WHERE md5(id)='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrow($res);
        $userTypeFrmDb = $records['user_type'];
        $editAct = 1;
        $disable = "disabled = 'disabled'";
    }
?>  
<form name="AddClient" id="AddClient" action="" method="post">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>
                                <a href="<?php echo ($userTypeFrmDb==0)?HOME_PATH_URL . "user.php?customer":HOME_PATH_URL . "user.php?supplier"; ?>">
                                    <?php echo ($userTypeFrmDb==0)?'Manage Customer':'Supplier';?></a> >> 
                                    <?php echo ($editAct == 1)?'Edit':'Add';?>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
            <tr><td>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <td colspan="2" class="bdr" height="25">
                                                    <h2>Add User</h2> <span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26" width="31%">User Type: <span class="redCol">* </span></td>
                                                <td class="marginleft" width="69%">
                                                    <div class="input text">
                                                        <?php
                                                            foreach ($clientType as $key => $value) {
                                                                $check = '';
                                                                $userType = array_key_exists('user_type', $records) ? $records['user_type'] : '';
                                                                if ($key == $userType) {
                                                                    $check = "checked = 'checked'";
                                                                } else if (!array_key_exists('user_type', $records) && $key == 0) {
                                                                    $check = "checked = 'checked'";
                                                                }
                                                                ?>
                                                                <input type="radio" id="<?php echo $key; ?>" name="user_type" class="user_type" value="<?php echo $key; ?>" <?php echo $check; ?> <?php echo $disable ?>><label for="<?php echo $key; ?>"><?php echo $value; ?></label>
                                                                <?php
                                                            }
                                                        ?></div></td>
                                            </tr>
                                            <?php
                                                if (key($_REQUEST) == 'edit') {
                                                    ?>
                                                    <tr>
                                                        <td class="marginleft" align="right" height="26">Email Address:</td>
                                                        <td class="marginleft"><div class="input text">
                                                                <?php echo isset($records['email']) ? $records['email'] : '' ?>
                                                            </div></td>
                                                    </tr>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td class="marginleft" align="right" height="26">Email Address: <span
                                                                class="redCol">* </span></td>
                                                        <td class="marginleft"><div class="input text">
                                                                <input type="text" name="email" id="email" maxlength="50" size="30" class="registrationTextbox" value="<?php echo isset($records['email']) ? $records['email'] : '' ?>"/>
                                                            </div></td>
                                                    </tr>
                                                    <?php
                                                }
                                            ?>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Name: <span class="redCol">* </span></td>
                                                <td class="marginleft">
                                                    <div class="input text">
                                                        <input type="text" name="name" id="name" minlength="2" maxlength="50" size="30" class="registrationTextbox required" value="<?php echo isset($records['name']) ? stripslashes($records['name']) : ''; ?>"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="business">

                                                <td class="marginleft" align="right" height="26" width="31%">Business Name: <span class="redCol">* </span></td>
                                                <td class="marginleft" width="69%">
                                                    <div class="input text">
                                                        <input type="text" name="business" id="business" minlength="2" maxlength="50" size="30" value="<?php echo isset($records['business']) ? stripslashes($records['business']) : ''; ?>" class="registrationTextbox required"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Industry:</td>
                                                <td class="marginleft">
                                                    <div class="input text">
                                                        <input type="text" name="industry" id="industry" minlength="2" maxlength="50" size="30" class="registrationTextbox" value="<?php echo isset($records['industry']) ? stripslashes($records['industry']) : ''; ?>"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Website:</td>
                                                <td class="marginleft">
                                                    <div class="input text">
                                                        <input type="text" name="website" id="website" maxlength="50" size="30" class="registrationTextbox" value="<?php echo isset($records['website']) ? stripslashes($records['website']) : ''; ?>"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Street Address: <span class="redCol">* </span></td>
                                                <td class="marginleft"><div class="input text">
                                                        <textarea name="address" id="address" rows="4" cols="29" minlength="2" maxlength="500" class="registrationTextbox required"><?php echo isset($records['address']) ? stripslashes($records['address']) : '' ?></textarea>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Country: <span class="redCol">* </span></td>
                                                <td class="marginleft"><div class="input text">
                                                        <select id="country_id" class="required country" name="country_id" style="width:235px;"><?php
                                                                echo countrylist($records['country_id']);
                                                            ?></select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">State: <span class="redCol">* </span></td>
                                                <td class="marginleft">
                                                    <div class="input text">
                                                        <select id="state_id" class="required" name="state_id" class="state" style="width:235px;">
                                                            <?php echo stateList($records['country_id'], $records['state_id']); ?>
                                                        </select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Zip Code: <span class="redCol">* </span></td>
                                                <td class="marginleft">
                                                    <div class="input text">
                                                        <input type="text" name="zipcode" id="zipcode"  maxlength="10" size="30" class="registrationTextbox required" value="<?php echo isset($records['zipcode']) ? $records['zipcode'] : ''; ?>"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Best Phone Number: <span class="redCol">* </span></td>
                                                <td class="marginleft">
                                                    <div class="input text">
                                                        <input type="text" name="phone" id="phone"  minlength="2" maxlength="20" size="30" value="<?php echo isset($records['phone']) ? $records['phone'] : ''; ?>" class="registrationTextbox phoneUS required"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26"> <span class="redCol"></span>Cell Phone:</td>
                                                <td class="marginleft">
                                                    <div class="input text">
                                                        <input type="text" name="cellphone" id="cellphone" minlength="2" maxlength="20" size="30" value="<?php echo isset($records['cellphone']) ? $records['cellphone'] : ''; ?>" class="registrationTextbox  phoneUS"/>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft r" align="right" height="26" width="31%"></td>
                                                <td class="r marginleft" width="69%">
                                                    <?php if (key($_REQUEST) == 'add') { ?>
                                                            <input type="submit" value="Submit" name="submit" class="submit_btn">
                                                        <?php } else { ?>
                                                            <input type="submit" value="Update" name="update" class="submit_btn">
                                                        <?php } ?>
                                                </td> 
                                            </tr>
                                            <!-- content area ends here -->
                                            <!--</td>-->
                                            <!--</tr>-->
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</form>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr>
        <td id="ds_calclass"></td>
    </tr>
</table>
<script type="text/javascript">
    $(document).ready(function() {
$('input[type="submit"]').focus();
<?php if ($userType == 1) { ?>
                $('.business').hide();
                $('#business').removeClass('required');
    <?php } ?>
        $('.user_type').click(function() {
            var userType = $(this).val();
            if (userType == 0) {
                $('.business').show();
                $('#business').addClass('required');
            } else {
                $('.business').hide();
                $('#business').removeClass('required');
                $('#business').val('');
            }
        });
        $('#AddClient').validate({
            rules: {
                "email": {"required": true,
                    "email": true,
                    "remote": {
                        url: 'ajax.php',
                        type: 'POST',
                        data: {action: "unquieEmail", email: function() {
                                return $('#email').val();
                            }
                        }
                    }
                }
            },
            messages: {
                email: {
                    "remote": "Email ID aleady registered."
                }
            }
        });

        if ($('#country_id').val() == 229) {
            $('input[name="zipcode"]').rules('remove');
            $('input[name="zipcode"]').rules('add', {
                digits: true,
                required: true,
                maxlength: 5
            });
        }
    });
</script>