<?php
global $db;
$id = $_GET['id'];

$sql_query = "SELECT pr.*,pr.id as pr_id, extinfo.*,extinfo.id as ex_id,sr.name as facility_name FROM " . _prefix("products") . " AS pr "
        . " Left join " . _prefix("services") . " AS sr ON pr.facility_type=sr.id"
        . " Left join " . _prefix("extra_facility") . " AS extinfo ON extinfo.product_id =pr.id"
        . " where md5(pr.id)='$id'";
$res = $db->sql_query($sql_query);
$records = $db->sql_fetchrowset($res);
//prd($sql_query);
//Service ids for selected product
$sql_service = "select service_id from " . _prefix("pro_services") . " where product_id=" . $records[0]['pr_id'];
$res_service = $db->sql_query($sql_service);
$records_service = $db->sql_fetchrowset($res_service);
foreach ($records_service as $ids) {
    $service_ids[] = $ids['service_id'];
}


//prd($service_ids);
if (count($records)) {
    foreach ($records as $record) {
        // prd($record);
        $ex_id = $record['ex_id'];
        $pr_id = $record['pr_id'];
        $supplier_id = $record['supplier_id'];
        $title = $record['title'];
        $description = $record['description'];
        $keyword = $record['keyword'];
        $image = $record['image'];
        $facility = $record['facility_type'];
        $city = $record['city_id'];
        $suburb = $record['suburb_id'];
        $restcare = $record['restcare_id'];
        $ext_url = $record['ext_url'];
        $youtube_video = $record['youtube_video'];
        $zip = $record['zip'];
        $staff_comment = $record['staff_comment'];
        $management_comment = $record['management_comment'];
        $activities_comment = $record['activity_comment'];
        $staff_image = $record['staff_image'];
        $management_image = $record['management_image'];
        $activities_image = $record['activity_image'];
        $extraInfo1 = $record['extra_info1'];
        $extraInfo2 = $record['extra_info2'];
        $extraInfo3 = $record['extra_info3'];
        $brief = $record['brief'];
        $noOfRooms = $record['no_of_room'];
        $noOfBeds = $record['no_of_beds'];
        $long = $record['longitude'];
        $lat = $record['latitude'];


        if (empty($lat) && empty($long)) {
            $geoData = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip=' . $IPaddress));
            $lat = $geoData['geoplugin_latitude'];
            $long = $geoData['geoplugin_longitude'];
        }
    }
}


if (isset($submit) && $submit == 'Update') {
    $fields_array = artwork();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $fields = array(
            'title' => trim($_POST['title']),
            'description' => trim($_POST['description']),
            'keyword' => trim($_POST['keyword']),
            'city_id' => trim($_POST['city']),
            'suburb_id' => trim($_POST['suburb']),
            'restcare_id' => trim($_POST['restcare']),
            'ext_url' => trim($_POST['ext_url']),
            'youtube_video' => $_POST['youtube_video'],
            'zip' => trim($_POST['zip']),
            'latitude' => $latitude,
            'longitude' => $longitude,
            'modified' => date('Y-m-d h:i:s', time()),
            'staff_comment' => trim($_POST['staff_comment']),
            'staff_image' => $stf_image_name,
            'management_comment' => trim($_POST['management_comment']),
            'management_image' => $mng_image_name,
            'activity_comment' => trim($_POST['activities_comment']),
            'activity_image' => $act_image_name
        );
        $extar_field = array(
            'brief' => trim($_POST['brief']),
            'no_of_room' => trim($_POST['noOfRooms']),
            'no_of_beds' => trim($_POST['noOfBeds']),
            'extra_info1' => trim($_POST['extraInfo1']),
            'extra_info2' => trim($_POST['extraInfo2']),
            'extra_info3' => trim($_POST['extraInfo3']),
            'modified ' => date('Y-m-d h:i:s', time())
        );
        $addFacility = $addMoreFacility;
//prd($extar_field);
        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('products'), $fields, $where);
        if ($update_result) {



            //Delete service type having product_id of updated service/product
            $queryD = "DELETE FROM " . _prefix("pro_services") . " WHERE product_id = '$pr_id'";
            $delete_service = mysqli_query($db->db_connect_id,$queryD);
            //Inserting services type ids in pro_services table
            if ($delete_service) {
                foreach ($_POST['facility_type'] as $servise_id) {
                    $services_field = array(
                        'supplier_id' => $useId,
                        'product_id' => $pr_id,
                        'service_id' => $servise_id,
                        'modified ' => date('Y-m-d h:i:s', time())
                    );
                    // Message for insert
                    $insert_service = $db->insert(_prefix('pro_services'), $services_field);
                }
            }
            $extar_field['product_id'] = $pr_id;
            $extra_where = "where id= '$ex_id'";
            $insert_result_extra = $db->update(_prefix('extra_facility'), $extar_field, $extra_where);
            if ($insert_result_extra) {
                if ($addFacility != "") {
                    $to = ADMIN_EMAIL;
                    $email = emailTemplate('supplier_add_facility');
                    if ($email['subject'] != '') {
                        $message = str_replace(array('{supplier_name}', '{addfacility}'), array($supplier_name, $addFacility), $email['description']);
                        $send = sendmail($to, $email['subject'], $message);
                    }
                }
                $msg = common_message_supplier(1, constant('UPDATE_PRODUCT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(HOME_PATH . "admin/user.php?supplierDetail&id=$supplier_id");
            }
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
//prd($id);
?>

<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<div class="container" style="padding:0px 3% 0;width: 80%;">
    <div class="row">
        <div class="dashboard_container">
            <div class="col-lg-12 col-md-12 col-sm-9 col-xs-12">
                <div class="dashboard_right_col">
                    <div class="col-lg-12 col-md-12">
                        <div class="col-md-8 col-lg-8">
                            <h2 class="hedding_h2"><i class="fa fa-pencil-square-o"></i> <span>Edit Service/Product</span></h2>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <button type="button" onclick="window.history.back();" name="back" id="submit" class="btn btn-primary pull-right" style=" padding:6px 20px 6px; margin-top:8%;">Go Back</button>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 thumbnail adbooking_container">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form name="addLogo" id="addLogo" action="" method="POST" class="form-horizontal form_payment" role="form" enctype="multipart/form-data">
                                    <input type="hidden" id="address" value=""/>

                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list_container">
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="title" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Product/Service Name <span class="redCol">*</span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="title" name="title" maxlength="50" class="form-control input_fild_1 required" type="text" value="<?php echo isset($title) ? stripslashes($title) : ''; ?>">
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="description" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Description</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <textarea rows="4" cols="50" maxlength="500" name="description" class="form-control input_fild_1" id="description"><?php
                                                        if ((isset($description)) && !empty($description)) {
                                                            echo stripslashes($description);
                                                        }
                                                        ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="keyword" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Keyword <span class="redCol">*</span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="keyword" name="keyword" maxlength="20" class="form-control input_fild_1" type="text" value="<?php echo isset($keyword) ? stripslashes($keyword) : ''; ?>">
                                                </div>
                                            </div>

                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="facility_type_id" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Facility Type <span class="redCol">* </span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <select id="facility_type_id" style="margin: 8px 0;" class="required form-control select_option_1 pull-left" name="facility_type[]" style="width:235px;" multiple="true"><?php echo serviceCatMultiple($service_ids); ?></select>
                                                </div>
                                            </div>
                                            <script type="text/javascript">
                                                $(document).ready(function() {
                                                    $('#facility_type_id').multiselect();
                                                })
                                            </script>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="city" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">City <span class="redCol">*</span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <select id="city" style="margin: 8px 0;" class="required form-control select_option_1 pull-left" name="city" style="width:235px;"><?php echo cityList($city); ?></select>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="suburb" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Suburb</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <select id="suburb" style="margin: 8px 0;" class="form-control select_option_1 pull-left" name="suburb" style="width:235px;"><?php echo suburbList($city, $suburb); ?></select>
                                                </div>
                                            </div>
                                            <div class="form-group" style="margin-bottom:6px;">
                                                <label for="restcare" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Rest Care</label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <select id="restcare" style="margin: 8px 0;" class="form-control select_option_1 pull-left" name="restcare" style="width:235px;"><?php echo restCareList($suburb, $restcare); ?></select>
                                                </div>
                                            </div>
                                            <div class="form-group"  style="margin-bottom:6px;">
                                                <label for="staff" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Facility Description </label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 thumbnail">
                                                    <div class="col-md-12">
                                                        <label for="brief" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Brief Description</label>
                                                        <textarea  name="brief" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                            if ((isset($brief)) && !empty($brief)) {
                                                                echo stripslashes($brief);
                                                            }
                                                            ?></textarea>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="noOfRooms" class="col-lg-4 col-md-2 col-sm-10 col-xs-12 label_list text-left duration">No of Rooms</label>
                                                        <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                                            <input id="noOfRooms" name="noOfRooms" maxlength="5" class="form-control input_fild_1  " type="text" value="<?php echo isset($noOfRooms) ? stripslashes($noOfRooms) : ''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="noOfBeds" class="col-lg-4 col-md-2 col-sm-10 col-xs-12 label_list text-left duration">No of Beds</label>
                                                        <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                                            <input id="noOfBeds" name="noOfBeds" maxlength="5" class="form-control input_fild_1  " type="text" value="<?php echo isset($noOfBeds) ? stripslashes($noOfBeds) : ''; ?>">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="extraInfo1" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Extra Info1</label>
                                                        <textarea id="extraInfo1"  name="extraInfo1" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                            if ((isset($extraInfo1)) && !empty($extraInfo1)) {
                                                                echo stripslashes($extraInfo1);
                                                            }
                                                            ?></textarea>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="extraInfo2" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Extra Info2</label>
                                                        <textarea id="extraInfo2"  name="extraInfo2" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                            if ((isset($extraInfo2)) && !empty($extraInfo2)) {
                                                                echo stripslashes($extraInfo2);
                                                            }
                                                            ?></textarea>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="extraInfo3" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Extra Info3</label>
                                                        <textarea id="extraInfo3"  name="extraInfo3" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                            if ((isset($extraInfo1)) && !empty($extraInfo1)) {
                                                                echo stripslashes($extraInfo1);
                                                            }
                                                            ?></textarea>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <label for="addMoreFacility" class="col-lg-4 col-md-2 col-sm-10 col-xs-12 label_list text-left duration">Add More Facility</label>
                                                        <div class="col-lg-7 col-md-8 col-sm-12 col-xs-12">
                                                            <input id="addMoreFacility" name="addMoreFacility" maxlength="150" class="form-control input_fild_1  " type="text" value="<?php echo isset($addMoreFacility) ? stripslashes($addMoreFacility) : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group"  style="margin-bottom:6px;">
                                                <label for="staff" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Staff </label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 thumbnail">
                                                    <div class="col-md-12">
                                                        <label for="comment" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Comment</label>
                                                        <textarea  name="staff_comment" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                            if ((isset($staff_comment)) && !empty($staff_comment)) {
                                                                echo stripslashes($staff_comment);
                                                            }
                                                            ?></textarea>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group"  style="margin-bottom:6px;">
                                                <label for="management" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Management </label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 thumbnail">
                                                    <div class="col-md-12">
                                                        <label for="comment" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Comment</label>
                                                        <textarea name="management_comment" cols="50" maxlength="500" class="form-control input_fild_1">
                                                            <?php
                                                            if ((isset($management_comment)) && !empty($management_comment)) {
                                                                echo stripslashes($management_comment);
                                                            }
                                                            ?>
                                                        </textarea>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group"  style="margin-bottom:6px;">
                                                <label for="activities" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Activities </label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 thumbnail">
                                                    <div class="col-md-12">
                                                        <label for="comment" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list ">Comment</label>
                                                        <textarea  name="activities_comment" cols="50" maxlength="500" class="form-control input_fild_1"><?php
                                                            if ((isset($activities_comment)) && !empty($activities_comment)) {
                                                                echo stripslashes($activities_comment);
                                                            }
                                                            ?></textarea>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="form-group" id="zipid" style="margin-bottom:6px;">
                                                <label for="zip" class="col-lg-4 col-md-4 col-sm-10 col-xs-12 label_list text-right duration">Zip Code <span class="redCol">*</span></label>
                                                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                                                    <input id="zip" name="zip" maxlength="4" class="form-control input_fild_1 required number" type="text" value="<?php echo isset($zip) ? stripslashes($zip) : ''; ?>">
                                                </div>
                                            </div>



                                            <!--   <?php if (file_exists(DOCUMENT_PATH . 'admin/files/product/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/product/' . $image ?>"/>
                                            <?php } ?>
                                           </div>
                                       </div>-->


                                            <div class="form-group text-center total_bg" style="width:93%; padding:2% 0 0 0;">
                                                <div class="form-group text-center total_bg" style="width:93%; padding:2% 0 0 0;">
                                                    <button type="submit" value="Update" name="submit" id="submit" class="btn btn-danger center-block btn_search btn_pay pull-right" style=" padding:6px 20px 6px; margin-bottom:2%;">UPDATE</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;color: black;}
    label.error{
        color:red;
        font-size: 10px;

    }
    select.error{
        color:#000;
    }
    input.error{
        color:#000;}
    .redCol{
        color:red;
    }

</style>
<script type="text/javascript">
    $('#expectDate').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '2010',
    });
    $(document).ready(function() {



        $("#openImage").fancybox({
            width: 400, // or whatever
            height: 400,
            autoSize: false
        });

        $('#city').change(function() {
            var cityId = $('#city').val();
            var path = '<?php echo HOME_PATH; ?>';
//            alert(cityId);
//            alert(path);
            $.ajax({
                url: path + 'ajaxFront.php?' + new Date().getTime(),
                data: 'action=getSuburb&cityId=' + cityId,
                type: "POST",
                cache: false,
                success: function(response) {
                    try {
                        $('#suburb').html(response);
                    } catch (e) {
                    }
                }
            })
        });
        $('#addLogo').validate({
            rules: {
                staff_image: {
                    accept: 'jpg,png,jpeg,gif'

                },
                management_image: {
                    accept: 'jpg,png,jpeg,gif'

                },
                activities_image: {
                    accept: 'jpg,png,jpeg,gif'

                }

            },
            messages: {
                staff_image: {
                    accept: "Image type jpg/png/jpeg/gif is allowed"
                },
                management_image: {
                    accept: "Image type jpg/png/jpeg/gif is allowed"
                },
                activities_image: {
                    accept: "Image type jpg/png/jpeg/gif is allowed"
                }
            },
            submitHandler: function(form) {
                $('#submit').attr('disabled', 'disabled');
                form.submit();

            }


        });
        $('#suburb').change(function() {
            var suburbId = $('#suburb').val();
            var path = '<?php echo HOME_PATH; ?>';
            $.ajax({
                url: path + 'ajaxFront.php?' + new Date().getTime(),
                data: 'action=getRestCare&suburbId=' + suburbId,
                type: "POST",
                cache: false,
                success: function(response) {
                    try {
                        $('#restcare').html(response);
                    } catch (e) {
                    }
                }
            })
        });


    });

</script>



