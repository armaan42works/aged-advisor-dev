<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
    global $db;
    include_once("./fckeditor/fckeditor.php");
    $description = '';
    $blogId = isset($_GET[blogId]) ? $_GET['blogId'] : '';
    if (isset($submit) && $submit == 'Submit') {

        $fields_array = comment();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {

            $getMax = "SELECT max(order_by) as count from " . _prefix("blog_comments") . " WHERE status = 1 && deleted = 0 ";
            $resMax = $db->sql_query($getMax);
            $dataMax = mysqli_fetch_assoc($resMax);
            $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
            $tmp = strip_tags($_POST["comment"], '&nbsp;');
            $_POST['comment'] = trim($tmp, '&nbsp;') == '' ? '' : $_POST["comment"];
            $fields = array(
                'blog_id' => $_POST['blog_id'],
                'comment' => $_POST['comment'],
                'user_type' => 0,
                'commented_on' => date('Y-m-d H:i:s')
            );


            $insert_result = $db->insert(_prefix('blog_comments'), $fields);
            if ($insert_result) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/blog.php?comment&id=" . $_POST['blog_id']);
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
    if (key($_REQUEST) == 'editComment') {
        $id = $_GET['commentId'];
        $sql_query = "SELECT * FROM " . _prefix("blog_comments") . " where id='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        if (count($records)) {
            foreach ($records as $record) {

                $blogId = $record['blog_id'];
                $comment = $record['comment'];
            }
        }
    }
    if (isset($update) && $update == 'Update') {
        $id = $_GET['commentId'];

        $fields_array = comment();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
//            $blogId = $_POST['blog_id'];
//            $comment = $_POST['comment'];
            $fields = array(
                'comment' => trim($_POST['comment']),
            );


            $where = "where id= '$id'";
            $update_result = $db->update(_prefix('blog_comments'), $fields, $where);
            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/blog.php?comment&id=" . $_POST['blog_id']);
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "blog.php?manage" ?>">Manage Blog</a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                    </ul>
                </div>
        </tr>
    </tbody>
</table>

<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Add Comment</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="form1" id="addblogs" method="POST" action=""  class="form-horizontal" role="form">
        <div class="form-group">
            <label for="comment" class="col-sm-2 control-label"> Comment<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea  cols="50" rows="4" maxlength="500" class="required form-control" name="comment" id="comment"><?php echo isset($comment) ? ($comment) : ''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <input type="hidden" name="blog_id" id="blog_id" value="<?php echo isset($blogId) ? $blogId : ''; ?>"/>

        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                    if (key($_REQUEST) == 'editComment') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                ?>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
$(document).ready(function() { $('input[type="submit"]').focus();});
$('#addblogs').validate();
</script>

