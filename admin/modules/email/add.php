<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
global $db;
$content = "";
include_once("./fckeditor/fckeditor.php");
if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM  " . _prefix("email_templates") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {

        foreach ($records as $record) {

            $title = $record['title'];
            $alias = $record['alias'];
            $subject = $record['subject'];
            $content = $record['description'];
        }
    }
}

if (isset($submit) && $submit == 'Submit') {

    $fields_array = mail_template();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $fields = array(
            'title' => trim($title),
            'alias' => trim($alias),
            'subject' => trim($subject),
            'description' => $FCKeditor1
        );

        $insert_result = $db->insert(_prefix('email_templates'), $fields);


        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/email.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (isset($update) && $update == 'Update') {

    $fields_array = mail_template();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {


        $fields = array(
            'title' => trim($_POST['title']),
            'alias' => trim($_POST['alias']),
            'subject' => trim($_POST['subject']),
            'description' => $_POST['FCKeditor1']
        );


        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('email_templates'), $fields, $where);

        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/email.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div style="margin-top: 10px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "email.php?manage" ?>">Manage Email Template</a>>>Add</li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
            ?>
        </div>
    </div>

    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Email Template</h2>
            </div>
            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="addEmailTemplate" id="addEmailTemplate" method="POST" action=""  class="form-horizontal" role="form">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Title<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="title" size="50" minlength="2" maxlength="50"  id="title"  value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="subject" class="col-sm-2 control-label">Subject<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="subject" size="50" minlength="4" maxlength="50"  id="subject"  value="<?php echo isset($subject) ? stripslashes($subject) : ''; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="alias" class="col-sm-2 control-label">Alias<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="alias" size="50" minlength="2" maxlength="50"  id="alias"  value="<?php echo isset($alias) ? stripslashes($alias) : ''; ?>"/>
                </div>
            </div>
            <?php
            if (isset($_POST['FCKeditor1'])) {
                $content = $_POST['FCKeditor1'];
            } elseif (isset($content)) {
                $content = $content;
            } else {
                $content = '';
            }
            ?>
            <div class="form-group">
                <label for="content" class="col-sm-2 control-label">Content<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <?php
                    $oFCKeditor = new FCKeditor('FCKeditor1');
                    $oFCKeditor->BasePath = './fckeditor/';
                    $oFCKeditor->Height = '400px';
                    $oFCKeditor->Value = $content;
                    $oFCKeditor->Create();
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">
                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type=submit]').focus();
        $('#addEmailTemplate').validate();
    });
</script>

<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>


