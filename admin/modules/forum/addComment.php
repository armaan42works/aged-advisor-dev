<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
global $db;
include_once("./fckeditor/fckeditor.php");
$description = '';
$forumId = isset($_GET[forumId]) ? $_GET['forumId'] : '';
if (isset($submit) && $submit == 'Submit') {

    $fields_array = comment();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

//            $getMax = "SELECT max(order_by) as count from " . _prefix("blog_comments") . " WHERE status = 1 && deleted = 0 ";
//            $resMax = $db->sql_query($getMax);
//            $dataMax = mysqli_fetch_assoc($resMax);
//            $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
        $tmp = strip_tags($_POST["comment"], '&nbsp;');
        $_POST['comment'] = trim(trim($tmp, '&nbsp;')) == '' ? '' : $_POST["comment"];
        $fields = array(
            'blog_id' => $_POST['forum_id'],
            'comment' => trim($_POST['comment']),
            'user_type' => 0,
            'commented_on' => date('Y-m-d H:i:s')
        );

        $insert_result = $db->insert(_prefix('blog_comments'), $fields);
        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/forum.php?comment&id=" . $_POST['forum_id']);
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (key($_REQUEST) == 'editComment') {
    $id = $_GET['commentId'];
    $sql_query = "SELECT * FROM " . _prefix("blog_comments") . " where id='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {

            $forumId = $record['blog_id'];
            $comment = $record['comment'];
        }
    }
}
if (isset($update) && $update == 'Update') {
    $id = $_GET['commentId'];

    $fields_array = comment();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
//            $forumId = $_POST['forum_id'];
//            $comment = $_POST['comment'];
        $fields = array(
            'comment' => trim($_POST['comment']),
        );


        $where = "where id= '$id'";
        $update_result = $db->update(_prefix('blog_comments'), $fields, $where);
        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/forum.php?comment&id=" . $_POST['forum_id']);
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div style="margin-top: 10px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "forum.php?manage" ?>">Manage forum</a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                        </ul>
                    </div>
            </tr>
        </tbody>
    </table>
    <div class="row" style="margin-bottom: 20px;">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
            ?>
        </div>
    </div>
    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class="">Add Comment</h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="form1" id="addforums" method="POST" action=""  class="form-horizontal" role="form">
            <div class="form-group">
                <label for="comment" class="col-sm-2 control-label"> Comment<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <textarea  cols="50" rows="4" class="required form-control" name="comment" id="comment" minlength="4" maxlength="500"><?php echo isset($comment) ? ($comment) : ''; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <input type="hidden" name="forum_id" id="forum_id" value="<?php echo isset($forumId) ? $forumId : ''; ?>"/>

            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'editComment') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#addforums').validate();
</script>

