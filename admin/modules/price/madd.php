<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
global $db;
$description = '';
$price = '';

if (isset($submit) && $submit == 'Submit') {

    $fields_array = m_price();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $getMax = "SELECT max(order_by) as count from " . _prefix("membership_prices") . " WHERE status = 1 && deleted = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = mysqli_fetch_assoc($resMax);
        $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
        $fields = array(
            'type' => $type,
            'description' => trim($_POST['description']),
            'price' => $_POST["price"],
            'duration' => $_POST["duration"],
            'number_of_review_response' => $_POST["number_of_review_response"],
            'title' => trim($title),
            'discount' => $discount,
            'video_enable' => isset($videoEnable) ? $videoEnable : 0,
            'logo_enable' => isset($logoEnable) ? $logoEnable : 0,
            'image_enable' => isset($imageEnable) ? $imageEnable : 0,
            'image_limit' => isset($imageLimit) ? $imageLimit : 1,
            'ext_url_enable' => isset($extUrlEnable) ? $extUrlEnable : 0,
            'youtube_video' => isset($youtubeVideo) ? $youtubeVideo : 0,
            'map_enable' => isset($mapEnable) ? $mapEnable : 0,
            'facility_desc_enable' => isset($facilityEnable) ? $facilityEnable : 0,
            'staff_enable' => isset($staffEnable) ? $staffEnable : 0,
            'management_enable' => isset($managementEnable) ? $managementEnable : 0,
            'activity_enable' => isset($activityEnable) ? $activityEnable : 0,
            'appointment_enable' => isset($appointment_enable) ? $appointment_enable : 0,
            'vacancy_enable' => isset($vacancy_enable) ? $vacancy_enable : 0,
            'send_message_enable' => isset($send_message_enable) ? $send_message_enable : 0,
            'events_calendar_enable' => isset($events_calendar_enable) ? $events_calendar_enable : 0
                //'address_enable' => isset($addressEnable) ? $addressEnable : 0,
                //'phone_number_enable' => isset($phoneNumberEnable) ? $phoneNumberEnable : 0
        );
        $insert_result = $db->insert(_prefix('membership_prices'), $fields);
        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/price.php?mprice");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (isset($update) && $update == 'Update') {
    $fields_array = m_price();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $title = $_POST['title'];
        $description = trim($_POST["description"]);
        $price = $_POST["price"];
        $duration = $_POST['duration'];
        $number_of_review_response = $_POST['number_of_review_response'];
        $discount = $_POST['discount'];
        
        $fields = array(
            'title' => trim($title),
            'description' => trim($description),
            'duration' => $duration,
            'number_of_review_response' => $number_of_review_response,
            'price' => $price,
            'discount' => $discount,
            'video_enable' => isset($videoEnable) ? $videoEnable : 0,
            'logo_enable' => isset($logoEnable) ? $logoEnable : 0,
            'image_enable' => isset($imageEnable) ? $imageEnable : 0,
            'image_limit' => isset($imageLimit) ? $imageLimit : 1,
            'ext_url_enable' => isset($extUrlEnable) ? $extUrlEnable : 0,
            'youtube_video' => isset($youtubeVideo) ? $youtubeVideo : 0,
            'map_enable' => isset($mapEnable) ? $mapEnable : 0,
            'facility_desc_enable' => isset($facilityEnable) ? $facilityEnable : 0,
            'staff_enable' => isset($staffEnable) ? $staffEnable : 0,
            'management_enable' => isset($managementEnable) ? $managementEnable : 0,
            'activity_enable' => isset($activityEnable) ? $activityEnable : 0,
            'appointment_enable' => isset($appointment_enable) ? $appointment_enable : 0,
            'vacancy_enable' => isset($vacancy_enable) ? $vacancy_enable : 0,
            'send_message_enable' => isset($send_message_enable) ? $send_message_enable : 0,
            'events_calendar_enable' => isset($events_calendar_enable) ? $events_calendar_enable : 0
//            'address_enable' => isset($addressEnable) ? $addressEnable : 0,
//            'phone_number_enable' => isset($phoneNumberEnable) ? $phoneNumberEnable : 0
//            'product_limit' => isset($productLimit) ? $productLimit : 1
        );
      //  prd($fields);

        $where = "where id = $id";
        $update_result = $db->update(_prefix('membership_prices'), $fields, $where);
        $query_update = $db->sql_query("UPDATE ". _prefix("pro_plans") ." SET response_of_review =".$number_of_review_response." WHERE plan_id = ".$id."");
        if ($update_result) {
            $queryD = "DELETE FROM " . _prefix("plan_services") . " WHERE  plan_type_id = $id";
            mysqli_query($db->db_connect_id,$queryD);
            foreach ($services as $service) {
                $created = date('Y-m-d h:i:s', time());
                if ($service != '') {
                    $query = 'Insert into ' . _prefix("plan_services") . ' (service_id, plan_type_id,created) values("' . $service . '","' . $id . '","' . $created . '")';
                    $insert_result = mysqli_query($db->db_connect_id,$query);
                }
            }
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/price.php?mprice");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

if ((key($_REQUEST) == 'medit') && !(isset($update) && $update == 'Update')) {
    $id = $_GET['id'];
    $ser_query = "SELECT id, name FROM " . _prefix("services") . " where status=1 AND deleted=0";
    $serResult = $db->sql_query($ser_query);
    $serviceRecords = $db->sql_fetchrowset($serResult);
    $selectedPlan_query = "SELECT id, service_id FROM " . _prefix("plan_services") . " where status=1 AND deleted=0 AND plan_type_id= $id";
    $selectedPlanResult = $db->sql_query($selectedPlan_query);
    $selectedPlanRecords = $db->sql_fetchrowset($selectedPlanResult);
    $servicesId = array();
    if (count($selectedPlanRecords)) {
        foreach ($selectedPlanRecords as $value) {
            $servicesId[] = $value['service_id'];
        }
    }
    $sql_query = "SELECT * FROM " . _prefix("membership_prices") . " where id = $id";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {

            $title = $record['title'];
            $description = trim($record['description']);
            $price = $record['price'];
            $duration = $record['duration'];
            $number_of_review_response = $record['number_of_review_response'];
            $discount = $record['discount'];
            $appointment_enable  = $record['appointment_enable'];
            $vacancy_enable  = $record['vacancy_enable'];
            $send_message_enable = $record['send_message_enable'];
            $events_calendar_enable = $record['events_calendar_enable'];
            $videoEnable = $record['video_enable'];
            $logoEnable = $record['logo_enable'];
            $imageEnable = $record['image_enable'];
            $imageLimit = $record['image_limit'];
            $extUrlEnable = $record['ext_url_enable'];
//          $productLimit = $record['product_limit'];
            $youtubeVideo = $record['youtube_video'];
            $mapEnable = $record['map_enable'];
            $facilityEnable = $record['facility_desc_enable'];
            $staffEnable = $record['staff_enable'];
            $managementEnable = $record['management_enable'];
            $activityEnable= $record['activity_enable'];
//          $addressEnable = $record['address_enable'];
//          $phoneNumberEnable = $record['phone_number_enable'];
        }
    }
}
?>
<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "price.php?mprice" ?>"> Manage Membership </a>>> <?php echo (key($_REQUEST) == 'medit') ? 'Edit' : 'Add'; ?></li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Update Plan</h2>
        </div>
        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form method="POST" name="addPlan" id="addPlan" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <!--<input type="text"  class="required form-control" name="title" id="name" placeholder="Title"  value="<?php echo isset($title) ? ($title) : ''; ?>"/>-->
                <input type="text" name="title" id=""  value="<?php
                // $plan = array('Basic', 'Business', 'Premium');
                if (isset($title)) {
                    echo stripslashes($title);
                }
                ?>" class="required form-control"  placeholder="Title"   />
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea name="description" cols="50" rows="4" id="descritption" class="required form-control" placeholder="Description"><?php echo isset($description) ? trim($description) : ''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-sm-2 control-label">Plan Price<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <div class="input-group ">
                    <span class="input-group-addon">$</span>
                    <input type="text" name="price" style="width:294px;" class="number col-sm-4 required form-control" id="price" value="<?php echo isset($price) ? $price : ''; ?>"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="duration" class="col-sm-2 control-label">Duration<span class="redCol">*</span></label>
            <div class="col-sm-4">
                <input type="text" name="duration" id=""  value="<?php
                if (isset($duration)) {
                    echo stripslashes($duration);
                }
                ?>" class="required form-control"/>
            </div>
        </div>
<div class="form-group">
            <label for="number_of_review_response" class="col-sm-2 control-label">Review Respponse<span class="redCol">*</span></label>
            <div class="col-sm-4">
                <input type="text" name="number_of_review_response" id=""  value="<?php
                if (isset($number_of_review_response)) {
                    echo stripslashes($number_of_review_response);
                }
                ?>" class="required form-control"/>
            </div>
        </div>
        <div class="form-group">
            <label for="upgrade" class="col-sm-2 control-label">Upgrade Discount Percentage<span class="redCol">* </span></label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="text" name="discount" style="width:294px;" class="number col-sm-4 required form-control" id="upgrade" value="<?php echo isset($discount) ? $discount : ''; ?>"/>
                    <span class="input-group-addon">%</span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="logoEnable" class="col-sm-2 control-label">Enable Show Logo</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <input type="checkbox" style="width:25px;" name="logoEnable" value="1"  class="col-sm-4 form-control" id="logoEnable" <?php echo ($logoEnable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="mapEnable" class="col-sm-2 control-label">Enable Show map</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <input type="checkbox" style="width:25px;" name="mapEnable" value="1"  class="col-sm-4 form-control" id="mapEnable" <?php echo ($mapEnable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <?php /* ?>
          <div class="form-group">
          <label for="addressEnable" class="col-sm-2 control-label">Enable Show Address</label>
          <div class="col-sm-4">
          <div class="input-group">
          <input type="checkbox" style="width:25px;" name="addressEnable" value="1"  class="col-sm-4 form-control" id="addressEnable" <?php echo ($addressEnable == 1) ? 'checked' : ''; ?> />
          </div>
          </div>
          </div>
          <div class="form-group">
          <label for="phoneNumberEnable" class="col-sm-2 control-label">Enable Show Phone Number</label>
          <div class="col-sm-4">
          <div class="input-group">
          <input type="checkbox" style="width:25px;" name="phoneNumberEnable" value="1"  class="col-sm-4 form-control" id="phoneNumberEnable" <?php echo ($phoneNumberEnable == 1) ? 'checked' : ''; ?> />
          </div>
          </div>
          </div>
          <?php */ ?>
        <div class="form-group">
            <label for="videoEnable" class="col-sm-2 control-label">Enable Show Video</label>
            <div class="col-sm-4">
                <div class="input-group">
                    <input type="checkbox" style="width:25px;" name="videoEnable" value="1"  class="col-sm-4 form-control" id="videoEnable" <?php echo ($videoEnable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
<!--        <div id="vlimitDiv" style="<?php echo ($videoEnable == 1 ? '' : 'display:none;'); ?>" class="form-group">
            <label for="videoLimit" class="col-sm-2 control-label">Video Limit</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="text" name="videoLimit"  class="number col-sm-4 form-control" id="videoLimit" value="<?php echo isset($videoLimit) ? $videoLimit : '1'; ?>"/>
                </div>
            </div>
        </div>-->
        <div class="form-group">
            <label for="imageEnable" class="col-sm-2 control-label">Enable Show Media(Image/Video)</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="imageEnable" class="col-sm-4  form-control" id="imageEnable" value="1"  <?php echo ($imageEnable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div id="imglimitDiv"  style="<?php echo ($imageEnable == 1 ? '' : 'display:none;'); ?>" class="form-group">
            <label for="imageLimit" class="col-sm-2 control-label">Media(Image/Video) Limit</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="text" name="imageLimit" class="number col-sm-4 form-control" id="imageLimit" value="<?php echo isset($imageLimit) ? $imageLimit : '1'; ?>"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="extUrlEnable" class="col-sm-2 control-label">Enable Show External Url</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="extUrlEnable" class="col-sm-4  form-control" id="extUrlEnable" value="1"  <?php echo ($extUrlEnable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="youtubeVideo" class="col-sm-2 control-label">Enable YouTube URL</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="youtubeVideo" class="col-sm-4  form-control" id="youtubeVideo" value="1"  <?php echo ($youtubeVideo == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="vacancy_enable" class="col-sm-2 control-label">Enable Enquire About Vacancy</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="vacancy_enable" class="col-sm-4  form-control" id="vacancy_enable" value="1"  <?php echo ($vacancy_enable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="send_message_enable" class="col-sm-2 control-label">Enable Send a Message</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="send_message_enable" class="col-sm-4  form-control" id="send_message_enable" value="1"  <?php echo ($send_message_enable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="appointment_enable" class="col-sm-2 control-label">Enable Appointment</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="appointment_enable" class="col-sm-4  form-control" id="appointment_enable" value="1"  <?php echo ($appointment_enable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="events_calendar_enable" class="col-sm-2 control-label">Enable Events Calendar</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="events_calendar_enable" class="col-sm-4  form-control" id="events_calendar_enable" value="1"  <?php echo ($events_calendar_enable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="facilityEnable" class="col-sm-2 control-label">Enable Facility Description</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="facilityEnable" class="col-sm-4  form-control" id="youtubeVideo" value="1"  <?php echo ($facilityEnable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="staffEnable" class="col-sm-2 control-label">Enable Staff</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="staffEnable" class="col-sm-4  form-control" id="youtubeVideo" value="1"  <?php echo ($staffEnable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="managementEnable" class="col-sm-2 control-label">Enable Management</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="managementEnable" class="col-sm-4  form-control" id="youtubeVideo" value="1"  <?php echo ($managementEnable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="activityEnable" class="col-sm-2 control-label">Enable Activity</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <input type="checkbox" style="width:25px;"  name="activityEnable" class="col-sm-4  form-control" id="youtubeVideo" value="1"  <?php echo ($activityEnable == 1) ? 'checked' : ''; ?> />
                </div>
            </div>
        </div>
        <?php /* ?>
          <div class="form-group">
          <label for="productLimit" class="col-sm-2 control-label">Product Limit</label>
          <div class="col-sm-4">
          <div class="input-group ">
          <input type="text" name="productLimit" class="number col-sm-4 form-control" id="productLimit" value="<?php echo isset($productLimit) ? $productLimit : '1'; ?>"/>
          </div>
          </div>
          </div>
          <?php */ ?>
        <div  class="form-group">
            <label for="services" class="col-sm-2 control-label">Choose Services</label>
            <div class="col-sm-4">
                <div class="input-group ">
                    <select multiple="true"  name="services[]" id='plan_type' class="required form-control" style="width:235px;">
                        <?php
                        foreach ($serviceRecords as $service) {
                            if (in_array($service['id'], $servicesId)) {
                                echo '<option value="' . $service['id'] . '" selected="selected">' . $service['name'] . '</option>';
                            } else {
                                echo '<option value="' . $service['id'] . '">' . $service['name'] . '</option>';
                            }
                        }
//                    $plan_type = $plan_type != '' ? $plan_type : $_POST['plan_type'];
//                    echo options($select_type, $plan_type);
                        ?>
                    </select> 
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                if (key($_REQUEST) == 'medit') {
                    ?>
                    <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                    <?php
                } else {
                    ?>
                    <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                    <?php
                }
                ?>
            </div>
        </div>
    </form>
</div>

<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
//
//        $('#videoEnable').change(function() {
//            if ($(this).is(":checked")) {
//                $('#vlimitDiv').show();
//            } else {
//                $('#vlimitDiv').hide();
//                $('#videoLimit').val('1');
//            }
//        });
        $('#imageEnable').change(function() {
            if ($(this).is(":checked")) {
                $('#imglimitDiv').show();
            } else {
                $('#imglimitDiv').hide();
                $('#imageLimit').val('1');
            }
        });
    });
    $('#addPlan').validate();
</script>
