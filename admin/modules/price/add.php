<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
    global $db;
    $description = '';
    include_once("./fckeditor/fckeditor.php");


    if (isset($submit) && $submit == 'Submit') {

        $fields_array = b_price();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {

            $getMax = "SELECT max(order_by) as count from " . _prefix("space_prices") . " WHERE status = 1 && deleted = 0";
            $resMax = $db->sql_query($getMax);
            $dataMax = mysqli_fetch_assoc($resMax);
            $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
            $fields = array(
                'no_of_day' => trim($no_of_day),
                'description' => trim($_POST['description']),
                'price' => trim($_POST["price"])
            );
            $insert_result = $db->insert(_prefix('space_prices'), $fields);
            if ($insert_result) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/price.php?comboprice");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
    if (key($_REQUEST) == 'edit') {
        $id = $_GET['id'];
        $sql_query = "SELECT * FROM " . _prefix("space_prices") . " where md5(id)='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        if (count($records)) {
            foreach ($records as $record) {
                $day = $record['no_of_day'];
                $description = $record['description'];
                $price = $record['price'];
            }
        }
    }
    if (isset($update) && $update == 'Update') {
        $fields_array = b_price();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {

            $title = $_POST["title"];
            $description = $_POST["description"];
            $price = $_POST["price"];
            $fields = array('no_of_day' => trim($no_of_day),
                'description' => trim($description),
            );
            if ($_POST['price'] != '') {
                $fields['price'] = trim($price);
            }
            $where = "where md5(id)= '$id'";
            $update_result = $db->update(_prefix('space_prices'), $fields, $where);
            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here
                if($id== 'c4ca4238a0b923820dcc509a6f75849b'){
                redirect_to(MAIN_PATH . "price.php?bprice");
                }else{
                    redirect_to(MAIN_PATH . "price.php?comboprice");
                }
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>
<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home</a>>><a href="<?php echo HOME_PATH_URL . "price.php?bprice" ?>">Manage Ad Space</a>>><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>


<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Space Price</h2>
        </div>
        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form method="POST" name="addPrice" id="addPrice" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="form-group">
            <label for="No of days" class="col-sm-2 control-label">No. of days<span class="redCol">*</span></label>
            <?php if($id== 'c4ca4238a0b923820dcc509a6f75849b'){?>
            <div class="col-sm-8">
                <input type="text"  class="required maxLength form-control" maxlength="50" name="no_of_day" id="no_of_day" readonly="true"  value="<?php echo isset($day) ? ($day) : ''; ?>"/>
            </div>
            <?php }else{?>
            <div class="col-sm-8">
                <input type="text"  class="required maxLength form-control" maxlength="50" name="no_of_day" id="no_of_day"   value="<?php echo isset($day) ? ($day) : ''; ?>"/>
            </div>
            <?php } ?>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea name="description" cols="50" rows="4" id="descritption" minlength="4" maxlength="500" class="required form-control" ><?php echo isset($description) ? trim($description) : ''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-sm-2 control-label">Add Price<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <div class="input-group ">
                    <span class="input-group-addon">$</span>
                    <input type="text" name="price" style="width:400px;" maxlength="10" class="number col-sm-4 required form-control" id="price" value="<?php echo isset($price) ? $price : ''; ?>"/>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">
                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                ?>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#addPrice').validate();
</script>
