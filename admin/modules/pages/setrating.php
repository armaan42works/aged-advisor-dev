<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
    global $db;

    if (key($_REQUEST) == 'setRating') {
//    $id = $_GET['id'];
        $sql_query = "SELECT * FROM " . _prefix("rating_score");
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        // prd($records);
        if (count($records)) {
            foreach ($records as $record) {
                $caring_staff = $record['caring_staff'];
                $quality_of_care = $record['quality_of_care'];
                $responsive_management = $record['responsive_management'];
                $trips_outdoor_activities = $record['trips_outdoor_activities'];
                $indoor_entertainment = $record['indoor_entertainment'];
                $social_atmosphere = $record['social_atmosphere'];
                $enjoyable_food = $record['enjoyable_food'];
                $overall_rating = $record['overall_rating'];
                $id = $record['id'];
            }
        }
    }

    if (isset($update) && $update == 'Update') {
        $fields_array = rate_score();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {

            $fields = array(
                'quality_of_care' => trim($_POST['quality_of_care']),
                'caring_staff' => trim($_POST['caring_staff']),
                'responsive_management' => trim($_POST['responsive_management']),
                'trips_outdoor_activities' => trim($_POST['trips_outdoor_activities']),
                'indoor_entertainment' => trim($_POST['indoor_entertainment']),
                'social_atmosphere' => trim($_POST['social_atmosphere']),
                'enjoyable_food' => trim($_POST['enjoyable_food']),
                'overall_rating' => trim($_POST['overall_rating']),
            );
            $where = "where id= '$id'";
            $update_result = $db->update(_prefix('rating_score'), $fields, $where);

            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/pages.php?setRating");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Set Rating</li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . '/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <?php echo $_SESSION['msg']; ?>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>
<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Set Rating</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"><?php
            if (isset($error)) {
//                echo '<div style="color:#FF0000">' . $errors . '</div>';
            }
        ?></p>
    <p class="clearfix"></p>
    <form name="addPage" id="addPage"  enctype="multipart/form-data"  method="POST" action=""  class="form-horizontal" role="form">
        <input type="hidden" name="rid" value="<?php echo isset($id) ? stripslashes($id) : ''; ?>">
        <div class="form-group">
            <label for="quality_of_care" class="col-sm-2 control-label">Quality of Care<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="quality_of_care" id="quality_of_care" maxlength="50" value="<?php echo isset($quality_of_care) ? stripslashes($quality_of_care) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="caring_staff" class="col-sm-2 control-label">Caring/Helpful Staff<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="caring_staff" id="caring_staff"  value="<?php echo isset($caring_staff) ? stripslashes($caring_staff) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="responsive_management" class="col-sm-2 control-label">Responsive Management<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="responsive_management" id="responsive_management"  value="<?php echo isset($responsive_management) ? stripslashes($responsive_management) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="value trips_outdoor_activities money" class="col-sm-2 control-label">Trips/Outdoor Activities<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="trips_outdoor_activities" id="trips_outdoor_activities"  value="<?php echo isset($trips_outdoor_activities) ? stripslashes($trips_outdoor_activities) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="indoor_entertainment" class="col-sm-2 control-label">Indoor Entertainment<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="indoor_entertainment" id="indoor_entertainment"  value="<?php echo isset($indoor_entertainment) ? stripslashes($indoor_entertainment) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="social_atmosphere" class="col-sm-2 control-label">Social Atmosphere<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="social_atmosphere" id="social_atmosphere"  value="<?php echo isset($social_atmosphere) ? stripslashes($social_atmosphere) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="enjoyable_food" class="col-sm-2 control-label">Enjoyable Food<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="enjoyable_food" id="enjoyable_food"  value="<?php echo isset($enjoyable_food) ? stripslashes($enjoyable_food) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="overall_rating" class="col-sm-2 control-label">Overall Rating<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="overall_rating" id="overall_rating"  value="<?php echo isset($overall_rating) ? stripslashes($overall_rating) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <input type="submit" value="Update" name="update" class="submit_btn btn">
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();

        $('#addPage').validate({
        });

    });

</script>

