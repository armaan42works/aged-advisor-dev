<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
global $db;

include_once("./fckeditor/fckeditor.php");


$imageName = '';
$image_path = '';
$banner_image = '';
$bannerName = '';
$article_path='';
$article_pdf='';

if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("pages") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    // prd($records);
    if (count($records)) {

        foreach ($records as $record) {
            $page_title = $record['page_title'];
            $url = $record['url'];
            $meta_title = $record['meta_title'];
            $keyword = $record['keyword'];
            $article_pdf =$record['article_pdf'];
            $description = $record['description'];
            $article_type= $record['article_type'];
            $content = $record['content'];
            //echo  $image_path1 = $record['image_path'];
            //echo $banner_image1 = $record['banner_image'];
            $banner_image = $record['banner_image'];
            $short_description = $record['short_description'];
            $imageName = $banner_image;
        }
    }
}



if (isset($submit) && $submit == 'Submit') {

    $fields_array = add_page();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

//        if (isset($_FILES['image_path']['name'])) {
//
//
//            if ($_FILES['image_path']['error'] == 0) {
//
//                $target_path = '../admin/files/pages/feature_image/';
//
//                $date = new DateTime();
//                $timestamp = $date->getTimestamp();
//                $imageName = $timestamp . '_' . imgNameSanitize(basename($_FILES['image_path']['name']), 20);
//                $target_path = $target_path . $imageName;
//                move_uploaded_file($_FILES['image_path']["tmp_name"], $target_path);
//                $_POST['image_path'] = $imageName;
//            }
//        }
        if (isset($_FILES['banner_image']['name'])) {


            if ($_FILES['banner_image']['error'] == 0) {

                //$target_paths = '../admin/files/pages/feature_image/';
                $target_paths = '../admin/files/pages/banner/';

                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $ext = end(explode('.', $_FILES['banner_image']['name']));
                $bannerName = $timestamp . '_' . basename($_FILES['banner_image']['name']);
                $target_path = $target_paths . $bannerName;
                $target_path_thumb = $target_paths . 'thumb_' . $bannerName;
                move_uploaded_file($_FILES['banner_image']["tmp_name"], $target_path);
                ak_img_resize($target_path, $target_path_thumb, 250, 250, $ext);
                $_POST['banner_image'] = $bannerName;
            }
        }

        if (isset($_FILES['article_pdf']['name'])) {


            if ($_FILES['article_pdf']['error'] == 0) {

                //$target_paths = '../admin/files/pages/feature_image/';
                $target_paths = '../admin/files/pages/article_pdf/';

                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $ext = end(explode('.', $_FILES['article_pdf']['name']));
                $articleName =basename($_FILES['article_pdf']['name']);
                $target_path = $target_paths . $articleName;
                $target_path_thumb = $target_paths . 'thumb_' . $articleName;
                move_uploaded_file($_FILES['article_pdf']["tmp_name"], $target_path);
                $_POST['article_pdf'] = $articleName;
            }
        }

        $content = $_POST["content"];
        $article_type = $_POST["article_type"];

        $fields = array(
            'page_title' => trim($page_title),
            'url' => trim($url),
            'meta_title' => trim($meta_title),
            'keyword' => trim($keyword),
            'description' => trim($description),
            'short_description' => trim($short_description),
            'content' => trim($content),
            'article_type' => trim($article_type)
                // 'image_path' => $imageName,
                //'banner_image' => trim($_POST['banner_image']),
        );
        $fields['banner_image'] = trim($_POST['banner_image']);
        $fields['article_pdf'] = trim($_POST['article_pdf']);
        $insert_result = $db->insert(_prefix('pages'), $fields);


        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/pages.php?pages");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

if (isset($update) && $update == 'Update') {

    $fields_array = add_page();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $content = $_POST["FCKeditor1"];

        $fields = array(
            'page_title' => trim($_POST['page_title']),
            'url' => trim($_POST['url']),
            'meta_title' => trim($_POST['meta_title']),
            'keyword' => trim($_POST['keyword']),
            'description' => trim($_POST['description']),
            'content' => trim($_POST['content']),
            'short_description' => trim($_POST['short_description']),
            'article_type' => trim($_POST['article_type']),
        );

        if (isset($_FILES['banner_image']['name'])) {


            if ($_FILES['banner_image']['error'] == 0) {

                $target_paths = '../admin/files/pages/feature_image/';
                $target_paths = '../admin/files/pages/banner/';


                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $ext = end(explode('.', $_FILES['banner_image']['name']));
                $bannerName = $timestamp . '_' . basename($_FILES['banner_image']['name']);
                $target_path = $target_paths . $bannerName;
                $target_path_thumb = $target_paths . 'thumb_' . $bannerName;
                move_uploaded_file($_FILES['banner_image']["tmp_name"], $target_path);
                ak_img_resize($target_path, $target_path_thumb, 250, 250, $ext);
                $_POST['banner_image'] = $bannerName;
            }
        }
        $fields['banner_image'] = trim($_POST['banner_image']);
        if ($_POST['banner_image'] == '') {
            $fields['banner_image'] = $imageName;
        }
        if (isset($_FILES['article_pdf']['name'])) {


            if ($_FILES['article_pdf']['error'] == 0) {

               // $target_paths = '../admin/files/pages/article_pdf/';
                $target_paths = '../admin/files/pages/article_pdf/';


                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $ext = end(explode('.', $_FILES['article_pdf']['name']));
                $articleName =basename($_FILES['article_pdf']['name']);
                $target_path = $target_paths . $articleName;
                $target_path_thumb = $target_paths . 'thumb_' . $articleName;
                move_uploaded_file($_FILES['article_pdf']["tmp_name"], $target_path);
                $_POST['article_pdf'] = $articleName;
            }
        }
        $fields['article_pdf'] = trim($_POST['article_pdf']);
         if ($_POST['article_pdf'] == '') {
            $fields['article_pdf'] = $article_pdf;
        }
// prd($fields);
        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('pages'), $fields, $where);

        if ($update_result) {
// Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
//ends here
            redirect_to(MAIN_PATH . "/pages.php?pages");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div style="margin-top: 10px;">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "pages.php?pages" ?>">Manage Article</a>>><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
            ?>
        </div>
    </div>
    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Pages</h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"><?php
            if (isset($error)) {
//                echo '<div style="color:#FF0000">' . $errors . '</div>';
            }
            ?></p>
        <p class="clearfix"></p>
        <form name="addPage" id="addPage"  enctype="multipart/form-data"  method="POST" action=""  class="form-horizontal" role="form">
            <div class="form-group">
                <label for="page_title" class="col-sm-2 control-label">Page Title<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="page_title" id="page_title" minlength="2" maxlength="50" value="<?php echo isset($page_title) ? stripslashes($page_title) : ''; ?>"/>
                </div>
            </div>
             <div class="form-group">
                <label for="page_title" class="col-sm-2 control-label">Url Link<span class="redCol"></span></label>
                <div class="col-sm-8">
                    <input type="text"  class="form-control" name="url" id="url"   value="<?php echo isset($url) ? stripslashes($url) : ''; ?>"/>
                </div>
            </div> 
<!--            <div class="form-group">
                <label for="url" class="col-sm-2 control-label">Url<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="url" id="url"  value="<?php echo isset($url) ? stripslashes($url) : ''; ?>"/>
                </div>
            </div>
                            <div class="form-group">
                                <label for="banner_image" class="col-sm-2 control-label">Upload Banner<span class="redCol">* </span></label>
                                <div class="col-sm-offset-2 col-sm-8">
            <?php // if (file_exists(DOCUMENT_PATH . 'admin/files/pages/feature_image/' . $image_path) && $image_path != '' && key($_REQUEST) == 'edit') { ?>
                                                                        <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/pages/feature_image/' . $image_path ?>">
            <?php //} ?>
                                </div>
                            </div>            -->


            <div class="form-group">
                <label for="image_path" class="col-sm-2 control-label">Upload Image<span class="redCol"></span></label>
                <div class="col-sm-8">
                    <input type="file" name="banner_image" id="image_upload">
                </div>
            </div>
            
            <div class="form-group">
                <!--<label for="banner_image" class="col-sm-2 control-label">Upload Banner<span class="redCol">* </span></label>-->
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if ($type == 0) {
                        if (file_exists(ADMIN_PATH . 'files/pages/banner/' . $banner_image) && $banner_image != '' && key($_REQUEST) == 'edit') {
                            ?>
                            <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/pages/banner/' . $banner_image ?>">
                            <?php
                        }
                    } else {
                        if (file_exists(ADMIN_PATH . 'files/pages/feature_image/' . $banner_image) && $banner_image != '' && key($_REQUEST) == 'edit') {
                            ?>
                            <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/pages/feature_image/' . $banner_image ?>">
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label for="article_path" class="col-sm-2 control-label">Upload Article PDF <span class="redCol"></span></label>
                <div class="col-sm-8">
                    <input type="file" name="article_pdf" id="pdf_upload">
                    <label> Uploaded File:<?php if($article_pdf){ echo $article_pdf;} else {echo '';} ?></label>
                </div>
            </div>
            <div class="form-group">
                <label for="article_type" class="col-sm-2 control-label"> Article Type <span class="redCol"></span></label>
                <div class="col-sm-8">
                   <select name= "article_type">
                       <option value="">Choose article type</option>
                       <option value="Checklist"<?php if($record['article_type']== 'Checklist'){ echo 'selected'; }?>>Checklist</option>
                       <option value="Free Report"<?php if($record['article_type']== 'Free Report'){ echo 'selected'; }?> >Free Report</option>
                       <option value="Article for only $4.95" <?php if($record['article_type']== 'Article for only $4.95'){ echo 'selected'; }?> >Article for only $4.95</option>
                   </select>
                </div>
            </div>
            
            <div class="form-group">
                <label for="meta_title" class="col-sm-2 control-label">Meta Title<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="meta_title" minlength="2" maxlength="50" id="meta_title"  value="<?php echo isset($meta_title) ? stripslashes($meta_title) : ''; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="keyword" class="col-sm-2 control-label">Meta Keyword<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="keyword" id="keyword" minlength="2" maxlength="50" value="<?php echo isset($keyword) ? stripslashes($keyword) : ''; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">Meta Description<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <textarea  rows="4" cols="50" minlength="4" maxlength="500" class="required form-control" name="description" id="description"  ><?php echo isset($description) ? stripslashes($description) : ''; ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="short_description" class="col-sm-2 control-label">Short Description<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <textarea  rows="4" cols="50" class="required form-control" name="short_description" id="short_description" minlength="4" maxlength="200"  ><?php echo isset($short_description) ? stripslashes($short_description) : ''; ?></textarea>
                </div>
            </div>
            <?php
            if (isset($_POST['content'])) {

                $content = $_POST['content'];
            } elseif (isset($content)) {
                $content = $content;
            } else {
                'empty';
                $content = '';
            }
            ?>
            <div class="form-group">
                <label for="content" class="col-sm-2 control-label">Content<span class="redCol">* </span></label>
                <div class="col-sm-8">
                    <?php
                    $oFCKeditor = new FCKeditor('content');
                    $oFCKeditor->BasePath = './fckeditor/';
                    $oFCKeditor->Height = '400px';
                    $oFCKeditor->Value = "$content";
                    $oFCKeditor->class = "form-control";
                    $oFCKeditor->Create();
                    ?>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>





<script type="text/javascript">
    $(document).ready(function() {

        $('input[type="submit"]').focus();
        $('.imageupload').hide();

        $('#type').change(function() {
            var value = $(this).val();

            if (parseInt(value) == 1) {
                $('.imageupload').show();
                $('#image_upload').addClass('required');
            } else {
                $('.imageupload').hide();
                $('#image_upload').removeClass('required');
            }
        });

<?php if ((isset($type) && $type == 1) || (isset($_POST['type']) && $_POST['type'] == 1)) {
    ?>
            $('.imageupload').show();
<?php }
?>
        $('#addPage').validate({
            rules: {
                image_path: {
                    extension: "jpeg|png|gif|jpg"
                },
                banner_image: {
                    extension: "jpeg|png|gif|jpg"
                },
                article_pdf: {
                    extension: "pdf"
                }
            },
            messages: {
                image_path: {
                    extension: "Please upload valid file formats"
                },
                banner_image: {
                    extension: "Please upload valid file formats"
                },
                article_pdf: {
                    extension: "Please upload valid file formats"
                },
            }
        });


    });




</script>

