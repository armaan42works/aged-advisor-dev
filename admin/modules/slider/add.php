<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
    global $db;
    $description = '';
    $image = '';
    include_once("./fckeditor/fckeditor.php");


    if (isset($submit) && $submit == 'Submit') {

        $fields_array = slider();
        $response = Validation::_initialize($fields_array, $_POST);


        if (isset($_FILES['image']['name'])) {

            if ($_FILES['image']['error'] == 0) {


                $image_info = getimagesize($_FILES["image"]["tmp_name"]);
                $image_width = $image_info[0];
                $image_height = $image_info[1];

                if (($image_width != 318) || $image_height != 321) {
                    $response['size'] = 'Please upload image of dimension 318*321.';
                }
            }
        }
        if (empty($response['size']) && isset($response['valid']) && $response['valid'] > 0) {
            if (isset($_FILES['image']['name'])) {

                if ($_FILES['image']['error'] == 0) {

                    $ext = end(explode('.', $_FILES['image']['name']));
                    $target_pathL = '../admin/files/slider/';
                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $logoName = $timestamp . '_' . imgNameSanitize(basename($_FILES['image']['name']), 20);
                    list($width, $height, $type, $attr) = getimagesize("img/flag.jpg");
                    $target_path = $target_pathL . $logoName;
//                    $target_path_slider = $target_pathL . 'slider_' . $logoName;
                    $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                    move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                    ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
//                    ak_img_resize($target_path, $target_path_slider, 318, 380, $ext);
                    $_POST['image'] = $logoName;
                }
            }
            $getMax = "SELECT max(order_by) as count from " . _prefix("sliders") . " WHERE status = 1 && deleted = 0";
            $resMax = $db->sql_query($getMax);
            $dataMax = mysqli_fetch_assoc($resMax);
            $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
            $fields = array(
                'title' => trim($title),
                'description' => trim($_POST['description']),
                'image' => $_POST["image"],
                'created' => date('Y-m-d h:i:s', time())
            );


            $insert_result = $db->insert(_prefix('sliders'), $fields);
            if ($insert_result) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/slider.php?manage");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                if ($message != '1.' && $message != '.') {
                    $errors .= $message . "<br>";
                }
            }
        }
    }
    if (key($_REQUEST) == 'edit') {
        $id = $_GET['id'];
        $sql_query = "SELECT * FROM " . _prefix("sliders") . " where md5(id)='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        if (count($records)) {
            foreach ($records as $record) {

                $title = $record['title'];
                $description = $record['description'];
                $image = $record['image'];
            }
        }
    }
    if (isset($update) && $update == 'Update') {





        $fields_array = slider();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($_FILES['image']['name'])) {

            if ($_FILES['image']['error'] == 0) {


                $image_info = getimagesize($_FILES["image"]["tmp_name"]);
                $image_width = $image_info[0];
                $image_height = $image_info[1];

                if (($image_width != 318) || $image_height != 321) {
                    $response['size'] = 'Please upload image of dimension 318*321.';
                }
            }
        }

        if (empty($response['size']) && isset($response['valid']) && $response['valid'] > 0) {
            if (isset($_FILES['image']['name'])) {

                if ($_FILES['image']['error'] == 0) {
//                    prd($image_info);
                    echo strrpos(basename($_FILES['image']['name']), '.', -1);
                    $ext = end(explode('.', $_FILES['image']['name']));
                    $target_pathL = '../admin/files/slider/';

                    $date = new DateTime();
                    $timestamp = $date->getTimestamp();
                    $logoName = $timestamp . '_' . imgNameSanitize(basename($_FILES['image']['name']), 20);
                    $target_path = $target_pathL . $logoName;
                    $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                    move_uploaded_file($_FILES['image']["tmp_name"], $target_path);
                    ak_img_resize($target_path, $target_path_thumb, 50, 50, $ext);
                    $_POST['image'] = $logoName;
                }
            }
            $title = trim($_POST["title"]);
            $description = trim($_POST["description"]);
            $image = $_POST["image"];
            $fields = array('title' => trim($title),
                'description' => trim($description),
            );
            if ($_POST['image'] != '') {
                $fields['image'] = $image;
            }

            $where = "where md5(id)= '$id'";
            $update_result = $db->update(_prefix('sliders'), $fields, $where);
            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('UPDATE'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/slider.php?manage");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                if ($message != '1.' && $message != '.') {
                    $errors .= $message . "<br>";
                }
            }
        }
    }
?>

<div class="row">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?><br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
        ?>
    </div>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "slider.php?manage" ?>"> Manage Slider Images</a>>> <?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?></li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> Slider</h2>
        </div>

        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="addSiler" id="addSiler" action="" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">Title<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="title" minlength="2" maxlength="50" id="title"  value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-sm-2 control-label">Description<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <textarea rows="4" cols="50" minlength="4" maxlength="500" name="description" class="required form-control" id=""><?php
                        if (isset($description)) {
                            echo stripslashes($description);
                        }
                    ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="image" class="col-sm-2 control-label">Upload image<span class="redCol">*</span><h2 class="redCol small pull-left"  style="float:left;margin-top: 2px;">[Image should be 318x321 pixel's size]</h2> </label>
            <div class="col-sm-8">
                <input type="file" name="image" id="image" class="required">
            </div>
        </div>
        <div class="form-group">
            <!--<label for="image" class="col-sm-2 control-label">Slider image<span class="redCol">* </span></label>-->
            <div class="col-md-2"></div>
            <div class="col-sm-8">
                <?php if (file_exists(DOCUMENT_PATH . 'admin/files/slider/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
                        <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/slider/' . $image ?>"/>
                    <?php } ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                ?>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('input[type="submit"]').focus();
    });
    $('#addSiler').validate();
<?php if (file_exists(DOCUMENT_PATH . 'admin/files/slider/' . $image) && $image != '' && key($_REQUEST) == 'edit') { ?>
            $('#image').removeClass('required');
    <?php } ?>
</script>



