<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
    /*     * ***************************************
     * Add Admin Quotes
     * *************************************** */

    global $db;
    $yes = '';
    $no = "checked = 'checked'";
    if (isset($id) && $id != '') {
        $getQuote = "SELECT  dq.id, du.name, du.business, dq.created, dq.coupon_id, dq.client_id FROM " . _prefix("quotes") . " as dq "
                . "INNER JOIN " . _prefix("users") . " as du ON dq.client_id = du.id  "
                . "WHERE md5(dq.id) = '$id' ";
        $res = $db->sql_query($getQuote);
        $quoteDetail = $db->sql_fetchrow($res);
    }
    if (isset($submit) && $submit == 'submit') {
        if ($_FILES['file_name']['error'] == 0) {

            $fileData = pathinfo($_FILES["file_name"]["name"]);
            $target_path = '../admin/files/quote_files/';
            $imageName = time() . '_' .imgNameSanitize(basename($_FILES['file_name']['name']),20);
            $target_path = $target_path . $imageName;

            move_uploaded_file($_FILES["file_name"]["tmp_name"], $target_path);
        }
        $fields = array(
            'quote_title' => $quote_title,
            'quotre_desc' => $quotre_desc,
            'filename' => $imageName,
            'subtotal' => $subtotal,
            'total' => $total,
            'quotes_id' => $quoteDetail['id'],
            'status' => 0
        );
        if ($total == $subtotal) {
            $fields['coupon_assigned'] = 0;
        } else {
            $fields['coupon_assigned'] = 1;
        }
        if (isset($coupon_assigned) && $coupon_assigned == 0) {
            $fields['coupon_assigned'] = 1;
            $fields['coupon_id'] = $coupon_gift;
            $couponId = "Select code from " . _prefix("coupons") . " WHERE id= '$coupon_gift'";
            $res = $db->sql_query($couponId);
            $couponCode = $db->sql_fetchrow($res);
            $coupon = array(
                'coupon_id' => $couponCode['code']
            );
            $where = " where id= '{$quoteDetail['id']}'";
            $inresult = $db->update(_prefix('quotes'), $coupon, $where);
        }
        $insert_result = $db->insert(_prefix('admin_quotes'), $fields);

        if ($insert_result) {
            redirect_to(MAIN_PATH . "/project.php?quote");
        }
    }
    if (isset($update) && $update == 'update') {

        $where = "where md5(id)= '$rec'";
        $fields = array(
            'quote_title' => $quote_title,
            'quotre_desc' => $quotre_desc,
            'subtotal' => $subtotal,
            'total' => $total,
            'quotes_id' => $quoteDetail['id'],
            'status' => 0,
            'coupon_assigned' => 0,
            'coupon_id' => '',
            'modified' => 'now()'
        );
        if ($coupon_assigned == 0) {
            $fields['coupon_assigned'] = 1;
            $fields['coupon_id'] = $coupon_gift;
        }
        if ($_FILES['file_name']['error'] == 0) {

            $fileData = pathinfo($_FILES["file_name"]["name"]);
            $target_path = '../admin/files/quote_files/';
            $imageName = time() . '_' . imgNameSanitize(basename($_FILES['file_name']['name']),20);
            $target_path = $target_path . $imageName;

            move_uploaded_file($_FILES["file_name"]["tmp_name"], $target_path);
            $fields['filename'] = $imageName;
        }

        $insert_result = $db->update(_prefix('admin_quotes'), $fields, $where);
        redirect_to(MAIN_PATH . "/project.php?quote");
    }
    if (isset($rec) && $rec != '') {
        $getAdminQuote = "SELECT daq.* , dq.payment_status FROM " . _prefix("admin_quotes") . " as daq "
                . "LEFT JOIN " . _prefix("quotes") . " as dq ON dq.id = daq.quotes_id "
                . "WHERE md5(daq.id) = '$rec'";
        $resGetAdminQuote = $db->sql_query($getAdminQuote);
        $dataGetAdminQuote = $db->sql_fetchrow($resGetAdminQuote);
        if ($dataGetAdminQuote['coupon_assigned'] == 1) {
            $yes = "checked = 'checked'";
            $no = '';
            $getCoupon = "select * from " . _prefix("coupons") . " where id = '{$dataGetAdminQuote['coupon_id']}'";
            $resGetCoupon = $db->sql_query($getCoupon);
            $dataGetCoupon = $db->sql_fetchrow($resGetCoupon);
            $amount = 'Amount :- $' . $dataGetCoupon['value'] . '<br>  Expiration Date : ' . date('M d, Y', strtotime($dataGetCoupon['expire_date']));
        } else if ($dataGetAdminQuote['coupon_assigned'] == 0) {
            $yes = '';
            $no = "checked = 'checked'";
            $amount = '';
        }
    }
?>  
<form name="addAdminQuotes" id="addAdminQuotes" action="" method="post" enctype="multipart/form-data">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>

            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "project.php?quote" ?>">Manage Quotes</a>>>Detailed Quotes</li>
                        </ul>
                    </div>
                </td>
            </tr>

        <tbody>
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                           width="100%">

                        <tbody>

                        <tbody>
                            <tr>
                                <td colspan="2" class="bdr" height="25">

                                    <h2>Submit Detailed Quote</h2><span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>

                                </td>
                            </tr>

                        </tbody>


                        <tr>
                            <td class="marginleft" align="right" height="26"> Quote request by :</td>
                            <td class="marginleft"><div class="input text">
                                    <?php echo stripslashes($quoteDetail['name']); ?>
                                </div></td>
                        </tr>
                        <tr>
                            <td class="marginleft" align="right" height="26"> Submitted On :</td>
                            <td class="marginleft"><div class="input text">
                                    <?php echo date('M d, Y H :i', strtotime($quoteDetail['created'])); ?>
                                </div></td>
                        </tr>
                        <tr>
                            <td class="marginleft" align="right" height="26"> Company :</td>
                            <td class="marginleft"><div class="input text">
                                    <?php echo stripslashes($quoteDetail['business']); ?>
                                </div></td>
                        </tr>
                        <tr class="business">

                            <td class="marginlef" align="right" height="26" width="31%">Quote Title: <span
                                    class="redCol">* </span></td>
                            <td class="marginleft" width="69%">
                                <div class="input text">
                                    <textarea id="quote_title" cols="40" name="quote_title" class="required"><?php echo isset($dataGetAdminQuote['quote_title']) ? stripslashes($dataGetAdminQuote['quote_title']) : ''; ?></textarea>
                                </div></td>
                        </tr>

                        <tr>
                            <td class="marginleft" align="right" height="26"> Quote Description: <span
                                    class="redCol">* </span></td>
                            <td class="marginleft"><div class="input text">
                                    <textarea id="quotre_desc" cols="40" rows="10" name="quotre_desc" class="required"><?php echo isset($dataGetAdminQuote['quotre_desc']) ? stripslashes($dataGetAdminQuote['quotre_desc']) : ''; ?></textarea>
                                </div></td>
                        </tr>
                        <tr>
                            <td class="marginleft" align="right" height="26"></td>
                            <td class="marginleft"><div class="input text" >

                                    <?php
                                        if (file_exists(DOCUMENT_PATH . 'admin/files/quote_files/' . $dataGetAdminQuote['filename']) && $dataGetAdminQuote['filename'] != '') {
                                            ?>
                                            <span><a href="<?php echo HOME_PATH_URL ?>download.php?file=<?php echo $dataGetAdminQuote['filename'] ?>" class="getFile" id="<?php echo $dataGetAdminQuote['filename'] ?>">Uploaded Document</a></span>
                                            <span><a href="javascript:void(0)" class="removeDownload" id="<?php echo $dataGetAdminQuote['id']; ?>"><img src="<?php echo ADMIN_IMAGE ?>li_delete.png" title="Delete Document" ></a></span>
                                        <?php } else { ?>
                                            <input type="file" name="file_name" id="file_name">
                                        <?php }
                                    ?>
                                </div>
                                <?php if ($dataGetAdminQuote['filename'] == '') {
                                        ?>
                                        <div style="color: #757575;font-family: 'PT Sans';font-size: 11px;font-style: italic;">Note* : Please upload pdf, doc, docx, ppt, pptx, pps, ppsx, odt, xls or xlsx format file.</div>
                                    <?php }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <td class="marginleft" align="right" height="26">Subtotal($): <span
                                    class="redCol">* </span></td>
                            <td class="marginleft"><div class="input text">
                                    <input type="text" name="subtotal" id="subtotal" maxlength="50" size="30" class="registrationTextbox required number" value="<?php echo isset($dataGetAdminQuote['subtotal']) ? stripslashes($dataGetAdminQuote['subtotal']) : ''; ?>"/>
                                    <div style="color: #757575;font-family: 'PT Sans';font-size: 11px;font-style: italic;"><?php echo$quoteDetail['coupon_id'] != '' ? 'Please enter amount more than the Coupon Value' : ''; ?></div>
                                </div></td>
                        </tr>
                        <?php
                            if ($quoteDetail['coupon_id'] == '') {
                                $where = 'Where coupon_id IS NOT NULL';
                                if (isset($dataGetAdminQuote['coupon_id']) && $dataGetAdminQuote['coupon_id'] != '') {
                                    $where .= "&& coupon_id != {$dataGetAdminQuote['coupon_id']}";
                                }
                                $getCoupon = "SELECT id, code,subtype,  value FROM " . _prefix("coupons") . " WHERE expire_date >= now() && status = 1 && id NOT IN (SELECT coupon_id from " . _prefix("quotes") . " where coupon_id is not null) && id NOT IN(SELECT coupon_id from " . _prefix("admin_quotes") . " $where)";
                                $resGetCoupon = mysqli_query($db->db_connect_id,$getCoupon);
                                ?>
                                <tr>
                                    <td class="marginleft" align="right" height="26">Would you like to offer a client a discount Coupon/Gift Card ?</td>
                                    <td class="marginleft"><div class="input text">
                                            <input type="radio" name="coupon_assigned" id="0" value="0" class="coupon" <?php echo $yes ?>><label for="0">Yes</label>
                                            <input type="radio" name="coupon_assigned" id="1" value="1" class="coupon" <?php echo $no; ?>><label for="1">No</label>
                                        </div></td>
                                </tr>
                                <tr class="code" style="display:none;">
                                    <td class="marginleft" align="right" height="26"> Coupon/Gift Code :</td>
                                    <td class="marginleft"><div class="input text">
                                            <select id="coupon_gift" name="coupon_gift">
                                                <?php
                                                $option = '<option value="">------- Select Coupon/Gift Code------</option>';
                                                while ($row = mysqli_fetch_assoc($resGetCoupon)) {
                                                    $select = $row['id'] == $dataGetAdminQuote['coupon_id'] ? 'selected' : '';
                                                    $option .= '<option value="' . $row['id'] . '" ' . $select . '>' . $row['code'] . '</option>';
                                                }
                                                echo $option;
                                                ?>
                                            </select>
                                        </div><span class="amount"><?php echo $amount; ?></span></td>
                                </tr>

                                <?php
                            } else {
                                $getCoupon = "SELECT code, subtype, value, expire_date FROM " . _prefix("coupons") . " WHERE code= '{$quoteDetail['coupon_id']}'";
                                $resCoupon = $db->sql_query($getCoupon);
                                $couponDetail = $db->sql_fetchrow($resCoupon);
                                ?>
                                <tr>
                                    <td class="marginleft" align="right" height="26"> Code :</td>
                                    <td class="marginleft"><div class="input text">
                                            <?php echo $couponDetail['code']; ?>
                                        </div></td>
                                </tr>
                                <tr>
                                <input type="hidden" id="userCoupon" value="<?php echo $couponDetail['value']; ?>">
                                <td class="marginleft" align="right" height="26"> Value(<?php echo $couponType[$couponDetail['subtype']] ?>) :</td>
                                <td class="marginleft"><div class="input text">
                                        <?php echo $couponDetail['value']; ?>
                                    </div></td>
                    </tr>
                    <tr>
                        <td class="marginleft" align="right" height="26"> Expiration Date :</td>
                        <td class="marginleft"><div class="input text">
                                <?php
                                echo date('M d, Y', strtotime($couponDetail['expire_date']));
                                echo "<br>";
                                echo date('Y-m-d', strtotime($couponDetail['expire_date'])) <= date('Y-m-d') && $dataGetAdminQuote['payment_status'] == 0 ? '<label class="redCol">Expired</label>' : '';
                                ?>
                            </div></td>
                    </tr>
                    <?php
                }
            ?>
            <tr>
                <td class="marginleft" align="right" height="26">Total($): <span
                        class="redCol">* </span></td>
                <td class="marginleft"><div class="input text">
                        <input type="text" name="total" id="total" maxlength="50" size="30" class="registrationTextbox required number" value="<?php echo date('Y-m-d', strtotime($couponDetail['expire_date'])) <= date('Y-m-d') && $dataGetAdminQuote['payment_status'] == 0 ? $dataGetAdminQuote['subtotal'] : $dataGetAdminQuote['total']; ?>" readonly/>
                    </div></td>
            </tr>


            <tr>
                <td class="marginleft r" align="right" height="26" width="31%"></td>
                <td class="r marginleft" width="69%">
                    <?php if (isset($rec) && $rec != '') { ?>
                            <input type="submit" value="update" name="update" class="submit" class="submit_btn">
                        <?php } else { ?>
                            <input type="submit" value="submit" name="submit" class="submit" class="submit_btn">
                        <?php } ?>
                </td> </tr>


        </tbody>

    </table>
</td>
</tr>
</tbody></tbody></table></form>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>
<script type="text/javascript">
    $(document).ready(function() {
<?php if (isset($dataGetAdminQuote['coupon_id']) && $dataGetAdminQuote['coupon_id'] == '') {
        ?>
                $('.code').show();
                $('.amount').show();
    <?php }
?>



        $('#expire_date').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            minDate: '0'
        });

        $('#subtotal').keyup(function() {
            var value = $(this).val();
            var userCoupon = $('#userCoupon').val();
            if (value == '') {
                value = 0;
            }
            if (userCoupon != '' && parseInt(value) > parseInt(userCoupon)) {
                value = value - userCoupon;
            } else {
                value = value;
            }
            $('#total').val(value);
            $('.amount').html('');
            $('#coupon_gift').val('');
        });

        $('.coupon').click(function() {
            var value = $(this).val();
            if (parseInt(value) == 0) {
                $('.code').show();
                $('#code').addClass('required');
            } else {
                $('.code').hide();
                $('#code').removeClass('required');
                $('#code').val('');
                $('.amount').html('');
                $('#coupon_gift').val('');
                $('#total').val($('#subtotal').val());
            }
        });
        $('#coupon_gift').on('change', function() {
            var subtotal = $('#subtotal').val();
            if (subtotal == '') {
                alert('Please enter Subtotal amount.');
                return false;
            }
            var values = $(this).val();
            if (values == "") {
                $('.amount').hide();
                $('#total').val(subtotal);
                return false;
            }
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: {action: 'checkCoupon', value: values},
                success: function(response) {
                    var rec = response.split('#@#');
                    var type = rec[0];
                    var value = rec[1];
                    if (parseInt(type) == 0) {
                        $('.amount').show();
                        if (parseInt(subtotal) <= parseInt(value)) {
                            alert('The subtotal is less than coupon value');
                            $('#coupon_gift').val('');
                            $('.amount').html('');
                            $('#total').val(subtotal);
                            return false;
                        } else {
                            $('.amount').html('Amount :- $' + value + '<br> Expiration Date: ' + rec[2]);
                            var total = subtotal - value;
                            $('#total').val(total);
                        }
                    }
                    if (parseInt(type) == 1) {
                        $('.amount').show();
                        var total = (subtotal * value) / 100;
                        $('.amount').html('Amount :- $' + total + '<br> Expiration Date: ' + rec[2]);
                        $('#total').val(subtotal - total);
                    }
                }
            });
        });

        $('#addAdminQuotes').validate({
            rules: {
                file_name: {
                    extension: "pdf|doc|docx|ppt|pptx|pps|ppsx|odt|xls|xlsx"
                }
            },
            messages: {
                file_name: {
                    extension: "Please upload valid file formats"
                }
            }
        });



        $('.removeDownload').click(function() {
            var sure = confirm("Do you want to delete this file?");
            if (sure) {
                var id = $(this).attr('id');
                $.ajax({
                    type: 'POST',
                    url: 'ajax.php',
                    data: {action: 'deleteFileName', id: id},
                    success: function(response) {
                        if (parseInt(response) == 1) {
                            location.reload();
                        }
                    }
                });
            }


        });



    });


</script>
