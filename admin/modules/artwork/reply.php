<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
    global $db;
    $admin_description = '';
    if (key($_REQUEST) == 'reply') {
        $id = $_GET['id'];
        $sql_query = "SELECT admin_description, admin_returned FROM " . _prefix("artworks") . " where md5(id)='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        if (count($records)) {
            foreach ($records as $record) {
                $admin_description = $record['admin_description'];
                $admin_returned = $record['admin_returned'];
            }
        }
    }
    if (isset($Reply) && $Reply == 'Reply') {

        if (isset($_FILES['admin_returned']['name'])) {
            if ($_FILES['admin_returned']['error'] == 0) {
                $ext =  end(explode('.', $_FILES['admin_returned']['name']));
                $target_pathL = '../admin/files/artwork/';
                $date = new DateTime();
                $timestamp = $date->getTimestamp();
                $logoName = $timestamp . '_' . imgNameSanitize(basename($_FILES['admin_returned']['name']),20);
                $target_path = $target_pathL . $logoName;
                $target_path_thumb = $target_pathL . 'thumb_' . $logoName;
                move_uploaded_file($_FILES['admin_returned']["tmp_name"], $target_path);
                ak_img_resize($target_path, $target_path_thumb, 50, 50,$ext);
                $_POST['admin_returned'] = $logoName;
            }
        }
        $date1 = str_replace("/","-",$_POST['admin_expected_date']);
              $admin_expected_date=  date('Y-m-d', strtotime($date1));
        $admin_description = $_POST["admin_description"];
        $plan_type = $_POST["plan_type"];
        if (isset($_POST["admin_returned"])) {
            $admin_returned = $_POST["admin_returned"];
        }
        $fields = array(
            'admin_description' => trim($admin_description),
            'admin_returned' => trim($admin_returned),
            'admin_expected_date' => trim($admin_expected_date),
            'approve_status' => 1,
        );
        $where = "where md5(id)= '$id'";
        $Reply_result = $db->Update(_prefix('artworks'), $fields, $where);
        if ($Reply_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "artwork.php?partwork&id=$pid");
        }
    }
?>


<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "artwork" ?>">Manage Artwork</a>>><a href="<?php echo HOME_PATH_URL . "artwork.php?partwork&id=".$pid; ?>">View Artwork</a>>> <?php echo (key($_REQUEST) == 'reply') ? 'Rply' : 'Add'; ?></li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Add Reply</h2>
        </div>
        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>

    <form method="POST" name="reply" id="reply" action=""  class="form-horizontal" role="form" enctype="multipart/form-data">
        
        <div class="form-group">
            <label for="admin_description" class="col-sm-2 control-label">Write Description<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <textarea name="admin_description" cols="50" rows="4" id="admin_description" class="required form-control"><?php echo isset($admin_description) ? $admin_description : ''; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label for="admin_expected_date" class="col-sm-2 control-label">Admin expected date<span class="redCol">*</span></label>
            <div class="col-sm-2">
                <input type="text" name="admin_expected_date" id="admin_expected_date" class="required form-control" value="<?php echo isset($admin_expected_date) ? $admin_expected_date : ''; ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="admin_returned" class="col-sm-2 control-label">Admin image<span class="redCol"><?php echo isset($admin_returned) ? '' : '*'; ?></span></label>
            <div class="col-sm-8">
                <input type="file" name="admin_returned" id="admin_returned" class="<?php echo isset($admin_returned) ? '' : 'required'; ?>">
            </div>
        </div>
        <div class="form-group">
            <!--<label for="image" class="col-sm-2 control-label">Logo image<span class="redCol">* </span></label>-->
            <div class="col-sm-offset-2 col-sm-8">
                <?php if (file_exists(DOCUMENT_PATH . 'admin/files/artwork/' . $admin_returned) && $admin_returned != '' && key($_REQUEST) == 'reply') { ?>
                        <img width="100px;" height="100px;" title="feature" src="<?php echo MAIN_PATH . '/files/artwork/' . $admin_returned ?>"/>
                    <?php } ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                    if (key($_REQUEST) == 'edit') {
                        ?>
                        <input type="submit" value="Reply" name="Reply" class="submit_btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">
                        <!--<input type="submit" value="update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">-->

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="Reply" name="Reply" class="submit_btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">
                        <!--<input type="submit" value="submit" name="submit" class="submit_btn btn">-->
                        <?php
                    }
                ?>
            </div>
        </div>
    </form>
</div>

<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

<script type="text/javascript">
    $('#admin_expected_date').datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: '2010',
        minDate: 0
    });
$(document).ready(function() { $('input[type="submit"]').focus();});
    $('#reply').validate();
</script>