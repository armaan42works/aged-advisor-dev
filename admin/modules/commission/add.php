<?php
    /*
     * Objective  : Add & Manage the default Commission
     * Filename : manage.php
     * Created By: Yogesh Joshi <yogesh.joshi@ilmp-tech.com>
     * Created On : 13 August 2014
     * Modified On : 19 August 2014
     */
?>
<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
    global $db;
    $limit_type_1 = "checked = 'checked'";
$limit_type_0 = '';

    if ((key($_REQUEST)) == 'add') {

        $sql_query = "SELECT * FROM  " . _prefix("commision") . " where sp_id='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        $num = $db->sql_numrows($res);
        if (count($records)) {

            foreach ($records as $record) {

                $starting_commision = $record['starting_commision'];
                $over_head = $record['over_head'];
                $depreciation_coefficient = $record['depreciation_coefficient'];
                $commision_limit = $record['commision_limit'];
                $max_project = $record['max_project'];
                $threshold_price = $record['threshold_price'];
                $bonus = $record['bonus'];
                $threshold_increment = $record['threshold_increment'];
                $threshold_count = $record['threshold_count'];
            }
            $limit_type_0 = $limit_type == 0 ? "checked = 'checked'" :'';
        $limit_type_1 = $limit_type == 1 ? "checked = 'checked'" :'';
        }
    }

    if (isset($submit) && $submit == 'Submit') {
        $fields_array = add_commision();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            $fields = array(
                'starting_commision' => $_POST['starting_commision'],
                'over_head' => $_POST['over_head'],
                'depreciation_coefficient' => $_POST['depreciation_coefficient'],
                'limit_type' => $_POST['limit_type'],
                'max_project' => $_POST['max_project'],
                'threshold_price' => $_POST['threshold_price'],
                'bonus' => $_POST['bonus'],
                'threshold_increment' => $_POST['threshold_increment'],
                'threshold_count' => $_POST['threshold_count']
            );


            if ($num == 0) {
                $fields['sp_id'] = $id;
                $update_result = $db->insert(_prefix('commision'), $fields);
            } else {
                $where = "where sp_id= '$id'";
                $update_result = $db->update(_prefix('commision'), $fields, $where);
            }

            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/position.php?application");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
    $getName = "SELECT name from " . _prefix("users") . " WHERE md5(id) = '$id'";
    $resGetName = $db->sql_query($getName);
    $dataGetName = $db->sql_fetchrow($resGetName);
?>

<form name="AddCommision" id="AddCommision" action="" method="post">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>

            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "commision.php?manage" ?>">Manage Commision</a>>>Add</li>
                        </ul>
                    </div>
                </td>
            </tr>
        <div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
            <?php
                if (isset($error)) {
                    echo '<div style="color:#FF0000">' . $errors . '</div>';
                }
                if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                    echo $_SESSION['msg'];
                    unset($_SESSION['msg']);
                }
            ?>
        </div>  
        <tbody>
            <tr>
                <td>
                    <table width="100%">

                        <tbody>
                            <tr>
                                <td colspan="2" class="bdr" height="25">

                                    <h2>Add Commission</h2><span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>

                                </td>
                            </tr>
                            <tr>

                                <td class="marginleft" align="right" height="26" width="31%">SalesPerson Name: <span
                                        class="redCol">* </span></td>
                                <td class="marginleft" width="69%">
                                    <div class="input text">
                                        <?php echo $dataGetName['name']; ?>
                                    </div>
                                </td>
                            </tr>
                             <tr>

                                <td class="marginleft" align="right" height="26" width="31%">Starting Commission(%): <span
                                        class="redCol">* </span></td>
                                <td class="marginleft" width="69%">
                                    <div class="input text">
                                        <input type="text" name="starting_commision" id="" class="required number" value="<?php
                                            if (isset($starting_commision)) {
                                                echo stripslashes($starting_commision);
                                            }
                                        ?>" />
                                    </div>
                                </td>
                            </tr>
                        <td class="marginleft" align="right" height="26" width="31%">Take of percentage or overhead fee(%): <span

                                class="redCol">* </span></td>
                        <td class="marginleft" width="69%">
                            <div class="input text">
                                <input type="text" name="over_head" id="" class="required number" value="<?php
                                    if (isset($over_head)) {
                                        echo stripslashes($over_head);
                                    }
                                ?>" />
                            </div>
                        </td>
            </tr>
            <tr>

                <td class="marginleft" align="right" height="26" width="31%">Depreciation Coefficient: <span

                        class="redCol">* </span><br><span style="font-family:'PT Sans';color:#757575;font-style:italic;font-size:12px;">(must be a negative number)</span></td>
                <td class="marginleft" width="69%">
                    <div class="input text">
                        <input type="text" name="depreciation_coefficient" id="" class="required number" value="<?php
                            if (isset($depreciation_coefficient)) {
                                echo stripslashes($depreciation_coefficient);
                            }
                        ?>" />
                    </div>
                </td>
            </tr>
            <tr><td colspan="2"><h1>Extra Commission Limit on:</h1></td><td></td></tr>
            <tr>
                <td class="marginleft" align="right" height="26"><input type="radio" name="limit_type" id="0" value="0" <?php echo $limit_type_0 ?>><label for="0">Subsequent projects by existing client(%)</label></td>
                <td class="marginleft"><input type="radio" name="limit_type" id="1" value="1" <?php echo $limit_type_1 ?>><label for="1">Max Number of Projects per clients</label></td>
            </tr>
            <tr>
                <td class="marginleft" align="right" height="26"> <input type="text" name="max_project" id="" class="required number"  value="<?php
                            if (isset($max_project)) {
                                echo stripslashes($max_project);
                            }
                        ?>" /></td>
                <td class="marginleft"></td>
            </tr>

            <tr><td  colspan="2"><h1>Setting for ramped commission rate:</h1></td><td></td></tr>
            <tr>
                <td class="marginleft" align="right" height="26">First threshold to qualify for commission increase ($): <span
                        class="redCol">* </span></td>
                <td class="marginleft"><div class="input text">
                        <input type="text" name="threshold_price" id="" class="registrationTextbox required number" value="<?php
                            if (isset($threshold_price)) {
                                echo stripslashes($threshold_price);
                            }
                        ?>"/>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26">Bonus to earn after threshold is met ($) : <span
                        class="redCol">* </span></td>
                <td class="marginleft"><div class="input text">
                        <input type="text" name="bonus" id="bonus" class="registrationTextbox required number" value="<?php
                            if (isset($bonus)) {
                                echo stripslashes($bonus);
                            }
                        ?>"/>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26">First Commission increment if threshold is met(%):  <span
                        class="redCol">* </span></td>
                <td class="marginleft"><div class="input text">
                        <input type="text" name="threshold_increment" id="threshold_increment"  value="<?php
                            if (isset($threshold_increment)) {
                                echo stripslashes($threshold_increment);
                            }
                        ?>" class=" required number"/>
                    </div></td>
            </tr>

            <tr>
                <td class="marginleft" align="right" height="26">Max threshold to stop incrementing commission ($):  <span
                        class="redCol">* </span></td>
                <td class="marginleft"><div class="input text">
                        <input type="text" name="threshold_count" id="threshold_count" value="<?php
                            if (isset($threshold_count)) {
                                echo stripslashes($threshold_count);
                            } else {
                                echo 0;
                            }
                        ?>" class="required number" readonly/>
                    </div></td>
            </tr>




            <tr>
                <td class="marginleft r" align="right" height="26" width="31%"></td>
                <td class="r marginleft" width="69%">

                    <input type="submit" value="Submit" name="submit" class="submit_btn">

                </td> </tr>

            <!-- content area ends here --></td>
            </tr>
        </tbody>

    </table>
</td>
</tr>
</tbody></td></tr></tbody></table></form>

<script type="text/javascript">
    $(document).ready(function() {

        $('#AddCommision').validate();

$('#threshold_increment').keyup(function(){
   var max_number = $(this).val();
   var bonus = $('#bonus').val();
   if(bonus != '' && max_number != ''){
       var total = max_number*bonus;
       $('#threshold_count').val(total);
   } else {
       $('#threshold_count').val('0');
   }
});
    });

</script>
