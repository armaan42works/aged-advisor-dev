<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
global $db;
$description = '';
$image = '';
if (isset($submit) && $submit == 'submit') {
    $fields_array = location();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $getMax = "SELECT max(order_by) as count from " . _prefix("cities") . " WHERE status = 1 && deleted = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = mysqli_fetch_assoc($resMax);
        $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;
        $fields = array(
            'title' => trim($title),
        );

        $insert_result = $db->insert(_prefix('cities'), $fields);
        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "location.php?manageCity");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (key($_REQUEST) == 'editCity') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("cities") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {
        foreach ($records as $record) {
            $title = $record['title'];
        }
    }
}
if (isset($update) && $update == 'update') {

    $fields_array = location();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $title = $_POST["title"];
        $fields = array('title' => trim($title),
        );
        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('cities'), $fields, $where);
        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "location.php?manageCity");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div style="margin-top: 10px;">

    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
        <tbody>
            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "location.php?manage" ?>"> Manage City </a>>> <?php echo (key($_REQUEST) == 'editCity') ? 'Edit' : 'Add'; ?></li>
                        </ul>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6 success">
            <?php if (isset($error)) { ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
                </div>
                <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
                <?php
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                ?>
                <div class="errorForm" id="error" style="display: block;">
                    <img align="absmiddle" src="<?php echo HOME_PATH . 'images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
                </div>
                <?php
                unset($_SESSION['msg']);
            }
            ?>
        </div>
    </div>
    <div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
        <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
            <div class="col-sm-6">
                <h2 class=""><?php echo (key($_REQUEST) == 'edit') ? 'Edit' : 'Add'; ?> City</h2>
            </div>

            <div class="col-sm-6">
                <h2 style="float:right;" class="redCol small">* fields required</h2>
            </div>
        </div>
        <p class="clearfix"></p>
        <form name="addCity" id="addCity" action="" method="POST" class="form-horizontal" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">City Name<span class="redCol">*</span></label>
                <div class="col-sm-8">
                    <input type="text"  class="required form-control" name="title" id="title" minlength="2" maxlength="50" value="<?php echo isset($title) ? stripslashes($title) : ''; ?>"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    <?php
                    if (key($_REQUEST) == 'editCity') {
                        ?>
                        <input type="submit" value="update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                        <?php
                    } else {
                        ?>
                        <input type="submit" value="submit" name="submit" class="submit_btn btn">
                        <?php
                    }
                    ?>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type="submit"]').focus();
    });
    $('#addCity').validate();
</script>
