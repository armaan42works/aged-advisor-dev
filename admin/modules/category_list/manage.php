<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        var data = '&start_date=&search_type=&end_date=';
        changePagination(page, li, 'category_list', data); 
        $('#search_form').submit(function(e) {
            $('#search_form').validate();
            e.preventDefault();
            data = '&start_date=' + $('#start_date').val() + '&search_type=' + $('#search_type').val() + '&end_date=' + $('#end_date').val();
            changePagination(page, li, 'category_list', data);
        });
    });
</script>
<div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
    <?php
    if (isset($error)) {
        echo '<div style="color:#FF0000">' . $errors . '</div>';
    }
    if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    ?>
</div>
<style type="text/css">table.logoList td,table.logoList tr{vertical-align: middle;}</style>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%" class="logoList">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>> Manage Consumer Information</li>
                        <li class="pull-right "><div id="link"><a href="<?php echo HOME_PATH_URL; ?>category_list.php?add">Add/New</a></div></li>
                    </ul>
                </div>
            </td>
        </tr>       
        <tr><td>
                <table class="table table-striped table-hover">
                    <tbody  id="pageData">
                        <?php // echo '<div id="pageData"></div>'; ?>
                    </tbody>
                </table></td>
        </tr>
    </tbody>
</table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr>
</table>