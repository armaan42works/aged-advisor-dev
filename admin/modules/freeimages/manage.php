<?php error_reporting(E_ALL); ?>
<script type="text/javascript">
    $(document).ready(function() {   
        var page = 0;
        var li = '0_no';
        var data = '&start_date=&search_type=&end_date=';
        changePagination(page, li, 'freeimagesList', data);
        $('#search_form').submit(function(e) {
            $('#search_form').validate();
            e.preventDefault();
          if (($('#search_type').val() == 'approved') || ($('#search_type').val() == 'disapproved')) {
                var data = '&search_input=1&search_type=' + $('#search_type').val();
                
            } else {
                var data = '&search_input=' + $.trim($('#search_input').val()) + '&search_type=' + $('#search_type').val();
        //alert(data);   
        }
            changePagination(page, li, 'freeimagesList', data);
        });
    });
</script>
<div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
    <?php
    if (isset($error)) {
        echo '<div style="color:#FF0000">' . $errors . '</div>';
    }
    if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    ?>
</div>

<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Manage FreeImage</li>
                        <li class="pull-right "><div id="link"><a href="<?php echo HOME_PATH_URL; ?>freeimages.php?add">Add/New</a></div></li>

                    </ul>
                </div>
            </td>
        </tr>

        <tr>
            <!-- End here -->
            <?php
            $search_array = array('title' => 'Title');
            ?>
            <td class="sublinks" height="40" valign="bottom">
                <form id="search_form" method="post" action="">
                    <input type="text" name="search_input" id="search_input" value="<?php echo isset($search_input) ? $search_input : ''; ?>">
                    <select name="search_type" id="search_type">
                        <?php
                        $option = '<option value="">------ Choose Option-----</option>';
                        foreach ($search_array as $key => $value) {
                            $option .="<option value='$key'>$value</option>";
                        }
                        echo $option;
                        ?>
                    </select>
                    <input type="submit" value="Search" id="btnsearch">
                </form>
            </td>
        </tr>
    <tbody>
    
        <tr>
            <td>
                <table  class="table table-striped table-hover">
                    <tbody  id="pageData">
                        <?php // echo '<div id="pageData"></div>';?>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>
