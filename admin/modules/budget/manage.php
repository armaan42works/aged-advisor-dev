<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
global $db;
if (isset($submit) && $submit == 'submit') {
/// whenever there will be submit update no insert
    $fields_array = budget();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $fields = array(
            'amount' => $amount
        );

        $where = "where id= '1'";
        $update_result = $db->update(_prefix('budget'), $fields, $where);
        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/budget.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

if ((key($_REQUEST)) == 'manage') {
    $sql_query = "SELECT * FROM " . _prefix("budget") . " where id='1'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {

        foreach ($records as $record) {

            $amount = $record['amount'];
        }
    }
}
?>
<div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
    <?php
    if (isset($error)) {
        echo '<div style="color:#FF0000">' . $errors . '</div>';
    }
    if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    ?>
</div>  
<form name="form1" id="managebudget" action="" method="post">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>

            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "budget.php?manage" ?>">Manage Budget</a>>>Add</li>
                        </ul>
                    </div>
                </td>
            </tr>



        <tbody>
            <tr>
                <td>
                    <table  width="100%">

                        <tbody>


                            <tr>
                                <td colspan="2" class="bdr" height="25">

                                    <h2>Add Budget</h2><span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>

                                </td>


                            <tr>

                                <td style="align:left; width: 30%; height: 26;">Minimum Budget: <span class="redCol">* </span> </td>
                                <td>

                                    <input type="text" name="amount" class="number" id="amount" maxlength="50" value="<?php echo stripslashes($amount); ?>" />

                                </td> 

                            </tr>

                            <tr>
                                <td style="align:left; width: 30%; height: 26;"></td>
                                <td class="marginleft r" align="right" height="26" width="31%">


                                    <input type="submit" value="submit" name="submit" class="submit_btn"></td>
                            </tr>

                        </tbody>

                    </table>

                </td>
            </tr>
        </tbody> 
        </tbody>
    </table>
</form>
<script type="text/javascript">

    $('#managebudget').validate();
</script>