<div class="dashboard_buttons" id="cpanel" style="padding-top: 15px;">
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Supplier" href="<?php echo HOME_PATH_URL . "user.php?supplier"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/user.jpg'; ?>">
                <span>Manage Supplier</span>
            </a>
        </div>
    </div>
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Ads" href="<?php echo HOME_PATH_URL . "ads.php?manage"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/coupon.jpg'; ?>">
                <span>Manage Ads</span>
            </a>
        </div>
    </div>
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Blogs"  href="<?php echo HOME_PATH_URL . "blog.php?manage"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/blog.jpg'; ?>">
                <span>Manage Blogs</span>
            </a>
        </div>
    </div>
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Forum"  href="<?php echo HOME_PATH_URL . "forum.php?mange"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/forum.jpg'; ?>">
                <span>Manage Forum</span>
            </a>
        </div>
    </div>
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Artwork"  href="<?php echo HOME_PATH_URL . "artwork.php?mange"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/Quotes.jpg'; ?>">
                <span>Manage Artwork</span>
            </a>
        </div>
    </div>
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Company Logo"  href="<?php echo HOME_PATH_URL . "clogo.php?mange"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/logo.jpg'; ?>">
                <span>Manage Company Logo</span>
            </a>
        </div>
    </div>

    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Slider Images"  href="<?php echo HOME_PATH_URL . "slider.php?mange"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/slider.jpg'; ?>">
                <span>Manage Slider Images</span>
            </a>
        </div>
    </div>
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Ad Space"  href="<?php echo HOME_PATH_URL . "price.php?bprice"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/plan.jpg'; ?>">
                <span>Manage Ad Space</span>
            </a>
        </div>
    </div>

    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Services Categories"  href="<?php echo HOME_PATH_URL . "service.php?mange"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/service.jpg'; ?>">
                <span>Manage Services Categories</span>
            </a>
        </div>
    </div>
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Reviews"  href="<?php echo HOME_PATH_URL . "review.php?review"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/review.jpg'; ?>">
                <span>Manage Reviews</span>
            </a>
        </div>
    </div>
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Email Templates"  href="<?php echo HOME_PATH_URL . "email.php?manage"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . '/img/dashboard/email.jpg'; ?>">
                <span>Manage Email Templates</span>
            </a>
        </div>
    </div>
    <div class="icon-wrapper cpanel">
        <div class="icon">
            <a title="Manage Location"  href="<?php echo HOME_PATH_URL . "location.php?manageCity"; ?>">
                <img alt="" src="<?php echo MAIN_PATH . 'img/dashboard/location.png'; ?>">
                <span>Manage Location</span>
            </a>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".dashboard_buttons a").tooltip();
        });
    </script>
</div>
<style type="text/css">
    .cpanel div.icon, #cpanel div.icon {
        float: left;
        margin-bottom: 15px;
        margin-right: 15px;
        text-align: center;
        height: 140px;
        width: 125px;
    }
    .cpanel div.icon a, #cpanel div.icon a {
        background-color: #F4F4F4;
        background-position: -30px center;
        border: 1px solid #CCCCCC;
        border-radius: 5px;
        color: #565656;
        display: block;
        float: left;
        height: 140px;
        text-decoration: none;
        transition-duration: 0.8s;
        transition-property: background-position, -moz-border-radius-bottomleft, -moz-box-shadow;
        vertical-align: middle;
        width: 125px;
    }
    a:link {
        color: #025A8D;
        outline: medium none;
        text-decoration: none;
    }
    a, img {
        margin: 0;
        padding: 0;
    }
    #cpanel img, .cpanel img {
        margin: 0 auto;
        padding: 10px 0;
        width:100px;
        height:100px;
    }
    img {
        border: 0 none;
    }
    #cpanel span, .cpanel span {
        display: block;
        text-align: center;
        color: #025A8D;
    }

    #cpanel div.icon a:hover, #cpanel div.icon a:focus, #cpanel div.icon a:active, .cpanel div.icon a:hover, .cpanel div.icon a:focus, .cpanel div.icon a:active {
        background-position: 0 center;
        border-bottom-left-radius: 50% 20px;
        box-shadow: -5px 10px 15px rgba(0, 0, 0, 0.25);
        position: relative;
        z-index: 10;
    }
</style>