<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
/*
 * Objective  : List of all the project progress
 * Filename : project_progress.php
 * Created By : Yogesh Chandra Joshi <yogesh.joshi@ilmp-tech.com> 
 * Created On : 28 August 2014
 */
?>
<?php
global $db;
if (isset($submit) && $submit == 'Submit') {
    $fields_array = change_email();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $sql_query = 'select *from ' . _prefix('users') . ' where email="' . $email . '"';
        $execute = $db->sql_query($sql_query);
        if ($db->sql_numrows($execute) > 0) {
            $update_query = 'update ' . _prefix('users') . ' set email="' . $new_email . '" where email="' . $email . '"';
            $update_result = $db->sql_query($update_query);
            if ($update_result > 0) {
                $msg = common_message(1, constant('CHANGE_EMAIL'));
            }
        } else {
            $msg = common_message(0, constant('ADMIN_EMAIL_ID'));
        }
    }
} else {
    $errors = '';
    foreach ($response as $key => $message) {
        $error = true;
        $errors .= $message . "<br>";
    }
}
?>	
<style type="text/css">
    .successForm{
        margin-left:0px !important;
        margin-right: 0px !important;
    }


</style>


<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Change Admin Email</li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<div>
    <?php
    if (isset($errors)) {
        echo '<div style="color:#FF0000">' . $errors . '</div>';
    }
    if (isset($msg) && !empty($msg)) {
        echo $msg;
    }
    ?>
</div>
<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Change Email Id</h2>
        </div>
        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="changeEmail" id="changeEmail" action="" method="post" onsubmit="return valid()"  class="form-horizontal" role="form" >
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">Old Email Id<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="text"  name="email" id="email" value="" placeholder="example@email.com" value="<?php echo isset($_SESSION['username']) ? $_SESSION['username'] : ''; ?>" class="required form-control">  
            </div>
        </div>
        <div class="form-group">
            <label for="new_email" class="col-sm-2 control-label">New Email<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="text" name="new_email" id="new_email" placeholder="example@email.com"  class="required form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="confirm_email" class="col-sm-2 control-label">Confirm Email Id<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="text" name="confirm_email" id="confirm_email" placeholder="example@email.com"   equalTo='#new_email'  class="required form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <input type="submit" value="Submit" name="submit" class="submit_btn btn">
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#changeEmail').validate();
    });
</script>