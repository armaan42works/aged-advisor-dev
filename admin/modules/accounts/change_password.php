<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
/* * *********Change Admin Password Page*********** */
?>
<?php
global $db;
if (isset($submit) && $submit == 'Submit') {
    $fields_array = change_password();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        if (strlen($new_password) >= 6 && strlen($new_password) <= 30) {
            $useremail= $userid;
            $password = md5($old_password);
            $newpassword = md5($new_password);
            $sql_query = 'select *from ' . _prefix('users') . ' where email="' . $useremail . '" and password="' . $password . '"';
            $execute = $db->sql_query($sql_query);
            $records = $db->sql_fetchrowset($execute);
            if ($db->sql_numrows($execute) > 0) {
                $update_query = 'update ' . _prefix('users') . ' set password="' . $newpassword . '" where email="' . $useremail . '"';
                $update_result = $db->sql_query($update_query);
                if ($update_result > 0) {
                    $to = $records[0]['email'];
                    $username = ucwords($records[0]['first_name']." ". $records[0]['last_name']);
                    $email = emailTemplate('Admin_change_password');
                    if ($email['subject'] != '') {
                        $message = str_replace(array('{password}', '{username}','{userid}'), array($new_password, $username,$to), $email['description']);

                        $send = sendmail($to, $email['subject'], $message);
                        if ($send) {
                            $msg = common_message(1, constant('PASSWORD_CHANGED'));
                        }
                    }
                    $username = '';
                    $password = '';
                }
            } else {
                $msg = common_message(0, constant('PASSWORD_INCORRECT'));
                $username = '';
            }
        } else {
            $msg = common_message(0, constant('PASSWORD_LENGTH'));
            $username = '';
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Manage Account</li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<div>
    <?php
    if (isset($errors)) {
        echo '<div style="color:#FF0000">' . $errors . '</div>';
    }
    if (isset($msg) && !empty($msg)) {
        echo $msg;
    }
    ?>
</div>
<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class="">Change Password</h2>
        </div>
        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="changePassword" id="changePassword" action=""   onsubmit="return valid()" method="POST" class="form-horizontal" role="form" >
        <div class="form-group">
            <label for="username" class="col-sm-2 control-label">User Id<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="text"   name="userid" id="username" value="<?php echo isset($_SESSION['Admin']) ? $_SESSION['AdminId'] : ''; ?>" class="required form-control">  
            </div>
        </div>
        <div class="form-group">
            <label for="old_password" class="col-sm-2 control-label">Old Password<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="password" name="old_password" id="old_password" class="required form-control" value="">
            </div>
        </div>
        <div class="form-group">
            <label for="new_password" class="col-sm-2 control-label">New Password<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="password" name="new_password" id="new_password" class="required form-control">
            </div>
        </div>
        <div class="form-group">
            <label for="confirm_password" class="col-sm-2 control-label">Confirm Password<span class="redCol">*</span></label>
            <div class="col-sm-8">
                <input type="password" name="confirm_password" id="confirm_password"  equalTo='#new_password' class="required form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <input type="submit" value="Submit" name="submit" class="submit_btn btn">
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#changePassword').validate();
    });
</script>
