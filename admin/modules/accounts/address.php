<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
    /*
     * Objective  : admin can add the address
     * Filename :quote.php
     * Created By : Yogesh Chandra Joshi <yogesh.joshi@ilmp-tech.com> 
     * Created On : 27 August 2014
     */
    global $db;
    $description = '';
    include_once("./fckeditor/fckeditor.php");


    if (isset($submit) && $submit == 'Submit') {
        
        $fields_array = address();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {

            
            $fields = array(
                'address' => $address,
                
            );
            

           $where = "where id= '1'";
            $update_result = $db->update(_prefix('address'), $fields, $where);
            if ($update_result) {
                // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                redirect_to(MAIN_PATH . "/admin.php?manage_address");
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
 
    if ((key($_REQUEST)) == 'manage_address') {
        $sql_query = "SELECT * FROM " . _prefix("address") . " where id='1'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        if (count($records)) {

            foreach ($records as $record) {

                $address = $record['address'];
            }
        }
    }
?>
<form name="form1" id="addAddress" action="" method="post">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>

            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="javascript:void(0);">Manage Address</a></li>
                    </ul>
                    </div>
            </tr>

 <div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
        <?php
            if (isset($error)) {
                echo '<div style="color:#FF0000">' . $errors . '</div>';
            }
            if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                echo $_SESSION['msg'];
                unset($_SESSION['msg']);
            }
        ?>
    </div>  

        <tbody>
            <tr>
                <td>
                    <table  width="100%">

                        <tbody>


                            <tr>
                                <td colspan="3" class="bdr" height="25">

                                    <h2>Manage Address</h2><span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>

                                </td>




                            <tr>
                                    <td  align="left"  width='25%' height="26">Address : <span class="redCol">* </span> </td>
                                <td>
                                    <div class="input text">

                                        <textarea rows="4" cols="50" name="address" class="required" id=""> <?php
                                                if (isset($address)) {
                                                    echo stripslashes($address);
                                                }
                                            ?></textarea> 

                                    </div>
                                </td> 
                            </tr>

                            <tr>
                                <td></td>
                                <td class="marginleft r" align="right" height="26" width="31%">
                                    <?php
                                        if (key($_REQUEST) == 'edit') {
                                            ?> 
                                            <input type="submit" value="Update" name="update" src="<?php echo ADMIN_IMAGE; ?>submit.gif">

                                            <?php
                                        } else {
                                            ?>
                                            <input type="submit" value="Submit" name="submit" class="submit_btn">
                                            <?php
                                        }
                                    ?>
                                </td>                  



                            </tr>

                            </tr>
                        </tbody>

                    </table></td></tr></tbody></table></form>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

<script type="text/javascript">

    $('#addAddress').validate();
</script>