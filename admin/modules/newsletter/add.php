<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
global $db;
$description = '';
include_once("./fckeditor/fckeditor.php");

if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("newsletter") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {

        foreach ($records as $record) {

            $tille = $record['title'];
            $description = $record['message'];
        }
    }
}

if (isset($submit) && $submit == 'Submit') {

    $fields_array = newsletter();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $title = $_POST["title"];
        $message = $_POST["FCKeditor1"];

        $fields = array('title' => trim($title),
            'message' => $message,
        );

        $insert_result = $db->insert(_prefix('newsletter'), $fields);
        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/newsletter.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

if (isset($update) && $update == 'Update') {

    $fields_array = newsletter();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $title = $_POST["title"];
        $message = $_POST["FCKeditor1"];


        $fields = array('title' => trim($title),
            'message' => $message,
        );

        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('newsletter'), $fields, $where);
        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/newsletter.php?manage");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>


<div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;margin-left:35px;">
<?php
if (isset($error)) {
    echo '<div style="color:#FF0000">' . $errors . '</div>';
}
?>
</div>  
<form name="form1" id="addNewsLetter" action="" method="post">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>

            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "newsletter.php?manage" ?>">Manage Newsletter</a>>>Add</li>
                        </ul>
                    </div>
            </tr>             

        <tbody>
            <tr>
                <td>
                    <table  width="100%">

                        <tbody>


                            <tr>
                                <td colspan="2" class="bdr" height="25">

                                    <h2>Add Newsletter</h2><span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>

                                </td>


                            <tr>

                                <td  align="left" height="26" width="100%">Title <span
                                        class="redCol">* </span></td>

                            </tr>

                            <tr>

                                <td width="100%">
                                    <div class="input text">
                                        <input type="text" name="title" minlength="2" maxlength="50" size="50" value="<?php if (isset($tille)) {
    echo stripslashes($tille);
} ?>" />
                                    </div>
                                </td> 
                            </tr>

                            <tr>
                                <td  width="100%" height="26"> Message: <span
                                        class="redCol">* </span></td>

                            </tr>

                            <tr>
                                <td >
                                    <div class="input text">
<?php
$oFCKeditor = new FCKeditor('FCKeditor1');
$oFCKeditor->BasePath = './fckeditor/';
$oFCKeditor->Height = '400px';
$oFCKeditor->Value = "$description";

$oFCKeditor->Create();
?>

                                    </div>
                                </td> 

                            </tr>



                            <tr>
                                <td class="marginleft r" align="right" height="26" width="31%">
<?php
if (key($_REQUEST) == 'edit') {
    ?> 
                                        <input type="submit" value="update" name="Update" class="submit_btn"></td>

    <?php
} else {
    ?>
                            <input type="submit" value="Submit" name="submit" class="submit_btn"></td>
                                        <?php
                                    }
                                    ?>
                        </td>
                        <td class="r marginleft" width="69%">



                            </tr>

                            <!-- content area ends here --></td>
            </tr>
        </tbody>

        </tbody></table></td></tr></tbody></table></form>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

