
<script type="text/javascript">
    $(document).ready(function() {
        var page = 0;
        var li = '0_no';
        changePagination(page, li, 'newsletter');
    });
</script> 
<div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
    <?php
    if (isset($error)) {
        echo '<div style="color:#FF0000">' . $errors . '</div>';
    }
    if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    ?>
</div> 
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

    <tbody>


        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Manage Newsletter</li>
                    </ul>
                </div>
            </td>
        </tr>       

    <tbody>
        <tr>
            <td>
                <table  align="center" border="0" cellpadding="0" cellspacing="0"
                        width="100%">

                    <tbody>


                        <?php
                        $query = "SELECT * FROM " . _prefix("newsletter") . " where deleted = 0 ";
                        $res = $db->sql_query($query);
                        $count = $db->sql_numrows($res);
                        if ($count > 0) {
                            $paginationCount = getPagination($count);
                        }
                        echo '<div id="pageData"></div>';
                        echo get_pagination_link($paginationCount, 'newsletter');
                        ?>    
                    </tbody>

                </table>
            </td>
        </tr>
    </tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

