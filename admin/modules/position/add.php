<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>

<?php
global $db;
$description = '';
include_once("./fckeditor/fckeditor.php");

function generate_unique_id() {
    $randomString = substr(number_format(time() * rand(), 0, '', ''), 0, 9);
    $checkCode = "SELECT unique_id FROM " . _prefix("position") . " where unique_id = '$randomString'";
    $result = mysqli_query($db->db_connect_id,$checkCode);
    $num = mysqli_num_rows($result);
    if ($num > 0) {
        generate_unique_id();
    }
    return 'J' . $randomString;
}

if (key($_REQUEST) == 'edit') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM " . _prefix("position") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {

        foreach ($records as $record) {

            $job_title = $record['job_title'];
            $interest_area = $record['interest_area'];
            $compensation = $record['compensation'];
            $description = $record['description'];
            $expire_date = $record['expire_date'];
        }
    }
}

if (isset($submit) && $submit == 'Submit') {
    $fields_array = add_positon();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {

        $unique_id = generate_unique_id();

        $fields = array(
            'job_title' => $job_title,
            'unique_id' => $unique_id,
            'interest_area' => $interest_area,
            'compensation' => $compensation,
            'description' => $_POST['FCKeditor1'],
            'expire_date' => $expire_date);

        $insert_result = $db->insert(_prefix('position'), $fields);
        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/position.php?position");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}

if (isset($update) && $update == 'Update') {

    $fields_array = add_positon();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {


        $fields = array(
            'job_title' => $_POST['job_title'],
            'interest_area' => $_POST['interest_area'],
            'compensation' => $_POST['compensation'],
            'description' => $_POST['FCKeditor1'],
            'expire_date' => date('Y-m-d H:i:s', strtotime($_POST['expire_date'])));



        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('position'), $fields, $where);
        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "/position.php?position");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;margin-left:35px;">
    <?php
    if (isset($error)) {
        echo '<div style="color:#FF0000">' . $errors . '</div>';
    }
    ?>
</div> 
<form name="AddPosition" id="AddPosition" action="" method="post">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>

            <tr style="background:#4b99e6;">
                <td>
                    <div class="breadcrumb">
                        <ul>
                            <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "position.php?position" ?>">Manage Positions</a>>>Add</li>
                        </ul>
                    </div>
                </td>
            </tr>

        <tbody>
            <tr>
                <td>
                    <table width="100%">

                        <tbody>
                            <tr>
                                <td colspan="2" class="bdr" height="25">

                                    <h2>Add User</h2><span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>

                                </td>
                            </tr>
                            <tr>

                                <td class="marginleft" align="right" height="26" width="31%">Job Title: <span
                                        class="redCol">* </span></td>
                                <td class="marginleft" width="69%">
                                    <div class="input text">
                                        <input type="text" name="job_title" minlength="2" maxlength="50" size="50" class="required" value="<?php
    if (isset($job_title)) {
        echo stripslashes($job_title);
    }
    ?>" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="marginleft" align="right" height="26">Industry of Interest: <span
                                        class="redCol">* </span></td>
                                <td class="marginleft"><div class="input text">
                                        <input type="text" name="interest_area" id="" class="required" minlength="2" maxlength="50" size="50" value="<?php
                                        if (isset($interest_area)) {
                                            echo stripslashes($interest_area);
                                        }
    ?>" />
                                    </div></td>
                            </tr>
                            <tr>
                                <td class="marginleft" align="right" height="26">Compensation:</td>
                                <td class="marginleft"><div class="input text">
                                        <input type="text" name="compensation" id="" minlength="2" maxlength="50" size="50" value="<?php
                                        if (isset($compensation)) {
                                            echo stripslashes($compensation);
                                        }
    ?>" />
                                    </div></td>
                            </tr>
                            <tr class="business">

                                <td class="marginleft" align="right" height="26" width="31%">Job Description: <span
                                        class="redCol">* </span></td>
                                <td class="marginleft" width="69%">
                                    <?php
                                    if (isset($record['description'])) {
                                        $description = $record['description'];
                                    }
                                    ?>
                                    <div class="input text">
                                        <?php
                                        $oFCKeditor = new FCKeditor('FCKeditor1');
                                        $oFCKeditor->BasePath = './fckeditor/';
                                        $oFCKeditor->Height = '300px';
                                        $oFCKeditor->Width = '600px';
                                        $oFCKeditor->Value = "$description";

                                        $oFCKeditor->Create();
                                        ?>
                                    </div></td>
                            </tr>

                            <tr>
                                <td class="marginleft" align="right" height="26">Expiration Date: <span
                                        class="redCol">* </span></td>
                                <td class="marginleft"><div class="input text">
                                        <input type="text" name="expire_date" id="expire_date" maxlength="30" size="30" class="registrationTextbox required" value="<?php
                                        if (isset($expire_date)) {
                                            echo stripslashes(date('M d, Y', strtotime($expire_date)));
                                        }
                                        ?>"/>
                                    </div></td>
                            </tr>









                            <tr>
                                <td class="marginleft" align="right" height="26" width="31%"></td>
                                <td class="marginleft" width="69%">
                                    <?php if (key($_REQUEST) == 'add') { ?>
                                        <input type="submit" value="Submit" name="submit" class="submit_btn">
                                    <?php } else { ?>
                                        <input type="submit" value="Update" name="update" class="submit_btn">
                                    <?php } ?>
                                </td> </tr>

                            <!-- content area ends here --></td>
                            </tr>
                        </tbody>

                    </table>
                </td>
            </tr>
        </tbody></td></tr></tbody></table></form>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

<script type="text/javascript">
    $(document).ready(function() {

        $('#expire_date').datepicker({
            dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            minDate: '0'
        });

        $('#AddPosition').validate();

    });

</script>
