<?php
    global $db;
    $description = '';
    include_once("./fckeditor/fckeditor.php");

    if (key($_REQUEST) == 'add') {
        $id = $_GET['id'];

        $sql_query = "SELECT id,client_id FROM  " . _prefix("quotes") . " where md5(id)='$id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);

        $client_id = $records[0]['client_id'];
        $quote_id = $records[0]['id'];

        $sql_query = "SELECT user_type ,email FROM  " . _prefix("users") . " where id ='$client_id'";
        $res = $db->sql_query($sql_query);
        $records = $db->sql_fetchrowset($res);
        $to = $records[0]['email'];
        $user_type = $records[0]['user_type'];
    }


    if (isset($submit) && $submit == 'Send') {


        $fields_array = message();
        $response = Validation::_initialize($fields_array, $_POST);
        if (isset($response['valid']) && $response['valid'] > 0) {
            $body = $_POST["FCKeditor1"];

            $getEmail = "SELECT id from " . _prefix('users') . " WHERE email like '$to' ";
            $res = $db->sql_query($getEmail);
            $data = $db->sql_fetchrow($res);

            $fields = array(
                'email_id' => $to,
                'subject' => $subject,
                'body' => $body,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'user_type' => $user_type,
                'quote_id' => $quote_id,
                'sender_id' => '0',
                'receiver_id' => $data['id'],
                'is_read_admin' => 1
            );

            $insert_result = $db->insert(_prefix('messages'), $fields);
            $message_id = mysqli_insert_id($db->db_connect_id);
            if ($insert_result) {


                $attachment = array();
                if (isset($_FILES['file_name'])) {
                    for ($i = 0; $i < count($_FILES['file_name']['name']); $i++) {

                        if ($_FILES['file_name']['error'][0] == 0) {
                            $fileData = pathinfo($_FILES['file_name']["name"][$i]);
                            $target_path = '../admin/files/message/';
                            $filename = rand(1, 100000);
                            $date = new DateTime();
                            $timestamp = $date->getTimestamp();
                            $imageName = $filename . $timestamp . '_' .imgNameSanitize(basename($_FILES['file_name']['name'][$i]),20);
                            $target_path = $target_path . $imageName;
                            $message_fields = array('message_id' => $message_id, 'filename' => $imageName);
                            if ($_FILES['file_name']['name'][$i] != "") {

                                $insert_attachment = $db->insert(_prefix('attachment'), $message_fields);
                            }

                            $var = move_uploaded_file($_FILES['file_name']["tmp_name"][$i], $target_path);
                            $attachment[$i] = ADMIN_PATH . 'files/message/' . $imageName;
                        }
                    }
                }

                $send = sendmail($to, $subject, $body, $attachment);
                $msg = common_message(1, constant('MESSAGE'));
                $_SESSION['msg'] = $msg;

                if ($user_type == 0) {
                    redirect_to(MAIN_PATH . "/message.php?client");
                } else {
                    redirect_to(MAIN_PATH . "/message.php?sp");
                }
            }
        } else {
            $errors = '';
            foreach ($response as $key => $message) {
                $error = true;
                $errors .= $message . "<br>";
            }
        }
    }
?>
<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<form name="form1" id="addMessage" action="" method="post" enctype="multipart/form-data">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>
        <div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px; margin-left:34px;">
            <?php
                if (isset($error)) {
                    echo '<div style="color:#FF0000">' . $errors . '</div>';
                }
                if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
                    echo $_SESSION['msg'];
                    unset($_SESSION['msg']);
                }
            ?>
        </div>

        <tr>
            <td>
                <div class="breadcrumb"><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "message.php?manage" ?>">Manage Message</a>>><b>Send Message</b></div>
            </td>

        </tr>
        <tbody>
            <tr>
                <td>
                    <table align="center" border="0" cellpadding="0" cellspacing="0"
                           width="100%">

                        <tbody>


                        <tbody>
                            <tr>
                                <td colspan="2" class="bdr" height="25">

                                    <h2>Send Message</h2><span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>

                                </td>
                            </tr>
                        </tbody>


                        <tr>

                            <td  align="left" height="26" width="100%"><span
                                    class="redCol">* </span>Subject</td>

                        </tr>

                        <tr>

                            <td width="100%">
                                <div class="input text">
                                    <input type="text" name="subject" class="required" id="subject" minlength="2" maxlength="50" size="50" value="<?php
                                        if (isset($subject)) {
                                            echo stripslashes($subject);
                                        }
                                    ?>" />
                                </div>
                            </td> 
                        </tr>

                        <tr>
                            <td  width="100" height="26"><span
                                    class="redCol">* </span> Message:</td>

                        </tr>

                        <tr>
                            <td >
                                <div class="input text">
                                    <?php
                                        $oFCKeditor = new FCKeditor('FCKeditor1');
                                        $oFCKeditor->BasePath = './fckeditor/';
                                        $oFCKeditor->Height = '300px';
                                        $oFCKeditor->Width = '800px';
                                        $oFCKeditor->Value = "";

                                        $oFCKeditor->Create();
                                    ?>

                                </div>
                            </td> 

                        </tr>

                        <tr>

                            <td><div class="input text" >
                                    <input type="file" name="file_name[]" id="file_name">
                                </div>
                                <div class="input text add" >
                                    <a href="javascript:void(0);" id="0" class="addFile">+Add</a>
                                </div></td>
                        </tr>
                        <tr>
                            <td class="marginleft r" align="right" height="26" width="31%"><input  style="margin-left:35%;" type="submit" value="Send" name="submit" class="submit_btn"></td>
                            <td class="r marginleft" width="69%">
                            </td>

                        </tr>

                        <!-- content area ends here --></td>
            </tr>
        </tbody>

    </table>
</td>
</tr>
</tbody></tbody></table></form>



<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

    <tbody>

    <tbody>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0"
                       width="100%">

                    <tbody>




                        <?php
                            $sql_query = "SELECT * FROM   " . _prefix("messages") . " where md5(quote_id)='$id' && (parent_message_id IS NULL ||  parent_message_id = '0') ORDER BY created DESC";
                            $res = $db->sql_query($sql_query);
                            $records = $db->sql_fetchrowset($res);
                            $count = $db->sql_numrows($res);

                            if ($count > 0) {
                                ?>
                                <tr>
                                    <th align="left" width="40%">From</th>
                                    <th align="left" width="30%">Subject</th>
                                    <th align="left" width="30%">Created</th>

                                </tr>                           
                                <?php
                                foreach ($records as $key => $record) {
                                    $getnom = "SELECT count(*) as total FROM " . _prefix("messages") . " WHERE parent_message_id = '{$record['id']}'";
                                    $resNom = $db->sql_query($getnom);
                                    $dataNom = $db->sql_fetchrow($resNom);
                                    $nom = $dataNom['total'];

                                    if ($nom > 0) {
                                        $nom = ' (' . $nom . ')';
                                    } else if ($nom == 0) {
                                        $nom = '';
                                    }
                                    if ($record['sender_id'] == 0) {
                                        $senderName = constant('ADMINNAME');
                                    } else {
                                        $getName = "SELECT name FROM " . _prefix("users") . " WHERE  id = '{$record['sender_id']}'";
                                        $res = $db->sql_query($getName);
                                        $name = $db->sql_fetchrow($res);
                                        $senderName = $name['name'];
                                    }
                                    ?>      
                                    <tr class='clickableRow' href=" <?php echo HOME_PATH_URL . "message.php?view&id=" . md5($record['id']); ?>" style="cursor:pointer;">

                                        <td><?php echo $senderName . $nom; ?></td>
                                        <td><?php echo $record['subject']; ?></td>
                                        <td><?php echo date('M d, Y', strtotime($record['created'])); ?></td>
                                    </tr>

                                    <?php
                                }
                            } else {
                                ?>
                            <div class="norecord"> No record Found</div>
                            <?php
                        }
                    ?>

    </tbody>

</table>
</td>
</tr>
</tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>



<script type="text/javascript">


    $(document).ready(function() {

        $('#addMessage').validate({
            rules: {
                file_name: {
                    extension: "pdf|doc|docx|ppt|pptx|pps|ppsx|odt|xls|xlsx"
                }
            },
            messages: {
                file_name: {
                    extension: "Please upload valid file formats"
                }
            }
        });

        $(".clickableRow").click(function() {
            window.document.location = $(this).attr("href");

        });

        $('.addFile').click(function() {
            var inputFile = '<div class="input text" ><input type="file" name="file_name[]" id="file_name"></div>';
            $('.add').before(inputFile);
        });



    });
</script>

