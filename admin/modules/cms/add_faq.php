<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
</style>
<?php
global $db;
$content = "";
$admin_id = $_SESSION['Aid'];

include_once("./fckeditor/fckeditor.php");
if (key($_REQUEST) == 'edit_Faq') {
    $id = $_GET['id'];
    $sql_query = "SELECT * FROM  " . _prefix("faqs") . " where md5(id)='$id'";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrowset($res);
    if (count($records)) {

        foreach ($records as $record) {

            $question = $record['title'];
            $content = $record['content'];
        }
    }
}

if (isset($submit) && $submit == 'Submit') {

    $fields_array = cms_faq();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {
        $getMax = "SELECT max(order_by) as count from " . _prefix("faqs") . " WHERE status = 1 && deleted = 0";
        $resMax = $db->sql_query($getMax);
        $dataMax = mysqli_fetch_assoc($resMax);
        $order = ($dataMax['count'] == NULL) ? 1 : $dataMax['count'] + 1;

        $fields = array(
            'title' => trim($question),
            'created_by' => $admin_id,
            'order_by' => $order,
            'content' => $FCKeditor1
        );
//prd($fields);
        $insert_result = $db->insert(_prefix('faqs'), $fields);


        if ($insert_result) {
            // Message for insert
            $msg = common_message(1, constant('INSERT'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "cms.php?manage_FAQ");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
if (isset($update) && $update == 'Update') {

    $fields_array = cms_faq();
    $response = Validation::_initialize($fields_array, $_POST);
    if (isset($response['valid']) && $response['valid'] > 0) {


        $fields = array(
            'title' => trim($_POST['question']),
            'modified_by' => trim($admin_id),
            'content' => $_POST['FCKeditor1'],
            'modified' => date('Y/m/d h:i:s', time())
        );


        $where = "where md5(id)= '$id'";
        $update_result = $db->update(_prefix('faqs'), $fields, $where);

        if ($update_result) {
            // Message for insert
            $msg = common_message(1, constant('UPDATE'));
            $_SESSION['msg'] = $msg;
            //ends here
            redirect_to(MAIN_PATH . "cms.php?manage_FAQ");
        }
    } else {
        $errors = '';
        foreach ($response as $key => $message) {
            $error = true;
            $errors .= $message . "<br>";
        }
    }
}
?>
<div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
    <div class="col-sm-offset-2 col-sm-6 success">
        <?php if (isset($error)) { ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $errors; ?>.<br>
            </div>
            <!--echo '<div style="color:#FF0000">' . $errors . '</div>';-->
            <?php
        }
        if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
            ?>
            <div class="errorForm" id="error" style="display: block;">
                <img align="absmiddle" src="<?php echo HOME_PATH . 'admin/images/error.png'; ?>">&nbsp;<?php echo $_SESSION['msg']; ?>.<br>
            </div>
            <?php
            unset($_SESSION['msg']);
        }
        ?>
    </div>
</div>
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">
    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "cms.php?manage_FAQ" ?>">Manage FAQs</a>>>Add</li>
                    </ul>
                </div>
            </td>
        </tr>
    </tbody>
</table>


<div class="container" style="width: 95%; border: 1px solid rgb(204, 204, 204);">
    <div class="row" style="margin-bottom: 20px; border-bottom: 1px solid rgb(204, 204, 204);">
        <div class="col-sm-6">
            <h2 class=""><?php echo (key($_REQUEST) == 'edit_Faq') ? 'Edit' : 'Add'; ?> FAQ</h2>
        </div>
        <div class="col-sm-6">
            <h2 style="float:right;" class="redCol small">* fields required</h2>
        </div>
    </div>
    <p class="clearfix"></p>
    <form name="addEmailTemplate" id="addEmailTemplate" method="POST" action=""  class="form-horizontal" role="form">
        <div class="form-group">
            <label for="question" class="col-sm-2 control-label">Question<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <input type="text"  class="required form-control" name="question" minlength="5" maxlength="250"  id="question"  value="<?php echo isset($question) ? stripslashes($question) : ''; ?>"/>
            </div>
        </div>        
        <?php
        if (isset($_POST['FCKeditor1'])) {
            $content = $_POST['FCKeditor1'];
        } elseif (isset($content)) {
            $content = $content;
        } else {
            $content = '';
        }
        ?>
        <div class="form-group">
            <label for="content" class="col-sm-2 control-label">Answer<span class="redCol">* </span></label>
            <div class="col-sm-8">
                <?php
                $oFCKeditor = new FCKeditor('FCKeditor1');
                $oFCKeditor->BasePath = './fckeditor/';
                $oFCKeditor->Height = '400px';
                $oFCKeditor->Value = $content;
                $oFCKeditor->Create();
                ?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-8">
                <?php
                if (key($_REQUEST) == 'edit_Faq') {
                    ?>
                    <input type="submit" value="Update" name="update" class="submit_btn btn" src="<?php echo ADMIN_IMAGE; ?>submit.gif">
                    <?php
                } else {
                    ?>
                    <input type="submit" value="Submit" name="submit" class="submit_btn btn">
                    <?php
                }
                ?>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('input[type=submit]').focus();
        $('#addEmailTemplate').validate();
    });
</script>

<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>