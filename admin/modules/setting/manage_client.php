
<?php
$records = array();
$type = 1;
$sql_query = "SELECT * FROM  " . _prefix("point_earning_setting") . " WHERE type= $type && deleted = 0 ";
$res = $db->sql_query($sql_query);
$num = mysqli_num_rows($res);
$records = $db->sql_fetchrowset($res);
?>
<div class="sublinks" height="40" valign="bottom" style="margin-bottom: 10px;">
    <?php
    if (isset($error)) {
        echo '<div style="color:#FF0000">' . $errors . '</div>';
    }
    if (isset($_SESSION['msg']) && !empty($_SESSION['msg'])) {
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
    ?>
</div> 
<table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

    <tbody>
        <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>>Manage Client Person Earning Point</li>
                    </ul>
                </div>
            </td>
        </tr>          
        <tr>
            <?php
            if (empty($records)) {
                ?>
                <td>
                    <div class="breadcrumb" align="right"><a href="<?php echo HOME_PATH_URL . "setting.php?add_client"; ?>">Add Client Earning Point </a></div>
                </td>
                <?php
            }
            ?>
        </tr>


    <tbody>
        <tr>
            <td>
                <table  align="center" border="0" cellpadding="0" cellspacing="0"
                        width="100%">

                    <tbody>

                        <?php
                        if ($num != 0) {
                            ?>
                            <tr>

                                <th align="left">Project Points</th>
                                <th align="left">Social Sharing Points</th>
                                <th align="left">Client Referral Points</th>
                                <th align="left">Minimum $ (to redeem)</th>
                                <th align="left">Action</th>
                            </tr>
                            <?php
                        }
                        if ($num != 0) {
                            foreach ($records as $record) {
                                ?>

                                <tr class="row_<?php echo $record['id']; ?>">

                                    <td><strong>Points : </strong><?php echo $record['referral_point']; ?><br>
                                        <strong>value($) : </strong><?php echo $record['referral_value']; ?></td>
                                    <td><strong>Points : </strong><?php echo $record['social_point']; ?><br>
                                        <strong>value($) : </strong><?php echo $record['social_value']; ?></td>
                                    <td><strong>Points : </strong><?php echo $record['client_point']; ?><br>
                                        <strong>value($) : </strong><?php echo $record['client_value']; ?></td>
                                    <td><?php echo $record['min_value']; ?></td>
                                    <td><span class="status-<?php echo $record['id']; ?>"><a href="javascript:void(0);" id="point_earning_setting-<?php echo $record['id']; ?>-<?php echo $record['status'] ?>" class="status"><img src="<?php echo $record['status'] == 1 ? './img/activate.png' : './img/deactivate.png'; ?>" title="<?php echo $record['status'] == 1 ? 'Active' : 'Inactive'; ?>" /></a></span>
                                        <span><a href="<?php echo HOME_PATH_URL . "setting.php?add_client" ?>" title="Edit"><img src="./img/li_edit.png" ></a>  </span><span><a href="javascript:void(0);" class="delete" id="del-point_earning_setting-<?php echo $record['id'] ?>"><img src="./img/li_delete.png" title="Delete" ></a>  </span></td>
                                </tr>

                                <?php
                            }
                        } else {
                            ?>
                            <tr><td class="norecord">No record Found</td></tr>
                            <?php
                        }
                        ?>



                        <!-- content area ends here --></td>
                        </tr>
                    </tbody>

                </table>
            </td>
        </tr>
    </tbody></tbody></table>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>

