<style>
    th,td,tr{border:0px!important; }
    .input{background-color:white;}
    </style>
<?php
    /*     * ***************************************
     * Add Soil Offer Page 
     * *************************************** */

    global $db;
    if(isset($submit) && $submit == 'Submit'){
        $fields = array(
            'referral_point' => $referral_point,
            'referral_value' => $referral_value,
            'social_point' => $social_point,
            'social_value' => $social_value,
            'min_value' => $min_value,
            'deleted' => 0,
            'status' => 1,
            'modified' => date('Y-m-d h:i s')
        );
        if($user_type == 1){
            $fields['client_point'] = $client_point;
            $fields['client_value'] = $client_value;
        }
         $where = "where type= $user_type";
            $insert_result = $db->update(_prefix('point_earning_setting'), $fields, $where);
            if ($insert_result) {
                if($user_type == 1){
                     // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                    $main_obj->redirect(HOME_PATH_URL."setting.php?client");
                } else {
                     // Message for insert
                $msg = common_message(1, constant('INSERT'));
                $_SESSION['msg'] = $msg;
                //ends here
                    $main_obj->redirect(HOME_PATH_URL."setting.php?sp");
                }
               
                foreach ($_POST as $key => $value) {
                    $$key = '';
                }
            }
    }
    $sql_query = "SELECT * FROM  ". _prefix("point_earning_setting") ." WHERE type= $type ";
    $res = $db->sql_query($sql_query);
    $records = $db->sql_fetchrow($res);
?>
<form name="AddEarningPoint" id="AddEarningPoint" action="" method="post">
    <table align="center" border="0" cellpadding="0" cellspacing="0" width="95%">

        <tbody>
            
             <tr style="background:#4b99e6;">
            <td>
                <div class="breadcrumb">
                    <ul>
                        <li><a href="<?php echo HOME_PATH_URL; ?>admin.php"><i class="fa fa-home"></i>Home </a>>><a href="<?php echo HOME_PATH_URL . "setting.php?sp" ?>">Manage Earning Points</a>>><?php echo ($records['deleted'] == 1) ? 'Add' : 'edit'; ?></li>
                    </ul>
                </div>
           </tr> <tbody>
                            <tr>
                                <td>
                                    <table width="100%">
                                                
                                        <tbody>
                                                <td colspan="2" class="bdr" height="25">
                                                   
                                                        <h2><?php echo key($_REQUEST) == 'add_sp' ? 'Add Earning Points for Sale Person': "Add Earning Points for Client" ?></h2><span class="redCol" style="float:right; margin-top:-10px;">* fields required</span>
                                                   
                                                </td>
                                           
                                                     
                                            <input type="hidden"value="<?php echo $type; ?>" name="user_type">
                                            <tr><td><h1>Project Point Setting :</h1></td><td></td></tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Point earned per assigned project:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="referral_point" id="referral_point" maxlength="50" size="30" class="registrationTextbox number" value="<?php echo $records['referral_point']; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">$ earning per point :</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="referral_value" id="referral_value" maxlength="50" size="30" class="registrationTextbox number" value="<?php echo $records['referral_value']; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <?php if(key($_REQUEST) == 'add_client'){
                                                ?>
                                            <tr><td><h1>Client Referral Point Setting :</h1></td><td></td></tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Point earned per referred client:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="client_point" id="client_point" maxlength="50" size="30" class="registrationTextbox number" value="<?php echo $records['client_point']; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">$ earning per point :</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="client_value" id="cleint_value" maxlength="50" size="30" class="registrationTextbox number" value="<?php echo $records['client_value']; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <?php
                                            } ?>
                                            <tr><td><h1>Social Sharing Point Setting :</h1></td><td></td></tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Point per sharing and per week:</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="social_point" id="social_point" maxlength="50" size="30" class="registrationTextbox number" value="<?php echo $records['social_point']; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft" align="right" height="26">$ earning per point :</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="social_value" id="social_value" maxlength="50" size="30" class="registrationTextbox number" value="<?php echo $records['social_value']; ?>"/>
                                                    </div></td>
                                            </tr>
                                            
                                            <tr>
                                                <td class="marginleft" align="right" height="26">Minimum $ to earn to redeem points :</td>
                                                <td class="marginleft"><div class="input text">
                                                        <input type="text" name="min_value" id="min_value" maxlength="50" size="30" class="registrationTextbox number" value="<?php echo $records['min_value']; ?>"/>
                                                    </div></td>
                                            </tr>
                                            <tr>
                                                <td class="marginleft r" align="right" height="26" width="31%"></td>
                                                <td class="r marginleft" width="69%">
                                                <input type="submit" value="Submit" name="submit" class="submit_btn">

                                           </td> </tr>

                                            <!-- content area ends here --></td>
                                            </tr>
                                        </tbody>

                                    </table>
                                </td>
                            </tr>
                        </tbody></tbody></table></form>
<table class="ds_box" cellpadding="0" cellspacing="0" id="ds_conclass" style="display: none;">
    <tr><td id="ds_calclass"></td></tr></table>
<script type="text/javascript">
$(document).ready(function(){
    $('#AddEarningPoint').validate();
})

</script>
