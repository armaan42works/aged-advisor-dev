<?php
    include("../application_top.php");
    include(INCLUDE_PATH . "header.php");
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td class="lt_nav" height="455" valign="top" width="17%">
                <!-- left navigation -->
                <?php include(INCLUDE_PATH . "left_navigation.php"); ?>
                <!-- left navigation ends here -->
            </td>
            <td valign="top">
                <!-- content area here -->
                <div id="body_content">
                    <?php
                        switch (key($_REQUEST)) {
                            case 'addCity':
                                include(MODULE_PATH . "location/addcity.php");
                                break;
                            case 'editCity':
                                include(MODULE_PATH . "location/addcity.php");
                                break;
                            case 'manageCity':
                                include(MODULE_PATH . "location/managecity.php");
                                break;
                            case 'addSuburb':
                                include(MODULE_PATH . "location/addsuburb.php");
                                break;
                            case 'editSuburb':
                                include(MODULE_PATH . "location/addsuburb.php");
                                break;
                            case 'manageSuburb':
                                include(MODULE_PATH . "location/managesuburb.php");
                                break;
                            case 'addRestcare':
                                include(MODULE_PATH . "location/addrestcare.php");
                                break;
                            case 'editRestcare':
                                include(MODULE_PATH . "location/addrestcare.php");
                                break;
                            case 'manageRestcare':
                                include(MODULE_PATH . "location/managerestcare.php");
                                break;
                            default:
                                include(MODULE_PATH . "location/manage.php");
                                break;
                        }
                    ?>
                </div>
                <!-- content area ends here -->
            </td>
        </tr>
    </tbody>
</table>

