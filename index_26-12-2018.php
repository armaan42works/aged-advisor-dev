<?php
ini_set('display_errors', '1');
$prId = '';
$spId = '';
$userId = '';
$pagestart = microtime();



$GLOBALS['process_time_start'] = array();
record_mtime('TOTAL PAGE TIME');

function record_mtime($name = 'Undefined') {
    if ($_SERVER['REMOTE_ADDR'] == "103.211.15.112" || $_SERVER['REMOTE_ADDR'] == "125.236.218.32") {
        if (ISSET($GLOBALS['process_time_start'][$name]['start']) && !ISSET($GLOBALS['process_time_start'][$name]['end'])) {
            $GLOBALS['process_time_start'][$name]['end'] = microtime(true);
        }
        if (!ISSET($GLOBALS['process_time_start'][$name]['start'])) {
            $GLOBALS['process_time_start'][$name]['start'] = microtime(true);
        }
    }
}

function record_time($name = 'Undefined') {
    record_mtime($name);
}

//record_mtime('application_top.php');
include("application_top.php");

//record_mtime('application_top.php');

function parse_path() {
    $path = array();
    if (isset($_SERVER['REQUEST_URI'])) {
        $request_path = explode('?', $_SERVER['REQUEST_URI']);

        $path['base'] = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/');
        $path['call_utf8'] = substr(urldecode($request_path[0]), strlen($path['base']) + 1);
        $path['call'] = utf8_decode($path['call_utf8']);
        if ($path['call'] == basename($_SERVER['PHP_SELF'])) {
            $path['call'] = '';
        }
        $path['call_parts'] = explode('/', $path['call']);

        $path['query_utf8'] = urldecode($request_path[1]);
        $path['query'] = utf8_decode(urldecode($request_path[1]));
        $vars = explode('&', $path['query']);
        foreach ($vars as $var) {
            $t = explode('=', $var);
            $path['query_vars'][$t[0]] = $t[1];
        }
    }
    return $path;
}

function must_be_secure() {
    if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] !== 'on') {
        if (!headers_sent()) {
            header(sprintf(
                            'Location: https://%s%s', $_SERVER['HTTP_HOST'], $_SERVER['REQUEST_URI']
            ));
            exit();
        }
    }
}

$sql_query = "select * from " . _prefix("users") . " where id='" . $_SESSION['csId'] . "' AND user_type=0";
$resC = $db->sql_query($sql_query);
$recordCUser = $db->sql_fetchrow($resC);

if (!isset($_REQUEST['id'])) {
    if ($module != 'logOut') {
        if (ISSET($_SESSION['SupValidate']) && $_SESSION['SupValidate'] == 2) {
            
        } else if ($recordCUser['validate'] == '3') {
            redirect_to(HOME_PATH . 'changePassword?id=' . md5($_SESSION['csId']));
        }
    }
}

function precheckbasketid($strBasketID = '') {
    if ($strBasketID == "" && ISSET($_COOKIE['POI'])) {
        $strBasketID = $_COOKIE['POI'];
    }
    $strBasketID = cleanthishere($strBasketID);
    if ($strBasketID) {
        // resave the cookie so id does not dissapear.
        setcookie("POI", $strBasketID, time() + (140 * 86400), "/", ".agedadvisor.nz");
        $GLOBALS['strBasketID'] = cleanthishere($strBasketID);
    } else {
//generate them a basketid as no cookie exists.
        $strUID = uniqid(rand(), true);
        $strDate = date("dmY");

        $SESSION = $strUID . "-" . $strDate;
        $strBasketID = $strUID . "-" . $strDate;
        setcookie("POI", $SESSION, time() + (140 * 86400), "/", ".agedadvisor.nz");
        $GLOBALS['strBasketID'] = cleanthishere($strBasketID);
    }
    return $strBasketID;
}

precheckbasketid();

record_mtime('common_ajaxPaging.php');

include(INCLUDE_FILE . "common_ajaxPaging.php");
record_mtime('common_ajaxPaging.php');
record_mtime('header.php');
include(INCLUDE_FILE . "header.php");
record_mtime('header.php');

$pathInfo = parse_path();


$module = $pathInfo['call_parts']['0'];


$childModule = str_replace('_', '-', $pathInfo['call_parts']['1']);
/* added 29sept */
$subchmodule = str_replace('_', '-', $pathInfo['call_parts']['2']);
$subpchmodule = str_replace('_', '-', $pathInfo['call_parts']['3']);
$subppchmodule = str_replace('_', '-', $pathInfo['call_parts']['4']);
if ($module != 'supplier' || $childModule != 'changePassword') {
    if ($module != 'terms_and_conditions' && $childModule != 'logout') {
        if ($_SESSION['SupValidate'] == 3) {
            redirect_to(HOME_PATH . 'supplier/changePassword?id=' . md5($_SESSION['userId']));
        }
    }
}



// Build table access_urls
$this_page_and_queries = $_SERVER['REQUEST_URI'];

$goodurl = TRUE;

if (preg_match("/\.(swf|js|css|cgi|pl|exe|bat|reg|png|gif|jpg|jpeg|txt|text|tar|tgz|zip|xml)/i", $this_page_and_queries)) {
    $goodurl = FALSE;
}

if (preg_match("/(DELETE|\/cgi-bin|bootstrap|login|agedadvisor|\/admin|password|Malcolm|2\.php|supplier|3\.php|4\.php|5\.php|jpeg|txt|text|sex|porn|fuck|\_|\+|cunt|\/user\/|\/cron\/|Login|logout|message|members|\/retirement-village\/|\/aged-care\/|\-1\-|\-2\-|\-3\-|\-4\-|\-5\-|comhttp|prid|spid|user_id|favicon)/i", $this_page_and_queries)) {
    $goodurl = FALSE;
}

if (preg_match("/(basket\.php|search-results\.php|customerDetails|orderConfirmation|thankyou|CCard|cancel_order|\/review\/)/i", $this_page_and_queries)) {
    $goodurl = FALSE;
}

if (stristr($this_page_and_queries, '%') || stristr($this_page_and_queries, "'") || stristr($this_page_and_queries, '"')) {
    $goodurl = FALSE;
}


if ($goodurl) {    
    $sql = "insert into access_urls(url) values ('" . mysqli_escape_string($db->db_connect_id,$this_page_and_queries) . "') ";
    mysqli_query($db->db_connect_id,$sql) or die("Error Entering the client details");
}

// Make only some pages HTTPS comment out if making ALL pages HTTPS
if ($module == 'login') {
    must_be_secure();
}
if ($module == 'supplier') {
    must_be_secure();
}
if ($module == 'rating-supplier') {
    must_be_secure();
}

/* do changes in this code 29sept */


record_mtime("get page modules based on url, switch ($module)");


switch ($module) {

    case 'unsubscribe-stats':
        require_once 'modules/unsubscribe/index.php';
        break;

    case 'monthly-stats-report':
        require_once 'modules/monthly_stats/index.php';
        break;

    case 'view-competitions':
        require_once 'modules/competition/view-competitions.php';
        break;

    case 'new-media':
        require_once 'modules/competition/new-media.php';
        break;

    case 'view-all-entries':
        require_once 'modules/competition/view-all-entries.php';
        break;

    case 'view-all-pictures-modal':
        require_once 'modules/competition/view-all-pictures-modal.php';
        break;

    case 'view-all-entries-modal':
        require_once 'modules/competition/view-all-entries-modal.php';
        break;

    case 'view-media':
        require_once 'modules/competition/view-media.php';
        break;

    case 'edit_media':
        require_once 'modules/competition/edit_media.php';
        break;

    case 'facility-login':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/facility_login/facility-login.php';
        break;

    case 'shortlist':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/shortlist/shortlist.php';
        break;

    case 'facility-followup-response':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/facility-followup-response/index.php';
        break;

    case 'reportabuse':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/review/report_reason.php';
        break;


    case 'login':
        if (isset($_SESSION['csId'])) {
            $url = HOME_PATH;
            redirect_to($url);
        } else {
            require_once 'modules/customer/login.php';
            unset($_SESSION['registerName']);
            unset($_SESSION['registerEmail']);
        }

        break;

    case 'register':

        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/customer/register.php';

        break;

    case 'forgotPassword':

        require_once 'modules/customer/forget_password.php';

        break;

    case 'changePassword':
        if (isset($_SESSION['csId'])) {
            require_once 'modules/customer/change_password.php';
        } else {
            $url = HOME_PATH . 'login';
            redirect_to($url);
        }
        break;
    case 'customerIssue':
        if (isset($_SESSION['csId'])) {
            require_once 'modules/customer/issue_mail.php';
        } else {
            $url = HOME_PATH . 'login';
            redirect_to($url);
        }
        break;

    case 'logOut':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/customer/logouts.php';
        break;

    case 'compare':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/compare/compare.php';
        break;

    case 'request-quote':

        if (isset($_SESSION['userId'])) {
            $url = HOME_PATH . 'user/quote';
            redirect_to($url);
        } else {
            require_once 'modules/quote/request-quote.php';
        }
        break;

    case 'verification':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/supplier/verification.php';
        break;

    case 'verifyCustomer':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/customer/verification.php';
        break;

    case 'thank-you':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/supplier/thankYou.php';
        break;

    case 'thankyou':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/customer/thankYou.php';
        break;

    case 'sorry':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/supplier/sorry.php';
        break;


    case 'service':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/pages/page.php';
        break;

    case 'contactus':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/pages/page.php';
        break;

    case 'forum':
        if ($childModule == 'view') {
            require_once 'modules/forum/forumView.php';
            break;
        }if ($childModule == 'question') {
            require_once 'modules/forum/postquestion.php';
            break;
        } else {
            require_once 'modules/forum/forumList.php';
            break;
        }

    case 'blog':
        if ($childModule == 'view') {
            require_once 'modules/blog/blogView.php';
            break;
        } else {
            require_once 'modules/blog/blogList.php';
            break;
        }

    case 'aboutus':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/pages/aboutus.php';
        break;

    case 'validate':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/pages/validate.php';
        break;


    case 'forgetPassword':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/pages/forget_password.php';
        break;

    case 'changePassword':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/pages/change_password.php';
        break;

    case 'career':
        if ($pathInfo['call_parts']['1'] == 'apply') {
            require_once 'modules/pages/apply.php';
        } else {
            require_once 'modules/pages/career.php';
        }

        break;

    case 'facebook-login':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/fblogin/home.php';

        break;


    case 'login-twitter.php':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'login-twitter.php';
        break;

    case 'twitter':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once HOME_PATH . 'twitter/getTwitterData.php';
        break;

    case 'artwork':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/supplier/artwork.php';
        break;

    case 'search2':
        require_once 'modules/search/search2.php';
        break;

    case 'jpeg':
        require_once 'modules/search/searchProDetail_copy.php';
        break;

    case 'search':
        if ($childModule == 'search') {
            if ($subpchmodule != '') {
                redirect_to(HOME_PATH . '404');
            } else
                require_once 'modules/search/search.php';
            break;
        }else if ($childModule == 'for') {
            if ($subppchmodule != '') {
                redirect_to(HOME_PATH . '404');
            } else
                require_once 'modules/search/search.php';
            break;
        } else if ($childModule == 'productDetail') {
            require_once 'modules/search/productDetail.php';
            break;
        } else if ($childModule == 'serarchProDetail') {
            require_once 'modules/search/searchProDetail.php';
            break;
        } else if ($childModule == 'searchProDetail') {
            require_once 'modules/search/searchProDetail.php';
            break;
        } else if ($childModule == 'ajaxmessage') {
            require_once 'modules/search/ajaxmessage.php';
            break;
        } else if ($childModule == 'ajax-appointment') {
            require_once 'modules/search/ajax_appointment.php';
            break;
        } else if ($childModule == 'aged-care') {
            if ($subpchmodule != '') {
                redirect_to(HOME_PATH . '404');
            } else
                require_once 'modules/search/searchProDetail.php';
            break;
        }else if ($childModule == 'home-care') {
            if ($subpchmodule != '') {
                redirect_to(HOME_PATH . '404');
            } else
                require_once 'modules/search/searchProDetail.php';
            break;
        } else if ($childModule == 'retirement-village') {
            if ($subpchmodule != '') {
                redirect_to(HOME_PATH . '404');
            } else
                require_once 'modules/search/searchProDetail.php';
            break;
        } else if ($childModule == 'other') {
            if ($subpchmodule != '') {
                redirect_to(HOME_PATH . '404');
            } else
                require_once 'modules/search/searchProDetail.php';
            break;
        } else if ($childModule == 'shortSearch') {
            require_once 'modules/search/search_footer.php';
            break;
        } else if ($childModule == 'viewReview') {
            require_once 'modules/search/viewReview.php';
            break;
        }break;

    case 'global-search':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/search/global_search.php';
        break;


    case 'articles':
        if (($childModule == 'list') || ($childModule == '')) {
            require_once 'modules/article/list.php';
            break;
        } else if ($childModule == 'view') {
            require_once 'modules/article/view.php';
            break;
        } else {
            redirect_to(HOME_PATH . '404');
        }

        break;

    case 'reviews':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/review/list.php';
        break;
    case 'about-us':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/about_us.php';
        break;

    case 'event-view':
        require_once 'modules/cms/event_view.php';
        break;

    case 'consumer-information':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/consumer_information.php';
        break;

    case 'ajax-appointment':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/ajax_appointment.php';
        break;



    case 'community':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/community.php';
        break;

    case 'awards':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/awards-2018.php';
        break;

    case 'awards_new':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/awards_new.php';
        break;

    case 'awards-best-retirement-villages-aged-care-2015-2016':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/awards-2015-2016.php';
        break;


    case 'awards-best-retirement-villages-aged-care-2017':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/awards.php';
        break;


    case 'awards-best-retirement-villages-aged-care-2018':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/awards-2018.php';
        break;



    case 'shortlist-cookies':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/shortlist_cookies.php';
        break;



    case 'Provider-Comparison':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/provider_comparison/default_comparison.php';
        break;

    case 'Provider-Comparison-Test':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/provider_comparison/default_comparison-test.php';
        break;

    case 'Provider-Comparison-Report':
        if ($childModule == 'report') {
            require_once 'modules/provider_comparison/default_comparison_report.php';
            break;
        } else {
            redirect_to(HOME_PATH . '404');
        }
        break;
    case 'Provider-Comparison-Report-Test':
        if ($childModule == 'report') {
            require_once 'modules/provider_comparison/default_comparison_report_test.php';
            break;
        } else {
            redirect_to(HOME_PATH . '404');
        }
        break;

    case 'Compare-Retirement-Villages-Entry-Age-Fees-Conditions':// New URL
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/provider_comparison/default_comparison.php';
        break;

    case 'Compare-Retirement-Villages-Entry-Age-Costs-Fees-Conditions2':// New URL
        require_once 'modules/provider_comparison/default_comparison2.php';
        break;

    case 'Compare-Retirement-Villages-Entry-Age-Costs-Fees-Conditions':// New URL
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/provider_comparison/default_comparison.php';
        break;



    case 'cron-daily':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/cron_daily.php';
        break;


    case 'cron-daily-test':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/shortlist/cron_daily.php';
        break;

    case 'sponsors':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/sponsors.php';
        break;


    case 'events':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/all_events.php';
        break;

    case 'best-retirement-village-and-aged-care-awards':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/awards.php';
        break;
    case 'contact-us':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/contact_us.php';
        break;
    case 'formal-complaints-hdc-dhb-moh':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/formal-complaints-hdc-dhb-moh.php';
        break;
    case 'sponsors-advertising':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/sponsors_advertising.php';
        break;
    case 'turm-con':
        if ($childModule = '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/turm_con.php';
        break;
    case 'terms-and-conditions':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/turm_con.php';
        break;
    case 'faq':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/cms/faq.php';
        break;

    case 'rating':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/rating/rating_new.php';

        break;
    case 'rating-supplier':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/rating/rating_new.php';
        break;
    case 'rating-supplier2':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/rating/rating_new.php';
        break;
    case 'extra-facility':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/extra-facility-enquiry/index.php';
        break;
    case 'why_am_i_being_charged':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/extra-facility-enquiry/whycharges.php';
        break;
    case 'other-facilities':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/extra-facility-enquiry/notinterested.php';
        break;
    case 'rating-home-care':
        if ($childModule != '') {
            redirect_to(HOME_PATH . '404');
        } else
            require_once 'modules/rating/rating-home-care.php';
        break;

    case 'supplier':
        if ($childModule == 'ranking') {
            require_once 'modules/supplier/ranking.php';
            break;
        }

        if ($childModule == 'update-card') {
            require_once 'modules/payment/a_CCard_system.php';
            break;
        }
        if ($childModule == 'process') {
            require_once 'modules/supplier/process.php';
            break;
        }
        if ($childModule == 'addPaymentSuccess') {
            require_once 'modules/supplier/paymentSuccess.php';
            break;
        } elseif ($childModule == 'addNotifyPayment') {
            require_once 'modules/supplier/notifyPayment.php';
            break;
        }
        if (isset($_SESSION['userId'])) {


            if (isset($_SESSION['proEntry'])) {


                if ($childModule == 'productList') {
                    require_once 'modules/supplier/dashboardProList.php';
                    break;
                } elseif ($childModule == 'question') {
                    // if (isset($_SESSION['userId'])) {
                    require_once 'modules/forum/postquestion.php';
                    break;
                } elseif ($childModule == 'accountInfo') {
                    require_once 'modules/supplier/accountInfo.php';
                    break;
                } elseif ($childModule == 'editAccountInfo') {
                    require_once 'modules/supplier/editAccountInfo.php';
                    break;
                } elseif ($childModule == 'artwork') {
                    require_once 'modules/supplier/artwork.php';
                    break;
                } elseif ($childModule == 'addAddsBooking') {
                    require_once 'modules/supplier/addBookingManager.php';
                    break;
                } elseif ($childModule == 'editAddsBooking') {
                    require_once 'modules/supplier/addBookingManager.php';
                    break;
                } elseif ($childModule == 'editArtAddsBooking') {
                    require_once 'modules/supplier/adArtBookingManager.php';
                    break;
                } elseif ($childModule == 'add-manager') {
                    require_once 'modules/supplier/addBookingList.php';
                    break;
                } elseif ($childModule == 'artwork-manager') {
                    require_once 'modules/supplier/artworkManager.php';
                    break;
                } elseif ($childModule == 'product-manager') {
                    require_once 'modules/product/productManager.php';
                    break;
                } elseif ($childModule == 'product-views') {
                    require_once 'modules/product/productViews.php';
                    break;
                } elseif ($childModule == 'order-manager') {
                    require_once 'modules/order/orderManager.php';
                    break;
                } elseif ($childModule == 'xlsProductUpload') {
                    require_once 'modules/product/xlsProductUpload.php';
                    break;
                } elseif ($childModule == 'extraInfo') {
                    require_once 'modules/product/extraInfo.php';
                    break;
                } elseif ($childModule == 'feedback-center') {
                    require_once 'modules/supplier/feedback.php';
                    break;
                } elseif ($childModule == 'enquiry-center') {
                    require_once 'modules/supplier/enquiry.php';
                    break;
                } elseif ($childModule == 'feedback-view') {
                    require_once 'modules/supplier/viewFeedback.php';
                    break;
                } elseif ($childModule == 'payment') {
                    require_once 'modules/payment/payment.php';
                    break;
                } elseif ($childModule == 'paymentSuccess') {
                    require_once 'modules/payment/paymentSuccess.php';
                    break;
                } elseif ($childModule == 'paymentProcess') {
                    require_once 'modules/payment/paymentProcess.php';
                    break;
                } elseif ($childModule == 'directCredit') {
                    require_once 'modules/payment/directCredit.php';
                    break;
                } elseif ($childModule == 'addBookingPaymentProcess') {
                    require_once 'modules/supplier/addBookingPaymentProcess.php';
                    break;
                } elseif ($childModule == 'paymentSuccess') {
                    require_once 'modules/payment/paymentSuccess.php';
                    break;
                } elseif ($childModule == 'payment-final') {
                    require_once 'modules/payment/paymentFinal.php';
                    break;
                } elseif ($childModule == 'gallery-manager') {
                    require_once 'modules/gallery/galleryList.php';
                    break;
                } elseif ($childModule == 'addGallery') {
                    require_once 'modules/gallery/addGallery.php';
                    break;
                } elseif ($childModule == 'editGallery') {
                    require_once 'modules/gallery/addGallery.php';
                    break;
                } elseif ($childModule == 'editfirstGallery') {
                    require_once 'modules/gallery/editfirstGallery.php';
                    break;
                } elseif ($childModule == 'addProduct') {
                    require_once 'modules/product/addProduct.php';
                    break;
                } elseif ($childModule == 'issue-mail') {
                    require_once 'modules/supplier/issue_mail.php';
                    break;
                } elseif ($childModule == 'channel') {
                    require_once 'modules/supplier/intermediate.php';
                    break;
                } elseif ($childModule == 'logout') {
                    require_once 'modules/supplier/logout.php';
                    break;
                } else {
                    redirect_to(HOME_PATH . 'supplier/productList');
                    break;
                }
            } else {
                if ($childModule == 'productList') {
                    require_once 'modules/supplier/dashboardProList.php';
                    break;
                } elseif ($childModule == 'changePassword') {
                    require_once 'modules/supplier/change_password.php';
                    break;
                } elseif ($childModule == 'channel') {
                    require_once 'modules/supplier/intermediate.php';
                    break;
                } elseif ($childModule == 'addNewProduct') {
                    require_once 'modules/supplier/addNewProduct.php';
                    break;
                } elseif ($childModule == 'addEvent') {
                    require_once 'modules/supplier/addEvent.php';
                    break;
                } elseif ($childModule == 'eventEdit') {
                    require_once 'modules/supplier/eventEdit.php';
                    break;
                } elseif ($childModule == 'addEventList') {
                    require_once 'modules/supplier/addeventList.php';
                    break;
                } elseif ($childModule == 'analytics') {
                    require_once 'modules/supplier/analytics.php';
                    break;
                } elseif ($childModule == 'profile') {
                    require_once 'modules/supplier/profile.php';
                    break;
                } elseif ($childModule == 'editProfile') {
                    require_once 'modules/supplier/editProfile.php';
                    break;
                } elseif ($childModule == 'logout') {
                    require_once 'modules/supplier/logout.php';
                    break;
                } elseif ($childModule == 'stats') {
                    require_once 'modules/supplier/stats.php';
                    break;
                } else {
                    redirect_to(HOME_PATH . 'supplier/productList');
                    break;
                }
            }
        } else {

            if (isset($_SESSION['xlsUser'])) {
                require_once 'modules/supplier/change_password.php';
                break;
            }
            if ($childModule == 'register') {
                require_once 'modules/supplier/register.php';
                break;
            } elseif ($childModule == 'forgetPassword') {
                require_once 'modules/supplier/forget_password.php';
                break;
            } elseif ($childModule == 'notifyPayment') {
                require_once 'modules/payment/notifyPayment.php';
                break;
            } elseif ($childModule == 'logout') {
                require_once 'modules/supplier/logout.php';
                break;
            } elseif ($childModule == 'thank-you') {
                require_once 'modules/supplier/thankYou.php';
                break;
            } elseif ($childModule == 'login') {


                if (strpos($_SERVER['HTTP_REFERER'], 'logout') > 0) {
                    session_destroy($_SESSION['userId']);
                    session_destroy($_SESSION['name']);
                    session_destroy($_SESSION['userType']);
                    unset($_SESSION['userId']);
                    unset($_SESSION['userEmail']);
                    unset($_SESSION['name']);
                    unset($_SESSION['userType']);
                    $past = time() - 10;
                    setcookie('remember', '0', $past);
                    setcookie('AutoLoginId', '', $past);
                    unset($_COOKIE['remember']);
                    unset($_COOKIE['AutoLoginId']);
                }

                if (isset($_SESSION['userId'])) {
                    $url = HOME_PATH . 'supplier/productList';
                    redirect_to($url);
                } else {
                    require_once 'modules/supplier/slogin.php';
                    unset($_SESSION['registerName']);
                    unset($_SESSION['registerEmail']);
                }
                break;
            } else {
                $url = HOME_PATH . 'supplier/login';
                redirect_to(HOME_PATH . '404');
                break;
            }
        }


    case 'welcome': {
            if ($childModule != '') {
                redirect_to(HOME_PATH . '404');
            } else
                require_once 'modules/supplier/welcome.php';
        }
        break;
    case "404": {
            if ($childModule != '') {
                redirect_to(HOME_PATH . '404');
            } else {
                header($_SERVER["SERVER_PROTOCOL"] . " 404 Not Found");
                require_once 'modules/pages/404.php';
            }
        }
        break;

    default:
        if (!isset($pathInfo['call_parts']['0']) || $pathInfo['call_parts']['0'] == '') {
            require_once 'modules/pages/home.php';
        } else {
            redirect_to(HOME_PATH . '404');
        }
        break;
}
echo "indexpage";
            die();
record_mtime("get page modules based on url, switch ($module)");


include(INCLUDE_FILE . "footer.php");
?>
