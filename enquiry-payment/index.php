<?php
require_once 'eway-rapid-php-master/include_eway.php';
include("../application_top.php");
global $db;
$dateTime = date('Y-m-d H:i:s');
$responseData = array();
$card_name = $_POST['card_name'];
$card_number = $_POST['card_number'];
$card_cvn = $_POST['card_cvn'];
$card_month = $_POST['month'];
$card_year = $_POST['year'];
$enqid = $_POST['enqid'];
$supid = $_POST['supid'];
$prodid = $_POST['prodid'];

$qryupdates="update ad_enquiries set time_of_response='$dateTime' where md5(id)='$enqid'"; 
$resqrychk=$db->sql_query($qryupdates);

$isAlreadyPaidQuery = "SELECT id,Supplier_id,prod_id,enquiry_paid_status,time_of_enquiry from ad_enquiries where md5(id)='$enqid'";     
$isAlreadyPaidRes = mysqli_query($db->db_connect_id, $isAlreadyPaidQuery );

while ( $isAlreadyPaidData = mysqli_fetch_array( $isAlreadyPaidRes ) ) {
    $paidStatus = $isAlreadyPaidData['enquiry_paid_status']; 
    $supp_id = $isAlreadyPaidData['Supplier_id'];
    $product_id = $isAlreadyPaidData['prod_id'];
    $date_time = $isAlreadyPaidData['time_of_enquiry'];
}

$facDetailsQuery = "SELECT postal_address_city,postal_address_zip,zipcode,first_name,last_name,company,email,phone,address,physical_address,address_other,address_other,address_suburb,address_city,postal_address_suburb from ad_users where id = $supid";     
$facDetailsRes = mysqli_query($db->db_connect_id, $facDetailsQuery );

while ( $facDetailsData = mysqli_fetch_array( $facDetailsRes ) ) {   
    $facfirstname = $facDetailsData['first_name'];
    $faclastname = $facDetailsData['last_name'];
    $facemail = $facDetailsData['email'];
    $facphone = $facDetailsData['phone']; 
    $company = $facDetailsData['company'];         
    $address = $facDetailsData['address'];
    $physicaladdress = $facDetailsData['physical_address'];
    $address_other = $facDetailsData['address_other'];
    $address_suburb = $facDetailsData['address_suburb'];
    $address_city = $facDetailsData['address_city'];
    $postal_address_suburb = $facDetailsData['postal_address_suburb'];
    $postal_address_city = $facDetailsData['postal_address_city'];
    $postal_address_zip = $facDetailsData['postal_address_zip'];
    $zipcode = $facDetailsData['zipcode'];    
}

if($paidStatus != 1){

    $roomsQuery = "SELECT adpro.title as title, no_of_room, no_of_beds, certification_service_type from ad_extra_facility AS exfac"
            . " LEFT JOIN ad_products AS adpro ON adpro.id = exfac.product_id "
            . "where md5(product_id)='$prodid' AND exfac.status=1 AND adpro.status=1";     
    $roomsQueryRes = mysqli_query($db->db_connect_id, $roomsQuery );

    while ( $roomsQueryData = mysqli_fetch_array( $roomsQueryRes ) ) {
        $aprtmt = $roomsQueryData['no_of_rooms'];
        $beds = $roomsQueryData['no_of_beds'];
        $fac_type = $roomsQueryData['certification_service_type'];
        $title = $roomsQueryData['title'];
    }
    
    function nigel_format($value){
        if(floor($value)==$value){
            return $value;            
        }else{
            return number_format($value,2);            
        }
    }

    if($fac_type == "Aged Care"){    
        if($beds >=100){
            $amt = 1667.5;
            $withoutgst = nigel_format(14.50);
            $gst = nigel_format($withoutgst*.15);
            $msgamt = nigel_format($amt/100,2);
        }
        if($beds >= 40 && $beds <= 99){
            $amt = 1092.5;
            $withoutgst = nigel_format(9.50);
            $gst = nigel_format($withoutgst*.15);
            $msgamt = nigel_format($amt/100,2);
        }
        if($beds < 40 ){
            $amt = 572;
            $withoutgst = nigel_format(4.97);
            $gst = nigel_format($withoutgst*.15);
            $msgamt = nigel_format($amt/100,2);
        }
    }                       
    if($fac_type == "Retirement Village" || $fac_type == "Home Care" ){
        if($aprtmt >=30 && $aprtmt <= 79 ){
            $amt = 2817.5;
            $withoutgst = nigel_format(24.50);
            $gst = nigel_format($withoutgst*.15);
            $msgamt = nigel_format($amt/100,2);
        }
        if($aprtmt >= 80){
            $amt = 3392.5;
            $withoutgst = nigel_format(29.50);
            $gst = nigel_format($withoutgst*.15);
            $msgamt = nigel_format($amt/100,2);
        }
        if($aprtmt < 30 ){
            $amt = 2242.5;
            $withoutgst = nigel_format(19.50);
            $gst = nigel_format($withoutgst*.15);
            $msgamt = nigel_format($amt/100,2);
        }
    }
    
    /*$planIDQuery = "SELECT plan_id from ad_pro_plans where pro_id = '$product_id' and supplier_id ='$supp_id' and current_plan = 1";  
    $planIDRes = mysqli_query($db->db_connect_id, $planIDQuery );

    while ( $planIDData = mysqli_fetch_array( $planIDRes ) ) {
        $plan_id = $planIDData['plan_id'];       
    }*/
    
    if( $supid == 499 ) {
        //Sandbox Amit
        $apiKey = '60CF3Cq3ZVApLWeLbCJvMwgbPpxASn6axE/NIrR3ILe3I+C+xB9j/fqeBEyDCSQpS3CSnB';     
        $apiPassword = 'ISWpbLvS'; 
        $apiEndpoint = \Eway\Rapid\Client::MODE_SANDBOX;                
    }else{    
        $apiKey = 'C3AB9A4j+RgAVsZe/Z0qtHXiGCnEq1RNBp6yz7x8IznHXUjG8KZfmcy+v40gUYibhOAgHY';
        $apiPassword = 'Lus5Clkx';
        $apiEndpoint = \Eway\Rapid\Client::MODE_PRODUCTION;
    }
                
    $client = \Eway\Rapid::createClient($apiKey, $apiPassword, $apiEndpoint);

    $transaction = [
        'Customer' => [        
        'FirstName' => $facfirstname,
        'LastName' => $faclastname,        
        'Mobile' => $facphone,
        'Street1' => $address,
        'Street2' => $postal_address_suburb, 
        'City' => $postal_address_city, 
        'PostalCode' => $postal_address_zip,            
        'Email' => $facemail,                   
        'CardDetails' => [
            'Name' => $card_name,
            'Number' => $card_number,
            'ExpiryMonth' => $card_month,
            'ExpiryYear' => $card_year,
            'CVN' => $card_cvn,
            ]            
        ],
        'Payment' => [
            'TotalAmount' => $amt,
            'InvoiceDescription' => "Pay-Now-Enquiry-Payment"
        ],
        'TransactionType' => \Eway\Rapid\Enum\TransactionType::PURCHASE,
    ];
    $realamt = $amt/100;
    $response = $client->createTransaction(\Eway\Rapid\Enum\ApiMethod::DIRECT, $transaction);
    if ($response->TransactionStatus) {
        $message = 'Your payment of <strong>$'.$msgamt.' (incl. gst) </strong>was successful. Enquirers details displayed below. Transaction ID : '.$response->TransactionID;
        $responseData = array("status"=>1, "response_message"=>$message);
        $trid = $response->TransactionID;

        $enqUpdateQuery="UPDATE `ad_enquiries` SET `enquiry_paid_status` =  1, `eway_transaction_id` = '$trid', `eway_amount` = '$realamt' WHERE md5(id) = '$enqid'";
        mysqli_query($db->db_connect_id,$enqUpdateQuery);    
        
        $created = date("Y-m-d H:i:s");
	
        $realamt = nigel_format($realamt);
        
        $fields = array(
            'supplier_id' => $supp_id,
            'product_id' => $product_id,
            'plan_id' => $plan_id,
            'amount' => $realamt,
            'our_order_number' => 'PN'.$trid,
            'description' => 'Payment for instant enquiry access, no upgrade selected.',
            'created' => $created,
            'order_no' => '',
            'approve_status' => '1'
        );
        $insert_result = $db->insert(_prefix('direct_credit'), $fields);                 
       
        $headers = "MIME-Version: 1.0\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1\n";
        $headers .= 'From:   ';           
        $headers .= "reviews@agedadvisor.co.nz";            
        $headers .= "\n";
        $headers .= "Cc: accounts@agedadvisor.co.nz\r\n";                 
        $emailTemplate = emailTemplate('pay_now_for_enquiry');
        if ($emailTemplate['subject'] != '') {           
            $message = str_replace(array(
                '{first_name}', 
                '{last_name}', 
                '{company}', 
                '{address}', 
                '{postal_address_suburb}', 
                '{postal_address_city}', 
                '{postal_address_zip}',
                '{realamt}',
                '{date_of_payment}',
                '{inv_number}',
                '{withoutgst}',
                '{gst}',
                '{title}',
                '{date_time}'
            ), 
            array(
                $facfirstname, 
                $faclastname, 
                $company, 
                $address, 
                $postal_address_suburb, 
                $postal_address_city, 
                $postal_address_zip,
                $realamt,
                $created,
                $trid,
                $withoutgst,
                $gst,
                $title,
                $date_time
            ), 
            stripslashes($emailTemplate['description']));                        
            //@mail( 'amitsingh9thjan@gmail.com', $emailTemplate['subject'], $message, $headers );            
            @mail( $facemail, $emailTemplate[ 'subject' ], $message, $headers );
        }
        
    }else{
        $error_message = array();
        foreach ($response->getErrors() as $error) {
            $error_message[] = \Eway\Rapid::getMessage($error); 
        }
        $responseData = array("status"=>2, "response_message"=>$error_message);
    }

    echo json_encode($responseData);
}else{
    $alreadyPaidMessage = "A payment has already been made for this enquiry";
    $responseData = array("status"=>3, "response_message"=>$alreadyPaidMessage);
    echo json_encode($responseData);
}
?>
