<?php
/*
 * Objective : All the popup on admin panel
 * Filename : popup.php
 * Created By : Sanket Khanna <sanket.khanna@ilmp-tech.com>
 * Created On : 7 August 2014
 * Modified On : 01 September 2014
 */
include("application_top.php");
$id = $_GET['id'];
switch (key($_REQUEST)) {
    case "prodImage":
        echo prodImage($id);
        break;
    case "gImage":
        echo galleryImage($id);
        break;
}

function prodImage($id) {
    global $db;
    $id = $_GET["id"];
    $query = "SELECT image FROM `" . _prefix("products") . "`WHERE md5(id) = '$id'";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $data = $db->sql_fetchrow($res);
    // pr($data);
    ?>
    <style>
        th,td,tr{border:0px!important; }
        .input{background-color:white;}
    </style>
    <div class="text-center">
        <img style="" src="<?php echo HOME_PATH_URL . 'files/product/' . $data['image']; ?>" />
    </div>
    <?php
}

function galleryImage($id) {
    global $db;




    $useId = $id;
    $sql_queryL = "SELECT member.* FROM " . _prefix("users") . " AS user"
            . " Left join " . _prefix("membership_prices") . " AS member ON member.id=user.account_type WHERE user.id='$useId'";
    $resL = $db->sql_query($sql_queryL);
    $accountType = $db->sql_fetchrow($resL);



    $gallery_type = 0;
    $uid = $id;
    $query = "SELECT id, title,file, status,type, created FROM " . _prefix("galleries") . "   "
            . " where deleted = 0  AND user_id=" . $uid . "  ORDER BY id DESC";
    $res = $db->sql_query($query);
    $data = $db->sql_fetchrowset($res);
    $count = $db->sql_numrows($res);
    if ($count > 0) {
        $output.='<div class="col-md-8 thumbnail" id="popup"><p>You can choose upto ' . $accountType['image_limit'] . ' media</p>';
        $counter = 1;
        foreach ($data as $key => $record) {
            $ImageExt = array("jpg", "png", "jpeg", "gif");
            $VideoExt = array("ogg", "ogv", "avi", "mpe?g", "mov", "wmv", "flv", "mp4");
            $ext = end(explode('.', $record['file']));
            if (in_array($ext, $VideoExt)) {
//                $target_pathL = ADMIN_PATH . 'files/gallery/videos/';
//                $type = 1;
//                uploadImageVideo($target_pathL);
                $fileVideo = ADMIN_PATH . 'files/gallery/videos/' . $record['file'];
                $fileVideoSrc = HOME_PATH . 'admin/files/gallery/videos/' . $record['file'];
                $fileVideoView = HOME_PATH . 'admin/files/gallery/videos/' . $record['file'];
//            $output.=!empty($record['file']) ? '<div class="col-md-6"><input type="radio" class="imgid" name="gallery_image"  value="' . $record['file'] . '"> <a  class="videoPlayer" href="' . HOME_PATH . 'modules/gallery/play_video.php?name=' . $record['file'] . '"><img title="' . $record['file'] . '"  src="' . ADMIN_IMAGE . 'default_video.png" alt=""></a> </div>' : "";
                $output.=!empty($record['file']) ? '<div class="col-md-6"><input type="checkbox" id="' . $counter . '-video"  class="imgid" name="gallery_image"  value="' . $record['id'] . '"> <label for"' . $counter . '-video"><img title="' . $record['file'] . '"  src="' . ADMIN_IMAGE . 'default_video.png" alt=""></label> </div>' : "";
            } else {
                $fileImage = ADMIN_PATH . 'files/gallery/images/thumb_' . $record['file'];
                $fileImageSrc = HOME_PATH . 'admin/files/gallery/images/thumb_' . $record['file'];
                $fileImageView = HOME_PATH . 'admin/files/gallery/images/' . $record['file'];
//            $output.=!empty($record['file']) ? '<div class="col-md-6"><input type="radio" id="'.$counter.'-img" class="imgid" name="gallery_image"  value="' . $record['file'] . '"> <a  class="fancybox" title="' . $record['file'] . '" href="' . $fileImageView . '"><img title="' . $record['file'] . '"  src="' . $fileImageSrc . '" alt=""></a></div>' : "";
                $output.=!empty($record['file']) ? '<div class="col-md-6"><input type="checkbox" id="' . $counter . '-img" class="imgid" name="gallery_image"  value="' . $record['id'] . '"> <label for="' . $counter . '-img"><img title="' . $record['file'] . '"  src="' . $fileImageSrc . '" alt=""></label></div>' : "";
            }
            $counter++;
        }
        $output.='</div>';
        echo $output;
    } else {
        $output = '<div class="col-md-12 text-center text-danger thumbnail">No Record found to select.</div>';
        echo $output;
    }
    ?>
    <style>
        #popup{
            width:400px;
            height: 400px;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
            $(".videoPlayer").fancybox({
                'transitionIn': 'elastic',
                'transitionOut': 'elastic',
                'speedIn': 600,
                'speedOut': 200
            });
            $('.imgid').click(function() {
                var value = $(this).val();
                if ($(this).prop("checked") == true) {
                    var imageInput = '<input type="hidden" id="g-' + $(this).val() + '" name="gImage[]" value="' + $(this).val() + '" />';
                    parent.$('#hiddenField').append(imageInput)
                    if (parent.$('#test').val() == '') {
                        parent.$('#test').val(value);
                    } else {
                        var oldVal = parent.$('#test').val() + ',' + value;
                        parent.$('#test').val(oldVal);
                    }

                } else if ($(this).prop("checked") == false) {
                    if (parent.$('#g-' + value).length) {
                        parent.$('#g-' + value).remove();
                    }
                }


            });
            $(".image").colorbox({
                width: '600px',
                height: '500px;'
            });
            $("input:checkbox").change(function() {
                var limit = '<?php echo $accountType['image_limit']; ?>';
                var value = $(this).val();
                var n = $("input:checked").length;
                if (n > limit) {
                    alert('You can choose upto ' + limit);
                    $(this).prop("checked", false);
                }
                //                else if ($(this).prop("checked") == true) {
                //                    var id = 'productList-' + $(this).val();
                //                    $("#" + id).val(value);
                //                } else if ($(this).prop("checked") == false) {
                //                    var id = 'productList-' + $(this).val();
                //                    $("#" + id).val('');
                //                }
            });
        })
    </script>
    <?php
    die;
}
?>




