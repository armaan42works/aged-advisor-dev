<?php

    require_once("../application_top.php");
    global $db;
    $query = "SELECT plan.*, user.first_name, user.last_name, user.email FROM " . _prefix("pro_plans") . " AS plan LEFT JOIN ".  _prefix('users')." AS user ON plan.supplier_id= user.id WHERE plan.current_plan = '1' && plan.status = '1' && plan.deleted = '0'";
    $res = $db->sql_query($query);
    $count = $db->sql_numrows($res);
    $data = $db->sql_fetchrowset($res);
    if ($count > 0) {
        foreach ($data as $result) {
            $name = $result['first_name'] . ' ' . $result['last_name'];
            switch ($result['plan_id']) {
                case 2:
                    $subscription = 'Business [Yearly]';
                    break;
                case 5:
                    $subscription = 'Premium [Yearly]';
                    break;
                case 3:
                    $subscription = 'Business [Half yearly]';
                    break;
                case 6:
                    $subscription = 'Premium [Half yearly]';
                    break;
                case 4:
                    $subscription = 'Business [Quarterly]';
                    break;
                case 7:
                    $subscription = 'Premium [Quarterly]';
                    break;
                default :
                    $subscription = 'Basic';
                    break;
            }

            $now = strtotime(date('y-m-d')); // Current date
            $your_date = strtotime($result['plan_end_date']);  //date of subscription expire
            $datediff = $your_date - $now;  //difference b/w current date and expire date
            $checkDays = floor($datediff / (60 * 60 * 24));
            if ($checkDays == '1' || $checkDays == '7' || $checkDays == '30') {
                $emailTemplate = emailTemplate('sub_expire');
                if ($emailTemplate['subject'] != '') {
                    $message = str_replace(array('{username}', '{subscription}', '{ex_date}'), array(trim($name), $subscription, $result['plan_end_date']), stripslashes($emailTemplate['description']));
                    sendmail($result['email'], $emailTemplate['subject'], $message);
                }
            }
        }
    }
?>
