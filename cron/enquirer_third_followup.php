<?php
require_once("../application_top.php");
global $db;

$queryID = "SELECT DISTINCT user_id FROM ad_enquiries WHERE third_followup_sent = 'n' and `chosen_a_facility` IS NULL and DATE(time_of_enquiry) <= DATE_SUB(CURDATE(), INTERVAL 90 DAY)";
$queryIDRes = mysqli_query($db->db_connect_id, $queryID );

while ( $queryIDData = mysqli_fetch_array( $queryIDRes ) ) {
    $userID = $queryIDData['user_id']; 
    
    $query = "SELECT enquirer.user_id,enquirer.prod_id,facility.title,enquirer.Supplier_id,enquirer.time_of_enquiry,userdetails.first_name,"
        . "userdetails.last_name,userdetails.email,enquirer.id,facility.quick_url FROM " . _prefix("enquiries") . " AS enquirer "
        . "LEFT JOIN " . _prefix('users') ." AS userdetails ON enquirer.user_id= userdetails.id "
        . "LEFT JOIN " . _prefix('products') ." AS facility ON enquirer.prod_id = facility.id "
        . "WHERE enquirer.user_id = $userID and userdetails.email != 'n/a' GROUP BY enquirer.prod_id";   
    
    $queryRes = mysqli_query($db->db_connect_id, $query );
    $yes_we_decided = '';
    $noId;
    
    while ( $queryData = mysqli_fetch_array( $queryRes ) ) {
        
        $user_id = $queryData['user_id'];  
       
        $prod_id = $queryData['prod_id']; 
       
        $facility = $queryData['title'];        
                       
        $yes_we_decided = $yes_we_decided."<p>".'<a style="background-color: #042E6F;border-color: #042E6F;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;" href="'.HOME_PATH . 'rating?spId=' .base64_encode($queryData['Supplier_id']) . '&prId=' .base64_encode($queryData['prod_id']).'&resp=YES&enq=' .base64_encode($queryData['id']) . '">Yes we decided on : '.$facility.'</a></p>';
        
        $supplier_id = $queryData['Supplier_id']; 
       
        $date = $queryData['time_of_enquiry'];        
       
        $first_name = $queryData['first_name'];
       
        $last_name = $queryData['last_name'];
       
        $email = $queryData['email'];       
       
        $id = $queryData['id'];
        
        $noId = $id.'-'.$noId;
       
        $fullname = $first_name . ' ' . $last_name;
        
        $fields = array(
        'third_followup_sent' => "y",               
        );	
        $where = "where id=".$id."";           
        $update_result = $db->update(_prefix('enquiries'), $fields, $where);
       
    }   
    $noId = rtrim($noId,"-");
    
    $yes_we_decided = $yes_we_decided."<p>".'<a style="background-color: #4F5E94;border-color: #4F5E94;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;" href="https://www.agedadvisor.nz/rating-supplier?resp=YES&enq='.$noId.'&prId=NULL">Yes we decided on : A different facility </a></p>';
   
    $no = "https://www.agedadvisor.nz/search/for/1121/?resp=NO&enq=".$noId;
    $no_changed_mind = "<a style='background-color: #f15922;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='".$no."'> No, we changed our minds </a>";
   
    $notyet = "https://www.agedadvisor.nz/search/for/1121/?resp=NOTYET&enq=".$noId;
    $still_looking = "<a style='background-color: #f15922;border-color: #d23f0a;display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;cursor: pointer;background-image: none;border: 1px solid transparent;border-radius: 4px;color: #fff;' href='".$notyet."'> Still looking / deciding </a>";   
     
    $headers = "MIME-Version: 1.0\n";
    $headers .= "Content-type: text/html; charset=iso-8859-1\n";
    $headers .= 'From:   ';           
    $headers .= "reviews@agedadvisor.co.nz";            
    $headers .= "\n";
    $headers .= "Cc: reviews@agedadvisor.co.nz\r\n";                   
    $emailTemplate = emailTemplate('enquirer_second_followup_90days');
    if ($emailTemplate['subject'] != '') {            
        $message = str_replace(array(
            '{fullname}',                     
            '{facility_name}', 
            '{yes_we_decided}', 
            '{no_changed_mind}', 
            '{still_looking}', 
            '{date}'), 
        array(
            trim($fullname),                     
            $facility, 
            $yes_we_decided, 
            $no_changed_mind, 
            $still_looking, 
            $date), 
        stripslashes($emailTemplate['description']));                
        @mail( $email, $emailTemplate[ 'subject' ], $message, $headers );
    }     
}
?>
