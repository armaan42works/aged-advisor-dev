<?php

require_once("../application_top.php");
global $db;

$query = "SELECT enquirer.user_id,enquirer.prod_id,facility.title,enquirer.Supplier_id,enquirer.time_of_enquiry,userdetails.first_name,userdetails.last_name, "
        . " userdetails.email,enquirer.id FROM " . _prefix("enquiries") . " AS enquirer LEFT JOIN " . _prefix('users') ." AS userdetails ON "
        . " enquirer.user_id= userdetails.id LEFT JOIN " . _prefix('products') ." AS facility ON enquirer.prod_id = facility.id "
        //. " WHERE enquirer.user_id = 10463 ";
        . " WHERE enquirer.second_followup_sent = 'n' and `chosen_a_facility` IS NULL and userdetails.email != 'n/a' and DATE(enquirer.time_of_enquiry) <= DATE_SUB(CURDATE(), INTERVAL 150 DAY)"
        . " GROUP BY enquirer.user_id, enquirer.prod_id";

$res = $db->sql_query($query);
$count = $db->sql_numrows($res);

if($count>0){
    while($result = mysqli_fetch_row($res)) {        
        $user_id = $result[0];         
        $prod_id = $result[1];        
        $facility = $result[2];
        $supplier_id = $result[3]; 
        $date = $result[4];        
        $first_name = $result[5];
        $last_name = $result[6];
        $email = $result[7];
        $id = $result[8];        
              
        $fullname = $first_name . ' ' . $last_name;                
       
        $emailTemplate = emailTemplate('enquirer_second_followup');
        if ($emailTemplate['subject'] != '') {            
            $message = str_replace(array('{fullname}', '{facility_name}','{date}'), array(trim($fullname), $facility, $date), stripslashes($emailTemplate['description']));
            sendmail($email, $emailTemplate['subject'], $message);
            //sendmail('nigelmatthews@ihug.co.nz', $emailTemplate['subject'], $message);
            //sendmail('amitsingh9thjan@gmail.com', $emailTemplate['subject'], $message);
            $fields = array(
                        'second_followup_sent' => "y"                            
                      );	
            $where = "where id=".$id."";
            $update_result = $db->update(_prefix('enquiries'), $fields, $where);           
        }        
        
    }
}
?>
